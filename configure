#!/bin/sh
#
# Authors:	Qt Cryptographic Architecture library developers 
#		(http://delta.affinix.com/qca/)
#
# Modified by: 	Mariusz Borowski <b0mar@nes.pl>
# 

show_usage() {
cat <<EOT
Usage: ./configure [OPTION]...

This script creates necessary configuration files to build/install.

Main options:
  --prefix=[path]    	Base path for build/install.  Default: /usr/local
  --with-qt-dir=[path]  Directory where Qt is installed.
  --docdir=[path]	Documentation path. Default: prefix/doc
  --debug		Produce a debugging version of the application.
  --help             	This help text.

EOT
}

while [ $# -gt 0 ]; do
	case "$1" in
		--prefix=*)
			PREFIX=`expr "${1}" : "--prefix=\(.*\)"`
			shift
			;;

		--with-qt-dir=*)
			QTDIR=`expr "${1}" : "--with-qt-dir=\(.*\)"`
			shift
			;;

		--docdir=*)
			DOCDIR=`expr "${1}" : "--docdir=\(.*\)"`
			shift
			;;
			
		--debug)
			QT_CONFIG="debug"
			shift
			;;
		--help) show_usage; exit ;;
		*) show_usage; exit ;;
	esac
done

PREFIX=${PREFIX:-/usr/local}
DOCDIR=${DOCDIR:-$PREFIX/doc}
QT_CONFIG=${QT_CONFIG:-release}
APP_VERSION=`cat VERSION`
#export QTINC=/usr/include/qt4

echo "Configuring QtCommander ..."
printf "Verifying Qt 4.x Multithreaded (MT) build environment ... "

if [ -z "$QTDIR" ]; then
	echo \$QTDIR not set... trying to find Qt manually
	for p in /usr/lib/qt /usr/share/qt /usr/share/qt4 /usr/local/lib/qt /usr/local/share/qt /usr/lib/qt4 /usr/local/lib/qt4 /usr/X11R6/share/qt /usr/local/Trolltech/Qt-4.1.4; do
		if [ -d "$p/mkspecs" ]; then
			QTDIR=$p
			break;
		fi;
	done
	if [ -z "$QTDIR" ]; then
		echo fail
		echo
		echo Unable to find Qt 'mkspecs'. Please set QTDIR
		echo manually. Perhaps you need to install Qt 3
		echo development utilities. You may download them either
		echo from the vendor of your operating system or from
		echo www.trolltech.com
		echo
		exit 1;
	fi
fi

if [ ! -x "$QTDIR/bin/qmake-qt4" ]; then
	qm=`type -p qmake-qt4`
	if [ ! -x "$qm" ]; then
		echo fail
		echo
		echo Sorry, you seem to have a very unusual setup,
		echo or I missdetected \$QTDIR=$QTDIR
		echo
		echo Please set \$QTDIR manually and make sure that
		echo \$QTDIR/bin/qmake-qt4 exists.
		echo
		exit 1;
	fi
else
	qm=$QTDIR/bin/qmake-qt4
fi

echo OK

export QTDIR
cat > .qmake.cache <<EOF
PREFIX=$PREFIX
DOCDIR=$DOCDIR
APP_VERSION=$APP_VERSION
CONFIG=$QT_CONFIG
QT_BIN_DIR=$QTDIR/bin
EOF

CWD=`pwd`
#create Makefiles
for i in `find . -type f -name "*.pro"`; do
    DIR=`dirname $i`
    PRJ_FILE=`basename $i`
    cd $DIR
    echo "Create Makefile from $PRJ_FILE..."
    $qm $PRJ_FILE -o Makefile
    cd $CWD
done

echo
echo "Good, your configure finished."
echo "Base path for install:		$PREFIX"
echo "Documentation install path:	$DOCDIR"
echo "Now run 'make'"
echo
