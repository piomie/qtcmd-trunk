// $Id$
/*!
	\author Piotr Mierzwi�ski
*/
/*! \class FSManager
	\short Klasa bazowa systemu plik�w, po kt�rej dziedzicz� wszystkie systemy plik�w.
	\n \n
	Spe�nia ona wiele zada� po�rednich (g��wnie poprzez metody wirtualne) przy uruchamianiu operacji plikowych,
	jest najwy�sz� warstw� pomi�dzy UI a systemem plik�w. Wykonywane s� tutaj wst�pne czynno�ci przed operacjami
	plikowymi, np. zbieranie nazw plik�w na list�, tworzenie, pokazywanie i chowanie dialog�w oraz czynno�ci
	inicjalizacyjne dla widoku listy (konieczne do wykonanie niekt�rych operacji). Wykonywane jest tutaj te� wst�pne
	sprawdzanie b��d�w. Znajduje si� tutaj r�wnie� obs�uga dekodowania po tzw. szybkiej zmianie niekt�rych atrybut�w
	pliku, np. praw dost�pu, czy czasu modyfikacji, podawanej wprost przez u�ytkownika. Obiekt tej klasy przechowuje
	�cie�k� do aktualnie otwartego katalogu w danym systemie plik�w. Zawiera r�wnie� metody wykrywaj�ce rodzaj
	systemu plik�w na podstawie podanej �cie�ki.
*/


/*! \fn FSManager::FSManager( const QString & sInputURL, ListView * pListView )
	\brief Konstruktor klasy.
	\n \n
	Inicjowane s� tu sk�adowe klasy i wczytywane ustawienia systemu plik�w z pliku konfiguracyjnego.
	\param sInputURL - nazwa katalogu, kt�ry b�dzie otwarty w bie��cym systemie plik�w;
		tutaj jest wykorzystywany do wykrywania bie��cego systemu plik�w,
	\param pListView - wska�nik na widok listy plik�w.
*/

/*! \fn FSManager::~FSManager()
	\brief Destruktor.
	\n \n
	Usuwane s� tutaj nast�puj�ce obiekty: dialog post�pu operacji, dialog wyszukiwania plik�w
	i dialog w�a�ciwo�ci pliku.
*/

/*! \fn FSManager::createVFS( VFS::KindOfFilesSystem eKindOfFS, const QString & sInputURL )
	\brief Metoda powoduje zaladowanie plugina obs�uguj�cego podany system plik�w.
	\param eKindOfFS - rodzaj systemu plik�w do za�adowania,
	\param sInputURL - URL katalogu/archiwum do odczytania.
*/

/*! \fn FSManager::removeVFS()
	\brief Metoda powoduje usuni�cie obiektu bie��cego systemu plik�w.
	W tym celu wy�adowywany jest plugin obs�uguj�cy go.
*/

/*! \fn FSManager::nameOfProcessedFile() const
	\brief Zwraca nazw� (ze �cie�k� absolut�) aktualnie przetwarzanego pliku.
	\return nazwa aktualnie przetwarzanego pliku.
*/

/*! \fn FSManager::errorCode() const
	\brief Zwraca kod b��du dla ostatniej operacji plikowej typu Error.
	\return kod b��du.
*/

/*! \fn FSManager::host() const
	\brief Zwraca nazw� g��wnego katalogu.
	\return nazwa g��wnego katalogu.
*/

/*! \fn FSManager::path() const
	\brief Zwraca bie��c� �cie�k� absolutn�.
	\return bie��ca �cie�ka absolutna.
*/

/*! \fn FSManager::port() const
	\brief Zwraca numer portu na kt�rym obs�ugiwany jest systemu plik�w.
	\n \n
	Porzyteczna tylko w systemie FtpFS.
	\return numer portu.
*/

/*! \fn FSManager::currentDirWritable() const
	\brief Zwraca informacj� o tym czy bie��cy katalog ma prawa zapisu dla aktualnie zalogowanego u�ytkownika.
*/

/*! \fn FSManager::weighFiles( const QStringList & slFilesList, bool bRealWeigh )
	\brief Metoda uruchamia wa�enie podanych na li�cie plik�w.
*/

/*! \fn FSManager::getWeighingStat( long long & nWeigh, uint & nFiles, uint & nDirs )
	\brief Wywo�anie funkcji pobieraj�cej wyniki operacji wa�enia, tj. waga ca�o�ci, ilo�� plik�w i katalog�w.
*/

/*! \fn FSManager::getNewUrlInfo( UrlInfoExt & uNewUrlInfo )
	\brief Pobiera informacj� o pliku wcze�niej zainicjowan� w sk�adowej klasy zawieraj�cej informacje o pliku.
*/

/*! \fn FSManager::breakOperation()
	\brief Uruchamia przerwanie bie��cej operacji.
*/

/*! \fn FSManager::rereadCurrentDir()
	\brief Uruchamia ponowne odczytanie bie��cego katalogu.
	\n \n
	Wywo�ywana jest tu metoda z klasy widoku listy plik�w.
*/

/*! \fn FSManager::createNewEmptyFile( bool bCreateFile=TRUE )
	\brief Metoda uruchamia procedur� tworzenia pustego pliku lub katalogu.
	\n \n
	Pokazywany jest tutaj te� dialog wst�pny dla bie��cej operacji.
	\param bCreateFile - warto�� TRUE wymusza utworzenie pustego pliku,
		w przeciwnym razie tworzony jest katalog (warto�� domy�lna to TRUE).
*/

/*! \fn FSManager::openFile( const QString & sFileName, bool bEditMode=FALSE )
	\brief Inicjuje otwarcie zaznaczonych plik�w.
	\n \n
	Nazwy plik�w do otwarcia zbierane s� na list�, je�li dany plik nie mo�na odczyta�,
	wtedy pokazywany jest komunikat b��du. Zebrane pliki otwierane s� w wewn�trznej
	lub zewn�trznej przegl�darce, w zale�no�ci od ustawienia w bazie skoja�e� danego typu pliku.
	\param sFileName - nazwa pliku do otwarcia,
	\param bEditMode - warto�� TRUE wymusza otwarcie bie��cego pliku w trybie  edycji.
*/

/*! \fn FSManager::copyFilesTo( const QString & sTargetPath, ListView::SelectedItemsList *pItemsList, bool removeSrc=FALSE )
	\brief Metoda uruchamia procedur� kopiowania lub przenoszenia plik�w.
	\n \n
	Pokazywany jest r�wnie� wst�pny dialog dla tej operacji. Gdy je�li kopiowany, lub przenoszony
	jest jeden plik to nast�puje sprawdzenie jest czy ma tak� sam� nazw� jak docelowy, je�li tak
	to pokazywany jest komunikt ostrzegawczy i nast�puje powr�t do dialogu wst�pnego operacji.
	\param sTargetPath - �cie�ka docelowa kopiowania lub przenoszenia,
	\param pItemsList - wska�nik listy zaznaczonych plik�w,
	\param bRemoveSrc - r�wne TRUE, wymusza usuni�cie plik�w �r�d�owych po ich skopiowaniu
		(operacja przenoszenia), w przeciwnym razie pliki s� tylko kopiowane.
*/

/*! \fn FSManager::removeFiles()
	\brief Metoda uruchamia procedur� usuwania plik�w z aktualnego systemu plik�w.
	\n \n
	Zbierane s� tutaj na list� nazwy plik�w do usuni�cia, a przed uruchomieniem
	operacji pokazywany jest dialog z pytaniem o potwierdzenia usuwania.
*/

/*! \fn FSManager::makeLink( const QString & sTargetPath, bool bEditOnly )
	\brief Metoda uruchamia procedur� tworzenia lub edycji linku.
	\n \n
	Zbierane s� na list� nazwy plik�w, dla kt�rych nale�y wykona� linki.
	Pokazywany jest r�wnie� wst�pny dialog dla tej operacji.
	\param sTargetPath - katalog docelowy dla linku,
	\param bEditOnly - r�wne TRUE, oznacza �e b�dzie to operacja edycji linku,
		w przeciwnym wypadku wykonywane jest tworzenie.
*/

/*! \fn FSManager::changeAttribut( ListView::ColumnName eColumn, const QString & sNewAttribTxt )
	\brief Metoda uruchamia procedur� zmiany atrybutu (w podanej kolumnie) dla bie��cego pliku.
	\param eColumn - kolumna w kt�rej nale�y zmieni� atrybut pliku,
	\param newAttribStr - ci�g opisuj�cy now� warto�� atrybutu.
*/

/*! \fn FSManager::newAttributName() { return m_sTargetURL; }
	\brief Zwraca docelow� nazw� pliku, kt�remu zmieniono nazw�,
*/

/*! \fn FSManager::showPropertiesDialog()
	\brief Funkcja powoduje pokazanie dialogu w�a�ciwo�ci dla bie��cego pliku.
*/

/*! \fn FSManager::showFindFileDialog()
	\brief Funkcja powoduje pokazanie dialogu wyszukiwania pliku.
*/


/*! \fn FSManager::showProgressDialog( VFS::Operation eOperation, bool bMove=FALSE, const QString & sTargetPath=QString::null )
	\brief Funkcja zajmuje si� zainicjowaniem i pokazaniem dialogu post�pu dla podanej operacji.
	\param eOperation - aktualnie wykonywana operacja,
	\param bMove - je�li r�wne TRUE, a operacj� jest kopiowanie, wtedy ustawia,
	\param sTargetPath - �cie�ka docelowa dla bie��cej operacji; niezb�dne dla kopiowania lub przenoszenia.
*/

/*! \fn FSManager::removeProgressDlg( bool bCheckWhetherCanToClose=FALSE )
	\brief Funkcja powoduje usuni�cie dialogu post�pu.
	\param bCheckWhetherCanToClose - r�wne TRUE, wymusza sprawdzenie informacji o tym,
		czy u�ytkownik wybra� zamkni�cie na dialogu post�pu.
*/


/*! \fn FSManager::needToChangeFS( QString & sInputURL )
	\brief Na podstawie podanej nazwy wykrywa rodzaj docelowego systemu plik�w.
	\n \n
	Je�li jest on r�ny od bie��cego, wtedy zwraca URL, w przeciwnym razie jest to warto�� pusta.Dodatkowo,
	je�eli bie��cy i docelowy system plik�w jest taki sam, wtedy por�wnywana jest �cie�ka.
	Je�li jest r�na od bie��cej, wtedy nast�puje pr�ba otwarcia katalogu (lub archiwum).
	\param sInputURL - nazwa pliku lub katalogu.
*/

/*! \fn FSManager::absoluteFileName( const QString & sFileName ) const;
	\brief Zwraca nazw� (pliku lub katalogu) z absolutn� �cie�k� dost�pu bie��cego FS-u dla podanego pliku lub katalogu.
	\param sFileName - nazwa pliku lub katalogu,
	\return nazwa z absolutn� �cie�k� dla bie��cego systemu plik�w.
*/

/*! \fn FSManager::rootName() const;
	\brief Zwraca ci�g b�d�cy nazw� katalogu root aktualnego systemu plik�w.
	\return katalog korzenia dla bie��cego systemu plik�w.
	Przyk�adowo dla lokalnego systemu plik�w zwracane jest: "/", natomiast dla FTP - "ftp://", itp.
	Rodzaj systemu plik�w trzyma sk�adowa \em m_eCurrentFilesSystem.
*/

/*! \fn FSManager::setCurrentURL( const QString & sFullPathName ) { m_sCurrentURL = sFullPathName; }
	\brief Funkcja inicjuje sk�adow� \em m_sCurrentURL podan� warto�ci� parametru.
	\param sFullPathName - nazwa, kt�r� nale�y zainicjowa� bie��c� �cie�k�.
*/

/*! \fn FSManager::previousDirName() const { return m_sPreviousDirName; }
	\brief Zwraca nazw� poprzedniego katalogu widocznego na li�cie.
*/

/*! \fn FSManager::currentURL() const { return m_pVFS->path() }
	\brief Zwraca absolutn� �cie�k� do bie��cego katalogu.
*/

/*! \fn FSManager::targetURL() const { return m_sTargetURL; }
	\brief Zwraca absolutn� nazw� docelowego pliku w operacji zmiany nazwy.
*/

/*! \fn FSManager::absHostURL() const;
	\brief Zwraca absolutn� bie��c� �cie�k� dla aktualnego systemu plik�w.
	\return absolutna �cie�ka, przy czym dla archiwum zwracana jest tu tylko jego nazwa.
*/


/*! \fn FSManager::hasFocus() const { return m_pListView->hasFocus() }
	\brief Metoda zwraca informacj�, czy lista plik�w zawiera focus.
	\return TRUE je�li lista zawiera focus, FALSE je�li go nie zawiera.
*/

/*! \fn FSManager::kindOfFilesSystem( const QString & sFullFileName )
	\brief Funkcja zwraca rodzaj systemu plik�w, na podstawie podanej nazwy.
	\param sFullFileName - �cie�ka do katalogu,
	\return rodzaj systemu plik�w.
*/

/*! \fn FSManager::archiveIsSupported( const QString & sFileName )
	\brief Zwraca informacje czy archiwum w pliku o podanej nazwie jest obs�ugiwane.
	\n \n
	Nale�y tu zauwa�y�, �e aktualnie wspierane s� nast�puj�ce rodzaje archiw�w:
	'tar', 'tar.gz', 'tar.bz2'.
	\param sFileName - nazwa pliku archiwum.
	\return TRUE je�li archiwum jest obs�ugiwane, w przeciwnym razie FALSE.
*/

/*! \fn FSManager::kindOfCurrentFS() const { return m_eCurrentFilesSystem; }
	\brief Metoda zwraca rodzaj bie��cego systemu plik�w.
	\return rodzaj systemu plik�w, patrz: \link KindOfFilesSystem KindOfFilesSystem \endlink.
*/

/*! \fn FSManager::kindOfViewFilesPanel() const { return m_pListView->kindOfView() }
	\brief Metoda zwraca rodzaj widoku listy w panelu.
	\return rodzaj widoku listy, patrz: \link KindOfListView KindOfListView \endlink.
*/

/*! \fn FSManager::parsePath( QString & sPath, bool bAlwaysAddSlash=TRUE )
	\brief Metoda wykonuje ewentualn� korekcj� podanej �cie�ki.
	\n \n
	Zamieniane s� tutaj na w�a�ciw� �cie�k� ci�gi znak�w '../' i './' oraz znaki:
	'~' (katalog domowy u�ytkownika), '.' (katalog bie��cy). Ci�gi slashy ('//') zamieniane s�
	na pojedyncze znaki '/'. Je�li podana �cie�ka nie jest absolutna, wtedy jest uzupe�niana bie��c�.
	\param sPath - �cie�ka do poprawienia,
	\param bAlwaysAddSlash - r�wne TRUE, wymusza dodanie znaku slash ('/') na ko�cu �cie�ki.
*/

/*! \fn FSManager::cmdString( VFS::Operation eOperation ) // TYLKO DLA TESTOW
	\brief Funkcja zwraca nazw� podanego kodu operacji na plikach.
	\param eOperation - kod operacji.
	\return nazwa operacji.
*/

/*! \fn FSManager::setWeighInPropetiesDlg()
	\brief Metoda powoduje ustawienie wagi pliku(�w) na dialogu w�a�ciwo�ci.
	\n \n
	Waga jest pobierana z bie��cego systemu plik�w za pomoc� wirtualnej funkcji.
*/

/*! \fn FSManager::initPropetiesDlgByUrl( const UrlInfoExt & uUrlInfo, const QString & sLoggedUserName )
	\brief Metoda wykonuje inicjalizacj� na dialogu w�a�ciwo�ci pliku podan� informacj� o pliku.
	\param uUrlInfo - adres obiektu z informacj� o pliku,
	\param sLoggedUserName - nazwa aktualnie zalogowanego u�ytkownika.
*/

/*! \fn FSManager::createFileView()
	\brief Metoda tworzy obiekt podgl�du (typu FileView) oraz ��czy sygna�y ze slotami.
*/

/*! \fn FSManager::setFilesAssociation( FilesAssociation *pFilesAssociation ) { m_pFilesAssociation = pFilesAssociation; }
	\brief Ustawia wska�nik do obiektu skoja�e� plik�w.
	\param pFilesAssociation - wska�nik do obiektu skoja�e� plik�w.
*/

/*! \fn FSManager::setRemoveToTrash( bool )
	\brief W��cza b�d� wy��cza bezpieczne usuwnie plik�w.
*/

/*! \fn FSManager::setWeighBeforeOperation( bool )
	\brief W��cza albo wy��cza czynno�� wa�enia plik�w i katalog�w przed operacj� plikow� (np. kopiowania).
*/

/*! \fn FSManager::setTrashPath( const QString & )
	\brief W��cza b�d� wy��cza bezpieczne usuwnie plik�w.
*/

/*! \fn FSManager::setWorkDirectoryPath( const QString & )
	\brief Ustawia �cie�k� do katalogu roboczego.
	\n \n
	U�ywane tylko w systemie ArchiveFS.
*/

/*! \fn FSManager::setStandByConnection( bool )
	\brief W��cza albo wy��cza utrzymywanie po��czenia ze zdalnym hostem.
	\n \n
	U�ywane tylko w systemie FtpFS.
*/

/*! \fn FSManager::setNumOfTimeToRetryIfServerBusy( uint )
	\brief Ustawia ilo�� powtarzania wykonania operacji po��czanie, gdy zdalny host nie odpowiada.
	\n \n
	U�ywane tylko w systemach FtpFS i SambaFS.
*/

/*! \fn FSManager::setAlwaysOverwrite( bool bAlwaysOverwite )
	\brief Ustawia status nadpisywania plik�w w przypadku operacji kopiowania i przenoszenia.
	\n \n
	Ustawienie to jest u�ywane przez dialog kopiowania.
	\param bAlwaysOverwite - r�wne TRUE zaznacza na dialogu opcje nadpisywania,
		dla FALSE opcja ta nie jest zaznaczana.
*/

/*! \fn FSManager::setAlwaysSavePermission( bool bSavePerm )
	\brief Ustawia status zachowywania praw dost�pu w przypadku operacji kopiowania i przenoszenia.
	\n \n
	Ustawienie to jest u�ywane przez dialog kopiowania. Domy�lnie w�a�ciwo�� ta jest w��czona.
	\param bSavePerm - r�wne TRUE zaznacza na dialogu, opcje zachowania praw,
		dla FALSE opcja ta nie jest zaznaczana.
*/

/*! \fn FSManager::setAlwaysAskBeforeDeleting( bool bAskBeforeDelete )
	\brief W��cza lub wy��cza pokazywanie okienka ostrzegawczego przed operacj� usuwania plik�w.
	\n \n
	Domy�lnie w�a�ciwo�� ta jest w��czona.
	\param bAskBeforeDelete - r�wne TRUE, wymusza pokazanie dialogu, dla FALSE okienko nie zostanie pokazane.
*/

/*! \fn FSManager::setAlwaysGetInToDirectory( bool bGetInToDir )
	\brief W��cza lub wy��cza operacj� wej�cia do katalogu po jego utworzeniu.
	\n \n
	Domy�lnie w�a�ciwo�� ta jest wy��czona.
	\param bGetInToDir - r�wne TRUE, wymusza wej�cie do katalogu, dla FALSE ono nie nast�pi.
*/

/*! \fn FSManager::setCloseProgressDlgAfterFinished( bool bCloseAfterFinished )
	\brief W��cza/wy��cza chowanie dialogu post�pu po zako�czeniu operacji.
	\n \n
	Domy�lnie w�a�ciwo�� ta jest wy��czona.
	\param bCloseAfterFinished - r�wne TRUE, oznacza zamkni�cie dialogu, w przypadku FALSE nie jest on zamykany.
*/

/*! \fn FSManager::putFile( const QByteArray & baBuffer, const QString & sFileName )
	\brief Zapisuje zawarto�� podanego bufora do podanego pliku.
	\param buffer - adres bufora,
	\param sFileName - nazwa pliku.
*/


/*! \var QString FSManager::m_sTargetURL
	\brief Docelowy URL.
*/
/*! \var QString FSManager::m_sCurrentURL
	\brief Bie��cy URL.
*/
/*! \var QString FSManager::m_sNewURL
	\brief Nowy URL (u�ywane przy zmianie bie��cej �cie�ki).
*/
/*! \var QString FSManager::m_sPreviousDirName
	\brief Nazwa poprzedniego katalogu.
*/
/*! \var VFS::KindOfFilesSystem FSManager::m_eCurrentFilesSystem
	\brief Bie��cy system plik�w.
*/
/*! \var QStringList FSManager::m_slSelectedFilesList
	\brief Lista zaznaczonych plik�w dla bie��cej operacji.
*/
/*! \var bool FSManager::m_bSavePermision
	\brief Status zachowania praw dost�pu.
*/
/*! \var bool FSManager::m_bAlwaysOverwrite
	\brief Status nadpisywania.
*/
/*! \var bool FSManager::m_bHardLink
	\brief Status tworzenia tzw. twardych link�w.
*/
/*! \var bool FSManager::m_bAskBeforeDelete
	\brief Status upewniania si� przed operacj� usuwania (dla TRUE pokazywane jest okno dialogowe).
*/
/*! \var bool FSManager::m_bGetInToDir
	\brief Status wej�cia do nowo utworzonego katalogu.
*/
/*! \var int  FSManager::m_nItemIdForOpenedFile
	\brief Indeks pliku do otwarcia na li�cie nazw (u�ywane gdy zaznaczono wi�cej ni� jeden plik i wybrano otwarcie).
*/
/*! \var bool FSManager::m_bOpenedFileEditMode
	\brief Status otwieranego pliku w trybie edycji.
*/
/*! \var bool FSManager::m_bFoundFilesSys
	\brief Status panela dla znalezionych plik�w.
*/


/*! \fn FSManager::numOfAllFiles()
	\brief Zwraca liczb� wszystkich plik�w i katalog�w w bie��cym katalogu.
	\n \n
	Metoda u�ywana przy pokazywaniu paska post�pu podczas wstawiania na list�.
	\return suma plik�w i katalog�w z bie��cego.
*/

/*! \fn FSManager::decodePermissions( int & nPermissions, const QString & sOldPerm, const QString & sNewPerm )
	\brief Zamienia prawa dost�pu podane w postaci ci�gu 'rwx' lub warto�ci �semkowej, na warto�� typu 'int'.
	\param nPermissions - adres zmiennej, w kt�rej nale�y umie�ci� wynik,
	\param sOldPerm - pierwotny ci�g opisuj�cy prawa dost�pu,
	\param sNewPerm - nowy ci�g opisuj�cy prawa dost�pu.
*/

/*! \fn FSManager::decodeTime( QDateTime & dtTime, const QDateTime & dtOldTime, const QString & sNewTime )
	\brief Funkcja powoduje zdekodowanie ci�gu tekstowego 'data czas' podanego przez u�ytkownika podczas tzw. szybkiej zmiany atrybutu.
	\n \n
	Ci�g tekstowy podany jako \em timeStr powinien mie� nast�puj�cy format: yyyy-mm-dd hh-mm-ss,
	przy czym nie wszystkie pola musz� tu wystapi� - konieczne jest co najmniej jedno z daty lub czasu.
	\param dtTime - adres obiektu z informacj� o czasie i dacie, w kt�rym jest zapisywana
		zdekodowana nowa warto�� daty i czasu,
	\param dtOldTime - stara warto�� daty i czasu,
	\param sNewTime - nowa warto�� daty i czasu podana przez u�ytkownika.
*/

/*! \fn FSManager::runExternalViewer( const QStringList & slFilesToView )
	\brief Metoda powoduje uruchomienie zewn�trznych program�w dla podgl�du ka�dego z podanej listy plik�w.
	\n \n
	Nazwy program�w - przegl�darek pobierane s� z bazy skojarze� plik�w.
	\em slFilesToView - lista nazw plik�w do podgl�du.
*/

/*! \fn FSManager::slotCloseCurrentDir()
	\brief Slot powoduje zmian� bie��cego katalogu o jeden poziom wy�ej.
*/

/*! \fn FSManager::slotOpen( const UrlInfoExt & ui )
	\brief Slot uruchamia procedure otwarcia katalogu, pliku zwyk�ego, lub archiwum.
	\n \n
	Je�li nast�pi pr�ba wyj�cia z najni�szego katalogu w systemie plik�w innym ni� lokalny,
	wtedy wymuszane jest przej�cie do poprzedniego katalogu z lokalnego systemu plik�w.
	UWAGA:
	Jako nazw� do \param ui mo�na poda� skr�ty specjalne tj.: '..', '~' z pustymi innymi atrybutami.
	\param ui - rozszerzona informacja o pliku.
*/


/*! \fn FSManager::slotInitializeShowsList()
	\brief Slot powoduje przygotowanie obiektu klasy ListView w celu pokazania na nim nowej listy element�w, np. dla widoku zwyk�ej listy czy�ci widok.
*/

/*! \fn FSManager::slotSetCurrentURL( const QString & sNewPath )
	\brief W slocie ustawiana jest tylko sk�adowa zawieraj�ca bie��c� �cie�k� systemu plik�w.
	\n \n
	Jest on wykorzystywany w widoku drzewa przez obiekt klasy widoku
	listy (przy poruszaniu kursorem mo�e si� zmieni� bie��cy katalog).
	\param sNewPath - nowa �cie�ka, kt�r� nale�y ustawi� jako bie��c�.
*/

/*! \fn FSManager::slotGetUrlInfoFromList( const QString & sFileName, UrlInfoExt & uUrlInfo )
	\brief Slot powoduje pobranie informacji o pliku wprost z widoku listy.
	\param sFileName - nazwa pliku, kt�remu nale�y pobra� reszte informacji,
	\param uUrlInfo - adres obiektu, w kt�rym umieszczana jest pobrana informacja.
*/


/*! \fn FSManager::slotShowFileOverwriteDlg( const QString & sSourceFileName, QString & sTargetFileName, int & nResult )
	\brief Slot powoduje pokazanie dialogu z pytaniem o nadpisywanie docelowego pliku.
	\n \n
	Dost�pne s� nast�puj�ce odpowiedzi: 'Yes', 'No', 'If different size', 'Rename', 'All', 'Update all',
	'None', 'Cancel'. Przed pokazaniem dialogu wstrzymywany jest stoper na dialogu post�pu, a po wybraniu
	opcji stoper jest uruchamiany ponownie.
	\param sSourceFileName - nazwa �r�d�owego plik,
	\param sTargetFileName - adres obiektu zawieraj�cego nazw� docelowego pliku,
		przy czym slot mo�e tu wpisa� now� nazw� (je�li u�ytkownik wybierze jej zmian�),
	\param nResult - adres zmiennej, kt�ra b�dzie zawiera� kod wci�ni�tego przycisku.
*/

/*! \fn FSManager::slotShowDirNotEmptyDlg( const QString & sDirName, int & nResult )
	\brief Slot powoduje pokazanie dialogu z pytaniem czy podany katalog usun�� rekurencyjnie.
	\n \n
	Mo�liwymi odpowiedziami s�: 'Yes', 'No', 'All', 'None','Cancel'.
	Przed pokazaniem dialogu wstrzymywany jest stoper na dialogu post�pu,
	po wybraniu opcji stoper jest uruchamiany ponownie.
	\param sDirName - nazwa usuwanego katalogu,
	\param nResult - adres zmiennej, kt�ra b�dzie zawiera� kod wci�ni�tego przycisku.
*/


/*! \fn FSManager::slotUpdateFindStatus( uint nMatches, uint nFiles, uint nDirectories )
	\brief W tym slocie nast�puje uaktualnienie informacji o wyszukiwaniu pliku na dialogu wyszukiwania.
	\param nMatches - ilo�� pasuj�cych element�w,
	\param nFiles - ilo�� wszystkich przej�anych plik�w,
	\param nDirectories - ilo�� wszystkich przej�anych katalog�w.
*/

/*! \fn FSManager::slotStartFind( const FindCriterion & fcFindCriterion )
	\brief Slot uruchamia procedur� wyszukiwania pliku.
	\n \n
	Je�li kryterium jest puste, wtedy dialog wyszukiwania jest przestawiany w stan 'zako�czono wyszukiwanie'.
	\param fcFindCriterion - adres obiektu zawieraj�cego kryterium wyszukiwania.
*/

/*! \fn FSManager::slotInsertMatchedItem( const UrlInfoExt & uUrlInfo )
	\brief Slot powoduje wstawienie na list� dialogu wyszukiwania pliku lub wzorca, podanego elemenu.
	\param uUrlInfo - adres obiektu zawieraj�cego informacj� o pliku.
*/

/*! \fn FSManager::slotReadFileToView( const QString & sFileName, QByteArray & baBuffer, int & nReadBlockSize, int nFileLoadMethod )
	\brief Slot powoduje wczytywanie wybranego pliku do podgl�du.
*/

/*! \fn FSManager::slotStartProgressDlgTimer( bool bStart )
	\brief Slot powoduje wystartowanie lub zatrzymanie stopera na dialogu post�pu.
	\param bStart - r�wne TRUE nakazuje uruchomienie stopera, FALSE powoduje jego zatrzymanie.
*/

/*! \fn FSManager::slotChangePropertiesOfFile( const UrlInfoExt & uNewUrlInfo, bool bAllSelected )
	\brief Slot urchamia procedur� zmiany atrubut�w dla zaznaczonych plik�w.
	\n \n
	Slot jest wywo�ywany (przy pomocy sygna�u) przez dialog w�a�ciwo�ci pliku.
	Patrz te�: \link setAttributs() setAttributs \endlink.
	\param uNewUrlInfo - adres obiektu zawieraj�cego now� informacj� o pliku,
	\param bAllSelected - r�wne TRUE, wymsza zmian� atrybut�w dla wszystkich zaznaczonych plik�w,
		w przeciwnym razie zmiana jest wykonywana tylko dla	bie��cego.
*/

/*! \fn FSManager::slotCloseAfterFinished( bool bCloseState )
	\brief Slot wywo�ywany w momencie zaznaczenia checkBox-u opisanego jako "Close after finished", na dialogu post�pu danej operacji.
	\param bCloseState - r�wne TRUE oznacza zamkni�cie dialogu post�pu.
*/

/*! \fn FSManager::slotCountingDirectory( int nItemNum )
	\brief Slot uruchamia procedur� wa�enia katalogu.
	\param nItemNum - indeks elementu na li�cie nazw plik�w.
	\n \n
	Slot jest wywo�ywany (przy pomocy sygna�u) przez dialog w�a�ciwo�ci pliku.
	Wa�enie plik�w jest uruchamiane w tzw. trybie 'RealWeigh', po wi�cej informacji patrz:
	\link sizeFiles() sizeFiles \endlink.
*/

/*! \fn FSManager::slotOperationInBackground()
	\brief Slot uruchamia operacj� w tle (NIE ZAIMPLEMENTOWANY !).
*/

/*! \fn FSManager::slotClosePropertiesDlg()
	\brief Slot powoduje usuni�cie dialogu w�a�ciwo�ci pliku oraz przerwanie bie��cej operacji.
*/

/*! \fn FSManager::slotCancelOperation()
	\brief Slot powoduje pokazanie dialog z pytaniem o potwierdzenie przerwania bie��cej operacji.
	\n \n
	Po potwierdzeniu przerwania wywo�ywana jest funkcja zatrzymuj�ca operacj�.
	Slot jest wywo�ywany w momencie przerwania jakiejkolwiek operacji.
*/


/*! \fn FSManager::slotCloseFindFileDlg()
	\brief Slot powoduje usuni�cie dialogu wyszukiwania pliku oraz przerwanie bie��cej operacji.
*/

/*! \fn FSManager::slotDeleteFile( const QString & sFileName )
	\brief W slocie uruchamiana jest procedura usuni�cia podanego pliku.
	\n \n
	Slot u�ywany przez obiekt klasy FindFileDlg.
	\param sFileName - nazwa pliku, kt�ry nale�y usun��.
*/

/*! \fn FSManager::slotJumpToTheFile( const QString & sFileName )
	\brief Slot powoduje ewentualn� zmian� bie��cego katalogu oraz skok kursora na li�cie do podanego pliku.
	\n \n
	Jest on u�ywany przez obiekt klasy FindFileDlg.
	\param sFileName - nazwa pliku, kt�ry nale�y otworzy�.
*/

/*! \fn FSManager::slotViewFile( const QString & sFileName, bool bEditMode )
	\brief Slot powoduje otwarcie podanego pliku w podanym trybie.
	\n \n
	Slot u�ywany jest przez obiekt klasy FindFileDlg.
	\param sFileName - nazwa pliku do otwarcia,
	\param bEditMode - tryb otwarcia (TRUE - edycja, FALSE - podgl�d).
*/

/*! \fn FSManager::signalSetInfo( const QString & sInfo )
	\brief Sygna� powoduje pokazanie na statusie panela podanej informacji.
	\n \n
	U�ywany jest do pokazywania zmiany statusu w bie��cej operacji, np. po��czenia ze zdalnym hostem.
	\param sInfo - tekst do wy�wietlenia.
*/

/*! \fn FSManager::signalResultOperation( VFS::Operation eOperation, bool bNoError )
	\brief Sygna� oznacza zako�czenie operacji.
	\n \n
	Jest wysy�any po zako�czenia operacji plikowej.
	\param eOperation - zako�czona operacja,
	\param bNoError - r�wne TRUE oznacza, �e operacja zako�czy�a si� bezb��dnie, FALSE m�wi o tym, �e wyst�pi� b��d.
*/

/*! \fn FSManager::signalDataTransferProgress( long long nDone, long long nTotal, uint nBytesTransfered )
	\brief Sygna� u�ywany do pokazywania post�pu dla operacji na pliku, np. kopiowania.
	\param nDone - ca�kowita liczba przetransportowanych bajt�w z danego pliku,
	\param nTotal - liczba wszystkich bajt�w danego pliku,
	\param nBytesTransfered - mie�ci liczb� bajt�w aktualnie przetransportowanych.
*/

/*! \fn FSManager::signalFileCounterProgress( int nValue, bool bFileCounting, bool bSetTotalValue )
	\brief Sygna� u�ywany do zliczania plik�w podczas wykonywania bie��cej operacji.
	\param nValue - liczba plik�w,
	\param bFileCounting - warto�� TRUE oznacza, �e zliczane s� pliki, w przeciwnym razie s� to katalogi,
	\param bSetTotalValue - waro�� TRUE oznacza, �e zliczana jest ilo�� wszystkich
		(u�ywany przy operacji wa�enia), dla FALSE - ilo�� przetworzonych.
*/

/*! \fn FSManager::signalNameOfProcessedFile( const QString & sFileName )
	\brief Sygna� u�ywany do wysy�ania nazwy aktualnie przetwarzanego pliku.
	\param sFileName - nazwa przetwarzanego pliku.
*/

/*! \fn FSManager::signalSetOperation( VFS::Operation op )
	\brief Sygna� u�ywany do ustawiania nazwy operacji na dialogu post�pu.
	\param op - kod operacji, patrz te� \link Operation Operation \endlink.
*/

/*! \fn FSManager::signalTotalProgress( int nDone, long long nTotalWeight ) // done in percent
	\brief Sygna� wysy�a warto�� procentow� informuj�c� o post�pie ca�ej operacji.
	\param nDone - warto�� w procentach oznaczaj�ca ile dot�d zrobiono,
	\param nTotalWeight - waga wszystkich przetwarznych plik�w.
*/

/*! \fn FSManager::signalOpenArchive( const QString & sFileName )
	\brief Sygna� nakazuje otwiercie podanego archiwum.
	\param sFileName - nazwa archiwum.
*/

/*! \fn FSManager::signalPutFile( const QByteArray & baBuffer, const QString & sFileName )
	\brief Sygna� przekazuje adres bufora i nazw� pliku do zapisu.
	\n \n
	U�ywany jest podczas operacji kopiowania lub przenoszenia.
	\param baBuffer - adres bufora,
	\param sFileName - nazwa pliku.
*/

