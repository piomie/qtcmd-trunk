// $Id$
/*!
	\author Piotr Mierzwi�ski
*/
/*! \class DevicesMenu
	\brief Klasa obs�uguje menu z list� urz�dze� przechowuj�cych dane.
	\n \n
	W klasie inicjowane jest menu z list� urz�dze� oraz stan ich zamontowania. Wykorzystywane s� do tego
	dwa pliki: /etc/fstab, /etc/mtab (stan zamontowania). Po wybraniu urz�dzenia przez u�ytkownika uruchamiana
	jest odpowiednia akcja. Mianowicie: je�li urz�dzenie jest zamontowane, wtedy nast�puje jego odmontowanie,
	je�eli nie jest zamontowane, wtedy podejmowana jest pr�ba zamontowania i trzecim przypadku, kiedy urz�dzenie
	jest zamontowane a u�ytkownik je wybierze, wtedy wysy�any jest sygna� \em signalPathChanged(), kt�ry wymusza
	zmian� bie��cej �cie�ki.
*/


/*! \fn DevicesMenu::DevicesMenu( QWidget *pParent=0, const char *sz_pName=0 )
	\brief Konstruktor klasy.
	\n \n
	Tworzone s� tutaj obiekty u�ywane w klasie.
	\param pParent - wska�nik na rodzica,
	\param sz_pName - nazwa dla obiektu.
*/

/*! \fn DevicesMenu::~DevicesMenu()
	\brief Destruktor.
	 Usuwane s� tu obiekty u�ywane w klasie.
*/

/*! \enum DevicesMenu::Operation
	\brief Rodzaj operacji jaki mo�na wykona� na bie��cym elemencie menu.
	\n \n
	Dost�pne s� nast�puje operacje: otwarcie katalogu (OPENDIR), zamontowanie (MOUNT) i odmontowanie (UNMOUNT).
*/

/*! \fn DevicesMenu::int exec( const QPoint & pos, int nIndexAtPoint=0 )
	\brief Powoduje pokazanie menu z list� dost�pnych urz�dze�.
	\n \n
	Pokazywane jest ono na pozycji (3,3) wzgl�dem obiektu rodzica. Po wybraniu opcji uruchamiana jest odpowiednia
	akcja. Je�li urz�dzenie jest zamontowane, wtedy nast�puje jego odmontowanie, je�eli nie jest zamontowane,
	wtedy podejmowana jest pr�ba zamontowania i trzecim przypadku kiedy urz�dzenie jest zamontowane a u�ytkownik
	je wybierze wtedy wysy�any jest tylko sygna� \em signalPathChanged(), kt�ry wymusza zmian� bie��cej �cie�ki.
	\param pos - pozycja dla menu,
	\param nIndexAtPoint -
*/

/*! \fn DevicesMenu::init()
	\brief Inicjowanie menu.
	\n \n
	Wczytywane s� tutaj dwa pliki: /etc/fstab, /etc/mtab. Nast�pnie inicjowane jest menu list� dost�pnych urz�dze�,
	a kolor ikony wskazuje stan ich zamotowania. Zwraca status operacji wczytania plik�w.
	 \return TRUE je�li operacja zako�czona powodzeniem, w przeciwnym wypadku FALSE.
*/

/*! \fn DevicesMenu::showInfoDlg( const QString & sCaption, const QString & sMsg )
	\brief Metoda powoduje pokazanie dialogu informacyjnego nt. aktualnego dzia�ania.
	\param sCaption - tytu� okienka,
	\param sMsg - wiadomo�� do pokazania.
*/

/*! \fn DevicesMenu::runAction()
	\brief Uruchamia (asynchronicznie) akcj� dla wybranego w menu polecenia.
	\n \n
	 Po uruchomieniu akcji pokazywy jest dialog informacyjny zawieraj�cy przycisk umo�liwiaj�cy przerwanie operacji.
*/

/*! \fn DevicesMenu::runProcess( Operation eOperation, const QString & sArguments )
	\brief Powoduje uruchamienie podanego polecenia z podanymi argumentami.
	\param eOperation - operacja do wykonania, patrz: \link DevicesMenu::Operation Operation \endlink,
	\param sArguments - argumenty dla polecenia.
*/

/*! \fn DevicesMenu::slotProcessExited()
	\brief Zawiera obs�ug� zako�czenia wykonywania polecenia.
	 Je�li podczas operacji wyst�pi� b��d, wtedy tutaj pokazywany jest stosowny komunikat.
*/

/*! \fn DevicesMenu::slotBreakProcess()
	\brief Powoduje przerwanie bie��cej operacji.
*/

/*! \fn DevicesMenu::slotUnmount()
	\brief Uruchamia proces odmontowywania bie��cego urz�dzenia.
*/

/*! \fn DevicesMenu::slotMount()
	\brief Uruchamia proces zamontowania bie��cego urz�dzenia.
*/

/*! \fn DevicesMenu::signalPathChanged( const QString & sPath )
	\brief Sygna� wymuszaj�cy zmian� �cie�ki w bie��cym panelu na podan�.
	\n \n
	\param sPath - nowa �cie�ka do katalogu.
*/


/*! \var QValueList DevicesMenu::m_vlMountedTab
	\brief Lista trzymaj�ca status zamontowania kolejnych urz�dze� z listy.
*/
/*! \var Operation DevicesMenu::m_eCurrentOperation
	\brief Zawiera kod wybranej z menu operacji.
*/
/*! \var QString DevicesMenu::m_sMountPath
	\brief Zawiera �cie�k� katalogu, w kt�rym zamontowane zostanie wybrane urz�dzenie.
*/
/*! \var QProcess DevicesMenu::m_pProcess
	\brief Wska�nik obiektu dla uruchamianego procesu (systemowe polecenie montowani lub odmontowania).
*/
/*! \var QDialog DevicesMenu::m_pInfoDlg
	\brief Wska�nik okna pokazuj�cego komunikaty nt wykonywanej operacji (informacja o stanie operacji lub b��dzie).
*/
/*! \var bool DevicesMenu::m_bActionIsFinished
	\brief
*/
