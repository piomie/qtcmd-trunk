# Plik utworzony przez menad?era projekt�w kdevelopa
# ------------------------------------------- 
# Podkatalog wzgl?dem g?�wnego katalogu projektu: ./doc
# Cel to projekt z podkatalogami 

CONFIG += warn_on
TEMPLATE = subdirs 
COPY = cp -rf
DEL_DIR = rm -rf
MKDIR = mkdir -p

docs_install.target = install_subdirs

docs_install.files = \
../AUTHORS \
../COPYING \
../BUG_LIST \
../ChangeLog \
../HACKING_pl.txt \
../IDEAs \
../PLANS \
../README \
../TODO \
../WISH_LIST \
pl \
qtcmdrc-example_pl.txt \
supported-keys_pl.html

docs_install.commands = @$$MKDIR $(INSTALL_ROOT)$${DOCDIR}/qtcmd-$${APP_VERSION} ; \
for i in $$docs_install.files ; do \
$$COPY \$$i $(INSTALL_ROOT)$${DOCDIR}/qtcmd-$${APP_VERSION} ; done \
#and temporarily
; find $(INSTALL_ROOT)$${DOCDIR}/qtcmd-$${APP_VERSION} -type d -name ".svn" -exec $$DEL_DIR {} \; 2>/dev/null || true

docs_uninstall.target = uninstall_subdirs
docs_uninstall.commands = @$$DEL_DIR $(INSTALL_ROOT)$${DOCDIR}/qtcmd-$${APP_VERSION}

QMAKE_EXTRA_UNIX_TARGETS += docs_install docs_uninstall
