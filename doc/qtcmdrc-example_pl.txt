[General]
Width=800                 # maksymalna wartosc to szerokosc pulpitu
Height=600                # maksymalna wartosc to wysokosc pulpitu
ShowToolBar=true          # dostepne wartosci: TRUE/FALSE
ShowMenuBar=true          # dostepne wartosci: TRUE/FALSE
SaveSettingsWhenQuit=true # dostepne wartosci: TRUE/FALSE
ShowButtonBar=true        # dostepne wartosci: TRUE/FALSE
FlatButtonBar=false       # dostepne wartosci: TRUE/FALSE
ShowQuitWarning=true      # dostepne wartosci: TRUE/FALSE

[Favorites]
Url0=home directory\\/home/user/
Url1=documents\\/home/user/Documents/

[FTP]
S01=session1\\ftp://user:password@localhost.com:21/
S02=session 2\\ftp://anonymous:anonymous@trolltech.com:21/
UsersName=anonymous^euser^eotheruser
StandByConnection=true    # czy podtrzymywac polaczenie po minieciu czasu bezczynnosci na serwerze; dostepne wartosci: TRUE/FALSE
NumOfTimeToRetryIfBusy=10 # ilosc powtorzen jesli serwer jest zajety;  dostepne wartosci: 0 - 999

[LeftPanel]
Width=400                 # jesli suma wart. z lewego i prawego panelu jest wieksza niz glowna szer.to ust.taka
KindOfPanel=0             # dostepne wart.: 0 - FilesPANEL, 1 - FileView

[FilesPanel]
Columns=0-170^e1-70^e2-114^e3-74^e4-48^e5-46^e  # 0 - numer kolumny, -170 rozmiar kolumny
ColorsScheme=DefaultColors
ColorsSchemeList=DefaultColors^eMyColorsScheme^e
DefaultFilesFilter=0      # Id filtru dla listy plikow, wartosci 0...4 to filtry standardowe, od 5 to filtry uzytkownika
FileSizeFormat=3          # 1 - bez spacji, 2 - ze spacjami, 3 - z przyrostkiem wagowym
FilterForFiltered=false   # filtruj juz przefiltrowane; dostepne wartosci: (TRUE/FALSE)
Font=Helvetica,10
NotHiddenCursor=true      # przy przewijaniu zawartosci panelu kursor nie chowa sie pod (gorna i dolna) krawedzia; dostepne wartosci: (TRUE/FALSE)
SelectFilesOnly=false     # przy operacji "zaznacz wszystkie" zaznacza tylko pliki (TRUE) lub pliki i katalogi (FALSE)
SynchronizeColumns=true   # czy kolumny maja przyjmowac ten sam rozmiar (w obu panelach) podczas zmiany ich rozmiaru; dostepne wartosci: 1/0 (TRUE/FALSE)
ShowIcons=true            # pokazuj przed nazwa elementu ikone; dostepne wartosci: (TRUE/FALSE)
ShowHidden=true           # pokazuj ukryte pliki i katalogi
TemplateSelection=*^e*.cpp^e*.h
UserPopUpMenu=101,102,103,104,105,106,108,109,1,107 # popUpMenu zdefiniowane przez uzytkownika
UserFilters=*.ui^e*.ui.h^e # lista filtrow uzytkownika

[FilesPanelShortcuts]
Copy files=F5 # skrot klawiszowy dla akcji 'Copy files'

[FileSystem]
AlwaysOverwrite=false     # czy zawsze nadpisywac pliki na serwerze FTP; dostepne wartosci: TRUE/FALSE
AlwaysSavePermission=true # czy zawsze zachowywac atrybuty pliku (data modyfikacji i prawa dostepu); dostepne wartosci: TRUE/FALSE
AskBeforeDelete=true      # czy pytac przed usunieciem plikow; dostepne wartosci: TRUE/FALSE
AlwaysGetInToDir=false    # czy po utworzeniu katalogu wejsc do jego wnetrza; dostepne wartosci: TRUE/FALSE
CloseProgressDlgAfterFinished=false # czy chowac dialog postepu po zakonczeniu operacji; dostepne wartosci: TRUE/FALSE
CopyMoveURLs=/^e/home/^   # lista miejsc docelowych dla operacji kopiowania i przenoszenia
CopyMoveURLsId=1          # id (liczone od 0) biezacej sciezki z listy miejsc docelowych kopiowania i przenoszenia
FindFileNames=IBM*^e*.html^eplik.txt^e # lista szukanych wczesniej wzorcow nazw plikow
FindFileNamesId=1         # id (liczone od 0) biezacego szukanego wzorca (nazwy pliku)
FindLocations=/^e/home^e  # lista lokalizaji, w ktorych byl szukany plik lub katalog
FindLocationsId=1         # id (liczone od 0) biezacej lokalizacji dla szukanego wzorca (nazwy pliku)
MakeLinkURLs=/home/file^e # lista utworzonych linkow
MakeLinkURLsId=0          # id (liczone od 0) biezacego linka z listy utworzonych
MakeDirNames=newDir^ed1^e # lista utworzonych katalogow
MakeDirNamesId=0          # id (liczone od 0) biezacego katalogu z listy utworzonych
MakeFileNames=newF^eff^e  # lista utworzonych pustych plikow
MakeFileNamesId=0         # id (liczone od 0) biezacego pustego pliku z listy utworzonych

[MyColorsScheme]
ShowSecondBgColor=true    # czy pokazywac dwukolorowe tlo listy; dostepne wartosci: TRUE/FALSE
ShowGrid=false            # czy pokazywac siatke; dostepne wartosci: TRUE/FALSE
TwoColorsCursor=true      # TRUE - kursor dwu kolorowy (ramka); FALSE kursor jedno kolorowy (pe�ny kursor)
ItemColor=000000
ItemColorUnderCursor=00000
SelectedItemColor=FF0000
SelectedItemColorUnderCursor=0000EA
BgColorOfSelectedItems=FFFFFF
BgColorOfSelectedItemsUnderCursor=FFFFFF
FirstBackgroundColor=FFFFFF  # kolor tla listy; dostepne wart.: szesnastkowa liczba formatu RRGGBB
SecondBackgroundColor=FF0423 # drugi kolor tla; dostepne wart.: szesnastkowa liczba formatu RRGGBB
FullCursorColor=0000EA
InsideFrameCursorColor=000080
OutsideFrameCursorColor=FF0000
GridColor=A9A9A9

[LFS]
AlwaysRemoveToTrash=false # czy zawsze usuwac (FALSE) czy przenosic do katalogu kosza (TRUE); dostepne wartosci: TRUE/FALSE
AlwaysWeighBeforeOperation=true # czy zawsze wazyc katalog przed operacja na nim; dostepne wartosci: TRUE/FALSE
TrashDirectory=/home/user/tmp/qtcmd/trash # sciezka do katalogu, gdzie przenoszone beda 'usuwane' pliki (jesli wlaczono usuwanie do kosza)

[Archives]
WorkingDirectory=/home/user/tmp/qtcmd/ # sciezka do katalogu roboczego (tu beda rozpakowywane przetwarzane archiwa oraz podgladane pliki z nich)
RarSolidArchive=true      # RAR, czy tworzyc archiwum typu 'Solid'; dostepne wartosci: TRUE/FALSE
RarSfxArchive=false       # RAR, czy tworzyc samorozpakowywalne archiwum; dostepne wartosci: TRUE/FALSE
RarSaveLinks=true         # RAR, czy zapisywac linki zamiast plikow wskazywanych przez nie; dostepne wartosci: TRUE/FALSE
RarSaveOwnerAndGroup=true # RAR, czy zapisywac dla kazdego pliku info o wlascicielu i grupie; dostepne wartosci: TRUE/FALSE
RarMultimediaCompression=false # RAR, czy uzyc kompresji dla multimediow
RarOvrExistingFiles=true  # RAR, czy zawsze nadpisywac istniejace pliki
ZipSfxArchive=false       # ZIP, czy tworzyc samorozpakowywalne archiwum; dostepne wartosci: TRUE/FALSE
ZipSaveSymbolicLinks=true # ZIP, czy zapisywac linki zamiast plikow wskazywanych przez nie; dostepne wartosci: TRUE/FALSE
ZipDoNotSaveExtraAttrib=false # ZIP, czy nie zapisywac informacji o wlascicielu i grupie oraz jego czasow; dostepne wartosci: TRUE/FALSE
Bzip2FullName=/usr/bin/bzip2   # polozenie kompresora 'bzip2'
GzipFullName=/bin/gzip         # polozenie kompresora 'gzip'
RarFullName=/usr/local/bin/rar # polozenie archiwizera 'rar'
TarFullName=/bin/tar           # polozenie archiwizera 'tar'
ZipFullName=/usr/bin/zip       # polozenie archiwizera 'zip'

[LeftFilesPanel]
KindOfView=0              # dostepne wartosci: 0 - FilesListVIEW, 1 - TreeVIEW
SortColumn=0              # sortuj po pierwszej kolumnie (po nazwie); dostepne wart.: 0,1,2,3,4,5
SortAscending=1           # dostepne wartosci: (TRUE/FALSE)
PathViewURLs=/home/^e/home/archive.rar^e/home/user/archive.tar.bz2#/subdir_arch^eftp://trolltech.com/source/^esmb://komputery/1/
PathViewURLsId=0          # biezacy URL po uruchomieniu, dostepne wartosci: 0 >= PathViewURLsId =< liczba_URLi


[RightPanel]
Width=400                 # szerokosc prawego panela
KindOfPanel=1             # dostepne wart.: 0 - FilesPANEL, 1 - FileView

[RightFileView]
URL=/home/user/Documents/SomeFileName.html


[FileView]
Size=600,400              # dziala tylko dla widoku w oknie (maksymalna wartosc to szerokosc pulpitu)
FullScreen=0              # dostepne wartosci: (TRUE/FALSE), dziala tylko dla widoku w oknie
ShowToolBar=1             # dostepne wartosci: (TRUE/FALSE), uzywane tylko dla widoku w oknie
NumOfCurrentURL=1         # biezacy URL po uruchomieniu, dostepne wartosci: 1 >= NumOfCurrentURL =< liczba_URLi
                          # pliki ladowane na 'liste plikow' widoczna z lewej strony okna podgladu
URLs=/home/plik.txt^e/home/strona.html^eftp://trolltech.com/source/ChangeLog^esmb://komputery/1/info.txt

[FileViewShortcuts]
Open=Ctrl+O # skrot klawiszowy dla akcji 'Open'

[TextFileView]
Font=Helvetica,10
TabWidth=2                # zakres dostepnych wartosci: 1...8
UseExternalEditor=0       # dostepne wartosci: (TRUE/FALSE)
ExternalEditor=/bin/vim

[TextFileViewShortcuts]
Find=Ctrl+F # skrot klawiszowy dla akcji 'Find'


[SyntaxHighlighting]
CC++/Normal=000000,,
CC++/String=FF0000,,
CC++/Decimal_Value=0000FF,,
CC++/Extentions=0095FF,Bold,
CC++/Comment=808080,,Italic
CC++/Key word=000000,Bold,
CC++/Data type=800000,,
CC++/Floating point=800080,,
CC++/Character=008080,,
CC++/Base N Integer=FF00FF,,
CC++/Preprocesor=00FF00,,
HTML/Normal=000000,,
HTML/String=FF0000,,
HTML/Decimal_Value;=0000FF,,
HTML/Comment=808080,,Italic
HTML/Key word=000000,Bold,
HTML/Data type=800000,,

[ImageFileView]

[SoundFileView]

[VideoFileView]

[BinaryFileView]

