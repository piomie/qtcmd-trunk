<!DOCTYPE TS><TS>
<context>
    <name>ListViewFindItem</name>
    <message>
        <source>Search</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LocationChooser</name>
    <message>
        <source>&amp;Current directory</source>
        <translation>&amp;Bieżący katalog</translation>
    </message>
    <message>
        <source>&amp;Original directory</source>
        <translation>&amp;Pierwotny katalog</translation>
    </message>
    <message>
        <source>&amp;Home directory</source>
        <translation>Katalog &amp;domowy</translation>
    </message>
    <message>
        <source>&amp;Root directory</source>
        <translation>Katalog &amp;korzenia</translation>
    </message>
    <message>
        <source>&amp;Previous directory</source>
        <translation>&amp;Poprzeni katalog</translation>
    </message>
    <message>
        <source>&amp;Next directory</source>
        <translation>&amp;Następny katalog</translation>
    </message>
    <message>
        <source>Clear history pathes</source>
        <translation>Czyść historię ścieżek</translation>
    </message>
    <message>
        <source>History contains</source>
        <translation>Historia zawiera</translation>
    </message>
    <message>
        <source>items</source>
        <translation>elementów</translation>
    </message>
    <message>
        <source>&amp;Yes</source>
        <translation>&amp;Tak</translation>
    </message>
    <message>
        <source>&amp;No</source>
        <translation>&amp;Nie</translation>
    </message>
    <message>
        <source>Select target directory</source>
        <translation>Wybór katalogu docelowego</translation>
    </message>
    <message>
        <source>All files</source>
        <translation>Wszystkie pliki</translation>
    </message>
    <message>
        <source>Select target file</source>
        <translation>Wybór pliku docelowego</translation>
    </message>
    <message>
        <source>&amp;Up directory</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Select directory</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Cl&amp;ear current</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Clear &amp;all</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Do you really want to clear all</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>StatusBar</name>
    <message>
        <source>Selected</source>
        <translation>Zaznaczono</translation>
    </message>
    <message>
        <source>files, from</source>
        <translation>plików, z</translation>
    </message>
    <message>
        <source>about total size</source>
        <translation>o całkowitym rozmiarze</translation>
    </message>
    <message>
        <source>bytes</source>
        <translation>bajtów</translation>
    </message>
    <message>
        <source>size</source>
        <translation>rozmiar</translation>
    </message>
</context>
</TS>
