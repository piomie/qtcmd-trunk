<!DOCTYPE TS><TS>
<context>
    <name>DevicesMenu</name>
    <message>
        <source>Error</source>
        <translation>Błąd</translation>
    </message>
    <message>
        <source>Cannot open file</source>
        <translation>Nie można otworzyć pliku</translation>
    </message>
    <message>
        <source>Open</source>
        <translation>Otwórz</translation>
    </message>
    <message>
        <source>Unmount</source>
        <translation>Odmontuj</translation>
    </message>
    <message>
        <source>Mounting</source>
        <translation>Montowanie</translation>
    </message>
    <message>
        <source>Unmounting</source>
        <translation>Odmontowywanie</translation>
    </message>
    <message>
        <source>Please wait.</source>
        <translation>Proszę czekać.</translation>
    </message>
    <message>
        <source>still is process</source>
        <translation type="obsolete">wciąż trwa</translation>
    </message>
    <message>
        <source>&amp;Break</source>
        <translation>&amp;Przerwij</translation>
    </message>
    <message>
        <source>No medium found.</source>
        <translation>Nie znaleziono nośnika.</translation>
    </message>
    <message>
        <source>Device is busy.</source>
        <translation>Urządzenie jest zajęte.</translation>
    </message>
    <message>
        <source>Error no.</source>
        <translation>Numer błędu.</translation>
    </message>
    <message>
        <source>device</source>
        <translation>urządzenia</translation>
    </message>
    <message>
        <source>mount</source>
        <translation>zamontować</translation>
    </message>
    <message>
        <source>umount</source>
        <translation>odmontować</translation>
    </message>
    <message>
        <source>Cannot check an information about devices is mounted</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Cannot not start</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Cannot not</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>still is sProcess</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FSManager</name>
    <message>
        <source>File system error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Plugin&apos;s path</source>
        <translation type="obsolete">Ścieżka do plugina</translation>
    </message>
    <message>
        <source>Plugin</source>
        <translation type="unfinished">Plugin</translation>
    </message>
    <message>
        <source>not exists in foregoing location !</source>
        <translation type="unfinished">nie istnieje w wyzaczonym miejscu !</translation>
    </message>
    <message>
        <source>directory</source>
        <translation type="unfinished">katalog</translation>
    </message>
    <message>
        <source>file</source>
        <translation type="unfinished">plik</translation>
    </message>
    <message>
        <source>Please to give a</source>
        <translation type="unfinished">Proszę podać</translation>
    </message>
    <message>
        <source>name:</source>
        <translation type="unfinished">nazwa:</translation>
    </message>
    <message>
        <source>new</source>
        <translation type="unfinished">nowy</translation>
    </message>
    <message>
        <source>Error</source>
        <translation type="unfinished">Błąd</translation>
    </message>
    <message>
        <source>This</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>already exists</source>
        <translation type="unfinished">już istnieje</translation>
    </message>
    <message>
        <source>Open it</source>
        <translation type="unfinished">Otwórz go</translation>
    </message>
    <message>
        <source>files selected</source>
        <translation type="unfinished">plików zaznaczonych</translation>
    </message>
    <message>
        <source>this files</source>
        <translation type="obsolete">te pliki</translation>
    </message>
    <message>
        <source>Remove files</source>
        <translation type="obsolete">Usuń pliki</translation>
    </message>
    <message>
        <source>Do you really want to delete</source>
        <translation type="unfinished">Czy na prawdę chcesz skasować</translation>
    </message>
    <message>
        <source>Make links</source>
        <translation type="unfinished">Tworzenie linków</translation>
    </message>
    <message>
        <source>Do you really want to make links for selected file(s)</source>
        <translation type="unfinished">Czy na prawdę chcesz utworzyć linki dla zaznaczonych plików</translation>
    </message>
    <message>
        <source>move</source>
        <translation type="unfinished">przenieś</translation>
    </message>
    <message>
        <source>copy</source>
        <translation type="unfinished">kopiuj</translation>
    </message>
    <message>
        <source>Source file :</source>
        <translation type="obsolete">Plik źródłowy :</translation>
    </message>
    <message>
        <source>Target file :</source>
        <translation type="obsolete">Plik docelowy :</translation>
    </message>
    <message>
        <source>Cannot</source>
        <translation type="unfinished">Nie można</translation>
    </message>
    <message>
        <source>to this same file</source>
        <translation type="obsolete">do tego samego pliku</translation>
    </message>
    <message>
        <source>copying</source>
        <translation type="unfinished">kopiowanie</translation>
    </message>
    <message>
        <source>removing</source>
        <translation type="unfinished">usuwanie</translation>
    </message>
    <message>
        <source>setting attributs</source>
        <translation type="unfinished">ustawianie atrybutów</translation>
    </message>
    <message>
        <source>Break</source>
        <translation type="unfinished">Przerwa</translation>
    </message>
    <message>
        <source>Do you really want to break an operation</source>
        <translation type="obsolete">Czy na prawdę chcesz przerwać operację</translation>
    </message>
    <message>
        <source>&amp;Yes</source>
        <translation type="unfinished">&amp;Tak</translation>
    </message>
    <message>
        <source>&amp;No</source>
        <translation type="unfinished">&amp;Nie</translation>
    </message>
    <message>
        <source>&amp;All</source>
        <translation type="unfinished">&amp;Wszystkie</translation>
    </message>
    <message>
        <source>N&amp;one</source>
        <translation type="unfinished">&amp;Nic nie robić</translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation type="unfinished">&amp;Anuluj</translation>
    </message>
    <message>
        <source>Delete directory</source>
        <translation type="unfinished">Usuwanie katalogu</translation>
    </message>
    <message>
        <source>Directory not empty</source>
        <translation type="unfinished">Katalog nie jest pusty</translation>
    </message>
    <message>
        <source>Delete it recursively ?</source>
        <translation type="unfinished">Usunąć go rekursywnie ?</translation>
    </message>
    <message>
        <source>Plugin&apos;s sPath</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Making empty</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Operation not yet supported in current file system.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>nFiles selected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>this nFiles</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Remove nFiles</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Source file:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Target file:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>to this same file!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Do you really want to break an eOperation</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FileOverwriteDialog</name>
    <message>
        <source>FileOverwriteDialog</source>
        <translation>FileOverwriteDialog</translation>
    </message>
    <message>
        <source>This file already exists !</source>
        <translation>Ten plik już istnieje !</translation>
    </message>
    <message>
        <source>23 september 2003, 19:42</source>
        <translation>23 september 2003, 19:42</translation>
    </message>
    <message>
        <source>9999.99 GB</source>
        <translation>9999.99 GB</translation>
    </message>
    <message>
        <source>Source file date :</source>
        <translation>Czas pliku źródłowego :</translation>
    </message>
    <message>
        <source>Target file date :</source>
        <translation>Czas pliku docelowego :</translation>
    </message>
    <message>
        <source>Overwrite it ?</source>
        <translation>Nadpisać go ?</translation>
    </message>
    <message>
        <source>&amp;Yes</source>
        <translation>&amp;Tak</translation>
    </message>
    <message>
        <source>Alt+Y</source>
        <translation>Alt+Y</translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation>&amp;Anuluj</translation>
    </message>
    <message>
        <source>Alt+C</source>
        <translation>Alt+C</translation>
    </message>
    <message>
        <source>N&amp;one</source>
        <translation>&amp;Nic nie robić</translation>
    </message>
    <message>
        <source>Alt+O</source>
        <translation>Alt+O</translation>
    </message>
    <message>
        <source>&amp;All</source>
        <translation>&amp;Wszystkie</translation>
    </message>
    <message>
        <source>Alt+A</source>
        <translation>Alt+A</translation>
    </message>
    <message>
        <source>&amp;Update all</source>
        <translation>&amp;Uaktualnij wszystkie</translation>
    </message>
    <message>
        <source>Alt+U</source>
        <translation>Alt+U</translation>
    </message>
    <message>
        <source>&amp;Rename</source>
        <translation>Zmień &amp;nazwę</translation>
    </message>
    <message>
        <source>Alt+R</source>
        <translation>Alt+R</translation>
    </message>
    <message>
        <source>If &amp;different size</source>
        <translation>Jeśli różny &amp;rozmiar</translation>
    </message>
    <message>
        <source>Alt+D</source>
        <translation>Alt+D</translation>
    </message>
    <message>
        <source>&amp;No</source>
        <translation>&amp;Nie</translation>
    </message>
    <message>
        <source>Alt+N</source>
        <translation>Alt+N</translation>
    </message>
    <message>
        <source>File overwriting</source>
        <translation type="obsolete">Nadpisywanie pliku</translation>
    </message>
    <message>
        <source>and size :</source>
        <translation>i rozmiar :</translation>
    </message>
</context>
<context>
    <name>FileOverwriteDlg</name>
    <message>
        <source>File overwriting</source>
        <translation type="unfinished">Nadpisywanie pliku</translation>
    </message>
</context>
<context>
    <name>FileView</name>
    <message>
        <source>Name</source>
        <translation>Nazwa</translation>
    </message>
    <message>
        <source>Location</source>
        <translation>Lokalizacja</translation>
    </message>
    <message>
        <source>Open in a &amp;new window</source>
        <translation>Otwórz w &amp;nowym oknie</translation>
    </message>
    <message>
        <source>&amp;Open</source>
        <translation>&amp;Otwórz</translation>
    </message>
    <message>
        <source>&amp;Save</source>
        <translation>&amp;Zapisz</translation>
    </message>
    <message>
        <source>&amp;Reload</source>
        <translation>&amp;Przeładuj</translation>
    </message>
    <message>
        <source>&amp;File</source>
        <translation>&amp;Plik</translation>
    </message>
    <message>
        <source>&amp;Edit</source>
        <translation>&amp;Edycja</translation>
    </message>
    <message>
        <source>&amp;View</source>
        <translation>&amp;Widok</translation>
    </message>
    <message>
        <source>&amp;Options</source>
        <translation>&amp;Opcje</translation>
    </message>
    <message>
        <source>&amp;Load old files list</source>
        <translation>Wczytaj &amp;starą listę plików</translation>
    </message>
    <message>
        <source>Save current files list</source>
        <translation>Zapisz bieżącą listę plików</translation>
    </message>
    <message>
        <source>Open &amp;recent files</source>
        <translation>Otwórz &amp;niedawne pliki</translation>
    </message>
    <message>
        <source>Save &amp;as</source>
        <translation>Zapisz &amp;jako</translation>
    </message>
    <message>
        <source>Side files &amp;list</source>
        <translation>&amp;Boczna lista plików</translation>
    </message>
    <message>
        <source>&amp;Quit</source>
        <translation>&amp;Wyjście</translation>
    </message>
    <message>
        <source>Show/Hide side &amp;list</source>
        <translation>Pokaż/schowaj boczą &amp;listę</translation>
    </message>
    <message>
        <source>Show/Hide &amp;menu bar</source>
        <translation>Pokaż/schowaj pasek &amp;menu</translation>
    </message>
    <message>
        <source>Show/Hide &amp;tool bar</source>
        <translation>Pokaż/schowaj pasek &amp;narzędziowy</translation>
    </message>
    <message>
        <source>Kind of view</source>
        <translation>Rodzaj widoku</translation>
    </message>
    <message>
        <source>Ra&amp;w</source>
        <translation>&amp;Surowy</translation>
    </message>
    <message>
        <source>&amp;Render</source>
        <translation>&amp;Renderowany</translation>
    </message>
    <message>
        <source>&amp;Hex</source>
        <translation>&amp;Heksadecymalny</translation>
    </message>
    <message>
        <source>&amp;Configure view</source>
        <translation>&amp;Konfiguracja podglądu</translation>
    </message>
    <message>
        <source>Save settings</source>
        <translation>Zapisz ustawienia</translation>
    </message>
    <message>
        <source>Open</source>
        <translation>Otwórz</translation>
    </message>
    <message>
        <source>Change kind of view to</source>
        <translation>Zmień rodzaj widoku na</translation>
    </message>
    <message>
        <source>Close view</source>
        <translation>Zamkni widok</translation>
    </message>
    <message>
        <source>File view error</source>
        <translation>Błąd podglądu pliku</translation>
    </message>
    <message>
        <source>Plugin&apos;s path</source>
        <translation>Ścieżka do plugina</translation>
    </message>
    <message>
        <source>Plugin</source>
        <translation>Plugin</translation>
    </message>
    <message>
        <source>not exists in foregoing location !</source>
        <translation>nie istnieje w wyzaczonym miejscu !</translation>
    </message>
    <message>
        <source>Try to open into binary view ?</source>
        <translation>Spróbować otworzyć w podglądzie binarnym ?</translation>
    </message>
    <message>
        <source>Plugins path</source>
        <translation>Ścieżka do plugina</translation>
    </message>
    <message>
        <source>Loading file in progress ...</source>
        <translation>Trwa ładowanie pliku ...</translation>
    </message>
    <message>
        <source>Open file into new window</source>
        <translation>Otwórz plik w nowym oknie</translation>
    </message>
    <message>
        <source>Open file</source>
        <translation>Otwórz plik</translation>
    </message>
    <message>
        <source>Loading aborted</source>
        <translation>Ładowanie przerwano</translation>
    </message>
    <message>
        <source>File has been saved.</source>
        <translation>Plik został zapisany.</translation>
    </message>
    <message>
        <source>Save as</source>
        <translation>Zapisz jako</translation>
    </message>
    <message>
        <source>Saving aborted</source>
        <translation>Zapisywanie przerwano</translation>
    </message>
    <message>
        <source>The files list has been loaded</source>
        <translation>Lista plików została załadowana</translation>
    </message>
    <message>
        <source>The files list has been saved</source>
        <translation>Lista plików została zapisana</translation>
    </message>
    <message>
        <source>Save file</source>
        <translation>Zapisz plik</translation>
    </message>
    <message>
        <source>Do you want to save the a changed document ?</source>
        <translation>Czy chcesz zapisać zmieniony dokument ?</translation>
    </message>
    <message>
        <source>Help</source>
        <translation>Pomoc</translation>
    </message>
    <message>
        <source>Reload</source>
        <translation>Przeładuj</translation>
    </message>
    <message>
        <source>Save</source>
        <translation>Zapisz</translation>
    </message>
    <message>
        <source>Open in new window</source>
        <translation>Otwórz w nowym oknie</translation>
    </message>
    <message>
        <source>Show/hide menu bar</source>
        <translation>Pokaż/schowaj pasek menu</translation>
    </message>
    <message>
        <source>Show/hide tool bar</source>
        <translation>Pokaż/schowaj pasek narzędziowy</translation>
    </message>
    <message>
        <source>Show/hide side list</source>
        <translation>Pokaż/schowaj boczną listę</translation>
    </message>
    <message>
        <source>Toggle to Raw mode</source>
        <translation>Przełącz na tryb surowy</translation>
    </message>
    <message>
        <source>Toggle to Edit mode</source>
        <translation>Przełącz na tryb edycji</translation>
    </message>
    <message>
        <source>Toggle to Render mode</source>
        <translation>Przełącz na tryb renderowany</translation>
    </message>
    <message>
        <source>Toggle to Hex mode</source>
        <translation>Przełącz na tryb Hex</translation>
    </message>
    <message>
        <source>Show settings</source>
        <translation>Pokaż ustawienia</translation>
    </message>
    <message>
        <source>Quit</source>
        <translation>Wyjście</translation>
    </message>
</context>
<context>
    <name>FilesPanel</name>
    <message>
        <source>Cannot open this archive</source>
        <translation>Nie można otworzyć tego archiwum</translation>
    </message>
    <message>
        <source>Permission denied.</source>
        <translation>Dostęp niedozwolony.</translation>
    </message>
    <message>
        <source>Cannot to embedded an external viewer !</source>
        <translation>Nie można osadzić zewnętrznego podglądu !</translation>
    </message>
    <message>
        <source>Free bytes on current disk</source>
        <translation>Wolnych bajtów na bieżącym dysku</translation>
    </message>
    <message>
        <source>Total bytes on current disk</source>
        <translation>Wszystkich bajtów na bieżącym dysku</translation>
    </message>
    <message>
        <source>More information ...</source>
        <translation>Więcej informacji ...</translation>
    </message>
    <message>
        <source>&amp;Configure filters</source>
        <translation>Konfiguracja &amp;filtrów</translation>
    </message>
    <message>
        <source>&amp;Configure favorites</source>
        <translation>Konfiguracja &amp;ulubionych</translation>
    </message>
    <message>
        <source>&amp;Add current URL</source>
        <translation>&amp;Dodaj bieżący URL</translation>
    </message>
    <message>
        <source>This kind of archives are not supported !</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FilesPanelForFind</name>
    <message>
        <source>Found files list.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FindFileDialog</name>
    <message>
        <source>FindFileDialog</source>
        <translation>FindFileDialog</translation>
    </message>
    <message>
        <source>&amp;Close</source>
        <translation>&amp;Zamknij</translation>
    </message>
    <message>
        <source>Alt+C</source>
        <translation>Alt+C</translation>
    </message>
    <message>
        <source>&amp;Find</source>
        <translation>&amp;Szukaj</translation>
    </message>
    <message>
        <source>Alt+F</source>
        <translation>Alt+F</translation>
    </message>
    <message>
        <source>&amp;Name/Location</source>
        <translation type="obsolete">&amp;Nazwa/Lokalizacja</translation>
    </message>
    <message>
        <source>&amp;Location :</source>
        <translation>&amp;Lokalizacja :</translation>
    </message>
    <message>
        <source>C&amp;ontents</source>
        <translation>&amp;Zawartość</translation>
    </message>
    <message>
        <source>Include &amp;binary files</source>
        <translation>Sprawdź też pliki &amp;binarne</translation>
    </message>
    <message>
        <source>Alt+B</source>
        <translation>Alt+B</translation>
    </message>
    <message>
        <source>A&amp;ttributs</source>
        <translation>&amp;Atrybuty</translation>
    </message>
    <message>
        <source>&amp;Take into consideration last modified time</source>
        <translation type="obsolete">&amp;Uwzględnij czas modyfikacji</translation>
    </message>
    <message>
        <source>Alt+T</source>
        <translation type="obsolete">Alt+T</translation>
    </message>
    <message>
        <source>Bytes</source>
        <translation>Bajtów</translation>
    </message>
    <message>
        <source>KB</source>
        <translation>KB</translation>
    </message>
    <message>
        <source>MB</source>
        <translation>MB</translation>
    </message>
    <message>
        <source>GB</source>
        <translation>GB</translation>
    </message>
    <message>
        <source>between :</source>
        <translation>pomiędzy :</translation>
    </message>
    <message>
        <source>and</source>
        <translation>a</translation>
    </message>
    <message>
        <source>is equal</source>
        <translation>jest równy</translation>
    </message>
    <message>
        <source>at least</source>
        <translation>ma co najmniej</translation>
    </message>
    <message>
        <source>maximum</source>
        <translation>ma maksymalnie</translation>
    </message>
    <message>
        <source>The file size :</source>
        <translation>Rozmiar pliku :</translation>
    </message>
    <message>
        <source>The file owner :</source>
        <translation>Właściciel pliku :</translation>
    </message>
    <message>
        <source>and group :</source>
        <translation>i grupy :</translation>
    </message>
    <message>
        <source>C&amp;ustomize</source>
        <translation type="obsolete">&amp;Dostosuj</translation>
    </message>
    <message>
        <source>Stop &amp;after</source>
        <translation>&amp;Zatrzymaj po</translation>
    </message>
    <message>
        <source>Alt+A</source>
        <translation>Alt+A</translation>
    </message>
    <message>
        <source>matches</source>
        <translation>dopasowaniach</translation>
    </message>
    <message>
        <source>Search into &amp;selected files only</source>
        <translation>Szukaj &amp;tylko w zaznaczonych plikach</translation>
    </message>
    <message>
        <source>Alt+S</source>
        <translation>Alt+S</translation>
    </message>
    <message>
        <source>Find &amp;recursive</source>
        <translation type="obsolete">Szukaj &amp;rekursywnie</translation>
    </message>
    <message>
        <source>Alt+R</source>
        <translation type="obsolete">Alt+R</translation>
    </message>
    <message>
        <source>directories</source>
        <translation type="obsolete">katalogów</translation>
    </message>
    <message>
        <source>Name</source>
        <translation>Nazwa</translation>
    </message>
    <message>
        <source>Size</source>
        <translation>Rozmiar</translation>
    </message>
    <message>
        <source>Time</source>
        <translation>Czas</translation>
    </message>
    <message>
        <source>Permission</source>
        <translation>Prawa dostępu</translation>
    </message>
    <message>
        <source>Owner</source>
        <translation>Właściciel</translation>
    </message>
    <message>
        <source>Group</source>
        <translation>Grupa</translation>
    </message>
    <message>
        <source>Location</source>
        <translation>Lokalizacja</translation>
    </message>
    <message>
        <source>&amp;Stop</source>
        <translation>&amp;Zatrzymaj</translation>
    </message>
    <message>
        <source>&amp;Pause</source>
        <translation>&amp;Przerwa</translation>
    </message>
    <message>
        <source>Alt+P</source>
        <translation>Alt+P</translation>
    </message>
    <message>
        <source>Clear &amp;list</source>
        <translation type="obsolete">&amp;Czyść listę</translation>
    </message>
    <message>
        <source>Alt+L</source>
        <translation type="obsolete">Alt+L</translation>
    </message>
    <message>
        <source>Find file(s)</source>
        <translation type="obsolete">Znajdź pliki</translation>
    </message>
    <message>
        <source>Ready.</source>
        <translation type="obsolete">Gotowy.</translation>
    </message>
    <message>
        <source>Matched:</source>
        <translation type="obsolete">Dopasowano :</translation>
    </message>
    <message>
        <source>from</source>
        <translation type="obsolete">z</translation>
    </message>
    <message>
        <source>files and</source>
        <translation type="obsolete">plików i</translation>
    </message>
    <message>
        <source>Searching</source>
        <translation type="obsolete">Szukanie</translation>
    </message>
    <message>
        <source>Pause.</source>
        <translation type="obsolete">Przerwa.</translation>
    </message>
    <message>
        <source>&amp;Go to file</source>
        <translation type="obsolete">&amp;Przejdź do pliku</translation>
    </message>
    <message>
        <source>&amp;View file</source>
        <translation type="obsolete">P&amp;odgląd pliku</translation>
    </message>
    <message>
        <source>&amp;Edit file</source>
        <translation type="obsolete">&amp;Edytuj plik</translation>
    </message>
    <message>
        <source>&amp;Delete file</source>
        <translation type="obsolete">&amp;Usuń plik</translation>
    </message>
    <message>
        <source>Don&apos;t gived a name to find</source>
        <translation type="obsolete">Nie podano nazwy do szukania</translation>
    </message>
    <message>
        <source>Don&apos;t gived a location for searching !</source>
        <translation type="obsolete">Nie podano lokalizacji wyszukiwania !</translation>
    </message>
    <message>
        <source>Filter :</source>
        <translation>Filtr :</translation>
    </message>
    <message>
        <source>Case se&amp;nsitive</source>
        <translation type="obsolete">Rozróżniaj wielkość &amp;liter</translation>
    </message>
    <message>
        <source>Alt+N</source>
        <translation type="obsolete">Alt+N</translation>
    </message>
    <message>
        <source>Na&amp;me :</source>
        <translation>Naz&amp;wa :</translation>
    </message>
    <message>
        <source>Contains &amp;text :</source>
        <translation>Zawierające &amp;tekst :</translation>
    </message>
    <message>
        <source>Regular &amp;expression</source>
        <translation type="obsolete">Wyrażenie &amp;regularne</translation>
    </message>
    <message>
        <source>Alt+E</source>
        <translation>Alt+E</translation>
    </message>
    <message>
        <source>Name/&amp;Location</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Case sensiti&amp;ve</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Alt+V</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Regular e&amp;xpression</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Alt+X</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Take into consideration last &amp;modified time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Alt+M</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Customi&amp;ze</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Find recursi&amp;ve</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Cl&amp;ear list</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>S&amp;how In panel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Alt+H</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FindFileDlg</name>
    <message>
        <source>Find file(s)</source>
        <translation type="unfinished">Znajdź pliki</translation>
    </message>
    <message>
        <source>Ready.</source>
        <translation type="unfinished">Gotowy.</translation>
    </message>
    <message>
        <source>Matched: %1 from %2 files and %3 directories</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Don&apos;t gived a name to find</source>
        <translation type="unfinished">Nie podano nazwy do szukania</translation>
    </message>
    <message>
        <source>Don&apos;t gived a location for searching !</source>
        <translation type="unfinished">Nie podano lokalizacji wyszukiwania !</translation>
    </message>
    <message>
        <source>Searching</source>
        <translation type="unfinished">Szukanie</translation>
    </message>
    <message>
        <source>&amp;Pause</source>
        <translation type="unfinished">&amp;Przerwa</translation>
    </message>
    <message>
        <source>Pause.</source>
        <translation type="unfinished">Przerwa.</translation>
    </message>
    <message>
        <source>Co&amp;ntinue</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Go to file</source>
        <translation type="unfinished">&amp;Przejdź do pliku</translation>
    </message>
    <message>
        <source>&amp;View file</source>
        <translation type="unfinished">P&amp;odgląd pliku</translation>
    </message>
    <message>
        <source>&amp;Edit file</source>
        <translation type="unfinished">&amp;Edytuj plik</translation>
    </message>
    <message>
        <source>&amp;Delete file</source>
        <translation type="unfinished">&amp;Usuń plik</translation>
    </message>
</context>
<context>
    <name>FtpConnectDialog</name>
    <message>
        <source>FtpConnectDialog</source>
        <translation>FtpConnectDialog</translation>
    </message>
    <message>
        <source>&amp;Password:</source>
        <translation>&amp;Hasło:</translation>
    </message>
    <message>
        <source>&amp;Directory:</source>
        <translation>&amp;Katalog:</translation>
    </message>
    <message>
        <source>&amp;Host:</source>
        <translation>&amp;Serwer:</translation>
    </message>
    <message>
        <source>P&amp;ort:</source>
        <translation>&amp;Port:</translation>
    </message>
    <message>
        <source>&amp;OK</source>
        <translation>&amp;OK</translation>
    </message>
    <message>
        <source>Alt+O</source>
        <translation>Alt+O</translation>
    </message>
    <message>
        <source>&amp;Save this session</source>
        <translation>&amp;Zapisz tę sesję</translation>
    </message>
    <message>
        <source>Alt+S</source>
        <translation>Alt+S</translation>
    </message>
    <message>
        <source>Connect to</source>
        <translation type="obsolete">Połącz z</translation>
    </message>
    <message>
        <source>Do not passed an user name !</source>
        <translation type="obsolete">Nie podano nazwy użytkownika !</translation>
    </message>
    <message>
        <source>Do not passed a password !</source>
        <translation type="obsolete">Nie podano hasła !</translation>
    </message>
    <message>
        <source>new session</source>
        <translation type="obsolete">nowa sesja</translation>
    </message>
    <message>
        <source>&amp;User:</source>
        <translation>&amp;Użytkownik :</translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation>&amp;Anuluj</translation>
    </message>
    <message>
        <source>Alt+C</source>
        <translation>Alt+C</translation>
    </message>
</context>
<context>
    <name>FtpConnectDlg</name>
    <message>
        <source>Connect to</source>
        <translation type="unfinished">Połącz z</translation>
    </message>
    <message>
        <source>Do not passed an sUser name !</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Do not passed a sPassword !</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>new session</source>
        <translation type="unfinished">nowa sesja</translation>
    </message>
</context>
<context>
    <name>InputDialog</name>
    <message>
        <source>InputDialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Save file &amp;permission</source>
        <translation type="unfinished">Zachowaj &amp;prawa dostępu</translation>
    </message>
    <message>
        <source>Alt+P</source>
        <translation type="unfinished">Alt+P</translation>
    </message>
    <message>
        <source>Always over&amp;write</source>
        <translation type="unfinished">Zawsze &amp;nadpisuj</translation>
    </message>
    <message>
        <source>Alt+W</source>
        <translation type="unfinished">Alt+W</translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation type="unfinished">&amp;Anuluj</translation>
    </message>
    <message>
        <source>Alt+C</source>
        <translation type="unfinished">Alt+C</translation>
    </message>
    <message>
        <source>&amp;OK</source>
        <translation type="unfinished">&amp;OK</translation>
    </message>
    <message>
        <source>Alt+O</source>
        <translation type="unfinished">Alt+O</translation>
    </message>
    <message>
        <source>In &amp;background</source>
        <translation type="unfinished">W &amp;tle</translation>
    </message>
    <message>
        <source>Alt+B</source>
        <translation type="unfinished">Alt+B</translation>
    </message>
</context>
<context>
    <name>InputDlg</name>
    <message>
        <source>Hard link</source>
        <translation type="unfinished">Twardy link</translation>
    </message>
    <message>
        <source>copy to</source>
        <translation type="unfinished">skopiować do</translation>
    </message>
    <message>
        <source>move to</source>
        <translation type="unfinished">przenosić do</translation>
    </message>
    <message>
        <source>new name for current file</source>
        <translation type="unfinished">nowa nazwa dla bieżącego pliku</translation>
    </message>
    <message>
        <source>new path for current link</source>
        <translation type="unfinished">nowa ścieżka dla bieżącego linku</translation>
    </message>
    <message>
        <source>new link name</source>
        <translation type="unfinished">nowa nazwa linku</translation>
    </message>
    <message>
        <source>Copying files to</source>
        <translation type="unfinished">Kopiowanie plików do</translation>
    </message>
    <message>
        <source>Moving files to</source>
        <translation type="unfinished">Przenoszenie plików do</translation>
    </message>
    <message>
        <source>Rename copied file</source>
        <translation type="unfinished">Zmiana nazwy kopiowanych plików</translation>
    </message>
    <message>
        <source>Making directory</source>
        <translation type="unfinished">Tworzenie katalogu</translation>
    </message>
    <message>
        <source>Making empty file</source>
        <translation type="unfinished">Tworzenie pustego pliku</translation>
    </message>
    <message>
        <source>Editing link</source>
        <translation type="unfinished">Edytowanie linku</translation>
    </message>
    <message>
        <source>Making link</source>
        <translation type="unfinished">Tworzenie linku</translation>
    </message>
</context>
<context>
    <name>ListView</name>
    <message>
        <source>Name</source>
        <translation type="unfinished">Nazwa</translation>
    </message>
    <message>
        <source>Size</source>
        <translation type="unfinished">Rozmiar</translation>
    </message>
    <message>
        <source>Time</source>
        <translation type="unfinished">Czas</translation>
    </message>
    <message>
        <source>Permission</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Owner</source>
        <translation type="unfinished">Właściciel</translation>
    </message>
    <message>
        <source>Group</source>
        <translation type="unfinished">Grupa</translation>
    </message>
    <message>
        <source>Location</source>
        <translation type="unfinished">Lokalizacja</translation>
    </message>
    <message>
        <source>UnSelect</source>
        <translation type="unfinished">Odznacz</translation>
    </message>
    <message>
        <source>Select</source>
        <translation type="unfinished">Zaznacz</translation>
    </message>
    <message>
        <source>group of files</source>
        <translation type="unfinished">grupa plików</translation>
    </message>
    <message>
        <source>Please to give new template:</source>
        <translation type="unfinished">Proszę podać nowy wzorzec:</translation>
    </message>
</context>
<context>
    <name>ListViewExt</name>
    <message>
        <source>Name</source>
        <translation type="obsolete">Nazwa</translation>
    </message>
    <message>
        <source>Size</source>
        <translation type="obsolete">Rozmiar</translation>
    </message>
    <message>
        <source>Time</source>
        <translation type="obsolete">Czas</translation>
    </message>
    <message>
        <source>Permission</source>
        <translation type="obsolete">Prawa dostępu</translation>
    </message>
    <message>
        <source>Owner</source>
        <translation type="obsolete">Właściciel</translation>
    </message>
    <message>
        <source>Group</source>
        <translation type="obsolete">Grupa</translation>
    </message>
    <message>
        <source>UnSelect</source>
        <translation type="obsolete">Odznacz</translation>
    </message>
    <message>
        <source>Select</source>
        <translation type="obsolete">Zaznacz</translation>
    </message>
    <message>
        <source>group of files</source>
        <translation type="obsolete">grupa plików</translation>
    </message>
    <message>
        <source>Please to give new template:</source>
        <translation type="obsolete">Proszę podać nowy wzorzec:</translation>
    </message>
</context>
<context>
    <name>PathView</name>
    <message>
        <source>&amp;Edit history</source>
        <translation>&amp;Edycja historii</translation>
    </message>
</context>
<context>
    <name>PopupMenuFP</name>
    <message>
        <source>&amp;Open</source>
        <translation type="unfinished">&amp;Otwórz</translation>
    </message>
    <message>
        <source>Open in panel &amp;beside</source>
        <translation type="unfinished">Otwórz w &amp;panelu obok</translation>
    </message>
    <message>
        <source>Re&amp;name</source>
        <translation type="unfinished">Zmień &amp;nazwę</translation>
    </message>
    <message>
        <source>&amp;Delete</source>
        <translation type="unfinished">&amp;Usuń</translation>
    </message>
    <message>
        <source>&amp;Copy</source>
        <translation type="unfinished">&amp;Kopiuj</translation>
    </message>
    <message>
        <source>&amp;Make a link</source>
        <translation type="unfinished">Utwórz &amp;link</translation>
    </message>
    <message>
        <source>Select &amp;All</source>
        <translation type="unfinished">Zaznacz &amp;wszystkie</translation>
    </message>
    <message>
        <source>&amp;UnSelect All</source>
        <translation type="unfinished">O&amp;dznacz wszystkie</translation>
    </message>
    <message>
        <source>Select with this &amp;same ext</source>
        <translation type="unfinished">Zaznacz z tym samym &amp;roszerzeniem</translation>
    </message>
    <message>
        <source>UnSelect with this same ext</source>
        <translation type="unfinished">Odznacz z tym samym rozszrzeniem</translation>
    </message>
    <message>
        <source>&amp;Properties</source>
        <translation type="unfinished">&amp;Właściwości</translation>
    </message>
    <message>
        <source>Open &amp;with</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PreOperationDialog</name>
    <message>
        <source>PreOperationDialog</source>
        <translation type="obsolete">PreOperationDialog</translation>
    </message>
    <message>
        <source>Save file &amp;permission</source>
        <translation type="obsolete">Zachowaj &amp;prawa dostępu</translation>
    </message>
    <message>
        <source>Alt+P</source>
        <translation type="obsolete">Alt+P</translation>
    </message>
    <message>
        <source>Always over&amp;write</source>
        <translation type="obsolete">Zawsze &amp;nadpisuj</translation>
    </message>
    <message>
        <source>Alt+W</source>
        <translation type="obsolete">Alt+W</translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation type="obsolete">&amp;Anuluj</translation>
    </message>
    <message>
        <source>Alt+C</source>
        <translation type="obsolete">Alt+C</translation>
    </message>
    <message>
        <source>&amp;OK</source>
        <translation type="obsolete">&amp;OK</translation>
    </message>
    <message>
        <source>Alt+O</source>
        <translation type="obsolete">Alt+O</translation>
    </message>
    <message>
        <source>In &amp;background</source>
        <translation type="obsolete">W &amp;tle</translation>
    </message>
    <message>
        <source>Alt+B</source>
        <translation type="obsolete">Alt+B</translation>
    </message>
    <message>
        <source>Hard link</source>
        <translation type="obsolete">Twardy link</translation>
    </message>
    <message>
        <source>copy to</source>
        <translation type="obsolete">skopiować do</translation>
    </message>
    <message>
        <source>move to</source>
        <translation type="obsolete">przenosić do</translation>
    </message>
    <message>
        <source>new name for current file</source>
        <translation type="obsolete">nowa nazwa dla bieżącego pliku</translation>
    </message>
    <message>
        <source>new path for current link</source>
        <translation type="obsolete">nowa ścieżka dla bieżącego linku</translation>
    </message>
    <message>
        <source>new link name</source>
        <translation type="obsolete">nowa nazwa linku</translation>
    </message>
    <message>
        <source>Copying files to</source>
        <translation type="obsolete">Kopiowanie plików do</translation>
    </message>
    <message>
        <source>Moving files to</source>
        <translation type="obsolete">Przenoszenie plików do</translation>
    </message>
    <message>
        <source>Rename copied file</source>
        <translation type="obsolete">Zmiana nazwy kopiowanych plików</translation>
    </message>
    <message>
        <source>Editing link</source>
        <translation type="obsolete">Edytowanie linku</translation>
    </message>
    <message>
        <source>Making directory</source>
        <translation type="obsolete">Tworzenie katalogu</translation>
    </message>
    <message>
        <source>Making empty file</source>
        <translation type="obsolete">Tworzenie pustego pliku</translation>
    </message>
    <message>
        <source>Making link</source>
        <translation type="obsolete">Tworzenie linku</translation>
    </message>
</context>
<context>
    <name>ProgressDialog</name>
    <message>
        <source>ProgressDialog</source>
        <translation>ProgressDialog</translation>
    </message>
    <message>
        <source>Source :</source>
        <translation>Źródło :</translation>
    </message>
    <message>
        <source>Target :</source>
        <translation>Cel :</translation>
    </message>
    <message>
        <source>Close after &amp;finished</source>
        <translation>Zamknij po &amp;zakończeniu</translation>
    </message>
    <message>
        <source>In &amp;background</source>
        <translation>W &amp;tle</translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation>&amp;Anuluj</translation>
    </message>
    <message>
        <source>Weighed :</source>
        <translation>Zważono :</translation>
    </message>
    <message>
        <source>Transfer :</source>
        <translation>Transfer :</translation>
    </message>
    <message>
        <source>Time :</source>
        <translation>Czas :</translation>
    </message>
    <message>
        <source>Estimated time :</source>
        <translation>Szcowany czas :</translation>
    </message>
    <message>
        <source>Moving</source>
        <translation type="obsolete">Przenoszenie</translation>
    </message>
    <message>
        <source>Copying</source>
        <translation type="obsolete">Kopiowanie</translation>
    </message>
    <message>
        <source>Removing</source>
        <translation type="obsolete">Usuwanie</translation>
    </message>
    <message>
        <source>Weighing</source>
        <translation type="obsolete">Ważenie</translation>
    </message>
    <message>
        <source>Setting the attributs</source>
        <translation type="obsolete">Ustawianie atrybutów</translation>
    </message>
    <message>
        <source>files</source>
        <translation type="obsolete">plików</translation>
    </message>
    <message>
        <source>bytes per second</source>
        <translation type="obsolete">bajtów na sekundę</translation>
    </message>
    <message>
        <source>per second</source>
        <translation type="obsolete">na sekundę</translation>
    </message>
    <message>
        <source>directories</source>
        <translation type="obsolete">katalogów</translation>
    </message>
    <message>
        <source>Copied</source>
        <translation type="obsolete">Skopiowano</translation>
    </message>
    <message>
        <source>Removed</source>
        <translation type="obsolete">Usunięto</translation>
    </message>
    <message>
        <source>Weighed</source>
        <translation type="obsolete">Zważono</translation>
    </message>
    <message>
        <source>Set the attributs</source>
        <translation type="obsolete">Ustawiono atrybutów</translation>
    </message>
</context>
<context>
    <name>ProgressDlg</name>
    <message>
        <source>Moving</source>
        <translation type="unfinished">Przenoszenie</translation>
    </message>
    <message>
        <source>Copying</source>
        <translation type="unfinished">Kopiowanie</translation>
    </message>
    <message>
        <source>Removing</source>
        <translation type="unfinished">Usuwanie</translation>
    </message>
    <message>
        <source>Weighing</source>
        <translation type="unfinished">Ważenie</translation>
    </message>
    <message>
        <source>Setting the attributs</source>
        <translation type="unfinished">Ustawianie atrybutów</translation>
    </message>
    <message>
        <source>files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>bytes per second</source>
        <translation type="unfinished">bajtów na sekundę</translation>
    </message>
    <message>
        <source>per second</source>
        <translation type="unfinished">na sekundę</translation>
    </message>
    <message>
        <source>directories</source>
        <translation type="unfinished">katalogów</translation>
    </message>
    <message>
        <source>Copied</source>
        <translation type="unfinished">Skopiowano</translation>
    </message>
    <message>
        <source>Removed</source>
        <translation type="unfinished">Usunięto</translation>
    </message>
    <message>
        <source>Weighed</source>
        <translation type="unfinished">Zważono</translation>
    </message>
    <message>
        <source>Set the attributs</source>
        <translation type="unfinished">Ustawiono atrybutów</translation>
    </message>
</context>
<context>
    <name>PropertiesDialog</name>
    <message>
        <source>PropertiesDialog</source>
        <translation>PropertiesDialog</translation>
    </message>
    <message>
        <source>&amp;OK</source>
        <translation>&amp;OK</translation>
    </message>
    <message>
        <source>Alt+O</source>
        <translation>Alt+O</translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation>&amp;Anuluj</translation>
    </message>
    <message>
        <source>Alt+C</source>
        <translation>Alt+C</translation>
    </message>
    <message>
        <source>General</source>
        <translation>Ogólne</translation>
    </message>
    <message>
        <source>Type :</source>
        <translation>Typ :</translation>
    </message>
    <message>
        <source>Location :</source>
        <translation>Położenie :</translation>
    </message>
    <message>
        <source>&amp;Name :</source>
        <translation>&amp;Nazwa :</translation>
    </message>
    <message>
        <source>Size :</source>
        <translation>Rozmiar :</translation>
    </message>
    <message>
        <source>Change access and modified &amp;time</source>
        <translation>Zmień &amp;czas dostępu i modyfikacji</translation>
    </message>
    <message>
        <source>Alt+T</source>
        <translation>Alt+T</translation>
    </message>
    <message>
        <source>&amp;Refresh</source>
        <translation>&amp;Odśwież</translation>
    </message>
    <message>
        <source>Alt+R</source>
        <translation>Alt+R</translation>
    </message>
    <message>
        <source>Permission</source>
        <translation></translation>
    </message>
    <message>
        <source>Change &amp;owner and group</source>
        <translation>Zmień &amp;nazwę użytkownika i grupy</translation>
    </message>
    <message>
        <source>Class</source>
        <translation>Klasa</translation>
    </message>
    <message>
        <source>User</source>
        <translation>Użytkownik</translation>
    </message>
    <message>
        <source>Group</source>
        <translation>Grupa</translation>
    </message>
    <message>
        <source>Other</source>
        <translation>Pozostali</translation>
    </message>
    <message>
        <source>Read</source>
        <translation>Odczyt</translation>
    </message>
    <message>
        <source>U</source>
        <translation>U</translation>
    </message>
    <message>
        <source>G</source>
        <translation>G</translation>
    </message>
    <message>
        <source>O</source>
        <translation>P</translation>
    </message>
    <message>
        <source>Write</source>
        <translation>Zapis</translation>
    </message>
    <message>
        <source>Executable</source>
        <translation>Uruchamianie</translation>
    </message>
    <message>
        <source>Special</source>
        <translation>Specjalne</translation>
    </message>
    <message>
        <source>SUID</source>
        <translation>SUID</translation>
    </message>
    <message>
        <source>SGID</source>
        <translation>SGID</translation>
    </message>
    <message>
        <source>SVTX</source>
        <translation>SVTX</translation>
    </message>
    <message>
        <source>Change &amp;permission</source>
        <translation>Zmień &amp;prawa dostępu</translation>
    </message>
    <message>
        <source>Alt+P</source>
        <translation>Alt+P</translation>
    </message>
    <message>
        <source>Group :</source>
        <translation>Grupa :</translation>
    </message>
    <message>
        <source>Owner :</source>
        <translation>Właściciel :</translation>
    </message>
    <message>
        <source>Customize</source>
        <translation>Dostosuj</translation>
    </message>
    <message>
        <source>&amp;Recursively changes</source>
        <translation>Zmiany &amp;rekursywne</translation>
    </message>
    <message>
        <source>ALL SELECTED</source>
        <translation type="obsolete">WSZYSTKIE ZAZNACZONE</translation>
    </message>
    <message>
        <source>directories and</source>
        <translation type="obsolete">katalogów i</translation>
    </message>
    <message>
        <source>files</source>
        <translation type="obsolete">pliki</translation>
    </message>
    <message>
        <source>directory</source>
        <translation type="obsolete">katalog</translation>
    </message>
    <message>
        <source>symbolic/hard link</source>
        <translation type="obsolete">link symboliczny/link twardy</translation>
    </message>
    <message>
        <source>file</source>
        <translation type="obsolete">plik</translation>
    </message>
    <message>
        <source>The file(s) properties</source>
        <translation type="obsolete">Właściwości pliku(ów)</translation>
    </message>
    <message>
        <source>Changes for :</source>
        <translation>Zmiany dla :</translation>
    </message>
    <message>
        <source>Last &amp;access time :</source>
        <translation>Czas ostatniego &amp;odczytu :</translation>
    </message>
    <message>
        <source>Last &amp;modified time :</source>
        <translation>Czas ostatniej &amp;modyfikacji :</translation>
    </message>
    <message>
        <source>&amp;Stop</source>
        <translation>&amp;Zatrzymaj</translation>
    </message>
    <message>
        <source>Alt+S</source>
        <translation>Alt+S</translation>
    </message>
    <message>
        <source>Weighing</source>
        <translation type="obsolete">Ważenie</translation>
    </message>
</context>
<context>
    <name>PropertiesDlg</name>
    <message>
        <source>The file(s) properties</source>
        <translation type="unfinished">Właściwości pliku(ów)</translation>
    </message>
    <message>
        <source>ALL SELECTED</source>
        <translation type="unfinished">WSZYSTKIE ZAZNACZONE</translation>
    </message>
    <message>
        <source>%1 directories and %2 files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Weighing, please wait</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>nFiles</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>directory</source>
        <translation type="unfinished">katalog</translation>
    </message>
    <message>
        <source>symbolic/hard link</source>
        <translation type="unfinished">link symboliczny/link twardy</translation>
    </message>
    <message>
        <source>file</source>
        <translation type="unfinished">plik</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>Usage:  qtcmd [options]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Options:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>URL to init left panel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>URL to init right panel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QtCmd</name>
    <message>
        <source>List View</source>
        <translation>Widok listy</translation>
    </message>
    <message>
        <source>Tree View</source>
        <translation>Widok drzewa</translation>
    </message>
    <message>
        <source>User defined</source>
        <translation>Zdefiniowane przez użytkownika</translation>
    </message>
    <message>
        <source>width of left panel</source>
        <translation>szerokość lewego panela</translation>
    </message>
    <message>
        <source>width of right panel</source>
        <translation>szerokość prawego panela</translation>
    </message>
    <message>
        <source>&amp;File</source>
        <translation>&amp;Plik</translation>
    </message>
    <message>
        <source>&amp;View</source>
        <translation>&amp;Widok</translation>
    </message>
    <message>
        <source>&amp;Settings</source>
        <translation>&amp;Ustawienia</translation>
    </message>
    <message>
        <source>&amp;Tools</source>
        <translation>&amp;Narzędzia</translation>
    </message>
    <message>
        <source>&amp;Help</source>
        <translation>&amp;Pomoc</translation>
    </message>
    <message>
        <source>Show/Hide &amp;menu bar</source>
        <translation>Pokaż/schowaj pasek &amp;menu</translation>
    </message>
    <message>
        <source>Show/Hide &amp;tool bar</source>
        <translation>Pokaż/schowaj pasek &amp;narzędziowy</translation>
    </message>
    <message>
        <source>Configure </source>
        <translation>Konfiguracja</translation>
    </message>
    <message>
        <source>FTP connection manager</source>
        <translation>Menadżer połączeń FTP</translation>
    </message>
    <message>
        <source>Show free space on current disk</source>
        <translation>Pokaż ilość wolnego miejsca na bieżącym dysku</translation>
    </message>
    <message>
        <source>Show storage devices list</source>
        <translation>Pokaż listę urządzeń masowych</translation>
    </message>
    <message>
        <source>Reread current directory</source>
        <translation>Odczytaj ponownie bieżący katalog</translation>
    </message>
    <message>
        <source>Show favorites</source>
        <translation>Pokaż ulubione</translation>
    </message>
    <message>
        <source>Fin&amp;d file</source>
        <translation>&amp;Znajdź plik</translation>
    </message>
    <message>
        <source>Quick get out from file system</source>
        <translation type="obsolete">Wyskocz z systemu plików</translation>
    </message>
    <message>
        <source>Kind of &amp;view for current panel</source>
        <translation>Rodzaj &amp;widoku dla bieżącego panela</translation>
    </message>
    <message>
        <source>&amp;Proportions of panels</source>
        <translation>&amp;Proporcje paneli</translation>
    </message>
    <message>
        <source>&amp;About</source>
        <translation>O &amp;programie</translation>
    </message>
    <message>
        <source>About &amp;Qt</source>
        <translation>O &amp;Qt</translation>
    </message>
    <message>
        <source>Change kind of list view</source>
        <translation>Zmień rodzaj widoku listy</translation>
    </message>
    <message>
        <source>Proportions of panels</source>
        <translation>Proporcje paneli</translation>
    </message>
    <message>
        <source>Twin panels files manager</source>
        <translation>Dwupanelowy menadżer plików</translation>
    </message>
    <message>
        <source>Plugins path</source>
        <translation>Ścieżka do plugina</translation>
    </message>
    <message>
        <source>Plugin</source>
        <translation>Plugin</translation>
    </message>
    <message>
        <source>not exists in foregoing location !</source>
        <translation>nie istnieje w wyzaczonym miejscu !</translation>
    </message>
    <message>
        <source>Show/Hide &amp;button bar</source>
        <translation>Pokaż/schowaj pasek z &amp;guzikami</translation>
    </message>
    <message>
        <source>Help</source>
        <translation>Pomoc</translation>
    </message>
    <message>
        <source>Change view</source>
        <translation>Zmień widok</translation>
    </message>
    <message>
        <source>View</source>
        <translation>Podgląd</translation>
    </message>
    <message>
        <source>Edit</source>
        <translation>Edytuj</translation>
    </message>
    <message>
        <source>Copy</source>
        <translation>Kopiuj</translation>
    </message>
    <message>
        <source>Move</source>
        <translation>Przenieś</translation>
    </message>
    <message>
        <source>Make dir</source>
        <translation>Utwórz katalog</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation>Usuń</translation>
    </message>
    <message>
        <source>Quit</source>
        <translation>Wyjście</translation>
    </message>
    <message>
        <source>Do you really want to quit</source>
        <translation>Czy na prawdę chcesz wyjść</translation>
    </message>
    <message>
        <source>Quit an application</source>
        <translation>Zakończ aplikację</translation>
    </message>
    <message>
        <source>View file</source>
        <translation>Podgląd pliku</translation>
    </message>
    <message>
        <source>Edit file</source>
        <translation>Edycja pliku</translation>
    </message>
    <message>
        <source>Copy files</source>
        <translation>Kopiowanie plików</translation>
    </message>
    <message>
        <source>Move files</source>
        <translation>Przenoszenie plików</translation>
    </message>
    <message>
        <source>Make directory</source>
        <translation>Tworzenie katalogu</translation>
    </message>
    <message>
        <source>Delete files</source>
        <translation>Usuwanie plików</translation>
    </message>
    <message>
        <source>Make an empty file</source>
        <translation>Tworzenie pustego pliku</translation>
    </message>
    <message>
        <source>Copy files to current directory</source>
        <translation>Kopiowanie pliku do bieżącego katalogu</translation>
    </message>
    <message>
        <source>Quick file rename</source>
        <translation>Szybka zmiana nazwy</translation>
    </message>
    <message>
        <source>Quick change of file modified date</source>
        <translation>Szybka zmiana czasu modyfikacji</translation>
    </message>
    <message>
        <source>Quick change of file permission</source>
        <translation>Szybka zmiana praw dostępu</translation>
    </message>
    <message>
        <source>Quick change of file owner</source>
        <translation>Szybka zmiana właściciela</translation>
    </message>
    <message>
        <source>Quick change of file group</source>
        <translation>Szybka zmiana grupy</translation>
    </message>
    <message>
        <source>Go to up level</source>
        <translation>Przejdź na wyższy poziom</translation>
    </message>
    <message>
        <source>Refresh directory</source>
        <translation>Odśwież katalog</translation>
    </message>
    <message>
        <source>Make a link</source>
        <translation>Utwórz link</translation>
    </message>
    <message>
        <source>Edit current link</source>
        <translation>Edytuj bieżący link</translation>
    </message>
    <message>
        <source>Select files with this same extention</source>
        <translation>Zaznacz pliki z tym samym roszerzeniem</translation>
    </message>
    <message>
        <source>Deselect files with this same extention</source>
        <translation>Odznacz pliki z tym samym roszerzeniem</translation>
    </message>
    <message>
        <source>Select all files</source>
        <translation>Zaznacz wszystkie pliki</translation>
    </message>
    <message>
        <source>Deselect all files</source>
        <translation>Odznacz wszystkie pliki</translation>
    </message>
    <message>
        <source>Select group of files</source>
        <translation>Zaznacz grupę plików</translation>
    </message>
    <message>
        <source>Deselect group of files</source>
        <translation>Odznacz grupę plików</translation>
    </message>
    <message>
        <source>Change view to list</source>
        <translation>Zmień widok na listę</translation>
    </message>
    <message>
        <source>Change view to tree</source>
        <translation>Zmień widok na drzewo</translation>
    </message>
    <message>
        <source>Quick file search</source>
        <translation>Szybkie szukanie pliku</translation>
    </message>
    <message>
        <source>Show history&apos;s menu</source>
        <translation>Pokaż menu historii</translation>
    </message>
    <message>
        <source>Show history</source>
        <translation>Pokaż historię</translation>
    </message>
    <message>
        <source>Go to previous URL</source>
        <translation>Przejdź do poprzedniego URLa</translation>
    </message>
    <message>
        <source>Go to next URL</source>
        <translation>Przejdź do następnego URLa</translation>
    </message>
    <message>
        <source>Move cursor to path view</source>
        <translation>Przenieś kursor do widoku ścieżki</translation>
    </message>
    <message>
        <source>Show free space</source>
        <translation>Pokaż ilość wolnego miejsca</translation>
    </message>
    <message>
        <source>Find file</source>
        <translation>Znajdź plik</translation>
    </message>
    <message>
        <source>Quick go out from current file system</source>
        <translation>Wyskocz z bieżącego systemu plików</translation>
    </message>
    <message>
        <source>Open current file or directory in left panel</source>
        <translation>Otwórz bieżący plik lub katalog w lewym panelu</translation>
    </message>
    <message>
        <source>Open current file or directory in right panel</source>
        <translation>Otwórz bieżący plik lub katalog w prawym panelu</translation>
    </message>
    <message>
        <source>Force open current directory in left panel</source>
        <translation>Wymuś otwarcie bieżącego katalogu w lewym panelu</translation>
    </message>
    <message>
        <source>Force open current directory in right panel</source>
        <translation>Wymuś otwarcie bieżącego katalogu w prawympanelu</translation>
    </message>
    <message>
        <source>Weigh current directory</source>
        <translation>Zważ bieżący katalog</translation>
    </message>
    <message>
        <source>Show settings of application</source>
        <translation>Pokaż ustawienia aplikacji</translation>
    </message>
    <message>
        <source>Show/hide menu bar</source>
        <translation>Pokaż/schowaj pasek menu</translation>
    </message>
    <message>
        <source>Show/hide tool bar</source>
        <translation>Pokaż/schowaj pasek narzędziowy</translation>
    </message>
    <message>
        <source>Show/hide buttons bar</source>
        <translation>Pokaż/schowaj pasek z guzikami</translation>
    </message>
    <message>
        <source>Show filters menu</source>
        <translation type="obsolete">Pokaż menu filtrów</translation>
    </message>
    <message>
        <source>Show FTP manager</source>
        <translation>Pokaż menadżer połączeń FTP</translation>
    </message>
    <message>
        <source>Show filters manager</source>
        <translation type="obsolete">Pokaż menadżer filtrów</translation>
    </message>
    <message>
        <source>Show file properties dialog</source>
        <translation>Pokaż okienko właściwości</translation>
    </message>
    <message>
        <source>QtCommander a old school file manager. An application is similar to Total Commander, but with many extras.</source>
        <translation type="obsolete">QtCommander jest meadżerem plików wywodzącym się ze starej szkoły. Aplikacja jest podobna do Total Commander, ale ma kilka dodatów.</translation>
    </message>
    <message>
        <source>QtCmd have a FTP client (no recursive operations), simple archive support (listing only),</source>
        <translation type="obsolete">QtCmd ma klienta FTP (be obsługiwania operacji rekurencyjnych), prostą obsługę archiwów (tylko listowanie),</translation>
    </message>
    <message>
        <source>full local files system support and mounted file system.</source>
        <translation type="obsolete">pełne wsparcie dla lokalnego systemu plików oraz system montowania systemu plików.</translation>
    </message>
    <message>
        <source>Possess advanced and very configurable file viewer and editor.</source>
        <translation type="obsolete">Posiada zaawansowany a bardzo konfigurowalny podgląd i edytor.</translation>
    </message>
    <message>
        <source>Go to home directory</source>
        <translation>Przejdź do katalogu domowego</translation>
    </message>
    <message>
        <source>Distributed under the terms of the GNU General Public License v2</source>
        <translation>Rozpowszechniane zgodnie z licencją GNU General Public License wersja 2</translation>
    </message>
    <message>
        <source>Open &amp;file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Open &amp;directory</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Open &amp;archive</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Configure file &amp;view</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Quick get out from the file system</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Open</source>
        <translation type="unfinished">&amp;Otwórz</translation>
    </message>
    <message>
        <source>Show pFilters menu</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Show pFilters manager</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Show settings of view</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Reload</source>
        <translation type="unfinished">Przeładuj</translation>
    </message>
    <message>
        <source>Open</source>
        <translation type="unfinished">Otwórz</translation>
    </message>
    <message>
        <source>Save</source>
        <translation type="unfinished">Zapisz</translation>
    </message>
    <message>
        <source>Save as</source>
        <translation type="unfinished">Zapisz jako</translation>
    </message>
    <message>
        <source>Open in new window</source>
        <translation type="unfinished">Otwórz w nowym oknie</translation>
    </message>
    <message>
        <source>Show/hide side list</source>
        <translation type="unfinished">Pokaż/schowaj boczną listę</translation>
    </message>
    <message>
        <source>Toggle to Raw mode</source>
        <translation type="unfinished">Przełącz na tryb surowy</translation>
    </message>
    <message>
        <source>Toggle to Edit mode</source>
        <translation type="unfinished">Przełącz na tryb edycji</translation>
    </message>
    <message>
        <source>Toggle to Render mode</source>
        <translation type="unfinished">Przełącz na tryb renderowany</translation>
    </message>
    <message>
        <source>Toggle to Hex mode</source>
        <translation type="unfinished">Przełącz na tryb Hex</translation>
    </message>
    <message>
        <source>Show settings</source>
        <translation type="unfinished">Pokaż ustawienia</translation>
    </message>
</context>
<context>
    <name>UrlsListManager</name>
    <message>
        <source>URLs List Manager</source>
        <translation type="obsolete">Menadżer listy URLi</translation>
    </message>
    <message>
        <source>&amp;New</source>
        <translation type="obsolete">&amp;Nowy</translation>
    </message>
    <message>
        <source>&amp;Rename</source>
        <translation type="obsolete">Z&amp;mień nazwę</translation>
    </message>
    <message>
        <source>&amp;Delete</source>
        <translation type="obsolete">&amp;Usuń</translation>
    </message>
    <message>
        <source>&amp;Go to URL</source>
        <translation type="obsolete">Przejdź do &amp;URLa</translation>
    </message>
    <message>
        <source>Alt+G</source>
        <translation type="obsolete">Alt+G</translation>
    </message>
    <message>
        <source>Do&amp;wn</source>
        <translation type="obsolete">W &amp;dół</translation>
    </message>
    <message>
        <source>&amp;Up</source>
        <translation type="obsolete">W &amp;górę</translation>
    </message>
    <message>
        <source>P&amp;assword</source>
        <translation type="obsolete">&amp;Hasło</translation>
    </message>
    <message>
        <source>&amp;Port</source>
        <translation type="obsolete">P&amp;ort</translation>
    </message>
    <message>
        <source>&amp;Host</source>
        <translation type="obsolete">&amp;Serwer</translation>
    </message>
    <message>
        <source>&amp;Save</source>
        <translation type="obsolete">&amp;Zapisz</translation>
    </message>
    <message>
        <source>&amp;OK</source>
        <translation type="obsolete">&amp;OK</translation>
    </message>
    <message>
        <source>Favorites Manager</source>
        <translation type="obsolete">Menadżer ulubionych</translation>
    </message>
    <message>
        <source>History Manager</source>
        <translation type="obsolete">Menadżer historii</translation>
    </message>
    <message>
        <source>FTP Connections Manager</source>
        <translation type="obsolete">Menadżer połączeń FTP</translation>
    </message>
    <message>
        <source>New FTP session</source>
        <translation type="obsolete">Nowa sesja FTP</translation>
    </message>
    <message>
        <source>Please to give a name:</source>
        <translation type="obsolete">Proszę podać nazwę:</translation>
    </message>
    <message>
        <source>new name</source>
        <translation type="obsolete">nowa nazwa</translation>
    </message>
    <message>
        <source>Item</source>
        <translation type="obsolete">element</translation>
    </message>
    <message>
        <source>Rename FTP session</source>
        <translation type="obsolete">Zmień nazwę sesji FTP</translation>
    </message>
    <message>
        <source>Direct&amp;ory</source>
        <translation type="obsolete">&amp;Katalog</translation>
    </message>
    <message>
        <source>Us&amp;er</source>
        <translation type="obsolete">&amp;Użytkownik</translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation type="obsolete">&amp;Anuluj</translation>
    </message>
    <message>
        <source>Alt+C</source>
        <translation type="obsolete">Alt+C</translation>
    </message>
    <message>
        <source>Filters Manager</source>
        <translation type="obsolete">Menadżer filtrów</translation>
    </message>
    <message>
        <source>&amp;Apply</source>
        <translation type="obsolete">Za&amp;stosuj</translation>
    </message>
    <message>
        <source>&amp;Pattern :</source>
        <translation type="obsolete">&amp;Wzorzec :</translation>
    </message>
    <message>
        <source>Connec&amp;t</source>
        <translation type="obsolete">&amp;Połącz</translation>
    </message>
    <message>
        <source>A new filter</source>
        <translation type="obsolete">Nowy filtr</translation>
    </message>
    <message>
        <source>A new favorite</source>
        <translation type="obsolete">Nowy ulubiony</translation>
    </message>
    <message>
        <source>Rename a filter</source>
        <translation type="obsolete">Zmień nazwę filtrowi</translation>
    </message>
    <message>
        <source>Rename a favorite</source>
        <translation type="obsolete">Zmień nazwę ulubionemu</translation>
    </message>
</context>
<context>
    <name>UrlsManagerDialog</name>
    <message>
        <source>URLs List Manager</source>
        <translation type="unfinished">Menadżer listy URLi</translation>
    </message>
    <message>
        <source>&amp;New</source>
        <translation type="unfinished">&amp;Nowy</translation>
    </message>
    <message>
        <source>&amp;Rename</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Delete</source>
        <translation type="unfinished">&amp;Usuń</translation>
    </message>
    <message>
        <source>&amp;Go to URL</source>
        <translation type="unfinished">Przejdź do &amp;URLa</translation>
    </message>
    <message>
        <source>Alt+G</source>
        <translation type="unfinished">Alt+G</translation>
    </message>
    <message>
        <source>Do&amp;wn</source>
        <translation type="unfinished">W &amp;dół</translation>
    </message>
    <message>
        <source>&amp;Up</source>
        <translation type="unfinished">W &amp;górę</translation>
    </message>
    <message>
        <source>Direct&amp;ory</source>
        <translation type="unfinished">&amp;Katalog</translation>
    </message>
    <message>
        <source>P&amp;assword</source>
        <translation type="unfinished">&amp;Hasło</translation>
    </message>
    <message>
        <source>Us&amp;er</source>
        <translation type="unfinished">&amp;Użytkownik</translation>
    </message>
    <message>
        <source>&amp;Port</source>
        <translation type="unfinished">P&amp;ort</translation>
    </message>
    <message>
        <source>&amp;Host</source>
        <translation type="unfinished">&amp;Serwer</translation>
    </message>
    <message>
        <source>&amp;Save</source>
        <translation type="unfinished">&amp;Zapisz</translation>
    </message>
    <message>
        <source>&amp;OK</source>
        <translation type="unfinished">&amp;OK</translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation type="unfinished">&amp;Anuluj</translation>
    </message>
    <message>
        <source>Alt+C</source>
        <translation type="unfinished">Alt+C</translation>
    </message>
</context>
<context>
    <name>UrlsManagerDlg</name>
    <message>
        <source>Favorites Manager</source>
        <translation type="unfinished">Menadżer ulubionych</translation>
    </message>
    <message>
        <source>History Manager</source>
        <translation type="unfinished">Menadżer historii</translation>
    </message>
    <message>
        <source>FTP Connections Manager</source>
        <translation type="unfinished">Menadżer połączeń FTP</translation>
    </message>
    <message>
        <source>Filters Manager</source>
        <translation type="unfinished">Menadżer filtrów</translation>
    </message>
    <message>
        <source>&amp;Apply</source>
        <translation type="unfinished">Za&amp;stosuj</translation>
    </message>
    <message>
        <source>&amp;Pattern :</source>
        <translation type="unfinished">&amp;Wzorzec :</translation>
    </message>
    <message>
        <source>Connec&amp;t</source>
        <translation type="unfinished">&amp;Połącz</translation>
    </message>
    <message>
        <source>New FTP session</source>
        <translation type="unfinished">Nowa sesja FTP</translation>
    </message>
    <message>
        <source>A new filter</source>
        <translation type="unfinished">Nowy filtr</translation>
    </message>
    <message>
        <source>A new favorite</source>
        <translation type="unfinished">Nowy ulubiony</translation>
    </message>
    <message>
        <source>Please to give a name:</source>
        <translation type="unfinished">Proszę podać nazwę:</translation>
    </message>
    <message>
        <source>new name</source>
        <translation type="unfinished">nowa nazwa</translation>
    </message>
    <message>
        <source>Item</source>
        <translation type="unfinished">element</translation>
    </message>
    <message>
        <source>Rename FTP session</source>
        <translation type="unfinished">Zmień nazwę sesji FTP</translation>
    </message>
    <message>
        <source>Rename a filter</source>
        <translation type="unfinished">Zmień nazwę filtrowi</translation>
    </message>
    <message>
        <source>Rename a favorite</source>
        <translation type="unfinished">Zmień nazwę ulubionemu</translation>
    </message>
</context>
</TS>
