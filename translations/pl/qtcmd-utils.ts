<!DOCTYPE TS><TS>
<context>
    <name>Filters</name>
    <message>
        <source>files and directories</source>
        <translation>pliki i katalogi</translation>
    </message>
    <message>
        <source>directories only</source>
        <translation>tylko katalogi</translation>
    </message>
    <message>
        <source>files only</source>
        <translation>tylko pliki</translation>
    </message>
    <message>
        <source>links</source>
        <translation>linki</translation>
    </message>
    <message>
        <source>hanging links</source>
        <translation>wiszące linki</translation>
    </message>
</context>
<context>
    <name>SystemInfo</name>
    <message>
        <source>Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>This file not exists</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Cannot open this file</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
