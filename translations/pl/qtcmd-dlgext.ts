<!DOCTYPE TS><TS>
<context>
    <name>ControlPanel</name>
    <message>
        <source>ControlPanel</source>
        <translation type="obsolete">ControlPanel</translation>
    </message>
    <message>
        <source>&amp;Play</source>
        <translation type="obsolete">&amp;Graj</translation>
    </message>
    <message>
        <source>Alt+P</source>
        <translation type="obsolete">Alt+G</translation>
    </message>
    <message>
        <source>&amp;Stop</source>
        <translation type="obsolete">&amp;Zatrzymaj</translation>
    </message>
    <message>
        <source>Alt+S</source>
        <translation type="obsolete">Alt+Z</translation>
    </message>
    <message>
        <source>Pa&amp;use</source>
        <translation type="obsolete">&amp;Wstrzymaj</translation>
    </message>
    <message>
        <source>Alt+U</source>
        <translation type="obsolete">Alt+W</translation>
    </message>
    <message>
        <source>&gt;</source>
        <translation type="obsolete">&gt;</translation>
    </message>
    <message>
        <source>&lt;&lt;</source>
        <translation type="obsolete">&lt;&lt;</translation>
    </message>
    <message>
        <source>&gt;&gt;</source>
        <translation type="obsolete">&gt;&gt;</translation>
    </message>
    <message>
        <source>&lt;</source>
        <translation type="obsolete">&lt;</translation>
    </message>
</context>
<context>
    <name>ControlPanelDialog</name>
    <message>
        <source>ControlPanelDialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Play</source>
        <translation type="unfinished">&amp;Graj</translation>
    </message>
    <message>
        <source>Alt+P</source>
        <translation type="unfinished">Alt+G</translation>
    </message>
    <message>
        <source>&amp;Stop</source>
        <translation type="unfinished">&amp;Zatrzymaj</translation>
    </message>
    <message>
        <source>Alt+S</source>
        <translation type="unfinished">Alt+Z</translation>
    </message>
    <message>
        <source>Pa&amp;use</source>
        <translation type="unfinished">&amp;Wstrzymaj</translation>
    </message>
    <message>
        <source>Alt+U</source>
        <translation type="unfinished">Alt+W</translation>
    </message>
    <message>
        <source>&gt;</source>
        <translation type="unfinished">&gt;</translation>
    </message>
    <message>
        <source>&lt;&lt;</source>
        <translation type="unfinished">&lt;&lt;</translation>
    </message>
    <message>
        <source>&gt;&gt;</source>
        <translation type="unfinished">&gt;&gt;</translation>
    </message>
    <message>
        <source>&lt;</source>
        <translation type="unfinished">&lt;</translation>
    </message>
</context>
<context>
    <name>KeyShortcutsPage</name>
    <message>
        <source>KeyShortcutsApp</source>
        <translation type="obsolete">KeyShortcutsApp</translation>
    </message>
    <message>
        <source>&amp;Default settings</source>
        <translation type="obsolete">&amp;Domyślne ustawienia</translation>
    </message>
    <message>
        <source>Alt+D</source>
        <translation type="obsolete">Alt+D</translation>
    </message>
    <message>
        <source>Action</source>
        <translation type="obsolete">Działanie</translation>
    </message>
    <message>
        <source>Shortcut</source>
        <translation type="obsolete">Skrót klawiszowy</translation>
    </message>
    <message>
        <source>Own</source>
        <translation type="obsolete">Własny</translation>
    </message>
    <message>
        <source>Default</source>
        <translation type="obsolete">Domyślny</translation>
    </message>
    <message>
        <source>None</source>
        <translation type="obsolete">Brak</translation>
    </message>
    <message>
        <source>Default shortcut</source>
        <translation type="obsolete">Domyślny skrót</translation>
    </message>
    <message>
        <source>&amp;Edit current shortcut</source>
        <translation type="obsolete">&amp;Edycja bieżącego linku</translation>
    </message>
    <message>
        <source>Alt+E</source>
        <translation type="obsolete">Alt+E</translation>
    </message>
    <message>
        <source>The &apos;%1&apos; key combination has already been allocated to the action</source>
        <translation type="obsolete">Skrót klawiszowy &apos;%1&apos; jest już powiązany z działaniem</translation>
    </message>
    <message>
        <source>Do you want to reassign it from that action to the current one ?</source>
        <translation>Czy chcesz zrezygnować z niego i zastąpić go bieżącym ?</translation>
    </message>
    <message>
        <source>Set a new key shortcut</source>
        <translation type="obsolete">Ustaw nowy skrót klawiszowy</translation>
    </message>
    <message>
        <source>The action &apos;%1&apos; has not been allocated to the key shortcut</source>
        <translation>Działanie &apos;%1&apos; zostało powiązane ze skrótem klawiszowym</translation>
    </message>
    <message>
        <source>Key shortcut conflict</source>
        <translation>Konflikt skr�u klawiszowego</translation>
    </message>
    <message>
        <source>Do you want to allocate it now ?</source>
        <translation>Czy chcesz powiązać go teraz ?</translation>
    </message>
    <message>
        <source>Sea&amp;rch an action</source>
        <translation type="obsolete">&amp;Wyszukaj działanie</translation>
    </message>
    <message>
        <source>Alt+H</source>
        <translation type="obsolete">Alt+S</translation>
    </message>
    <message>
        <source>Ctrl+S</source>
        <translation>Ctrl+S</translation>
    </message>
    <message>
        <source>The &apos;%1&apos; key combination has already been allocated to the sAction</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Set a new key sShortcut</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>KeyShortcutsPageDialog</name>
    <message>
        <source>KeyShortcutsApp</source>
        <translation type="unfinished">KeyShortcutsApp</translation>
    </message>
    <message>
        <source>&amp;Default settings</source>
        <translation type="unfinished">&amp;Domyślne ustawienia</translation>
    </message>
    <message>
        <source>Alt+D</source>
        <translation type="unfinished">Alt+D</translation>
    </message>
    <message>
        <source>Action</source>
        <translation type="unfinished">Działanie</translation>
    </message>
    <message>
        <source>Shortcut</source>
        <translation type="unfinished">Skrót klawiszowy</translation>
    </message>
    <message>
        <source>Own</source>
        <translation type="unfinished">Własny</translation>
    </message>
    <message>
        <source>Default</source>
        <translation type="unfinished">Domyślny</translation>
    </message>
    <message>
        <source>None</source>
        <translation type="unfinished">Brak</translation>
    </message>
    <message>
        <source>Default shortcut</source>
        <translation type="unfinished">Domyślny skrót</translation>
    </message>
    <message>
        <source>&amp;Edit current shortcut</source>
        <translation type="unfinished">&amp;Edycja bieżącego linku</translation>
    </message>
    <message>
        <source>Alt+E</source>
        <translation type="unfinished">Alt+E</translation>
    </message>
    <message>
        <source>Sea&amp;rch an action</source>
        <translation type="unfinished">&amp;Wyszukaj działanie</translation>
    </message>
    <message>
        <source>Alt+H</source>
        <translation type="unfinished">Alt+S</translation>
    </message>
</context>
<context>
    <name>MessageBox</name>
    <message>
        <source>MessageBox</source>
        <translation type="obsolete">MessageBox</translation>
    </message>
    <message>
        <source>&amp;Yes</source>
        <translation>&amp;Tak</translation>
    </message>
    <message>
        <source>Alt+Y</source>
        <translation type="obsolete">Alt+T</translation>
    </message>
    <message>
        <source>&amp;No</source>
        <translation>&amp;Nie</translation>
    </message>
    <message>
        <source>Alt+N</source>
        <translation type="obsolete">Alt+N</translation>
    </message>
    <message>
        <source>&amp;All</source>
        <translation type="obsolete">&amp;Wszystkie</translation>
    </message>
    <message>
        <source>Alt+A</source>
        <translation type="obsolete">Alt+W</translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation>&amp;Anuluj</translation>
    </message>
    <message>
        <source>Alt+C</source>
        <translation type="obsolete">Alt+A</translation>
    </message>
    <message>
        <source>&amp;Rename</source>
        <translation type="obsolete">&amp;Zmień nazwę</translation>
    </message>
    <message>
        <source>Alt+R</source>
        <translation type="obsolete">Alt+Z</translation>
    </message>
    <message>
        <source>&amp;OK</source>
        <translation>&amp;OK</translation>
    </message>
    <message>
        <source>Error</source>
        <translation>Błąd</translation>
    </message>
</context>
<context>
    <name>MessageBoxDialog</name>
    <message>
        <source>MessageBoxDialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Yes</source>
        <translation type="unfinished">&amp;Tak</translation>
    </message>
    <message>
        <source>Alt+Y</source>
        <translation type="unfinished">Alt+T</translation>
    </message>
    <message>
        <source>&amp;No</source>
        <translation type="unfinished">&amp;Nie</translation>
    </message>
    <message>
        <source>Alt+N</source>
        <translation type="unfinished">Alt+N</translation>
    </message>
    <message>
        <source>&amp;All</source>
        <translation type="unfinished">&amp;Wszystkie</translation>
    </message>
    <message>
        <source>Alt+A</source>
        <translation type="unfinished">Alt+W</translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation type="unfinished">&amp;Anuluj</translation>
    </message>
    <message>
        <source>Alt+C</source>
        <translation type="unfinished">Alt+A</translation>
    </message>
    <message>
        <source>&amp;Rename</source>
        <translation type="unfinished">&amp;Zmień nazwę</translation>
    </message>
    <message>
        <source>Alt+R</source>
        <translation type="unfinished">Alt+Z</translation>
    </message>
</context>
<context>
    <name>ToolBarPage</name>
    <message>
        <source>ToolBarPage</source>
        <translation type="obsolete">ToolBarPage</translation>
    </message>
    <message>
        <source>&amp;Delete</source>
        <translation type="obsolete">&amp;Usuń</translation>
    </message>
    <message>
        <source>Alt+D</source>
        <translation type="obsolete">Alt+D</translation>
    </message>
    <message>
        <source>Bars :</source>
        <translation type="obsolete">Paski :</translation>
    </message>
    <message>
        <source>&gt;&gt;</source>
        <translation type="obsolete">&gt;&gt;</translation>
    </message>
    <message>
        <source>Ins</source>
        <translation type="obsolete">Wst</translation>
    </message>
    <message>
        <source>/\</source>
        <translation type="obsolete">/\</translation>
    </message>
    <message>
        <source>\/</source>
        <translation type="obsolete">\/</translation>
    </message>
    <message>
        <source>Actions :</source>
        <translation type="obsolete">Dostępne działania :</translation>
    </message>
    <message>
        <source>Change kind of list view</source>
        <translation>Zmień rodzaj widoku listy</translation>
    </message>
    <message>
        <source>FTP connection manager</source>
        <translation>Menadżer połączeń FTP</translation>
    </message>
    <message>
        <source>Reread current directory</source>
        <translation>Odczytaj ponownie bieżący katalog</translation>
    </message>
    <message>
        <source>Proportions of panels</source>
        <translation>Proporcje paneli</translation>
    </message>
    <message>
        <source>Show storage devices list</source>
        <translation>Pokaż listę urządzeń masowych</translation>
    </message>
    <message>
        <source>Show free space for current disk</source>
        <translation>Pokaż ilość wolnego miejsca na dysku</translation>
    </message>
    <message>
        <source>Find file</source>
        <translation>Znajdź plik</translation>
    </message>
    <message>
        <source>Show favorites</source>
        <translation>Pokaż ulubione</translation>
    </message>
    <message>
        <source>Quick get out from file system</source>
        <translation>Wyskocz z systemu plików</translation>
    </message>
    <message>
        <source>Configure </source>
        <translation>Konfiguracja</translation>
    </message>
    <message>
        <source>global icons</source>
        <translation>ikony globalne</translation>
    </message>
    <message>
        <source>Open in new window</source>
        <translation>Otwórz w nowym oknie</translation>
    </message>
    <message>
        <source>Open</source>
        <translation>Otwórz</translation>
    </message>
    <message>
        <source>Save</source>
        <translation>Zapisz</translation>
    </message>
    <message>
        <source>Print</source>
        <translation>Drukuj</translation>
    </message>
    <message>
        <source>Reload</source>
        <translation>Odświerz</translation>
    </message>
    <message>
        <source>Undo</source>
        <translation>Cofnij</translation>
    </message>
    <message>
        <source>Redo</source>
        <translation>Przywróć</translation>
    </message>
    <message>
        <source>Cut</source>
        <translation>Wytnij</translation>
    </message>
    <message>
        <source>Copy</source>
        <translation>Kopiuj</translation>
    </message>
    <message>
        <source>Paste</source>
        <translation>Wklej</translation>
    </message>
    <message>
        <source>Find</source>
        <translation>Znajdź</translation>
    </message>
    <message>
        <source>Find next</source>
        <translation>Znajdź następne</translation>
    </message>
    <message>
        <source>Replace</source>
        <translation>Zamień</translation>
    </message>
    <message>
        <source>Go to line</source>
        <translation>Przejdź do linii</translation>
    </message>
    <message>
        <source>Change kind of view to</source>
        <translation>Zmień rodzaj widoku na</translation>
    </message>
    <message>
        <source>Zoom in</source>
        <translation>Powiększ</translation>
    </message>
    <message>
        <source>Zoom out</source>
        <translation>Pomniejsz</translation>
    </message>
    <message>
        <source>Configure view</source>
        <translation>Konfiguracja podglądu</translation>
    </message>
    <message>
        <source>Show/Hide side list</source>
        <translation>Pokaż/schowaj listę</translation>
    </message>
    <message>
        <source>text view</source>
        <translation>Podgląd tekstowy</translation>
    </message>
    <message>
        <source>image view</source>
        <translation>Podgląd graficzny</translation>
    </message>
    <message>
        <source>sound view</source>
        <translation>Podgląd dźwiękowy</translation>
    </message>
    <message>
        <source>video view</source>
        <translation>Podgląd wideo</translation>
    </message>
</context>
<context>
    <name>ToolBarPageDialog</name>
    <message>
        <source>ToolBarPageDialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Delete</source>
        <translation type="unfinished">&amp;Usuń</translation>
    </message>
    <message>
        <source>Alt+D</source>
        <translation type="unfinished">Alt+D</translation>
    </message>
    <message>
        <source>Bars :</source>
        <translation type="unfinished">Paski :</translation>
    </message>
    <message>
        <source>&gt;&gt;</source>
        <translation type="unfinished">&gt;&gt;</translation>
    </message>
    <message>
        <source>Ins</source>
        <translation type="unfinished">Wst</translation>
    </message>
    <message>
        <source>/\</source>
        <translation type="unfinished">/\</translation>
    </message>
    <message>
        <source>\/</source>
        <translation type="unfinished">\/</translation>
    </message>
    <message>
        <source>Actions :</source>
        <translation type="unfinished">Dostępne działania :</translation>
    </message>
</context>
</TS>
