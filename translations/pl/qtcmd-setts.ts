<!DOCTYPE TS><TS>
<context>
    <name>ArchivesPage</name>
    <message>
        <source>Archives</source>
        <translation>Archiwa</translation>
    </message>
    <message>
        <source>&amp;Default settings</source>
        <translation>&amp;Domyślne ustawienia</translation>
    </message>
    <message>
        <source>Alt+D</source>
        <translation>Alt+D</translation>
    </message>
    <message>
        <source>&amp;Work directory :</source>
        <translation>Katalog &amp;roboczy :</translation>
    </message>
    <message>
        <source>Detect &amp;supported archivers</source>
        <translation>&amp;Wykryj obsługiwane archiwizery</translation>
    </message>
    <message>
        <source>available</source>
        <translation>dostępne</translation>
    </message>
    <message>
        <source>name</source>
        <translation>nazwa</translation>
    </message>
    <message>
        <source>compression level</source>
        <translation>poziom kompresji</translation>
    </message>
    <message>
        <source>full name</source>
        <translation>pełna nazwa</translation>
    </message>
    <message>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <source>Select full name for archiver</source>
        <translation>Wybierz pełną nazwę archiwizera</translation>
    </message>
    <message>
        <source>DefaultColors</source>
        <translation>DomyślnyKolor</translation>
    </message>
    <message>
        <source>store</source>
        <translation>bez kompresji</translation>
    </message>
    <message>
        <source>fast</source>
        <translation>szybka</translation>
    </message>
    <message>
        <source>best</source>
        <translation>najlepsza</translation>
    </message>
    <message>
        <source>the fastest</source>
        <translation>najszybsza</translation>
    </message>
    <message>
        <source>default</source>
        <translation>domyślny</translation>
    </message>
    <message>
        <source>good</source>
        <translation>dobra</translation>
    </message>
    <message>
        <source>the best</source>
        <translation>najlepsza</translation>
    </message>
    <message>
        <source>Create solid archive</source>
        <translation>Utwórz jednolite archiwum</translation>
    </message>
    <message>
        <source>Create SFX archive</source>
        <translation>Utwórz samorozpakowujące się archiwum</translation>
    </message>
    <message>
        <source>Save symbolic links</source>
        <translation>Zachowaj linki symboliczne</translation>
    </message>
    <message>
        <source>Save file owner and group</source>
        <translation>Zachowaj właściciela i grupę</translation>
    </message>
    <message>
        <source>Multimedia compression</source>
        <translation>Kompresja multimedialna</translation>
    </message>
    <message>
        <source>Overwrite existing files</source>
        <translation>Nadpisuj istniejące pliki</translation>
    </message>
    <message>
        <source>Do not save file owner and group and file times</source>
        <translation>Nie zachowywuj właściciela, grupy i czasu pliku</translation>
    </message>
    <message>
        <source>&amp;OK</source>
        <translation>&amp;OK</translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation>&amp;Anuluj</translation>
    </message>
    <message>
        <source>Dictionary size</source>
        <translation>Rozmiar słownika</translation>
    </message>
    <message>
        <source>settings</source>
        <translation>ustawienia</translation>
    </message>
    <message>
        <source>All files</source>
        <translation>Wszystkie pliki</translation>
    </message>
    <message>
        <source>Select archiver file</source>
        <translation>Wybierz archiwizera</translation>
    </message>
    <message>
        <source>This file not exists !</source>
        <translation>Ten plik nie istnieje !</translation>
    </message>
    <message>
        <source>This file isn&apos;t executable !</source>
        <translation>Ten plik nie jest uruchamialny !</translation>
    </message>
    <message>
        <source>A&amp;dditional settings</source>
        <translation>&amp;Ustawienia dodatkowe</translation>
    </message>
</context>
<context>
    <name>ColorsPage</name>
    <message>
        <source>Colors</source>
        <translation>Kolory</translation>
    </message>
    <message>
        <source>Selected item color</source>
        <translation>Kolor zaznaczonego elementu</translation>
    </message>
    <message>
        <source>Item color under cursor</source>
        <translation>Kolor elementu pod kursorem</translation>
    </message>
    <message>
        <source>Preview</source>
        <translation>Podgląd</translation>
    </message>
    <message>
        <source>Background color of selected item</source>
        <translation>Kolor tła zaznaczonego elementu</translation>
    </message>
    <message>
        <source>Selected item color under cursor</source>
        <translation>Kolor zaznaczonego elementu pod kursorem</translation>
    </message>
    <message>
        <source>Background color of selected item under cursor</source>
        <translation>Kolor tła zaznaczonego elementu pod kursorem</translation>
    </message>
    <message>
        <source>Second background color</source>
        <translation>Drugi kolor tła</translation>
    </message>
    <message>
        <source>(First) Background color</source>
        <translation>(Pierwszy) Kolor tła</translation>
    </message>
    <message>
        <source>Show &amp;grid</source>
        <translation>Pokaż &amp;siatkę</translation>
    </message>
    <message>
        <source>Alt+G</source>
        <translation>Alt+G</translation>
    </message>
    <message>
        <source>Full cursor color</source>
        <translation>Kolor pełnego kursora</translation>
    </message>
    <message>
        <source>Item color</source>
        <translation>Kolor elementu</translation>
    </message>
    <message>
        <source>Grid color</source>
        <translation>Kolor siatki</translation>
    </message>
    <message>
        <source>&amp;New</source>
        <translation type="obsolete">&amp;Nowy</translation>
    </message>
    <message>
        <source>Alt+N</source>
        <translation>Alt+N</translation>
    </message>
    <message>
        <source>&amp;Delete current</source>
        <translation type="obsolete">&amp;Usuń bieżący</translation>
    </message>
    <message>
        <source>Alt+D</source>
        <translation>Alt+D</translation>
    </message>
    <message>
        <source>&amp;Save</source>
        <translation>&amp;Zapisz</translation>
    </message>
    <message>
        <source>Alt+S</source>
        <translation>Alt+S</translation>
    </message>
    <message>
        <source>&amp;Full cursor</source>
        <translation>&amp;Pełny kursor</translation>
    </message>
    <message>
        <source>Alt+F</source>
        <translation></translation>
    </message>
    <message>
        <source>&amp;Two-colors cursor</source>
        <translation>&amp;Dwu-kolorowy kursor</translation>
    </message>
    <message>
        <source>Alt+T</source>
        <translation>Alt+T</translation>
    </message>
    <message>
        <source>Inside frame cursor color</source>
        <translation>Kolor wewnętrznej ramki kursora</translation>
    </message>
    <message>
        <source>Outside frame cursor color</source>
        <translation>Kolor zewnętrznej ramki kursora</translation>
    </message>
    <message>
        <source>Scheme :</source>
        <translation>Schemat :</translation>
    </message>
    <message>
        <source>Text</source>
        <translation>Tekst</translation>
    </message>
    <message>
        <source>TextUnderCursor</source>
        <translation>TekstPodKursorem</translation>
    </message>
    <message>
        <source>Selection</source>
        <translation>Zaznaczenie</translation>
    </message>
    <message>
        <source>SelUnderCursor</source>
        <translation>ZaznPodKursorem</translation>
    </message>
    <message>
        <source>New scheme name</source>
        <translation>Nazwa nowego schematu</translation>
    </message>
    <message>
        <source>Please to give a name:</source>
        <translation>Proszę podać nazwę:</translation>
    </message>
    <message>
        <source>newScheme</source>
        <translation>nowySchemat</translation>
    </message>
    <message>
        <source>&amp;New scheme</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Delete current scheme</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ColumnsPage</name>
    <message>
        <source>Columns</source>
        <translation>Kolumny</translation>
    </message>
    <message>
        <source>&amp;Default settings</source>
        <translation>&amp;Domyślne ustawienia</translation>
    </message>
    <message>
        <source>Alt+D</source>
        <translation>Alt+D</translation>
    </message>
    <message>
        <source>Sizes synchronization</source>
        <translation>Synchronizacja rozmiarów</translation>
    </message>
    <message>
        <source>Name</source>
        <translation>Nazwa</translation>
    </message>
    <message>
        <source>Size</source>
        <translation>Rozmiar</translation>
    </message>
    <message>
        <source>Time</source>
        <translation>Czas</translation>
    </message>
    <message>
        <source>Permission</source>
        <translation>Prawa dostępu</translation>
    </message>
    <message>
        <source>Owner</source>
        <translation>Właściciel</translation>
    </message>
    <message>
        <source>Group</source>
        <translation>Grupa</translation>
    </message>
    <message>
        <source>Show</source>
        <translation>Pokaż</translation>
    </message>
</context>
<context>
    <name>FontAppPage</name>
    <message>
        <source>FontAppPage</source>
        <translation>FontAppPage</translation>
    </message>
    <message>
        <source>Test font</source>
        <translation>Test fontu</translation>
    </message>
    <message>
        <source>&amp;Italic</source>
        <translation>Po&amp;chylony</translation>
    </message>
    <message>
        <source>Alt+I</source>
        <translation>Alt+I</translation>
    </message>
    <message>
        <source>&amp;Bold</source>
        <translation>Po&amp;grubiony</translation>
    </message>
    <message>
        <source>Alt+B</source>
        <translation>Alt+B</translation>
    </message>
    <message>
        <source>Size :</source>
        <translation>Rozmiar :</translation>
    </message>
    <message>
        <source>Set &amp;default font</source>
        <translation>Ustaw &amp;domyślny font</translation>
    </message>
    <message>
        <source>Alt+D</source>
        <translation>Alt+D</translation>
    </message>
    <message>
        <source>Helvetica, 10</source>
        <translation>Helvetica, 10</translation>
    </message>
    <message>
        <source>Set &amp;original font</source>
        <translation>Ustaw &amp;pierwotny font</translation>
    </message>
    <message>
        <source>Alt+O</source>
        <translation>Alt+O</translation>
    </message>
</context>
<context>
    <name>FtpPage</name>
    <message>
        <source>Ftp</source>
        <translation>Ftp</translation>
    </message>
    <message>
        <source>&amp;Stand by connection</source>
        <translation>&amp;Utrzymywać połączenie</translation>
    </message>
    <message>
        <source>Alt+S</source>
        <translation>Alt+S</translation>
    </message>
    <message>
        <source>&amp;Default settings</source>
        <translation>&amp;Domyślne ustawienia</translation>
    </message>
    <message>
        <source>Alt+D</source>
        <translation>Alt+D</translation>
    </message>
    <message>
        <source>Number of time to retry if FTP server busy</source>
        <translation>Liczba powtórek jeśli serwer FTP jest zajęty</translation>
    </message>
</context>
<context>
    <name>LfsPage</name>
    <message>
        <source>Lfs</source>
        <translation>Lfs</translation>
    </message>
    <message>
        <source>Always remove to the &amp;trash</source>
        <translation>Zawsze usuwać do &amp;kosza</translation>
    </message>
    <message>
        <source>Alt+T</source>
        <translation>Alt+T</translation>
    </message>
    <message>
        <source>Always &amp;weigh files before operation</source>
        <translation>Zawsze &amp;ważyć przed operacją</translation>
    </message>
    <message>
        <source>Alt+W</source>
        <translation>Alt+W</translation>
    </message>
    <message>
        <source>&amp;Default settings</source>
        <translation>&amp;Domyślne ustawienia</translation>
    </message>
    <message>
        <source>Alt+D</source>
        <translation>Alt+D</translation>
    </message>
    <message>
        <source>Trash :</source>
        <translation>Kosz :</translation>
    </message>
</context>
<context>
    <name>OtherFSPage</name>
    <message>
        <source>OtherFSPage</source>
        <translation>OtherFSPage</translation>
    </message>
    <message>
        <source>Always &amp;overwrite</source>
        <translation>Zawsze &amp;nadpisywać</translation>
    </message>
    <message>
        <source>Alt+O</source>
        <translation>Alt+O</translation>
    </message>
    <message>
        <source>Always save &amp;permission</source>
        <translation>Zawsze zachowywać &amp;ustawienia</translation>
    </message>
    <message>
        <source>Alt+P</source>
        <translation>Alt+P</translation>
    </message>
    <message>
        <source>&amp;Default settings</source>
        <translation>&amp;Domyślne ustawienia</translation>
    </message>
    <message>
        <source>Alt+D</source>
        <translation>Alt+D</translation>
    </message>
    <message>
        <source>Always ask before deleting</source>
        <translation>Zawsze pytać przed usuwaniem</translation>
    </message>
    <message>
        <source>Always &amp;get in to directory after creation it</source>
        <translation>Zawsze &amp;wchodzić do katalogu po jego utworzeniu</translation>
    </message>
    <message>
        <source>Alt+G</source>
        <translation>Alt+G</translation>
    </message>
    <message>
        <source>Always close progress dialog after operation finished</source>
        <translation>Zawsze zamykać okno postępu po zakończeniu operacji</translation>
    </message>
</context>
<context>
    <name>OtherListSettingsPage</name>
    <message>
        <source>OtherListSettings</source>
        <translation>OtherListSettings</translation>
    </message>
    <message>
        <source>File size format</source>
        <translation>Format rozmiaru pliku</translation>
    </message>
    <message>
        <source>1234567</source>
        <translation>1234567</translation>
    </message>
    <message>
        <source>1 234 567 B</source>
        <translation>1 234 567 B</translation>
    </message>
    <message>
        <source>1.18 MB</source>
        <translation>1.18 MB</translation>
    </message>
    <message>
        <source>&amp;Default settings</source>
        <translation>&amp;Domyślne ustawienia</translation>
    </message>
    <message>
        <source>Alt+D</source>
        <translation>Alt+D</translation>
    </message>
    <message>
        <source>Show the &amp;icons</source>
        <translation>Pokaż &amp;ikony</translation>
    </message>
    <message>
        <source>Alt+I</source>
        <translation>Alt+I</translation>
    </message>
    <message>
        <source>Cursor always &amp;visible</source>
        <translation>Kursor zawsze &amp;widoczny</translation>
    </message>
    <message>
        <source>Alt+C</source>
        <translation>Alt+C</translation>
    </message>
    <message>
        <source>Select &amp;files only (applied to group selection)</source>
        <translation>Zaznaczaj tylko &amp;pliki (zastosowanie przy zaznaczeniach grupowych)</translation>
    </message>
    <message>
        <source>Alt+S</source>
        <translation>Alt+S</translation>
    </message>
    <message>
        <source>Default filter :</source>
        <translation>Domyślny filtr :</translation>
    </message>
    <message>
        <source>Use filter for filtered too</source>
        <translation>Użyj filtru do już przefiltrowanych</translation>
    </message>
    <message>
        <source>Show &amp;hidden</source>
        <translation>Pokaż &amp;ukryte</translation>
    </message>
    <message>
        <source>Alt+H</source>
        <translation>Alt+H</translation>
    </message>
</context>
<context>
    <name>SettingsDialog</name>
    <message>
        <source>FilesPanel - SettingsDialog</source>
        <translation>FilesPanel - SettingsDialog</translation>
    </message>
    <message>
        <source>List</source>
        <translation>Lista</translation>
    </message>
    <message>
        <source>Fonts</source>
        <translation>Fonty</translation>
    </message>
    <message>
        <source>Colors</source>
        <translation>Kolory</translation>
    </message>
    <message>
        <source>Columns</source>
        <translation>Kolumny</translation>
    </message>
    <message>
        <source>Other settings</source>
        <translation>Pozostałe ustawienia</translation>
    </message>
    <message>
        <source>Files Systems</source>
        <translation>System plików</translation>
    </message>
    <message>
        <source>Local</source>
        <translation>Lokalny</translation>
    </message>
    <message>
        <source>FTP</source>
        <translation>FTP</translation>
    </message>
    <message>
        <source>Archives</source>
        <translation>Archiwa</translation>
    </message>
    <message>
        <source>Other</source>
        <translation>Inne</translation>
    </message>
    <message>
        <source>Key shortcuts</source>
        <translation>Skróty klawiszowe</translation>
    </message>
    <message>
        <source>Tool bar</source>
        <translation>Paski narzędziowe</translation>
    </message>
    <message>
        <source>Alt+K</source>
        <translation>Alt+K</translation>
    </message>
    <message>
        <source>&amp;Save</source>
        <translation>&amp;Zapisz</translation>
    </message>
    <message>
        <source>Alt+S</source>
        <translation>Alt+S</translation>
    </message>
    <message>
        <source>&amp;Apply</source>
        <translation>Za&amp;stosuj</translation>
    </message>
    <message>
        <source>Alt+A</source>
        <translation>Alt+A</translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation>&amp;Anuluj</translation>
    </message>
    <message>
        <source>Alt+C</source>
        <translation>Alt+C</translation>
    </message>
    <message>
        <source>QtCommander - settings</source>
        <translation>Ustawienia QtCommander-a</translation>
    </message>
    <message>
        <source>O&amp;K</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
