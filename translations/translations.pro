# Plik utworzony przez menad?era projekt�w kdevelopa
# ------------------------------------------- 
# Podkatalog wzgl?dem g?�wnego katalogu projektu: ./translation
# Cel to projekt z podkatalogami 

CONFIG += warn_on 
TEMPLATE = subdirs 
TRANS_DIRS = pl

trans_update.target=tr_update
trans_update.commands = @find ../ -type f -name "*.pro" -exec $$QT_BIN_DIR/lupdate {} 2>/dev/null \;

#mktrans.target = mk_trans
mktrans.target = all
mktrans.commands = @for i in $$TRANS_DIRS ; do $$QT_BIN_DIR/lrelease \$$i/*.ts ; done

trans_install.target = install_subdirs
trans_install.depends = mktrans 
trans_install.commands = @for i in $$TRANS_DIRS ; do \
    $(MKDIR) $(INSTALL_ROOT)$$PREFIX/share/locale/\$$i/LC_MESSAGES ; \
    cp -f \$$i/*.qm $(INSTALL_ROOT)$$PREFIX/share/locale/\$$i/LC_MESSAGES ; \
done 

trans_uninstall.target = uninstall_subdirs
trans_uninstall.commands = @for i in $$TRANS_DIRS ; do \
    ls \$$i/*.qm | sed -e "s|\$$i/||" | xargs -i $(DEL_FILE) $(INSTALL_ROOT)$$PREFIX/share/locale/\$$i/LC_MESSAGES/{} ; \
done 

trans_distclean.target = distclean
trans_distclean.depends = clean

trans_clean.target = clean
#trans_clean.commands = @ls */*.qm | xargs $(DEL_FILE)
trans_clean.commands = @$(DEL_FILE) */*.qm

QMAKE_EXTRA_UNIX_TARGETS += trans_update mktrans trans_install trans_uninstall trans_distclean trans_clean
