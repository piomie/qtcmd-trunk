/* XPM */
static const char *treeview[]={
"16 16 6 1",
"# c #000000",
"d c #0000c0",
"b c #585858",
"a c #c3c3c3",
"c c #ff0000",
". c #ffffff",
".#............ab",
".#............ab",
".#######......ab",
".#............ab",
".#cccc........ab",
".#............ab",
".#ccccccccc...ab",
".#............ab",
".#ccccc.......ab",
".#............ab",
".#ddddddd.....ab",
".#............ab",
".####.........ab",
".#............ab",
".######.......ab",
"..............ab"};

/* XPM */
static char const *ftptool[]={
"16 16 6 1",
"b c #000000",
"# c #0000c0",
"c c #585858",
"d c #c00000",
". c #c0ffc0",
"a c #ffc0c0",
"................",
"................",
"#######.........",
"#######.........",
"##.........aaaa.",
"##.....bc..dddda",
"#####..bc..ddddd",
"####cccbcccdd..d",
"##..bbbbbbcdd..d",
"##...ccbcc.dd..d",
"##....cb...ddddd",
"##....cb..bdddda",
"##....cbbbcddaa.",
".......ccc.dd...",
"...........dd...",
"...........dd..."};

/* XPM */
static const char *reread[] = {
"32 32 8 1",
"       c None",
".      c #AAAAAAAAAAAA",
"X      c #000000000000",
"o      c #555555555555",
"O      c #00000000FFFF",
"+      c #000000005555",
"@      c #CCCC98982020",
"#      c #444432320A0A",
"                                ",
"                                ",
"                                ",
"                                ",
"                                ",
"                                ",
"        ...XXXXXXXXXXo          ",
"       .XOOOOOOOOOXo. .o.       ",
"      .XOOOOOOOO+o.  .X@X.      ",
"     .XOOOOO+XXo.   .X@@@X.     ",
"     oOOOOOX.      .X@@@@@X.    ",
"    .+OOOO+.      .X@@@@@@@X.   ",
"    oOOOOOX      .X@@@@@@@@@X.  ",
"    XOOOOOX      oXX#@@@@@#XXo  ",
"    XOOOOOX        .X@@@@@X.    ",
"    XOOOOOX         X@@@@@X     ",
"    XOOOOOX         X@@@@@X     ",
"   .XOOOOOX.        X@@@@@X     ",
" oXX+OOOOO+XXo      X@@@@@X     ",
" .XOOOOOOOOOX.      X@@@@@o     ",
"  .XOOOOOOOX.      .#@@@@#.     ",
"   .XOOOOOX.     ..X@@@@@o      ",
"    .XOOOX.   .oXX#@@@@@X.      ",
"     .XOX.  .o#@@@@@@@@X.       ",
"      .X. .oX@@@@@@@@@X.        ",
"         oXXXXXXXXXX..          ",
"                                ",
"                                ",
"                                ",
"                                ",
"                                ",
"                                "};

/* XPM */
static const char *proportions[]={
"16 16 6 1",
"c c #0000c0",
". c #303030",
"a c #808080",
"b c #a0a0a0",
"d c #ff0000",
"# c #ffffff",
".####.ab########",
"......ab########",
".####.ab########",
"######ab.b####b.",
"#ccc##ab........",
"######ab.b####b.",
"######ab########",
"######ab#ddd#dd#",
"#ddd##ab########",
"######ab########",
".###.#ab##cc#cc#",
".....#ab########",
".###.#ab.a####b.",
"######ab........",
"######ab.a####b.",
"######ab########"};

/* XPM */
static const char *freespace[]={
"16 16 6 1",
". c None",
"c c #0000ff",
"b c #303030",
"d c #58a8ff",
"a c #c00000",
"# c #e08d8d",
"....#aaaaa#.....",
"..#aaaaaaaaa#...",
".#aaaaaaaaaaa#..",
".abbbababaaaaa..",
"#aabaaabaaaaaa#.",
"aabaaababaaaaaa.",
"aaaaaaaaaaacccc.",
"aaaaaaaaccccccc.",
"aaaaaaacccccccc.",
"aaaaaaaccbcbccc.",
"#aaaaaacccbcccd.",
".aaaaaaccbcbcc..",
".#aaaaaccccccd..",
"..#aaaacccccd...",
"....#aacccd.....",
"................"};

/* XPM */
static const char *quickgetout[]={
"32 32 7 1",
"a c #0000ff",
"b c #303030",
"e c #58a8ff",
"# c #a8dcff",
"c c #c00000",
"d c #ffff00",
". c #ffffff",
"................................",
"................................",
"................................",
"............#aaaaaaa#...........",
"...........#aaaaaaaaa#..........",
"..........#aa#.....#aa#.........",
".........#aa#.......#aa#........",
".........aaa.........#aa........",
"........#aa#..........aa........",
"........aa#.........aaaaaaa.....",
"........aa...........aaaaa......",
".......#aa............aaa.......",
".......aa#.............a........",
".......aa.......................",
".......aa.......................",
".......aa.......................",
"bbbbbbbaabbbbbbbbbbbbbbbbbbbbbbb",
"bbbbbbbaabbbbbbbcccccccccccccccc",
"dddddddaaddddddbcccccccccccccccc",
"ddddddeaaddddddbcccccccccccccccc",
"ddddddaaeddddddbcccccccccccccccc",
"ddddddaadddddddbcccccccccccccccc",
"ddddddaadddddddbcccccccccccccccc",
"ddddddaadddddddbcccccccccccccccc",
"ddddddaadddddddbcccccccccccccccc",
"dddddeaadddddddbcccccccccccccccc",
"dddddaaedddddddbcccccccccccccccc",
"dddddaaddddddddbcccccccccccccccc",
"dddeaaaddddddddbcccccccccccccccc",
"eaaaaedddddddddbcccccccccccccccc",
"aaaaaedddddddddbcccccccccccccccc",
"aaaaeddddddddddbcccccccccccccccc"};

/* XPM */
static char *findfile[]={
"20 20 12 1",
"g c None",
". c None",
"b c #000000",
"e c #303030",
"i c #404000",
"# c #585858",
"j c #808080",
"a c #a0a0a0",
"d c #a8dcff",
"f c #c00000",
"h c #ffffc0",
"c c #ffffff",
"..............######",
"...abbbbba....#cccc#",
"..abdddddba...#ceec#",
".abdffffddbagg#cccc#",
".bddfhhfdddbgg#ceec#",
".bddfhhfdddbgg#cccc#",
".bddfhhfdddbgg#cccc#",
".bddfhhfdddbgg######",
".bddffffdddbgg......",
".abddddddde#gg......",
"..abdddddb#i#g...jjj",
"...abbbbb#iii#...jcc",
"...ggggggg#iii#..jce",
"..######ggg#iii#.jcc",
"..#cccc#...j#iii#jcc",
"..#ceec#...jc#iii#jj",
"..#cccc#...jec#iii#.",
"..#ceec#...jccj#iii#",
"..#cccc#...jjjj.#ii#",
"..######.........##g"};
