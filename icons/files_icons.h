/***************************************************************************
                          icons.h  -  description
                             -------------------
    begin                : Sun Apr 30 2000
    copyright            : (C) 2000 by Piotr Mierzwiński
    email                : peterm@go2.pl
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

// ---there is 19 icons: bmp, c, cpp, exe,  gif, h, html, jpg, lib, package, png, txt, unknown, xpm, xbm

static const char *bmp[]={
"16 16 5 1",
". c None",
"b c #c3c3c3",
"# c #000000",
"c c #0000ff",
"a c #ffffff",
"....##########..",
"....#aaaaaaaa#b.",
"....#aaaaaaaa###",
"....#aaaaaaaaaa#",
"################",
"#cccccccccccccc#",
"#caaccaccacaacc#",
"#cacacaaaacacac#",
"#caaccaccacaacc#",
"#cacacaccacaccc#",
"#caaccaccacaccc#",
"#cccccccccccccc#",
"################",
"....#aaaaaaaaaa#",
"....############",
"................"};

static const char *c[]={
"16 16 6 1",
". c None",
"# c #808080",
"b c #c3c3c3",
"d c #000080",
"c c #000000",
"a c #ffffff",
".#########......",
".#aaaaaaab#.....",
".#aaaaaaaba#....",
".#aaaaaaacccc...",
".#aaddddaaabc...",
".#addaaddaabc...",
".#addaaddaabc...",
".#addaaaaaabc...",
".#addaaaaaabc...",
".#addaaddaabc...",
".#addaaddaabc...",
".#aaddddaaabc...",
".#aaaaaaaaabc...",
".#bbbbbbbbbbc...",
".cccccccccccc...",
"................"};

static const char *cpp[]={
"16 16 6 1",
". c None",
"# c #808080",
"b c #c3c3c3",
"d c #000080",
"c c #000000",
"a c #ffffff",
".#########......",
".#aaaaaaab#.....",
".#aaaaaaaba#....",
".#aaaaaaacccc...",
".#aaaaaaaaabc...",
".#adddaaadabc...",
".#ddaddadddbc...",
".#ddaddaadabc...",
".#ddaaaaaaabc...",
".#ddaddaadabc...",
".#ddaddadddbc...",
".#adddaaadabc...",
".#aaaaaaaaabc...",
".#bbbbbbbbbbc...",
".cccccccccccc...",
"................"};

static const char *exe[]={
"16 16 6 1",
". c None",
"# c #808080",
"a c #c3c3c3",
"b c #000000",
"c c #0000c0",
"d c #ffffff",
"................",
"................",
"################",
"#aaaaaaaaaaaaa#b",
"#acccccccccccc#b",
"#accccccdbdbdb#b",
"#a#############b",
"#adddddddddddd#b",
"#adddddddddddd#b",
"#adddddddddddd#b",
"#adddddddddddd#b",
"#adddddddddddd#b",
"#adddddddddddd#b",
"###############b",
"bbbbbbbbbbbbbbbb",
"................"};

static const char *gif[]={
"16 16 5 1",
". c None",
"c c #008000",
"b c #c3c3c3",
"# c #000000",
"a c #ffffff",
"....##########..",
"....#aaaaaaaa#b.",
"....#aaaaaaaa###",
"....#aaaaaaaaaa#",
"################",
"#cccccccccccccc#",
"#ccaaacaaacaaaa#",
"#cacccccaccaccc#",
"#cacacccaccaaac#",
"#caccaccaccaccc#",
"#ccaaccaaacaccc#",
"#cccccccccccccc#",
"################",
"....#aaaaaaaaaa#",
"....############",
"................"};

static const char *h[]={
"16 16 6 1",
". c None",
"# c #808080",
"b c #c3c3c3",
"c c #000080",
"d c #000000",
"a c #ffffff",
".#########......",
".#aaaaaaab#.....",
".#aaaaaaaba#....",
".#accaaaadddd...",
".#accaaaaaabd...",
".#accaaaaaabd...",
".#acccccaaabd...",
".#accaaccaabd...",
".#accaaccaabd...",
".#accaaccaabd...",
".#accaaccaabd...",
".#accaaccaabd...",
".#aaaaaaaaabd...",
".#bbbbbbbbbbd...",
".dddddddddddd...",
"................"};

/*
static const char *hide[]={
"16 16 6 1",
". c None",
"# c #808080",
"b c #c3c3c3",
"d c #000000",
"a c #ffffff",
"c c #ff0000",
".#########......",
".#aaaaaaab#.....",
".#aaaaaaaba#....",
".#aaaccaadddd...",
".#aaaccaaaabd...",
".#aaaccaaaabd...",
".#aaaccaaaabd...",
".#aaaccaaaabd...",
".#aaaccaaaabd...",
".#aaaaaaaaabd...",
".#aaaccaaaabd...",
".#aaaccaaaabd...",
".#aaaaaaaaabd...",
".#bbbbbbbbbbd...",
".dddddddddddd...",
"................"};
*/

static const char *html[] = {
"16 16 13 1",
" 	c None",
".	c #000080800000",
"X	c #000040400000",
"o	c #000000008080",
"O	c #0000FFFF0000",
"+	c #0000C0C00000",
"@	c #80808080FFFF",
"#	c #C0C0FFFFC0C0",
"$	c #00000000FFFF",
"%	c #303030303030",
"&	c #000000000000",
"*	c #000080808080",
"=	c #000040404040",
"                ",
"      .Xoo      ",
"    ..O++@oo    ",
"   .O#+O@$@oo   ",
"  .O###O+O$o$%  ",
"  .#O#O+O@@$o%  ",
" o@+#O+O@@@@$o& ",
" o@+O+@O@@@$oo& ",
" o@@O*@@@@@@$o& ",
" o$@@OO@+@@$oo& ",
"  o$@@@O+.$oo%  ",
"  o$$@+...=oo&  ",
"   &o$o..=Xo&   ",
"    &%oo=X%&    ",
"      &&&%      ",
"                "};


static const char *jpg[]={
"16 16 5 1",
". c None",
"b c #c3c3c3",
"c c #808000",
"# c #000000",
"a c #ffffff",
"....##########..",
"....#aaaaaaaa#b.",
"....#aaaaaaaa###",
"....#aaaaaaaaaa#",
"################",
"#cccccccccccccc#",
"#caaaacaacccaaa#",
"#ccacccacacaccc#",
"#ccacccaaccacaa#",
"#ccacccacccacca#",
"#caacccaccccaaa#",
"#cccccccccccccc#",
"################",
"....#aaaaaaaaaa#",
"....############",
"................"};

static const char *lib[]={
"16 16 7 1",
". c None",
"# c #808080",
"b c #c3c3c3",
"d c #808000",
"c c #000000",
"e c #ffff00",
"a c #ffffff",
".#########......",
".#aaaaaaab#.....",
".#aaaaaaaba#....",
".#aaaaaaacccc...",
".#aaaaaaaaabc...",
".#addcaaaaabc...",
".#acedcaaaabc...",
".#dec##caaabc...",
".#acbca#caabc...",
".#aa#ac##cabc...",
".#aaacbbcaabc...",
".#aaaaccaaabc...",
".#aaaaaaaaabc...",
".#bbbbbbbbbbc...",
".cccccccccccc...",
"................"};

static const char *package[]={
"16 16 4 1",
". c None",
"# c #808080",
"b c #9db9c8",
"a c #ffff00",
"................",
"....############",
"...#aaaaa#aaaa##",
"..############a#",
".#aaaaa#aaaa##a#",
"#aaaaa#aaaa#a#a#",
"###########aa###",
"#aaaa#aaaa#aa#a#",
"#aaaa#aaaa#a##a#",
"#aaaa#aaaa##a#a#",
"###########aa##.",
"#aaaa#aaaa#aa#..",
"#aaaa#aaaa#a#...",
"#aaaa#aaaa##....",
"###########.....",
"................"};

static const char *png[]={
"16 16 5 1",
". c None",
"b c #c3c3c3",
"# c #000000",
"c c #0000ff",
"a c #ffffff",
"....#########...",
"....#aaaaaaa#bb.",
"....#aaaaaaa####",
"....#aaaaaaaaaa#",
"################",
"#cccccccccccccc#",
"#caaccaccaccaaa#",
"#cacacaacacaccc#",
"#caaccacaacacaa#",
"#cacccaccacacca#",
"#cacccaccaccaac#",
"#cccccccccccccc#",
"################",
"....#aaaaaaaaaa#",
"....############",
"................"};

static const char *txt[]={
"16 16 5 1",
". c None",
"b c #808080",
"c c #c3c3c3",
"# c #000000",
"a c #ffffff",
"..#.#.#.#.#.....",
".#ababababa#....",
".b#a#a#a#a#a#...",
".baaaaaaaaac#...",
".baaaaaaaaac#...",
".ba###a###ac#...",
".baaaaaaaaac#...",
".ba#a###a#ac#...",
".baaaaaaaaac#...",
".ba####a##ac#...",
".baaaaaaaaac#...",
".ba##a####ac#...",
".baaaaaaaaac#...",
".bcccccccccc#...",
".############...",
"................"};

static const char *unknown[]={
"16 16 5 1",
". c None",
"# c #808080",
"b c #c3c3c3",
"c c #000000",
"a c #ffffff",
".#########......",
".#aaaaaaab#.....",
".#aaaaaaaba#....",
".#aaaaaaacccc...",
".#aaaaaaaaabc...",
".#aaaaaaaaabc...",
".#aaaaaaaaabc...",
".#aaaaaaaaabc...",
".#aaaaaaaaabc...",
".#aaaaaaaaabc...",
".#aaaaaaaaabc...",
".#aaaaaaaaabc...",
".#aaaaaaaaabc...",
".#bbbbbbbbbbc...",
".cccccccccccc...",
"................"};

static const char *xbm[]={
"16 16 6 1",
". c None",
"b c #c3c3c3",
"c c #00ffff",
"# c #000000",
"d c #0000ff",
"a c #ffffff",
"....##########..",
"....#aaaaaaaa#b.",
"....#aaaaaaaa###",
"....#aaaaaaaaaa#",
"################",
"#cdcdcdcdcdcdcd#",
"#dcdcd##c##dcdc#",
"#cdcdcd#d#dcdcd#",
"#dcdcdcd#dcdcdc#",
"#cdcdcd#d#dcdcd#",
"#dcdcd##c##dcdc#",
"#cdcdcdcdcdcdcd#",
"################",
"....#aaaaaaaaaa#",
"....############",
"................"};

static const char *xpm[]={
"16 16 6 1",
". c None",
"c c #808080",
"b c #c3c3c3",
"d c #ff00ff",
"# c #000000",
"a c #ffffff",
"....##########..",
"....#aaaaaaaa#b.",
"....#aaaaaaaa###",
"....#aaaaaaaaaa#",
"################",
"#cdcdcdcdcdcdcd#",
"#dadcacaadcacda#",
"#caaaadadadaaaa#",
"#ddaadcaadcaaaa#",
"#caaaadadcdadca#",
"#daddacacdcacda#",
"#cdcdcdcdcdcdcd#",
"################",
"....#aaaaaaaaaa#",
"....############",
"................"};
