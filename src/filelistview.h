/***************************************************************************
                          filelistview  -  description
                             -------------------
    begin                : nie kwi 23 2006
    copyright            : (c) 2006 by Mariusz Borowski (based on ListView from qtcmd-0.7)
    email                : mariusz@nes.pl

    copyright            : See COPYING file that comes with this project

 $Id$
 ***************************************************************************/

#ifndef FILELISTVIEW_H
#define FILELISTVIEW_H

#include <q3listview.h>

/**
    @author Mariusz Borowski <mariusz@nes.pl>
*/
class FileListView : public Q3ListView
{
public:
    FileListView();
    virtual ~FileListView();
};

#endif
