/***************************************************************************
                         panel.cpp  -  description
                            -------------------
   begin                : nie lut 20 2005
   copyright            : (C) 2005 by Piotr Mierzwinski
   email                : peterm@o2.pl

   copyright            : See COPYING file that comes with this project

$Id$
***************************************************************************/

#include "panel.h"

Panel::Panel( uint nWidth, uint nHeight, QWidget *pParent, const char * sz_pName )
	: QWidget(pParent, sz_pName)
{
	qDebug("Panel::Panel()");
	//qDebug("Panel, resize to w=%d, h=%d (only if not 0)", nWidth, nHeight );
	if (nWidth != 0 && nHeight != 0)
		resize( nWidth, nHeight );
}

