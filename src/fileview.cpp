/***************************************************************************
                          fileview.cpp  -  description
                             -------------------
    begin                : Sun Oct 27 2002
    copyright            : (C) 2002 by Piotr Mierzwiński
    email                : peterm@go2.pl

    Copyright: See COPYING file that comes with this project

 $Id$
 ***************************************************************************/

#include <QList>
#include <QFileDialog>
#include <QTimer>
#include <QLibrary>
#include <QSplitter>
#include <QStringList>
#include <QFileInfo>
#include <QSettings>
#include <QApplication>
#include <QKeyEvent>
#include <QPixmap>
#include <QCloseEvent>
#include <QDesktopWidget>
#include <QMenuBar>
#include <QIcon>
#include <QActionGroup>
#include <QToolButton>
#include <QKeySequence>
#include <QToolBar>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QtAlgorithms>
#include <QTreeWidget>

#include "vfs.h"
#include "panel.h"
#include "fileview.h"
#include "statusbar.h"
#include "fsmanager.h" // for call static fun.'kindOfFile()'
#include "messagebox.h"

#include "settings.xpm"
#include "closeview.xpm"
#include "mainview_icons.h"

#include "plugins/view/text/textview.h"
#include "plugins/view/settings/textviewsettings.h" // a structure contains the settings
#include "pluginmanager.h"


static QList<int> sSW;  // nWidth of 2 spliter obj.
static bool sCanShowView;


FileView::FileView( const QStringList & slFilesList, uint nCurrentItem, bool bEditMode, QWidget *pParent, const char *sz_pName )
	: Panel(0,0, pParent, sz_pName)
	, m_pView(NULL), m_pStatus(NULL), m_pSideListView(NULL), m_pSettingsWidget(NULL), m_pFilesAssociation(NULL)
	, m_eKindOfFile(UNKNOWNfile), m_eKindOfView(View::UNKNOWNview), m_bInitEditMode(bEditMode)
{
	qDebug("FileView::FileView");
	if ( slFilesList.isEmpty() )
		return;

    m_sCurrentDirPath = FileInfoExt::filePath( slFilesList[nCurrentItem] );
    m_bViewInWindow = (pParent) ? FALSE : TRUE;
    m_pKeyShortcuts = NULL;
    setName( sz_pName );

    m_pViewParent = (m_bViewInWindow) ? this : pParent;

    initView(); // inits: nWidth, nHeight, m_nInitWidth and whether to show toolBar, by data from the config file, m_sPluginsPath

    /////////////////// Layouts ///////////////////////////////

    QVBoxLayout *mainLayout = new QVBoxLayout;
    mainLayout->setSpacing(1);
    mainLayout->setContentsMargins(1,1,1,1);
    //add menu
    m_pMenuBar = new QMenuBar(m_pViewParent);
    mainLayout->setMenuBar(m_pMenuBar);
    //add toolbar
    m_pToolBar = new QToolBar("toolBar", m_pViewParent);
    mainLayout->addWidget(m_pToolBar);
    //add splitter
    m_pSplitter = new QSplitter( Qt::Horizontal, m_pViewParent );
    m_pSplitter->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    mainLayout->addWidget(m_pSplitter);
    //add statusbar
    m_pStatus = new StatusBar( m_pViewParent );
    mainLayout->addWidget(m_pStatus);
    this->setLayout(mainLayout);

    /////////////////// Layouts ///////////////////////////////

	m_pSplitter->setOpaqueResize( TRUE ); // during size change still shows contains of panels
	m_pSplitter->setHandleWidth( 3 ); // default is 6
	m_pSplitter->show();

    m_pSideListView = new QTreeWidget(m_pSplitter);
    QStringList labels;
    labels << tr("Name") << tr("Location");
    m_pSideListView->setHeaderLabels(labels);
    m_pSideListView->setIndentation(0);

    connect( m_pSideListView, SIGNAL(itemActivated(QTreeWidgetItem*,int)), this, SLOT(slotOpenCurrentItem(QTreeWidgetItem*,int)));

	m_pStatus->setSqueezingText( TRUE );

	insertFilesList( slFilesList ); // into the side list
    QList<QTreeWidgetItem*> items = m_pSideListView->findItems(FileInfoExt::fileName(slFilesList[nCurrentItem]), Qt::MatchExactly);
    Q_ASSERT(items.count() == 1);
    m_pSideListView->setCurrentItem(items.at(0));

	resize( m_nInitWidth, m_nInitHeigth );

	m_nSideListWidth = width()*24/100; // 24% of total nWidth
	uint initListViewWidth = (slFilesList.count() > 1) ? m_nSideListWidth : 0;
	sSW.clear();
	sSW << initListViewWidth << width()-initListViewWidth;
	m_bShowTheFilesList = (initListViewWidth) ? TRUE : FALSE;
//FIXME    m_pSideListView->header()->resizeSection( 0, m_nSideListWidth );

	initMenuBarAndToolBar();
	initKeyShortcuts();

	m_sInitFileName = slFilesList[nCurrentItem];
	sCanShowView = TRUE;
}


FileView::~FileView()
{
	qDebug("FileView, destruktor");
	if ( m_pSettingsWidget ) { delete m_pSettingsWidget; m_pSettingsWidget = 0; }
	removeView(); // remove view and unload plugin*/
}


void FileView::initView()
{
	QSettings settings;

	if ( m_bViewInWindow ) {
		QString sSize = settings.readEntry( "/qtcmd/FileView/Size" );
		m_nInitWidth  = 600; // default value
		m_nInitHeigth = 400; // default value
		bool bOK;

		if ( ! sSize.isEmpty() ) {
			int nWidth = (sSize.left( sSize.find(',') )).toInt(&bOK);
			if ( bOK )
				if ( nWidth >= 200 && nWidth <= QApplication::desktop()->width() )
					m_nInitWidth = nWidth;

			int nHeight = (sSize.right( sSize.length()-sSize.findRev(',')-1 )).toInt(&bOK);
			if ( bOK )
				if ( nHeight >= 100 && nHeight <= QApplication::desktop()->height() )
					m_nInitHeigth = nHeight;
		}
	}
	else {
		m_nInitHeigth = m_pViewParent->height();
		m_nInitWidth  = m_pViewParent->width();
	}

	m_bShowMenuBar = settings.readBoolEntry( "/qtcmd/FileView/ShowMenuBar", TRUE );
	m_bShowToolBar = settings.readBoolEntry( "/qtcmd/FileView/ShowToolBar", TRUE );

	// --- init plugins settings
	m_slPluginsNameList.clear();
    m_slPluginsNameList << "textview" << "imageview" << "soundview" << "videoview" << "binaryview" << "";
}


void FileView::initKeyShortcuts()
{
	m_pKeyShortcuts = new KeyShortcuts();

	m_pKeyShortcuts->append( Help_VIEW, tr("Help"), Qt::Key_F1 );
	m_pKeyShortcuts->append( Reload_VIEW, tr("Reload"), Qt::Key_F5 );
	m_pKeyShortcuts->append( Open_VIEW, tr("Open"), Qt::CTRL+Qt::Key_O );
	m_pKeyShortcuts->append( Save_VIEW, tr("Save"), Qt::CTRL+Qt::Key_S );
	m_pKeyShortcuts->append( SaveAs_VIEW, tr("Save as"), Qt::ALT+Qt::CTRL+Qt::Key_S );
	m_pKeyShortcuts->append( OpenInNewWindow_VIEW, tr("Open in new window"), Qt::CTRL+Qt::Key_N );
	m_pKeyShortcuts->append( ShowHideMenuBar_VIEW, tr("Show/hide menu bar"), Qt::CTRL+Qt::Key_M );
	m_pKeyShortcuts->append( ShowHideToolBar_VIEW, tr("Show/hide tool bar"), Qt::CTRL+Qt::Key_T );
	m_pKeyShortcuts->append( ShowHideSideList_VIEW, tr("Show/hide side list"), Qt::CTRL+Qt::Key_L );
	m_pKeyShortcuts->append( ToggleToRawMode_VIEW, tr("Toggle to Raw mode"), Qt::ALT+Qt::CTRL+Qt::Key_W );
	m_pKeyShortcuts->append( ToggleToEditMode_VIEW, tr("Toggle to Edit mode"), Qt::Key_F4 );
	m_pKeyShortcuts->append( ToggleToRenderMode_VIEW, tr("Toggle to Render mode"), Qt::ALT+Qt::CTRL+Qt::Key_R );
	m_pKeyShortcuts->append( ToggleToHexMode_VIEW, tr("Toggle to Hex mode"), Qt::ALT+Qt::CTRL+Qt::Key_H );
	m_pKeyShortcuts->append( ShowSettings_VIEW, tr("Show settings"), Qt::CTRL+Qt::SHIFT+Qt::Key_S );
	m_pKeyShortcuts->append( Quit_VIEW, tr("Quit"), Qt::CTRL+Qt::Key_Q );

	m_pKeyShortcuts->updateEntryList( "/qtcmd/FileViewShortcuts/" ); // update from config file
	slotApplyKeyShortcuts( m_pKeyShortcuts ); // for actions in current window
}


void FileView::initMenuBarAndToolBar()
{
	// --- inits main menu
    // File menu
    m_pFileMenu = m_pMenuBar->addMenu(tr("&File"));
    m_pNewA = m_pFileMenu->addAction(QPixmap(filenew), tr("Open in a &new window"), this, SLOT(slotNew()), Qt::CTRL+Qt::Key_N);
    m_pOpenA = m_pFileMenu->addAction(QPixmap(fileopen), tr("&Open"), this, SLOT(slotOpen()), Qt::CTRL+Qt::Key_O);
    m_pRecentFilesMenu = m_pFileMenu->addMenu(tr("Open &recent files"));
    m_pSaveA = m_pFileMenu->addAction(QPixmap(filesave), tr("&Save"), this, SLOT(slotSave()), Qt::CTRL+Qt::Key_S);
    m_pSaveAsA = m_pFileMenu->addAction(tr("Save &as"), this, SLOT(slotSaveAs()), Qt::ALT+Qt::CTRL+Qt::Key_S);
    m_pFileMenu->addSeparator();
    m_pReloadA = m_pFileMenu->addAction(QPixmap(filereload), tr("&Reload"), this, SLOT(slotReload()), Qt::Key_F5);
    QMenu *pSideListMenu = m_pFileMenu->addMenu(tr("Side files &list"));
    (void) pSideListMenu->addAction(tr("&Load old files list"),    this, SLOT(slotLoadFilesList()));
    (void) pSideListMenu->addAction(tr("Save current files list"), this, SLOT(slotSaveFilesList()));
    m_pQuitA = m_pFileMenu->addAction(tr("&Quit"), this, SLOT(close()), Qt::CTRL+Qt::Key_Q);

    connect( m_pRecentFilesMenu, SIGNAL(triggered(QAction*)), this, SLOT(slotItemOfRecentFilesMenuActivated(QAction*)) );

    // Edit menu
    m_pEditMenu = m_pMenuBar->addMenu(tr("&Edit"));

    // View menu
    m_pViewMenu = m_pMenuBar->addMenu(tr("&View"));
    m_pModeOfViewMenu = m_pViewMenu->addMenu(tr("Kind of view"));

    QActionGroup *actGrp = new QActionGroup(this);
    actGrp->setExclusive(true);

    m_pRawModeA = actGrp->addAction(tr("Ra&w"));
    m_pRawModeA->setCheckable(true);
    m_pRawModeA->setShortcut(Qt::ALT + Qt::CTRL + Qt::Key_W);

    m_pRenderModeA = actGrp->addAction(tr("&Render"));
    m_pRenderModeA->setShortcut(Qt::ALT + Qt::CTRL + Qt::Key_R);
    m_pRenderModeA->setCheckable(true);

    m_pHexModeA = actGrp->addAction(tr("&Hex"));
    m_pHexModeA->setShortcut(Qt::ALT + Qt::CTRL + Qt::Key_H);
    m_pHexModeA->setCheckable(true);

    m_pEditModeA = actGrp->addAction(tr("&Edit"));
    m_pEditModeA->setShortcut(Qt::Key_F4);
    m_pEditModeA->setCheckable(true);

    connect( m_pModeOfViewMenu, SIGNAL(triggered(QAction*)), this, SLOT(slotChangeModeOfView(QAction*)) );

    m_pModeOfViewMenu->addAction(m_pRawModeA);
    m_pModeOfViewMenu->addAction(m_pRenderModeA);
    m_pModeOfViewMenu->addAction(m_pHexModeA);
    m_pModeOfViewMenu->addAction(m_pEditModeA);

    m_pViewMenu->addSeparator();
    m_pSideListA = m_pViewMenu->addAction(QPixmap(sidelist), tr("Show/Hide side &list"), this, SLOT(slotShowTheFilesList()), Qt::CTRL+Qt::Key_L);
    m_pSideListA->setCheckable(true);
    m_pSideListA->setChecked(m_bShowTheFilesList);
    m_pMenuBarA  = m_pViewMenu->addAction(tr("Show/Hide &menu bar"), this, SLOT(slotShowHideMenuBar()), Qt::CTRL+Qt::Key_M);
    m_pMenuBarA->setCheckable(true);
    m_pToolBarA  = m_pViewMenu->addAction(tr("Show/Hide &tool bar"), this, SLOT(slotShowHideToolBar()), Qt::CTRL+Qt::Key_T);
    m_pToolBarA->setCheckable(true);

    // Options menu
    m_pOptionsMenu = m_pMenuBar->addMenu(tr("&Options"));
    m_pConfigureA = m_pOptionsMenu->addAction(QPixmap(settings), tr("&Configure view"), this, SLOT(slotShowSettings()), Qt::CTRL+Qt::SHIFT+Qt::Key_S);
    m_pSaveSettingsA = m_pOptionsMenu->addAction(tr("Save settings"), this, SLOT(slotSaveSettings()));

	// --- init ToolBar
    m_pToolBar->addAction(m_pNewA);
    m_pOpenBtn = new QToolButton(m_pToolBar);
    m_pOpenBtn->setPopupMode(QToolButton::MenuButtonPopup);
    m_pOpenBtn->setMenu(m_pRecentFilesMenu);
    m_pOpenBtn->setDefaultAction(m_pOpenA);
    m_pToolBar->addWidget(m_pOpenBtn);

    m_pToolBar->addAction(m_pSaveA);
    m_pToolBar->addAction(m_pReloadA);

    QToolButton *viewBtn = new QToolButton;
    viewBtn->setPopupMode(QToolButton::MenuButtonPopup);
    viewBtn->setIcon(QPixmap(viewmode));
    viewBtn->setText(tr("Change kind of view to")+"...");
    viewBtn->setMenu(m_pModeOfViewMenu);
    m_pToolBar->addWidget(viewBtn);

    m_pToolBar->addAction(m_pConfigureA);
    m_pToolBar->addAction(m_pSideListA);

    m_pAddonToolBar = new QToolBar("AddonToolBar", m_pViewParent);
    m_pToolBar->addWidget(m_pAddonToolBar);

    // map actions
    m_actionsModes[m_pRawModeA] = View::RAWmode;
    m_actionsModes[m_pRenderModeA] = View::RENDERmode;
    m_actionsModes[m_pHexModeA] = View::HEXmode;
    m_actionsModes[m_pEditModeA] = View::EDITmode;

	if ( ! m_bViewInWindow ) {
        QAction *closeA = m_pToolBar->addAction(QPixmap(closeview), tr("Close view"), this, SIGNAL(signalClose()));
        closeA->setShortcut(Qt::Key_Escape);
    }
	slotShowHideMenuBar();
	slotShowHideToolBar();
}


void FileView::updateMenuBar()
{
    // activate an appropriate action
    QMap<QAction*, View::ModeOfView>::iterator it = qFind(m_actionsModes.begin(), m_actionsModes.end(), m_eModeOfView);
    Q_ASSERT(it != m_actionsModes.end());
    it.key()->setChecked(true);

    m_pSaveA->setVisible( (m_eModeOfView == View::EDITmode && m_eKindOfView == View::TEXTview) );
	if ( ! m_bItIsTextFile ) { // binary file
        m_pSaveA->setVisible(false);
        m_pEditModeA->setVisible(false);
	}
	else // it's text file
         m_pEditModeA->setVisible(true);

    m_pRenderModeA->setVisible(true);
	if ( m_eKindOfView == View::BINARYview && (m_eKindOfFile == ARCHIVEfile || m_eKindOfFile == UNKNOWNfile) )
        m_pRenderModeA->setVisible(false);
	if ( m_eKindOfView == View::TEXTview && (m_eKindOfFile == SOURCEfile || m_eKindOfFile == UNKNOWNfile) )
        m_pRenderModeA->setVisible(false);

    m_pSaveA->setEnabled(m_pEditModeA->isChecked()); // enable/disable save
	m_pSideListA->setVisible( (m_pSideListView->topLevelItemCount() > 1) );
}


bool FileView::createView( const QString & sFileName, View::KindOfView _eKindOfView )
{
	// --- detect kind of file and init kind of view
	View::KindOfView eKindOfView = _eKindOfView;

	if ( eKindOfView == View::UNKNOWNview ) { // then detect it
		if ( m_pFilesAssociation ) {
			if ( m_pFilesAssociation->loaded() )
				m_eKindOfFile = m_pFilesAssociation->kindOfFile( sFileName );
			else
				m_eKindOfFile = FileInfoExt::kindOfFile( sFileName ); // FilesAssociation not loaded
		}
		else
			m_eKindOfFile = FileInfoExt::kindOfFile( sFileName ); // FilesAssociation not loaded

		if ( m_eKindOfFile == IMAGEfile )  eKindOfView = View::IMAGEview;
		else
		if ( m_eKindOfFile == SOUNDfile )  eKindOfView = View::SOUNDview;
		else
		if ( m_eKindOfFile == VIDEOfile )  eKindOfView = View::VIDEOview;
		else
		if ( ! m_bItIsTextFile && (m_eKindOfFile == UNKNOWNfile || m_eKindOfFile == ARCHIVEfile) )
			eKindOfView = View::BINARYview;
		else
			eKindOfView = View::TEXTview;
	}
	if ( eKindOfView == m_eKindOfView ) // attempt to change to this same kind of view
		return TRUE;
	else
		removeView();

	qDebug("FileView::createView, init eKindOfView=%d, detectable eKindOfView=%d", eKindOfView, eKindOfView );

	// --- loading a plugin
    if ( ! m_pView ) {
        PluginManager *plugMgr = PluginManager::instance();
        m_pView = plugMgr->createViewPlugin(m_slPluginsNameList[eKindOfView], m_pSplitter);
        if ( ! m_pView ) {
            MessageBox::critical( this, tr("An error occured when loading a %1 plugin: %2").arg(m_slPluginsNameList[eKindOfView]).arg(plugMgr->errorMessage()) );
            QApplication::restoreOverrideCursor(); // FIXME: why we need this ?
            return false;
        }
        m_pSplitter->addWidget(m_pView);
    }

	m_pSplitter->setSizes( sSW );
	m_pSideListView->resize( sSW[0], m_pSplitter->height() );

	// --- initialize menu bar, status bar and tool bar
	m_eKindOfView = eKindOfView;
	qDebug(" '%s' has been created", qPrintable(kindOfViewStr(m_eKindOfView)) );
	m_pView->clear();

	if ( m_pStatus ) {
		m_pStatus->showNormal();
		connect( m_pView, SIGNAL(signalUpdateStatus(const QString & , uint, bool)),
			m_pStatus, SLOT(slotSetMessage(const QString & , uint, bool)) );
	}
	// - update the actions

	if ( m_eKindOfView == View::TEXTview || m_eKindOfView == View::BINARYview )
    {
		if ( m_pView->editMenuBarActions() ) // actions available
            foreach (QAction *action, m_pView->editMenuBarActions()->actions())
                m_pEditMenu->addAction(action);

		if ( m_pView->viewMenuBarActions() )
            foreach (QAction *action, m_pView->viewMenuBarActions()->actions())
                m_pViewMenu->addAction(action);
	}

	m_pAddonToolBar->clear();

	if ( m_pView->viewToolBarActions() ) // actions available
        foreach (QAction *action, m_pView->viewToolBarActions()->actions())
            m_pAddonToolBar->addAction(action);

	if ( m_pView->editToolBarActions() )
        foreach (QAction *action, m_pView->editToolBarActions()->actions())
            m_pAddonToolBar->addAction(action);

	m_pView->setKeyShortcuts( m_pKeyShortcuts );

	return TRUE;
}


void FileView::removeView()
{
	if ( m_pView ) {
		delete m_pView; m_pView = 0;
	}
}


void FileView::insertFilesList( const QStringList & slFilesList )
{
	if ( m_pSideListView ) {
		for ( int i = 0; i < slFilesList.count(); ++i ) {
			QStringList lst;
			lst << FileInfoExt::fileName(slFilesList[i]) << FileInfoExt::filePath(slFilesList[i]);
			m_pSideListView->addTopLevelItem(new QTreeWidgetItem(lst));
		}
	}
}


void FileView::loadingLate( const QString & sFileName )
{
	m_sInitFileName = sFileName;
	slotReadyRead(); // asynchronously loading file with name 'm_sInitFileName'
}


void FileView::slotReadyRead()
{
	qDebug("FileView::slotReadyRead()" );
	m_pStatus->slotSetMessage( tr("Loading file in progress ...") );

	bool bIsLocalFS = (FSManager::kindOfFilesSystem(m_sInitFileName) == VFS::LOCALfs);
	if ( sCanShowView ) { // need for FTP, becouse part of file has been read
		sCanShowView = FALSE;
		if ( m_bViewInWindow )
			show();
		if ( ! bIsLocalFS )
			slotLoadFile(); // preparing view window for show
	}
	if ( bIsLocalFS )
		QTimer::singleShot( 0, this, SLOT(slotLoadFile()) );
	else
		slotReadFile();
}


void FileView::slotLoadFile()
{
	qDebug("FileView::slotLoadFile()");
	m_bDetectIsBinOrTextFile = FALSE;
	m_sCurrentFileName = m_sInitFileName;
	QApplication::setOverrideCursor( Qt::waitCursor );

	bool bIsLocalFS = (FSManager::kindOfFilesSystem(m_sCurrentFileName) == VFS::LOCALfs);
	m_nFileLoadMethod = View::APPEND;

	if ( FileInfoExt::kindOfFile(m_sInitFileName) == IMAGEfile && bIsLocalFS )
		m_nFileLoadMethod = View::ALL;
	m_nBytesRead = (m_nFileLoadMethod == View::ALL) ? -1 : 10240;

	// init read file
	if ( bIsLocalFS )
		QTimer::singleShot( 0, this, SLOT(slotReadFile()) );

	// --- update window's capion
	if ( m_bViewInWindow )
		setCaption( m_sCurrentFileName + " - QtCommander" );
	// --- inserts files name to the recent files list
	if ( m_pRecentFilesMenu ) {
		bool bInsertName = TRUE;
		for( uint i=0; i<m_pRecentFilesMenu->count(); i++ )
			if ( m_pRecentFilesMenu->text(i) == m_sCurrentFileName ) {
				bInsertName = FALSE;
				break;
			}
		if ( bInsertName )
			m_pRecentFilesMenu->insertItem( m_sCurrentFileName, m_pRecentFilesMenu->count() );
	}
}


void FileView::slotReadFile()
{
	emit signalReadFile( m_sInitFileName, m_baBinBuffer, m_nBytesRead, m_nFileLoadMethod );

	if ( m_nBytesRead < 1 && m_pView ) { // m_nBytesRead < 0 when an error occures
		QApplication::restoreOverrideCursor();
		if ( m_nBytesRead == 0 ) // file has been loaded
			m_pView->updateStatus();
		if ( (m_eModeOfView == View::HEXmode || (! m_bItIsTextFile && m_eModeOfView == View::RAWmode)) && m_baBinBuffer.size() > 0 ) // need to move a contents to top of view
			m_pView->updateContents( View::NONE, m_eModeOfView, m_sCurrentFileName, m_baBinBuffer );
		updateMenuBar();
		return;
	}
	if ( m_nBytesRead > 0 )
		m_baBinBuffer.resize( m_nBytesRead+1 );
	else
		m_baBinBuffer.resize( 0 );

	if ( ! m_bDetectIsBinOrTextFile ) { // executed one time
		m_bDetectIsBinOrTextFile = TRUE;
		m_bItIsTextFile = (m_baBinBuffer.count(char(0)) < 2); // whether a file has two or more 0's bytes
		qDebug(" It's text file=%d, try create view", m_bItIsTextFile );
		if ( ! createView(m_sInitFileName) ) {
			close( TRUE );
			return;
		}
		if ( m_bItIsTextFile ) {
			if ( m_bInitEditMode ) {
				m_bInitEditMode = FALSE;
				m_eModeOfView = View::EDITmode;
			}
			else
				m_eModeOfView = (m_eKindOfFile == RENDERfile) ? View::RENDERmode : View::RAWmode;
		} // if ( m_bItIsTextFile )
		else
			m_eModeOfView = ((m_eKindOfFile == IMAGEfile || m_eKindOfFile == SOUNDfile || m_eKindOfFile == VIDEOfile) ? View::RENDERmode : View::HEXmode);

		updateMenuBar();
	} // if ( ! m_bDetectIsBinOrTextFile )
	if ( m_bItIsTextFile && m_nBytesRead > 0 )
		m_baBinBuffer[ m_nBytesRead ] = '\0'; // need to cuts some rubbishes during conversion to local8Bit

	m_pView->updateContents( (View::UpdateMode)m_nFileLoadMethod, m_eModeOfView, m_sCurrentFileName, m_baBinBuffer );

	if ( m_nFileLoadMethod == View::ALL || m_nFileLoadMethod == View::ON_DEMAND ) {
		QApplication::restoreOverrideCursor();
		return;
	}
	// only for HTML files (sets all contents a file - need to fix bug in the HTML rendering)
	if ( m_nFileLoadMethod == View::APPEND && m_nBytesRead < 10240 && m_bItIsTextFile && m_eModeOfView == View::RENDERmode )
		m_pView->updateContents( View::ALL, m_eModeOfView, m_sCurrentFileName, m_baBinBuffer );

	if ( FSManager::kindOfFilesSystem(m_sInitFileName) == VFS::LOCALfs)
		QTimer::singleShot( 0, this, SLOT(slotReadFile()) ); // slotReadFile();

	m_pStatus->slotSetMessage( tr("Loading file in progress ...") );
}


void FileView::slotNew()
{
    QString sFileName = QFileDialog::getOpenFileName(this, tr("Open file into new window")+" - QtCommander", m_sCurrentDirPath);

	if ( sFileName.isEmpty() )
		return;

	QStringList slFilesList;
	slFilesList.append( sFileName );
	FileView *view = new FileView( slFilesList, 0 );
	view->show();
}


void FileView::slotOpen()
{
    QString sFileName = QFileDialog::getOpenFileName(this, tr("Open file")+" ... - QtCommander", m_sCurrentDirPath);

	if ( ! sFileName.isEmpty() ) {
		if ( m_pView ) { // remove old view
			delete m_pView;  m_pView = 0;
			m_eKindOfView = View::UNKNOWNview;
		}
		loadingLate( sFileName ); // asynchronously file loading
	}
	else
		m_pStatus->slotSetMessage( tr("Loading aborted"), 2000 );
}


void FileView::slotSave()
{
	if ( ! m_pView->isModified() || ! m_bItIsTextFile )
		return;

	if ( m_pView->saveToFile(m_sCurrentFileName) ) {
		m_pStatus->slotSetMessage( tr("File has been saved."), 2000 );
		emit signalUpdateItemOnLV( m_sCurrentFileName );
	}
}


void FileView::slotSaveAs()
{
	QString sStartPath = m_sCurrentFileName.left( m_sCurrentFileName.findRev('/')+1 );
    QString sFileName  = QFileDialog::getSaveFileName(this, tr("Save as")+" ... - QtCommander", sStartPath);

	if ( ! sFileName.isEmpty() ) {
		if ( m_bItIsTextFile ) {
			m_pView->saveToFile( sFileName );
			emit signalUpdateItemOnLV( sFileName ); // trzeba wstawic elem.
		}
	}
	else
		m_pStatus->slotSetMessage( tr("Saving aborted"), 2000 );
}


void FileView::slotReload()
{
	if ( m_pView ) {
		delete m_pView;  m_pView = 0; // remove old view
		m_eKindOfView = View::UNKNOWNview;
	}
	loadingLate( m_sCurrentFileName ); // asynchronously file loading
}

void FileView::slotChangeModeOfView(QAction *act)
{
    Q_CHECK_PTR(act);
    m_eModeOfView = m_actionsModes[act];
    qDebug("FileView::slotChangeModeOfView, to %d", m_eModeOfView);

    if ( ! m_bItIsTextFile ) { // it's binary file
        //m_pView->getData( m_baBinBuffer ); // b.wolno dziala (dla HEXmode), lepiej wczytac jeszcze raz
        View::KindOfView eKindOfView = (m_eModeOfView == View::RAWmode || m_eModeOfView == View::HEXmode) ? View::BINARYview : View::UNKNOWNview;
        createView( m_sCurrentFileName, eKindOfView );
        bool bIsLocalFS = (FSManager::kindOfFilesSystem(m_sCurrentFileName) == VFS::LOCALfs);
        m_nFileLoadMethod = View::APPEND;
        if ( m_eKindOfFile == IMAGEfile && m_eKindOfView != View::BINARYview && bIsLocalFS )
            m_nFileLoadMethod = View::ALL;
        m_nBytesRead = (m_nFileLoadMethod == View::ALL) ? -1 : 10240;
        m_pView->clear();
        if ( bIsLocalFS )
            QTimer::singleShot( 0, this, SLOT(slotReadFile()) );
    }
    else { // text's file
        if ( m_eModeOfView == (int)View::HEXmode ) {
            if ( m_eKindOfView == View::TEXTview )
                m_pView->getData( m_baBinBuffer ); // gets (maybe) changed data
            createView( m_sCurrentFileName, View::BINARYview );
        }
        else
            createView( m_sCurrentFileName );

        if ( m_eModeOfView == (int)View::RENDERmode )
            m_pView->clear(); // need to HTML render turn on
        m_pView->updateContents( View::ALL, m_eModeOfView, m_sCurrentFileName, m_baBinBuffer );
    }

    updateMenuBar();
}

void FileView::slotItemOfRecentFilesMenuActivated(QAction *act)
{
    Q_CHECK_PTR(act);
    QString sFileName = act->text();
	if ( sFileName != m_sCurrentFileName ) {
		m_pView->clear();
		loadingLate( sFileName ); // asynchronously file loading
	}
}


void FileView::slotShowHideMenuBar()
{
	if ( ! m_bViewInWindow ) { // embedded view must be without menu bar
        m_pMenuBar->setVisible(false);
		return;
	}

    m_pMenuBarA->setChecked(m_bShowMenuBar);
    m_pMenuBar->setVisible(m_bShowMenuBar);
	m_bShowMenuBar = ! m_bShowMenuBar;
}


void FileView::slotShowHideToolBar()
{
    m_pToolBarA->setChecked(m_bShowToolBar);
    m_pToolBar->setVisible(m_bShowToolBar);
    m_pAddonToolBar->setVisible(m_bShowToolBar);
	m_bShowToolBar = ! m_bShowToolBar;
}


void FileView::slotShowTheFilesList()
{
	if ( m_pSideListView->topLevelItemCount () < 2 )
		return;

	m_bShowTheFilesList = ! m_bShowTheFilesList;
    m_pSideListA->setChecked(m_bShowTheFilesList);
	sSW[0] = (m_bShowTheFilesList) ? m_nSideListWidth : 0;
	m_pSplitter->setSizes( sSW );
}


void FileView::slotSaveSettings()
{
    m_pSaveSettingsA->setChecked(false);

	QString sSize = QString::number(width())+","+QString::number(height());
    QSettings settings;
	settings.writeEntry( "/qtcmd/FileView/Size", sSize );
	settings.writeEntry( "/qtcmd/FileView/ShowMenuBar", ! m_bShowMenuBar );
	settings.writeEntry( "/qtcmd/FileView/ShowToolBar", ! m_bShowToolBar );

	m_pView->slotSaveSettings();
}


void FileView::slotLoadFilesList()
{
	QSettings settings;
	QStringList slFilesList = settings.readListEntry( "/qtcmd/FileView/URLs" );

	if ( slFilesList.isEmpty() )
		return;

	insertFilesList( slFilesList );

	// if the files list is invisible (or only part is visible) then show it
	if ( sSW[0] < width()*24/100 ) {
		m_bShowTheFilesList = FALSE;
		slotShowTheFilesList();
	}

	m_pStatus->slotSetMessage( tr("The files list has been loaded"), 2000 );
}


void FileView::slotSaveFilesList()
{
	QSettings settings;
	QStringList slFilesList = settings.readListEntry( "/qtcmd/FileView/URLs" );

    for (int i = 0; i < m_pSideListView->topLevelItemCount(); ++i) {
        QTreeWidgetItem *item = m_pSideListView->topLevelItem(i);
        Q_CHECK_PTR(item);
        slFilesList.append( item->text(1) + item->text(0) );
    }

	if ( settings.writeEntry("/qtcmd/FileView/URLs", slFilesList) )
		m_pStatus->slotSetMessage( tr("The files list has been saved"), 2000 );
}


void FileView::slotOpenCurrentItem( QTreeWidgetItem *item, int /*column*/ )
{
	m_pView->clear();
	loadingLate( item->text(1) + item->text(0) ); // asynchronously file loading
}


void FileView::slotShowSettings()
{
    if ( ! m_pSettingsWidget ) {
        PluginManager *plugMgr = PluginManager::instance();
        m_pSettingsWidget = plugMgr->createWidgetPlugin("settingsview", this);
        if ( ! m_pSettingsWidget ) {
            MessageBox::critical( this, tr("Error when loading a settings plugin: %1").arg(plugMgr->errorMessage()) );
            return;
        }

        connect( m_pSettingsWidget, SIGNAL(signalApply(const TextViewSettings &)),
         m_pView, SLOT(slotApplyTextViewSettings(const TextViewSettings &)) );
//      connect( m_pSettingsWidget, SIGNAL(signalApply(const ImageViewSettings &)),
//       m_pView, SLOT(slotApplyTextViewSettings(const ImageViewSettings &)) );
//      connect( m_pSettingsWidget, SIGNAL(signalApply(const SoundViewSettings &)),
//       m_pView, SLOT(slotApplyTextViewSettings(const SoundViewSettings &)) );
//      connect( m_pSettingsWidget, SIGNAL(signalApply(const VideoViewSettings &)),
//       m_pView, SLOT(slotApplyTextViewSettings(const VideoViewSettings &)) );
        connect( m_pSettingsWidget, SIGNAL(signalApplyKeyShortcuts(KeyShortcuts *)),
         this, SLOT(slotApplyKeyShortcuts(KeyShortcuts *)) );
        connect( m_pSettingsWidget, SIGNAL(signalSaveSettings()), m_pView, SLOT(slotSaveSettings()) );
        // --- init settings dlg.
        connect( this, SIGNAL(signalSetCurrentKindOfView(View::KindOfView)),
         m_pSettingsWidget, SLOT(slotSetCurrentKindOfView(View::KindOfView)) );
        connect( this, SIGNAL(signalSetKeyShortcuts(KeyShortcuts *)),
         m_pSettingsWidget, SLOT(slotSetKeyShortcuts(KeyShortcuts *)) );
        emit signalSetCurrentKindOfView( m_eKindOfView );
        emit signalSetKeyShortcuts( m_pKeyShortcuts );
    }

    Q_CHECK_PTR(m_pSettingsWidget);
    m_pSettingsWidget->show();
}


void FileView::slotApplyKeyShortcuts( KeyShortcuts * pKeyShortcuts )
{
	if ( ! pKeyShortcuts )
		return;

	m_pNewA->setShortcut( pKeyShortcuts->key(OpenInNewWindow_VIEW) );
	m_pOpenA->setShortcut( pKeyShortcuts->key(Open_VIEW) );
	m_pReloadA->setShortcut( pKeyShortcuts->key(Reload_VIEW) );
	m_pMenuBarA->setShortcut( pKeyShortcuts->key(ShowHideMenuBar_VIEW) );
	m_pToolBarA->setShortcut( pKeyShortcuts->key(ShowHideToolBar_VIEW) );
	m_pSideListA->setShortcut( pKeyShortcuts->key(ShowHideSideList_VIEW) );
	m_pConfigureA->setShortcut( pKeyShortcuts->key(ShowSettings_VIEW) );

    m_pRawModeA->setShortcut( pKeyShortcuts->key(ToggleToRawMode_VIEW) );
    m_pRenderModeA->setShortcut( pKeyShortcuts->key(ToggleToRenderMode_VIEW) );
    m_pHexModeA->setShortcut( pKeyShortcuts->key(ToggleToRenderMode_VIEW) );
    m_pEditModeA->setShortcut( pKeyShortcuts->key(ToggleToEditMode_VIEW) );

	m_pOpenBtn->setTextLabel( tr("Open")+"\t("+m_pKeyShortcuts->keyStr(Open_VIEW)+")" );

    m_pSaveAsA->setShortcut( pKeyShortcuts->key(SaveAs_VIEW) );
    m_pQuitA->setShortcut( pKeyShortcuts->key(Quit_VIEW) );

    if (m_pView)
        m_pView->setKeyShortcuts(pKeyShortcuts);
}

void FileView::initLoadFileToView( FilesAssociation * /*pFA, FilesPanel * pFP*/ ) // nie mozna tu przekazac wsk.do FilesPanel !
{
//	setFilesAssociation( pFA );

// 	connect( this, SIGNAL(signalReadFile(const QString &, QByteArray &, int &, int)),
// 		pFP, SLOT(slotReadFileToView(const QString &, QByteArray &, int &, int)) );

//	slotReadyRead();
}

// FUN.TYMCZASOWA - PRZEZNACZONA TYLKO DO TESTOW
QString FileView::kindOfViewStr( View::KindOfView eKindOfView )
{
	if ( eKindOfView == View::TEXTview )
		return "TEXTview";
	else
	if ( eKindOfView == View::IMAGEview )
		return "IMAGEview";
	else
	if ( eKindOfView == View::SOUNDview )
		return "SOUNDview";
	else
	if ( eKindOfView == View::VIDEOview )
		return "VIDEOview";
	else
	if ( eKindOfView == View::BINARYview )
		return "BINARYview";

	return "Unknown view";
}


void FileView::keyPressEvent( QKeyEvent * pKE )
{
	if ( pKE->key() == Qt::Key_Escape )
		close( TRUE ); // close and delete this
}

/*
	bool eventFilter( QObject * , QEvent * );
bool FileView::eventFilter( QObject * o, QEvent * e )
{
	if ( e->type() == QEvent::KeyPress ) {
		qDebug("FileView::eventFilter, KeyPress");
	if ( o == m_pSideListView )
		if ( e->key() == Key_Delete ) {
			QListViewItem *deletedItem = m_pSideListView->currentItem();
			bool isItemBelow = (bool)deletedItem->itemBelow();
			m_pSideListView->setCurrentItem( isItemBelow ? deletedItem->itemBelow() : deletedItem->itemAbove() );
			m_pSideListView->takeItem( deletedItem );
			return TRUE;
		}
	}
// zmieniac kolor statusu, kiedy okno jest aktywne
// Mozna w klasie SqueezeLabel dac metode (setActive) zmieniajaca kolor tla.

	focusOutEvent( QFocusEvent * );
	focusInEvent( QFocusEvent * );
	QColor mStatusBgColor;
	mStatusBgColor = m_pStatus->paletteBackgroundColor();
		m_pStatus->setPalette( mStatusBgColor );
		m_pStatus->setPalette( QColor( 219, 255, 244 ) ); //  QColor( 221, 255, 227 ); // blado zielony

	return QWidget::eventFilter( o, e );    // standard event processing
}
*/


void FileView::closeEvent( QCloseEvent *pCE )
{
	if ( m_pView ) {
		if ( ! m_pView->isModified() ) {
			pCE->accept();
			delete this; // force call destruktor (for unload a plugin)
			return;
		}
	}
	else {
		pCE->accept();
		delete this; // force call destruktor (for unload a plugin)
		return;
	}

	int nResult = MessageBox::yesNo( this,
		tr("Save file")+" - QtCommander",
		tr("Do you want to save changed document")+"?",
		MessageBox::Yes, TRUE // TRUE - show the Cancel button, too
	);
	switch( nResult )
	{
		case MessageBox::Yes:
			slotSave();
			pCE->accept();
			delete this; // force call destruktor (for unload a plugin)
			break;

		case MessageBox::No:
			pCE->accept();
			delete this; // force call destruktor (for unload a plugin)
			break;

		case MessageBox::All: // 'Cancel' button pressed
		default: // just for sanity
			pCE->ignore();
			break;
	}
}
