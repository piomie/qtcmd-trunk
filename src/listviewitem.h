/***************************************************************************
                          listview.h  -  description
                             -------------------
    begin                : Mon Jul 9 2001
    copyright            : (C) 2001 by Mariusz Borowski (initializer)
                         : (C) 2002,2003 by Piotr Mierzwi�ski (developer)
    emails               : mborowski@silesia.net, peterm@go2.pl

    copyright            : See COPYING file that comes with this project
 ***************************************************************************/

#ifndef _LISTVIEWITEM_H_
#define _LISTVIEWITEM_H_

#include "urlinfoext.h"

#include <q3listview.h>
#include <qtooltip.h>
//Added by qt3to4:
#include <QPixmap>
#include <Q3ValueList>

/**
  *@authors: Mariusz Borowski (init), Piotr Mierzwi�ski (develop)
  */
class ListView;

/**
 @short Klasa dymku dla elementu listy.
 Jest ona wykorzystywana do pokazywania pe�nych nazw przyci�tych kom�rek listy
 oraz dla pokazywania �cie�ek docelowych dla link�w.
 */
class ListViewToolTip //: public QToolTip
{
public:
	/** Konstruktor klasy.\n
	 @param pParent - wska�nik na rodzica,
	 @param lv - wska�nik na list�, dla kt�rej element�w b�dzie pokazywany dymek.\n
	 Ustawiany jest tutaj rozmiar fontu dla dymku.
	 */
	ListViewToolTip( QWidget * /*pParent*/, ListView *lv ) : mView(lv) {}

	/** Metoda wywo�ywana jest tylko wtedy, gdy kursor znajdzie si� na elementem.\n
	 @param pos - pozycja kursora.\n
	 Jest tu pokazywana informacja w postaci dymku, dla przyci�tego tekstu w
	 kom�rce nad kt�r� kursor si� aktualnie znajduj. Je�li plik pod kursorem
	 jest linkiem wtedy pokazywana jest jego �cie�ka docelowa (element listy
	 trzyma tak� informacj�).
	 */
	void maybeTip( const QPoint & pos );

private:
	ListView *mView;

};


/** @short Klasa elementu widoku listy.
 Klasa zawiera metody pomagaj�ce w u�ywaniu jej jako obiektu elementu listy
 plik�w. Oferuje nast�puj�ce w�a�ciwo�ci:
  @li zwraca informacj� czy bie��cy element jest katalogiem czy plikiem;
  @li zwraca absolutn� �cie�k� dla bie��cego elementu (je�li widokiem jest lista
  wtedy wstawiany jest bie��ca �cie�ka);
  @li w��cza/wy��cza pokazywanie rozmiar�w katalog�w
  @li umo�liwia ukrycie i pokazanie elementu
  @li zwraca informacj� typu UrlInfoExt (atrybuty pliku).\n
 Obiekty tej klasy mo�na inicjowa� tylko obiektami klasy @em UrlInfoExt poprzez
 jej konstruktory oraz za pomoc� metod. Klasa zawiera dwie metody rysowania
 kursora na li�cie, mianowicie: jako dwukolorowa ramka oraz wype�niona jednym
 kolorem belka.
*/
class ListViewItem : public Q3ListViewItem
{
public:
	/** Konstruktor klasy.\n
	 Inicjowane s� tu sk�adowe klasy.
	 */
	ListViewItem( ListView *pParent );

	/** Konstruktor klasy.\n
	 Inicjowane s� tu sk�adowe klasy. Konstruktor u�ywany jest przez widok listy,
	 widok drzewa u�ywa go tylko do wstawienia elementu korzenia.
	 */
	ListViewItem( ListView *pParent, const UrlInfoExt & urlInfo );

	/** Konstruktor klasy.\n
	 Inicjowana jest tylko klasa, po kt�rej dziedziczy bie��ca.
	 */
	ListViewItem( ListViewItem *pParent );

	/** Konstruktor klasy.\n
	 Inicjowane s� tu sk�adowe klasy. Konstruktor u�ywany jest przez widok drzewa.
	 */
	ListViewItem( ListViewItem *pParent, const UrlInfoExt & urlInfo );

	/** Przeci��ona metoda klasy QListViewItem, kt�ra jest odpowiedzialna
	 ze rysowanie kom�rki elementu listy oraz t�a dla ka�dej z nich.\n
	 @param p - wska�nik 'rysownika' zwi�zany z otwartym urz�dzeniem rysuj�cym \n
	 @param cg - grupa kolor�w do u�ycia \n
	 @param col - bie��ca kolumna w kt�rej nast�puje rysowanie \n
	 @param width - szeroko�� bie��cej kolumny \n
	 @param alignment - strona do ustawiania zawarto�ci rysowanego elementu (lewa,centrum,prawa)
	 */
	virtual void paintCell(  QPainter *p, const QColorGroup & cg, int col, int widht, int alignment );

	/** Funkcja odpowiedzialna za rysowanie kursora wraz z zawarto�ci�.
	 */
	virtual void paintFocus( QPainter *p, const QColorGroup & cg, const QRect & r );

	/** Funkcja wykonuje korekcj� sortowania.\n
	 Poprawka polega na umieszczaniu katalog�w na pocz�tku, a elementu ".." (je�li
	 jest) na samej g�rze. Korygowane jest te� sortowanie po rozmiarze.
	 */
	virtual QString key( int column, bool ascending ) const;

	/** Zwraca zawarto�� podanej kom�rki elementu.
	 @param column - kolumna z kt�rej nale�y zwr�ci� warto��.\n
	 Przy czy w przypadku kolumny rozmiar�w zale�y to od typu elementu.
	 Je�li bie��cym elementem jest katalog, wtedy zwracana jest, w zale�no�ci od
	 warto�ci sk�adowej @em m_bShowDirSize , dla warto�ci TRUE - rozmiar katalogu
	 (w formacie @em BKBMBGBformat ), dla warto�ci FALSE - ci�g "\<DIR\>". Je�eli
	 elementem jest plik (lub link), wtedy zwracany jest rozmiar (niesformatowany)
	 */
	virtual QString text( int column ) const;

	/** Ustawia w podanym numerze kom�rki odpowiadaj�cej jej podan� warto��.\n
	 @param column - numer kom�rki, w kt�rym nale�y zmieni� warto��,
	 @param str - nowa warto�� dla podanej kom�rki.
	 */
	virtual void setText( int column, const QString & str );

	/** Zwraca informacj� o widoczno�� elementu.\n
	 @return TRUE je�li element jest widoczny, w przeciwnym razie FALSE.
	 */
	virtual bool isVisible() const { return height(); }

	/** Pokazuje drzewo lub zwyk�� list� plik�w, w zale�no�ci od rodzaju widoku
	 ustawionego w klasie macie�ystej.
	 @param open - je�li r�wne TRUE, wtedy plus przy elemencie zmieniany jest na
	 minus, dla FALSE dzieje si� odwrotnie. Dodatkowo w przypadku, gdy elementem
	 jest archiwum zmieniana jest ikona na otwart� (TRUE) lub zamkni�t� (FALSE)
	 paczk�.\n
	 U�ywane przy widoku drzewa.\n
	 */
	void setOpen( bool open );

	/** Metoda zwraca nazw� bie��cego pliku wraz z absolutn� �cie�k� do niego,
	 przyczym nazwa nie zawiera nazwy hosta ('/', 'ftp://', itp.).
	 @param addSlashIfDir jest r�wny TRUE i elementem jest katalog, wtedy do nazwy
	 dodawany jest znak '/'.
	 */
	QString fullName( bool addSlashIfDir=TRUE );

	/** Metoda zwraca obiekt klasy @em UrlInfoExt z informacj� o pliku.
	 @return obiekt klasy @em UrlInfoExt .
	*/
	UrlInfoExt entryInfo() const { return m_uiEntryInfo; }

	/** Zwraca informacj� o tym, czy bie��cy element jest katalogiem.
	 @return TRUE je�li element jest katalogiem, w przeciwnym razie FALSE.
	 Przy czym ka�dy link jest zawsze traktowany jak plik.
	 */
	bool isDir() const { return mIsDir; }

	/** W��cza pokazywanie rozmiar�w katalog�w zamiast ci�gu "\<DIR\>".
	 */
	void setShowDirSize( bool show ) { m_bShowDirSize = show; }

	/** Metoda powoduje ukrycie elementu.
	 */
	void hide() {
		if ( height() )  Q3ListViewItem::setHeight(0);
	}

	/** Metoda powoduje pokazanie elementu.
	 */
	void show();

private:
	Q3Header      *m_pHeader;
	ListView     *m_pListView;
	ListViewItem *m_pListViewItem;

	QString mTxt, ext;
	UrlInfoExt m_uiEntryInfo;
	bool mIsDir, m_bShowDirSize;

	typedef Q3ValueList<UrlInfoExt> UrlInfoList;
	UrlInfoList::iterator it;


	/** Zwraca wska�nik do listy rodzica.\n
	 @return wska�nik do listy rodzica.
	 */
	ListView *listViewExt() const { return (ListView *)Q3ListViewItem::listView(); }

	/** Ustawia podan� pixmap� w pierwszej kom�rce elementu.\n
	 @param p - pixmapa do ustawienia.
	 */
	void setPixmap( QPixmap p );

	/** Ustawia odpowiedni� w�a�ciwo�� 'expandable' dla elementu - pliku.\n
	 U�ywane przy widoku drzewa.\n Je�li element jest katalogiem, a bie��cy
	 u�ytkownik mo�e go czyta� i nie jest on pusty, wtedy w�a�ciwo�� 'expandable'
	 jest ustawiana na TRUE, w przeciwnym razie oraz w przypadku pliku nast�puje
	 ustawienie na FALSE.
	 */
	void setup();

	/** Zwraca przyci�ty, podany ci�g tekstowy, kt�ry jest dopasowany do
	 szeroko�ci podanego numeru kolumny listy.\n
	 @param str - ci�g tekstowy do przyci�cia,
	 @param column - numer kolumny, w kt�rej znajduje si� podany ci�g,
	 @param columnWidth - szeroko�� kolumny, w kt�rej znajduje si� podany ci�g,
	 @param fontWeight - TRUE je�li ci�g jest pisany fontem QFont::bold,
	 w przeciwnym razie FALSE.
	 */
	QString trimedString( const QString & str, const int & column, const int & columnWidth, const int & fontWeight );

};

#endif
