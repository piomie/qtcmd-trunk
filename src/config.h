/***************************************************************************
                          config  -  description
                             -------------------
    begin                : sob kwi 22 2006
    copyright            : (c) 2006 by Mariusz Borowski
    email                : mariusz@nes.pl

    copyright            : See COPYING file that comes with this project

 $Id$
 ***************************************************************************/

#ifndef CONFIG_H
#define CONFIG_H

#include <qsettings.h>

/**
	@author Mariusz Borowski <mariusz@nes.pl>
*/
class Config
{
public:
    ~Config();
    static Config* instance();
    //main window settings

    int mainWindowWidth(); //const;
    void setMainWindowWidth(int width);
    int mainWindowHeight(); //const;
    void setMainWindowHeight(int height);
    bool isMenuBarVisible(); //const;
    void setMenuBarVisible(bool visible);
    bool isToolBarVisible();// const;
    void setToolBarVisible(bool visible);
    bool isButtonBarVisible();// const;
    void setButtonBarVisible(bool visible);
    bool isFlatButtonBar();// const;
    void setFlatButtonBar(bool flat);
    bool isQuitWarnEnabled();// const;
    void setQuitWarnEnabled(bool enable);
    int leftPanelWidth();// const;
    void setLeftPanelWidth(int width);
    //panel settings
    int kindOfLeftPanel();// const;
    void setKindOfLeftPanel(int k);
    int kindOfRightPanel();// const;
    void setKindOfRightPanel(int k);
    //paths
    QString translationsDirPath();// const;
    QString applicationRootDirPath();

private:
    Config();
    QSettings   m_settings;
};

#endif
