/***************************************************************************
                          config  -  description
                             -------------------
    begin                : sob kwi 22 2006
    copyright            : (c) 2006 by Mariusz Borowski
    email                : mariusz@nes.pl, piom@nes.pl

    copyright            : See COPYING file that comes with this project

 $Id$
 ***************************************************************************/
#include <QFile>
#include <QLocale>
#include <QApplication>
#include <QDesktopWidget>

#include "config.h"


Config::Config()
{
}

Config::~Config()
{
}

Config* Config::instance()
{
    static Config conf;
    return &conf;
}

int Config::mainWindowWidth() //const
{
    int width = m_settings.readNumEntry("/qtcommander/General/Width");
    if (width <= 600 || width >= QApplication::desktop()->width()+1)
        width = 800;
    return width;
}

void Config::setMainWindowWidth(int width)
{
    m_settings.writeEntry("/qtcommander/General/Width", width);
}

int Config::mainWindowHeight() //const
{
    int height = m_settings.readNumEntry("/qtcommander/General/Height");
    if (height <= 400 || height >= QApplication::desktop()->height()+1)
        height = 600;
    return height;
}

void Config::setMainWindowHeight(int height)
{
    m_settings.writeEntry("/qtcommander/General/Height", height);
}

bool Config::isMenuBarVisible() //const
{
    return m_settings.readBoolEntry("/qtcommander/General/ShowMenuBar", true);
}

void Config::setMenuBarVisible(bool visible)
{
    m_settings.writeEntry("/qtcommander/General/ShowMenuBar", visible);
}

bool Config::isToolBarVisible() //const
{
    return m_settings.readBoolEntry("/qtcommander/General/ShowToolBar", true);
}

void Config::setToolBarVisible(bool visible)
{
    m_settings.writeEntry("/qtcommander/General/ShowToolBar", visible);
}

bool Config::isButtonBarVisible() //const
{
    return m_settings.readBoolEntry("/qtcommander/General/ShowButtonBar", true);
}

void Config::setButtonBarVisible(bool visible)
{
    m_settings.writeEntry("/qtcommander/General/ShowButtonBar", visible);
}

bool Config::isFlatButtonBar() //const
{
    return m_settings.readBoolEntry("/qtcommander/General/FlatButtonBar", true);
}

void Config::setFlatButtonBar(bool flat)
{
    m_settings.writeEntry("/qtcommander/General/FlatButtonBar", flat);
}

bool Config::isQuitWarnEnabled() //const
{
    return m_settings.readBoolEntry("/qtcommander/General/ShowQuitWarning", true);
}

void Config::setQuitWarnEnabled(bool enable)
{
    m_settings.writeEntry("/qtcommander/General/ShowQuitWarning", enable);
}

int Config::leftPanelWidth( ) //const
{
    return m_settings.readNumEntry("/qtcommander/LeftPanel/Width");
}

void Config::setLeftPanelWidth(int width)
{
    m_settings.writeEntry("/qtcommander/LeftPanel/Width", width);
}

int Config::kindOfLeftPanel() //const
{
    return m_settings.readNumEntry("/qtcommander/LeftPanel/KindOfPanel", 3); //Panel::UNKNOWN_PANEL
}

void Config::setKindOfLeftPanel(int k)
{
    m_settings.writeEntry("/qtcommander/LeftPanel/KindOfPanel", k);
}

int Config::kindOfRightPanel() //const
{
    return m_settings.readNumEntry("/qtcommander/RightPanel/KindOfPanel", 3); //Panel::UNKNOWN_PANEL
}

void Config::setKindOfRightPanel(int k)
{
    m_settings.writeEntry("/qtcommander/RightPanel/KindOfPanel", k);
}

QString Config::translationsDirPath() //const
{
	QString path =  qApp->applicationDirPath();
	//FIXME add support for both locale formats: 'ab' and 'ab_AB'
	//QString locale = QString(QTextCodec::locale()).left(2); // Qt3
	QString sLocale = QLocale().name().left(2);
	if (QFile::exists(path + "/share/sLocale/" + sLocale + "/LC_MESSAGES/qtcmd.qm"))
		path += "/share/locale/" + sLocale + "/LC_MESSAGES/";
	else // executed from source project directory
		path += "/translations/" + sLocale + "/";

	return path;
}



QString Config::applicationRootDirPath()
{
    QString path =  qApp->applicationDirPath();
    return path.left(path.lastIndexOf('/'));
}