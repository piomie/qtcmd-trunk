/***************************************************************************
                          pathview.cpp  -  description
                             -------------------
    begin                : Wed Oct 23 2002
    copyright            : (C) 2002 by Piotr Mierzwi�ski
    email                : peterm@go2.pl

    copyright            : See COPYING file that comes with this project

 $Id$
 ***************************************************************************/
#include <QSettings>

#include "urlsmanagerdlg.h"
#include "fileinfoext.h"
#include "pathview.h"


PathView::PathView( Q3ListView *pListView, QWidget *pParent, const char *szName )
	: QWidget(pParent, szName)
	, m_pComboPathView(NULL), m_pListView(pListView)
{
	m_sSide = QString(szName).left(QString(szName).indexOf('P'));

	m_pComboPathView = new LocationChooser(this);
	m_pComboPathView->buttonMenu()->insertItem( tr("&Edit history"),  this, SLOT(slotEditHistory()), 0, 2000 );
	m_pComboPathView->setMenuItemVisible(LocationChooser::ORIGINAL_DIR, FALSE);
	m_pComboPathView->setMenuItemVisible(LocationChooser::CURRENT_DIR,  FALSE);
	m_pComboPathView->setButtonSide(LocationChooser::LeftButton);
	m_pComboPathView->setShowSeparator(FALSE);
	m_pComboPathView->setCurrentPath("/");
	m_pComboPathView->setKeyListReading("/qtcmd/"+m_sSide+"FilesPanel/PathViewURLs", TRUE); // TRUE - read and init list
	m_pComboPathView->setLeftButtonAction(LocationChooser::PREVIOUS_DIR);
	m_pComboPathView->setRightButtonAction(LocationChooser::NEXT_DIR);
	m_pComboPathView->setBottomButtonAction(LocationChooser::UP_DIR);
	m_pComboPathView->setMaximumHeight(PATHVIEW_HEIGHT);

	m_pComboPathView->setFocusPolicy(Qt::ClickFocus);

	connect(m_pComboPathView, SIGNAL(locationChanged(const QString &)),
		this, SLOT(slotChangePathInFS(const QString &)));
	connect(m_pComboPathView, SIGNAL(textChanged(const QString &)),
		this, SLOT(slotTextChanged(const QString &)));
// 	connect(m_pComboPathView, SIGNAL(returnPressed(const QString &)),
// 	 this, SIGNAL(signalPathChanged(const QString &)));

	setTabOrder(m_pComboPathView, pListView);
	setMaximumHeight(PATHVIEW_HEIGHT);
	m_bTextInserted = false;

	m_pComboPathView->lineEdit()->installEventFilter(this);
}


PathView::~PathView()
{
	m_pComboPathView->saveSettings();
	delete m_pComboPathView;
}

//	init();
	/** Metoda inicjuj�ca widok �cie�ki.\n
	 Wczytywana jest tutaj i wstawiana do widoku, lista URLi oraz numer bie��cego
	 URLa, kt�ry staje si� bie��cym w widoku.
	 */
//	void init();
/*
void PathView::init()
{
	QSettings *settings = new QSettings();
	QString side = QString(name()).left(QString(name()).indexOf('P'));

	// --- read list of URLs for the ComboPath obj.
	QStringList listOfURLs = settings->readListEntry("/qtcmd/"+side+"FilesPanel/URLs");
	if (listOfURLs.isEmpty())
		listOfURLs.append("/"); // defaultPanelPath

	// an each URL should be ended by slash ('/')
	for(uint i=0; i<listOfURLs.count(); i++)
		if (listOfURLs[i].at(listOfURLs[i].length()-1) != '/')
			listOfURLs[i] += '/';
	// init combo sPath by the list
	m_pComboPathView->insertStringList(listOfURLs);

	// --- read number of current URL
	int numOfCurrentItem = settings->readNumEntry("/qtcmd/"+side+"FilesPanel/NumOfCurrentURL");
	if (numOfCurrentItem < 1 || numOfCurrentItem > (int)listOfURLs.count()) {
		numOfCurrentItem = 1;
	}
	numOfCurrentItem--; // becouse saved numer has been increased about 1
	m_pComboPathView->setCurrentItem(numOfCurrentItem);

	delete settings;
}
*/


QString PathView::prevUrl( bool bGetFromLocalFS ) const
{
	QString sItemName, sUrlOut = "/";
	uint nAllItems = m_pComboPathView->count();
	uint nPrevId   = m_pComboPathView->prevItemId();

	while (nAllItems--) {
		if (bGetFromLocalFS) { // indexOf sPath from local file system
			sItemName = m_pComboPathView->text(nPrevId);
			if (sItemName.startsWith('/') && ! FileInfoExt::isArchive(sItemName)) {
				sUrlOut = sItemName;
				break;
			}
			else
				nPrevId = ((nPrevId > 0) ? --nPrevId : m_pComboPathView->count()-1);
		}
		else
			return m_pComboPathView->prevEntry();
	}

	return sUrlOut;
}


void PathView::setStatusWritable()
{
	QColor bgColor;
	VFS::DirWritable dw;

	emit signalPossibleDirWritable(dw); // get writable status

	if (dw == VFS::ENABLEdw)
		bgColor = QColor(228, 240, 223);
	else
	if (dw == VFS::DISABLEdw)
		bgColor = QColor(255, 229, 229);
	else
		bgColor = Qt::white;

	m_pComboPathView->lineEdit()->setPaletteBackgroundColor(QColor(bgColor));
	m_pComboPathView->lineEdit()->setPaletteForegroundColor(Qt::black);
}


void PathView::pathAutoCompletion( const QString & sPath )
{
	QString sLastName = sPath;

	if (sPath != "/")
		sLastName = sPath.right(sPath.length() - sPath.findRev('/', sPath.length()-1)-1);

	QString sCurrentName = listContainsItem(sLastName);

	if (! sCurrentName.isEmpty()) {
		int nHighlightLen = sCurrentName.length() - sLastName.length();
		m_bTextInserted = TRUE;
		m_pComboPathView->lineEdit()->insert(sCurrentName.right(nHighlightLen));
		m_pComboPathView->lineEdit()->cursorForward(TRUE, - nHighlightLen);
		m_bTextInserted = FALSE;
	}
}


QString PathView::listContainsItem( const QString & sItemName )
{
	if (sItemName.isEmpty())
		return sItemName;

	bool bFound = FALSE;
	bool bTreeView = ((ListView *)m_pListView)->isTreeView();

	Q3ListViewItem *pParentOfCurrentItem = NULL, *pCurrentItem = NULL;
	Q3ListViewItemIterator it = m_pListView;

	if (bTreeView) {
		pParentOfCurrentItem = m_pListView->currentItem()->parent();
		if (pParentOfCurrentItem)
			if (! pParentOfCurrentItem->text(0).isEmpty())
				it = pParentOfCurrentItem;
		it++;
	}

	while (TRUE) {
		pCurrentItem = it.current();
		if (bTreeView) {
			if (pCurrentItem) {
				if (pCurrentItem->parent() != pParentOfCurrentItem)
					break;
			}
			else
				break;
		}
		else
			if (pCurrentItem == 0)
				break;

		if (! ((ListViewItem *)pCurrentItem)->isDir()) {
			++it;
			continue;
		}

		m_sStr = pCurrentItem->text(0).left(sItemName.length());
		if (m_sStr.contains(sItemName)) {
			m_sStr = pCurrentItem->text(0);
			bFound = TRUE;
			break;
		}
		++it;
	}

	return bFound ? m_sStr : QString::null;
}


void PathView::resizeEvent( QResizeEvent * )
{
	m_pComboPathView->resize(width()+1, PATHVIEW_HEIGHT);
}


bool PathView::eventFilter( QObject *pObiect, QEvent *pEvent )
{
	if (pEvent->type() == QEvent::KeyPress) {
		if (m_bTextInserted)
			m_bTextInserted = FALSE;

		QKeyEvent *pKeyEvent = (QKeyEvent *)pEvent;
		int key = pKeyEvent->key();
		if (m_pComboPathView->lineEdit()->hasSelectedText())
			if (key == Qt::Key_Backspace || key == Qt::Key_Delete) {
				m_pComboPathView->lineEdit()->del();
				m_bTextInserted = TRUE;
			}
	}

	return QWidget::eventFilter(pObiect, pEvent);    // standard event processing
}

//     ----- SLOTS -----

void PathView::slotChangePathInFS( const QString & sNewPath )
{
	emit signalPathChanged(sNewPath);
	setStatusWritable();
	//updateToolTips();
}


void PathView::slotEditHistory()
{
	QString sNewURL;
	if (UrlsManagerDlg::showDialog(sNewURL, UrlsManagerDlg::HISTORY, currentUrl(), this)) { // let's go to URL
		emit signalPathChanged(sNewURL);

		// path(es) maybe have be removed, so need to read all again
		QSettings settings;
		QStringList slListOfURLs = settings.readListEntry("/qtcmd/"+m_sSide+"FilesPanel/PathViewURLs");
		m_pComboPathView->insertStringList(slListOfURLs, TRUE);
	}
	parentWidget()->setFocus();
}


void PathView::slotPathChanged( const QString & sNewPath )
{
	m_bTextInserted = TRUE; // secure before twice dir.listing
	m_pComboPathView->insertItem(sNewPath);
	m_bTextInserted = FALSE;
	setStatusWritable();
	//updateToolTips();
}


void PathView::slotTextChanged( const QString & sNewTxt )
{
	if (! m_pListView || sNewTxt.isEmpty() || m_bTextInserted)
		return;

	if (sNewTxt.indexOf("\n") != -1 || sNewTxt.indexOf("\t") != -1)  {
		m_pComboPathView->lineEdit()->setText(mOldStr);
		return;
	}

	bool bFtpOrSmbStrIsOnBegin = (sNewTxt.startsWith("ftp:/") || sNewTxt.startsWith("sftp:/") || sNewTxt.startsWith("smb:/"));
	bool bOnlyFtpOrSmbStr    = (sNewTxt == "ftp:///" || sNewTxt == "sftp:///" || sNewTxt == "smb:///"); // block return behind insert theird slash

    int len = sNewTxt.length();
	if ((sNewTxt.indexOf("//") != -1 && ! bFtpOrSmbStrIsOnBegin) || bOnlyFtpOrSmbStr ||
		sNewTxt.at(0) ==  '.' || (len > 1 && sNewTxt.at(1) == '.')  || (sNewTxt.at(0) == '/' && len > 1 && sNewTxt.at(1) == '.') ) {
			m_pComboPathView->lineEdit()->setText(sNewTxt.left(sNewTxt.length() - 1) );
			return;
	}

	pathAutoCompletion(sNewTxt);

	m_sStr = m_pComboPathView->lineEdit()->text();
	if (m_sStr.endsWith('/')) {
		if (m_sStr == "ftp:/" || m_sStr == "smb:/" || m_sStr == "nfs:/" || m_sStr == "ftp://" || m_sStr == "smb://" || m_sStr == "nfs://")
			return;

		if (m_sStr != "~") {
			if (! m_sStr.endsWith('/'))
				m_sStr += "/";
		}
		m_bRestoreOldPath = (m_sStr.indexOf("ftp://") != 0 && m_sStr.indexOf("sftp://") != 0);
		emit signalPathChanged(m_sStr);
	}
}


//	void updateToolTips();
//void PathView::updateToolTips()
//{
// TODO obsluge ponizszego trzeba wrzucic do klasy 'LocationChooser'
/*
	int menuHeight  = mQuickCdMenu->sizeHint().height()-5; // 5 - 2*separator
	int actionH     = menuHeight/mQuickCdMenu->count()+4;
	QRect prevActRect = QRect(0, actionH*4, mQuickCdMenu->width(), actionH);
	QRect nextActRect = QRect(0, actionH*5, mQuickCdMenu->width(), actionH);

	QToolTip::remove(mQuickCdMenu, prevActRect);
	QToolTip::remove(mQuickCdMenu, nextActRect);

	QToolTip::add(mQuickCdMenu, prevActRect, prevUrl());
	QToolTip::add(mQuickCdMenu, nextActRect, nextUrl());
*/
//}
