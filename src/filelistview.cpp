/***************************************************************************
                          filelistview  -  description
                             -------------------
    begin                : nie kwi 23 2006
    copyright            : (C) 2006 by Mariusz Borowski
    email                : mariusz@nes.pl

    copyright            : See COPYING file that comes with this project

 $Id$
 ***************************************************************************/

/*  Zakres odpowiedzialno�ci klasy:
    - wyswietlanie listy plikow (5 kolumn)
    - rozszerzon� obs�uge kolorowania element�w listy
    - wsparcie dla szybkiego wyszukiwania po nazwie pliku
    - wsparcie dla edycji element�w listy - aby by�o mo�liwe trzeba wskaza� dla kt�rej kolumny jest to mo�liwe
    - sortowanie po kolumnach
    - siatka
    - mo�liwo�� wy�wietlania ikon
    - wsparcie dla zaznaczania/odznaczania plik�w (+ okna dialogowe)
    - menu kontekstowe
*/

#include "filelistview.h"

FileListView::FileListView()
 : Q3ListView()
{
}


FileListView::~FileListView()
{
}


