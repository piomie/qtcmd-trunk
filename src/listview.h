/***************************************************************************
                          listview.h  -  description
                             -------------------
    begin                : Mon Jul 9 2001
    copyright            : (C) 2001 by Mariusz Borowski (initializer)
                         : (C) 2002,2003 by Piotr Mierzwi�ski (developer)
    email                : mborowski@silesia.net, peterm@go2.pl

    copyright            : See COPYING file that comes with this project

 $Id$
 ***************************************************************************/

#ifndef _LISTVIEW_H_
#define _LISTVIEW_H_

#include <q3header.h>
#include <q3listview.h>
#include <qstringlist.h>
//Added by qt3to4:
#include <Q3ValueList>
#include <QPixmap>
#include <QMouseEvent>
#include <QEvent>

#include "listviewfinditem.h"
//#include "listvieweditbox.h"
#include "listviewitem.h"
#include "popupmenufp.h"
#include "urlinfoext.h"
#include "filters.h"
#include "enums.h"

/**
  *@authors: Mariusz Borowski (initialized), Piotr Mierzwi�ski (ext.and develop)
 **/
/** @short Klasa widoku listy plik�w.
 Jest to rozszerzona wzgl�dem QListView klasa, przystosowana do obs�ugi listy
 plik�w. Zawiera wiele pomocnych metod pomagaj�cych w obs�udze listy plik�w.
 Nale�� do nich:\n
  @li pokazywanie listy w dw�ch widokach: jako zwyk�ej listy i jako drzewa;
  @li wiele metod kolorowania listy i jej element�w (kolorowanie element�w przy
   zaznaczeniu i bez niego, kolorowanie t�a, kolorowanie kursora, rysowanie
	 siatki w okre�lonym kolorze);
  @li ustawianie tzw. pe�nego kursora lub kursora jako dwukolorowej ramki;
  @li ustawianie rodziny i rozmiaru fontu dla element�w listy;
  @li pokazywanie i chowanie ikon dla element�w (w pierwszej kolumnie), przy
   czym rodzaj ikony uzale�niony jest od rozszerzenia pliku;
  @li ustawianie sortowania dla okre�lonej kolumny i w okre�lonym kierunku;
  @li w��czana/wy��czana w�a�ciwo�� pokazywania ca�y czas kursora podczas
   przesuwania (w pionie) zawarto�ci widoku;
  @li wiele metod zaznaczania element�w (pojedynczo, grupami wg wzorca,
   wszystkie z okre�lonym rozszerzeniem, w zakresie wskazanym mysz�, wszystkie
   istniej�ce). Przy czym mo�na wybra� do sortowania same pliki, same katalogi,
   lub jedne i drugie.
  @li wy�wietlanie wybranych kolumn (mo�liwo�� wy��czania i w��czania
   okre�lonych);
  @li pokazywanie post�pu wstawiania na list� element�w
  @li mo�liwo�� filtrowania element�w listy. Mog� by� pokazywane:
   - wszystkie pliki i katalogi, opr�cz ukrytych,\n
   - wszystkie pliki i katalogi,\n
   - tylko katalogi,\n
   - elementy pasuj�ce do filtru zdefiniowanego przez u�ytkownika \n \n
 Tworzone s� tu r�wnie� obiekty szybkiego wyszukiwania elementu na li�cie,
 zmiany nazwy, oraz menu podr�cznego.
*/
class ListView : public Q3ListView
{
	friend class ListViewItem;
	Q_OBJECT
//private:
// 	typedef QValueList<UrlInfoExt> UrlInfoList;
// 	UrlInfoList mBufferUrlsList;

public:
	/** Konstruktor listy.\n
	 @param pParent - wska�nik rodzica dla listy,
	 @param bPopupMenu - TRUE oznacza, �e nale�y utworzy� obiekt menu podr�cznego,
	 @param bToolTip - TRUE oznacza, �e nale�y utworzy� obiekt dymka dla elementu,
	 @param bRenameBox - TRUE oznacza, �e nale�y utworzy� obiekt okienka
	  edycyjnego, s�u��cego do szybkiej zmiany warto�ci kom�rki bie��cego elementu
	 @param bFindDlg - TRUE oznacza, �e nale�y utworzy� obiekt dialogu do szybkiego
	  s�u��cego do wyszukiwania elementu na li�cie,
	 @param sz_pName - nazwa dla obiektu.\n
	 Zawiera domy�lne ustawienia dla listy, czyli: brak siatki, szary kolor
	 siatki, lista bez dwu kolorego t�a, drugi kolor t�a dwukolorowego jako blady
	 pomara�cz, bia�y kolor t�a, granatowy wype�niony kursor, pokazuj ikony przy
	 elemencie w pierwszej kolumnie, czcionka na li�cie to Helvetica o rozmiarze
	 10, nie grupuj rozmiar�w plik�w.
	 */
	ListView( QWidget *pParent, bool bPopupMenu=TRUE, bool bToolTip=TRUE, bool bRenameBox=TRUE, bool bFindDlg=TRUE, const char *sz_pName=0 );

	/** Destruktor klasy.\n
	 Usuwane s� tutaj obiekty tworzone wewn�trz.
	 */
	~ListView();


	/** Metoda ustawia kolor t�a dla zaznaczonego tekstu na podany.\n
	 @param highlBgColor - nowy kolor t�a.
	 */
	void setHighlightBgColor( const QColor & highlBgColor );

	/** Zwraca kolor t�a dla zaznaczonego tekstu.\n
	 @return kolor t�a.
	 */
	const QColor & highlightBgColor() const;


	/** Metoda ustawia kolor zaznaczonego tekstu na podany.\n
	 @param highlTextColor - kolor do ustawienia.
	 */
	void setHighlightedTextColor( const QColor & highlTextColor );

	/** Zwraca kolor kolor zaznaczonego tekstu.\n
	 @return kolor tekstu.
	 */
	const QColor & highlightedTextColor() const;


	/** Metoda ustawia kolor niezaznaczonego tekstu na podany.\n
	 @param txtColor - kolor do ustawienia
	 */
	void setTextColor( const QColor & txtColor );

	/** Zwraca kolor niezaznaczonego tekstu.\n
	 @return kolor tekstu.
	 */
	const QColor & textColor() const;


	/** Metoda ustawia kolor tekstu niezaznaczonego elementu pod kursorem.\n
	 @param c - nowy kolor tekstu.
	 */
	void setTextColorUnderCursor( const QColor & c );

	/** Zwraca kolor tekstu niezaznaczonego elementu pod kursorem.\n
	 @return kolor tekstu.
	 */
	const QColor & textColorUnderCursor() const;


	/** Metoda ustawia kolor tekstu zaznaczonego elementu pod kursorem.\n
	 @param c - nowy kolor tekstu.
	 */
	void setHighlightTextColorUnderCursor( const QColor & c );

	/** Zwraca kolor tekstu zaznaczonego elementu pod kursorem.\n
	 @return kolor tekstu.
	 */
	const QColor & highlightedTextColorUnderCursor() const;


	/** Metoda ustawia drugi kolor t�a (t�o mo�e by� w paski).\n
	 @param c - nowy kolor drugiego paska t�a (pierwszy jest zawsze bia�y).
	 */
	void setSecondColorOfBg( const QColor & c );

	/** Zwraca drugi kolor t�a.\n
	 @return drugi kolor t�a.
	 */
	const QColor & secondColorOfBg() const;

	/** W��cza/wy��cza dwukolorowe t�o.\n
	 @param TRUE - wymusza rysowanie dwukolorowego t�o, w przeciwnym razie b�dzie
	 mie� jeden kolor.\n
	 Domy�lnie w�a�ciwo�� ta jest wy��czona.
	 */
	void setEnableTwoColorsOfBg( bool enable = TRUE );

	/** Zwraca informacj� o tym czy t�o jest dwukolorowe.\n
	 @return TRUE - oznacza, �e t�o jest dwukolorowe, w przeciwnym razie ma jeden.
	 */
	bool enabledTwoColorsOfBg() const;


	/** Metoda ustawia kolor siatki (domy�lnym jest szary - 'Qt::gray').\n
	 @param gridColor - nowy kolor dla siatki.
	 */
	void setGridColor( const QColor & gridColor );

	/** Zwraca kolor siatki.\n
	 @return kolor siatki.
	 */
	const QColor & gridColor() const;

	/** Metoda w��cza/wy��cza pokazywanie siatki.\n
	 @param showGrid - r�wne TRUE, oznacza �e siatka zostanie pokazana,
	 w przeciwnym razie nie jest b�dzie rysowana
	 */
	void setShowGrid( bool showGrid = TRUE );

	/** Zwraca informacj� o tym czy siatka jest pokazywana.\n
	 @return TRUE, je�li jest pokazywana, w przeciwnym razie FALSE.
	 */
	bool grid() const;


	/** Metoda w��cza/wy��cza tzw. pe�ny kursor (belka wype�niona jednym kolorem).\n
	 @param fullCursor - r�wne TRUE, oznacza �e rysowany b�dzie pe�ny kursor,
	 w przeciwnym razie rysowana jest dwukolorowa ramka.\n
	 Domy�lnie w�a�ciwo�c ta jest w��czona.
	 */
	void setFullCursor( bool fullCursor = TRUE );

	/** Metoda ustawia kolor dla tzw. pe�nego kursora (belka wype�niona jednym
	 kolorem). Domy�lnym jest kolor granatowy ('Qt::darkBlue').\n
	 @param c - nowy kolor pe�nego kursora.
	 */
	void setFullCursorColor( const QColor & c );

	/** Zwraca informacj� o tym czy pokazywany jest tzw. pe�ny kursor.\n
	 @return TRUE je�li pokazywany jest pe�ny kursor, w przeciwnym razie FALSE.
	 */
	bool isFullCursor() const;


	/** Metoda w��cza/wy��cza rysowanie kursora jako dwukolorowej ramki.\n
	 @param dblFrameCursor - r�wne TRUE, wymusza rysowanie kursora jako
	 dwukolorowej ramki.\n
	 Domy�lnie w�a�ciwo�c ta jest wy��czona.
	 */
	void setDblFrameCursor( bool dblFrameCursor = TRUE );

	/** Metoda ustawia kolory dla kursora jako dwukolorowej ramki.\n
	 @param colInside - kolor dla wewn�trznej ramki,
	 @param colOutside - kolor dla zewn�trznej ramki.
	 */
	void setDblFrameCursorColors( const QColor & colInside, const QColor & colOutside );


	/** Metoda ustawia kolor dla kursora pe�nego i dwukolorowej ramki.\n
	 @param cursorColor - nowy kolor kursora, przy czym dla kursora jako
	 dwukolorowej (obywdwie ramki b�d� mia�y ten sam kolor).
	 */
	void setCursorColor( const QColor & cursorColor );

	/** Zwraca kolor kursora.\n
	 @return kolor kursora.
	 */
	const QColor & cursorColor() const;

	/** Zwraca kolor wewn�trznej ramki kursora.\n
	 @return kolor wewn�trznej ramki.
	 */
	const QColor & frameInsCursorColor() const;

	/** Zwraca kolor zewn�trznej ramki kursora.\n
	 @return kolor zewn�trznej ramki.
	 */
	const QColor & frameOutCursorColor() const;


	/** Zwraca rozmiar bie��cego fontu listy (w punktach).\n
	 @return rozmiar fontu.
	 */
	uint fontSize() const { return mFontSize; }

	/** Zwraca nazw� rodziny bie��cego fontu listy.
	 @return nazwa rodziny fontu.
	 */
	QString fontFamily() const { return m_sFontFamily; }

	/** Ustawia rozmiar bie��cego fontu (w punktach) na podany.\n
	 @param fontSize - rozmiar fontu.
	 */
	void setFontSize( const uint fontSize ) { setFontList( m_sFontFamily, fontSize ); }

	/** Ustawia rodzin� fontu na podan�.\n
	 @param fontFamily - nowa rodzina fontu.
	 */
	void setFontFamilly( const QString & fontFamily ) { setFontList( fontFamily, mFontSize ); }

	/** Metoda powoduje ustawienie rodziny i rozmiaru czcionki dla listy.\n
	 @param fontFamily - rodzina czcionki,
	 @param fontSize - rozmiar czcionki (w punktach).
	 */
	void setFontList( const QString & fontFamily, const uint fontSize );


	/** Funkcja uruchamia formatowanie podanego ci�gu cyfr.\n
	 @param numStr - ci�g do sformatowania i umieszczenia wyniku.\n
	 Wywo�ywana jest tu funkcja zewn�trzna formatuj�ca ci�g cyfr. Przy czym rodzaj
	 formatowania jest ustawiony w sk�adowej @em m_eKindOfFormating. Wynik
	 formatowania zapisywany jest w podanym parametrze.\n
	 */
	void formatNumberStr( QString & numStr );

	/** W��cza/wy��cza wy�wietlanie ikon (na pocz�tku pierwszej kolumny).\n
	 @param showIcons - r�wne TRUE - ikony b�d� pokazywane, w przeciwnym razie
	 nie zostan� wy�wietlone.\n
	 */
	void setShowIcons( bool showIcons = TRUE );


	/** Zwraca ikone odpowiadaj�c� rozszerzeniu pliku podanego jako obiekt
	 informacji o pliku.\n
	 @param urlInfo - adres obiektu z informacj� o pliku,
	 @return wska�nik na nowoutworzony obiekt QPixmap.\n
	 Przy czym prawa do uruchamiania maj� wi�kszy priorytet od rozszerzenia pliku.
	 */
	QPixmap * icon( const UrlInfoExt & ) const;


	/** Zwraca informacj� o tym czy pokazywane s� ikony.\n
	 @return TRUE - oznacza, �e ikony s� pokazywane, w przeciwnym wypadku s�
	 niewidoczne.
	 */
	bool showIcons() const { return m_bShowIcons; }

	/** Zwraca szeroko�� ikony dla elementu listy.\n
	 @return szeroko�� ikony (w punktach).
	 */
	uint iconWidth() const { return mIconWidth; }


	/** Zwraca informacj� o aktualnym rodzaju formatowania rozmiar�w plik�w.\n
	 @return rodzaj formatowania, patrz: @see KindOfFormating.
	 */
	KindOfFormating kindOfGroupFileSize() const { return m_eKindOfFormating; }

	/** Ustawia rodzaj formatowania rozmiar�w plik�w na podany.\n
	 @param kindOfFormating - rodzaj formatowania, patrz: @see KindOfFormating.
	 */
	void setKindOfGroupFileSize( KindOfFormating kindOfFormating ) { m_eKindOfFormating = kindOfFormating; }


	/** Metoda s�u�y do zmiany widoku listy na podany.\n
	 @param kindOfView - nowy widok dla listy typu @see KindOfListView
	 */
	void setKindOfView( KindOfListView kindOfView );

	/** Zwraca informacj� czy bie��cym widokiem jest drzewo.\n
	 @return TRUE je�li widokiem jest drzewo, w przeciwnym razie FALSE.
	 */
	bool isTreeView() const { return (m_eKindOfView==TREE_PANELVIEW); }

	/** Zwraca informacj� czy bie��cym widokiem jest zwyk�a lista.\n
	 @return TRUE je�li widokiem jest zwyk�a lista, w przeciwnym razie FALSE.
	 */
	bool isListView() const { return (m_eKindOfView==LIST_PANELVIEW); }

	/** Zwraca informacj� o rodzaju bie��cego widoku.\n
	 @return rodzaj widoku, patrz: @see KindOfListView.
	 */
	KindOfListView kindOfView() const { return m_eKindOfView; }


	/** Zwraca informacj� o tym, czy dla link�w pokazywane d�ugo�ci ich �cie�ki,
	 czy rozmiary plik�w docelowych.\n
	 @return TRUE - oznacza, �e pokazywane s� d�ugo�ci ich �cie�ki, w przeciwnym
	 razie rozmiaru plik�w docelowych.
	 */
	bool showFileLinkSize() const { return m_bShowFileLinkSize; }

	/** Metoda w��cza/wy��cza pokazywanie rozmiar�w dla link�w jako d�ugo�� ich
	 �cie�ki.\n
	 @param TRUE - wymusza pokazywanie d�ugo�ci ich �cie�ki, w przeciwnym razie
	 rozmiaru pliku docelowego
	 */
	void setShowFileLinkSize( bool showFileLinkSize ) { m_bShowFileLinkSize = showFileLinkSize; }

	/** Powoduje zastosowanie filtra dla listy plik�w.\n
	 @param fTemplate - wzorzec dopasowania plik�w,
	 @param dirFilter - typ plik�w.\n
	 Po wykonaniu tej metody, na li�cie pozostan� tylko pasuj�ce do wzora i typu,
	 reszta zostanie ukryta.
	 */
	void applyFilter( const QString & fTemplate, Filters::DirFilter dirFilter, bool filterForFiltered=FALSE );

//	UrlInfoList & bufferUrlsList()  { return mBufferUrlsList;         }
//	uint bufferUrlsListCount()      { return mBufferUrlsList.count(); }
//	void setBufferUrlsList( const QValueList<UrlInfoExt> & urlsList ) { mBufferUrlsList = urlsList;        }
//	void appendToBufferUrlsList( const UrlInfoExt & urlInfo )         { mBufferUrlsList.append( urlInfo ); }

	/** Zwraca numer kolumny, po kt�rej sortowana jest lista.
	 @return numer sortowanej kolumny.
	 */
	uint sortColumn() const { return mSortColumn; }

	/** Zwraca kierunek sortowania.
	 @return TRUE oznacza od najmniejszej do najwi�kszej warto�ci,
	 FALSE - odwrotnie
	 */
	bool ascending() const { return mAscending;  }

	/** Metoda ustawia sortowanie listy wg podanych parametr�w.\n
	 @param column - kolumna, po kt�rej nale�y sortowa�,
	 @param ascending - kierunek sortowania (TRUE - od najmniejszego do
	 najwi�kszego, FALSE - odwrotnie).
	 */
	void setSorting( int column, bool ascending );



	/** Zwraca informacj� o tym, czy kursor jest nie ukrywany na li�cie podczas
	 jej przewijania.\n
	 @return TRUE oznacza, �e kursor nie jest ukrywany, w przeciwnym razie jest
	 ukrywany.
	 */
	bool focusAlwaysVisible() const { return m_bNotHiddenFocus; }

	/** Metoda w��cza/wy��cza ci�g�e mechanizm nieukrywania kursora na li�cie
	 podczas jej przewijania.\n
	 @param focusAlwaysVisible - TRUE wymusza nieukrywanie kursora, w przeciwnym
	 razie b�dzie on ukrywany.\n
	 Domy�lnie w�a�ciwo�� ta jest w��czona.
	 */
	void setFocusAlwaysVisible( bool focusAlwaysVisible ) { m_bNotHiddenFocus = focusAlwaysVisible; }


//	void takeItem( const QString & item ) { removeItem(item); };
	/** Funkcja pr�buje usun�c element o podanej nazwie.\n
	 @param name - nazwa (warto�� pierwszej kolumny) elementu do usuni�cia.\n
	 Po udanym usuni�ciu, kursor jest ustawiany na nast�pny element listy.
	 */
	void removeItem( const QString & name );

	/** Funkcja powoduje wstawienie podanego elementu na list�.\n
	 @param urlInfo - informacja o elemencie,
	 @param parentItemTree - wska�nik elementum kt�ry b�dzie rodzicem dla
	 wstawianego (u�ywane dla widoku drzewa),
	 @param tryRemoveThisSameItem - r�wne TRUE wykonuje pr�b� usuni�cia elememtu
	 o nazwie podanej w @em urlInfo, przed wstawieniem nowego elementu; warto��
	 FALSE umo�liwia wstawienie kolejnego elementu o takiej samej nazwie,
	 @param setProgress - r�wne TRUE wymusza uaktualnienie paska post�pu
	 */
	void insertItem( const UrlInfoExt & urlInfo, Q3ListViewItem *parentItemTree, bool tryRemoveThisSameItem=TRUE, bool setProgress=FALSE );


	/** Funkcja u�ywana do ustawiania kursora na podanego nazw� elementu na
	 li�cie.\n
	 @param name - nazwa elementu, na kt�rym nale�y umie�cic kursor.\n
	 Je�li podana nazwa nie istnieje, wtedy kursor ustawiany jest na pierwszy
	 element listy.
	*/
	void moveCursorToName( const QString & name );


	/** Metoda powoduje ustwienie tekstu dla bie��cego elementu w podanej
	 kolumnie.\n
	 @param column - numer kolumny, w kt�rej nale�y ustawi� tekst,
	 @param str - tekst do ustawienia.
	 */
	void setTextInColumn( int column, const QString & str );

	/** Zwraca ci�g tekstowy bie��cego elementu z podanej kolumny.\n
	 @param column - kolumna w kt�rej znajduje si� tekst do pobrania.
	 */
	QString currentItemText( uint column = 0 ) const { return currentItem()->text( column ); }


	/** Zwraca liczb� element�w na li�cie lub drzewie.\n
	 @param minusOneForList - r�wne TRUE wymusza korekt� (-1 dla zwracanej liczby
	  element�w). Przydatne jest to dla widoku listy, dzi�ki temu nie jest liczony
	  element '..'.
	 @return liczba element�w listy.
	 */
	uint numOfAllItems( bool minusOneForList=TRUE );


	/** Powoduje zaznaczenie grupy element�w we wskazanym mysz� zakresie.\n
	 @param from - wska�nik elementu, od kt�rego trzeba zacz�� zaznacza�
	  (w��cznie),
	 @param to - wska�nik elementu, na kt�rym nale�y sko�czy� zaznaczanie
	  (w��cznie),
	 @param clear - r�wne TRUE, oznacza odznaczanie, w przeciwnym razie elementy
	  zostan� zaznaczone
	 @param invert - r�wne TRUE powoduje inwersje zaznaczania na wybranym zakresie
	 */
	void selectRange( Q3ListViewItem * from, Q3ListViewItem * to, bool clear = FALSE, bool invert = FALSE );

	/** Zaznacza lub odznacza element listy.\n
	 @param item - wska�nik na element listy,
	 @param select - r�wne TRUE, wymusza zaznaczenie, w przyciwnym wypadku element
	 jest odznaczany.\n
	 Uaktualniana jest tu r�wnie� lista zaznaczonych, a na ko�cu wysy�any sygna�
	 @see signalUpdateOfListStatus().
	 */
	void setSelected( Q3ListViewItem * item, bool select );

	/** Metoda zapisuje na list� nazwy wraz ze scie�kami wszystkie zaznaczone
	 elementy, przy czym je�li �adnego nie zaznaczono, wtedy dopisuje tylko nazw�
	 bie��cego.\n
	 @param selectedNamesList - adres listy ci�g�w, na kt�rej umieszczone zostan�
	 nazwy.
	 */
	void collectAllSelectedNames( QStringList & selectedNamesList );

	/** Zlicza wszystkie zaznaczone pliki (ich liczb� oraz ca�kowit� wag�), przy
	 czym katalogi nie s� wa�one.\n
	 @param addToTheSelectedItemsList - r�wny TRUE, wymusza dodanie zaznaczonego
	 elementu do listy zaznaczonych.\n
	 Na ko�cu wysy�any jest sygna� @see signalUpdateOfListStatus().
	 */
	void countAndWeightSelectedFiles( bool addToTheSelectedItemsList=TRUE );

	/** Zwraca liczb� wszystkich zaznaczonych element�w.\n
	 @return liczba zaznaczonych element�w.
	 */
	uint numOfSelectedItems() const { return mNumOfSelectedItems; }

	/** Zwraca wag� wszystkich zaznaczonych element�w.\n
	 @return waga zaznaczonych.
	 */
	long long weightOfSelected() const { return mWeightOfSelected; }

	/** Zwraca informacj� o tym czy w zaznaczanie grupowe obejmuje tylko
	zaznaczanie plik�w.\n
	@return TRUE - zaznaczane s� tylko pliki, w przeciwnym razie wszystkie
	elementy.
	 */
	bool selectFilesOnly() const { return m_bSelectFilesOnly; }

	/** W��cza/wy��cza dla zaznaczania grupowego zaznaczanie tylko plik�w.\n
	 @param filesOnly - r�wne TRUE, wymusza zaznaczanie tylko plik�w, w przeciwnym
	 razie zaznaczane b�d� wszystkie elementy.
	 */
	void setSelectFilesOnly( bool filesOnly ) { m_bSelectFilesOnly = filesOnly; }


	/** Zwraca wska�nik do obiektu menu podr�cznego.\n
	 @return wska�nik do obiektu typu @see PopupMenuFP.
	 */
	PopupMenuFP *popupMenu() { return m_pPopupMenuFP; }

	/** Pokazuje menu podr�czne.\n
	 @param onCursorPos - r�wne TRUE, wymusza ustawienie menu na wsp�rz�dnych
	 kursora myszy. Warto�� FALSE powoduje, �e pozycj� jest: X = prawy brzeg
	 pierwszej kolumny, Y = gorny brzeg bie��cego elementu listy.
	 */
	void showPopupMenu( bool onCursorPos=TRUE );

	/** Zwraca wska�nik do obiektu dialogu szybkiego wyszukiwania elementu na
	 li�cie.\n
	 @return wska�nik do obiektu typu @see ListViewFindItem.
	 */
	ListViewFindItem *findItemDlg() { return m_pListViewFindItem; } // do wyrzucenia w przyszlosci

	/** Funkcja pokazuje minidialog z okienkiem edycyjnym do wpisywania nazwy
	 szukanego na li�cie elementu.\n
	 */
	void showFindItemDialog();


	/** Pr�buje odnale�� na li�cie lub w drzewie element o podanej nazwie.\n
	 @param name - nazwa elementu do odnalezienia,
	 @return wska�nik do znalezionego elementu, lub NULL, je�li nie
	 zosta� on odnaleziony.
	 Szukanie nast�puje w pierwszej kolumnie listy.
	 */
	Q3ListViewItem * findName( const QString & name );

	/** Zwraca informacj� o tym, czy bie��cy element jest katalogiem.
	 @return TRUE je�li element jest katalogiem, w przeciwnym razie FALSE.
	 Przy czym ka�dy link jest zawsze traktowany jak plik.
	 */
	bool currentItemIsDir() { return ((ListViewItem *)currentItem())->isDir(); }

	/** Powoduje, �e podany element pojawia si� w widocznym obszarze listy.\n
	 @param item - wska�nik elementu, kt�ry nale�y pokaza�.
	 */
	void ensureItemVisible( Q3ListViewItem * item=0 );

	/** Powoduje, �e element o podanej nazwie pojawia si� w widocznym obszarze
	 listy.\n
	 @param name - nazwa elementu (z pierwszej kolumny), kt�ry nale�y pokaza�.
	 */
	void ensureNameVisible( const QString & name );


	/** Zwraca �cie�k� z nazw� bie��cego elementu.\n
	 @param absolutePath - r�wne TRUE, wymusza zwr�cenie �cie�ki absolutnej,
	  w przeciwnym razie zwracana jest wzgl�dna
	 @param findParent - (u�ywane dla widoku drzewa); je�li r�wny TRUE, wtedy
	 @return �cie�ka bie��cego elementu.
	 sprawdzane jest czy bie��cy element jest otwarty, je�li tak to zwracana jest
	 jego �cie�ka. Je�eli bie��cy element jest zamkni�ty, wtedy zwracana jest
	 �cie�ka jego rodzica. Przy czym dla elementu korznia zwracana jest �cie�ka
	 hosta (zawarta w sk�adowej @em mHost). Warto�� FALSE, wtedy zwracana jest
	 �cie�ka bie��cego elementu.\n
	 Je�eli elementem jest katalog, wtedy do nazwy dodawany jest znak slash ("/").
	 */
	QString pathForCurrentItem( bool absolutePath=TRUE, bool findParent=FALSE );

	/** Zwraca absolutn� �cie�k� dla podanego elementu lub jego nazw�.\n
	 @param item - wska�nik na elementu, dla kt�rego nale�y zwr�ci� �cie�k�; przy
	 czym je�li jest on r�wny NULL, wtedy brany jest element bie��cy,
	 @param addNameWhenDir - r�wne TRUE wymusza dodanie nazwy elementu, je�li jest
	 on katalogiem.
	 @return �cie�ka podanego elementu.
	 Dla widoku drzewa, zwracana jest absolutna �cie�ka dla podanego elementu
	 (bez jego nazwy), dla widoku listy jest to nazwa elemetu z pierwszej kolumny.
	 */
	QString absolutePathForItem( Q3ListViewItem * item=0, bool addNameWhenDir=FALSE );


	/** Rodzaj kolumny. */
	enum ColumnName { NAMEcol=0, SIZEcol, TIMEcol, PERMcol, OWNERcol, GROUPcol, LOCATIONcol };

	/** Zwraca szeroko�� podanej kolumny.\n
	 @param column - numer kolumny, kt�rej szeroko�� nale�y zwr�ci�,
	 @return szeroko�� kolumny.
	 */
	int columnSize( uint column ) const { return header()->sectionSize( column ); }

	/** Dodaje kolumn� o podanym numerze oraz ustawia jej szeroko��.\n
	 @param columnName - numer kolumny,
	 @param columnWidth - szeroko�� kolumny.\n
	 Je�li podana szeroko�� jest r�wna 0, wtedy kolumna jest usuwana, je�li
	 wi�ksza ni� zero, wtedy podana szeroko�� jest szeroko�ci� kolumny. Je�eli
	 szeroko�� jest mniejsza od minimalnej szeroko�ci dla danej kolumny, wtedy
	 przyjmowana jest warto�� domy�lna.
	 */
	void setColumn( ColumnName columnName, uint columnWidth );

	/** Powoduje pokazanie lub ukrycie nag��wka z kolumnami.\n
	 @param show - r�wne TRUE wymusza pokazanie kolumn, FALSE ukrywa go.
	 */
	void setShowColumns( bool show = TRUE );


	/** Ustawia nazw� hosta dla bie��cego systemu plik�w.\n
	 @param hostName - nowa nazwa hosta.
	 */
	void setHostName( const QString & hostName )   { mHost = hostName;  }

	/** Ustawia aktualn� �cie�k� w bie��cym systemie plik�w.\n
	 @param url - nowa bie��ca �cie�ka.
	 */
	void setCurrentURL( const QString & url ) { m_sCurrentURL = url; }

// 	void forceShowToolTip( const QPoint & pos ) { m_pToolTip->maybeTip( pos );


	/** Metoda uruchamia procedure ponownego odczytania bie��cego katalogu.\n
	 Wywo�ywane s� tu kolejno metody @em close() i @em open() .
	 */
	void refresh();

	/** Funkcja powoduje uruchomienie procedury otwarcia pliku.\n
	 @param ui - rozszerzona informacja o pliku,
	 @param closeIfOpen - r�wne TRUE spowoduje zamkni�cie ga��zi, je�li ta jest
	 ju� otwarta (dziala tylko dla widoku drzewa).\n
	 Inicjowane jest tutaj odpowiednie zachowanie odpowiednio dla widoku drzewa
	 i listy, a po jego zako�czeniu wysy�any sygna� @em signalOpen() powoduj�cy
	 wylistowanie lub pokaznie podgl�du podanego pliku.
	 */
	void open( const UrlInfoExt & ui=UrlInfoExt::null, bool closeIfOpen=FALSE );

	/** Metoda uruchamia procedur� zamkni�cia ga��zi o podanej �cie�ce.\n
	 @param path - �cie�ka dla ga��zi do zamkni�cia.\n
	 Je�eli bie��cym widokiem jest drzewo wtedy ga��� jest zamykana, je�li
	 widokiem jest lista, wtedy zerowane s� tylko sk�adowe okre�laj�ce liczb�
	 i wag� zaznaczonych.
	 */
	void close( const QString & path=QString::null );


	/** Metoda powoduje zaincjowanie widoku listy/drzewa, przed listowaniem
	 plik�w.\n
	 @param path - �cie�ka bie��cego katalogu (wykorzystywana do okre�lenia czy
	 katalog zawiera urz�dzenia),
	 @param numOfAllItems - liczba wszystkich element�w z katalogu,
	 @param owner - nazwa bie��cego u�ytkowanika (inicjuje sk��dow� klasy),
	 @param group - nazwa grupy do kt�rej nale�y bie��cy u�ytkownik  (inicjuje
	 sk�adow� klasy),
	 @param forceCleaning - r�wne TRUE wymusza czyszczenie listy.\n
	 Je�li widokiem b�dzie lista, wtedy widok jest czyszczony, zerowana jest lista
	 zaznaczonych oraz sk�adowe okre�laj�ce ilo�� zaznaczonych i ich wag�, a tak�e
	 wstawiany jest element '..'.
	 */
	void initFilesList( const QString & path, uint numOfAllItems, const QString & owner, const QString & group=QString::null, bool forceCleaning=FALSE );


	/** Rodzaj uaktualnienia listy.
	 */
	enum UpdateListOperation { INSERTitems=1, REMOVEitems=2, DESELECTitems=4, INSERTitemsAsLinks=8 };
//	typedef QPtrList<QListViewItem> SelectedItemsList;
	typedef Q3ValueList<Q3ListViewItem *> SelectedItemsList;

	/** Zwraca wska�nik na list� zaznaczonych element�w.\n
	 @return wska�nik do listy zaznaczonych typu @em SelectedItemsList
	 */
	SelectedItemsList * selectedItemsList() { return m_pSelectedItemsList; }

	/** Metoda powoduje uaktualnienie listy.\n
	 @param updateListOp - rodzaj uaktualnienia, patrz: @see UpdateListOperation,
	 @param list - wska�nik na list� element�w, kt�rymi nale�y uaktualni� bie��c�,
	 @param newUrlInfo - informacja o pliku (u�ywana je�li uaktualniany jest jeden
	 element).\n
	 Po wykonaniu operacji, kursor ustawiany jest na ostatni uaktualniony element,
	 po czym wysy�any zostaje sygna� uaktualniaj�cy informacj� o li�cie.
	 */
	void updateList( int updateListOp, SelectedItemsList *listOfItems=0, const UrlInfoExt & newUrlInfo=UrlInfoExt::null );

	/** Metoda zeruje list� zaznaczonych element�w.
	 */
	void clearSelectedItemsList() { if ( m_pSelectedItemsList->count() )  m_pSelectedItemsList->clear(); }

	/** Metoda usuwa podany element z listy zaznaczonych.\n
	 @param item - wska�nik elementu do usuni�cia.
	 */
	void removeItemFromSIL( Q3ListViewItem * item );


	/** Zwraca numer widocznej kolumny odpowiadaj�cy podanemu.\n
	 @param column - numer kolumny do sprawdzenie
	 @return numer kolumny
	 */
	int sectionMap( int column ) const { return (column<m_nlColumnsList.count()) ? m_nlColumnsList[column] : -1; }

private:
	QColor mGridColor, mSecondColorOfBg;
	QColor mTextColorUnderCursor, mTextColorHighlightUnderCursor;
	QColor mFullCursorColor, mFrameInsCursorColor, mFrameOutCursorColor;

	bool m_bShowGrid, m_bEnableTwoColorsOfBg;
	bool m_bFullCursor, m_bDblFrameCursor;
	bool m_bNotHiddenFocus;
	bool m_bShowFileLinkSize;
	bool m_bShowIcons;
	QString m_sFontFamily;
	uint mFontSize;
	uint mIconWidth;
	uint mNumOfSelectedItems;
	long long mWeightOfSelected;
	bool m_bSelectFilesOnly, m_bInvSelection;
	uint mSortColumn;
	bool mAscending;
	Q3ListViewItem *mCurrentItem;
	QString mHost, m_sCurrentURL;

	ListViewToolTip *m_pToolTip;
	Q3ListViewItem *m_pRootsItemOfCurrentPath;
	SelectedItemsList *m_pSelectedItemsList;

	KindOfFormating m_eKindOfFormating;
	KindOfListView  m_eKindOfView;

	bool m_bDragging;
	QTimer *m_pScrollTimer;

	ListViewFindItem *m_pListViewFindItem;
	//ListViewEditBox *m_pListViewEditBox;
	PopupMenuFP *m_pPopupMenuFP;

	bool mCurrentDirIsDevDir;
	uint mInsertedItemsCounter, mNumOfAllItemsPB; // use by settings progressBar

	QWidget *m_pParent;
	QString m_sOwner, m_sGroup; // of listView

	Q3ValueList <int> m_nlColumnsList;

protected:
	/** Przechwytywane s� tutaj wszelkie zdarzenia listy.\n
	 @param o - wska�nik obiektu, kt�rego dotyczy zadarzenie,
	 @param e - wska�nik obiektu zdarzenia.
	 W przypadku wyst�pienia zdarzenia FocusOut i je�li menu podr�czne lub dialog
	 szukania element�w jest widoczny - kursor listy jest ustawiany na ni�. Dla
	 zdarzenia FocusIn je�li okienko edycyjne jest widoczne lub je�li menu
	 podr�czne lub dialog szukania element�w jest widoczny nic nie jest robione,
	 w przeciwnym razie wysy�any jest sygna� uaktualniaj�cy widok �cie�ki bie��c�.
	 W przypadku zdarzenia KeyPress, je�li menu podr�czne jest widoczne to
	 wci�ni�te klawisze przesy�ane s� do obiektu klasy menu podr�cznego. Je�eli
	 dialog szukania element�w jest widoczny, wtedy wci�ni�cie klawiszy F3...F8
	 spowoduje wys�anie zdarzenia wci�ni�cia klawisza do obiektu klasy rodzica
	 dla bie��cej. Przechwytywane s� tutaj te� niekt�re klawisze wspieraj�ce
	 obs�ug� drzewa.
	 */
	bool eventFilter( QObject * o, QEvent * e );

	/** Funkcja wywo�ywana podczas przesuwania kursorem myszy po widoku listy
	 z wci�ni�tym lewym klawiszem.\n
	 @param e - zdarzenie myszy.\n
	 Jest tu uruchamiane automatyczne przewijanie widoku, gdy kursor wyjdzie poza
	 dolny lub g�rny margines listy.
	*/
	void contentsMouseMoveEvent( QMouseEvent * e );

	/** Obs�uga zdarzenia klikni�cia lewego lub prawego klawisza myszki.\n
	 @param e - zdarzenie myszy.\n
	 Po klikni�ciu prawym klawiszem pokazywane jest menu podr�czne. Przytrzymanie
	 klawisza SHIFT i klikni�cie lewym klawiszem myszy spowoduje zaznaczenie
	 elementu na kt�rym stoi kursor,
	*/
	void contentsMousePressEvent( QMouseEvent * e );

	/** Funkcja wywo�ywana po zwolnieniu przycisku myszy.\n
	 Jest ona niezb�dna dla poprawnego dzia�ania automatycznego przewijania widoku
	 listy, gdy wci�ni�ty jest lewy klawisz myszy a kursor jest poza g�rynym lub
	 dolnym marginesem listy.
	 */
	void contentsMouseReleaseEvent( QMouseEvent * );

	/** Funkcja wywo�ywana po podw�jny klikni�ciu lewym klawiszem myszy w element
	 listy.\n
	 @param e - zdarzenie myszy.\n
	 Uruchamiana jest tu procedura obs�ugi dla podwojnego klikni�cia (wysy�any
	 jest sygna� @see returnPressed).
	 */
	void contentsMouseDoubleClickEvent( QMouseEvent * );


	/** Metoda powoduje ustawienia kolor�w element�w listy.\n
	 @param r - element do kolorowania,
	 @param c - kolor do ustawienia.\n
	 U�ywana jest przez inne metody zmieniaj�ce kolory poszczeg�lnych element�w.
	*/
	void setColor( QColorGroup::ColorRole, const QColor & );


	/** Otwiera ga��� o podanej �cie�ce.\n
	 @param ui - rozszerzona informacja o pliku,
	 @param moveViewAtTop - r�wne TRUE wymusza przesuni�cie widoku na sam� g�r�.\n
	 Metoda wysy�a sygna� @em signalOpen() z parametrem �cie�ki, powoduj�cy
	 wylistowanie zawarto�ci tego katalogu w systemie plik�w.
	 */
	void openBranch( const UrlInfoExt & ui=UrlInfoExt::null, bool moveViewAtTop=TRUE );

	/** Usuwa zawarto�� ga��zi, o podanej �cie�ce.\n
	 @param absPathName - �cie�ka dla ga��zi do wyczyszczenia.
	 */
	void closeBranch( const QString & absPathName=QString::null );

public slots:
	/** Zaznacza lub odznacza bie��cy element oraz dolicza go (ilo��, rozmiar) do
	 zaznaczonych/odznaczonych, po czym powoduje uaktualnienie informacji
	 o plikach.
	 */
	void slotSelectCurrentItem();

	/** Zaznacza lub odznacza wszystkie elementy (pomijaj�c ten o nazwie '..').\n
	 @param clear - r�wne TRUE, oznacza �e elementy b�d� odznaczane, w przeciwnym
	 razie zostan� zaznaczone.\n
	 Uaktualniana jest tu r�wnie� informacja o plikach.
	*/
	void slotSelectAllItems( bool clear = FALSE );

	/** Wykonuje inwersje na wszystkich elementach listy ( pomijaj�c ten o nazwie
	 '..' ).\n Uaktualniana jest tu r�wnie� informacja o plikach.
	*/
	void slotInvertSelections();

	/** Zaznacza lub odznacza elementy wg podanego wzorca.\n
	 @param select - r�wne TRUE, wymusza zanaczanie element�w, w przeciwnym razie
	 zostan� odznaczone,
	 @param pattern - wzorzec, wg kt�rego nast�pi zaznaczenie lub odznaczanie.\n
	 Funkcja zaznaczania uaktualnia r�wnie� statusow� informacj� o plikach.
	 */
	void slotSelectByUsePattern( bool clear, const QString & pattern );

	/** Zaznacza lub odznacza pliki z tym samym rozszerzeniem, jak ten, na kt�rym
	 aktualnie stoi kursor.\n
	 @param clear - r�wne TRUE, oznacza �e elementy b�d� odznaczane, w przeciwnym
	 wypadku zostan� zaznaczone.\n
	 Uaktualniana jest tu r�wnie� informacja o plikach.
	 */
	void slotSelectWithThisSameExtension( bool clear = FALSE );

	/** Pokazuje okno dialogowe s�u��ce do pobierania wzorca do zaznaczenia lub
	 odznaczenia element�w.\n
	 @param clear - r�wne TRUE, oznacza �e elementy b�d� odznaczane, w przeciwnym
	 razie zostan� zaznaczone.\n
	 Po wci�ni�ciu 'OK', elementy s� zaznaczane lub odznaczane wg podanego wzorca.
	 Mo�na r�wnie� poda� kilka wzorc�w, wtedy nale�y je oddzieli� znakiem "/".
	 Lista wzorc�w jest za ka�dym razem zapisywana do pliku konfiguracyjnego,
	 przy czym ostatnio wybrany lub nowy zawsze znajduje si� na pierwszym miejscu.
	*/
	void slotSelectGroupOfItems( bool clear = FALSE );

	/** Slot wywo�ywany w momencie zmiany rozmiaru podanej kolumny.\n
	 @param section - numer kolumny, kt�rej jest zmieniany rozmiar,
	 @param oldSize - poprzedni rozmiar bie��cej kolumny,
	 @param newSize - nowy rozmiar bie��cej kolumny.\n
	 Slot jest wykorzystywany przy synchronizacji kolumn, dba te� aby okienko
	 edycyjne pokazywane w okre�lonej kom�rce bie��cego elementu zachowa�o
	 pozycje.
	 */
	void slotHeaderSizeChange( int section, int oldSize, int newSize );

	/** Metoda powoduje pokazanie na pozycji bie��cego elementu, w podanej
	 kolumnie okienka edycyjnego.\n
	 @param column - numer kolumny, w kt�rej nale�y pokaza� okienko edycyjne.
	 */
	void slotShowEditBox( int column );


	/** Slot uruchamia wstawienie podanego elementu na list�.\n
	 @param urlInfo - informacja o elemencie.\n
	 Wewn�trz jest wywo�ywana funkcja wstawiaj�ca element na list�.
	 */
	void slotInsertItem( const UrlInfoExt & urlInfo );

	/** Slot usuwa element o podanej nazwie z listy zaznaczonych.\n
	 @param name - nazwa elementu do usuni�cia.
	 */
	void slotRemoveNameFromSIL( const QString & name );

private slots:
	/** Funkcja wywo�ywana po klikni�ciu w jedn� z kolumn.\n
	 @param section - numer klikni�tej kolumny.\n
	 Je�li kursor jest niewidoczny, wtedy przesuwany jest widok tak, aby kursor by�
	 pokazany. Inicjowane s� tu sk�adowe: kierunku sortowania (@em mAscending) oraz
	 @em mSortColumn (okre�la, po kt�rej kolumnie jest sortowana lista)
	 */
	void slotHeaderClicked( int section );

	/** Slot wywo�ywany w momencie przewijania zawarto�ci panelu.\n
	 @param contentsY - pozycja Y widoku.\n
	 Je�li sk�adowa @em m_bNotHiddenFocus ma warto�� TRUE, wtedy wykonywana jest tutaj
	 korekcja pozycji kursora, je�li ten wyjdzie poza g�rny lub dolny margines.
	 W efekcie czego kursor jest zawsze widoczny na panelu podczas jego przewijania.
	*/
	void slotChangeVslider( int contentsY );

	/** Slot wywo�ywany w momencie przewijania suwakiem poziomym listy.\n
	 Nast�puje tutaj tylko odrysowanie bie��cego elementu, poniewa� nie jest to
	 robione automatycznie.
	 */
	void slotChangeHSlider( int );

	/** Slot powoduje automatyczne przesuwanie si� zawarto�ci listy.\n
	 Je�li kursor myszy znajduje poza g�rnym marginesem, wtedy lista przesuwa si�
	 w d�, natomiast kiedy jest poza dolnym to lista pod��a w g�r�.
	 */
	void slotDoAutoScroll();

signals:
	/** Sygna� jest wysy�any w momencie, gdy nast�pi potwierdzenia (wci�ni�ty
	 zostanie Return/Enter) podczas szybkiej zmiany warto�ci kom�rki.\n
	 @param column - numer kolumny, w kt�rej nast�puje zmiana,
	 @param oldText - poprzednia warto�� tekstowa w kom�rce,
	 @param newText - nowa warto�� tekstowa dla kom�rki.\n
	 Uruchamia on procedure podejmuj�c� pr�be zmian� atrybutu dla bie��cego pliku.
	 */
	void signalRenameCurrentItem( int column, const QString & oldText, const QString & newText );

	/** Sygna� jest wysy�any w po uaktualnieniu listy i zmianie katalogu w widoku
	 drzewa.\n
	 @param numOfSelectedItems - liczba aktualnie zaznaczonych element�w,
	 @param numOfAllItems - liczby wszystkich element�w,
	 @param weightOfSelected - waga zaznaczonych element�w.\n
	 Wymusza on uaktualnienie informacji na statusie bie��cego panela.
	 */
	void signalUpdateOfListStatus( uint numOfSelectedItems, uint numOfAllItems, uint weightOfSelected );

	/** Sygna� powoduje uaktualnienie informacji o bie��cej �cie�ce na jej widoku.\n
	 @param url - nazwa nowej �cie�ki.\n
	 W przypadku widoku drzewa sygna� jest wywo�ywany przy ka�dej zmienie
	 pozycji kursora na li�cie. Dla obydwu widok�w jest zawsze wywo�ywany gdy
	 focus zostanie postawiony na li�cie.
	 */
	void signalPathChanged( const QString & url );

	/** Sygna� jest wysy�any w momencie wstawiania element�w na list�.\n
	 @param doneInPercent - warto�� procentowa wstawionych ju� element�w,
	 w stosunku do wszystkich z bie��cego katalogu.\n
	 Powoduje on uaktualnianie paska post�pu dla listowania bie��cego katalogu.
	 */
	void signalSetProgress( int doneInPercent );


	/** Sygna� jest wysy�any podczas pr�by otworzenia bie��cego pliku lub katalogu.\n
	 @param ui - rozszerzona informacja o pliku.\n
	 Powoduje on uruchomienie procedury zmiany katalogu na podany lub otwarcie podgl�du pliku.
	 */
	void signalOpen( const UrlInfoExt & ui );

	/** Sygna� jest wysy�any tylko na widoku drzewa w przypadku, gdy bie�acy
	 katalog lub archiwum jest zamykane.
	 */
	void signalClose(); // current list

};



inline void ListView::setSecondColorOfBg( const QColor & c )
{
	mSecondColorOfBg = c;  triggerUpdate();
}

inline void ListView::setTextColorUnderCursor( const QColor & c )
{
	mTextColorUnderCursor = c;  triggerUpdate();
}

inline void ListView::setHighlightTextColorUnderCursor( const QColor & c )
{
	mTextColorHighlightUnderCursor = c;  triggerUpdate();
}

inline const QColor & ListView::secondColorOfBg() const     { return mSecondColorOfBg;     }
inline const QColor & ListView::gridColor() const           { return mGridColor;           }
inline const QColor & ListView::cursorColor() const         { return mFullCursorColor;      }
inline const QColor & ListView::frameInsCursorColor() const { return mFrameInsCursorColor; }
inline const QColor & ListView::frameOutCursorColor() const { return mFrameOutCursorColor; }

inline bool ListView::grid() const                 { return m_bShowGrid;            }
inline bool ListView::enabledTwoColorsOfBg() const { return m_bEnableTwoColorsOfBg; }
inline bool ListView::isFullCursor() const         { return m_bFullCursor;          }

inline void ListView::setShowGrid( bool show )               { m_bShowGrid = show;	     triggerUpdate();       }
inline void ListView::setEnableTwoColorsOfBg( bool enable )  { m_bEnableTwoColorsOfBg = enable; triggerUpdate(); }
inline void ListView::setGridColor( const QColor & c )       { mGridColor = c;        triggerUpdate();       }
inline void ListView::setFullCursorColor( const QColor & c ) { mFullCursorColor = c;   triggerUpdate();       }

inline const QColor & ListView::highlightBgColor() const      { return colorGroup().highlight();       }
inline const QColor & ListView::highlightedTextColor() const  { return colorGroup().highlightedText(); }
inline const QColor & ListView::textColor() const             { return colorGroup().text();            }
inline const QColor & ListView::textColorUnderCursor() const  { return mTextColorUnderCursor;          }
inline const QColor & ListView::highlightedTextColorUnderCursor() const  { return mTextColorHighlightUnderCursor; }

inline void ListView::setFullCursor( bool fullCursor )
{
	m_bFullCursor = fullCursor;  m_bDblFrameCursor = false;
}

inline void ListView::setDblFrameCursor( bool dblFrameCursor )
{
	m_bDblFrameCursor = dblFrameCursor;  m_bFullCursor = false;
}

inline void ListView::setCursorColor( const QColor & c )
{
	setFullCursorColor( c );  setDblFrameCursorColors( c, c );
}

inline void ListView::setDblFrameCursorColors( const QColor & colInside, const QColor & colOutside )
{
	mFrameInsCursorColor = colInside;  mFrameOutCursorColor = colOutside;
}

#endif
