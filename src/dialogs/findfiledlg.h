/***************************************************************************
                          findfiledlg  -  description
                             -------------------
    begin                : pon mar 14 2005
    copyright            : (c) 2005 by Piotr Mierzwiński
    email                : peterm@go2.pl

    copyright            : See COPYING file that comes with this project

 $Id$
 ***************************************************************************/
#ifndef FINDFILEDLG_H
#define FINDFILEDLG_H

#include <QKeyEvent>
#include <QCloseEvent>


#include "ui_findfiledialog.h"

#include "findcriterion.h"

class QTreeView;
//class QMenu;

class Filters;
class UrlInfoExt;
//class FindCriterion;
class FilesAssociation;
/**
	@author Piotr Mierzwiński
*/
class FindFileDlg : public QDialog, public Ui::FindFileDialog
{
	Q_OBJECT
public:
	FindFileDlg();
	~FindFileDlg();

	void init( const QString & sCurrentPath, FilesAssociation *pFA );

	void findFinished();
	void updateFindStatus( uint nMatches, uint nFiles, uint nDirectories );
	void insertMatchedItem( const UrlInfoExt & uUI );

private:
	Filters *m_pFilters;
	FindCriterion m_FindCriterion;

	//QMenu *m_pDirectoryMenu;
	bool m_bFindPause;

private slots:
	void slotFindStart();
	void slotFindStop();
	void slotPause();

	void slotClearList();
	//void slotDoubleClickItem( Q3ListViewItem *pItem );
	//void slotRightButtonClickedOnList( Q3ListViewItem * pItem, const QPoint & pos, int );

	void slotShowInPanel();

protected:
	void closeEvent( QCloseEvent * pCE );
	void keyPressEvent( QKeyEvent * pKE );

signals:
	void signalClose();
	void signalPause( bool );
	void signalDeleteFile( const QString & );
	void signalJumpToTheFile( const QString & );
	void signalViewFile( const QString & , bool );
	void signalFindStart( const FindCriterion & );
	void signalShowInPanel( QTreeView * );

};

#endif
