/***************************************************************************
                          inputdlg.cpp  -  description
                             -------------------
    begin                : pi� mar 11 2005
    copyright            : (C) 2005 by Piotr Mierzwi�ski
    email                : peterm@go2.pl

    copyright            : See COPYING file that comes with this project

 $Id$
 ***************************************************************************/
#include <QMap>
//#include <QSettings>
#include <QCheckBox>

#include "vfs.h"
#include "inputdlg.h"
#include "squeezelabel.h"
#include "locationchooser.h"

// TODO
// Dodac fun. getText bez param.: 'bool & bP1', 'bool & bAlwaysOvr', 'bool bP3', 'bool bDirsOnly'
// wart.param bP1, bAlwaysOvr, bP3, bDirsOnly  wczyKrochmalna 2/1015tywac z pliku konf. (trzeba by wtedy przekazywac 'klucze')
// dla MakeLink od 'bP1': mHardLink, mAlwaysOverwrite, editOnly, FALSE, domyslna_wart=(0)
// dla Copy/Move od 'bP1': mSavePermision, mAlwaysOverwrite, domyslna_wart=(FALSE), domyslna_wart=(TRUE), domyslna_wart=(0)

InputDlg::InputDlg()
{
	setupUi(this);
}

InputDlg::~InputDlg()
{
}

QString InputDlg::getText( const QString & sInfoFileText, const QString & sInitEditText, VFS::Operation eOperation, const QString & sKeyListReading, bool & bOkPressed, bool & bP1, bool & bAlwaysOvr, bool bP3, bool bDirsOnly )
{
	if ( eOperation != VFS::MakeLink && eOperation != VFS::Copy && eOperation != VFS::Move && eOperation != VFS::Rename && eOperation != VFS::MakeDir && eOperation != VFS::Touch )
		return QString::null;

	InputDlg *pDlg = new InputDlg;
	Q_CHECK_PTR( pDlg );

	QString sInitTxt = sInitEditText;
	pDlg->m_pLocationChooser->setOriginalPath( sInitEditText );
	pDlg->m_pLocationChooser->setMenuItemVisible( LocationChooser::CURRENT_DIR, FALSE );

	if ( eOperation == VFS::MakeLink ) {
		delete pDlg->m_pBackgroundCpyBtn;
		pDlg->m_pLocationChooser->setGetMode( LocationChooser::GetFile );

		if ( bP3 ) { // hides checkBoxes, becouse it's edit link operation
			pDlg->m_pCheckBox1->hide();
			pDlg->m_pCheckBox2->hide();
		}
		else
			pDlg->m_pCheckBox1->setText( QObject::tr("Hard link") );
	}
	else
	if ( eOperation == VFS::Copy || eOperation == VFS::Move ) {
		pDlg->m_pBackgroundCpyBtn->setEnabled( FALSE ); // 'in background' not supported yet
	}
	else { // other operation (Rename/MakeDir/Touch)
		pDlg->m_pCheckBox1->hide();
		pDlg->m_pCheckBox2->hide();
		delete pDlg->m_pBackgroundCpyBtn;
		pDlg->m_pLocationChooser->setButtonSide( LocationChooser::NoneButton );
		if ( eOperation == VFS::Touch )
			pDlg->m_pLocationChooser->setGetMode( LocationChooser::GetFile );
	}

	pDlg->m_pCheckBox1->setChecked( bP1 ); // savePerm or hard link
	pDlg->m_pCheckBox2->setChecked( bAlwaysOvr );


	QMap <VFS::Operation, QString> mActions, mCaptions;
	mActions.insert( VFS::Copy,   QObject::tr("copy to") );
	mActions.insert( VFS::Move,   QObject::tr("move to") );
	mActions.insert( VFS::Rename, QObject::tr("new name for current file") );
// 	mActions.insert( MakeDir, "" );
// 	mActions.insert( Touch, "" );
	if ( bP3 ) // editOnly for MakeLink operation is TRUE
		mActions.insert( VFS::MakeLink, QObject::tr("new path for current link") );
	else
		mActions.insert( VFS::MakeLink, QObject::tr("new link name") );

	mCaptions.insert( VFS::Copy,    QObject::tr("Copying files to")+" ..." );
	mCaptions.insert( VFS::Move,    QObject::tr("Moving files to")+" ..." );
	mCaptions.insert( VFS::Rename,  QObject::tr("Rename copied file") );
	mCaptions.insert( VFS::MakeDir, QObject::tr("Making directory") );
	mCaptions.insert( VFS::Touch,   QObject::tr("Making empty file") );
	if ( bP3 ) // editOnly for MakeLink operation is TRUE
		mCaptions.insert( VFS::MakeLink, QObject::tr("Editing link") );
	else
		mCaptions.insert( VFS::MakeLink, QObject::tr("Making link") );


	if ( eOperation != VFS::Rename ) // init list only for MakeLink, Copy and Move operation
		pDlg->m_pLocationChooser->setKeyListReading( sKeyListReading, TRUE ); // TRUE - read and init settings

	((QWidget *)pDlg)->setWindowTitle( mCaptions[eOperation]+" - QtCommander" );
	if ( eOperation != VFS::MakeDir && eOperation != VFS::Touch ) {
		pDlg->mActionLabel->setText( mActions[eOperation]+" :" );
		pDlg->m_pLocationChooser->setText( sInitEditText );
	}
	else
	if ( pDlg->m_pLocationChooser->text().isEmpty() ) // list (dir.names) not read
		pDlg->m_pLocationChooser->setText( sInitEditText );

	pDlg->m_pFileInfoLabel->setText( sInfoFileText );
	pDlg->m_pLocationChooser->selectAll();
	pDlg->m_pLocationChooser->setFocus();

	pDlg->m_bShowDirsOnly = bDirsOnly;
	pDlg->m_sTargetPath = (bDirsOnly) ? sInitEditText : sInitEditText.left( sInitEditText.findRev('/') );

	QString sTargetFile = sInitEditText;

	//pDlg->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)5, (QSizePolicy::SizeType)0, 0, 0, pDlg->sizePolicy().hasHeightForWidth() ) ); // prefered
	//pDlg->adjustSize();
	//bOkPressed = (((QDialog *)pDlg)->exec() == QDialog::Accepted);
	bOkPressed = (pDlg->exec() == QDialog::Accepted);

	if ( bOkPressed ) {
		sTargetFile = pDlg->m_pLocationChooser->text();
		bP1         = pDlg->m_pCheckBox1->isChecked(); // savePerm or hard link
		bAlwaysOvr  = pDlg->m_pCheckBox2->isChecked();
		pDlg->m_pLocationChooser->insertItem( sTargetFile );
		pDlg->m_pLocationChooser->saveSettings(); // save list entries
	}

	delete pDlg;

	return sTargetFile;
}

