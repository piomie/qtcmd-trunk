/***************************************************************************
                          findfiledlg  -  description
                             -------------------
    begin                : pon mar 14 2005
    copyright            : (C) 2005 by Piotr Mierzwi�ski
    email                : peterm@go2.pl

    copyright            : See COPYING file that comes with this project

 $Id$
 ***************************************************************************/

//#include <qsettings.h>

#include <QMenu>
#include <QKeyEvent>
#include <QTreeView>
#include <QDateTime>
#include <QCheckBox>
#include <QTabWidget>
#include <QCloseEvent>
#include <QDialog>


#include "filters.h"
#include "urlinfoext.h"
#include "locationchooser.h"
#include "filesassociation.h"

#include "functions.h" // for formatNumber()
#include "messagebox.h"
#include "systeminfo.h"   // for getOwners()
#include "fileinfoext.h"

#include "findfiledlg.h"


FindFileDlg::FindFileDlg()
	: FindFileDialog()
{
	setupUi(this);

	QList <QVariant> headerData;
	headerData << tr("Name") << tr("Size") << tr("Time") << tr("Permission") << tr("Owner") << tr("Group") << tr("Location");
	// konfigurowalne kolumny (mozliwosc dodawania i ususania)

	//m_pFoundFilesListView->addColumn("size", 10); // hidden column for real files size
	m_pFoundFilesListView->setColumnWidth(7, 0);

	connect( m_pFindBtn, SIGNAL( clicked() ), this, SLOT( slotFindStart() ) );
	connect( m_pStopBtn, SIGNAL( clicked() ), this, SLOT( slotFindStop() ) );
	connect( m_pPauseBtn, SIGNAL( clicked() ), this, SLOT( slotPause() ) );
	connect( m_pClearListBtn, SIGNAL( clicked() ), this, SLOT( slotClearList() ) );
	// TODO m_pFoundFilesListView: add support for doubleClicked  - call: slotDoubleClickItem
	// TODO m_pFoundFilesListView: add support for rightButtonClicked - call: rightButtonClicked

	connect( m_pShowInPanelBtn, SIGNAL( clicked() ), this, SLOT( slotShowInPanel() ) );
}

FindFileDlg::~FindFileDlg()
{
}


void FindFileDlg::init( const QString & sCurrentPath, FilesAssociation */*pFA*/ )
{
	setCaption( tr("Find file(s)")+" - QtCommander" );
	//QSettings settings;

	// --- init first Tab
	// 	int caseSensitiveName = readNumEntry( "/qtcmd/FilesPanel/FindCaseSensitiveName", -1 );
	// 	m_pNameCaseSensitiveChkBox->setChecked( (caseSensitiveName<0) ? FALSE : (bool)caseSensitiveName );
	setTabOrder( m_pNameChooser, m_pLocationChooser );
	m_pLocationChooser->setCurrentPath( sCurrentPath );
	m_pLocationChooser->setKeyListReading( "/qtcmd/FilesSystem/FindLocations", TRUE );
	m_pLocationChooser->setOriginalPath( m_pLocationChooser->text() );

	m_pNameChooser->setKeyListReading( "/qtcmd/FilesSystem/FindFileNames", TRUE );
	m_pNameChooser->setFocus();

	// --- second Tab ( NOT YET SUPPORTED )
	m_pContainsTextEdit->setEnabled( FALSE );

	// --- init third Tab
	m_pFirstDateTimeEdit->setDateTime( QDateTime::currentDateTime() );
	m_pSecondDateTimeEdit->setDateTime( QDateTime::currentDateTime() );

	QStringList slList = SystemInfo::getAllUsers();
	m_pOwnerComboBox->insertStringList( slList );
    slList = SystemInfo::getAllGroups();
	m_pGroupComboBox->insertStringList( slList );

	// --- init forth Tab
	m_pFindRecursiveChkBox->setChecked( TRUE );
	// init the filters
	m_pFilters = new Filters( /*pFA*/ );
	for ( uint i=0; i<m_pFilters->baseFiltersNum(); i++ )
		m_pFiltersComboBox->insertItem( m_pFilters->name(i) );

	m_pSearchInSelectedChkBox->setEnabled( FALSE ); // NOT YET SUPPORTED

	findFinished();
	m_pCloseBtn->setAccel( Qt::Key_Escape );
}


void FindFileDlg::findFinished()
{
	m_bFindPause = FALSE;

	m_pFindBtn->setEnabled( TRUE );
	m_pStopBtn->setEnabled( FALSE );
	m_pPauseBtn->setEnabled( FALSE );
	// 	m_pTabWidget->setEnabled( TRUE );
	for (int i=0; i<4; i++)
		m_pTabWidget->page(i)->setEnabled( TRUE );

	m_pClearListBtn->setEnabled( TRUE );
	m_pStatusInfoLab->setText( tr("Ready.") );

//	if (m_pFoundFilesListView->childCount() > 0)
//		m_pFoundFilesListView->setCurrentItem( m_pFoundFilesListView->firstChild() );

//	m_pClearListBtn->setEnabled( (m_pFoundFilesListView->childCount() > 0) );
//	m_pShowInPanelBtn->setEnabled( (m_pFoundFilesListView->childCount() > 0) );
}


void FindFileDlg::updateFindStatus( uint nMatches, uint nFiles, uint nDirs )
{
	QString sMsg = QString(tr("Matched: %1 from %2 files and %3 directories")).arg(nMatches).arg(nFiles).arg(nDirs);

	m_pStatusDetailsLab->setText( sMsg );
}


void FindFileDlg::insertMatchedItem( const UrlInfoExt & uUI )
{
	QString sName = FileInfoExt::fileName( uUI.name() );
	QString sLocation = FileInfoExt::filePath( uUI.name() );

/*
	(void) new Q3ListViewItem(
		m_pFoundFilesListView
		, sName, uUI.sizeStr(BKBMBGBformat)
		, uUI.lastModifiedStr(), uUI.permissionsStr(), uUI.owner(), uUI.group(), sLocation
		, uUI.sizeStr() // hidden column
	);
*/
}


void FindFileDlg::slotFindStart()
{
	QString sCurrentComboText;

	// --- check and update the comboBoxes
	sCurrentComboText = m_pNameChooser->text();
	m_FindCriterion.name = sCurrentComboText;
	if ( sCurrentComboText.isEmpty() ) {
		m_pTabWidget->setCurrentPage( 0 );
		MessageBox::critical( this, tr("Don't gived a name to find")+" !" );
		m_pNameChooser->setFocus();
		return;
	}
	else
		m_pNameChooser->insertItem( sCurrentComboText, 0 );

	sCurrentComboText = m_pLocationChooser->text();
	m_FindCriterion.location = sCurrentComboText;
	if ( sCurrentComboText.isEmpty() ) {
		m_pTabWidget->setCurrentPage( 0 );
		MessageBox::critical( this, tr("Don't gived a location for searching !") );
		m_pLocationChooser->setFocus();
		return;
	}
	else
		m_pLocationChooser->insertItem( sCurrentComboText, 0 );

	// --- enables and disables obj.
	m_pStopBtn->setEnabled( TRUE );
	m_pFindBtn->setEnabled( FALSE );
	m_pPauseBtn->setEnabled( TRUE );
	m_pShowInPanelBtn->setEnabled( FALSE );

	//	m_pTabWidget->setEnabled( FALSE );
	for (uint i=0; i<4; i++)
		m_pTabWidget->page(i)->setEnabled( FALSE );
	m_pClearListBtn->setEnabled( FALSE );

	// ---initialize find criterion obj.
	m_FindCriterion.caseSensitiveName = m_pNameCaseSensitiveChkBox->isChecked();

	m_FindCriterion.containText       = m_pContainsTextEdit->text();
	m_FindCriterion.caseSensitiveText = m_pTextCaseSensitiveChkBox->isChecked();
	m_FindCriterion.includeBinFiles   = m_pIncludeBinaryChkBox->isChecked();
	m_FindCriterion.regExpText        = m_pTextRegExpChkBox->isChecked();

	m_FindCriterion.checkTime  = m_pTimeChkBox->isChecked();
	m_FindCriterion.firstTime  = m_pFirstDateTimeEdit->dateTime();
	m_FindCriterion.secondTime = m_pSecondDateTimeEdit->dateTime();

	m_FindCriterion.checkFileSize = (m_pFileSizeChkBox->isChecked()) ? (FindCriterion::CheckFileSize)m_pFileSizeQualifiersComboBox->currentItem() : FindCriterion::NONE;
	qint64 nFileSize = (m_FindCriterion.checkFileSize!=FindCriterion::NONE) ? m_pFileSizeSpinBox->value() : -1;
	if ( nFileSize >= 0 ) {
		uint suffixesId = m_pFileSizeSuffixesComboBox->currentItem();
		if ( suffixesId == 1 )
			nFileSize *= 1024; // for KB
		if ( suffixesId == 2 )
			nFileSize *= 1073741824; // for MB
		if ( suffixesId == 3 )
			nFileSize *= (1073741824*1024); // for GB

		m_FindCriterion.fileSize = nFileSize;
	}

	QStringList slList;
	slList << m_pOwnerComboBox->currentText();
    int nUID = (int) SystemInfo::getCurrentUserID();
	slList = QStringList() << m_pGroupComboBox->currentText(); // get Id for current (in combo) user name
    int nGID = (int) SystemInfo::getCurrentUserGroupID(); // get Id for current (in combo) group name

	m_FindCriterion.checkFileOwner = m_pFileOwnerChkBox->isChecked();
	m_FindCriterion.checkFileGroup = m_pFileGroupChkBox->isChecked();
	m_FindCriterion.ownerId        = (m_FindCriterion.checkFileOwner) ? nUID : -1;
	m_FindCriterion.groupId        = (m_FindCriterion.checkFileGroup) ? nGID : -1;

	m_FindCriterion.kindOfFiles        = (FindCriterion::KindOfFiles)m_pFiltersComboBox->currentItem();
	m_FindCriterion.findRecursive      = m_pFindRecursiveChkBox->isChecked();
	m_FindCriterion.searchIntoSelected = m_pSearchInSelectedChkBox->isChecked();
	m_FindCriterion.stopIfXMatched     = m_pStopMatchesChkBox->isChecked();
	m_FindCriterion.stopAfterXMaches   = (m_FindCriterion.stopIfXMatched) ? m_pMatchesNumSpinBox->value() : -1;

	// --- make an destination action
	m_pStatusInfoLab->setText( tr("Searching")+"..." );

	//m_pFoundFilesListView->clear(); // hasNoMember
	emit signalFindStart( m_FindCriterion );
	m_pFoundFilesListView->setFocus();
}

void FindFileDlg::slotFindStop()
{
	emit signalFindStart( FindCriterion() ); // sends an empty obj.

	m_pPauseBtn->setText( tr("&Pause") );
	findFinished();
}


void FindFileDlg::slotPause()
{
	m_bFindPause = ! m_bFindPause;

	emit signalPause( m_bFindPause );

	m_pStatusInfoLab->setText( m_bFindPause ? tr("Pause.") : tr("Searching")+"..." );
	m_pPauseBtn->setText( m_bFindPause ? tr("Co&ntinue") : tr("&Pause") );

	m_pShowInPanelBtn->setEnabled( m_bFindPause );
	m_pClearListBtn->setEnabled( m_bFindPause );

	for (int i=0; i<4; i++)
		m_pTabWidget->page(i)->setEnabled( m_bFindPause );

//	if (m_pFoundFilesListView->childCount() > 0)
//		m_pFoundFilesListView->setCurrentItem( m_pFoundFilesListView->firstChild() );
}

/*
void FindFileDlg::slotDoubleClickItem( Q3ListViewItem *pItem )
{
	if ( m_pFindBtn->isEnabled() ) {
		emit signalJumpToTheFile( pItem->text(6)+pItem->text(0) ); // then file location+name
		close();
	}
}
*/

void FindFileDlg::slotClearList()
{
//	m_pFoundFilesListView->clear(); // has no member
	m_pStatusDetailsLab->clear();
}

/** Obs�uga zdarzenia zamkni�cia bie��cego okna.\n
 @param pCE - zdarzenie zamkni�cia okna.\n
 Uruchamiana jest tu procedura zatrzymania operacji wyszukiwania po czym
 zapisywane s� niekt�re ustawienia (listy: szukanych nazw i lokacji).
 */
void FindFileDlg::closeEvent( QCloseEvent * pCE )
{
	slotFindStop();

	// --- save names and locations to the config file
	//QSettings settings;
	m_pNameChooser->saveSettings();
	m_pLocationChooser->saveSettings(); // save list entries

	// --- close current dialog
	emit signalClose();
	pCE->ignore();
}

/** Obsluga klawiatury dla widoku listy znalezionych plik�w.
*/
void FindFileDlg::keyPressEvent( QKeyEvent *pKE )
{
	if ( ! m_pFoundFilesListView->hasFocus() ) {
		if ( pKE->key() == Qt::Key_Enter || pKE->key() == Qt::Key_Return )
			slotFindStart();
		return;
	}
	return;
/*
	bool bFindEnabled = m_pFindBtn->isEnabled();
	//Q3ListViewItem *pItem = m_pFoundFilesListView->currentItem();
	if ( pItem == NULL )
		return;

	QString sFullFileName = pItem->text(6)+pItem->text(0);

	switch( pKE->key() )
	{
	case Qt::Key_Enter:
	case Qt::Key_Return:
		slotDoubleClickItem( pItem );
		break;

	case Qt::Key_F3:
		if ( bFindEnabled )
			emit signalViewFile( sFullFileName, FALSE );
		break;

	case Qt::Key_F4:
		if ( bFindEnabled )
			emit signalViewFile( sFullFileName, TRUE );
		break;

	case Qt::Key_F8:
	case Qt::Key_Delete:
		if ( bFindEnabled )
			emit signalDeleteFile( sFullFileName );
		break;

	default:
		break;
	}
*/
}

/*
void FindFileDlg::slotRightButtonClickedOnList( Q3ListViewItem * pItem, const QPoint & pos, int )
{
	if ( ! pItem || ! m_pFindBtn->isEnabled() )
		return;

	enum { GOTO=1, VIEW, EDIT, DELETE };

	QMenu menu;
	menu.addMenu( tr("&Go to file")+"\tReturn/Enter" ); // TODO inna obsluga id dla pozycji menu
	menu.addMenu( tr("&View file")+"\tF3" );
	menu.addMenu( tr("&Edit file")+"\tF4" );
	menu.addMenu( tr("&Delete file")+"\tDelete/F8" );
// 	menu.addMenu( tr("&Go to file")+"\tReturn/Enter", GOTO );
// 	menu.addMenu( tr("&View file")+"\tF3", VIEW );
// 	menu.addMenu( tr("&Edit file")+"\tF4", EDIT );
// 	menu.addMenu( tr("&Delete file")+"\tDelete/F8", DELETE );

	//int nID = menu.exec( pos );
	QAction *a = menu.exec( pos ); // TODO obsluga menu dla zwr.  QAction

// 	QString sFullFileName = pItem->text(6)+pItem->text(0);
//
// 	if ( nID == GOTO )
// 		slotDoubleClickItem( pItem );
// 	else if ( nID == VIEW )
// 		emit signalViewFile( sFullFileName, FALSE );
// 	else if ( nID == EDIT )
// 		emit signalViewFile( sFullFileName, TRUE );
// 	else if ( nID == DELETE )
// 		emit signalDeleteFile( sFullFileName );

}
*/

void FindFileDlg::slotShowInPanel()
{
	hide();
	emit signalShowInPanel( m_pFoundFilesListView );
	close(true);
}

