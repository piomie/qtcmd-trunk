/***************************************************************************
                          aboutdlg.cpp  -  description
                             -------------------
    begin                : �ro gru 10 2008
    copyright            : (C) 2008 by Piotr Mierzwi�ski, Mariusz Borowski
    email                : piom@nes.pl, mariusz@nes.pl

    copyright            : See COPYING file that comes with this project

 $Id$
 ***************************************************************************/

#include "aboutdlg.h"


AboutDlg::AboutDlg( const QString & sVersion )
{
	setupUi(this);
	m_pNameAndVerLab->setText("QtCommander "+sVersion);

	QPalette parentPalette = m_pAboutTabWidget->palette();
	QPalette newPalette = palette();
	newPalette.setBrush(QPalette::Base, parentPalette.window());
	m_pAboutProgramTxtBrowser->setPalette(newPalette);
}

