/***************************************************************************
                          ftpconnectdlg  -  description
                             -------------------
    begin                : wto mar 15 2005
    copyright            : (c) 2005 by Piotr Mierzwi�ski
    email                : peterm@go2.pl

    copyright            : See COPYING file that comes with this project

 $Id$
 ***************************************************************************/
#ifndef FTPCONNECTDLG_H
#define FTPCONNECTDLG_H

#include "ui_ftpconnectdialog.h"

/**
	@author Piotr Mierzwi�ski
*/
class FtpConnectDlg : public QDialog, Ui::FtpConnectDialog
{
	Q_OBJECT
public:
	FtpConnectDlg();
	~FtpConnectDlg();

	/** Statyczna funkcja pokazuj�ca g��wne okno po��czenia FTP.
	 * @param sInputURL - URL inicjuj�cy dialog,
	 * @return TRUE je�li po��czenie ma zosta� wykonane, w przeciwnym wypadku FALSE.
	*/
	static bool showDialog( QString & sInputURL );

private:
	/** Metoda inicjuj�ca dialog sk�adowymi nowego URLa ('sInputURL'). Nazwy
	 * urzytkownik�w, s� odczytywane z pliku konfiguracyjnego.
	 * @param sInputURL - URL inicjuj�cy dialog.
	*/
	void initSession( const QString & sInputURL );

	/** Funkcja generuje nowy URL na podstawie warto�ci p�l dialogu.
	 * @param sNewURL - sformatowany URL (wg. wzorca "ftp://user:pass@hostdir")
	*/
	void makeNewURL( QString & sNewURL );

private:
	QStringList m_sUsersList;

};

#endif
