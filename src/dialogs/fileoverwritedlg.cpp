/***************************************************************************
                          fileoverwritedlg  -  description
                             -------------------
    begin                : wto mar 15 2005
    copyright            : (C) 2005 by Piotr Mierzwiński
    email                : peterm@go2.pl

    copyright            : See COPYING file that comes with this project

 $Id$
 ***************************************************************************/
//#include <QtGui>
#include <QToolTip>

#include "urlinfoext.h"
#include "squeezelabel.h"
#include "fileoverwritedlg.h"


FileOverwriteDlg::FileOverwriteDlg()
{
	setupUi(this);

	m_pButtonGroup = new QButtonGroup;
	m_pButtonGroup->addButton(m_pYesBtn, Yes);
	m_pButtonGroup->addButton(m_pNoBtn, No);
	m_pButtonGroup->addButton(m_pDiffSizeBtn, DiffSize);
	m_pButtonGroup->addButton(m_pRenameBtn, Rename);
	m_pButtonGroup->addButton(m_pAllBtn, All);
	m_pButtonGroup->addButton(m_pUpdateBtn, Update);
	m_pButtonGroup->addButton(m_pNoneBtn, None);
	m_pButtonGroup->addButton(m_pCancelBtn, Cancel);
	connect( m_pButtonGroup, SIGNAL(buttonClicked( int )), this, SLOT(done(int)) );
}


FileOverwriteDlg::~FileOverwriteDlg()
{
}

int FileOverwriteDlg::show( const UrlInfoExt & uiTargetFileURL, const UrlInfoExt & uiSourceFileURL )
{
	FileOverwriteDlg *pDlg = new FileOverwriteDlg;
	Q_CHECK_PTR( pDlg );

	pDlg->setWindowTitle( tr("File overwriting")+" - QtCommander" );

	pDlg->m_pTargetFileNameLabValue->setText( "\""+uiTargetFileURL.name()+"\"" );
	pDlg->m_pSourceDateLabValue->setText( uiSourceFileURL.lastModifiedStr() );
	pDlg->m_pTargetDateLabValue->setText( uiTargetFileURL.lastModifiedStr() );
	pDlg->m_pSourceSizeLabValue->setText( uiSourceFileURL.sizeStr(BKBMBGBformat) );
	pDlg->m_pTargetSizeLabValue->setText( uiTargetFileURL.sizeStr(BKBMBGBformat) );

	pDlg->m_pSourceSizeLabValue->setToolTip( uiSourceFileURL.sizeStr(THREEDIGITSformat) );
	pDlg->m_pTargetSizeLabValue->setToolTip( uiTargetFileURL.sizeStr(THREEDIGITSformat) );

	// --- show dialog
	int nResult = ((QDialog *)pDlg)->exec();

	delete pDlg;
	pDlg = 0;

	return nResult;
}
