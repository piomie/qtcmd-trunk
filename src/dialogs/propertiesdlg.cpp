/***************************************************************************
                          propertiesdlg  -  description
                             -------------------
    begin                : pon mar 14 2005
    copyright            : (C) 2005 by Piotr Mierzwiński
    email                : peterm@go2.pl

    copyright            : See COPYING file that comes with this project

 $Id$
 ***************************************************************************/

#include <sys/stat.h> // for constans: S_ISUID, S_ISGID, S_ISVTX

#include <QCheckBox>
#include <QComboBox>
#include <QLineEdit>
#include <QTabWidget>
#include <QPushButton>

#include "filters.h"
#include "functions.h"
#include "urlinfoext.h"
#include "messagebox.h"
#include "systeminfo.h"   // for getOwners()
#include "fileinfoext.h"
#include "squeezelabel.h"

#include "propertiesdlg.h"

//#include <QtGui>

PropertiesDlg::PropertiesDlg()
{
	setupUi(this);

	connect( mCancelBtn, SIGNAL( clicked() ), this, SLOT( close() ) );
	connect( mOkBtn, SIGNAL( clicked() ), this, SLOT( slotOkPressed() ) );
	connect( mNameComboBox, SIGNAL( activated(const QString&) ), this, SLOT( slotInitAllTabs(const QString&) ) );
	connect( mRefreshBtn, SIGNAL( clicked() ), this, SLOT( slotRefresh() ) );
	connect( mStopBtn, SIGNAL( clicked() ), this, SLOT( slotStop() ) );

	// --- build buttons groups
	m_pClassButtonGroup = new QButtonGroup;
	m_pClassButtonGroup->addButton(m_pClassUserChkBox,  USER_PERM);
	m_pClassButtonGroup->addButton(m_pClassGroupChkBox, GROUP_PERM);
	m_pClassButtonGroup->addButton(m_pClassOtherChkBox, OTHER_PERM);
	m_pClassButtonGroup->setExclusive(FALSE);
	connect( m_pClassButtonGroup, SIGNAL( buttonClicked(int) ), this, SLOT( slotInitPermissonForClass(int) ) );

	m_pReadButtonGroup = new QButtonGroup;
	m_pReadButtonGroup->addButton(m_pReadUserChkBox,  USER_PERM);
	m_pReadButtonGroup->addButton(m_pReadGroupChkBox, GROUP_PERM);
	m_pReadButtonGroup->addButton(m_pReadOtherChkBox, OTHER_PERM);
	m_pReadButtonGroup->setName("m_pReadButtonGroup");
	m_pReadButtonGroup->setExclusive(FALSE);
	connect( m_pReadButtonGroup, SIGNAL( buttonClicked(int) ), this, SLOT( slotCheckGroupButton(int) ) );
	connect( m_pReadGroupBox, SIGNAL( toggled(bool) ), this, SLOT( slotSetButtonGroup(bool) ) );

	m_pWriteButtonGroup = new QButtonGroup;
	m_pWriteButtonGroup->addButton(m_pWriteUserChkBox,  USER_PERM);
	m_pWriteButtonGroup->addButton(m_pWriteGroupChkBox, GROUP_PERM);
	m_pWriteButtonGroup->addButton(m_pWriteOtherChkBox, OTHER_PERM);
	m_pWriteButtonGroup->setName("m_pWriteButtonGroup");
	m_pWriteButtonGroup->setExclusive(FALSE);
	connect( m_pWriteButtonGroup, SIGNAL( buttonClicked(int) ), this, SLOT( slotCheckGroupButton(int) ) );
	connect( m_pWriteGroupBox, SIGNAL( toggled(bool) ), this, SLOT( slotSetButtonGroup(bool) ) );

	m_pExecButtonGroup = new QButtonGroup;
	m_pExecButtonGroup->addButton(m_pExeUserChkBox,  USER_PERM);
	m_pExecButtonGroup->addButton(m_pExeGroupChkBox, GROUP_PERM);
	m_pExecButtonGroup->addButton(m_pExeOtherChkBox, OTHER_PERM);
	m_pExecButtonGroup->setName("m_pExecButtonGroup");
	m_pExecButtonGroup->setExclusive( FALSE );
	connect( m_pExecButtonGroup, SIGNAL( buttonClicked(int) ), this, SLOT( slotCheckGroupButton(int) ) );
	connect( m_pExeGroupBox, SIGNAL( toggled(bool) ), this, SLOT( slotSetButtonGroup(bool) ) );

	// -- need to enabled, becouse Qt default disabled GroupBox with properties checkable
	enableButtonGroup( m_pReadButtonGroup );
	enableButtonGroup( m_pWriteButtonGroup );
	enableButtonGroup( m_pExecButtonGroup );

	// TEMPORARY dialog have disabled properties checkable, but support exist here (does not work properly)
	m_bReadGroupChanged  = FALSE;
	m_bWriteGroupChanged = FALSE;
	m_bWriteGroupChanged = FALSE;
}

PropertiesDlg::~PropertiesDlg()
{
}

// TODO
// 1. Kiedy zaznaczonych jest kilka plikow umozliwic zmiane atrybutow dla kazego z osobna.
//
void PropertiesDlg::init( const QStringList & slFileNameList, FilesAssociation *pFA, const QString & sCurrentLoggedUserName ) // outside call
{
	setCaption( tr("The file(s) properties")+" - QtCommander" );
	m_sLoggedUserName = sCurrentLoggedUserName;
	m_pFilesAssociation = NULL;
	mNameComboBox->clear();
	if ( slFileNameList.count() > 1 )
		mNameComboBox->insertItem( tr("ALL SELECTED") );
	mNameComboBox->insertStringList( slFileNameList );
	initFiltersAndAssociation( pFA );
	slotInitAllTabs( mNameComboBox->text(0) );

	//mStopBtn->setEnabled( FALSE );
	mCancelBtn->setAccel( Qt::Key_Escape );
	if ( mNameComboBox->lineEdit() )
		connect( mNameComboBox->lineEdit(), SIGNAL(returnPressed()), this, SLOT(slotOkPressed()) );
}

void PropertiesDlg::init( const UrlInfoExt & uUrlInfo, FilesAssociation *pFA, const QString & sCurrentLoggedUserName )
{
	initFiltersAndAssociation( pFA );
	mNameComboBox->setCurrentText( uUrlInfo.name() );
	m_sLoggedUserName = sCurrentLoggedUserName;
}

void PropertiesDlg::initFiltersAndAssociation( FilesAssociation *pFA )
{
	m_pFilesAssociation = pFA;
	Filters *pFilters = new Filters( /*pFA*/ );

	for ( uint i=0; i<pFilters->baseFiltersNum(); i++ )
		mFiltersComboBox->insertItem( pFilters->name(i) );

	delete pFilters;
}

void PropertiesDlg::setSize( long long nWeight, int nDirs, int nFiles, bool bBigInfo )
{
	QString sSizeInfo = formatNumber(nWeight, BKBMBGBformat)+" ("+formatNumber(nWeight, THREEDIGITSformat)+")";
	if ( nWeight == 0 )
		sSizeInfo = "? B (? B)";
	if ( bBigInfo )
		sSizeInfo += "\n"+QString(tr("%1 directories and %2 files")).arg(nDirs).arg(nFiles);

	mSizeLab->setText( tr("Weighing, please wait")+"..." );
	bool bWeightFihished = ((nDirs > 0 && nWeight > 0) || nFiles > 0);
	if (bWeightFihished)
		mSizeLab->setText( sSizeInfo );

	mRefreshBtn->setEnabled(bWeightFihished);
	mStopBtn->setEnabled(! bWeightFihished);
}


int PropertiesDlg::permissions()
{
	int nPerm = 0;

	if ( m_pReadUserChkBox->isChecked()  )  nPerm += QFile::ReadUser;
	if ( m_pReadGroupChkBox->isChecked() )  nPerm += QFile::ReadGroup;
	if ( m_pReadOtherChkBox->isChecked() )  nPerm += QFile::ReadOther;
	if ( m_pWriteUserChkBox->isChecked()  ) nPerm += QFile::WriteUser;
	if ( m_pWriteGroupChkBox->isChecked() ) nPerm += QFile::WriteGroup;
	if ( m_pWriteOtherChkBox->isChecked() ) nPerm += QFile::WriteOther;
	if ( m_pExeUserChkBox->isChecked()  )   nPerm += QFile::ExeUser;
	if ( m_pExeGroupChkBox->isChecked() )   nPerm += QFile::ExeGroup;
	if ( m_pExeOtherChkBox->isChecked() )   nPerm += QFile::ExeOther;

	if ( m_pSuidChkBox->isChecked() ) nPerm += S_ISUID;
	if ( m_pSgidChkBox->isChecked() ) nPerm += S_ISGID;
	if ( m_pSvtxChkBox->isChecked() ) nPerm += S_ISVTX;

	return nPerm;
}

bool PropertiesDlg::isRecursive()
{
	return mChangeRecursiveChkBox->isChecked();
}

int PropertiesDlg::currentItem()
{
	return mNameComboBox->currentItem();
}


void PropertiesDlg::slotInitAllTabs( const QString & sFileName )
{
	QString sTypeInfo, sLocation;
	bool bAllSelected = FALSE;
	uint nFiles = 0, nDirs = 0;
	long long nWeight = 0;
	UrlInfoExt uUrlInfo, uUrlInfoT;
	QStringList slList;
	QListWidgetItem *pLWitem = NULL;

	//qDebug() << "PropertiesDlg::slotInitAllTabs, sFileName= " << sFileName;
	/*if ( mNameComboBox->count() > 1 )
		if ( mNameComboBox->currentItem() == 0 ) // bAllSelected
			bAllSelected = TRUE;*/
	if ( sFileName == tr("ALL SELECTED") ) {
		bAllSelected = TRUE;
		for( int i=1; i<mNameComboBox->count(); i++ ) {
			if ( uUrlInfo.isSymLink() )
				nFiles++;
			else
				( uUrlInfo.isDir() ) ? nDirs++ : nFiles++;
			emit signalGetUrlForFile( mNameComboBox->text(i), uUrlInfo );
			if ( ! uUrlInfo.isDir() )
				nWeight += uUrlInfo.size();
			if ( i == 1 )
				uUrlInfoT = uUrlInfo; // NNN
		}

		sTypeInfo = tr("file(s)");
		sLocation = FileInfoExt::filePath( mNameComboBox->text(1) );
		uUrlInfo = uUrlInfoT; // access and modified time for first item // NNN
		//emit signalGetUrlForFile( mNameComboBox->text(1), uUrlInfo ); // for access and modified time
		mNameComboBox->setEditable( FALSE );
		slotRefresh(); // starts weighing
	}
	else { // the single file or directory
		emit signalGetUrlForFile( sFileName, uUrlInfo );
		sLocation = FileInfoExt::filePath( sFileName );
		nWeight = uUrlInfo.size();

		if ( uUrlInfo.isDir() && ! uUrlInfo.isSymLink() ) {
			sTypeInfo = tr("directory");
			nDirs = 1;
			slotRefresh(); // starts weighing
		}
		else {
			if ( m_pFilesAssociation )
				sTypeInfo = m_pFilesAssociation->fileDescription( sFileName );
			if ( sTypeInfo.isEmpty() || ! m_pFilesAssociation )
				sTypeInfo = (uUrlInfo.isSymLink()) ? tr("symbolic/hard link") : tr("file");
			nFiles = 1;
		}
		mNameComboBox->setEditable( TRUE );
	}

	(sTypeInfo == tr("directory")) ? mRefreshBtn->show() : mRefreshBtn->hide();
	(sTypeInfo == tr("directory")) ? mStopBtn->show()    : mStopBtn->hide();
	mNameComboBox->setFocus();

	// --- init first tab
	mLocationLab->setText( sLocation );
	mTypeLab->setText( sTypeInfo );
	setSize( nWeight, nDirs, nFiles, (bAllSelected || (uUrlInfo.isDir() && ! uUrlInfo.isSymLink())) );

	m_pLastAccessTimeEdit->setDateTime( uUrlInfo.lastRead() );
	m_pLastModifiedTimeEdit->setDateTime( uUrlInfo.lastModified() );

	if ( ! bAllSelected ) {
		mNameComboBox->setCurrentText( FileInfoExt::fileName(uUrlInfo.name()) );
//		qDebug("*** setCurrentText = name=%s", FileInfoExt::sFileName(uUrlInfo.name()).toLatin1().data() );
	}
	// --- init second tab
		// - clear all permissions
	m_pClassUserChkBox->setChecked( FALSE );
	m_pClassGroupChkBox->setChecked( FALSE );
	m_pClassOtherChkBox->setChecked( FALSE );
	slotInitPermissonForClass( USER_PERM );
	slotInitPermissonForClass( GROUP_PERM );
	slotInitPermissonForClass( OTHER_PERM );
	m_pSuidChkBox->setChecked( FALSE );
	m_pSgidChkBox->setChecked( FALSE );
	m_pSvtxChkBox->setChecked( FALSE );

	if ( bAllSelected ) {
		m_pReadButtonGroup->button( USER_PERM )->setChecked( TRUE );
		m_pReadButtonGroup->button( GROUP_PERM )->setChecked( TRUE );
		m_pReadButtonGroup->button( OTHER_PERM )->setChecked( TRUE );  // read for all
		m_pWriteButtonGroup->button( USER_PERM )->setChecked( TRUE ); // write for user
	}
	else {
		int nPerm = uUrlInfo.permissions();
		if ( nPerm & QFileInfo::ReadUser )   m_pReadButtonGroup->button(  USER_PERM )->setChecked( TRUE );
		if ( nPerm & QFileInfo::WriteUser )  m_pWriteButtonGroup->button( USER_PERM )->setChecked( TRUE );
		if ( nPerm & QFileInfo::ExeUser )    m_pExecButtonGroup->button(  USER_PERM )->setChecked( TRUE );
		if ( nPerm & QFileInfo::ReadGroup )  m_pReadButtonGroup->button(  GROUP_PERM )->setChecked( TRUE );
		if ( nPerm & QFileInfo::WriteGroup ) m_pWriteButtonGroup->button( GROUP_PERM )->setChecked( TRUE );
		if ( nPerm & QFileInfo::ExeGroup )   m_pExecButtonGroup->button(  GROUP_PERM )->setChecked( TRUE );
		if ( nPerm & QFileInfo::ReadOther )  m_pReadButtonGroup->button(  OTHER_PERM )->setChecked( TRUE );
		if ( nPerm & QFileInfo::WriteOther ) m_pWriteButtonGroup->button( OTHER_PERM )->setChecked( TRUE );
		if ( nPerm & QFileInfo::ExeOther )   m_pExecButtonGroup->button(  OTHER_PERM )->setChecked( TRUE );

		if ( nPerm & S_ISUID )  m_pSuidChkBox->setChecked( TRUE );
		if ( nPerm & S_ISGID )  m_pSgidChkBox->setChecked( TRUE );
		if ( nPerm & S_ISVTX )  m_pSvtxChkBox->setChecked( TRUE );
	}
	// TODO fix it: does not work correctly, becouse below function need to know sender (selects buttons group)
	// some reason is use in slotCheckGroupButton  button_id  instead  sender name
	slotCheckGroupButton(0); // READ
	slotCheckGroupButton(1); // WRITE
	slotCheckGroupButton(2); // EXEC

	// --- init user and sGroup combo
	slList = SystemInfo::getAllUsers();
	m_pOwnerListWidget->clear();
	m_pOwnerListWidget->addItems( slList );

    slList = SystemInfo::getAllGroups();
	m_pGroupListWidget->clear();
	m_pGroupListWidget->addItems( slList );

	QString sOwner, sGroup;
	if ( bAllSelected ) {
		sOwner = SystemInfo::getCurrentUserName(); // get current logged user name
		sGroup = SystemInfo::getCurrentGroupName();// get current logged user's sGroup name
	}
	else {
		sOwner = uUrlInfo.owner();
		sGroup = uUrlInfo.group();
	}
//	bool enableOwnerChanges = (sOwner == m_sLoggedUserName || m_sLoggedUserName == "root");

	if (m_pGroupListWidget->findItems( sGroup, Qt::MatchExactly ).first() == NULL) // not bFound sGroup
		m_pOwnerListWidget->insertItem( 0, sGroup );

	pLWitem = m_pOwnerListWidget->findItems(sOwner, Qt::MatchExactly).first();
	m_pOwnerListWidget->setCurrentItem( pLWitem );
	m_pOwnerListWidget->scrollToItem( pLWitem, QAbstractItemView::PositionAtTop ); // vertical policy must be 'prefered'

	pLWitem = m_pGroupListWidget->findItems(sGroup, Qt::MatchExactly).first();
	m_pGroupListWidget->setCurrentItem( pLWitem );
	m_pGroupListWidget->scrollToItem( pLWitem, QAbstractItemView::PositionAtTop ); // vertical policy must be 'prefered'

	// --- set third Tab
	mChangeRecursiveChkBox->setEnabled( nDirs );
	// TODO mChangeRecursiveChkBox domyslnie ustawic na nieaktywne, i uaktywaniac gdy ktorys
	// z chkBoxow (mChangePermissionChkBox, mChangeOwnerChkBoxm, mChangeTimeChkBox) jest wlaczany
	//mChangeRecursiveChkBox->setEnabled( FALSE );
	mTabWidget->setTabEnabled( mTabWidget->page(2), (uUrlInfo.isDir() || bAllSelected) );

	// --- other settings
/* // nie mozna tutaj pobrac wlasciciela, ani innych atrybutow dla katalogu rodzica
	bool changeAttributsAllowed = TRUE; // except sGroup, it always allowed

	if ( ! bAllSelected ) {
		emit signalGetUrlForFile( FileInfoExt::filePath(sFileName), uUrlInfo );

		if ( uUrlInfo.sOwner() != m_sLoggedUserName )
			changeAttributsAllowed = FALSE;
	}

	mChangeTimeChkBox->setEnabled( changeAttributsAllowed );
	mChangePermissionChkBox->setEnabled( changeAttributsAllowed );
*/
}

void PropertiesDlg::slotInitPermissonForClass( int nPermClass ) // int nPermClass
{
	bool bIsChecked = m_pClassButtonGroup->button(nPermClass)->isChecked();

	m_pReadButtonGroup->button( nPermClass )->setChecked( bIsChecked );
	m_pWriteButtonGroup->button( nPermClass )->setChecked( bIsChecked );
	m_pExecButtonGroup->button( nPermClass )->setChecked( bIsChecked );
}

void PropertiesDlg::slotCheckGroupButton( int nButtonId )
{
	if (nButtonId < 0 || nButtonId > 2)
		return;
	// -- check for class
	bool r = m_pReadButtonGroup->button( nButtonId )->isChecked();
	bool w = m_pWriteButtonGroup->button( nButtonId )->isChecked();
	bool x = m_pExecButtonGroup->button( nButtonId )->isChecked();

	m_pClassButtonGroup->button(nButtonId)->setChecked( r && w && x );

	// -- check for group
	if (sender() == NULL) {
		qDebug("PropertiesDlg::slotCheckGroupButton - No sender found!");
		return;
	}
	QString sSenderName = sender()->name();
	//qDebug() << "PropertiesDlg::slotCheckGroupButton:  sSenderName=" << sSenderName << ", nButtonId= " << nButtonId;
	QGroupBox *pGroupBox = NULL;
	QButtonGroup *pButtonGroup = NULL;
	if (sSenderName == "m_pReadButtonGroup") { // or (nButtonId == 0) // READ group
		pGroupBox    = m_pReadGroupBox;
		pButtonGroup = m_pReadButtonGroup;
	}
	else
	if (sSenderName == "m_pWriteButtonGroup") { // or (nButtonId == 1) // WRITE group
		pGroupBox    = m_pWriteGroupBox;
		pButtonGroup = m_pWriteButtonGroup;
	}
	else
	if (sSenderName == "m_pExecButtonGroup") { // or (nButtonId == 2) // EXEC group
		pGroupBox    = m_pExeGroupBox;
		pButtonGroup = m_pExecButtonGroup;
	}
	if (pGroupBox == NULL)
		return;

	r = pButtonGroup->button( USER_PERM  )->isChecked();
	w = pButtonGroup->button( GROUP_PERM )->isChecked();
	x = pButtonGroup->button( OTHER_PERM )->isChecked();

	//pGroupBox->setChecked( r && w && x ); // temporary comment, becouse this called signal toggled that connected to slotSetButtonGroup that unchecks all
}

void PropertiesDlg::slotRefresh()
{
	mSizeLab->setText( tr("Weighing, please wait")+"..." );
	emit signalRefresh( mNameComboBox->currentItem() );
}

void PropertiesDlg::slotStop()
{
	mSizeLab->setText( tr("Stoped weighing") );
	emit signalRefresh( -1 );
}

void PropertiesDlg::slotOkPressed()
{
	//QString sCurrentName  = FileInfoExt::filePath(mNameComboBox->text(mNameComboBox->currentItem()))+mNameComboBox->currentText();

	//QString loggedUserName = SystemInfo::getUser();  // get current logged user name
	//QString loggedGroupName = SystemInfo::getUser( FALSE );  // get current logged user's sGroup name
	QString sCurrentName  = FileInfoExt::fileName(mNameComboBox->currentText());
	QString sCurrentOwner = m_pOwnerListWidget->currentItem()->text();
	QString sCurrentGroup = m_pGroupListWidget->currentItem()->text();
	QDateTime dtCurrentModifiedTime = m_pLastModifiedTimeEdit->dateTime();
	QDateTime dtCurrentAccessTime   = m_pLastAccessTimeEdit->dateTime();
	int nCurrentPermission = permissions();

	bool bAllSelected = FALSE;
	if ( mNameComboBox->count() > 1 )
		if ( mNameComboBox->currentItem() == 0 ) // bAllSelected
			bAllSelected = TRUE;
	//bool bAllSelected = (mNameComboBox->currentText() == tr("ALL SELECTED")) ? TRUE : FALSE;

	// --- check whether an attribut has been modified
	QDateTime dtNullDateTime;

	if ( ! bAllSelected ) {
		UrlInfoExt uUrlInfo;
		emit signalGetUrlForFile( mNameComboBox->text(mNameComboBox->currentItem()), uUrlInfo );
		QString sNewName = FileInfoExt::fileName(uUrlInfo.name());

		if ( sCurrentName == sNewName )
			sCurrentName = "";
		if ( nCurrentPermission == uUrlInfo.permissions() )
			nCurrentPermission = -1;
		if ( sCurrentOwner == uUrlInfo.owner() )
			sCurrentOwner = "";
		if ( sCurrentGroup == uUrlInfo.group() )
			sCurrentGroup = "";
		if ( dtCurrentAccessTime == uUrlInfo.lastRead() )
			dtCurrentAccessTime = dtNullDateTime;
		if ( dtCurrentModifiedTime == uUrlInfo.lastModified() )
			dtCurrentModifiedTime = dtNullDateTime;
	}
	else // bAllSelected is true, name must be empty
		sCurrentName = "";

	if ( ! mChangeOwnerChkBox->isChecked() )
		sCurrentOwner = sCurrentGroup = "";
	if ( ! mChangeTimeChkBox->isChecked() )
		dtCurrentAccessTime = dtCurrentModifiedTime = dtNullDateTime;
	if ( ! mChangePermissionChkBox->isChecked() )
		nCurrentPermission = -1;


	// --- make temporary the 'UrlInfoExt' object
	UrlInfoExt URLinfo(
	 sCurrentName, 0, nCurrentPermission, sCurrentOwner, sCurrentGroup, // size (0) is dummy
	 dtCurrentModifiedTime, dtCurrentAccessTime,
	 FALSE, FALSE, FALSE, // there following attributs: isDir, isFile, isSymLink are dummy
	 FALSE, FALSE // there following attributs: isReadable, isExecutable are dummy
	);

	emit signalOkPressed( URLinfo, bAllSelected );
}


void PropertiesDlg::closeEvent( QCloseEvent * e )
{
	emit signalClose();
	e->ignore();
}


bool PropertiesDlg::changePermission()
{
	return (mChangePermissionChkBox->isEnabled()) ? mChangePermissionChkBox->isChecked() : FALSE;
}


bool PropertiesDlg::changeOwnerAndGroup()
{
	return (mChangeOwnerChkBox->isEnabled()) ? mChangeOwnerChkBox->isChecked() : FALSE;
}


bool PropertiesDlg::changeTime()
{
	return (mChangeTimeChkBox->isEnabled()) ? mChangeTimeChkBox->isChecked() : FALSE;
}


int PropertiesDlg::changesFor()
{
	return mFiltersComboBox->currentItem();
	// changes for All
	// ... for nFiles only
	// ... for nDirs only
	// SPRAWDZIC JAK W KLASIE OBSLUGUJACE WYSZUKIWANIE
	// szukac wywolania changesFor()
}


void PropertiesDlg::slotActivatedName( const QString & sFullName )
{
	if ( mNameComboBox->count() > 1 )
		if ( mNameComboBox->currentItem() > 0 ) // not bAllSelected
			mNameComboBox->setCurrentText( FileInfoExt::fileName(sFullName) );
}


void PropertiesDlg::setEnableAllChanges( bool bEnable )
{
	mChangeTimeChkBox->setEnabled( bEnable );
	mChangePermissionChkBox->setEnabled( bEnable );
	//mChangeOwnerChkBox->setEnabled( bEnable );
}

void PropertiesDlg::slotSetButtonGroup( bool bToggled )
{
	if (sender() == NULL) {
		qDebug("PropertiesDlg::slotSetButtonGroup() - No sender found!");
		return;
	}
	QString sSenderName = sender()->name();
	//qDebug() << "PropertiesDlg::slotSetButtonGroup:  sSenderName=" << sSenderName << ", bChecked= " << bToggled;

	//bool bGroupChanged = FALSE;
	QButtonGroup * pButtonGroup = NULL;
	if (sSenderName == "m_pReadGroupBox") {
		pButtonGroup = m_pReadButtonGroup;
		//bGroupChanged = m_bReadGroupChanged;
	}
	else
	if (sSenderName == "m_pWriteGroupBox") {
		pButtonGroup = m_pWriteButtonGroup;
		//bGroupChanged = m_bWriteGroupChanged;
	}
	else
	if (sSenderName == "m_pExeGroupBox") {
		pButtonGroup = m_pExecButtonGroup;
		//bGroupChanged = m_bExecGroupChanged = ?;
	}
	enableButtonGroup( pButtonGroup );

	if (pButtonGroup == NULL)
		return;
	//if (bGroupChanged) // prevent to uncheck all buttons after uncheck one in all checked in group
	//	return;

	if (sSenderName == "m_pReadGroupBox") {
		m_bReadGroupChanged = FALSE;
	}
	else
	if (sSenderName == "m_pWriteGroupBox") {
		m_bWriteGroupChanged = FALSE;
	}
	else
	if (sSenderName == "m_pExeGroupBox") {
		m_bExecGroupChanged = FALSE;
	}

	for (int i=0; i<m_pReadButtonGroup->buttons().count(); i++) {
		pButtonGroup->button(i)->setChecked(bToggled);
	}
}

void PropertiesDlg::enableButtonGroup( QButtonGroup * pButtonGroup )
{
	for (int i=0; i<pButtonGroup->buttons().count(); i++) {
		pButtonGroup->button(i)->setEnabled( TRUE );
	}
}

