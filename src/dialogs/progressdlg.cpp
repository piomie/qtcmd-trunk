/***************************************************************************
                          progressdlg  -  description
                             -------------------
    begin                : wto mar 15 2005
    copyright            : (C) 2005 by Piotr Mierzwiński
    email                : peterm@go2.pl

    copyright            : See COPYING file that comes with this project

 $Id$
 ***************************************************************************/
#include <QTimer>

#include "functions.h" // for: formatNumber

#include "progressdlg.h"


ProgressDlg::ProgressDlg()
{
	setupUi(this);
}


ProgressDlg::~ProgressDlg()
{
}


void ProgressDlg::init( VFS::Operation eOperation, bool bCloseAfterFinished, bool bMove, const QString & sTargetPath )
{
	QString sTitle;

	if ( eOperation == VFS::Copy )
		sTitle = (bMove) ? tr("Moving") : tr("Copying");
	else
	if ( eOperation == VFS::Remove )
		sTitle = tr("Removing");
	else
	if ( eOperation == VFS::Weigh )
		sTitle = tr("Weighing");
	else
	if ( eOperation == VFS::SetAttributs )
		sTitle = tr("Setting the attributs");

	slotSetOperation( eOperation );

	m_pCancelBtn->setAccel( Qt::Key_Escape );

	m_pTimeLabel->hide();          m_pTimeLabValue->hide();
	m_pTransferLabel->hide();      m_pTransferLabValue->hide();
	m_pEstimatedTimeLabel->hide(); m_pEstimatedTimeLabValue->hide();

	// remove not useable objects
	if ( eOperation == VFS::Remove || eOperation == VFS::Weigh || eOperation == VFS::SetAttributs ) {
		delete m_pTrgLabel;         m_pTrgLabel        = NULL;
		delete m_pTargetSLabValue;  m_pTargetSLabValue = NULL;
		delete m_pFileProgress;     m_pFileProgress    = NULL;
		delete m_pTransferLabel;      delete m_pTransferLabValue;      m_pTransferLabValue      = NULL;
		delete m_pEstimatedTimeLabel; delete m_pEstimatedTimeLabValue; m_pEstimatedTimeLabValue = NULL;
		delete m_pLabCurrentWeight; m_pLabCurrentWeight = NULL;

		if ( eOperation == VFS::Weigh )
			delete m_pTotalProgress;

		adjustSize();
	}

	setCaption( sTitle+" "+tr("files")+" - QtCommander" );

	if ( eOperation == VFS::Copy || eOperation == VFS::Move )
		m_pTargetSLabValue->setText( sTargetPath );

	m_pCloseAfterFinishedChkBox->setChecked( bCloseAfterFinished );

	connect( m_pCloseAfterFinishedChkBox, SIGNAL(toggled( bool )),
		this, SIGNAL(signalCloseAfterFinished( bool )) );

	m_pTimer = new QTimer( this );
	connect( m_pTimer, SIGNAL(timeout()), this, SLOT(slotSetTime()) );

	if ( eOperation == VFS::Copy || eOperation == VFS::Move ) {
		m_pFileProgress->setRange( 0, 100 );
		m_pTransferLabValue->setText( "? "+tr("bytes per second") );
	}
	if ( eOperation == VFS::Copy || eOperation == VFS::Move || eOperation == VFS::Remove )
		m_pTotalProgress->setRange( 0, 100 );

	m_nBytesTransfered = 0;
	m_sTotalFiles = "0";
	m_sTotalDirs = "0";
	m_sNumFiles = "0";
	m_sNumDirs = "0";
	m_nTotalWeight = 0;

	m_pTime.setHMS( 0, 0, 0 );
	m_pTime.start();

	slotSetTime();
	timerStart();
}


void ProgressDlg::slotSetFilesName( const QString & sSourceName, const QString & sTargetName )
{
	m_pSourceSLabValue->setText( sSourceName );

	if ( m_pTargetSLabValue != NULL )
		m_pTargetSLabValue->setText( sTargetName );
}


void ProgressDlg::slotDataTransferProgress( long long nDone, long long nTotal, uint nBytesTransfered )
{
	if ( m_pFileProgress != NULL && nTotal > 0 ) {
		if ( nDone == nTotal )
			m_pFileProgress->setValue( 100 );
		else
			m_pFileProgress->setValue( (nDone*100) / nTotal );
	}
	m_nBytesTransfered += nBytesTransfered;
}


void ProgressDlg::slotSetTotalProgress( int nDone, long long nTotalWeight )
{
	if ( nDone > 0 )
		m_pTotalProgress->setValue( nDone );

	m_nTotalWeight = nTotalWeight;
}


void ProgressDlg::slotSetTime() // and transfer
{
	if ( m_pDateTime.time().second() == 1 ) {
		m_pTimeLabel->show(); m_pTimeLabValue->show();

		if ( m_pTransferLabValue != NULL ) {
			m_pTransferLabel->show();
			m_pTransferLabValue->show();
		}
		if ( m_pEstimatedTimeLabValue != NULL ) {
			m_pEstimatedTimeLabel->show();
			m_pEstimatedTimeLabValue->show();
		}
	}
	m_pDateTime.setTime_t( m_pTime.elapsed()/1000, Qt::UTC );
	m_pTimeLabValue->setText( (m_pDateTime.time()).toString() );
	// TODO sprobowac zmienic m_pDateTime na QTime (zrobic tak jak nizej)
	if ( m_nBytesTransfered > 0 ) {
		m_pTransferLabValue->setText( formatNumber(m_nBytesTransfered,BKBMBGBformat)+" "+tr("per second") );
		// --- set an estimated time
		if ( m_nTotalWeight > 0 ) {
			QTime estimatedTime(0, 0);
			estimatedTime = estimatedTime.addSecs( m_nTotalWeight/m_nBytesTransfered );
			m_pEstimatedTimeLabValue->setText( estimatedTime.toString() );
		}
		m_nBytesTransfered = 0;
	}
}

void ProgressDlg::slotSetProcessedNumber( int nValue, bool bFileCounting, bool bTotalValue )
{
	if ( bTotalValue ) {
		if ( bFileCounting )
			m_sTotalFiles = QString::number( nValue );
		else
			m_sTotalDirs  = QString::number( nValue );

		m_pProcessedLabValue->setText(
			tr("files")+": "+m_sTotalFiles+", "+tr("directories")+": "+m_sTotalDirs
		);
	}
	else {
		if ( bFileCounting )
			m_sNumFiles = QString::number( nValue );
		else
			m_sNumDirs  = QString::number( nValue );

		m_pProcessedLabValue->setText(
			tr("files")+": "+m_sNumFiles+"\\"+m_sTotalFiles+", "+
			tr("directories")+": "+m_sNumDirs +"\\"+m_sTotalDirs
		);
	}
// 	qDebug("%s %s", m_pProcessedLabel->text().toLatin1().data(), m_pProcessedLabValue->text().toLatin1().data() );
}


void ProgressDlg::slotSetOperation( VFS::Operation eOperation )
{
	QString sMsg;

	if ( eOperation == VFS::Copy || eOperation == VFS::Move )   sMsg = tr("Copied");
//	else
//	if ( eOperation == VFS::Move )   sMsg = tr("Moved");
	else
	if ( eOperation == VFS::Remove ) sMsg = tr("Removed");
	else
	if ( eOperation == VFS::Weigh )  sMsg = tr("Weighed");
	else
	if ( eOperation == VFS::SetAttributs ) sMsg = tr("Set the attributs");
	else // unknown operation
		return;

	m_pProcessedLabel->setText( sMsg+" :" );
}

void ProgressDlg::timerStart()
{
	m_pTimer->start( 1000 ); // emits signal every 1 seconds
}

void ProgressDlg::timerStop( bool bRemoveIt )
{
	m_pTimer->stop();
	m_pInBrgdBtn->setEnabled( FALSE );

	if ( bRemoveIt )
		delete m_pTimer;
}

void ProgressDlg::closeEvent( QCloseEvent * pCloseEvent )
{
	m_pTimer->stop();
	emit signalCancel();
	pCloseEvent->ignore();
}
