/***************************************************************************
                          helpdialog.h  -  description
                             -------------------
    begin                : sun jan 23 2005
    copyright            : (C) 2005 by Piotr Mierzwiński
    email                : peterm@go2.pl

    copyright            : See COPYING file that comes with this project

 $Id$
 ***************************************************************************/
#ifndef HELPDLG_H
#define HELPDLG_H

#include <QDialog>

#include "ui_helpdialog.h"

class QPushButton;
class QTextBrowser;

/** @short Klasa zawiera okienko pomocy dla aplikacji.
*/
/**
@author Piotr Mierzwiński
*/
class HelpDlg : public QDialog, public Ui::HelpDialog
{
	Q_OBJECT
public:
	HelpDlg();
	~HelpDlg() {}

};

#endif
