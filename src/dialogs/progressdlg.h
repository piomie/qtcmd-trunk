/***************************************************************************
                          progressdlg  -  description
                             -------------------
    begin                : wto mar 15 2005
    copyright            : (c) 2005 by Piotr Mierzwiński
    email                : peterm@go2.pl

    copyright            : See COPYING file that comes with this project

 $Id$
 ***************************************************************************/

#ifndef PROGRESSDLG_H
#define PROGRESSDLG_H

#include "ui_progressdialog.h"

#include "vfs.h"

#include <QCloseEvent>

class QTime;
class QTimer;
class QDateTime;
/**
	@author Piotr Mierzwiński
*/
class ProgressDlg : public QDialog, Ui::ProgressDialog
{
	Q_OBJECT
public:
	ProgressDlg();
	~ProgressDlg();

	void init( VFS::Operation eOperation, bool bCloseAfterFinished, bool bMove=FALSE, const QString & sTargetPath=QString::null );

	void timerStop( bool bRemoveIt=FALSE );
	void timerStart();

private:
	QTime m_pTime;
	QTimer *m_pTimer;
	QDateTime m_pDateTime;
	QString m_sTotalFiles;
	QString m_sTotalDirs;
	QString m_sNumFiles;
	QString m_sNumDirs;
	uint m_nBytesTransfered;
	long long m_nTotalWeight;


public slots:
	void slotSetFilesName( const QString & sSourceName, const QString & sTargetName );
	void slotDataTransferProgress( long long nDone, long long nTotal, uint nBytesTransfered );
	void slotSetTotalProgress( int nDone, long long nTotalWeight );
	void slotSetProcessedNumber( int nValue, bool bFileCounting, bool bTotalValue );
	void slotSetOperation( VFS::Operation eOperation );

private slots:
	void slotSetTime(); // and transfer

protected:
	void closeEvent( QCloseEvent * pCE );

signals:
	void signalCancel();
	void signalCloseAfterFinished( bool );

};

#endif
