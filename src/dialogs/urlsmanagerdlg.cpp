/***************************************************************************
                          urlsmanagerdlg  -  description
                             -------------------
    begin                : pi� mar 11 2005
    copyright            : (C) 2005 by Piotr Mierzwi�ski
    email                : peterm@go2.pl

    copyright            : See COPYING file that comes with this project

 $Id$
 ***************************************************************************/
#include <QtGui> // inludes: QSettings, QDir, QInputDialog, QMessageBox
#include <QDialog>
#include <QKeyEvent>

#include "url.h"
#include "urlsmanagerdlg.h"
#include "locationchooser.h"


UrlsManagerDlg::UrlsManagerDlg()
	: UrlsManagerDialog()
{
	setupUi(this);

	connect(m_pNewBtn, SIGNAL(clicked()), this, SLOT(slotNew()));
	connect(m_pDeleteBtn, SIGNAL(clicked()), this, SLOT(slotDelete()));
	connect(m_pCancelBtn, SIGNAL(clicked()), this, SLOT(close()));
	connect(m_pOkBtn, SIGNAL(clicked()), this, SLOT(accept()));
	connect(m_pUpBtn, SIGNAL(clicked()), this, SLOT(slotMoveUp()));
	connect(m_pDownBtn, SIGNAL(clicked()), this, SLOT(slotMoveDown()));
	connect(m_pSaveBtn, SIGNAL(clicked()), this, SLOT(slotSave()));
	connect(m_pRenameBtn, SIGNAL(clicked()), this, SLOT(slotRename()));
	connect(m_pGotoUrlBtn, SIGNAL(clicked()), this, SLOT(slotGotoUrl()));
	connect(m_pHostEdit, SIGNAL(textChanged(const QString&)), this, SLOT(slotTextChanged(const QString&)));
	connect(m_pUserEdit, SIGNAL(textChanged(const QString&)), this, SLOT(slotTextChanged(const QString&)));
	connect(m_pPasswordEdit, SIGNAL(textChanged(const QString&)), this, SLOT(slotTextChanged(const QString&)));
	connect(m_pLocationChooser, SIGNAL(textChanged(const QString&)), this, SLOT(slotTextChanged(const QString&)));

	connect(m_pURLsList, SIGNAL(itemSelectionChanged()),                 this, SLOT(slotItemSelected()));
	connect(m_pURLsList, SIGNAL(itemDoubleClicked( QListWidgetItem * )), this, SLOT(slotItemDoubleClicked( QListWidgetItem * )));

	// -- fix tab order
	QWidget::setTabOrder(m_pHostEdit, m_pLocationChooser->lineEdit());
	QWidget::setTabOrder(m_pLocationChooser->lineEdit(), m_pUserEdit);
}

UrlsManagerDlg::~UrlsManagerDlg()
{
}

bool UrlsManagerDlg::showDialog( QString & sNewURL, KindOfDialog eKindOfDialog, const QString & sCurrentPath, QWidget *pParent )
{
	UrlsManagerDlg *pDlg = new UrlsManagerDlg;
	Q_CHECK_PTR(pDlg);
	pDlg->m_sPanelSideName = "";
	if (pParent) {
		pDlg->m_sPanelSideName = pParent->name();
		pDlg->m_sPanelSideName = pDlg->m_sPanelSideName.left(pDlg->m_sPanelSideName.indexOf('P'));
	}

	pDlg->m_sCurrentPath = sCurrentPath;

	QString sCaption;
	if (eKindOfDialog == FAVORITES)
		sCaption = tr("Favorites Manager");
	else
	if (eKindOfDialog == HISTORY)
		sCaption = tr("History Manager");
	else
	if (eKindOfDialog == FTP)
		sCaption = tr("FTP Connections Manager");
	else
	if (eKindOfDialog == FILTERS)
		sCaption = tr("Filters Manager");
	pDlg->setCaption(sCaption + " - QtCommander");

	pDlg->m_nOldItemId = -1;
	pDlg->m_bLockStore = TRUE;
	pDlg->m_bLockIfHighlighted = FALSE;
	pDlg->m_eKindOfDialog = eKindOfDialog;
	pDlg->initURLs(); // reads URLs data from the config file and inserts them on the list
	int nCurrentItemId = pDlg->m_pURLsList->currentRow();

	QString sOriginalPath;
	if ( pDlg->m_pURLsList->count() > 0) {
		if (nCurrentItemId > pDlg->m_slURLsStringList.count()-1)
			qDebug("UrlsManagerDlg::showDialog. Index out of range!");
		else {
			if (pDlg->m_eKindOfDialog == HISTORY) { // history items has'nt names
				sOriginalPath = pDlg->m_slURLsStringList[nCurrentItemId];
			}
			else {
				QChar separator = (pDlg->m_eKindOfDialog == FILTERS) ? '=' : '\\';
				sOriginalPath = pDlg->m_slURLsStringList[nCurrentItemId].section(separator, 1);
			}
			sOriginalPath = URL(sOriginalPath).path();
			//qDebug() << "sOriginalPath= " << sOriginalPath;
		}
	}
	pDlg->m_pLocationChooser->setOriginalPath(sOriginalPath);
	pDlg->m_pLocationChooser->setCurrentPath(sCurrentPath);

	if (eKindOfDialog == FTP) {
		pDlg->m_pLocationChooser->setMenuItemVisible(LocationChooser::SELECT_DIR, FALSE);
		pDlg->m_pLocationChooser->setMenuItemVisible(LocationChooser::HOME_DIR, FALSE);
		pDlg->m_pLocationChooser->setLeftButtonAction(LocationChooser::ROOT_DIR);
		pDlg->m_pLocationChooser->setBottomButtonAction(LocationChooser::UP_DIR);
	}
	pDlg->m_pLocationChooser->setRightButtonAction(LocationChooser::ORIGINAL_DIR);

	bool bGotoURL = FALSE;
	int  nResult  = pDlg->exec();

	if (nResult == QDialog::Accepted)
		pDlg->slotSave();
	else
	if (nResult == 1111) {
		bGotoURL = TRUE;
		sNewURL  = pDlg->m_sNewURL;
	}

	delete pDlg;
	pDlg = 0;

	return bGotoURL;
}

void UrlsManagerDlg::initURLs()
{
	QSettings settings;

	if (m_eKindOfDialog == FILTERS)
		m_slURLsStringList = settings.readListEntry("/qtcmd/FilesPanel/UserFilters");
	else
	if (m_eKindOfDialog != HISTORY) {
	// sUrl == "sUrlName\sUser:password@sHost:portdir" // FTP
	// sUrl == "/some/path" // LOCALfs
		uint nNum    = 0;
		bool bEmpty  = FALSE;
		QString sUrl = "";
		QString sKindOfURLstr = (m_eKindOfDialog==FTP) ? "FTP/S" : "Favorites/Url";

		while(! bEmpty) {
			sUrl = settings.readEntry(QString("/qtcmd/"+sKindOfURLstr+"%1").arg(nNum));
			if (! (bEmpty=sUrl.isEmpty()))
				m_slURLsStringList.append(sUrl);
			nNum++;
		}
	}
	else // HISTORY
		m_slURLsStringList = settings.readListEntry("/qtcmd/"+m_sPanelSideName+"FilesPanel/PathViewURLs");

	m_nAllReadURLs = m_slURLsStringList.count(); // need to slotSave()

	if (m_eKindOfDialog == FILTERS) {
		m_pGotoUrlBtn->setText(tr("&Apply"));
		m_pDirLab->setText(tr("&Pattern :"));
		m_pDirLab->setBuddy(m_pLocationChooser);
		m_pHostLab->hide();   m_pHostEdit->hide();
		m_pPortLab->hide();   m_pPortSpin->hide();
		m_pUserLab->hide();   m_pUserEdit->hide();
		m_pPassLab->hide();   m_pPasswordEdit->hide();
		m_pLocationChooser->setButtonSide(LocationChooser::NoneButton);
		m_pLocationChooser->setGettingWidget(LocationChooser::LineEdit);
	}
	if (m_eKindOfDialog == FTP) {
		m_pGotoUrlBtn->setText(tr("Connec&t"));
	}
	if (m_slURLsStringList.count() == 0) {
		// disable all buttons (except 'New')
		m_pGotoUrlBtn->setEnabled(FALSE);
		m_pDeleteBtn->setEnabled(FALSE);
		m_pRenameBtn->setEnabled(FALSE);
		m_pDownBtn->setEnabled(FALSE);
		//m_pSaveBtn->setEnabled(FALSE);
		m_pUpBtn->setEnabled(FALSE);
		// disable all line's edit
		m_pLocationChooser->setEnabled(FALSE);
		m_pHostEdit->setEnabled(FALSE);
		m_pUserEdit->setEnabled(FALSE);
		m_pPortSpin->setEnabled(FALSE);
		m_pPasswordEdit->setEnabled(FALSE);
		return;
	}

	QChar separator = (m_eKindOfDialog == FILTERS) ? '=' : '\\';
	if (m_eKindOfDialog != HISTORY) {
		for(int i=0; i<m_slURLsStringList.count(); i++)
			m_pURLsList->insertItem(i, m_slURLsStringList[i].section(separator, 0, 0));
	}
	else // HISTORY
		m_pURLsList->addItems(m_slURLsStringList);

	m_pURLsList->setCurrentRow(0);

	adjustSize();
}


void UrlsManagerDlg::moveCurrentItem( Direct eDirect )
{
	if ( m_pURLsList->count() < 2 )
		return;

	if ( ! m_pURLsList->hasFocus() )
		m_pURLsList->setFocus();

	QListWidgetItem *pReplacedItem = NULL;
	QListWidgetItem *pCurrentItem  = m_pURLsList->currentItem();
	if (pCurrentItem == NULL)
		return;

	int nIdItem1      = m_pURLsList->row(pCurrentItem);
	int nItemHeight   = m_pURLsList->visualItemRect(pCurrentItem).height();
	int nCurrItemYpos = m_pURLsList->visualItemRect(pCurrentItem).y()+6;

	if ( eDirect == UP )
		pReplacedItem = m_pURLsList->itemAt( QPoint(5,nCurrItemYpos-nItemHeight) );
	else
	if ( eDirect == DOWN)
		pReplacedItem = m_pURLsList->itemAt( QPoint(5,nCurrItemYpos+nItemHeight) );

	if ( pReplacedItem == NULL || pReplacedItem == pCurrentItem )
		return;

	m_pURLsList->insertItem( m_pURLsList->currentRow()+((eDirect == UP) ? -1 : 2),
		(pReplacedItem=new QListWidgetItem(*pCurrentItem)) );

	m_pURLsList->takeItem(m_pURLsList->row(pCurrentItem));
	m_pURLsList->setCurrentItem(pReplacedItem);

// --- change places item1 with item2 into table of URLs data
	int nIdItem2 = m_pURLsList->currentRow();
	//qDebug("UrlsManagerDlg::moveCurrentItem, (movable) nIdItem1= %d, (replacable) nIdItem2= %d", nIdItem1, nIdItem2);
	if (nIdItem1 > m_pURLsList->count()-1 || nIdItem2 > m_pURLsList->count()-1)
		qDebug("UrlsManagerDlg::moveCurrentItem. Index out of range!");
	else {
		QString sMovableName  = m_slURLsStringList[ nIdItem1 ];
		QString sReplacesName = m_slURLsStringList[ nIdItem2 ];
		m_slURLsStringList[ nIdItem1 ] = sReplacesName;
		m_slURLsStringList[ nIdItem2 ] = sMovableName;
	}
	slotItemSelected(); // need to update fields
}

void UrlsManagerDlg::saveCurrentUrlDataToList( int nIndex )
{
	//qDebug() << "UrlsManagerDlg::saveCurrentUrlDataToList, nIndex= " << nIndex;
	QString sItem, sFTPname;
	QString sHost = m_pHostEdit->text();
	QString sUser = m_pUserEdit->text();
	QString sPass = m_pPasswordEdit->text();
	QString sDir  = m_pLocationChooser->text();
	QString sPort = QString::number(m_pPortSpin->value());
	if (nIndex > m_pURLsList->count()-1) {
		qDebug("UrlsManagerDlg::saveCurrentUrlDataToList. Index out of range!");
		return;
	}
	QString sSN = m_pURLsList->item(nIndex)->text(); // section name
	if (m_eKindOfDialog == FTP || (m_eKindOfDialog == FAVORITES && ! sDir.isEmpty())) {
		if (sDir.at(0) != '/')
			sDir.insert(0, '/');
		if (sDir.at(sDir.length()-1) != '/')
			sDir += "/";
	}

	if (sHost.startsWith("ftp://") || sHost.startsWith("sftp://")) {
		sFTPname = sHost.left(sHost.indexOf("//")+2);
		sHost.remove(0, sFTPname.length()); // remove protocol name
	}

	if (m_eKindOfDialog == FTP) {
		if (sHost.isEmpty())
			sHost = "localhost";
		if (sUser.isEmpty())
			sUser = "anonymous";
		if (sPort.isEmpty())
			sPort = "21";

		uint nHostLen = sHost.length()-1;
		if (sHost.at(nHostLen) == '/') // sHost name shouldn't have a slash character at the end
			sHost.truncate(nHostLen);

		sItem = sSN+"\\"+sFTPname+sUser+":"+sPass+"@"+sHost+":"+sPort+sDir;
	}
	else
	if (m_eKindOfDialog == FILTERS)
		sItem = sSN+'='+sDir;
	else // FAVORITES (LOCALfs) and HISTORY
	if (! sDir.isEmpty()) {
		if (sDir == "~")
			sDir = QDir::homeDirPath()+"/";
		else
		if (sDir.at(0) == '.' && sDir.at(1) == '/') {
			sDir.remove(0, 2);
			sDir.insert(0, m_sCurrentPath);
		}
		else
		if (sDir.at(0) != '/')
			sDir.insert(0, "/");
		if (sDir.at(sDir.length()-1) != '/')
			sDir += "/";

		if (m_eKindOfDialog == HISTORY)
			sItem = sDir;
		else
			sItem = sSN+"\\"+sDir;
	}

	m_slURLsStringList[ nIndex ] = sItem;
	if ( m_nOldItemId > -1 ) {
		m_slURLsStringList[ m_nOldItemId ] = sItem;
		m_nOldItemId = -1;
	}
}


void UrlsManagerDlg::clearAllCells()
{
	m_bLockIfHighlighted = TRUE;
	m_pHostEdit->clear();
	m_pLocationChooser->setText("");
	m_pUserEdit->clear();
	m_pPasswordEdit->clear();
	m_pPortSpin->setValue(21);
	m_bLockIfHighlighted = FALSE;
}

void UrlsManagerDlg::slotGotoUrl()
{
	if (! m_pURLsList->count() || m_pLocationChooser->text().isEmpty())
		return;

	int nCurrentItemId = m_pURLsList->currentRow();
	saveCurrentUrlDataToList(nCurrentItemId);

	if (nCurrentItemId > m_slURLsStringList.count()-1) {
		qDebug("UrlsManagerDlg::slotGotoUrl. Index out of range!");
		return;
	}
	if (m_eKindOfDialog == HISTORY)
		m_sNewURL = m_slURLsStringList[ nCurrentItemId ];
	else {
		QChar separator = (m_eKindOfDialog == FILTERS) ? '=' : '\\';
		m_sNewURL = m_slURLsStringList[ nCurrentItemId ].section(separator, 1, 1);
	}
	if (m_eKindOfDialog == FAVORITES || m_eKindOfDialog == HISTORY || m_eKindOfDialog == FILTERS)
		if (m_pLocationChooser->text().isEmpty())
			return;

	// save current URL data
	QSettings settings;
	if (m_eKindOfDialog != HISTORY) {
		QString sKindOfURLstr = (m_eKindOfDialog==FTP) ? "FTP/S" : "Favorites/Url";
		settings.writeEntry(QString("/qtcmd/"+sKindOfURLstr+"%1").arg(nCurrentItemId),
		 m_slURLsStringList[ nCurrentItemId ]
		);
	}
	else
	if (m_eKindOfDialog != FILTERS)
		settings.writeEntry("/qtcmd/"+m_sPanelSideName+"FilesPanel/URLs", m_slURLsStringList);

	if (m_eKindOfDialog != FILTERS)
		done(1111);
}

void UrlsManagerDlg::slotItemDoubleClicked( QListWidgetItem * )
{
	slotGotoUrl(); // use by button too
}


void UrlsManagerDlg::slotNew()
{
	m_nOldItemId = m_pURLsList->currentRow();
	int nAllURLs = m_pURLsList->count();
	if (nAllURLs)
		saveCurrentUrlDataToList(m_nOldItemId);

	QString sUrlName, sKindOfURLstr;

	if (m_eKindOfDialog != HISTORY) {
		if (m_eKindOfDialog == FTP)
			sKindOfURLstr = tr("New FTP session");
		else
		if (m_eKindOfDialog == FILTERS)
			sKindOfURLstr = tr("A new filter");
		else
		if (m_eKindOfDialog == FAVORITES)
			sKindOfURLstr = tr("A new favorite");

		bool bOK = FALSE;
		sUrlName = QInputDialog::getText(
			sKindOfURLstr+" - QtCommander", tr("Please to give a name:"),
			QLineEdit::Normal, tr("new name"), &bOK, this
		);
		if (bOK == FALSE)
			return;
	}
	else
		sUrlName = tr("Item")+QString(" %1").arg(m_pURLsList->count());

	m_slURLsStringList.append("x");

	clearAllCells();

	m_pURLsList->insertItem(nAllURLs, sUrlName);
	m_pURLsList->setCurrentRow(nAllURLs);

	if (m_pURLsList->count() == 1) {
		m_pGotoUrlBtn->setEnabled(TRUE);
		m_pDeleteBtn->setEnabled(TRUE);
		m_pRenameBtn->setEnabled(TRUE);
		//m_pSaveBtn->setEnabled(TRUE);
		// enable line-edits (all for FTP)
		m_pLocationChooser->setEnabled(TRUE);
		if (m_eKindOfDialog == FTP) {
			m_pHostEdit->setEnabled(TRUE);
			m_pUserEdit->setEnabled(TRUE);
			m_pPortSpin->setEnabled(TRUE);
			m_pPasswordEdit->setEnabled(TRUE);
		}
	}

	if (m_pHostEdit->isEnabled())
		m_pHostEdit->setFocus();
	else
		m_pLocationChooser->setFocus();
}

void UrlsManagerDlg::slotDelete()
{
	if (m_pURLsList->selectedItems().count() == 0)
		return;

	m_nOldItemId = -1;
	const int nCurrentItemId = m_pURLsList->currentRow();
	if (nCurrentItemId > m_slURLsStringList.count()-1) {
		qDebug("UrlsManagerDlg::slotDelete. Index out of range!");
	}
	else
		m_slURLsStringList.remove(m_slURLsStringList.at(nCurrentItemId));
	m_pURLsList->takeItem(nCurrentItemId);

	if (m_pURLsList->count())
		m_pURLsList->setCurrentRow(m_pURLsList->currentRow());
	else { // The ListBox obj. is bEmpty
		clearAllCells();
		// disable all buttons (except 'New')
		m_pGotoUrlBtn->setEnabled(FALSE);
		m_pDeleteBtn->setEnabled(FALSE);
		m_pRenameBtn->setEnabled(FALSE);
		m_pDownBtn->setEnabled(FALSE);
		//m_pSaveBtn->setEnabled(FALSE);
		m_pUpBtn->setEnabled(FALSE);
		// disable all line-edits
		m_pLocationChooser->setEnabled(FALSE);
		m_pHostEdit->setEnabled(FALSE);
		m_pUserEdit->setEnabled(FALSE);
		m_pPortSpin->setEnabled(FALSE);
		m_pPasswordEdit->setEnabled(FALSE);
	}

	// -- set item after deleted as current item or current if no previous
	QListWidgetItem *prevItem = m_pURLsList->item(m_pURLsList->currentRow()-1);
	QListWidgetItem *nextItem = m_pURLsList->item(m_pURLsList->currentRow()+1);
	if (prevItem != NULL) {
		if (nextItem != NULL)
			m_pURLsList->setCurrentItem(nextItem);
	}
}

void UrlsManagerDlg::slotRename()
{
	//m_pURLsList->openPersistentEditor( m_pURLsList->currentItem() );
	//m_pURLsList->editItem(m_pURLsList->currentItem());
	//m_pURLsList->closePersistentEditor ( m_pURLsList->currentItem() );
	//return;

	QString sKindOfURLstr;
	if (m_eKindOfDialog == FTP)
		sKindOfURLstr = tr("Rename FTP session");
	else
	if (m_eKindOfDialog == FILTERS)
		sKindOfURLstr = tr("Rename a filter");
	else
	if (m_eKindOfDialog == FAVORITES)
		sKindOfURLstr = tr("Rename a favorite");

	bool bOK = FALSE;
	QString sUrlName = QInputDialog::getText(
		sKindOfURLstr+" - QtCommander", tr("Please to give a name:"),
		QLineEdit::Normal, m_pURLsList->currentItem()->text(), &bOK, this
	);
	if (! bOK)
		return;

	int nCurrentItemId = m_pURLsList->currentRow();
	m_pURLsList->item(nCurrentItemId)->setText(sUrlName);

	saveCurrentUrlDataToList(nCurrentItemId);
}

void UrlsManagerDlg::slotMoveUp()
{
	moveCurrentItem( UP );
}

void UrlsManagerDlg::slotMoveDown()
{
	moveCurrentItem( DOWN );
}

void UrlsManagerDlg::slotItemSelected()
{
	int nIndexItem = m_pURLsList->currentRow();
	//qDebug() << "UrlsManagerDlg::slotItemSelected, nIndexItem= " << nIndexItem;
	if (nIndexItem < 0)
		return;

	// --- enable/disable buttons used to an sItem moving
	bool bUpEnabled = TRUE, bDownEnabled = TRUE;
	if (nIndexItem == 0) {
		bUpEnabled   = FALSE;
		bDownEnabled = TRUE;
	}
	else
	if (nIndexItem == m_pURLsList->count()-1) {
		bUpEnabled   = TRUE;
		bDownEnabled = FALSE;
	}
	if (m_pURLsList->count() == 1) {
		bUpEnabled = FALSE,  bDownEnabled = FALSE;
	}
	m_pUpBtn->setEnabled(bUpEnabled);
	m_pDownBtn->setEnabled(bDownEnabled);

	if (m_slURLsStringList.count() == 0)
		return;

	if (m_nOldItemId > -1 && ! m_bLockStore) // auto-save
		saveCurrentUrlDataToList(m_nOldItemId);

	clearAllCells();

	if (nIndexItem > m_slURLsStringList.count()-1) {
		qDebug("UrlsManagerDlg::slotItemSelected. Index out of range!");
		return;
	}
	QString sUrl = m_slURLsStringList[ nIndexItem ];
	if (sUrl.isEmpty() || sUrl == "x")
		return;

	QString sPath;
	QChar separator = (m_eKindOfDialog == FILTERS) ? '=' : '\\';
	//qDebug() << "UrlsManagerDlg::slotItemSelected, (0) sUrl= " << sUrl;

	if (m_eKindOfDialog == FTP) {
		//bool bParseError = FALSE;
		QString sHost, sUser, sPass;//, sPort;
		uint nPort = 0;
	// --- parse sUrl
		if (sUrl.indexOf("\\")+1)
			sUrl = sUrl.section("\\", 1, 1); // get URL (part from right side of separator)
		URL url(sUrl);
		if (! url.parseError()) {
			sUser = url.user();
			sHost = url.host();
			sPass = url.password();
			nPort = url.port();
			sPath = url.path();
		}
		// init fields
		m_bLockIfHighlighted = TRUE;
		m_pHostEdit->setText(sHost);
		m_pUserEdit->setText(sUser);
		m_pPasswordEdit->setText(sPass);
		m_pPortSpin->setValue(nPort);
		m_bLockIfHighlighted = FALSE;
		if (url.parseError()) {
			m_pLocationChooser->setText("");
			return;
		}
	}
	else
	if (m_eKindOfDialog == FAVORITES || m_eKindOfDialog == FILTERS)
		sPath = sUrl.section(separator, 1, 1); // get URL (part with right side of separator)
	else // HISTORY
		sPath = sUrl;

	m_bLockIfHighlighted = TRUE;
	m_pLocationChooser->setText(sPath);
	m_bLockIfHighlighted = FALSE;

	if (m_pHostEdit->text().isEmpty()) { // LOCALfs
		m_pHostEdit->setEnabled(FALSE);
		m_pUserEdit->setEnabled(FALSE);
		m_pPortSpin->setEnabled(FALSE);
		m_pPasswordEdit->setEnabled(FALSE);
	}
	if (m_eKindOfDialog == HISTORY)
		m_pRenameBtn->hide();
	else
		sUrl = m_slURLsStringList[nIndexItem].section(separator, 1); // other then history items have names

	// -- update LocationChooser 'Original path'
	m_pLocationChooser->setOriginalPath(URL(sUrl).path());
}


void UrlsManagerDlg::slotTextChanged( const QString & )
{
	// enable automate save current changed text if highlighting changed
	m_nOldItemId = (! m_bLockIfHighlighted) ? m_pURLsList->currentRow() : -1;
	m_bLockStore = (m_nOldItemId == -1);
}


void UrlsManagerDlg::slotSave()
{
	if (m_pURLsList->count())
		saveCurrentUrlDataToList(m_nOldItemId=m_pURLsList->currentRow());

	if (m_eKindOfDialog == FTP)
	{
		if (m_pHostEdit->text().isEmpty())
		{
			QMessageBox::warning(this, tr("Save data"), tr("Host name is empty!")+"\n\n"+tr("Data not saved."));
			return;
		}
	}
	if (m_pLocationChooser->text().isEmpty())
	{
		QMessageBox::warning(this, tr("Save data"), tr("Directory name is empty!")+"\n\n"+tr("Data not saved."));
		return;
	}

	QSettings settings;
	QString sKindOfURLstr = (m_eKindOfDialog == FTP) ? "FTP/S" : "Favorites/Url";

	// --- remove all reads (in init fun. of current dialog) URLs
	if (m_eKindOfDialog != HISTORY && m_eKindOfDialog != FILTERS) {
		for (uint i=0; i<m_nAllReadURLs; i++)
			settings.removeEntry(QString("/qtcmd/"+sKindOfURLstr+"%1").arg(i));
	}

	// --- save all URLs
	if (m_eKindOfDialog == FILTERS)
		settings.writeEntry("/qtcmd/FilesPanel/UserFilters", m_slURLsStringList);
	else
	if (m_eKindOfDialog != HISTORY) {
		for (int i=0; i<m_slURLsStringList.count(); i++)
			settings.writeEntry(QString("/qtcmd/"+sKindOfURLstr+"%1").arg(i), m_slURLsStringList[i]);
	}
	else
		settings.writeEntry("/qtcmd/"+m_sPanelSideName+"FilesPanel/URLs", m_slURLsStringList);
}

void UrlsManagerDlg::keyPressEvent( QKeyEvent *pKE )
{
	if (! m_pURLsList->hasFocus()) {
		QDialog::keyPressEvent(pKE);
		return;
	}

	switch (pKE->key())
	{
		case Qt::Key_Insert:
			slotNew();
			break;

		case Qt::Key_Delete:
			slotDelete();
			break;

		case Qt::Key_F2:
			slotRename();
			break;

		default:
			QDialog::keyPressEvent(pKE);
			break;
	}
}

