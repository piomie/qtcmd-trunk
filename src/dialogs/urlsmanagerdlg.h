/***************************************************************************
                          urlsmanagerdlg  -  description
                             -------------------
    begin                : pi� mar 11 2005
    copyright            : (c) 2005 by Piotr Mierzwi�ski
    email                : peterm@go2.pl

    copyright            : See COPYING file that comes with this project

 $Id$
 ***************************************************************************/

#ifndef URLSMANAGERDLG_H
#define URLSMANAGERDLG_H

#include "ui_urlsmanagerdialog.h"

#include <QMenu>
#include <QDialog>
#include <QKeyEvent>

class QMenu;
class LocationChooser;
/**
	@author Piotr Mierzwi�ski
*/
/** @short Dialog do zarz�dzania URL-ami.
*/
class UrlsManagerDlg : public QDialog, Ui::UrlsManagerDialog
{
	Q_OBJECT
public:
	UrlsManagerDlg();
	~UrlsManagerDlg();

	enum KindOfDialog {
		FTP=0, HISTORY, FAVORITES, FILTERS, APPLICATIONS, HIDINGPLACE
	};

	/** Statyczna funkcja pokazuj�ca g��wne okno menad�era URLi i filtr�w.
	 @param sNewURL - nowy URL/filtr wybrany przez u�ytkownika,
	 @param eKindOfDialog - rodzaj dialogu, dost�pne warto�ci: FTP, HISTORY, FAVORITES, FILTERS,
	 @param sCurrentPath - bie��ca �cie�ka w panelu (inicjowany jest ni� obiekt LocationChooser-a)
	 @param pParent - wska�nik na rodzica.UrlsManagerDlg
	 Elementy wczytywane s� z pliku konfiguracyjnego po czym wstawiane na list�.
	 */
	static bool showDialog( QString & sNewURL, KindOfDialog eKindOfDialog, const QString & sCurrentPath=QString::null, QWidget *pParent=NULL );

private:
	/** Wczytywane s� tutaj, z pliku konfiguracyjnego i wstawiane na list�, wszystkie elementy listy danego typu dialogu.
	 */
	void initURLs();

	/** Metoda powoduje zapis danych znajduj�cych si� w polach edycyjnych dialogu
	 do bufora (listy) na podanej pozycji.UrlsManagerDlg
	 @param nIndex - pozycja na kt�rej nale�y zapisa� dane.UrlsManagerDlg
	 Je�li pojawi� si� puste pola, wtedy s� one wype�niane domy�lnymi warto�ciami
	 (host='localhost', dir='/', user='anonymous',port='21').
	 */
	void saveCurrentUrlDataToList( int nIndex );

	enum Direct { UP=0, DOWN };

	/** Przenosi bie��cy element w podanym kierunku na li�cie oraz w buforze URLi.
	 * @param eDirect kierunek przesuwu.
	 */
	void moveCurrentItem( Direct eDirect );

	/** Funkcja czy�ci pola edycyjne dialogu, przy czym port dostaje warto�� '21'.
	 */
	void clearAllCells();

private slots:
	/** Slot powoduj�cy dodanie na ko�cu listy nowego elementu.UrlsManagerDlg
	 */
	void slotNew();

	/** W funkcji usuwany jest bie��cy element listy oraz kasowany w buforze.
	 */
	void slotDelete();

	/** Slot powoduje zmian� nazwy (na li�cie oraz w buforze) bie��cego elementu.
	 */
	void slotRename();

	/** Metoda powoduje przeniesienie bie��cego elementu (na li�cie oraz w buforze)
	 o jedn� pozycj� do g�ry.
	 */
	void slotMoveUp();

	/** Metoda powoduje przeniesienie bie��cego elementu (na li�cie oraz w buforze)
	 o jedn� pozycj� w d�.
	 */
	void slotMoveDown();

	/** Slot wywo�ywany w momencie zmiany tekstu w widgetcie pobieraj�cym �cie�k�.
	 */
	void slotTextChanged( const QString & );

	/** Wywo�ywane jest tutaj polecenie s�u��ce do zmiany bie��cej �cieki na aktualn�
	 * na li�cie.
	 */
	void slotGotoUrl();

	/** Slot wywo�ywany w momencie pod�wietlenia elementu listy.UrlsManagerDlg
	 * Ustawiana jest tutaj aktywno�� przycisk�w g�ra/d�, a tak�e wype�niane s� pola
	 * edycyjne dla danych wybranej sesji.
	 */
	void slotItemSelected();

	/** Podw�jne klikni�cie uruchamia slot, kt�ry wywo�uje slotGotoUrl().
	*/
	void slotItemDoubleClicked( QListWidgetItem * pItem );

	/** W funkcji wykonywany jest zapis do pliku konfiguracyjnego, wszystkich
	 element�w.
	 */
	void slotSave();

	/** Przechwytywane s� tutaj wszelkie wci�ni�te klawisze, gdy aktywna jest
	 lista.UrlsManagerDlg
	 @param pKE - zdarzenie wci�ni�tego klawisza.UrlsManagerDlg
	 Wci�ni�cie Insert powoduje uruchomienie obs�ugi wstawienia nowego elementu.
	 Klawisz Delete usuwa bie��cy element.
	 */
	void keyPressEvent( QKeyEvent *pKE );

private:
	QMenu *m_pDirectoryMenu;

	KindOfDialog m_eKindOfDialog;

	uint m_nAllReadURLs;
	int  m_nOldItemId;
	bool m_bLockStore;
	bool m_bLockIfHighlighted;

	QString m_sNewURL;
	QString m_sCurrentPath;
	QString m_sPanelSideName;

	QStringList m_slURLsStringList;

};

#endif
