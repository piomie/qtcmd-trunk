/***************************************************************************
                          fileoverwritedlg  -  description
                             -------------------
    begin                : wto mar 15 2005
    copyright            : (c) 2005 by Piotr Mierzwiński
    email                : peterm@go2.pl

    copyright            : See COPYING file that comes with this project

 $Id$
 ***************************************************************************/
#ifndef FILEOVERWRITEDLG_H
#define FILEOVERWRITEDLG_H

#include "ui_fileoverwritedialog.h"

class UrlInfoExt;
/**
	@author Piotr Mierzwiński
*/
class FileOverwriteDlg : public QDialog, Ui::FileOverwriteDialog
{
	Q_OBJECT
public:
	FileOverwriteDlg();
	~FileOverwriteDlg();

	static int show( const UrlInfoExt & uiTargetFileURL, const UrlInfoExt & uiSourceFileURL );

	enum FileOverwriteBtns { Yes=1, No, DiffSize, Rename, All, Update, None, Cancel };

private:
	QButtonGroup *m_pButtonGroup;

};

#endif
