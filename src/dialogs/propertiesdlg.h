/***************************************************************************
                          propertiesdlg  -  description
                             -------------------
    begin                : pon mar 14 2005
    copyright            : (c) 2005 by Piotr Mierzwi�ski
    email                : peterm@go2.pl

    copyright            : See COPYING file that comes with this project

 $Id$
 ***************************************************************************/

#ifndef PROPERTIESDLG_H
#define PROPERTIESDLG_H

#include "ui_propertiesdialog.h"

#include <QCloseEvent>

class UrlInfoExt;
class FilesAssociation;
/**
	@author Piotr Mierzwi�ski
*/
class PropertiesDlg : public QDialog, Ui::PropertiesDialog
{
	Q_OBJECT
public:
	PropertiesDlg();
	~PropertiesDlg();

	void init( const QStringList & slFileNameList, FilesAssociation *pFA, const QString & sCurrentLoggedUserName );
	void init( const UrlInfoExt & uUrlInfo, FilesAssociation *pFA, const QString & sCurrentLoggedUserName );
	void setSize( long long nWeight, int nDirs, int nFiles, bool bBigInfo );
	bool isRecursive();
	int  currentItem();
	bool changePermission();
	bool changeOwnerAndGroup();
	bool changeTime();
	int  changesFor();

private:
	void initFiltersAndAssociation( FilesAssociation *pFA );
	void setEnableAllChanges( bool bEnable );
	int  permissions();

private:
	QString m_sLoggedUserName;

	FilesAssociation *m_pFilesAssociation;

	enum { USER_PERM=0, GROUP_PERM, OTHER_PERM };

	QButtonGroup *m_pClassButtonGroup;
	QButtonGroup *m_pReadButtonGroup, *m_pWriteButtonGroup, *m_pExecButtonGroup;

	bool m_bReadGroupChanged, m_bWriteGroupChanged, m_bExecGroupChanged;

	void enableButtonGroup( QButtonGroup *pButtonGroup );

private slots:
	void slotStop();
	void slotRefresh();

	/** Funkcja wywo�ywana po wci�ni�ciu przycisku OK.
	*/
	void slotOkPressed();
	void slotInitPermissonForClass( int nPermClass );
	void slotCheckGroupButton( int nButtonId );
	void slotInitAllTabs( const QString & sFileName );
	void slotActivatedName( const QString & sFullName );
	void slotSetButtonGroup( bool bToggled );

protected:
	void closeEvent( QCloseEvent * e );

signals:
	void signalClose();
	void signalRefresh( int );
	void signalGetUrlForFile( const QString & sFileName, UrlInfoExt & uUrlInfo );
	void signalOkPressed( const UrlInfoExt & , bool );

};

#endif
