/***************************************************************************
                          aboutdlg  -  description
                             -------------------
    begin                : �ro gru 10 2008
    copyright            : (c) 2008 by Piotr Mierzwi�ski, Mariusz Borowski
    email                : piom@nes.pl, mariusz@nes.pl

    copyright            : See COPYING file that comes with this project

 $Id$
 ***************************************************************************/
#ifndef ABOUTDLG_H
#define ABOUTDLG_H

#include "ui_aboutdialog.h"

/**
	@author Piotr Mierzwi�ski <piom@nes.pl>
*/
class AboutDlg : public QDialog, Ui::AboutDialog
{
public:
	AboutDlg( const QString & sVersion );

};

#endif
