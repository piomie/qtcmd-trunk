/***************************************************************************
                          helpdialog.xpp  -  description
                             -------------------
    begin                : sun jan 23 2005
    copyright            : (C) 2005 by Piotr Mierzwiński
    email                : peterm@go2.pl

    copyright            : See COPYING file that comes with this project
 $Id$
 ***************************************************************************/
#include <QTextBrowser>
#include <QApplication>
#include <QPushButton>
#include <QLocale>
#include <QFile>

//#include <QtGui>

#include "../icons/previous.xpm"
#include "../icons/next.xpm"
#include "../icons/home.xpm"

#include "fileinfoext.h"
#include "helpdlg.h"


HelpDlg::HelpDlg()
{
	setupUi(this);

	m_pBackButton->setIcon(QPixmap((const char**)previous));
	m_pForwardButton->setIcon(QPixmap((const char**)next));
	m_pHomeButton->setIcon(QPixmap((const char**)home));

	QString sAppsPath = qApp->applicationDirPath();
	QString sHelpPath = FileInfoExt::filePath(sAppsPath);
	//qDebug("sAppsPath=%s, sHelpPath=%s", sAppsPath.toLatin1().data(), sHelpPath.toLatin1().data() );

	// -- set doc path
	QString sLocale   = QLocale().name().left(2); // pl, en, ru, etc.
	//qDebug() << "sLocale= " << sLocale;
	if (QFile::exists(sHelpPath+".tmp")) { // apps. executed from project
		sHelpPath = FileInfoExt::filePath(sHelpPath) + "doc/";
	}
	else { // maybe apps. is installed
		sHelpPath += "share/doc/qtcmd/";
	}
	sHelpPath += sLocale;
	//qDebug() << "helpPath= " << sHelpPath;

	if (QFile::exists(sHelpPath +"/index.html"))
		m_pTextBrowser->setSource(QUrl( sHelpPath +"/index.html" ));
	else
		qDebug("HelpDialog::loadFile(). File %s/\"index.html\" not found!", sHelpPath.toLatin1().data());
}

