/***************************************************************************
                          ftpconnectdlg  -  description
                             -------------------
    begin                : wto mar 15 2005
    copyright            : (C) 2005 by Piotr Mierzwiński
    email                : peterm@go2.pl

    copyright            : See COPYING file that comes with this project

 $Id$
 ***************************************************************************/
//#include <QtGui>
#include <QSettings>

#include "messagebox.h"
#include "ftpconnectdlg.h"


FtpConnectDlg::FtpConnectDlg()
{
	setupUi(this);
}

FtpConnectDlg::~FtpConnectDlg()
{
}


bool FtpConnectDlg::showDialog( QString & sInputURL )
{
	FtpConnectDlg *pDlg = new FtpConnectDlg;
	Q_CHECK_PTR( pDlg );

	pDlg->setCaption( tr("Connect to")+" ... - QtCommander" );
	pDlg->initSession( sInputURL );

	bool bRunConnection = FALSE;
	uint nCount = 0;

	while( pDlg->exec() == QDialog::Accepted ) {
		bRunConnection = TRUE;  nCount = 0;
		if ( pDlg->m_pUserCombo->currentText().isEmpty() )
			MessageBox::critical( 0, tr("Do not gived user name!") );
		else
			nCount++;

		if ( pDlg->m_pPasswordEdit->text().isEmpty() )
			MessageBox::critical( 0, tr("Do not gived password!") );
		else
			nCount++;

		if ( nCount == 2 )
			break;
	}

	if ( bRunConnection ) {
		pDlg->makeNewURL( sInputURL ); // the 'sInputURL' will be include a new URL

		QSettings settings;
		if ( pDlg->m_pSaveSessionChkBox->isChecked() ) {
			// --- save an URL to the end of session list
			int n = -1;
			bool addSession = TRUE;
			QString session = "x";
			while ( ! session.isEmpty() ) { // find number of last session
				n++;
				session = settings.readEntry( QString("/qtcmd/FTP/S%1").arg(n) );
				if ( session.section( "\\", 1,1 ) == sInputURL ) // disable add this same session
					addSession = FALSE;
			}
			if ( addSession )
				settings.writeEntry( QString("/qtcmd/FTP/S%1").arg(n),
					tr("new session")+"\\"+sInputURL );
		}
		// --- add new sUser name (only if not exists it on the list) and save new list
		QString sCurrentUserName = pDlg->m_pUserCombo->currentText();
		bool bAddUser = TRUE;
		for ( int id=0; id < pDlg->m_sUsersList.count(); id++ ) {
			if ( pDlg->m_sUsersList[id] == sCurrentUserName ) {
				bAddUser = FALSE;
				break;
			}
		}
		if ( bAddUser ) { // new sUser, add it's name to the config file
			pDlg->m_sUsersList.append( sCurrentUserName );
			settings.writeEntry( "/qtcmd/FTP/UsersName", pDlg->m_sUsersList, '\\' );
		}
	}

	delete pDlg;
	pDlg = 0;

	return bRunConnection;
}

void FtpConnectDlg::initSession( const QString & sInputURL )
{
	// --- read sUser's list from config file
	QSettings settings;

	m_sUsersList = settings.readListEntry( "/qtcmd/FTP/UsersName", '\\' );
	if ( m_sUsersList.isEmpty() ) {
		m_sUsersList.append( "anonymous" );
		settings.writeEntry( "/qtcmd/FTP/UsersName", "anonymous"/*, '\\'*/ );
	}
	m_pUserCombo->insertStringList( m_sUsersList );


	// --- parse sInputURL
	QString sHost, sDir, sUser = "", sPassword = "";
	int nPort = 21, nMonkeyPos, nColonPos, nPortPos;

	sDir = sInputURL;
	sDir = sDir.remove( "ftp://" );

	nMonkeyPos = sDir.indexOf('@');
	if ( nMonkeyPos != -1 ) {
		sUser = sDir.left( nMonkeyPos ); // cut sUser name (and maybe sPassword)
		nColonPos  = sUser.indexOf(':');
		if ( nColonPos != -1 ) {
			sPassword = sUser.right( sUser.length()-nColonPos-1 );
			sUser = sUser.left( nColonPos );
		}
		sDir = sDir.right( sDir.length()-nMonkeyPos-1 ); // rest (without '@') save into sDir
	}
	sHost = sDir.left( sDir.indexOf('/') );

	nPortPos = sDir.indexOf(':');
	if ( nPortPos != -1 ) {
		nPort = QString(sHost.right(sHost.length()-nPortPos-1)).toInt();
		sHost = sHost.left( nPortPos ); // rest (without 'nPort:') save into sHost
	}
	sDir = sDir.right( sDir.length()-sDir.indexOf('/') );


	m_pDirEdit->setText( sDir );
	m_pHostEdit->setText( sHost );
	m_pPortSpin->setValue( nPort );
	m_pPasswordEdit->setText( sPassword );


	if ( sUser.isEmpty() ) // into an URL is not include sUser
		return;

	bool bInsertUser = TRUE;
	for ( int id=0; id < m_sUsersList.count(); id++ )
		if ( m_sUsersList[id] == sUser ) {
			m_pUserCombo->setCurrentItem( id );
			bInsertUser = FALSE;
			break;
		}

	if ( bInsertUser ) {
		m_pUserCombo->insertItem( sUser );
		m_pUserCombo->setCurrentItem( 0 );
	}
}

void FtpConnectDlg::makeNewURL( QString & sNewURL )
{
	QString sHost = m_pHostEdit->text();
	QString sDir  = m_pDirEdit->text();
	QString sUser = m_pUserCombo->currentText();
	QString sPass = m_pPasswordEdit->text();
	QString nPort = QString::number( m_pPortSpin->value() );

	if ( sDir.at(0) != '/' )
		sDir.insert( 0, '/' );
	if ( sDir.at(sDir.length()-1) != '/' )
		sDir += "/";

	if ( sHost.indexOf("ftp://") == 0 )
		sHost.remove( 0, 6 );

	if ( sHost.isEmpty() )
		sHost = "localhost";
	if ( sUser.isEmpty() )
		sUser = "anonymous";
	if ( nPort.isEmpty() )
		nPort = "21";

	uint hostLen = sHost.length()-1;
	if ( sHost.at(hostLen) == '/' ) // sHost name cannot be ended by the slash
		sHost.truncate( hostLen );

	sNewURL = "ftp://"+sUser+":"+sPass+"@"+sHost+":"+nPort+sDir;
}
