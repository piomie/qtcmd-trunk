/***************************************************************************
                          inputdlg.h  -  description
                             -------------------
    begin                : pi� mar 11 2005
    copyright            : (c) 2005 by Piotr Mierzwi�ski
    email                : peterm@go2.pl

    copyright            : See COPYING file that comes with this project

 $Id$
 ***************************************************************************/
#ifndef INPUTDLG_H
#define INPUTDLG_H

#include "vfs.h"
#include "enums.h"

#include "ui_inputdialog.h"

/**
	@author Piotr Mierzwi�ski
*/
/** @short Dialog wspiera operacje: MakeLink, Copy, Move, Rename.
*/
class InputDlg : public QDialog, Ui::InputDialog
{
public:
	InputDlg();
	~InputDlg();

	/** Funkcja pokazuje dialog pobieranie nazwy docelowej dla podanej operacji.
	 * @param sInfoFileText - nazwa pliku, kt�rego dotyczy operacja,
	 * @param sInitEditText - domy�lna nazwa docelowa dla bie��cej operacji,
	 * @param eOperation - wykonywana operacja,
	 * @param sKeyListReading - nazwa klucza w pliku konfiguracyjnym dla listy, kt�r� nale�y wstawi� w ComboBox,
	 * @param bOkPressed - zawiera informacje o wci�ni�ciu przycisku OK na bie��cym dialogu
	 */
	static QString getText( const QString & sInfoFileText, const QString & sInitEditText, VFS::Operation eOperation, const QString & sKeyListReading, bool & bOkPressed, bool & bP1, bool & bAlwaysOvr, bool bP3=FALSE, bool bDirsOnly=TRUE );

private:
	bool m_bShowDirsOnly;

	QString m_sTargetPath;

};

#endif
