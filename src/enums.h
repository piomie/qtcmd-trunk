
#ifndef ENUMS_H
#define ENUMS_H

#include <qdir.h>

// enum KindOfPanel       { FILESpanel, FILEVIEWpanel, UNKNOWNpanel };
enum KindOfListView    { LIST_PANELVIEW=0, TREE_PANELVIEW, UNKNOWN_PANELVIEW };

enum KindOfFormating { NONEformat=0, THREEDIGITSformat, BKBMBGBformat };

enum KindOfFile { SOURCEfile, RENDERfile, IMAGEfile, SOUNDfile, ARCHIVEfile, VIDEOfile, UNKNOWNfile };
enum KindOfViewer  { INTERNALviewer=0, EXTERNALviewer, UNKNOWNviewer };

enum KindOfArchive { ARJarch, RARarch, ZIParch, LZHarch, ZOOarch,  TARarch, TBZIP2arch, TGZIParch, TZarch,  CPIOarch, CGZIParch, CZarch,  UNKNOWNarch,  COMPRESScompr, BZIP2compr, GZIPcompr, UNKNOWNcompr };
enum KindOfPackage { DEBpkg, RPMpkg, SLACKWAREpkg, UNKNOWNpkg };

#endif
