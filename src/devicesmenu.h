/***************************************************************************
                          devicesmenu.h  -  description
                             -------------------
    begin                : Fri May 30 2003
    copyright            : (C) 2003 by Piotr Mierzwiński
    email                : peterm@go2.pl

    copyright            : See COPYING file that comes with this project

 $Id$
 ***************************************************************************/

/** @file doc/api/src/devicesmenu.dox */

#ifndef DEVICESMENU_H
#define DEVICESMENU_H

#include <QProcess>

#include "menuext.h"

class DevicesMenu: public MenuExt
{
	Q_OBJECT
public:
	DevicesMenu( QWidget *pParent=NULL );
	~DevicesMenu();

	enum Operation { OPENDIR=0, MOUNT, UNMOUNT };

	int exec( const QPoint & position );

private:
	Operation m_eCurrentOperation;

	QString  m_sMountPath;

	QProcess *m_pProcess;
	QDialog  *m_pInfoDlg;

	bool m_bActionIsFinished;


	bool init();
	void showInfoDlg( const QString & sCaption, const QString & sMsg );
	void runAction();
	void runProcess( Operation eOperation, const QString & sArguments );

private slots:
	void slotProcessFinished( int nExitCode, QProcess::ExitStatus exitStatus );
	void slotBreakProcess();
	void slotUnmount();
	void slotMount();

signals:
	void signalPathChanged( const QString & sPath );

};

#endif
