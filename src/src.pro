# Plik utworzony przez menad?era projekt�w kdevelopa
# ------------------------------------------- 
# Podkatalog wzgl?dem g?�wnego katalogu projektu: ./src
# Cel to program:  qtcmd

INSTALLS += target 
target.path = $$PREFIX/bin 
#The following line was changed from FORMS to FORMS3 by qt3to4
TRANSLATIONS += ../translations/pl/qtcmd.ts 
HEADERS += plugins/vfs.h \
           plugins/view.h \
           plugins/pluginmanager.h \
           qtcmd.h \
           panel.h \
           enums.h \
           pathview.h \
           fsmanager.h \
           filespanel.h \
           findcriterion.h \
           devicesmenu.h \
           popupmenufp.h \
           fileview.h \
           dialogs/urlsmanagerdlg.h \
           dialogs/findfiledlg.h \
           dialogs/propertiesdlg.h \
           dialogs/ftpconnectdlg.h \
           dialogs/progressdlg.h \
           dialogs/fileoverwritedlg.h \
           dialogs/inputdlg.h \
           dialogs/aboutdlg.h \
           dialogs/helpdlg.h \
           listview.h \
           listviewitem.h \
           filespanelforfind.h \
           config.h \
           filelistview.h
SOURCES += dialogs/urlsmanagerdlg.cpp \
           dialogs/findfiledlg.cpp \
           dialogs/propertiesdlg.cpp \
           dialogs/ftpconnectdlg.cpp \
           dialogs/progressdlg.cpp \
           dialogs/fileoverwritedlg.cpp \
           dialogs/inputdlg.cpp \
           dialogs/aboutdlg.cpp \
           dialogs/helpdlg.cpp \
           plugins/pluginmanager.cpp \
	       devicesmenu.cpp \
           main.cpp \
           panel.cpp \
           qtcmd.cpp \
           pathview.cpp \
           fileview.cpp \
           fsmanager.cpp \
           popupmenufp.cpp \
	       filespanel.cpp \
           listview.cpp \
           listviewitem.cpp \
           filespanelforfind.cpp \
           config.cpp \
           filelistview.cpp 
FORMS += dialogs/fileoverwritedialog.ui \
			dialogs/findfiledialog.ui \
			dialogs/ftpconnectdialog.ui \
			dialogs/inputdialog.ui \
			dialogs/progressdialog.ui \
			dialogs/propertiesdialog.ui \
			dialogs/urlsmanagerdialog.ui \
			dialogs/helpdialog.ui \
			dialogs/aboutdialog.ui
TARGETDEPS += ../build/lib/libqtcmddlgext.so \
              ../build/lib/libqtcmdutils.so \
              ../build/lib/libqtcmduiext.so
LIBS += -lqtcmduiext \
        -lqtcmdutils \
        -lqtcmddlgext
INCLUDEPATH += . \
               plugins \
               plugins/fp \
               plugins/view \
               ../src/plugins/fp/localfs \
               ../src/libs/qtcmddlgext \
               ../src/libs/qtcmdutils \
               ../src/libs/qtcmduiext \
               libs/.ui \
               dialogs \
               ../icons
MOC_DIR = ../build/.tmp/moc
UI_DIR = ../build/.tmp/ui
OBJECTS_DIR = ../build/.tmp/obj
QMAKE_LIBDIR = ../build/lib
QMAKE_CXXFLAGS_RELEASE += -O2
QMAKE_CXXFLAGS_DEBUG += -g3
TARGET = qtcmd
DESTDIR = ../build/bin
CONFIG += warn_on \
			qt \
			thread
TEMPLATE = app
#The following line was inserted by qt3to4
QT += network  qt3support 
#The following line was inserted by qt3to4
CONFIG += uic4
QMAKE_CLEAN += "../qtcmd.kdevses ../qtcmd.kdevelop.pcs"
