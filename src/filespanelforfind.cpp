/***************************************************************************
                          filespanelforfind.cpp  -  description
                             -------------------
    begin                : �ro kwi 20 2005
    copyright            : (C) 2005 by Piotr Mierzwi�ski
    email                : peterm@go2.pl

    copyright            : See COPYING file that comes with this project

 $Id$
 ***************************************************************************/

#include "pathview.h"
#include "listview.h"
#include "filespanelforfind.h"
#include <stdlib.h>
//Added by qt3to4:
#include <QResizeEvent>

//#define PATHVIEW_HEIGHT		27

FilesPanelForFind::FilesPanelForFind(const QString & sInitURL, QWidget * pParent, Panel::KindOfPanel eKindOfPanel, const char * sz_pName)
	: FilesPanel(sInitURL, pParent, eKindOfPanel, sz_pName)
{
	qDebug("---- name=%s, sInitURL=%s", name(), sInitURL.toLatin1().data() );

	m_pListView->setColumn(ListView::LOCATIONcol, 170);
	m_pListView->setKindOfView(LIST_PANELVIEW);
	//m_pListView->setCurrentURL(sInitURL);

	// ukryc przycisk przy historii sciezek
	// historia sciezek nieaktywna
	m_pPathView->setEnabled(FALSE);
	// albo:
	// utworzyc nowy obiekt klasy LocationChooser z linijka edycyjna tylko do odczytu
	m_pLE_PathView = new QLineEdit(tr("Found files list."), this);
	m_pLE_PathView->setReadOnly(TRUE);
	m_pLE_PathView->setFocusPolicy(Qt::NoFocus);
}


FilesPanelForFind::~FilesPanelForFind()
{
	delete m_pLE_PathView;
}

	// po jednym elemencie jest wstawiane na liste, petla nie jest potrzebna ! i jednorazowo mozna ust. updateView
void FilesPanelForFind::updateView()
{
	FilesPanel::updateView();

	ListView::SelectedItemsList *pItemsList = NULL;
	emit signalGetSILfromPanelBeside( pItemsList );
	//qDebug("FilesPanelForFind(); pItemsList=%d", (int)pItemsList ); // == NULL

	if (pItemsList == NULL)
		return;

	//qDebug("FilesPanelForFind();  pItemsList->count()=%d, m_pPathView->currentUrl()=%s", pItemsList->count(), m_pPathView->currentUrl().toLatin1().data() );
	m_pListView->initFilesList(m_pPathView->currentUrl(), pItemsList->count(), "root", "root", TRUE);

	ListView::SelectedItemsList::iterator it;
	Q3ListViewItem *item;
	UrlInfoExt ui;

	// -- fill the panel list by the FindFilesDialog list
	for (it = pItemsList->begin(); it != pItemsList->end(); ++it)
	{
		item = (*it);

		ui.setName(item->text(0));
		ui.setSize(::atoll(item->text(7)));
		ui.setLastModified(QDateTime::fromString(item->text(2), Qt::ISODate));
		ui.setPermissionsStr(item->text(3));
		ui.setOwner(item->text(4));
		ui.setGroup(item->text(5));
		ui.setLocation(item->text(6));

		ui.setFile(TRUE);

		m_pListView->slotInsertItem(ui);
	}

	m_pListView->countAndWeightSelectedFiles(FALSE); // lista pItemsList nie jest czyszczona !
	//qDebug("FilesPanelForFind();  pItemsList->count()=%d", pItemsList->count());

	//emit signalUpdateOfListStatus( mNumOfSelectedItems, numOfAllItems(), mWeightOfSelected );
}


void FilesPanelForFind::resizeEvent( QResizeEvent * e )
{
//	qDebug("FilesPanelForFind::resizeEvent(),  width()=%d, height()=%d", width(), height());
	FilesPanel::resizeEvent(e);
//	qDebug("e->size().width()=%d, e->size().height()=%d", e->size().width(), e->size().height());

// 	m_pPathView->setGeometry(0, 0, width(), PATHVIEW_HEIGHT );
// 	m_pListView->setGeometry( 0, PATHVIEW_HEIGHT-2, width(), height()-(PATHVIEW_HEIGHT+STATUSBAR_HEIGHT) ); // PATHVIEW_HEIGHT+1+
// 	m_pStatusBar->setGeometry( 0, m_pListView->height()+PATHVIEW_HEIGHT-1, width()-m_pFilterButton->width(), STATUSBAR_HEIGHT );
// 	m_pFilterButton->setGeometry( m_pStatusBar->width(), m_pListView->height()+PATHVIEW_HEIGHT-1, m_pFilterButton->width(), STATUSBAR_HEIGHT );
	m_pLE_PathView->setGeometry(0, 0, width(), PATHVIEW_HEIGHT );
}

