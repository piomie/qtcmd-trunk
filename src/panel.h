/***************************************************************************
                         panel.h  -  description
                            -------------------
   begin                : nie lut 20 2005
   copyright            : (C) 2005 by Piotr Mierzwinski
   email                : peterm@o2.pl

   copyright            : See COPYING file that comes with this project

$Id$
***************************************************************************/

#ifndef PANEL_H
#define PANEL_H

#include <qwidget.h>
#include <q3valuelist.h>
//Added by qt3to4:
#include <QResizeEvent>

#include "vfs.h"

class Q3ListViewItem;

class Filters;
class UrlInfoExt;
class KeyShortcuts;
class FilesAssociation;
class ListViewSettings;
class FileSystemSettings;
class OtherAppSettings;

/**
	@author Piotr Mierzwiński
*/
class Panel : public QWidget
{
	Q_OBJECT
public:
	Panel( uint nWidth, uint nHeight, QWidget *pParent, const char *sz_pName=0 );
	virtual ~Panel() {};

	virtual bool fileSystemCreated() const { return FALSE; };
	virtual bool fileViewCreated()   const { return FALSE; };

	virtual QString currentURL() const { return QString::null; }
	virtual bool hasFocus() const { return FALSE; }
	virtual int  kindOfView() const { return -1; }

	virtual void updateView() = 0;

	virtual void setKeyShortcuts( KeyShortcuts * ) = 0;
	virtual void setFilesAssociation( FilesAssociation * ) = 0;

	// --- FilesPanel
	virtual void openCurrentFile( bool ) {}
	virtual void createNewEmptyFile( bool ) {}
	virtual void putFile( const QByteArray & , const QString & ) {}
	virtual void copyFiles( bool ) {}

	virtual void setFilters( Filters * )  {};
	virtual void resizeSection( uint , uint , uint ) {}
	virtual void setPanelBesideURL( const QString & ) {}
	virtual void getKindOfFS( VFS::KindOfFilesSystem & ) {}
	virtual void updateList( int , Q3ValueList<Q3ListViewItem *> *, const UrlInfoExt & ) {}
	virtual void getSelectedItemsList( Q3ValueList<Q3ListViewItem *> *& ) {}

	// --- FileView
	virtual void initLoadFileToView( FilesAssociation * ) {}


	enum KindOfPanel { FILES_PANEL=0, FILES_PANEL_FORFIND, FILEVIEW_PANEL, UNKNOWN_PANEL };
	virtual KindOfPanel kindOfPanel() const { return UNKNOWN_PANEL; }
	virtual void SetKindOfPanel( KindOfPanel ) {}


public slots:
	virtual void slotChangeKindOfView( uint ) = 0;
	// --- FilesPanel
	virtual void slotShowFtpManagerDlg()  {};
	virtual void slotRereadCurrentDir()   {};
	virtual void slotShowSimpleDiscStat() {};
	virtual void slotShowDevicesMenu()    {};
	virtual void slotShowFavorites()      {};
	virtual void slotQuickGetOutFromFS()  {};
	virtual void slotShowFindFileDlg()    {};
	virtual void slotPathChanged( const QString & ) {}

	virtual void slotCopy( bool , bool ) {}
	virtual void slotDelete()   {}
	virtual void slotMakeLink() {}

	virtual void slotApplyListViewSettings( const ListViewSettings & )     {};
	virtual void slotApplyFileSystemSettings( const FileSystemSettings & ) {};
	virtual void slotApplyOtherAppSettings( const OtherAppSettings & )     {};


protected:
//FIXME: why it's a pure virtulal ?
//	virtual void resizeEvent( QResizeEvent * ) = 0;

signals:
	// --- FilesPanel
	void signalHeaderSizeChange( int, int, int );
	void signalSetPanelBesideURL( const QString & );
	void signalChangePanel( Panel::KindOfPanel, const QString & , bool );

	void signalCopyFiles( bool );
	//void signalGetKindOfFSInPanelBeside( VFS::KindOfFilesSystem & );
	void signalUpdateSecondPanel( int, Q3ValueList<Q3ListViewItem *> *, const UrlInfoExt & );
	void signalGetSILfromPanelBeside( Q3ValueList<Q3ListViewItem *> *& );
	void signalApplyKeyShortcut( KeyShortcuts * );
	void signalPutFile( const QByteArray &, const QString & );

	// --- FileView
	void signalUpdateItemOnLV( const QString & );
	void signalClose();

};

#endif
