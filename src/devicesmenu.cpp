/***************************************************************************
                          devices.cpp  -  description
                             -------------------
    begin                : Fri May 30 2003
    copyright            : (C) 2003 by Piotr Mierzwiński
    email                : peterm@go2.pl

    copyright            : See COPYING file that comes with this project

 $Id$
 ***************************************************************************/
#include <QtGui>

#include "messagebox.h"
#include "devicesmenu.h"

#include "mounted.xpm"
#include "notmounted.xpm"

#include "enums.h"
#include "functions.h"  // for few static functions
#include "systeminfo.h" // for getting capacity and free space on device


DevicesMenu::DevicesMenu( QWidget *pParent )
	: MenuExt(pParent), m_pInfoDlg(NULL)
{
	m_pProcess = new QProcess;
	connect( m_pProcess, SIGNAL(finished(int, QProcess::ExitStatus)), this, SLOT(slotProcessFinished(int, QProcess::ExitStatus)) );
}


DevicesMenu::~DevicesMenu()
{
	delete m_pProcess;
	if (m_pInfoDlg != NULL)
		delete m_pInfoDlg;
}


bool DevicesMenu::init()
{
	QFile   file;
	QString sLine;
	QString sMtabFileBuffer, sFStabFileBuffer;

	// -- read /etc/mtab to buffer
	file.setFileName("/etc/mtab");
	if (file.open(QIODevice::ReadOnly)) {
		QTextStream stream(&file);
		sMtabFileBuffer = stream.readAll(); // read entire stream
		file.close();
	}
	else {
		MessageBox::warning(this,
			tr("Error")+" - QtCommander",
			tr("Cannot open file")+": /etc/mtab\n"+
			tr("Cannot get the information about mounted devices")
		);
	}

	// -- read /etc/fstab to buffer (support UUID)
	file.setFileName("/etc/fstab");
	if (! file.open(QIODevice::ReadOnly)) {
		MessageBox::critical(this, tr("Cannot open file")+": /etc/fstab");
		return FALSE;
	}
	else {
		QTextStream stream(&file);
		do {
			sLine = stream.readLine(); // read line by line
			if (sLine.indexOf("swap") < 0) {
				if (sLine.startsWith("UUID=", Qt::CaseInsensitive) || sLine.startsWith("/dev", Qt::CaseInsensitive))
					sFStabFileBuffer += sLine + "\n";
			}
		}
		while (! sLine.isNull());
		file.close();
	}

	if (sFStabFileBuffer.size() > 0) {
		// -- read directory /dev/disk/by-uuid
		QDir dir("/dev/disk/by-uuid");
		dir.setFilter(QDir::Files | QDir::NoDotAndDotDot);

		QFileInfo fileInfo;
		QFileInfoList fiList = dir.entryInfoList();
		for (int i = 0; i < fiList.size(); i++) {
			fileInfo = fiList.at(i);
			if (fileInfo.isSymLink()) {
				//qDebug() << "fileName= " << fileInfo.fileName() << ", symlinkTarget= " << fileInfo.symLinkTarget();
				sFStabFileBuffer.replace(fileInfo.fileName(), fileInfo.symLinkTarget());
				sFStabFileBuffer.replace("UUID=", "", Qt::CaseInsensitive);
			}
		}
	}
	QStringList slFStab = sFStabFileBuffer.split("\n");
	//qDebug() << "sFStabFileBuffer=\n" << sFStabFileBuffer;

	// --- create menu
	enum mtabCols { DEV=0, PATH, FS };
	static const char **iconName;
	QStringList slFStabLine;
	bool bIsMounted = FALSE;
	QMenu *pSubMenu = NULL;
	char cCharId = 'a'; // id for menu items
	QString sPrefix;
	long long freeBytes, allBytes;
	QString freeBytesStr, totalBytesStr;
	QString sMenuTitle;
	QAction *a1, *a2;

	clear(); // menu (current object)

	for (int i=0; i<slFStab.count()-1; i++) {
		sLine = slFStab.at(i);
		slFStabLine = sLine.split(QRegExp("[\\s][\\t]*")); // split by space and tab
		bIsMounted  = sMtabFileBuffer.indexOf(slFStabLine[DEV])+1;
		iconName = (bIsMounted) ? mounted : notmounted;

		if (cCharId == 'z'+1) // TODO add support to character-id for more than 25 devices (maybe to use digits)
			cCharId = 'a';
		sPrefix = "&"+QString((QChar)cCharId)+"  ";
		cCharId++;
		sMenuTitle = slFStabLine[PATH];

		if (bIsMounted) {
			// get device capacity and free space
			SystemInfo::getStatFS( sMenuTitle, freeBytes, allBytes );
			totalBytesStr = formatNumber( allBytes,  BKBMBGBformat );
			freeBytesStr  = formatNumber( freeBytes, BKBMBGBformat );
			sMenuTitle = sPrefix+slFStabLine[PATH]+"\t("+totalBytesStr+"/"+freeBytesStr+")";
			// create menu items
			if (slFStabLine[PATH] != "/" && slFStabLine[PATH] != "/usr" && slFStabLine[PATH] != "/var") {
				pSubMenu = new QMenu(this);
				pSubMenu->setTitle(sMenuTitle);
				pSubMenu->setIcon(QIcon(iconName));
				a1 = pSubMenu->addAction( tr("Open") );
				a1->setStatusTip("open "+slFStabLine[PATH]); // set command processed in exec()
				a2 = pSubMenu->addAction( tr("Unmount") );
				a2->setStatusTip("unmount "+slFStabLine[PATH]);
				addMenu(pSubMenu);
			}
			else {
				a1 = addAction( QIcon(iconName), sMenuTitle, slFStabLine[DEV]+" "+slFStabLine[FS] ); // icon,text,toolTip
				a1->setStatusTip("open "+slFStabLine[PATH]);
			}
		}
		else {
			a1 = addAction( QIcon(iconName), sPrefix+slFStabLine[PATH], slFStabLine[DEV]+" "+slFStabLine[FS] ); // icon,text,toolTip
			a1->setStatusTip("mount "+slFStabLine[PATH]);
		}
	}

	return TRUE;
}


void DevicesMenu::showInfoDlg( const QString & sCaption, const QString & sMsg  )
{
	if ( m_pInfoDlg != NULL ) {
		delete m_pInfoDlg;  m_pInfoDlg = NULL;
	}
	m_pInfoDlg = new QDialog;
	// dialog should be unresizeable
	m_pInfoDlg->setMinimumSize(320, 140);
	m_pInfoDlg->setMaximumSize(320, 140);
	m_pInfoDlg->setWindowTitle(sCaption+" - QtCommander");

	QLabel *pLab = new QLabel(sMsg, m_pInfoDlg);
	pLab->setGeometry(20, 30, 250, 40);

	QPushButton *btn = new QPushButton(tr("&Break"), m_pInfoDlg);
	btn->move(m_pInfoDlg->width()/2-btn->width()/2, m_pInfoDlg->height()-btn->height()-15);
	connect(btn, SIGNAL(clicked()), this, SLOT(slotBreakProcess()));

	m_bActionIsFinished = FALSE;
	m_pInfoDlg->exec();
	while (! m_bActionIsFinished)
		m_pInfoDlg->exec();
}


int DevicesMenu::exec( const QPoint & position )
{
	if (! init()) // read information and build the menu
		return -1;

	QAction *pAction  = QMenu::exec(parentWidget()->mapToGlobal(position));
	if (pAction == NULL)
		return -1;

	enum mtabCols { COMMAND=0, PATH };
	QString sCommand = pAction->statusTip();
	QStringList slCommand = sCommand.split(QRegExp("[\\s][\\t]*")); // split by space and tab

	if (sCommand.startsWith("open")) {
		qDebug() << "open dir: " << slCommand.at(PATH);
		emit signalPathChanged(slCommand.at(PATH));
	}
	else
	if (sCommand.startsWith("mount")) {
		qDebug() << "mount dir: " << slCommand.at(PATH);
		m_sMountPath = slCommand.at(PATH);
		m_eCurrentOperation = MOUNT;
		runAction();
	}
	else
	if (sCommand.startsWith("unmount")) {
		qDebug() << "unmount dir: " << slCommand.at(PATH);
		m_sMountPath = slCommand.at(PATH);
		m_eCurrentOperation = UNMOUNT;
		runAction();
	}

	return 0;
}


void DevicesMenu::runProcess( Operation eOperation, const QString & sArguments )
{
	QString sProcessName = (eOperation == MOUNT) ? "mount" : "umount";
	QString sProcess     = sProcessName+" "+sArguments;

	qDebug() << "DevicesMenu::runProcess, exec=" << sProcess;

	m_pProcess->start(sProcess);
}


void DevicesMenu::runAction()
{
	if (m_eCurrentOperation == MOUNT)
		QTimer::singleShot(0, this, SLOT(slotMount()));
	else
	if (m_eCurrentOperation == UNMOUNT)
		QTimer::singleShot(0, this, SLOT(slotUnmount()));

	// --- show an info dialog
	QString sAction = (m_eCurrentOperation == MOUNT) ? tr("Mounting") : tr("Unmounting");
	QString sMsg    = tr("Please wait.")+"\n"+sAction+" "+tr("is still processed")+"...";

	showInfoDlg(sAction+" - QtCommander", sMsg);
}


void DevicesMenu::slotUnmount()
{
	runProcess(UNMOUNT, m_sMountPath);
}


void DevicesMenu::slotMount()
{
	runProcess(MOUNT, m_sMountPath);
}



void DevicesMenu::slotProcessFinished( int nExitCode, QProcess::ExitStatus exitStatus )
{
	//qDebug() << "DevicesMenu::slotProcessFinished, nExitCode= " << nExitCode << ", exitStatus= " << exitStatus;
	m_bActionIsFinished = TRUE;
	if (m_pInfoDlg) {
		delete m_pInfoDlg;  m_pInfoDlg = NULL;
	}

	if (exitStatus == QProcess::NormalExit && nExitCode == 0)
		emit signalPathChanged( m_sMountPath );
	else { // error occures
		QString sError;
		if (nExitCode == 32) // try mount
			sError = tr("No medium found.");
		else
		if (nExitCode == 1 || nExitCode == 2) // try umount
			sError = tr("Device is busy.");
// 		else
// 		if (nExitCode == 2) // umount
// 			sError = tr("Device is not mounted (according to mtab)");
		else
		if (nExitCode != 0)
			sError = tr("Error no.")+" "+QString::number(nExitCode);

		QString sAction = (m_eCurrentOperation == MOUNT) ? tr("mount") : tr("umount");
		MessageBox::critical(this, tr("Cannot not")+" "+sAction+" "+tr("device for directory")+" "+m_sMountPath+"\n\n"+sError);
		//MessageBox::critical(this, tr("Cannot not")+" "+sAction+" "+tr("device for directory")+" "+m_sMountPath+".\n\n"+sError);
		qDebug() << "DevicesMenu::slotProcessFinished, nExitCode= " << nExitCode;
	}
}


void DevicesMenu::slotBreakProcess()
{
	if (m_pProcess->exitStatus() == QProcess::CrashExit) { // condition maybe not necessary (not tested) :/
		m_pProcess->kill();
		m_bActionIsFinished = TRUE;
		delete m_pInfoDlg; m_pInfoDlg = 0;
	}
}
