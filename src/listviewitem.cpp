/***************************************************************************
                          listviewitem.cpp  -  description
                             -------------------
    begin                : Mon Jul 9 2001
    copyright            : (C) 2002 by Piotr Mierzwiński
    email                : peterm@go2.pl

    Copyright: See COPYING file that comes with this project

 $Id$
 ***************************************************************************/

#include "listview.h"
#include "listviewitem.h"
#include "fileinfoext.h" // for isArchive()

//#include "../icons/folder.xpm"
//#include "../icons/folder_lock.xpm"
#include "../icons/folder_open.xpm"
#include "../icons/package_open.xpm"
//#include "../icons/folder_link.xpm"

#include <qpainter.h>
//Added by qt3to4:
#include <QPixmap>


ListViewItem::ListViewItem( ListView *pParent )
	: Q3ListViewItem( pParent ), m_pListView(pParent)
{
	m_pListViewItem = 0;
	m_bShowDirSize = FALSE;
	m_pHeader = pParent->header();

// 	pParent->setPaletteBackgroundColor( Qt::white );
	pParent->setColor( QColorGroup::Text, Qt::black ); // setTextColor
	pParent->setColor( QColorGroup::Highlight, Qt::white ); // setHighlightBgColor
	pParent->setColor( QColorGroup::HighlightedText, Qt::blue );
	//setHighlightedTextColor, highlightedTextColor()
}


ListViewItem::ListViewItem( ListViewItem *pParent ) : Q3ListViewItem( pParent ) {}


ListViewItem::ListViewItem( ListView *pParent, const UrlInfoExt & urlInfo )
	: Q3ListViewItem( pParent, urlInfo.name() ), m_uiEntryInfo( urlInfo )
{
	m_pListViewItem = 0;
	m_bShowDirSize = FALSE;
	mIsDir = urlInfo.isDir();

	if ( pParent->showIcons() )
		setPixmap( *pParent->icon( urlInfo ) );
}


ListViewItem::ListViewItem( ListViewItem *pParent, const UrlInfoExt & urlInfo )
	: Q3ListViewItem( pParent, urlInfo.name() ), m_uiEntryInfo( urlInfo )
{
	m_bShowDirSize = FALSE;
	m_pListViewItem = pParent;
	mIsDir = urlInfo.isDir();

	if ( listViewExt()->showIcons() )
		setPixmap( *listViewExt()->icon(urlInfo) );
}


void ListViewItem::show()
{
	if ( height() )
		return;

	QFontMetrics fm( QFont(
	 ((ListView *)listView())->fontFamily(), // listViewExt()->fontFamily() powoduje: "syntax error before ->"
	 listViewExt()->fontSize(),
	 QFont::Light)
	);

	Q3ListViewItem::setHeight( fm.height()+4 ); // 4 for margin
}


QString ListViewItem::key( int column, bool ascending ) const
{
	bool dots = (text(ListView::NAMEcol) == "..");
	char prefix;

	if ( ascending )
		prefix = dots ? '0' : ( mIsDir ? '1' : '2' );
	else
		prefix = dots ? '4' : ( mIsDir ? '3' : '2' );

	QString key = (column != 1) ? text( column ) : text( column ).rightJustify( 24, '0' );

	return prefix + key;
}


QString ListViewItem::text( int column ) const
{
	if ( m_uiEntryInfo.name().isEmpty() )
		return QString("");

	switch ( column )
	{
		case ListView::NAMEcol:
        {
            QString name = m_uiEntryInfo.name();
            if (name.endsWith('/'))
                name = name.left(name.length() - 1);
            return name;
        }
		case ListView::SIZEcol:
			return (m_uiEntryInfo.isDir()) ? (m_bShowDirSize ? m_uiEntryInfo.sizeStr(BKBMBGBformat) : QString("<DIR>")) : m_uiEntryInfo.sizeStr();
		case ListView::TIMEcol:     return m_uiEntryInfo.lastModifiedStr();
		case ListView::PERMcol:     return m_uiEntryInfo.permissionsStr();
		case ListView::OWNERcol:    return m_uiEntryInfo.owner();
		case ListView::GROUPcol:    return m_uiEntryInfo.group();
		case ListView::LOCATIONcol: return m_uiEntryInfo.location();
	}

	return QString("???");
}


void ListViewItem::setText( int column, const QString & str )
{
//	int clmn = listViewExt()->sectionMap( column );

		 if ( column == ListView::NAMEcol  )  m_uiEntryInfo.setName( str );
	// QString in Qt-3.1.x not supports 'long long' type !
//	else if ( clmn == ListView::SIZEcol  )  m_uiEntryInfo.setSize( QString(str).toDouble() );
	else if ( column == ListView::SIZEcol  )  m_uiEntryInfo.setSize( QString(str).toLongLong() );
	else if ( column == ListView::TIMEcol  )  m_uiEntryInfo.setLastModified( QDateTime::fromString(str, Qt::LocalDate/*Qt::ISODate*/) );
	else if ( column == ListView::PERMcol  )  m_uiEntryInfo.setPermissionsStr( str );
	else if ( column == ListView::OWNERcol )  m_uiEntryInfo.setOwner( str );
	else if ( column == ListView::GROUPcol )  m_uiEntryInfo.setGroup( str );
	else if ( column == ListView::LOCATIONcol )  m_uiEntryInfo.setLocation( str );
}


void ListViewItem::setPixmap( QPixmap p )
{
	Q3ListViewItem::setPixmap( ListView::NAMEcol, p );
}


void ListViewItem::setup()
{
	if ( listViewExt()->isTreeView() ) {
		setExpandable( FALSE );
		if ( mIsDir )
			if ( m_uiEntryInfo.isReadable() && ! m_uiEntryInfo.isEmpty() )
				setExpandable( TRUE );
	}

	Q3ListViewItem::setup();
}


void ListViewItem::setOpen( bool open )
{
	bool isTreeView = listViewExt()->isTreeView();

	if ( isTreeView ) {
		Q3ListViewItem::setOpen( open );
		if ( open ) {
			if ( FileInfoExt::isArchive(text(0)) )
				setPixmap( QPixmap(package_open) );
			else
				setPixmap( QPixmap(folder_open) );
		}
		if ( m_uiEntryInfo.name() == "" )
			hide();
	}
}


QString ListViewItem::fullName( bool addSlashIfDir )
{
	QString name;
	ListView *lve = listViewExt();

	if ( lve == NULL )
		return name;

	if ( lve->isTreeView() ) {
		if ( mIsDir || m_uiEntryInfo.isFile() )
			name = m_pListViewItem->fullName();
	}
	else // the ListView
		name = lve->m_sCurrentURL;

	name += text(0); // add the current item name

	if ( mIsDir && addSlashIfDir )
		if ( name != lve->mHost )
			name += '/';

	return name;
}


void ListViewItem::paintCell( QPainter *p, const QColorGroup &/*cg*/, int column, int width, int alignment )
{
	if ( ! p )
		return;

	m_pListView = listViewExt();
	m_pHeader   = m_pListView->header();

	if ( m_pListView->m_bEnableTwoColorsOfBg ) {
		QColor color = itemPos()%(height()*2) ? m_pListView->mSecondColorOfBg : m_pListView->backgroundColor();
		p->fillRect( 0, 0, width, height(), color );  // set background color
	}
	else
		p->fillRect( 0, 0, width, height(), m_pListView->backgroundColor() );  // draw background color

	if ( m_pListView->m_bShowGrid )  {
		//p->setPen( Qt::DotLine );
		p->setPen( m_pListView->mGridColor );
		p->drawLine( 0, height()-1,  width, height()-1 );
		p->drawLine( width-1, 0,  width-1, height()-1 );
	}

	int r = m_pListView ? m_pListView->itemMargin() : 1;
	int marg = m_pListView ? m_pListView->itemMargin() : 1;

	if ( m_pListView->showIcons() && column == ListView::NAMEcol )  {
		const QPixmap *icon = pixmap( 0 );
		if ( icon )  {
			p->drawPixmap( r, (height() - icon->height()) / 2, *icon );
			r += icon->width() + listView()->itemMargin();
		}
 	}
  else
		r += 2;

	mTxt = text( m_pListView->sectionMap(column) );
	if ( ! mTxt.isEmpty() ) {
		int Align, fontWeight;

		fontWeight = QFont::Light;
		if ( column == ListView::NAMEcol && isDir() )  fontWeight = QFont::Bold;

		if ( m_pListView->sectionMap(column) == ListView::SIZEcol )
			Align = (isDir()) ? Qt::AlignCenter : Qt::AlignRight | Qt::AlignVCenter;
		else
			Align = alignment | Qt::AlignVCenter;

		uint margin = 3;
		if ( m_pListView->sectionMap(column) == ListView::SIZEcol ) {
			if ( isDir() )  margin = 0;
			else
				m_pListView->formatNumberStr( mTxt );
		}
		int cfi = (column == ListView::NAMEcol) ? m_pListView->mIconWidth : 0;
		int w= m_pHeader->sectionSize( column ) - cfi - margin;

		if ( isSelected() )
			p->setPen( m_pListView->highlightedTextColor() );  // set color of selection
		else
			p->setPen( m_pListView->textColor() );

		p->setFont(  QFont(m_pListView->m_sFontFamily, m_pListView->mFontSize, fontWeight)  );
		p->drawText(  r, 1, width-marg-r, height(), Align, trimedString( mTxt, column, w, fontWeight )  );
	}
}


void ListViewItem::paintFocus( QPainter *p, const QColorGroup &, const QRect & r )
{
	m_pListView = listViewExt();
	m_pHeader   = m_pListView->header();
	bool isTreeView = m_pListView->isTreeView();

	int xT = (isTreeView) ? r.x()+1 : r.x();
	int wT = (isTreeView) ? m_pHeader->headerWidth()-xT : m_pHeader->headerWidth();

	if ( m_pListView->isFullCursor() )
		p->fillRect( r, m_pListView->cursorColor() );
	else  {  //  draws a two colors double frame
		p->setPen( m_pListView->frameOutCursorColor() );
		p->drawRect( xT, r.y(), wT, height() );
		p->setPen( m_pListView->frameInsCursorColor() );
		p->drawRect( xT+1, r.y() + 1, wT - 2, height() - 2 );
	}

	if ( isSelected() )
		p->setPen( m_pListView->highlightedTextColorUnderCursor() );
	else
		p->setPen( m_pListView->textColorUnderCursor() );

	int cfx, cfi; // correction for x pos., correction for icon (only first column)
	int fontWeight, w, clmn;
	unsigned int x, y = r.y() + 1, align, margin;
	unsigned int nMaxColumns = m_pHeader->count();

	// draw contents of all columns
	for (uint column=0; column<nMaxColumns; column++)
	{
		align = Qt::AlignLeft | Qt::AlignVCenter;  margin = 3;  cfx = 0;  cfi = 0;
		clmn = m_pListView->sectionMap( column );
		if (clmn < 0)
			continue;
		mTxt = text( clmn );

		if ( column == ListView::NAMEcol )  // draw text (and pixmap) for first column (name of file)
		{
			if ( m_pListView->showIcons() )  {
				const QPixmap *icon = pixmap( 0 );
				if ( icon )
					p->drawPixmap( (isTreeView) ? xT : 1-m_pHeader->offset(), y+1, *icon );
			}
			fontWeight = ( isDir() ) ? QFont::Bold : QFont::Light;
			cfx = (m_pListView->showIcons()) ? 1 : 0;
			cfi = m_pListView->mIconWidth;
		}
		else
			fontWeight = QFont::Light;

		if ( clmn == ListView::SIZEcol ) {
			if ( isDir() )  {
				align = Qt::AlignCenter;  margin = 0;  cfx = -1;
			}
			else {
				align = Qt::AlignRight | Qt::AlignVCenter;  cfx = 1;
				m_pListView->formatNumberStr( mTxt );
			}
		}

		if ( isTreeView && column == ListView::NAMEcol ) {
			x = xT + cfi + margin + cfx - 3;
			w = wT - cfi - margin;
		}
		else {
			x = m_pHeader->sectionPos( column ) - m_pHeader->offset() + cfi + margin;
			w = m_pHeader->sectionSize( column ) - cfi - margin;
		}
		int cw = m_pHeader->sectionSize( column ) - cfi - margin;

		p->setFont(  QFont( m_pListView->m_sFontFamily, m_pListView->mFontSize, fontWeight )  );
		p->drawText(  x-cfx, y,  w, r.height(),  align, trimedString(mTxt, column, cw, fontWeight)  );
	}
}


QString ListViewItem::trimedString( const QString &str, const int &column, const int &columnWidth, const int &fontWeight )
{
	bool truncate = false;
	QString txtStr = str, tmpTxt = "...";
	QFontMetrics fm( QFont( m_pListView->m_sFontFamily, m_pListView->mFontSize, fontWeight ) );

	int treeMargin = 0;
	if ( column == ListView::NAMEcol && m_pListView->isTreeView() )
		treeMargin = 20 * depth(); // default: m_pListView->treeStepSize() == 20

	if ( fm.width( txtStr )+treeMargin > columnWidth )  {
		truncate = true;
		int i = 0;
		while ( fm.width( tmpTxt + txtStr[ i ] )+treeMargin < columnWidth )
			tmpTxt += txtStr[ i++ ];

		tmpTxt.remove( 0, 3 );
		if ( tmpTxt.isEmpty() )  tmpTxt = txtStr.left( 1 );

		tmpTxt += "...";
	}
	if ( truncate )
		txtStr = tmpTxt;

	return txtStr;
}

// -------------- --------- ListViewToolTip --------- --------------

void ListViewToolTip::maybeTip( const QPoint & pos )
{
	if ( ! mView )
		return;

	Q3ListViewItem *item = mView->itemAt( pos );
	if ( ! item )
		return;

	Q3Header *pHeader = mView->header();

	int column = pHeader->sectionAt( pos.x()+pHeader->offset() );
	bool itemIsDir = ((ListViewItem *)item)->isDir();
	bool isNameColumn = (column == ListView::NAMEcol);
	int fontWeight = (itemIsDir) ? QFont::Bold : QFont::Light;
	int ic = (isNameColumn) ? mView->iconWidth() : 0;
	int xtc = (mView->isTreeView() && isNameColumn) ? (mView->treeStepSize() * (item->depth()-1)) + ic : 0;
	int xc = 3 + ic + xtc;
	int columnWidth = pHeader->sectionSize( column ) - xc;
	int xItemPos = pHeader->sectionPos( column ) - pHeader->offset() + xc;
	QRect r = QRect( xItemPos, mView->itemPos(item) - mView->contentsY(), columnWidth, item->height() );

	QString txt = item->text( column );

	if ( column == ListView::SIZEcol )
		if ( ! itemIsDir )
			mView->formatNumberStr( txt );

	int xtcw = 0;
	if ( mView->isTreeView() && isNameColumn )
		xtcw += 4;

	if ( r.contains( pos ) )  {
		QFontMetrics fm( QFont( mView->fontFamily(), mView->fontSize(), fontWeight ) );
		if ( fm.width( txt )+xtcw > columnWidth )
            QToolTip::showText(r.topLeft(), txt, mView, r);
			//tip( r, txt );
		else
		if ( column == ListView::NAMEcol )
			if ( ((ListViewItem *)item)->entryInfo().isSymLink() )
                QToolTip::showText(r.topLeft(), ((ListViewItem *)item)->entryInfo().readLink(), mView, r);
				//tip( r, ((ListViewItem *)item)->entryInfo().readLink() );
	}
}
