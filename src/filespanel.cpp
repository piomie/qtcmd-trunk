/***************************************************************************
                          filespanel.cpp  -  description
                             -------------------
    begin                : Sun Oct 27 2002
    copyright            : (C) 2002, 2003 by Piotr Mierzwiński
    email                : peterm@go2.pl

    Copyright            : See COPYING file that comes with this project

 $Id$
 ***************************************************************************/

//#include "urlslistmanager.h"
#include "urlsmanagerdlg.h"
#include "ftpconnectdlg.h"
#include "findcriterion.h"
#include "functions_col.h"
#include "qtcmdactions.h"
//Added by qt3to4:
#include <QResizeEvent>
#include <QPixmap>
#include <QKeyEvent>
#include <Q3PopupMenu>
#include "keyshortcuts.h"
#include "fileinfoext.h"
#include "devicesmenu.h"
#include "urlinfoext.h"
#include "messagebox.h"
#include "filespanel.h" // includes "qheader.h" too
#include "systeminfo.h" // for getting user and group
#include "statusbar.h"
#include "functions.h" // for few static functions
#include "pathview.h"
#include "filters.h"

#include "plugins/fp/settings/listviewsettings.h"
#include "plugins/fp/settings/otherappsettings.h"
#include "plugins/fp/settings/filesystemsettings.h"

#include "filter.xpm"

#include <qtoolbutton.h>
#include <qsettings.h>
#include <qregexp.h>
//#include <qtimer.h>

#define STATUSBAR_HEIGHT	21

static QString s_sTS;
static bool s_bSettingsHasRead;
ListViewSettings s_LVsettings;

FilesPanel::FilesPanel( const QString & sInitURL, QWidget *pParent, Panel::KindOfPanel eKindOfPanel, const char *sz_pName )
	: Panel(0,0, pParent, sz_pName)
{
//	sInitURL // odczytywac z pliku konf. zamiast w odczyt.w QtCmd i przekazywac tu

	//qDebug("FilesPanel::FilesPanel, name=%s, sInitURL=%s", sz_pName, sInitURL.toLatin1().data() );
	qDebug() << "FilesPanel::FilesPanel, name= " << sz_pName << " sInitURL= " << sInitURL;
	m_nWidth = pParent->width();
	m_pFilesAssociation = NULL;
	m_pKeyShortcuts = NULL;
	m_pFiltersMenu = NULL;
	m_pFSManager = NULL;
	m_pFilters = NULL;
	m_sInitUrl  = "";
	m_nFilterId = 0;
	m_eKindOfPanel = eKindOfPanel;
	setName(sz_pName);

	m_pListView     = new ListView( this );
	m_pPathView     = new PathView( m_pListView, this, QString(sz_pName)+"_VPW" );
	m_pStatusBar    = new StatusBar( this );
	m_pFilterButton = new QToolButton( this );
	m_pDevicesMenu  = new DevicesMenu( m_pListView );

	m_pStatusBar->resize(m_nWidth-3, STATUSBAR_HEIGHT); // it's needed by settings the infoStrList (in the StatusBar class)

	connect( m_pPathView, SIGNAL(signalPossibleDirWritable( VFS::DirWritable & )),
		this, SLOT(slotPossibleDirWritable( VFS::DirWritable & )) );
	connect( m_pPathView, SIGNAL(signalPathChanged(const QString &)),
		this, SLOT(slotPathChanged(const QString &)) );
	connect( m_pDevicesMenu, SIGNAL(signalPathChanged(const QString &)),
		this, SLOT(slotPathChanged(const QString &)) );

	// connect ListView's signals
	connect( (Q3ListView *)m_pListView->header(), SIGNAL(sizeChange(int, int, int)),
		this, SIGNAL(signalHeaderSizeChange(int, int, int)) );
	connect( m_pListView, SIGNAL(signalPathChanged(const QString &)),
		this, SLOT(slotPathChangedForTheViewPath(const QString &)) );
	connect( m_pListView, SIGNAL(signalRenameCurrentItem(int, const QString &, const QString &)),
		this, SLOT(slotRenameCurrentItem(int, const QString &, const QString &)) );
	connect( m_pListView, SIGNAL(returnPressed(Q3ListViewItem *)),
		this, SLOT(slotReturnPressed(Q3ListViewItem *)) );
	connect( m_pListView, SIGNAL(signalSetProgress(int)),
		m_pStatusBar, SLOT(slotSetProgress(int)) );
	connect( m_pListView, SIGNAL(signalUpdateOfListStatus(uint, uint, uint)),
		m_pStatusBar, SLOT(slotUpdateOfListStatus(uint , uint , uint)) );

	connect( m_pListView->popupMenu(), SIGNAL(signalDelete()),              this, SLOT(slotDelete()) );
	connect( m_pListView->popupMenu(), SIGNAL(signalMakeLink()),            this, SLOT(slotMakeLink()) );
	connect( m_pListView->popupMenu(), SIGNAL(signalCopy(bool , bool)),     this, SLOT(slotCopy(bool , bool)) );
	connect( m_pListView->popupMenu(), SIGNAL(signalShowProperties()),      this, SLOT(slotShowProperties()) );
	connect( m_pListView->popupMenu(), SIGNAL(signalOpenInNextPanel(bool)), this, SLOT(slotOpenInNextPanel(bool)) );


	initPanel(); // initialize: m_nKindOfListView
	initLookOfList(); // initialize: columns, FontFamily, FontSize, ShowIcons, colors of the listFiles and cursor, kind of group size of file
	initFiltersButton(); // initialize button contais filters menu

//	if (eKindOfPanel == FILES_PANEL)
//	{
		QString sInitUrl = ( sInitURL.isEmpty() ) ? m_pPathView->currentUrl() : sInitURL;
		if (eKindOfPanel == FILES_PANEL_FORFIND)
			sInitUrl.insert(0, "ffs://"); // FoundFiles (by FilesPanelForFind)
			//sInitUrl = "/"; // need to load LocalFS

		if ( ! sInitURL.isEmpty() ) {
		// originally an application can be executed from LOCALfs
			QString sInitUrl = sInitURL;

			if ( sInitUrl == "./" )
				sInitUrl = QDir::currentDirPath();
			else
			if ( sInitUrl.at(0) == '~' )
				sInitUrl = QDir::homeDirPath();
			else
			if (sInitUrl.at(0) != '/' && eKindOfPanel != FILES_PANEL_FORFIND)
				sInitUrl = QDir::currentDirPath()+"/"+sInitURL;
			if ( sInitUrl.at(sInitUrl.length()-1) != '/' )
				sInitUrl += "/";

			m_pPathView->slotPathChanged( sInitUrl ); // shows a path without waiting for listing directory
		}

		m_pFSManager = new FSManager( sInitUrl, m_pListView );

		if ((m_bFScreated=createFS( sInitUrl )))
		{
			qDebug(">> FS created <<");
			connect( m_pFSManager, SIGNAL(signalResultOperation(VFS::Operation, bool)),
				this, SLOT(slotResultOperation(VFS::Operation, bool)) );
			connect( m_pFSManager, SIGNAL(signalOpenArchive(const QString & )),
				this, SLOT(slotOpenArchive( const QString & )) );
			connect( m_pFSManager, SIGNAL(signalSetInfo( const QString & )),
				m_pStatusBar, SLOT(slotSetMessage( const QString & )) );
			connect( m_pFSManager, SIGNAL(signalPutFile(const QByteArray &, const QString &)),
				this, SIGNAL(signalPutFile(const QByteArray &, const QString &)) );
			connect( m_pListView, SIGNAL(signalPathChanged(const QString &)),
				m_pFSManager, SLOT(slotSetCurrentURL(const QString &)) );
			connect( m_pListView, SIGNAL(signalOpen(const UrlInfoExt &)),
				m_pFSManager, SLOT(slotOpen(const UrlInfoExt &)) );
			connect( m_pListView, SIGNAL(signalClose()),
				m_pFSManager, SLOT(slotCloseCurrentDir()) );
			connect( m_pFSManager, SIGNAL(signalChangePanel(Panel::KindOfPanel, const QString &, bool)),
				this, SIGNAL(signalChangePanel(Panel::KindOfPanel, const QString &, bool)) );

			if (eKindOfPanel == FILES_PANEL)
				QTimer::singleShot( 0, this, SLOT(slotInitPanelList()) );
		}
//	}
// 	else // FILES_PANEL_FORFIND
// 		m_bFScreated = TRUE;
}


FilesPanel::~FilesPanel() // tu zapisywac do pliku konf. wszystkie ustawienia listy: kolory, itp.
{
//	qDebug("FilesPanel - Destructor");
	s_bSettingsHasRead = FALSE; // or saved
	saveSettings();
	removeFS();

	delete m_pDevicesMenu;  m_pDevicesMenu = 0;
	delete m_pListView;     m_pListView = 0;
	delete m_pStatusBar;    m_pStatusBar = 0;
	delete m_pPathView;     m_pPathView = 0;
	delete m_pFilterButton;
	delete m_pFiltersMenu;
}


void FilesPanel::initPanel()
{
	s_sTS = name();  // eg. sz_pName == "LeftPVM_FP"
	QString panelSide = s_sTS.left( s_sTS.find('P') );
	QSettings *settings = new QSettings();

	// --- read kind of view (list/tree)
	m_nKindOfListView = settings->readNumEntry( "/qtcmd/"+panelSide+"FilesPanel/KindOfView", -1 );
	if ( m_nKindOfListView < 0 || m_nKindOfListView == UNKNOWN_PANELVIEW )
		m_nKindOfListView = LIST_PANELVIEW;

	// read it from config file (need to saved in Destruktor)
	m_pListView->setSorting( ListView::NAMEcol, TRUE ); // default let's sort increase by first column (name)

	delete settings;
}


void FilesPanel::initLookOfList()
{
	if ( ! s_bSettingsHasRead ) { // settings will be reading one time
		s_bSettingsHasRead = TRUE;
		QSettings *settings = new QSettings();
		const QStringList defaultColumnList = QStringList::split( ',', "0-170,1-70,2-107,3-70,4-55,5-55" );

		m_bFilterForFiltered = settings->readBoolEntry( "/qtcmd/FilesPanel/FilterForFiltered", FALSE );
		s_LVsettings.filterForFiltered = m_bFilterForFiltered;
		// --- reads columns info
		bool columnsSynchronize = settings->readBoolEntry( "/qtcmd/FilesPanel/SynchronizeColumns", TRUE );
		QStringList columnsList = settings->readListEntry( "/qtcmd/FilesPanel/Columns" );
		if ( columnsList.isEmpty() ) {
			columnsList = defaultColumnList;
			settings->writeEntry( "/qtcmd/FilesPanel/Columns", columnsList );
		}
		int columnWidth;
		for (int i=0; i<6; i++) { // 6 - number of all columns
			if ( columnsList[i].section('-', 0, 0 ) == QString("%1").arg(i) ) { // current column is visible
				columnWidth = (columnsList[i].section( '-', -1 )).toInt();
				if ( (columnWidth > 0 && columnWidth < 10) || columnWidth < 0 )
					columnWidth = (defaultColumnList[i].section('-', -1 )).toInt();
			}
			else
				columnWidth = 0; // don't show current column

			s_LVsettings.columnsWidthTab[i] = columnWidth;
		}
		s_LVsettings.columnsSynchronize = columnsSynchronize;

		// ----- reads fonts info
		QString font = settings->readEntry( "/qtcmd/FilesPanel/Font" );
		if ( font.isEmpty() )
			font = "Helvetica,10"; // default application fonts
		// parse 'font'
		s_LVsettings.fontFamily = font.left( font.find(',') );
		QString fontSizeStr = font.section( ',', 1,1 ).stripWhiteSpace();
		bool ok;
		int fontSize = fontSizeStr.toInt( &ok );
		if ( ! ok || (fontSize < 8 || fontSize > 24) )
			fontSize = 10; // default size
		s_LVsettings.fontSize = fontSize;

		// ----- reads misc info about the list view
		s_LVsettings.showIcons = settings->readBoolEntry( "/qtcmd/FilesPanel/ShowIcons", TRUE );
		s_LVsettings.cursorAlwaysVisible = settings->readBoolEntry( "/qtcmd/FilesPanel/NotHiddenCursor", TRUE );
		int	nFileSizeFormatSB = BKBMBGBformat;
//		int nFileSizeFormatSB = settings.readNumEntry( "/qtcmd/FilesPanel/FileSizeFormatStatusBar", BKBMBGBformat );
//		if ( nFileSizeFormatSB < NONEformat || nFileSizeFormatSB > BKBMBGBformat )
//			nFileSizeFormatSB = BKBMBGBformat;

		int	nFileSizeFormatFL = BKBMBGBformat;
//		int nFileSizeFormatFL = settings.readNumEntry( "/qtcmd/FilesPanel/FileSizeFormatFileList", BKBMBGBformat );
//		if ( nFileSizeFormatFL < NONEformat || nFileSizeFormatFL > BKBMBGBformat )
//			nFileSizeFormatFL = BKBMBGBformat;

		s_LVsettings.selectFilesOnly     = settings->readBoolEntry( "/qtcmd/FilesPanel/SelectFilesOnly", FALSE );
		s_LVsettings.showHiddenFiles     = settings->readBoolEntry( "/qtcmd/FilesPanel/ShowHiddenFiles", TRUE );
		s_LVsettings.fileSizeFormatSB    = nFileSizeFormatSB;
		s_LVsettings.fileSizeFormatFL    = nFileSizeFormatFL;

		// --- reads info about the colors
		QString colorsScheme = settings->readEntry( "/qtcmd/FilesPanel/ColorsScheme", "DefaultColors" );

		s_LVsettings.showSecondBackgroundColor = settings->readBoolEntry( "/qtcmd/"+colorsScheme+"/ShowSecondBgColor", TRUE );
		s_LVsettings.twoColorsCursor = settings->readBoolEntry( "/qtcmd/"+colorsScheme+"/TwoColorsCursor", TRUE );
		s_LVsettings.showGrid = settings->readBoolEntry( "/qtcmd/"+colorsScheme+"/ShowGrid", FALSE );

		QString colorStr;
		colorStr = settings->readEntry( "/qtcmd/"+colorsScheme+"/ItemColor", "000000" );
		s_LVsettings.itemColor = color(colorStr, "000000");
		colorStr = settings->readEntry( "/qtcmd/"+colorsScheme+"/ItemColorUnderCursor", "0000EA" );
		s_LVsettings.itemColorUnderCursor = color(colorStr, "0000EA");
		colorStr = settings->readEntry( "/qtcmd/"+colorsScheme+"/SelectedItemColor", "FF0000" );
		s_LVsettings.selectedItemColor = color(colorStr, "FF0000");
		colorStr = settings->readEntry( "/qtcmd/"+colorsScheme+"/SelectedItemColorUnderCursor", "57BA00" );
		s_LVsettings.selectedItemColorUnderCursor = color(colorStr, "57BA00");
		colorStr = settings->readEntry( "/qtcmd/"+colorsScheme+"/BgColorOfSelectedItems", "FFFFFF" );
		s_LVsettings.bgColorOfSelectedItems = color(colorStr, "FFFFFF");
		colorStr = settings->readEntry( "/qtcmd/"+colorsScheme+"/BgColorOfSelectedItemsUnderCursor", "FFFFFF" );
		s_LVsettings.bgColorOfSelectedItemsUnderCursor = color(colorStr, "FFFFFF");
		colorStr = settings->readEntry( "/qtcmd/"+colorsScheme+"/FirstBackgroundColor", "FFFFFF" );
		s_LVsettings.firstBackgroundColor = color(colorStr, "FFFFFF");
		colorStr = settings->readEntry( "/qtcmd/"+colorsScheme+"/SecondBackgroundColor", "FFEBDC" );
		s_LVsettings.secondBackgroundColor = color(colorStr, "FFEBD5");
		colorStr = settings->readEntry( "/qtcmd/"+colorsScheme+"/FullCursorColor", "00008B" );
		s_LVsettings.fullCursorColor = color(colorStr, "00008B");
		colorStr = settings->readEntry( "/qtcmd/"+colorsScheme+"/InsideFrameCursorColor", "000080" );
		s_LVsettings.insideFrameCursorColor = color(colorStr, "000080");
		colorStr = settings->readEntry( "/qtcmd/"+colorsScheme+"/OutsideFrameCursorColor", "FF0000" );
		s_LVsettings.outsideFrameCursorColor = color(colorStr, "FF0000");
		colorStr = settings->readEntry( "/qtcmd/"+colorsScheme+"/GridColor", "A9A9A9" );
		s_LVsettings.gridColor = color(colorStr, "A9A9A9");

		delete settings;
	}

	slotApplyListViewSettings( s_LVsettings );
}


void FilesPanel::initFiltersButton()
{
	m_pFiltersMenu = new MenuExt( this );
	connect( m_pFiltersMenu, SIGNAL(activated(int)), this, SLOT(slotChangeFilter(int)) );

	m_pFilterButton->setIconSet( QPixmap((const char**)filter) );
	m_pFilterButton->setPopupMode( QToolButton::InstantPopup );
	m_pFilterButton->setFocusPolicy( Qt::NoFocus ); // so Tab key cannot move focus here
	m_pFilterButton->setPopup( m_pFiltersMenu );
	m_pFilterButton->setMaximumWidth( 32 );
}

void FilesPanel::setFilters( Filters * pFilters )
{
	if ( m_pFiltersMenu == NULL || m_pKeyShortcuts == NULL || pFilters == NULL )
		return;

	m_pFilters = pFilters;

	m_pFiltersMenu->clear();
		// add items to the filters menu
	m_pFiltersMenu->insertItem( tr("&Configure filters")+" ("+m_pKeyShortcuts->keyStr(ShowFiltersManager_APP)+")", this, SLOT(slotShowFiltersManager()), 0, 1000 ); // obsolte
	//m_pFiltersMenu->addAction( tr("&Configure filters"), this, SLOT(slotShowFiltersManager()), m_pKeyShortcuts->key(ShowFiltersManager_APP) );
	m_pFiltersMenu->addSeparator();
	for ( uint i=0; i<pFilters->count(); i++ )
		//m_pFiltersMenu->insertItem( pFilters->name(i), pFilters->patterns(i), i ); // obsolte
		m_pFiltersMenu->addAction( pFilters->name(i), pFilters->patterns(i) ); // text,toolTip

	s_LVsettings.defaultFilter = pFilters->defaultId();
	slotChangeFilter( s_LVsettings.defaultFilter );
}


void FilesPanel::saveSettings()
{
	QSettings *settings = new QSettings();

	if ( ! s_bSettingsHasRead ) { // this setting will be saving one time
		s_bSettingsHasRead = TRUE;

		bool saveColumns = FALSE;
		int columnWidth;
		QStringList columnsList;
		for ( int i=0; i<6; i++ ) { // 6 - number of all columns
			columnWidth = s_LVsettings.columnsWidthTab[i];
			if ( m_pListView->columnSize(i) != columnWidth )
				saveColumns = TRUE;
			columnsList.append( QString("%1-%2").arg(i).arg(columnWidth) );
		}
		if ( saveColumns )
			settings->writeEntry( "/qtcmd/FilesPanel/Columns", columnsList );
	}
	s_sTS = name();  // np. name == "LeftPVM_FP"
	QString side = s_sTS.left( s_sTS.find('P') );

	settings->writeEntry( "/qtcmd/"+side+"FilesPanel/KindOfView", m_pListView->kindOfView() );

	delete settings;
}


void FilesPanel::updateView()
{
	m_pPathView->setGeometry(0, 0, width(), PATHVIEW_HEIGHT );
	m_pListView->setGeometry( 0, PATHVIEW_HEIGHT-2, width(), height()-(PATHVIEW_HEIGHT+STATUSBAR_HEIGHT) ); // PATHVIEW_HEIGHT+1+
	m_pStatusBar->setGeometry( 0, m_pListView->height()+PATHVIEW_HEIGHT-1, width()-m_pFilterButton->width(), STATUSBAR_HEIGHT );
	m_pFilterButton->setGeometry( m_pStatusBar->width(), m_pListView->height()+PATHVIEW_HEIGHT-1, m_pFilterButton->width(), STATUSBAR_HEIGHT );

	m_pPathView->showNormal();
	m_pListView->showNormal();
	m_pStatusBar->showNormal();
	m_pFilterButton->showNormal();

	showNormal();
}


bool FilesPanel::createFS( const QString & sURL )
{
	if (m_pFSManager == NULL)
		return FALSE;

	QString sInputURLs = sURL;
	VFS::KindOfFilesSystem kindOfFS = m_pFSManager->kindOfFilesSystem(sURL);

	// TODO ArchiveFS::archiveIsSupported przeniesc np. do FS
	if (FileInfoExt::isArchive(sURL)) { // check whether current archive is supported
		if (! FSManager::archiveIsSupported(sURL)) {
			MessageBox::critical(this,
				"'"+QFileInfo(sURL).extension(FALSE)+"'"+
				tr("This kind of archives are not supported !")
			);
			return FALSE;
		}
	}

	qDebug("FilesPanel::createFS(), try with '%s', m_sInitUrl=%s", sInputURLs.toLatin1().data(), m_sInitUrl.toLatin1().data() );

	if ( kindOfFS == VFS::FTPfs && ! m_sInitUrl.isEmpty() ) {
		if ( ! FtpConnectDlg::showDialog(sInputURLs) ) { // user canceled connection
// 			qDebug("__ connection canceled");
			m_pPathView->slotPathChanged( m_pFSManager->currentURL() ); // restores previous path
			return FALSE;
		}
	}

//	if ( m_pFSManager ) {
		m_pFSManager->parsePath( sInputURLs );
		if ( ! m_pFSManager->createVFS(kindOfFS, sInputURLs) )
			return FALSE;
//	}
//	else
//		return FALSE;

	// --- init some settings of listView
	if ( m_pFSManager->kindOfCurrentFS() != VFS::ARCHIVEfs && m_pListView->isTreeView() ) // NNN: m_pListView->isTreeView()
		m_pListView->clear(); // need to go out from non local FS, and shows tree for non local FS
	m_pListView->setHostName( m_pFSManager->host() );
	if ( m_pListView->kindOfView() != UNKNOWN_PANELVIEW )
		m_nKindOfListView = m_pListView->kindOfView();
	m_pListView->setKindOfView( (KindOfListView)m_nKindOfListView );

	m_sFinalPathOfTree = m_pFSManager->absHostURL(); // dla arch.jest tu tylko nazwa arch.
	if (m_sFinalPathOfTree.isEmpty())
		m_sFinalPathOfTree = sInputURLs;
	QString sNewURL = (m_pFSManager->kindOfCurrentFS() != VFS::ARCHIVEfs) ? m_sFinalPathOfTree : sInputURLs; // TYMCZASOWE
	UrlInfoExt ui;
	ui.setName(sNewURL); // set location too
	ui.setLocation(FileInfoExt::filePath(sNewURL));

	if ( m_sInitUrl.isEmpty() )
		m_sInitUrl = sInputURLs;
	else // used when user want go out from FS, or select an url in the path view
		m_pListView->open(ui); // sNewURL, m_sFinalPathOfTree

// 	if ( m_pFilters )
// 		slotChangeFilter( m_nFilterId );

	return TRUE;
}


void FilesPanel::removeFS()
{
	if (m_pFSManager == NULL)
		return;

	delete m_pFSManager;  m_pFSManager = NULL;
}


void FilesPanel::slotRereadCurrentDir()
{
	if ((! hasFocus() && ! m_pPathView->hasFocus()) || m_pFSManager == NULL) // change view only for an active panel
		return;

	if (m_eKindOfPanel == FILES_PANEL)
		m_pFSManager->rereadCurrentDir();
}


void FilesPanel::slotChangeKindOfView( uint nKindOfListView )
{
	if ((! hasFocus() && ! m_pPathView->hasFocus()) || m_pFSManager == NULL) // change view only for an active panel
		return;
	if (nKindOfListView == (uint)m_pListView->kindOfView() || m_eKindOfPanel != FILES_PANEL)
		return;

	m_pListView->setKindOfView( (KindOfListView)nKindOfListView );
	m_sFinalPathOfTree = m_pFSManager->currentURL();
	m_nKindOfListView = nKindOfListView;
	m_pListView->open(); // no param == current path
}


void FilesPanel::keyPressEvent( QKeyEvent *pKeyEvent )
{
	if (m_pKeyShortcuts == NULL || m_pFSManager == NULL) // no keyShortcuts loaded
		return;

	int key = pKeyEvent->key();
	int ks  = m_pKeyShortcuts->ke2ks( pKeyEvent );

	if ( ks == m_pKeyShortcuts->key(OpenCurrentFileOrDirectoryInLeftPanel_APP) )
		slotOpenInNextPanel( FALSE );
	else
	if ( ks == m_pKeyShortcuts->key(OpenCurrentFileOrDirectoryInRightPanel_APP) )
		slotOpenInNextPanel( FALSE );
	else
	if ( ks == m_pKeyShortcuts->key(ForceOpenCurrentDirectoryInLeftPanel_APP ) )
		slotOpenInNextPanel( TRUE );
	else
	if ( ks == m_pKeyShortcuts->key(ForceOpenCurrentDirectoryInRightPanel_APP) )
		slotOpenInNextPanel( TRUE );
	else
	if ( ks == m_pKeyShortcuts->key(GoToPreviousURL_APP) )
		slotPathChanged( m_pPathView->prevUrl() );
	else
	if ( ks == m_pKeyShortcuts->key(GoToNextURL_APP) )
		slotPathChanged( m_pPathView->nextUrl() );
	else
	if ( ks == m_pKeyShortcuts->key(ShowHistory_APP) )
		m_pPathView->showPopupList();
	else
	if ( ks == m_pKeyShortcuts->key(ShowHistoryMenu_APP) )
		m_pPathView->showButtonMenu();
	else
	if ( ks == m_pKeyShortcuts->key(ShowFilters_APP) )
		m_pFiltersMenu->popup( m_pListView->mapToGlobal(QPoint( m_pFilterButton->x(), m_pListView->height()-m_pFiltersMenu->height() )) );
	else
	if ( ks == m_pKeyShortcuts->key(ShowFiltersManager_APP) )
		slotShowFiltersManager();
	else
	if ( ks == m_pKeyShortcuts->key(ChangeViewToList_APP) )
		slotChangeKindOfView( (uint)LIST_PANELVIEW );
	else
	if ( ks == m_pKeyShortcuts->key(ChangeViewToTree_APP) )
		slotChangeKindOfView( (uint)TREE_PANELVIEW );
	else
	if ( ks == m_pKeyShortcuts->key(QuickFileSearch_APP) )
		m_pListView->showFindItemDialog();
	else
	if ( key == Qt::Key_Menu )
		m_pListView->showPopupMenu( FALSE );
	else
	if ( ks == m_pKeyShortcuts->key(ShowProperties_APP) )
		slotShowProperties();
	else
	if ( ks == m_pKeyShortcuts->key(MoveCursorToPathView_APP) || key == Qt::Key_Backtab )
		m_pPathView->setFocus();
	else
	if ( ks == m_pKeyShortcuts->key(ViewFile_APP) )
		slotReturnPressed( 0 ); // FIXME key F3 made wrong action: slotReturnPressed (in future this will made run program)
	else
	if ( ks == m_pKeyShortcuts->key(EditFile_APP) )
		openCurrentFile( TRUE ); // TRUE - open in edit mode
	else
	if ( ks == m_pKeyShortcuts->key(MakeAnEmptyFile_APP) )
		m_pFSManager->createNewEmptyFile(); // and open it in edit mode
	else
	if ( ks == m_pKeyShortcuts->key(CopyFiles_APP) )
		slotCopy( FALSE, FALSE );
	else
	if ( ks == m_pKeyShortcuts->key(CopyFilesToCurrentDirectory_APP) )
		slotCopy( TRUE, FALSE );
	else
	if ( ks == m_pKeyShortcuts->key(MoveFiles_APP) )
		slotCopy( FALSE, TRUE );
	else
	if ( ks == m_pKeyShortcuts->key(QuickFileRename_APP) )
		slotCopy( TRUE, TRUE );
	else
	if ( ks == m_pKeyShortcuts->key(MakeDirectory_APP) )
		createNewEmptyFile( FALSE );
	else
	if ( ks == m_pKeyShortcuts->key(DeleteFiles_APP) || key == Qt::Key_Delete )
		slotDelete();
	else
	if ( ks == m_pKeyShortcuts->key(MakeALink_APP) || key == Qt::Key_F9 )
		slotMakeLink();
	else
	if ( ks == m_pKeyShortcuts->key(EditCurrentLink_APP) )
		slotEditLink();
	else
	if ( ks == m_pKeyShortcuts->key(WeighCurrentDirectory_APP) ) {
		if ( m_pFSManager->kindOfCurrentFS() == VFS::LOCALfs ) { // weighing in other than LocalFS is not yet supported
			if (! m_pListView->currentItemIsDir()) // file
				m_pListView->setSelected(m_pListView->currentItem(), TRUE);
			else { // dir
				if (m_pListView->isSelected(m_pListView->currentItem())) { // deselect directory
					m_pListView->setSelected(m_pListView->currentItem(), FALSE);
					m_pListView->setTextInColumn(ListView::SIZEcol, "<DIR>");
				}
				else {// directory is'nt selected, let's weigh it
					m_pFSManager->weighFiles(QStringList() << m_pListView->pathForCurrentItem(), FALSE); // weighing without round to FS block size
                }
			}
		}
	}
	else
	if ( ks == m_pKeyShortcuts->key(GoToUpLevel_APP) )
		m_pFSManager->slotCloseCurrentDir();
	else
	if ( ks == m_pKeyShortcuts->key(GoToHomeDirectory_APP) )
		slotPathChanged( QDir::homeDirPath()+"/" );
	else
	if ( ks == m_pKeyShortcuts->key(RefreshDirectory_APP) )
		slotRereadCurrentDir();
	else
	if ( key == Qt::Key_Insert ) { // mark current item
		m_pListView->slotSelectCurrentItem();
		m_pCurrentItem = m_pListView->currentItem();
		m_pListView->ensureItemVisible(m_pCurrentItem->itemBelow());
		if (m_pListView->isTreeView())
			m_pPathView->slotPathChanged(m_pListView->pathForCurrentItem(TRUE, TRUE));
	}
	else
	//if ( ks == m_pKeyShortcuts->key(InvertSelection_APP) )
	if ( key == Qt::Key_Asterisk )
		m_pListView->slotInvertSelections();
	else
	if ( ks == m_pKeyShortcuts->key(SelectFilesWithThisSameExtention_APP) )
		m_pListView->slotSelectWithThisSameExtension();
	else
	if ( ks == m_pKeyShortcuts->key(DeselectFilesWithThisSameExtention_APP) )
		m_pListView->slotSelectWithThisSameExtension( TRUE );
	else
	if ( ks == m_pKeyShortcuts->key(SelectAllFiles_APP) )
		m_pListView->slotSelectAllItems();
	else
	if ( ks == m_pKeyShortcuts->key(DeselectAllFiles_APP) )
		m_pListView->slotSelectAllItems( TRUE );
	else
	if ( ks == m_pKeyShortcuts->key(SelectGroupOfFiles_APP) )
		m_pListView->slotSelectGroupOfItems();
	else
	if ( ks == m_pKeyShortcuts->key(DeselectGroupOfFiles_APP) )
		m_pListView->slotSelectGroupOfItems( TRUE );
	else
	if ( ks == m_pKeyShortcuts->key(QuickChangeOfFilePermission_APP) )
		m_pListView->slotShowEditBox( ListView::PERMcol );
	else
	if ( ks == m_pKeyShortcuts->key(QuickChangeOfFileModifiedDate_APP) )
		m_pListView->slotShowEditBox( ListView::TIMEcol );
	else
	if ( ks == m_pKeyShortcuts->key(QuickChangeOfFileOwner_APP) )
		m_pListView->slotShowEditBox( ListView::OWNERcol );
	else
	if ( ks == m_pKeyShortcuts->key(QuickChangeOfFileGroup_APP) )
		m_pListView->slotShowEditBox( ListView::GROUPcol );
}


void FilesPanel::copyFiles( bool bMove )
{
	if (m_pFSManager == NULL)
		return;

	// --- getting selected files
	ListView::SelectedItemsList *pItemsList = NULL;
	emit signalGetSILfromPanelBeside(pItemsList);

	qDebug() << "FilesPanel::copyFiles, pItemsList->count()= " << pItemsList->count();

	if (pItemsList != NULL)
		m_pFSManager->copyFilesTo(m_pFSManager->currentURL(), pItemsList, bMove);
}

// ---------- SLOTs ------------

void FilesPanel::slotReturnPressed( Q3ListViewItem * )
{
	m_sFinalPathOfTree = m_pListView->pathForCurrentItem();
	qDebug() << "FilesPanel::slotReturnPressed(). New path= " << m_sFinalPathOfTree;
	UrlInfoExt ui;
	ui = ((ListViewItem *)m_pListView->currentItem())->entryInfo();
	ui.setLocation(m_sFinalPathOfTree);
	m_pListView->open(ui, TRUE); // TRUE - closes if open (a branch)
	//m_pListView->open((m_sFinalPathOfTree=m_pListView->pathForCurrentItem()), TRUE); // TRUE - closes if open (a branch)
}


void FilesPanel::slotOpenArchive( const QString & fileName )
{
	if (! FileInfoExt::isArchive(fileName) || m_pFSManager == NULL)
		return;

	if ( ! ((ListViewItem *)m_pListView->currentItem())->entryInfo().isReadable() ) {
		MessageBox::critical( this,
			fileName+"\n\n"+tr("Cannot open this archive")+".\n"+tr("Permission denied.")
		);
		return;
	}
	createFS( m_pFSManager->currentURL()+FileInfoExt::fileName(fileName) );
}


void FilesPanel::slotOpenInNextPanel( bool bDirForce )
{
	if (m_pFSManager == NULL)
		return;

	QString sFileName;

	if ( ! bDirForce ) {
		sFileName = m_pFSManager->currentURL() + m_pListView->currentItemText();
		if ( m_pListView->currentItemIsDir() )
			sFileName += "/";
		else {
			if ( m_pFilesAssociation )
				if ( m_pFilesAssociation->loaded() ) {
					if ( m_pFilesAssociation->kindOfViewer(sFileName) == EXTERNALviewer ) {
						MessageBox::critical( this, tr("Cannot to embedded an external viewer !") );
						return;
					}
				}
		}
	}
	else // open current directory
		sFileName = m_pFSManager->currentURL();

	emit signalChangePanel(FILES_PANEL, sFileName, FALSE); // FALSE - next panel
}


void FilesPanel::slotRenameCurrentItem( int nColumn, const QString & sOldText, const QString & sNewText )
{
	if (! sNewText.isEmpty() && sOldText != sNewText && ! m_pFSManager)
		m_pFSManager->changeAttribut( (ListView::ColumnName)nColumn, sNewText );
}


void FilesPanel::slotDelete()
{
	if (m_pFSManager != NULL)
		m_pFSManager->removeFiles();
}


void FilesPanel::slotMakeLink()
{
	if (m_pFSManager != NULL)
		m_pFSManager->makeLink( m_sPanelBesideURL, FALSE );
}


void FilesPanel::slotEditLink()
{
	if (m_pFSManager != NULL)
		m_pFSManager->makeLink( m_sPanelBesideURL, TRUE );
}


void FilesPanel::slotCopy( bool bShiftPressed, bool bMove )
{
	if (m_pFSManager == NULL)
		return;

	if (bShiftPressed && bMove) { // show rename edit box on current item position
		m_pListView->slotShowEditBox( ListView::NAMEcol );
		return;
	}

	// collect file names, if not selected then adds current file
	QStringList slNamesList;
	m_pListView->collectAllSelectedNames( slNamesList ); // collect abs. files name

	emit signalCopyFiles( bMove ); // calls copyFiles() for the second panel
	/*
	VFS::KindOfFilesSystem eFsInPanelBeside;
	emit signalGetKindOfFSInPanelBeside( eFsInPanelBeside );

	QStringList slNamesList;
	m_pListView->collectAllSelectedNames( slNamesList ); // collect abs. files name

	if ( eFsInPanelBeside != VFS::LOCALfs )
		emit signalCopyFiles( slNamesList, bMove ); // calls copyFiles() for the second panel
	else {
		QString sTmpPanelBesideURL = m_sPanelBesideURL;
		if ( bShiftPressed && ! bMove )  // try copy file to current directory
			m_sPanelBesideURL = m_pFSManager->currentURL();
		if ( ! m_pFSManager->copyFilesTo(m_sPanelBesideURL, slNamesList, bMove) )
			m_sPanelBesideURL = sTmpPanelBesideURL;
	}
	//qDebug("==back to FilesPanel::slotCopy(), from m_pFSManager->copyFilesTo");
	*/
}


void FilesPanel::slotShowProperties()
{
	m_pFSManager->showPropertiesDialog();
}


void FilesPanel::slotPathChanged( const QString & sInputURLs )
{
	if (m_pFSManager == NULL || m_eKindOfPanel != FILES_PANEL)
		return;

	qDebug("FilesPanel::slotPathChanged, try to '%s'", sInputURLs.toLatin1().data() );

	m_sFinalPathOfTree = sInputURLs;
	bool bNeedToChange = m_pFSManager->needToChangeFS( m_sFinalPathOfTree );

	if ( m_sFinalPathOfTree.isEmpty() )
		m_sFinalPathOfTree = m_pPathView->prevUrl(TRUE); // get previous path from LocalFS

	if ( bNeedToChange )
		createFS( m_sFinalPathOfTree );
}


void FilesPanel::slotResultOperation( VFS::Operation eOperation, bool bNoError )
{
	qDebug("FilesPanel::slotResultOperation(), operation= %s, errorStat=%d, file=%s", FSManager::cmdString(eOperation).toLatin1().data(), !bNoError, m_pFSManager->nameOfProcessedFile().toLatin1().data() );
	ListView::SelectedItemsList *selectedItemsList = NULL;
	QString processedFile = m_pFSManager->nameOfProcessedFile();
	QString currentURL    = m_pFSManager->currentURL();
	UrlInfoExt urlInfo;
	VFS::Error errorCode = m_pFSManager->errorCode();
	bool moveWithRemoveError = (eOperation == VFS::Move && errorCode == VFS::CannotRemove);
	bool copyOrMoveWithSetAttrError = ((eOperation == VFS::Copy || eOperation == VFS::Move) && errorCode == VFS::CannotSetAttributs);
	bool weighWithError = (eOperation == VFS::Weigh && errorCode == VFS::CannotRead);

	if ( bNoError || (eOperation == VFS::Copy && errorCode == VFS::CannotSetAttributs) )
		m_pFSManager->removeProgressDlg( TRUE );

	if ( bNoError || copyOrMoveWithSetAttrError || moveWithRemoveError || weighWithError )  {
		if ( eOperation == VFS::Copy || eOperation == VFS::Move || eOperation == VFS::MakeLink ) {
			QString path;
			VFS::KindOfFilesSystem FSofProcessedFile = m_pFSManager->kindOfFilesSystem(FileInfoExt::filePath(processedFile));
			if ( m_pFSManager->kindOfCurrentFS() == VFS::LOCALfs )
				path = (eOperation == VFS::Move) ? currentURL : ((m_sPanelBesideURL != currentURL) ? m_sPanelBesideURL : QString(""));
			else
				path = (FSofProcessedFile != VFS::LOCALfs) ? currentURL : m_sPanelBesideURL;

			if ( path == FileInfoExt::filePath(processedFile) || currentURL != FileInfoExt::filePath(processedFile) ) {
				if ( FSofProcessedFile == VFS::LOCALfs && m_pFSManager->kindOfCurrentFS() != VFS::LOCALfs ) {
					// copy/move to FTP
					emit signalGetSILfromPanelBeside( selectedItemsList );
					if ( selectedItemsList->count() == 0 )
						return;
					m_pListView->updateList( ListView::INSERTitems, selectedItemsList );
					if ( eOperation == VFS::Move && bNoError )
						emit signalUpdateSecondPanel( ListView::REMOVEitems, selectedItemsList, urlInfo );
					else
						emit signalUpdateSecondPanel( ListView::DESELECTitems, selectedItemsList, urlInfo );
				}
				else { // copy/move inside LOCALfs or copy/move to LOCALfs
					selectedItemsList = m_pListView->selectedItemsList();
					if ( selectedItemsList->count() == 0 ) {
						if ( eOperation != VFS::MakeLink ) // need to update selected list when occures skip the copying files
							m_pListView->countAndWeightSelectedFiles( TRUE );
						return;
					}
					if ( eOperation == VFS::MakeLink && selectedItemsList->count() == 1 )
						urlInfo.setName( m_pFSManager->currentURL()+FileInfoExt::fileName(processedFile) );
					if ( m_pFSManager->targetURL() == m_sPanelBesideURL )
						emit signalUpdateSecondPanel( (eOperation==VFS::MakeLink) ? ListView::INSERTitemsAsLinks : ListView::INSERTitems, selectedItemsList, urlInfo );
					if ( eOperation == VFS::Move && bNoError )
						m_pListView->updateList( ListView::REMOVEitems );
					else
						m_pListView->updateList( ListView::DESELECTitems );
				}
				if ( eOperation != VFS::MakeLink ) // need to update selected list when occures skip the copying files
					m_pListView->countAndWeightSelectedFiles( TRUE );
			}
			else { // Copy/Move/MakeLink to this same directory
				if ( m_pListView->numOfSelectedItems() == 0 ) {
					m_pFSManager->getNewUrlInfo( urlInfo );
					// need to change user and group for all items on the selectedList, before update it
					if ( eOperation != VFS::Move && m_pFSManager->kindOfFilesSystem(processedFile) == VFS::LOCALfs ) {
                        urlInfo.setOwner(SystemInfo::getCurrentUserName());
                        urlInfo.setGroup(SystemInfo::getCurrentGroupName());
					}
					m_pListView->clearSelectedItemsList();
					if ( ! urlInfo.name().isEmpty() )
						m_pListView->updateList( ListView::INSERTitems, 0, urlInfo );
				}
			}
		} // Copy/Move/MakeLink
		else
		if ( eOperation == VFS::SetAttributs || eOperation == VFS::Rename ) {
			m_pFSManager->getNewUrlInfo( urlInfo );
			m_pListView->updateList( ListView::REMOVEitems | ListView::INSERTitems, 0, urlInfo ); // remove and insert item - urlInfo
		}
		else
		if ( eOperation == VFS::Remove ) {
			if ( currentURL == FileInfoExt::filePath(processedFile) )
				m_pListView->updateList( ListView::REMOVEitems );
		}
		else
		if ( eOperation == VFS::Put || eOperation == VFS::MakeDir || eOperation == VFS::Touch ) {
			m_pFSManager->getNewUrlInfo( urlInfo );
			if ( FileInfoExt::filePath(processedFile) == currentURL )
				m_pListView->updateList( ListView::INSERTitems, 0, urlInfo );
			else
			if ( FileInfoExt::filePath(processedFile) == m_sPanelBesideURL )
				emit signalUpdateSecondPanel( ListView::INSERTitems, 0, urlInfo );
		}
		else
		if ( eOperation == VFS::Weigh ) {
			if ( ! m_pFSManager->setWeighInPropetiesDlg() ) {
				long long totalWeigh;
				uint totalFiles, totalDirs;
				m_pFSManager->getWeighingStat( totalWeigh, totalFiles, totalDirs );
		// class QString in Qt-3.1.x not supports long long type !
				//QString sizeStr = QString("%1").arg((double)totalWeigh, 0, 'f'); // for large numbers
				//sizeStr = sizeStr.left( sizeStr.find('.') ); // get everything to a dot
				QString sizeStr = QString::number( totalWeigh );
				qDebug("__ Weigh=%s (%lld B), files=%d, dirs=%d", sizeStr.toLatin1().data(), totalWeigh, totalFiles, totalDirs );
				m_pListView->setCurrentItem( m_pListView->findName(processedFile) );
				m_pListView->setTextInColumn( ListView::SIZEcol, sizeStr ); // in currentItem
				m_pListView->setSelected( m_pListView->currentItem(), TRUE );
			}
		}
		else
		if ( eOperation == VFS::List ) {
			processedFile = m_pFSManager->currentURL(); // get path from VFS
			m_pStatusBar->clear( TRUE ); // hides progressBar
			m_pListView->setCurrentURL( processedFile );
			m_pPathView->slotPathChanged( processedFile );
			m_pFSManager->slotSetCurrentURL( processedFile );
			m_pStatusBar->slotUpdateOfListStatus( m_pListView->numOfSelectedItems(), m_pListView->numOfAllItems(), m_pListView->weightOfSelected() );
			emit signalSetPanelBesideURL( processedFile );
// 			qDebug( "__ sel=%d, all=%d, w-all=%d", m_pListView->numOfSelectedItems(), m_pListView->childCount()-1, m_pListView->weightOfSelected() );

			if ( m_pListView->isListView() ) {
				if ( ! m_pFSManager->previousDirName().isEmpty() )
					m_pListView->moveCursorToName( m_pFSManager->previousDirName() );
			}
			else { // TreeView
				Q3ListViewItem *currentItem = m_pListView->currentItem();
				if ( currentItem ) {
					if ( currentItem->firstChild() ) {
						m_pListView->moveCursorToName( currentItem->firstChild()->text(0) );
						// need to open next dir of the tree
						QString nextDirForTree = m_sFinalPathOfTree;
						if ( nextDirForTree != processedFile ) {
							nextDirForTree = nextDirForTree.left( nextDirForTree.find('/', processedFile.length()+1) ); // TYMCZASOWE
							//m_pListView->open( nextDirForTree );
							UrlInfoExt ui;
							ui.setName(nextDirForTree); // set location too
							m_pListView->open(ui);
						}
					}
				}
			}
		}
		else
		if ( eOperation == VFS::Connect )
			m_pPathView->slotPathChanged( m_pFSManager->absHostURL() );
		else
		if ( eOperation == VFS::Find )
			m_pFSManager->slotStartFind( FindCriterion() );
		else
		if ( eOperation == VFS::Open )
			m_pFSManager->createFileView();

		// --- synchronization with second panel
		if ( eOperation == VFS::Copy || eOperation == VFS::Move || eOperation == VFS::MakeLink || eOperation == VFS::MakeDir || eOperation == VFS::Put || eOperation == VFS::Touch || eOperation == VFS::Remove ) {
			// 'Copy' and 'MakeLink' only for eOperation in the current panel
			selectedItemsList = m_pListView->selectedItemsList();
			if ( eOperation != VFS::Remove )
				m_pListView->ensureNameVisible( processedFile );
			// whether in the panel beside is opened dir. with path from current panel
			if ( m_sPanelBesideURL == currentURL ) {
				if ( eOperation == VFS::Remove || eOperation == VFS::Move )
					emit signalUpdateSecondPanel( ListView::REMOVEitems, selectedItemsList, urlInfo );
				else
					emit signalUpdateSecondPanel( ListView::INSERTitems, selectedItemsList, urlInfo );
			}
			if ( eOperation != VFS::Copy && eOperation != VFS::Move ) // NNN
				selectedItemsList->clear();
		}
		else
		if ( eOperation == VFS::SetAttributs || eOperation == VFS::Rename || eOperation == VFS::Open ) {
			selectedItemsList = m_pListView->selectedItemsList();
			if ( m_sPanelBesideURL == currentURL && eOperation != VFS::Open )
				emit signalUpdateSecondPanel( ListView::REMOVEitems | ListView::INSERTitems, selectedItemsList, urlInfo );

			if ( eOperation != VFS::Open || (eOperation == VFS::Open && ! m_pListView->numOfSelectedItems() && selectedItemsList->count() == 1) )
				selectedItemsList->clear();

			if ( eOperation == VFS::Rename )
				m_pListView->ensureNameVisible( m_pFSManager->newAttributName() );
		}

		return;
	} // bNoError

	// ----- eOperation finished with an error
//	Error errorCode = m_pFSManager->errorCode();
	qDebug("__ERROR: code=%d, host=%s, path=%s, currentURL()=%s, nameOfProcessedFile=%s",
		errorCode, m_pFSManager->host().toLatin1().data(), m_pFSManager->path().toLatin1().data(), m_pFSManager->currentURL().toLatin1().data(), processedFile.toLatin1().data() );

	if ( eOperation == VFS::Open || eOperation == VFS::SetAttributs || eOperation == VFS::Rename ) {
		if ( errorCode == VFS::NotExists )
			m_pListView->removeItem( processedFile );
	}
	else
	if ( eOperation == VFS::Cd || eOperation == VFS::List ) {
		if ( processedFile.isEmpty() )
			return;
		if ( errorCode == VFS::NotExists || errorCode == VFS::CannotRead ) {
			QString newPath = FileInfoExt::filePath(processedFile);
			qDebug("__(modify) nameToRemove=%s, pathToRemove=%s, newPath=%s, kindOfCurrentFS=%d", FileInfoExt::fileName(processedFile).toLatin1().data(), processedFile.toLatin1().data(), newPath.toLatin1().data(), m_pFSManager->kindOfCurrentFS() );
			if ( m_pFSManager->kindOfCurrentFS() == VFS::LOCALfs ) {
				m_pPathView->slotRemovePath( processedFile ); // attempts to remove a path from the history list
				m_pPathView->slotPathChanged( newPath );
			}
			if ( errorCode == VFS::NotExists ) {
				if ( currentURL == newPath )
					m_pListView->removeItem( processedFile );
				else {
					//m_pListView->open( newPath );
					UrlInfoExt ui;
					ui.setName(newPath); // set location too
					m_pListView->open(ui);
				}
				m_pListView->countAndWeightSelectedFiles();
				m_pStatusBar->slotUpdateOfListStatus( m_pListView->numOfSelectedItems(), m_pListView->numOfAllItems(), m_pListView->weightOfSelected() );
			}
		}
	} // Cd
	else
	if ( eOperation == VFS::Copy || eOperation == VFS::Move || eOperation == VFS::Remove ) {
		if ( errorCode == VFS::NotExists ) {
			if ( currentURL == FileInfoExt::filePath(processedFile) )
				m_pListView->removeItem( processedFile );
		}
		if ( eOperation != VFS::Remove ) // need to update selected list when occures skip the copying files
			m_pListView->countAndWeightSelectedFiles( TRUE ); // NNN
	}
	else
	if ( eOperation == VFS::Find )
		m_pFSManager->slotStartFind( FindCriterion() ); // restores dlg.usable
	else
	if ( eOperation == VFS::Connect ) { // failed connections - create list with previous local fs directory
		createFS( m_pPathView->prevUrl(TRUE) );
		m_pListView->moveCursorToName( processedFile );
	}
	else
	if ( eOperation == VFS::HostLookup ) // HostLookup have breaked by HostLookup
		createFS( m_pFSManager->absHostURL() );

	if ( eOperation == VFS::Copy || eOperation == VFS::Move || eOperation == VFS::Remove || eOperation == VFS::Open || eOperation == VFS::SetAttributs || eOperation == VFS::Rename )
		if ( m_sPanelBesideURL == currentURL && errorCode != VFS::BreakOperation )
			emit signalUpdateSecondPanel( ListView::REMOVEitems, m_pListView->selectedItemsList(), urlInfo );

	if ( eOperation != VFS::Copy && eOperation != VFS::Move )
		m_pListView->clearSelectedItemsList();
}


void FilesPanel::slotInitPanelList()
{
	if (! m_pListView->isVisible() || m_eKindOfPanel != FILES_PANEL)
		return;

	if ( m_pFSManager ) {
		if ( m_pFSManager->kindOfFilesSystem( m_sInitUrl ) == VFS::FTPfs ) {
			if ( ! FtpConnectDlg::showDialog(m_sInitUrl) ) {
				// User canceled connection
				// m_sInitUrl need to init, becouse it is checked into createFS()
				//qDebug("__ connection canceled");
				m_sInitUrl = m_pPathView->prevUrl(TRUE);
				createFS( m_sInitUrl ); // create list with previous localFS path
				return;
			}
		}
		UrlInfoExt ui;
		ui.setName(m_sInitUrl); // location sets too
		m_pListView->open(ui);
		//m_pListView->open( m_sInitUrl );
		// after start a focus must be in the left panel
		if ( QString(name()).find("Left") != -1 )
			setFocus();
	}
}


void FilesPanel::slotShowFtpManagerDlg()
{
	if (! hasFocus() && ! m_pPathView->hasFocus()) // signal has been sended to both panels
		return;

	QString sNewURL;
	if ( UrlsManagerDlg::showDialog(sNewURL, UrlsManagerDlg::FTP, m_pFSManager->path()) ) { // init connection
		m_sInitUrl = ""; // need to skip 'FtpConnectDlg' by 'createFS' fun.
		if (createFS( sNewURL )) {
			UrlInfoExt ui;
			ui.setName(FileInfoExt::fileName(sNewURL)); // set location too
			m_pListView->open(ui);
			//m_pListView->open( sNewURL/*m_pFSManager->absHostURL()*/ );
		}
	}
}


void FilesPanel::slotShowDevicesMenu()
{
	if (! hasFocus() && ! m_pPathView->hasFocus()) // signal has been sended to both panels
		return;

	m_pDevicesMenu->exec( QPoint(3,3) );
}


void FilesPanel::slotShowSimpleDiscStat()
{
	if (! hasFocus() && ! m_pPathView->hasFocus()) // signal has been sended to both panels
		return;

	if (m_pFSManager->kindOfCurrentFS() != VFS::LOCALfs || m_eKindOfPanel != FILES_PANEL)
		return;

	long long freeBytes, allBytes;
	SystemInfo::getStatFS( m_pFSManager->currentURL(), freeBytes, allBytes ); // TODO in SystemInfo::getStatFS

	QString freeBytesStr  = formatNumber( freeBytes, BKBMBGBformat );
	QString totalBytesStr = formatNumber( allBytes, BKBMBGBformat );

	Q3PopupMenu *statDiskMenu = new Q3PopupMenu();
	statDiskMenu->setFocusPolicy( Qt::NoFocus );
//	statDiskMenu->insertTearOffHandle();
	statDiskMenu->insertItem( tr("Free bytes on current disk")+": "+freeBytesStr );
	statDiskMenu->insertItem( tr("Total bytes on current disk")+": "+totalBytesStr );
//	statDiskMenu->insertItem( tr("More information..."), this, SLOT(slotShowSimpleAllDiscsStat()), 0, 3 );
	statDiskMenu->insertItem( tr("More information ..."), 3 );
	statDiskMenu->setItemEnabled( 3, FALSE );
	statDiskMenu->exec( m_pListView->mapToGlobal(QPoint(2,2)) );

	delete statDiskMenu;
}
/*
	void slotShowSimpleAllDiscsStat();
void FilesPanel::slotShowSimpleAllDiscsStat()
{
	QStringList discsList;
	SystemInfo::getAllowDisc( discsList );
}
*/


void FilesPanel::slotShowFavorites()
{
	if ( ! hasFocus() && ! m_pPathView->hasFocus() ) // signal has been sended to both panels
		return;

	QString url;
	uint i = 0;
	bool empty = FALSE;
	QSettings settings;
	QStringList slFavorites;
	MenuExt *pFavoritesMenu = new MenuExt;

	while( ! empty ) {
		url = settings.readEntry( QString("/qtcmd/Favorites/Url%1").arg(i) );
		if ( (empty=url.isEmpty()) )
			continue;

		slFavorites.append( url.section("\\", 1,1) );
		//pFavoritesMenu->insertItem( url.section( "\\", 0,0 ), slFavorites[i], i+101 ); // obsolte
		pFavoritesMenu->addAction( url.section( "\\", 0,0 ), slFavorites[i] ); // text,toolTip
		i++;
	}
	pFavoritesMenu->insertSeparator();
	pFavoritesMenu->insertItem( tr("&Configure favorites"), this, SLOT(slotShowFavoriteManager()) );
	pFavoritesMenu->insertItem( tr("&Add current URL"), this, SLOT(slotAddCurrentToFavorites()) );
	pFavoritesMenu->exec( m_pListView->mapToGlobal(QPoint(2,2)) ); // returns QAction *
	//int urlId = pFavoritesMenu->exec( m_pListView->mapToGlobal(QPoint(2,2)) );

	//if ( urlId > 100 )
	//	slotPathChanged( slFavorites[urlId-101] );

	delete pFavoritesMenu;
}

void FilesPanel::slotShowFavoriteManager()
{
	if ( ! hasFocus() && ! m_pPathView->hasFocus() ) // signal has been sended to both panels
		return;

	QString sNewURL;
	if ( UrlsManagerDlg::showDialog(sNewURL, UrlsManagerDlg::FAVORITES, m_pFSManager->path()) ) { // let's go to URL
		m_sInitUrl = ""; // need to skip 'FtpConnectDlg' into 'createFS' fun.
		createFS( sNewURL );
		if ( m_pFSManager ) {
			UrlInfoExt ui;
			ui.setName(m_pFSManager->absHostURL());
			ui.setLocation(FileInfoExt::filePath(m_pFSManager->absHostURL()));
			m_pListView->open(ui);
			//m_pListView->open( m_pFSManager->absHostURL() );
		}
	}
}


void FilesPanel::slotAddCurrentToFavorites()
{
	if (m_eKindOfPanel != FILES_PANEL)
		return;

	QString path = m_pFSManager->currentURL();
	QString newFavorite = path.section('/', path.count('/')-1)+"\\"+path;
	QSettings settings;
	QStringList fl = settings.entryList( "/qtcmd/Favorites/" ); // get all favorites
	settings.writeEntry( QString("/qtcmd/Favorites/Url%1").arg(fl.count()), newFavorite );
}


void FilesPanel::slotShowFiltersManager()
{
	QString newPattern;
	if ( UrlsManagerDlg::showDialog(newPattern, UrlsManagerDlg::FILTERS, QString::null) ) // let's apply a new filter
		m_pListView->applyFilter( newPattern, Filters::AllFiles );

	m_pFilters->updateFilters();
	setFilters( m_pFilters ); // need to do for both panels
}


void FilesPanel::slotQuickGetOutFromFS()
{
	if (! hasFocus() && ! m_pPathView->hasFocus()) // signal has been sended to both panels
		return;

	if ( kindOfCurrentFS() != VFS::LOCALfs ) {
		UrlInfoExt ui;
		ui.setName("../");
		ui.setLocation(m_pFSManager->host());
		ui.setDir(TRUE);
		m_pFSManager->slotOpen(ui);
		//m_pFSManager->slotOpen( m_pFSManager->host()+"../" );
	}
}


void FilesPanel::slotShowFindFileDlg()
{
	if ((! hasFocus() && ! m_pPathView->hasFocus()) || m_eKindOfPanel != FILES_PANEL) // signal has been sended to both panels
		return;

	m_pFSManager->showFindFileDialog();
}


void FilesPanel::slotPossibleDirWritable( VFS::DirWritable & dw )
{
	if ( m_pFSManager )
		dw = m_pFSManager->currentDirWritable();
}


void FilesPanel::slotPathChangedForTheViewPath( const QString & sPath )
{
	m_pFSManager->slotSetCurrentURL( sPath );
	m_pPathView->slotPathChanged( sPath );
}


void FilesPanel::slotChangeFilter( int nFilterId )
{
	if ( ! m_pFilters || nFilterId == 1000 )
		return;

	m_nFilterId = nFilterId;
	m_pFilterButton->setTextLabel( m_pFiltersMenu->text(nFilterId)+"\n("+m_pFilters->patterns(nFilterId)+")" );

	Filters::DirFilter dirFilterTab[5] = {
	 Filters::AllFiles, Filters::DirsOnly, Filters::FilesOnly, Filters::SymLinks, Filters::HangSymLinks
	};
	Filters::DirFilter dirFilter = (nFilterId > (int)m_pFilters->baseFiltersNum()-1) ? Filters::AllFiles : dirFilterTab[nFilterId];

	m_pListView->applyFilter( m_pFilters->patterns(nFilterId), dirFilter, m_bFilterForFiltered );
	// TODO m_pListView->applyFilter(), need to synchronization with next panel
}


void FilesPanel::slotApplyListViewSettings( const ListViewSettings & lvs )
{
	// --- sets columns
	for (int i=0; i<6; i++) // 6 - number of all columns
		m_pListView->header()->removeLabel( 0 );
	for (int i=0; i<6; i++) // 6 - number of all columns
		m_pListView->setColumn( (ListView::ColumnName)i, lvs.columnsWidthTab[i] );
	m_pListView->setShowColumns( TRUE );

	// --- sets fonts and other list settings
	m_pListView->setFontList( lvs.fontFamily, lvs.fontSize );
	m_pListView->setShowIcons( lvs.showIcons );
	m_pListView->setKindOfGroupFileSize( (KindOfFormating)lvs.fileSizeFormatFL );
	m_pListView->setFocusAlwaysVisible( lvs.cursorAlwaysVisible );
	m_pListView->setSelectFilesOnly( lvs.selectFilesOnly );

	// --- sets colors
	m_pListView->setDblFrameCursor( lvs.twoColorsCursor );
	m_pListView->setFullCursor( ! lvs.twoColorsCursor );
	if ( lvs.twoColorsCursor )
		m_pListView->setDblFrameCursorColors( lvs.insideFrameCursorColor, lvs.outsideFrameCursorColor );
	else
		m_pListView->setCursorColor( lvs.fullCursorColor );

	m_pListView->setTextColor( lvs.itemColor );
	m_pListView->setTextColorUnderCursor( lvs.itemColorUnderCursor ); // QColor(0,0,234)
	m_pListView->setHighlightedTextColor( lvs.selectedItemColor );
	m_pListView->setHighlightTextColorUnderCursor( lvs.selectedItemColorUnderCursor ); // QColor(87,186,0)
	m_pListView->setHighlightBgColor( lvs.bgColorOfSelectedItems );
	m_pListView->setBackgroundColor( lvs.firstBackgroundColor );
	m_pListView->setBackgroundMode(Qt::PaletteBackground);
	m_pListView->setPaletteBackgroundColor( lvs.firstBackgroundColor );

	m_pListView->setShowGrid( lvs.showGrid );
	m_pListView->setGridColor( lvs.gridColor );

	m_pListView->setEnableTwoColorsOfBg( lvs.showSecondBackgroundColor );
	m_pListView->setSecondColorOfBg( lvs.secondBackgroundColor );// QColor(255,235,220) 255,246,230;  204,255,212 - blady pomaranczowy, (blady zielony)
}


void FilesPanel::slotApplyOtherAppSettings( const OtherAppSettings & /*oas*/ )
{
	// sets shortcuts and toolBar
}


void FilesPanel::slotApplyFileSystemSettings( const FileSystemSettings & fss )
{
	VFS::KindOfFilesSystem currentFS = m_pFSManager->kindOfCurrentFS();

	if ( currentFS == VFS::ARCHIVEfs ) {
		m_pFSManager->setWorkDirectoryPath( fss.workDirectoryPath );
	}
	else
	if ( currentFS == VFS::LOCALfs ) {
		m_pFSManager->setRemoveToTrash( fss.removeToTrash );
		m_pFSManager->setWeighBeforeOperation( fss.weighBeforeOperation );
		m_pFSManager->setTrashPath( fss.trashPath );
	}
	else
	if ( currentFS == VFS::FTPfs ) {
		m_pFSManager->setStandByConnection( fss.standByConnection );
		m_pFSManager->setNumOfTimeToRetryIfServerBusy( fss.numOfTimeToRetryIfFtpBusy );
	}
	// settings for all
	m_pFSManager->setAlwaysOverwrite( fss.alwaysOverwrite );
	m_pFSManager->setAlwaysSavePermission( fss.alwaysSavePermission );
	m_pFSManager->setAlwaysAskBeforeDeleting( fss.alwaysAskBeforeDeleting );
	m_pFSManager->setAlwaysGetInToDirectory( fss.alwaysGetInToDirectory );
	m_pFSManager->setCloseProgressDlgAfterFinished( fss.closeProgressDlgAfterFinished );
}

// ---------- EVENTs ----------

void FilesPanel::resizeEvent( QResizeEvent * )
{
	m_pPathView->setGeometry(0, 0, width(), PATHVIEW_HEIGHT );
	m_pListView->setGeometry( 0, PATHVIEW_HEIGHT-2, width(), height()-(PATHVIEW_HEIGHT+STATUSBAR_HEIGHT) ); // PATHVIEW_HEIGHT+1+
	m_pStatusBar->setGeometry( 0, m_pListView->height()+PATHVIEW_HEIGHT-1, width()-m_pFilterButton->width(), STATUSBAR_HEIGHT );
	m_pFilterButton->setGeometry( m_pStatusBar->width(), m_pListView->height()+PATHVIEW_HEIGHT-1, m_pFilterButton->width(), STATUSBAR_HEIGHT );
}

