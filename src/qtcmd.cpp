/***************************************************************************
                         qtcmd.cpp  -  description
                         -------------------
   begin                : nie lut 20 2005
   copyright            : (C) 2005 by Piotr Mierzwinski
   email                : peterm@o2.pl

   copyright            : See COPYING file that comes with this project

$Id$
***************************************************************************/

#define VERSION "0.8 alfa"
#define AUTHOR "Piotr Mierzwi�ski"
#define AUTHORMAIL "peterm@go2.pl"
#define AUTHOR2 "Mariusz Borowski (since ver. 0.8a)"
#define AUTHORMAIL2 "b0mar@nes.pl"
// ----------------------------------

#include "qtcmd.h"
#include "panel.h"
#include "helpdlg.h"
#include "fileview.h"
#include "filespanel.h"
#include "filespanelforfind.h"

#include "filters.h"
#include "listview.h"
//#include "buttonsbar.h"
#include "keyshortcuts.h"
#include "filesassociation.h"

#include "messagebox.h"
// #include "fileinfoext.h"
#include "qtcmdactions.h"

#include "qtcmd.xpm"
#include "storage.xpm"
#include "settings.xpm"
#include "favorites.xpm"
#include "mainview_icons.h"
#include "toolbarapp_icons.h"

#include "config.h"
#include "aboutdlg.h"
#include "pluginmanager.h"


QtCmd::QtCmd(const QString &sLeftURL, const QString &sRightURL, QWidget* pParent, const char *sz_pName, Qt::WFlags f)
	:  QMainWindow(pParent, sz_pName, f),
		m_eKindOfLeftPanel(Panel::UNKNOWN_PANEL), m_eKindOfRightPanel(Panel::UNKNOWN_PANEL),
		m_pLeftPanel(0), m_pRightPanel(0), m_pFilters(0), m_pKeyShortcuts(0), m_pKeyShortcutsForView(0),
		m_pFilesAssociation(0), m_pSettingsAppWidget(0), m_pSettingsWidget(0), m_eCurrentPanel(NO_PANEL)
{
	setIcon( qtcmd );
	initSettings(); // reads and initialize: width and height of main window and size of panels
	initKeyShortcutsForView();
	initBars();
	initPanels(sLeftURL, (! sLeftURL.isEmpty() ? sRightURL : QString("")));
}


QtCmd::~QtCmd()
{
	Config *conf = Config::instance();
	conf->setMainWindowWidth(width());
	conf->setMainWindowHeight(height());
	conf->setFlatButtonBar(m_bFlatButtonBar);
	conf->setQuitWarnEnabled(m_bShowQuitWarn);

	if ( m_pSettingsWidget ) {
		delete m_pSettingsWidget; m_pSettingsWidget = 0;
	}

	delete m_pFilesAssociation;
}

void QtCmd::initSettings()
{
	Config *conf = Config::instance();
	int width  = conf->mainWindowWidth();
	int height = conf->mainWindowHeight();
	resize(width, height);

	// ----- reads info whether to show or not menu and tool bar
	m_bFlatButtonBar    = conf->isFlatButtonBar();
	m_bShowQuitWarn     = conf->isQuitWarnEnabled();

		// ----- reads the panels size
	m_nWidthOfLeftPanel = conf->leftPanelWidth();
	if (m_nWidthOfLeftPanel == 0 || m_nWidthOfLeftPanel >= width)
		m_nWidthOfLeftPanel = width / 2;
	m_nWidthOfRightPanel = width - m_nWidthOfLeftPanel;
	// --- read kind of panel
	Panel::KindOfPanel eKindOfPanel, eDefaultKoPanel = Panel::FILES_PANEL;
	eKindOfPanel        = (Panel::KindOfPanel) conf->kindOfLeftPanel();
	m_eKindOfLeftPanel  = (eKindOfPanel != Panel::FILES_PANEL && eKindOfPanel != Panel::FILEVIEW_PANEL) ? eDefaultKoPanel : eKindOfPanel;
	eKindOfPanel        = (Panel::KindOfPanel) conf->kindOfRightPanel();
	m_eKindOfRightPanel = (eKindOfPanel != Panel::FILES_PANEL && eKindOfPanel != Panel::FILEVIEW_PANEL) ? eDefaultKoPanel : eKindOfPanel;

	// --- init library obj.
//PLUGIN    m_sPluginsPath = conf->pluginsDirPath() + "/"; // FIXME remove + '/'
}

void QtCmd::initBars()
{
	// --- Create menubar
	// --- FILE MENU
	QMenu *pFileMenu = menuBar()->addMenu(tr("&File"));
	pFileMenu->addAction(QPixmap(fileopen), tr("Open &file"),      this, SLOT(slotOpenFile()));
	pFileMenu->addAction(QPixmap(fileopen), tr("Open &directory"), this, SLOT(slotOpenDirectory()), Qt::CTRL+Qt::Key_O);
	pFileMenu->addAction(QPixmap(fileopen), tr("Open &archive"),   this, SLOT(slotOpenArchive()),   Qt::CTRL+Qt::Key_A);
	pFileMenu->addSeparator();
	m_pQuitAppsA = pFileMenu->addAction(tr("Quit an application"), this, SLOT(slotQuit()));

	// --- VIEW MENU
	// - kind of list view
	m_pKindOfVFPmenu = new QMenu(tr("Change kind of list view"));
	QAction *pListAct = m_pKindOfVFPmenu->addAction(tr("List View"));
	pListAct->setCheckable(TRUE);
	QAction *pTreeAct = m_pKindOfVFPmenu->addAction(tr("Tree View"));
	pTreeAct->setCheckable(TRUE);

	m_pLVactGroup = new QActionGroup( this );
	m_pLVactGroup->setExclusive(TRUE);
	m_pLVactGroup->addAction(pListAct);
	m_pLVactGroup->addAction(pTreeAct);
	connect(m_pLVactGroup, SIGNAL(triggered( QAction * )), this, SLOT(slotChangeListView( QAction * )));

	// - proportion of panels
	m_pProporActGroup = new QActionGroup( this );
	m_pProporActGroup->setExclusive( TRUE );
	connect(m_pProporActGroup, SIGNAL(triggered( QAction * )), this, SLOT(slotChangeProportionOfPanels( QAction * )));

	m_pProportionsOfPanelsMenu = new QMenu(tr("Proportions of panels"));
	QAction *p5050Act = m_pProporActGroup->addAction(tr("50/50 %"));
	p5050Act->setCheckable(TRUE);
	QAction *p3070Act = m_pProporActGroup->addAction(tr("30/70 %"));
	p3070Act->setCheckable(TRUE);
	QAction *p7030Act = m_pProporActGroup->addAction(tr("70/30 %"));
	p7030Act->setCheckable(TRUE);
	QAction *pUserDefAct = m_pProporActGroup->addAction(tr("User defined"));
	pUserDefAct->setCheckable(TRUE);
	pUserDefAct->setToolTip(
		tr( "width of left panel" )  + " = " + QString::number( m_nWidthOfLeftPanel )+"\n"+
		tr( "width of right panel" ) + " = " + QString::number( m_nWidthOfRightPanel )
	);
	m_pProportionsOfPanelsMenu->addAction(p5050Act);
	m_pProportionsOfPanelsMenu->addAction(p3070Act);
	m_pProportionsOfPanelsMenu->addAction(p7030Act);
	m_pProportionsOfPanelsMenu->addAction(pUserDefAct);

	// - view menu
	QMenu *pViewMenu = menuBar()->addMenu(tr("&View"));  // m_pViewMenu
	m_pShowHideMenuBarA = pViewMenu->addAction(tr("Show/Hide &menu bar"), this, SLOT(slotShowHideMenuBar()));
	m_pShowHideMenuBarA->setCheckable(TRUE);
	m_pShowHideToolBarA = pViewMenu->addAction(tr("Show/Hide &tool bar"), this, SLOT(slotShowHideToolBar()));
	m_pShowHideToolBarA->setCheckable(TRUE);
	m_pShowHideBtnBarA  = pViewMenu->addAction(tr("Show/Hide &button bar"), this, SLOT(slotShowHideButtonBar()));
	m_pShowHideBtnBarA->setCheckable(TRUE);

	pViewMenu->addAction(m_pShowHideMenuBarA);
	pViewMenu->addAction(m_pShowHideToolBarA);
	pViewMenu->addAction(m_pShowHideBtnBarA);
	pViewMenu->addSeparator();
	pViewMenu->addMenu(m_pKindOfVFPmenu);
	pViewMenu->addMenu(m_pProportionsOfPanelsMenu);

	// --- SETTINGS MENU
	QMenu *pSettingsMenu = menuBar()->addMenu(tr("&Settings"));
	m_pConfigureA = pSettingsMenu->addAction(QPixmap(settings), tr("&Configure") + " QtCommander", this, SLOT(slotShowAppSettings()));
	m_pConfViewA  = pSettingsMenu->addAction(QPixmap(settings), tr("Configure file &view"), this, SLOT(slotShowViewSettings()));

	// --- TOOLS MENU
	QMenu *pToolsMenu = menuBar()->addMenu(tr("&Tools"));
	m_pFtpMmngA     = pToolsMenu->addAction(QPixmap(ftptool),     tr("FTP connection manager"), this, SIGNAL(signalShowFtpManagerDlg()));
	m_pFreeSpaceA   = pToolsMenu->addAction(QPixmap(freespace),   tr("Show free space on current disk"), this, SIGNAL(signalShowFreeSpace()));
	m_pStorageA     = pToolsMenu->addAction(QPixmap(storage),     tr("Show storage devices list"), this, SIGNAL(signalDevicesList()));
	m_pRereadA      = pToolsMenu->addAction(QPixmap(reread),      tr("Reread current directory"), this, SIGNAL(signalRereadCurrentDir()));
	m_pFavoritesA   = pToolsMenu->addAction(QPixmap(favorites),   tr("Show favorites"), this, SIGNAL(signalShowFavorites()));
	m_pFindFilesA   = pToolsMenu->addAction(QPixmap(findfile),    tr("Fin&d file"), this, SIGNAL(signalFindFile()));
	m_pQuickGetOutA = pToolsMenu->addAction(QPixmap(quickgetout), tr("Quick get out from the file system"), this, SIGNAL(signalQuickGetOutFromFS()));

	// --- HELP MENU
	QMenu *pHelpMenu = menuBar()->addMenu(tr("&Help"));
	pHelpMenu->addAction(tr("&About"),    this, SLOT(slotShowAbout()));
	pHelpMenu->addAction(tr("About &Qt"), this, SLOT(slotShowAboutQt()));
	pHelpMenu->addAction(tr("&Help"),     this, SLOT(slotShowHelp()), Qt::Key_F1);

	// --- Create toolbar
	m_pToolBar = addToolBar(tr("Tools"));

	m_pChangeKindOfViewToolBtn = new QToolButton(m_pToolBar);
	//m_pChangeKindOfViewToolBtn->setPopupMode( QToolButton::InstantPopup );
	m_pChangeKindOfViewToolBtn->setPopupMode(QToolButton::MenuButtonPopup); // show popup handle
	m_pChangeKindOfViewToolBtn->setPopup(m_pKindOfVFPmenu);
	m_pChangeKindOfViewToolBtn->setIcon(QPixmap(treeview));

	m_pProportionsBtn = new QToolButton(m_pToolBar);
	//m_pProportionsBtn->setPopupMode( QToolButton::InstantPopup );
	m_pProportionsBtn->setPopupMode(QToolButton::MenuButtonPopup); // show popup handle
	m_pProportionsBtn->setIcon(QPixmap(proportions));
	m_pProportionsBtn->setPopup(m_pProportionsOfPanelsMenu);

	m_pToolBar->addWidget(m_pChangeKindOfViewToolBtn);
	m_pToolBar->addAction(m_pFtpMmngA);
	m_pToolBar->addAction(m_pRereadA);
	m_pToolBar->addAction(m_pFindFilesA);
	m_pToolBar->addWidget(m_pProportionsBtn);
	m_pToolBar->addAction(m_pFreeSpaceA);
	m_pToolBar->addAction(m_pStorageA);
	m_pToolBar->addAction(m_pFavoritesA);
	m_pToolBar->addAction(m_pQuickGetOutA);
	m_pToolBar->addAction(m_pConfigureA);

	initButtonsBar();

	// -- init bars and menu action settings
	bool bMenuBarVisible, bToolBarVisible, bButtonBarVisible;
	Config *conf = Config::instance();
	(bMenuBarVisible   = conf->isMenuBarVisible())   ? menuBar()->show()    : menuBar()->hide();
	(bToolBarVisible   = conf->isToolBarVisible())   ? m_pToolBar->show()   : m_pToolBar->hide();
	(bButtonBarVisible = conf->isButtonBarVisible()) ? m_pButtonBar->show() : m_pButtonBar->hide();
	if (m_pShowHideMenuBarA != NULL)
		m_pShowHideMenuBarA->setChecked(bMenuBarVisible);
	if (m_pShowHideToolBarA != NULL)
		m_pShowHideToolBarA->setChecked(bToolBarVisible);
	if (m_pShowHideBtnBarA != NULL)
		m_pShowHideBtnBarA->setChecked(bButtonBarVisible);

	// TODO init proportion and kind of view
}

void QtCmd::initButtonsBar()
{
	m_pButtonBar = new QToolBar( this );
	addToolBar(Qt::BottomToolBarArea, m_pButtonBar);

	QStringList slButtonsLabel;
	slButtonsLabel << "F1 "+tr("Help") << "F2 "+tr("Change view") << "F3 "+tr("View") << "F4 "+tr("Edit") << "F5 "+tr("Copy");
	slButtonsLabel << "F6 "+tr("Move") << "F7 "+tr("Make dir") << "F8 "+tr("Delete") << "F9 "+tr("Make a link") << "F10 "+tr("Quit");

	const char *buttonSlotTab[] = {
		SLOT(slotShowHelp()),   // F1
		SLOT(slotChangeView()), // F2
		SLOT(slotFileView()),   // F3
		SLOT(slotFileEdit()),   // F4
		SLOT(slotCopy()),       // F5
		SLOT(slotMove()),       // F6
		SLOT(slotMakeDir()),    // F7
		SLOT(slotDelete()),     // F8
		SLOT(slotMakeLink()),   // F9
		SLOT(slotQuit())        // F10
	};
	QPushButton *pBtn;
	QFont boldFont;   boldFont.setWeight( QFont::Bold );
	const uint nMaxButtons = slButtonsLabel.count();

	for ( uint i=0; i<nMaxButtons; i++ ) {
		pBtn = new QPushButton( slButtonsLabel.at(i), this );
		pBtn->setFocusPolicy( Qt::NoFocus );
		pBtn->setFlat( m_bFlatButtonBar );
		pBtn->setFont( boldFont );
		if ( i == nMaxButtons-1 ) // F10
			pBtn->setAccel( Qt::Key_F10 );
		connect( pBtn, SIGNAL(clicked()), this, buttonSlotTab[i] );

		m_pButtonBar->addWidget(pBtn);
		if ( i != nMaxButtons-1 ) // put separator next to button except last button
			m_pButtonBar->addSeparator();
		m_btpButtonBar[i] = pBtn;
	}
}

void QtCmd::initKeyShortcuts()
{
	m_pKeyShortcuts = new KeyShortcuts;

	m_pKeyShortcuts->append( Help_APP, tr("Help"), Qt::Key_F1 );
	m_pKeyShortcuts->append( ViewFile_APP, tr("View file"), Qt::Key_F3 );
	m_pKeyShortcuts->append( EditFile_APP, tr("Edit file"), Qt::Key_F4 );
	m_pKeyShortcuts->append( CopyFiles_APP, tr("Copy files"), Qt::Key_F5 );
	m_pKeyShortcuts->append( MoveFiles_APP, tr("Move files"), Qt::Key_F6 );
	m_pKeyShortcuts->append( MakeDirectory_APP, tr("Make directory"), Qt::Key_F7 );
	m_pKeyShortcuts->append( DeleteFiles_APP, tr("Delete files"), Qt::Key_F8 );
	m_pKeyShortcuts->append( MakeAnEmptyFile_APP, tr("Make an empty file"), Qt::SHIFT+Qt::Key_F4 );
	m_pKeyShortcuts->append( CopyFilesToCurrentDirectory_APP, tr("Copy files to current directory"), Qt::SHIFT+Qt::Key_F5 );
	m_pKeyShortcuts->append( QuickFileRename_APP, tr("Quick file rename"), Qt::SHIFT+Qt::Key_F6 );
	m_pKeyShortcuts->append( QuickChangeOfFileModifiedDate_APP, tr("Quick change of file modified date"), Qt::SHIFT+Qt::Key_F7 );
	m_pKeyShortcuts->append( QuickChangeOfFilePermission_APP, tr("Quick change of file permission"), Qt::SHIFT+Qt::Key_F8 );
	m_pKeyShortcuts->append( QuickChangeOfFileOwner_APP, tr("Quick change of file owner"), Qt::SHIFT+Qt::Key_F9 );
	m_pKeyShortcuts->append( QuickChangeOfFileGroup_APP, tr("Quick change of file group"), Qt::SHIFT+Qt::Key_F10 );
	m_pKeyShortcuts->append( GoToUpLevel_APP, tr("Go to up level"),Qt::Key_Backspace );
	m_pKeyShortcuts->append( RefreshDirectory_APP, tr("Refresh directory"), Qt::CTRL+Qt::Key_R );
	m_pKeyShortcuts->append( MakeALink_APP, tr("Make a link"), Qt::CTRL+Qt::Key_L );
	m_pKeyShortcuts->append( EditCurrentLink_APP, tr("Edit current link"), Qt::CTRL+Qt::SHIFT+Qt::Key_L );
//	m_pKeyShortcuts->append( InvertSelection_APP, tr("Invert current selection"), Qt::Key_Asterisk );
	m_pKeyShortcuts->append( SelectFilesWithThisSameExtention_APP, tr("Select files with this same extention"), Qt::ALT+Qt::Key_Plus );
	m_pKeyShortcuts->append( DeselectFilesWithThisSameExtention_APP, tr("Deselect files with this same extention"), Qt::ALT+Qt::Key_Minus );
	m_pKeyShortcuts->append( SelectAllFiles_APP, tr("Select all files"), Qt::CTRL+Qt::Key_Plus );
	m_pKeyShortcuts->append( DeselectAllFiles_APP, tr("Deselect all files"), Qt::CTRL+Qt::Key_Minus );
	m_pKeyShortcuts->append( SelectGroupOfFiles_APP, tr("Select group of files"), Qt::Key_Plus );
	m_pKeyShortcuts->append( DeselectGroupOfFiles_APP, tr("Deselect group of files"), Qt::Key_Minus );
	m_pKeyShortcuts->append( ChangeViewToList_APP, tr("Change view to list"),Qt::ALT+Qt::CTRL+Qt::Key_I );
	m_pKeyShortcuts->append( ChangeViewToTree_APP, tr("Change view to tree"), Qt::ALT+Qt::CTRL+Qt::Key_T );
	m_pKeyShortcuts->append( QuickFileSearch_APP, tr("Quick file search"), Qt::CTRL+Qt::Key_S );
	m_pKeyShortcuts->append( ShowHistoryMenu_APP, tr("Show history's menu"), Qt::CTRL+Qt::Key_H );
	m_pKeyShortcuts->append( ShowHistory_APP, tr("Show history"), Qt::ALT+Qt::Key_Down );
	m_pKeyShortcuts->append( ShowFilters_APP, tr("Show pFilters menu"), Qt::ALT+Qt::Key_Up );
	m_pKeyShortcuts->append( GoToPreviousURL_APP, tr("Go to previous URL"), Qt::CTRL+Qt::Key_Up );
	m_pKeyShortcuts->append( GoToNextURL_APP, tr("Go to next URL"), Qt::CTRL+Qt::Key_Down );
	m_pKeyShortcuts->append( GoToHomeDirectory_APP, tr("Go to home directory"), Qt::CTRL+Qt::Key_Home );
	m_pKeyShortcuts->append( MoveCursorToPathView_APP, tr("Move cursor to path view"), Qt::ALT+Qt::CTRL+Qt::Key_P );
	m_pKeyShortcuts->append( ShowFtpManager_APP, tr("Show FTP manager"), Qt::ALT+Qt::CTRL+Qt::Key_C );
	m_pKeyShortcuts->append( ShowFiltersManager_APP, tr("Show pFilters manager"), Qt::ALT+Qt::CTRL+Qt::Key_F );
	m_pKeyShortcuts->append( ShowStorageDevicesList_APP, tr("Show storage devices list"), Qt::CTRL+Qt::Key_D );
	m_pKeyShortcuts->append( ShowFreeSpace_APP, tr("Show free space"), Qt::CTRL+Qt::Key_F );
	m_pKeyShortcuts->append( ShowFavorites_APP, tr("Show favorites"), Qt::CTRL+Qt::Key_Backslash );
	m_pKeyShortcuts->append( FindFile_APP, tr("Find file"), Qt::ALT+Qt::Key_F7 );
	m_pKeyShortcuts->append( QuickGoOutFromCurrentFileSystem_APP, tr("Quick go out from current file system"), Qt::CTRL+Qt::Key_Backspace );
	m_pKeyShortcuts->append( OpenCurrentFileOrDirectoryInLeftPanel_APP, tr("Open current file or directory in left panel"), Qt::CTRL+Qt::Key_Left );
	m_pKeyShortcuts->append( OpenCurrentFileOrDirectoryInRightPanel_APP, tr("Open current file or directory in right panel"), Qt::CTRL+Qt::Key_Right );
	m_pKeyShortcuts->append( ForceOpenCurrentDirectoryInLeftPanel_APP, tr("Force open current directory in left panel"), Qt::ALT+Qt::CTRL+Qt::Key_Left );
	m_pKeyShortcuts->append( ForceOpenCurrentDirectoryInRightPanel_APP, tr("Force open current directory in right panel"), Qt::ALT+Qt::CTRL+Qt::Key_Right );
	m_pKeyShortcuts->append( WeighCurrentDirectory_APP, tr("Weigh current directory"), Qt::Key_Space );
	m_pKeyShortcuts->append( ShowSettingsOfApplication_APP, tr("Show settings of application"), Qt::ALT+Qt::CTRL+Qt::Key_S );
	m_pKeyShortcuts->append( ShowSettingsOfView_APP, tr("Show settings of view"), Qt::CTRL+Qt::SHIFT+Qt::Key_S );
	m_pKeyShortcuts->append( ShowHideMenuBar_APP, tr("Show/hide menu bar"), Qt::CTRL+Qt::Key_M );
	m_pKeyShortcuts->append( ShowHideToolBar_APP, tr("Show/hide tool bar"), Qt::CTRL+Qt::Key_T );
	m_pKeyShortcuts->append( ShowHideButtonBar_APP, tr("Show/hide buttons bar"), Qt::CTRL+Qt::Key_B );
	m_pKeyShortcuts->append( ShowProperties_APP, tr("Show file properties dialog"), Qt::ALT+Qt::Key_Return );
	m_pKeyShortcuts->append( QuitApps_APP, tr("Quit an application"), Qt::CTRL+Qt::Key_Q );

	m_pKeyShortcuts->updateEntryList( "/qtcmd/FilesPanelShortcuts/" ); // update from config file
	slotApplyKeyShortcuts( m_pKeyShortcuts ); // for actions in current window
}

void QtCmd::initKeyShortcutsForView()
{
	m_pKeyShortcutsForView = new KeyShortcuts();

	m_pKeyShortcutsForView->append( Help_VIEW, tr("Help"), Qt::Key_F1 );
	m_pKeyShortcutsForView->append( Reload_VIEW, tr("Reload"), Qt::Key_F5 );
	m_pKeyShortcutsForView->append( Open_VIEW, tr("Open"), Qt::CTRL+Qt::Key_O );
	m_pKeyShortcutsForView->append( Save_VIEW, tr("Save"), Qt::CTRL+Qt::Key_S );
	m_pKeyShortcutsForView->append( SaveAs_VIEW, tr("Save as"), Qt::ALT+Qt::CTRL+Qt::Key_S );
	m_pKeyShortcutsForView->append( OpenInNewWindow_VIEW, tr("Open in new window"), Qt::CTRL+Qt::Key_N );
	m_pKeyShortcutsForView->append( ShowHideMenuBar_VIEW, tr("Show/hide menu bar"), Qt::CTRL+Qt::Key_M );
	m_pKeyShortcutsForView->append( ShowHideToolBar_VIEW, tr("Show/hide tool bar"), Qt::CTRL+Qt::Key_T );
	m_pKeyShortcutsForView->append( ShowHideSideList_VIEW, tr("Show/hide side list"), Qt::CTRL+Qt::Key_L );
	m_pKeyShortcutsForView->append( ToggleToRawMode_VIEW, tr("Toggle to Raw mode"), Qt::ALT+Qt::CTRL+Qt::Key_W );
	m_pKeyShortcutsForView->append( ToggleToEditMode_VIEW, tr("Toggle to Edit mode"), Qt::Key_F4 );
	m_pKeyShortcutsForView->append( ToggleToRenderMode_VIEW, tr("Toggle to Render mode"), Qt::ALT+Qt::CTRL+Qt::Key_R );
	m_pKeyShortcutsForView->append( ToggleToHexMode_VIEW, tr("Toggle to Hex mode"), Qt::ALT+Qt::CTRL+Qt::Key_H );
	m_pKeyShortcutsForView->append( ShowSettings_VIEW, tr("Show settings"), Qt::CTRL+Qt::SHIFT+Qt::Key_S );
	m_pKeyShortcutsForView->append( Quit_VIEW, tr("Quit"), Qt::CTRL+Qt::Key_Q );

	m_pKeyShortcutsForView->updateEntryList( "/qtcmd/FileViewShortcuts/" ); // update from config file
	//slotApplyKeyShortcuts( m_pKeyShortcutsForView ); // for actions in current window
}

void QtCmd::initPanels( const QString & sLeftURL, const QString & sRightURL )
{
	m_pSplitter = new QSplitter( Qt::Horizontal, this, "Spliter" );
	m_pSplitter->resize( width(), height() );
	m_pSplitter->setOpaqueResize( TRUE ); // during size changing shows contains of panels
	m_pSplitter->setHandleWidth( 3 ); // default is 6
	m_pSplitter->show();
	setCentralWidget( m_pSplitter );

	initKeyShortcuts(); // create and init obj.
	m_pFilesAssociation = new FilesAssociation(); // load the files association
	m_pFilters = new Filters( m_pFilesAssociation );

	createPanel(LEFT_PANEL,  m_eKindOfLeftPanel,  sLeftURL.isEmpty() ? "/" : sLeftURL);
	createPanel(RIGHT_PANEL, m_eKindOfRightPanel, sRightURL.isEmpty() ? "/" : sRightURL);

	if (m_eKindOfLeftPanel == Panel::FILES_PANEL)
		m_pChangeKindOfViewToolBtn->setPopup( m_pKindOfVFPmenu );
	else
		m_pChangeKindOfViewToolBtn->setPopup( 0 ); // non popupMenu
}

void QtCmd::createPanel( PanelSide ePanelSide, Panel::KindOfPanel eKindOfPanel, const QString & sURL )
{
	Panel *pPanel = (ePanelSide == LEFT_PANEL) ? m_pLeftPanel : m_pRightPanel;
	delete pPanel;

// 	QString sPanelSide = (ePanelSide == LEFT_PANEL) ? "LeftPanel" : "RightPanel";

	if (eKindOfPanel == Panel::FILES_PANEL || eKindOfPanel == Panel::FILES_PANEL_FORFIND)
	{
		if (eKindOfPanel == Panel::FILES_PANEL_FORFIND)
		{
			if (ePanelSide == LEFT_PANEL)
				m_pLeftPanel  = new FilesPanelForFind( sURL, m_pSplitter, eKindOfPanel, "LeftPanel_FPFF" );
			else // RIGHT_PANEL
				m_pRightPanel = new FilesPanelForFind( sURL, m_pSplitter, eKindOfPanel, "RightPanel_FPFF" );
		}
		else
		{
			if (ePanelSide == LEFT_PANEL)
				m_pLeftPanel  = new FilesPanel( sURL, m_pSplitter, eKindOfPanel, "LeftPanel_FP" );
			else // RIGHT_PANEL
				m_pRightPanel = new FilesPanel( sURL, m_pSplitter, eKindOfPanel, "RightPanel_FP" );
		}

		pPanel = (ePanelSide == LEFT_PANEL) ? m_pLeftPanel : m_pRightPanel;

		if (! pPanel->fileSystemCreated())
			return;

		connect( this, SIGNAL(signalSetNewKindOfView( uint )), pPanel, SLOT(slotChangeKindOfView( uint )) );
		connect( this, SIGNAL(signalShowFtpManagerDlg()),      pPanel, SLOT(slotShowFtpManagerDlg()) );
		connect( this, SIGNAL(signalRereadCurrentDir()),       pPanel, SLOT(slotRereadCurrentDir()) );
		connect( this, SIGNAL(signalShowFreeSpace()),          pPanel, SLOT(slotShowSimpleDiscStat()) );
		connect( this, SIGNAL(signalDevicesList()),            pPanel, SLOT(slotShowDevicesMenu()) );
		connect( this, SIGNAL(signalShowFavorites()),          pPanel, SLOT(slotShowFavorites()) );
		connect( this, SIGNAL(signalQuickGetOutFromFS()),      pPanel, SLOT(slotQuickGetOutFromFS()) );
		connect( this, SIGNAL(signalFindFile()),               pPanel, SLOT(slotShowFindFileDlg()) );

		connect( this, SIGNAL(signalApplyListViewSettings(const ListViewSettings &)),
				 pPanel, SLOT(slotApplyListViewSettings(const ListViewSettings &)) );
		connect( this, SIGNAL(signalApplyFileSystemSettings(const FileSystemSettings &)),
				 pPanel, SLOT(slotApplyFileSystemSettings(const FileSystemSettings &)) );
		connect( this, SIGNAL(signalApplyOtherAppSettings(const OtherAppSettings &)),
				 pPanel, SLOT(slotApplyOtherAppSettings(const OtherAppSettings &)) );

		connect( pPanel, SIGNAL(signalHeaderSizeChange(int, int, int)),
				 this, SLOT(slotHeaderSizeChange(int, int, int)) );
		connect( pPanel, SIGNAL(signalSetPanelBesideURL(const QString &)),
				 this, SLOT(slotSetPanelBesideURL(const QString &)) );
		connect( pPanel, SIGNAL(signalChangePanel(Panel::KindOfPanel, const QString & , bool)),
				 this, SLOT(slotChangePanel(Panel::KindOfPanel, const QString & , bool)) );

		connect( pPanel, SIGNAL(signalCopyFiles(bool)), this, SLOT(slotCopyFiles(bool)) );
// 		connect( pPanel, SIGNAL(signalGetKindOfFSInPanelBeside( VFS::KindOfFilesSystem & )), // DEPRECATED
// 				 this, SLOT(slotGetKindOfFSInPanelBeside( VFS::KindOfFilesSystem & )) );
		connect( pPanel, SIGNAL(signalUpdateSecondPanel(int, Q3ValueList<Q3ListViewItem *> *, const UrlInfoExt &)),
				 this, SLOT(slotUpdateSecondPanel(int, Q3ValueList<Q3ListViewItem *> *, const UrlInfoExt &)) );
		connect( pPanel, SIGNAL(signalGetSILfromPanelBeside( Q3ValueList<Q3ListViewItem *> *& )),
				 this, SLOT(slotGetSILfromPanelBeside( Q3ValueList<Q3ListViewItem *> *& )) );
		connect( pPanel, SIGNAL(signalApplyKeyShortcut(KeyShortcuts *)),
				 this, SLOT(slotApplyKeyShortcuts(KeyShortcuts *)) );
		connect( pPanel, SIGNAL(signalPutFile(const QByteArray &, const QString &)),
				 this, SLOT(slotPutFile(const QByteArray &, const QString &)) );

		int nPanelWidth = (ePanelSide == LEFT_PANEL) ? m_nWidthOfLeftPanel : m_nWidthOfRightPanel;

		pPanel->resize( nPanelWidth, height() );
		pPanel->updateView();
		pPanel->setFocus();
	}
//	else
// 	if (eKindOfPanel == Panel::FILEVIEW_PANEL)
// 	{
// 		QStringList slList = QStringList() << sURL; //FIXME: tricki :)
//
// 		if (ePanelSide == LEFT_PANEL)
// 			m_pLeftPanel  = new FileView( slList, 0, FALSE, this, "LeftPanel_VP" );
// 		else // RIGHT_PANEL
// 			m_pRightPanel = new FileView( slList, 0, FALSE, this, "RightPanel_VP" );
//
// 		pPanel = (ePanelSide == LEFT_PANEL) ? m_pLeftPanel : m_pRightPanel;
//
// 		if (! pPanel->fileViewCreated()) // is not a pointer to the object type
// 			return;
//
// 		connect( pPanel, SIGNAL(signalUpdateItemOnLV( const QString & )), this, SLOT(slotUpdateItemOnLV( const QString & )) );
// 		connect( pPanel, SIGNAL(signalClose()), this, SLOT(slotCloseView()) );
// 	}

	if (pPanel == NULL)
		return;

	if ( m_pFilesAssociation->loaded() )
		pPanel->setFilesAssociation( m_pFilesAssociation );

	pPanel->setKeyShortcuts( m_pKeyShortcuts );
	pPanel->setFilters( m_pFilters );
}


Panel * QtCmd::panel( bool bCurrent ) //const
{
	m_eCurrentPanel = NO_PANEL;
	QString sSenderName = sender()->name();
	//qDebug() << "sSenderName=" << sSenderName;

	if (sSenderName.find("Left") != -1)
		m_eCurrentPanel = LEFT_PANEL;
	else
	if (sSenderName.find("Right") != -1)
		m_eCurrentPanel = RIGHT_PANEL;

	Panel *pPanel = NULL;

	if (sSenderName.isEmpty()) {
		if (m_pLeftPanel->hasFocus()) {
			m_eCurrentPanel = LEFT_PANEL;
			return bCurrent ? m_pLeftPanel : m_pRightPanel;
		}
		else
		if (m_pRightPanel->hasFocus()) {
			m_eCurrentPanel = RIGHT_PANEL;
			return bCurrent ? m_pRightPanel : m_pLeftPanel;
		}
	}
	else {
		if (bCurrent)
			pPanel = (m_eCurrentPanel == LEFT_PANEL)  ? m_pLeftPanel : m_pRightPanel;
		else // get other panel
			pPanel = (m_eCurrentPanel == RIGHT_PANEL) ? m_pRightPanel : m_pLeftPanel;
	}

	return pPanel;
}

// ---------- SLOTs ------------

void QtCmd::slotApplyKeyShortcuts( KeyShortcuts *pKeyShortcuts )
{
	if (pKeyShortcuts == NULL)
		return;

	m_pKeyShortcuts = pKeyShortcuts;

	m_pQuitAppsA->setShortcut( pKeyShortcuts->key(QuitApps_APP) );
	m_pShowHideMenuBarA->setShortcut( pKeyShortcuts->key(ShowHideMenuBar_APP) );
	m_pShowHideMenuBarA->setShortcutContext( Qt::WindowShortcut );
	m_pShowHideToolBarA->setShortcut( pKeyShortcuts->key(ShowHideToolBar_APP) );
	m_pShowHideBtnBarA->setShortcut( pKeyShortcuts->key(ShowHideButtonBar_APP) );
	m_pConfigureA->setShortcut( pKeyShortcuts->key(ShowSettingsOfApplication_APP) );
	m_pConfViewA->setShortcut( pKeyShortcuts->key(ShowSettingsOfView_APP) );
	m_pFtpMmngA->setShortcut( pKeyShortcuts->key(ShowFtpManager_APP) );
	m_pFreeSpaceA->setShortcut( pKeyShortcuts->key(ShowFreeSpace_APP) );
	m_pStorageA->setShortcut( pKeyShortcuts->key(ShowStorageDevicesList_APP) );
	m_pRereadA->setShortcut( pKeyShortcuts->key(RefreshDirectory_APP) );
	m_pFavoritesA->setShortcut( pKeyShortcuts->key(ShowFavorites_APP) );
	m_pFindFilesA->setShortcut( pKeyShortcuts->key(FindFile_APP) );
	m_pQuickGetOutA->setShortcut( pKeyShortcuts->key(QuickGoOutFromCurrentFileSystem_APP) );
}


void QtCmd::slotApplyFilters( Filters * pFilters )
{
	if ( pFilters ) {
		m_pLeftPanel->setFilters( pFilters );
		m_pRightPanel->setFilters( pFilters );
	}
}


void QtCmd::slotShowAbout()
{
	AboutDlg aboutDlg(VERSION);
	aboutDlg.exec();
	/*
	QMessageBox::about( this, "QtCmd",
		"<H3><CENTER>QtCommander "VERSION" </CENTER></H3><P>"
		+tr("Twin panels files manager")
		+"<P>(C) 2002, "AUTHOR", "AUTHORMAIL"<P>"AUTHOR2", "AUTHORMAIL2
		+"<P><I>"+tr("Distributed under the terms of the GNU General Public License v2")+"</I>"
	);
	*/
}

void QtCmd::slotShowAboutQt()
{
	QMessageBox::aboutQt( this, "QtCmd" );
}

void QtCmd::slotShowHelp()
{
	HelpDlg *pDlg = new HelpDlg;

	pDlg->setCaption( tr("Help")+" - QtCommander" );
	pDlg->resize( 530, 610 );
	pDlg->exec();

	delete pDlg;  pDlg = NULL;
}


void QtCmd::slotOpenFile()
{
	QString sCurrentPath = "/"; // get from current panel
	QString sFileName = QFileDialog::getOpenFileName(this,
		tr("Open file into new window")+" - QtCommander", sCurrentPath
	);
	if ( sFileName.isEmpty() )
		return;

}

void QtCmd::slotOpenDirectory()
{
	QString sCurrentPath = "/"; // get from current panel
	QString sDirName = QFileDialog::getExistingDirectory(this,
		tr("Open Directory")+" - QtCommander", sCurrentPath,
		QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks
	);
	if (sDirName.isEmpty())
		return;

}

void QtCmd::slotOpenArchive()
{
	QString sCurrentPath = "/"; // get from current panel
	QString sFileName = QFileDialog::getOpenFileName(this,
		tr("Open archive in current panel")+" - QtCommander", sCurrentPath,
		tr("Archives (*.rar *.zip *.tar *.tar.bz2 *.tar.gz *.ace *.arj *.lzma)")
	);
	if ( sFileName.isEmpty() )
		return;
}

void QtCmd::slotQuit()
{
	if ( m_bShowQuitWarn ) {
		int nResult = MessageBox::yesNo( this,
			tr("Quit an application")+" - QtCommander",
			tr("Do you really want to quit")+" ?",
			MessageBox::No
		);
		//qDebug() << "nResult=" << nResult;
		if ( nResult == MessageBox::Yes )
			qApp->closeAllWindows();

	}
	else
		qApp->closeAllWindows();
}


void QtCmd::slotChangeView()
{
	KindOfListView eKindOfLV = UNKNOWN_PANELVIEW;
	if (panel() != NULL) {
		if (panel()->hasFocus()) {
			if (panel()->kindOfPanel() != Panel::FILES_PANEL)
				return;
			else
				eKindOfLV = (KindOfListView)panel()->kindOfView();
		}
	}
	eKindOfLV = (eKindOfLV == TREE_PANELVIEW) ? LIST_PANELVIEW : TREE_PANELVIEW;
	emit signalSetNewKindOfView( eKindOfLV );
}


void QtCmd::slotFileView()
{
	if (panel() != NULL)
		panel()->openCurrentFile( FALSE ); // panel default is current
}

void QtCmd::slotFileEdit()
{
	if (panel() != NULL)
		panel()->openCurrentFile( TRUE );
}

void QtCmd::slotCopy()
{
	if (panel() != NULL)
		panel()->slotCopy( FALSE, FALSE );
}

void QtCmd::slotMove()
{
	if (panel() != NULL)
		panel()->slotCopy( FALSE, TRUE );
}

void QtCmd::slotMakeDir()
{
	if (panel() != NULL)
		panel()->createNewEmptyFile( TRUE );
}

void QtCmd::slotDelete()
{
	if (panel() != NULL)
		panel()->slotDelete();
}

void QtCmd::slotMakeLink()
{
	if (panel() != NULL)
		panel()->slotMakeLink();
}

void QtCmd::slotPutFile( const QByteArray & baBuffer, const QString & sFileName )
{
	panel(FALSE)->putFile( baBuffer, sFileName ); // FALSE - make for panel beside
}


void QtCmd::slotHeaderSizeChange( int nSection, int nOldSize, int nNewSize )
{
	panel(FALSE)->resizeSection( nSection, nOldSize, nNewSize ); // FALSE - make for panel beside
}


void QtCmd::slotShowHideMenuBar()
{
	Config *conf = Config::instance();
	bool bVisible = ! conf->isMenuBarVisible();
	conf->setMenuBarVisible(bVisible);
	bVisible ? menuBar()->show() : menuBar()->hide();
}

void QtCmd::slotShowHideToolBar()
{
	Config *conf = Config::instance();
	bool bVisible = ! conf->isToolBarVisible();
	conf->setToolBarVisible(bVisible);
	bVisible ? m_pToolBar->show() : m_pToolBar->hide();
}

void QtCmd::slotShowHideButtonBar()
{
	Config *conf = Config::instance();
	bool bVisible = ! conf->isButtonBarVisible();
	conf->setButtonBarVisible(bVisible);
	bVisible ? m_pButtonBar->show() : m_pButtonBar->hide();
}


void QtCmd::slotChangeListView( QAction * pAction )
{
	if (m_pLVactGroup != NULL && m_pLVactGroup->actions().count() <= 0)
		return;

	int nItemId = m_pLVactGroup->actions().indexOf( pAction );
	//qDebug() << "QtCmd::slotChangeListView, nItemId= " << nItemId;
	if (nItemId < 0 || nItemId > TREE_PANELVIEW)
		qDebug("QtCmd::slotChangeListView. Index out of range!");
	else
		emit signalSetNewKindOfView( (KindOfListView)nItemId );
}


void QtCmd::slotChangeProportionOfPanels( QAction * pAction )
{
	if (m_pProporActGroup != NULL && m_pProporActGroup->actions().count() <= 0)
		return;

	int nProportionId = m_pProporActGroup->actions().indexOf( pAction );
	//qDebug() << "QtCmd::slotChangeProportionOfPanels, nProportionId= " << nProportionId;
	if ( nProportionId < PP_5050 || nProportionId > PP_USER )
		return;

	uint nLeftPanelWidth = 400, nRightPanelWidth = 300;

	switch ( nProportionId )
	{
		case PP_5050:
			nLeftPanelWidth  = width()*50/100;
			nRightPanelWidth = width()*50/100;
		break;

		case PP_3070:
			nLeftPanelWidth  = width()*30/100;
			nRightPanelWidth = width()*70/100;
		break;

		case PP_7030:
			nLeftPanelWidth  = width()*70/100;
			nRightPanelWidth = width()*30/100;
		break;

		case PP_USER:
			nLeftPanelWidth  = m_nWidthOfLeftPanel;
			nRightPanelWidth = m_nWidthOfRightPanel;
		break;

		default:
		break;
	}

	static QList <int> s_vlPanelsWidth; // width of 2 spliter obj.
	s_vlPanelsWidth.clear();
	s_vlPanelsWidth.append( nLeftPanelWidth );
	s_vlPanelsWidth.append( nRightPanelWidth );
	m_pSplitter->setSizes( s_vlPanelsWidth );
}


void QtCmd::slotShowAppSettings()
{
	if ( ! m_pSettingsAppWidget ) {
		PluginManager *pPluginMgr = PluginManager::instance();
		m_pSettingsAppWidget = pPluginMgr->createWidgetPlugin("settingsapp", this);
		if ( ! m_pSettingsAppWidget ) {
			MessageBox::critical( this, tr("An error occured when loading a settingsapp plugin: %1").arg(pPluginMgr->errorMessage()) );
			return;
		}

		connect( m_pSettingsAppWidget, SIGNAL(signalApply(const ListViewSettings &)),
			this, SIGNAL(signalApplyListViewSettings(const ListViewSettings &)) );
		connect( m_pSettingsAppWidget, SIGNAL(signalApply(const FileSystemSettings &)),
			this, SIGNAL(signalApplyFileSystemSettings(const FileSystemSettings &)) );
		connect( m_pSettingsAppWidget, SIGNAL(signalApply(const OtherAppSettings &)),
			this, SIGNAL(signalApplyOtherAppSettings(const OtherAppSettings &)) );
		connect( m_pSettingsAppWidget, SIGNAL(signalApplyKeyShortcuts(KeyShortcuts *)),
			this, SLOT(slotApplyKeyShortcuts(KeyShortcuts *)) );
		// --- init settings ppDlg.
		connect( this, SIGNAL(signalSetKeyShortcuts(KeyShortcuts *)),
			m_pSettingsAppWidget, SLOT(slotSetKeyShortcuts(KeyShortcuts *)) );

		emit signalSetKeyShortcuts( m_pKeyShortcuts );

		connect( this, SIGNAL(signalSetFilesAssociation(FilesAssociation *)),
			m_pSettingsAppWidget, SLOT(slotSetFilesAssociation(FilesAssociation *)) );

		emit signalSetFilesAssociation( m_pFilesAssociation );
	}
	Q_CHECK_PTR(m_pSettingsAppWidget);
	m_pSettingsAppWidget->show();
}

void QtCmd::slotShowViewSettings()
{
	if ( ! m_pSettingsWidget ) {
		PluginManager *pPluginMgr = PluginManager::instance();
		m_pSettingsWidget = pPluginMgr->createWidgetPlugin("settingsview", this);
		if ( ! m_pSettingsWidget ) {
			MessageBox::critical( this, tr("An error occured when loading a settingsview plugin: %1").arg(pPluginMgr->errorMessage()) );
			return;
		}

//      connect( m_pSettingsWidget, SIGNAL(signalApplyKeyShortcuts(KeyShortcuts *)),
//          this, SLOT(slotApplyKeyShortcuts(KeyShortcuts *)) );
//      connect( m_pSettingsWidget, SIGNAL(signalSaveSettings()), mView, SLOT(slotSaveSettings()) );
//  // --- init settings dlg.
//      connect( this, SIGNAL(signalSetCurrentKindOfView(View::KindOfView)),
//          m_pSettingsWidget, SLOT(slotSetCurrentKindOfView(View::KindOfView)) );
		connect( this, SIGNAL(signalSetKeyShortcuts(KeyShortcuts *)),
			m_pSettingsWidget, SLOT(slotSetKeyShortcuts(KeyShortcuts *)) );

//      emit signalSetCurrentKindOfView( mKindOfView );
		emit signalSetKeyShortcuts( m_pKeyShortcutsForView );
    }
	Q_CHECK_PTR(m_pSettingsWidget);
	m_pSettingsWidget->show();
}

void QtCmd::slotSetPanelBesideURL( const QString & sUrl )
{
	panel(FALSE)->setPanelBesideURL( sUrl ); // FALSE - make for panel beside
}


void QtCmd::slotChangePanel( Panel::KindOfPanel eKindOfPanel, const QString & sInitURL, bool bCurrent )
{
	QString sFName = sInitURL;
	Panel *pPanel  = panel(bCurrent);  // FALSE - make for next panel
	PanelSide eNextPanel = (m_eCurrentPanel == RIGHT_PANEL) ? LEFT_PANEL : RIGHT_PANEL;
	PanelSide ePanelSide = (bCurrent) ? m_eCurrentPanel : eNextPanel;

	if (eKindOfPanel == Panel::FILES_PANEL)
	{
		if ( sFName.at(sFName.length()-1) == '/') { // let's open directory
			sFName.remove( sInitURL.find(".."), 3 );

			if (pPanel->kindOfPanel() == Panel::FILES_PANEL)
				pPanel->slotPathChanged(sFName);
			else
				createPanel(ePanelSide, eKindOfPanel, sFName);
		}
	}
	else
	if (eKindOfPanel ==  Panel::FILES_PANEL_FORFIND)
	{
		sFName = panel(FALSE)->currentURL();
		createPanel(ePanelSide, eKindOfPanel, sFName);
	}
	else
	if (eKindOfPanel ==  Panel::FILEVIEW_PANEL) // FIXME poprawic podglad w panelu obok
	{
		// ---- we are need to open a file
		// need to use a file load mechanizm from the panel beside
		//pPanelBeside->createPanel( Panel::FILEVIEW_PANEL, sFName );
		createPanel(ePanelSide, Panel::FILEVIEW_PANEL, sFName);
		//pPanel->initLoadFileToView( m_pFilesAssociation, panel()->filesPanel() );
		//pPanelBeside->initLoadFileToView( m_pFilesAssociation, panel() );
	//zamiast powyzszego wyw.
	// 	pPanelBeside->setFilesAssociation( m_pFilesAssociation );
	// 	connect( pPanelBeside, SIGNAL(signalReadFile(const QString &, QByteArray &, int &, int)),
	// 		panel(), SLOT(slotReadFileToView(const QString &, QByteArray &, int &, int)) );
	// 	pPanelBeside->slotReadyRead();
	}

}


void QtCmd::slotCloseView()
{
	createPanel(m_eCurrentPanel, Panel::FILEVIEW_PANEL);
	panel()->setPanelBesideURL( panel(FALSE)->currentURL() ); // FALSE - make for panel beside
}


void QtCmd::slotUpdateItemOnLV( const QString & /*sFileName*/ )
{
	//slotUpdateSecondList( Open, sFileName, "" );
	// FIXME uaktulniac liste przy podgladzie pliku osadzonym w panelu
	// wykorzystywany przez osadzony w panelu podgladu pliku (w trybie edycji, zapis, uaktualnienie el.na liscie w panelu obok)
}

/*
void QtCmd::slotUpdateSecondList( Operation eOperation, const QString & sFileName, const QString & sNewFileName )
{
	panel(FALSE)->updateList( eOperation, sFileName, sNewFileName );
}
*/

void QtCmd::slotCopyFiles( bool bMove )
{
	panel(FALSE)->copyFiles(bMove); // FALSE - make for panel beside
}

void QtCmd::slotGetKindOfFSInPanelBeside( VFS::KindOfFilesSystem & eKindOfFS )
{
	panel(FALSE)->getKindOfFS( eKindOfFS ); // FALSE - make for panel beside
}

void QtCmd::slotUpdateSecondPanel( int nUpdateListOp, Q3ValueList<Q3ListViewItem *> *pItemsList, const UrlInfoExt & uNewUrlInfo )
{
	panel(FALSE)->updateList( nUpdateListOp, pItemsList, uNewUrlInfo ); // FALSE - make for panel beside
}

void QtCmd::slotGetSILfromPanelBeside( Q3ValueList<Q3ListViewItem *> *& pSelectedItemsList )
{
	panel(FALSE)->getSelectedItemsList( pSelectedItemsList ); // FALSE - make for panel beside
}


void QtCmd::resizeEvent( QResizeEvent * )
{
	setMinimumSize( 600, 400 );
}

