
#ifndef _EXTENTIONTABLES_H
#define _EXTENTIONTABLES_H

static const char *sourceFileExt[] = {
	"cpp", "cxx", "cc", "c", "C", "h", "hh", "pl", "perl", "sh", "csh", "py", "java",
	"php", "php3", "php4", "css", "xml", "xslt", "sql", "S", "sgml", "wml", "awk",
	"p", "pas", "pp", "dpr", "adb", "ads",
	"moc", "tcl", "tk", "rb", "ruby",
	"dif", "diff", "patch",
	"ts",
	0
};

static const char *renderFileExt[] = {
	"htm", "html", "shtml", "rtf",
	"latex", "tex", "texi", "docbook", "dvi",
	"ui", "xmi",
	"1", "2", "3", "4", "5", "6", "7", "8", "9", // manual
	0
};

static const char *bindocFileExt[] = {
	"pdf", "ps",
	"doc", "sxw", "kwd", // text
	"xls", "sxc", "ksp", // sheet
	"po",
	0
};

static const char *imageFileExt[] = {
	"bmp", "gif", "jpg", "jpeg", "png", "xpm", "jp2", "svg", "pcx", "djv", "djvu",
	"tiff", "tga", "xbm", "cgm", "eps", "ico", "pbm", "pgm", "ppm", "wmf",
	"swf", "spl", "mng",
	0
};

static const char *soundFileExt[] = {
	"wav", "mp3", "ogg", "cda", "mid", "midi",
	"mod", "s3m", "stm", "ult", "uni", "xm", "m15", "mtm", "669", "it", // amiga sound
	"au", "wv", "mpc", "flac",
	"m3u", "cue", 
	0
};

static const char *archiveFileExt[] = {
	"gz", "bz2", "tar", "tgz", "zip", "rar", "sfx", "arj", "jar",
	"rpm", "cpio", "deb", "lzh", "ace", "Z", "tzo", "zoo",
	0
};

static const char *videoFileExt[] = {
	"asf", "asx",
	"avi", "wmv",
	"divx", "mpg", "mpeg",
	"qt", "mov", "moov", "qtvr",
	"iff", "anim3", "anim5", "anim7",
	"fli", "flc",
	0
};

#endif
