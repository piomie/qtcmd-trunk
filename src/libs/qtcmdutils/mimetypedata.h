/***************************************************************************
                          mimetypedata.h  -  description
                             -------------------
    begin                : sat feb 9 2008
    copyright            : (C) 2008 by Piotr MierzwiDski
    email                : peterm@go2.pl

    copyright            : See COPYING file that comes with this project
 ***************************************************************************/
#ifndef _MIMETYPEDATA_
#define _MIMETYPEDATA_

#include <QObject>

/**
	@author Piotr MierzwiDski <piotrmierzwinski@gmail.com>
*/
namespace MimeDataInfo {

	enum FileType { NULL_TYPE=0, RAW_TEXT, RENDERABLE_TEXT, BINARY_DOC, IMAGE, SOUND, VIDEO, ARCHIVE };

	static const struct MimeData {
		const char * typeDesc; // main key
		QString      category;
		QString      subCategory;
		FileType     fileType;
		const char * extentions;
		QString      description;
	} mimeData[] = {
		{ "ada",  QObject::tr("text"), QObject::tr("source"), RAW_TEXT, "adb ads", QObject::tr("ADA source file") },
		{ "asm",  QObject::tr("text"), QObject::tr("source"), RAW_TEXT, "S",    QObject::tr("Assembler source file") },
		{ "awk",  QObject::tr("text"), QObject::tr("script"), RAW_TEXT, "awk",  QObject::tr("AWK script") },
   { "c_c++ hdr", QObject::tr("text"), QObject::tr("source"), RAW_TEXT, "h hh", QObject::tr("C or C++ header file") },
   { "c_c++ src", QObject::tr("text"), QObject::tr("source"), RAW_TEXT, "cpp cxx cc c C", QObject::tr("C or C++ source file") },
		{ "java", QObject::tr("text"), QObject::tr("source"), RAW_TEXT, "java", QObject::tr("Java source file") },
		{ "moc",  QObject::tr("text"), QObject::tr("source"), RAW_TEXT, "moc",  QObject::tr("Qt source MOC file") },
		{ "pas",  QObject::tr("text"), QObject::tr("source"), RAW_TEXT, "p pas pp dpr", QObject::tr("Pascal source file") },
		{ "perl", QObject::tr("text"), QObject::tr("script"), RAW_TEXT, "perl pl", QObject::tr("Script in Perl") },
		{ "php",  QObject::tr("text"), QObject::tr("script"), RAW_TEXT, "php php3 php4", QObject::tr("PHP script") },
		{ "py",   QObject::tr("text"), QObject::tr("script"), RAW_TEXT, "py",   QObject::tr("Python script") },
		{ "sh",   QObject::tr("text"), QObject::tr("script"), RAW_TEXT, "sh csh ksh", QObject::tr("Shell script") },
   { "c_c++ src", QObject::tr("text"), QObject::tr("source"), RAW_TEXT, "cpp cxx cc c C", QObject::tr("C or C++ source file") },
		{ "sql",  QObject::tr("text"), QObject::tr("script"), RAW_TEXT, "sql",  QObject::tr("SQL script") },
		{ "tcl",  QObject::tr("text"), QObject::tr("script"), RAW_TEXT, "tcl",  QObject::tr("TCL script") },
		{ "tk",   QObject::tr("text"), QObject::tr("script"), RAW_TEXT, "tk",   QObject::tr("TK script") },
		{ "wml",  QObject::tr("text"), QObject::tr("script"), RAW_TEXT, "wml",  QObject::tr("WML script") },
 { "javaScript",  QObject::tr("text"), QObject::tr("script"), RENDERABLE_TEXT, "js", QObject::tr("Java script") },
		{ "log",  QObject::tr("text"), QObject::tr("document"), RAW_TEXT, "log",  QObject::tr("Log file") },
		{ "txt",  QObject::tr("text"), QObject::tr("document"), RAW_TEXT, "txt", QObject::tr("Plain text file") },
		{ "xslt", QObject::tr("text"), QObject::tr("markup"), RAW_TEXT, "xslt", QObject::tr("XSLT Style Sheet file") },
		{ "jsp",  QObject::tr("text"), QObject::tr("markup"), RAW_TEXT, "jsp", QObject::tr("Java script") },
		{ "html", QObject::tr("text"), QObject::tr("markup"), RENDERABLE_TEXT, "htm html shtml", QObject::tr("HTML document") },
		{ "css",  QObject::tr("text"), QObject::tr("markup"), RAW_TEXT, "css",  QObject::tr("Cascade Style Sheet file") },
		{ "latex",QObject::tr("text"), QObject::tr("markup"), RENDERABLE_TEXT, "latex", QObject::tr("LaTeX document") },
		{ "ruby", QObject::tr("text"), QObject::tr("markup"), RAW_TEXT, "rb ruby", QObject::tr("RUBY script") },
		{ "sgml", QObject::tr("text"), QObject::tr("markup"), RAW_TEXT, "sgml", QObject::tr("SGML file") },
		{ "vrml", QObject::tr("text"), QObject::tr("markup"), RAW_TEXT, "sgml", QObject::tr("Virtual Reality Markup Language") },
		{ "ts",   QObject::tr("text"), QObject::tr("markup"), RAW_TEXT, "ts",   QObject::tr("Source translations for Qt's program") },
		{ "asp",  QObject::tr("text"), QObject::tr("markup"), RAW_TEXT, "asp",  QObject::tr("ASP file") },
		{ "xml",  QObject::tr("text"), QObject::tr("markup"), RAW_TEXT, "xml",  QObject::tr("Xtra Markup Language") },
		{ "tex",  QObject::tr("text"), QObject::tr("markup"), RENDERABLE_TEXT, "tex texi", QObject::tr("TeX document") },
		{ "ui",   QObject::tr("text"), QObject::tr("markup"), RENDERABLE_TEXT, "ui",    QObject::tr("Source of User Interface for Qt's programm") },
		{ "diff", QObject::tr("text"), QObject::tr("other"),  RAW_TEXT, "dif diff patch", QObject::tr("Differents between two files") },
		{ "um",   QObject::tr("text"), QObject::tr("other"),  RAW_TEXT, "um",   QObject::tr("Cascade Style Sheet") },

	 { "docbook", QObject::tr("text"), QObject::tr("document"), RENDERABLE_TEXT, "docbook", QObject::tr("DocBook document") },
		{ "dvi",  QObject::tr("text"), QObject::tr("document"), RENDERABLE_TEXT, "dvi",   QObject::tr("DVI TeX file") },
		{ "rtf",  QObject::tr("text"), QObject::tr("document"), RENDERABLE_TEXT, "rtf",   QObject::tr("RTF document") },
		{ "man",  QObject::tr("text"), QObject::tr("document"), RENDERABLE_TEXT, "1 2 3 4 5 6 7 8 9", QObject::tr("Manual textbook document") },
		{ "xmi",  QObject::tr("text"), QObject::tr("document"), RENDERABLE_TEXT, "xmi",   QObject::tr("UML Umbrello modeler file") },

		{ "pdf", QObject::tr("document"), "", BINARY_DOC, "pdf", QObject::tr("PDF document") },
		{ "doc", QObject::tr("document"), "", BINARY_DOC, "doc", QObject::tr("Microsoft Word document") },
		{ "sxw", QObject::tr("document"), "", BINARY_DOC, "sxw", QObject::tr("OpenOffice.org 1.x text document") },
		{ "kwd", QObject::tr("document"), "", BINARY_DOC, "kwd", QObject::tr("KWord text document") },
		{ "odf", QObject::tr("document"), "", BINARY_DOC, "odf", QObject::tr("Open Office document") },
		{ "xls", QObject::tr("document"), "", BINARY_DOC, "xls", QObject::tr("Microsoft Excel document") },
		{ "sxc", QObject::tr("document"), "", BINARY_DOC, "sxc", QObject::tr("OpenOffice.org sheet") },
		{ "ksp", QObject::tr("document"), "", BINARY_DOC, "ksp", QObject::tr("KWord sheet") },
		{ "po",  QObject::tr("document"), "", BINARY_DOC, "po", QObject::tr("Binary translations for Qt's program") },
		{ "ppo", QObject::tr("document"), "", BINARY_DOC, "ppo", QObject::tr("Microsoft Power Point Presentation") },
		{ "ps",  QObject::tr("document"), "", BINARY_DOC, "ps", QObject::tr("PostScript document") },

		{ "bmp",  QObject::tr("image"), "", IMAGE, "bmp", QObject::tr("BMP image") },
		{ "gif",  QObject::tr("image"), "", IMAGE, "gif", QObject::tr("GIF image") },
		{ "jpeg", QObject::tr("image"), "", IMAGE, "jpeg jpg", QObject::tr("JPEG image") },
		{ "png",  QObject::tr("image"), "", IMAGE, "png", QObject::tr("PNG image") },
		{ "xpm",  QObject::tr("image"), "", IMAGE, "xpm", QObject::tr("X PixMap image") },
    { "jpeg2000", QObject::tr("image"), "", IMAGE, "jp2", QObject::tr("JPEG 2000 image") },
		{ "svg",  QObject::tr("image"), "", IMAGE, "svg", QObject::tr("Scalable Vector Graphics") },
		{ "pcx",  QObject::tr("image"), "", IMAGE, "pcx", QObject::tr("PCX image") },
		{ "djvu", QObject::tr("image"), "", IMAGE, "djv djvu", QObject::tr("D'Javu image") },
		{ "tiff", QObject::tr("image"), "", IMAGE, "tiff", QObject::tr("TIFF image") },
		{ "tga",  QObject::tr("image"), "", IMAGE, "tga", QObject::tr("Truevision Targa image") },
		{ "xbm",  QObject::tr("image"), "", IMAGE, "xbm", QObject::tr("X BitMap image") },
		{ "cgm",  QObject::tr("image"), "", IMAGE, "cgm", QObject::tr("Computer Graphics Metafile") },
		{ "eps",  QObject::tr("image"), "", IMAGE, "eps", QObject::tr("EPS image") },
		{ "ico",  QObject::tr("image"), "", IMAGE, "ico", QObject::tr("Microsoft Windows icons") },
		{ "pbm",  QObject::tr("image"), "", IMAGE, "pbm", QObject::tr("Portable BitMap image") },
		{ "pgm",  QObject::tr("image"), "", IMAGE, "pgm", QObject::tr("Portable GrayMap image") },
		{ "ppm",  QObject::tr("image"), "", IMAGE, "ppm", QObject::tr("Portable PixMap image") },
		{ "xmf",  QObject::tr("image"), "", IMAGE, "wmf", QObject::tr("Microsoft Windows Metafile") },
		{ "swf",  QObject::tr("image"), "", IMAGE, "swf", QObject::tr("Shockwave Flash Media") },
		{ "spl",  QObject::tr("image"), "", IMAGE, "spl", QObject::tr("Netscape Shockwave Flash") },
		{ "mng",  QObject::tr("image"), "", IMAGE, "mng", QObject::tr("MNG image") },

		{ "wav",  QObject::tr("sound"), "", SOUND, "wav",  QObject::tr("WAV file") },
		{ "mp3",  QObject::tr("sound"), "", SOUND, "mp3",  QObject::tr("MPEG Layer3 sound") },
		{ "ogg",  QObject::tr("sound"), "", SOUND, "ogg",  QObject::tr("OGG file") },
		{ "cda",  QObject::tr("sound"), "", SOUND, "cda",  QObject::tr("Audio track of Compact Disk") },
		{ "midi", QObject::tr("sound"), "", SOUND, "mid midi", QObject::tr("MIDI file") },
		{ "mod",  QObject::tr("sound"), "", SOUND, "mod s3m stm ult uni xm m15 mtm 669 it", QObject::tr("Amiga sound file") },
		{ "au",   QObject::tr("sound"), "", SOUND, "au",   QObject::tr("ULAW (Sun) audio file") },
		{ "wv",   QObject::tr("sound"), "", SOUND, "wv",   QObject::tr("WavPack lossless audio file") },
		{ "mpc",  QObject::tr("sound"), "", SOUND, "mpc",  QObject::tr("Musepack audio file") },
		{ "flac", QObject::tr("sound"), "", SOUND, "flac", QObject::tr("Flac lossless audio file") },
		{ "ape",  QObject::tr("sound"), "", SOUND, "ape",  QObject::tr("Monkey's Audio Codec (lossless audio file)") },
		{ "m3u",  QObject::tr("sound"), "", SOUND, "m3u",  QObject::tr("Music play list") },
		{ "cue",  QObject::tr("sound"), "", SOUND, "cue",  QObject::tr("Text file that details the layout of tracks on a compact disc") },

		{ "asf",  QObject::tr("video"), "", VIDEO, "asf asx", QObject::tr("Microsoft Media Format") },
		{ "avi",  QObject::tr("video"), "", VIDEO, "avi wmv", QObject::tr("Microsoft AVI Video") },
		{ "divx", QObject::tr("video"), "", VIDEO, "divx", QObject::tr("DIVX video format") },
		{ "mpeg", QObject::tr("video"), "", VIDEO, "mpeg mpg", QObject::tr("MPEG video format") },
		{ "qt",   QObject::tr("video"), "", VIDEO, "qt mov moov qtvr", QObject::tr("Quicktime video format") },
		{ "iff",  QObject::tr("video"), "", VIDEO, "anim3 anim5 anim7 iff", QObject::tr("IFF (Animation) video format") },
		{ "fli",  QObject::tr("video"), "", VIDEO, "flc fli", QObject::tr("Autodesk FLIC file") },

		{ "gz",   QObject::tr("archive"), "", ARCHIVE, "gz",  QObject::tr("GZip compressed file") },
		{ "bz2",  QObject::tr("archive"), "", ARCHIVE, "bz2", QObject::tr("BZip2 compressed file") },
		{ "tar",  QObject::tr("archive"), "", ARCHIVE, "tar", QObject::tr("Tarball archive") },
		{ "tgz",  QObject::tr("archive"), "", ARCHIVE, "tar.gz tgz ", QObject::tr("GZip compressed Tar file") },
		{ "tbz",  QObject::tr("archive"), "", ARCHIVE, "tar.bz tar.bz2 tbz tbz2", QObject::tr("BZip2 compressed Tar file") },
		{ "zip",  QObject::tr("archive"), "", ARCHIVE, "zip", QObject::tr("ZIP archive") },
		{ "rar",  QObject::tr("archive"), "", ARCHIVE, "rar sfx", QObject::tr("RAR archive") },
		{ "arj",  QObject::tr("archive"), "", ARCHIVE, "arj jar", QObject::tr("ARJ archive") },
		{ "rpm",  QObject::tr("archive"), "", ARCHIVE, "rpm srpm", QObject::tr("RedHat package") },
		{ "cpio", QObject::tr("archive"), "", ARCHIVE, "cpio", QObject::tr("CPIO file") },
		{ "deb",  QObject::tr("archive"), "", ARCHIVE, "deb",  QObject::tr("Debian package") },
		{ "lzh",  QObject::tr("archive"), "", ARCHIVE, "lzh",  QObject::tr("LZH archive") },
		{ "ace",  QObject::tr("archive"), "", ARCHIVE, "ace",  QObject::tr("ACE") },
		{ "Z",    QObject::tr("archive"), "", ARCHIVE, "Z",    QObject::tr("Z compressed file") },
		{ "tzo",  QObject::tr("archive"), "", ARCHIVE, "tzo",  QObject::tr("Z compressed Tar file") },
		{ "zoo",  QObject::tr("archive"), "", ARCHIVE, "zoo",  QObject::tr("Zoo archive") },
		{ "lzma", QObject::tr("archive"), "", ARCHIVE, "lzma", QObject::tr("LzMa compressed file") },
		{ "7z",   QObject::tr("archive"), "", ARCHIVE, "7z",   QObject::tr("7Zip archive") },
		{ 0, 0, 0, NULL_TYPE, 0, 0 }
	};

}

#endif
