/***************************************************************************
                          fileinfoext.cpp  -  description
                             -------------------
    begin                : tue dec 9 2003
    copyright            : (C) 2003 by Piotr Mierzwi�ski
    email                : peterm@go2.pl

    copyright            : See COPYING file that comes with this project

 $Id$
 ***************************************************************************/
#include <QUrlInfo>

#include "fileinfoext.h"
#include "extentions.cpp" // tables that contains file extentions

#include <errno.h>


KindOfFile FileInfoExt::kindOfFile( const QString & sFileName )
{
	if (sFileName.isEmpty() || sFileName.at(sFileName.length()-1) == '/')
		return UNKNOWNfile;

	bool bFoundExt = FALSE;
	KindOfFile eKindOfFile = UNKNOWNfile;
	QString sFileCompleteExt = QFileInfo(sFileName).suffix().toLower(), sCompleteExt;

	uint ti=0, i;
	const char **extentionTab;
	const char **extentionTabPtrs[] = {
		sourceFileExt, renderFileExt, imageFileExt, soundFileExt, archiveFileExt, videoFileExt, bindocFileExt,
		0
	};

	KindOfFile kindOfFileTab[] = {
		SOURCEfile, RENDERfile, IMAGEfile, SOUNDfile, ARCHIVEfile, VIDEOfile,
		UNKNOWNfile
	};


	while( (extentionTab=extentionTabPtrs[ti]) ) {
		i = 0;
		do {
			sCompleteExt = extentionTab[i++];
			if ( sCompleteExt == sFileCompleteExt ) {
				eKindOfFile = kindOfFileTab[ti];
				bFoundExt = true;
				break;
			}
       	} while ( ! sCompleteExt.isEmpty() );

		if ( bFoundExt )
			break;
		ti++;
	}

	return eKindOfFile;
}


bool FileInfoExt::isArchive( const QString & sFileName )
{
	return !sFileName.isEmpty() && (kindOfFile(archiveFullName(sFileName)) == ARCHIVEfile);
}


QString FileInfoExt::archiveFullName( const QString & sFileNameIn )
{
	bool bArchive = FALSE;
	QString sFileName = sFileNameIn;
	uint nFileNameLen = sFileName.length()-1;

	if ( sFileName.at(nFileNameLen) == '/' ) // it's directory
		sFileName.remove( nFileNameLen, '/' );

	while ( ! sFileName.isEmpty() ) {
		if ( FileInfoExt::kindOfFile(sFileName) == ARCHIVEfile ) {
			bArchive = true;
			break;
		}
		sFileName = sFileName.left( sFileName.lastIndexOf('/', sFileName.length()-2) );
		if ( sFileName.count('/') < 2 )
			break;
	}

	return sFileName;
}


KindOfArchive FileInfoExt::kindOfArchive( const QString & sFileNameIn )
{
// "rpm", "cpio", "deb", "ace", "tzo", "zoo"
	QString sFileName  = archiveFullName( sFileNameIn );
	QString sFirstExt  = QFileInfo(sFileName).suffix().toLower(); // first from right side
	QString sSecondExt = QFileInfo(sFileName).completeSuffix().toLower();
	uint nDotNum = sSecondExt.count('.')-1;
	sSecondExt = sSecondExt.section( '.', nDotNum, nDotNum );

	if ( sFirstExt == "bz2" ) {
		if ( sSecondExt != "cpio" ) {
			if ( sSecondExt == "tar" )
				return TBZIP2arch;
			else
				return BZIP2compr;
		}
	}
	else
	if ( sFirstExt == "gz" ) {
		if ( sSecondExt != "cpio" ) {
			if ( sSecondExt == "tar" )
				return TGZIParch;
			else
				return GZIPcompr;
		}
	}
	else
	if ( sFirstExt == "z" ) {
		if ( sSecondExt != "cpio" ) {
			if ( sSecondExt == "tar" )
				return TZarch;
			else
				return COMPRESScompr;
		}
	}
	else
	if ( sFirstExt == "gz" ) {
		if ( sSecondExt == "cpio" )
			return CGZIParch;
		else
			return GZIPcompr;
	}
	else
	if ( sFirstExt == "z" ) {
		if ( sSecondExt == "cpio" )
			return TZarch;
		else
			return COMPRESScompr;
	}
	else
	if ( sFirstExt == "tgz" )
		return TGZIParch;
	else
	if ( sFirstExt == "tar" )
		return TARarch;
	else
	if ( sFirstExt == "cpio" )
		return CPIOarch;
	else
	if ( sFirstExt == "rar" || sFirstExt == "sfx" )
		return RARarch;
	else
	if ( sFirstExt == "zip" || sFirstExt == "jar" )
		return ZIParch;
	else
	if ( sFirstExt == "arj" )
		return ARJarch;
	else
	if ( sFirstExt == "lzh" )
		return LZHarch;
	else
	if ( sFirstExt == "zoo" )
		return ZOOarch;

	return UNKNOWNarch;
}


QString FileInfoExt::filePath( const QString & sFullFileName )
{
	if (sFullFileName.isEmpty()) {
		qDebug("FileInfoExt::filePath(). WARNING: Input path is empty! Return \"/\".");
		return "/";
	}

	bool bAbsolutePath = (sFullFileName.startsWith('/') ||
		sFullFileName.startsWith("smb://") || sFullFileName.startsWith("arc://") ||
		sFullFileName.startsWith("ftp://") || sFullFileName.startsWith("sftp://")
	);
	if ( ! bAbsolutePath )
		return sFullFileName;

	if ( sFullFileName.indexOf("//") != -1 ) {
		if ( sFullFileName.count('/') < 4 )
			return sFullFileName;
	}
	QString sPath;

	if ( sFullFileName.at(sFullFileName.length()-1) == '/' ) // directory
		sPath = sFullFileName.left( sFullFileName.lastIndexOf('/', sFullFileName.length()-2)+1 );
	else
		sPath = sFullFileName.left( sFullFileName.lastIndexOf('/')+1 );

	return sPath;
}


QString FileInfoExt::fileName( const QString & sFullFileName )
{
	if ( sFullFileName.isEmpty()) {
		qDebug("FileInfoExt::fileName(). WARNING: Input path is empty! Return \"\".");
		return QString("");
	}
	QString sFileName;

	if (sFullFileName.endsWith('/')) { // directory
		int numOfSlashes = sFullFileName.count('/')-1;
		sFileName = sFullFileName.section( '/', numOfSlashes, numOfSlashes );
	}
	else
		sFileName = sFullFileName.right( sFullFileName.length() - sFullFileName.lastIndexOf('/') - 1 );

	return sFileName;
}


QString FileInfoExt::hostName( const QString & sUrl, bool bAddSlash )
{
	QString sHost;

	if ( sUrl.startsWith('/') )
		sHost = "/";
	else
	if ( sUrl.startsWith("ftp://") || sUrl.startsWith("sftp://") || sUrl.startsWith("smb://") || sUrl.startsWith("arc://") )
		sHost = sUrl.left( sUrl.indexOf('/', sUrl.indexOf("//")+1) );

	if ( bAddSlash && sHost != "/" )
		sHost += "/";

	return sHost;
}


bool FileInfoExt::specialFile( const QString & sFileName, SpecialFileType sft )
{
	return doStat( sFileName, (int)sft );
}


bool FileInfoExt::specialPerm( const QString & sFileName, SpecialPerm sp )
{
	return doStat( sFileName, (int)sp );
}


bool FileInfoExt::doStat( const QString & sFileName, int nStatInfo )
{
	if ( sFileName.at(0) != '/' ) {
		qDebug("FileInfoExt::doStat(). Illegal file system of file '%s' !\nAvailable only for local FS.", qPrintable(sFileName) );
		return FALSE;
	}

	QString fName = sFileName;

	if ( nStatInfo == S_IFSOCK || nStatInfo == S_IFBLK || nStatInfo == S_IFCHR || nStatInfo == S_IFIFO ) {
		QFileInfo file( fName );
		if ( file.isSymLink() )
			fName = file.readLink();
	}

	struct stat statBuffer;
	if ( ::lstat(QFile::encodeName( fName ), & statBuffer) != 0 )  {
		qDebug("FileInfoExt::doStat(). lstat error=%d, file=%s", errno, qPrintable(fName) );
		return FALSE;
	}

	bool bPermission = statBuffer.st_mode & nStatInfo;

	if ( nStatInfo == S_IFCHR )  bPermission = S_ISCHR( statBuffer.st_mode );
	else
	if ( nStatInfo == S_IFBLK )  bPermission = S_ISBLK( statBuffer.st_mode );
	else
	if ( nStatInfo == S_IFIFO )  bPermission = S_ISFIFO( statBuffer.st_mode );
	else
	if ( nStatInfo == S_IFSOCK)  bPermission = S_ISSOCK( statBuffer.st_mode );

	return bPermission;
}


int FileInfoExt::permission( const QString & sFileName )
{
	if ( ! sFileName.startsWith('/') || sFileName.isEmpty() ) {
		qDebug("FileInfoExt::permission(). Illegal file system '%s' !\nAvailable only for local FS.", qPrintable(sFileName) );
		return -1;
	}

	int nPerm = 0;
	QFileInfo fileInfo( sFileName );

	if ( fileInfo.isSymLink() )
		fileInfo.setFile( fileInfo.readLink() );
	else
	if ( ! fileInfo.exists() )
		return -1;

	if ( fileInfo.permission( QFile::ReadUser   ) )   nPerm += QUrlInfo::ReadOwner;
	if ( fileInfo.permission( QFile::WriteUser  ) )   nPerm += QUrlInfo::WriteOwner;
	if ( fileInfo.permission( QFile::ExeUser    ) )   nPerm += QUrlInfo::ExeOwner;
	if ( fileInfo.permission( QFile::ReadGroup  ) )   nPerm += QUrlInfo::ReadGroup;
	if ( fileInfo.permission( QFile::WriteGroup ) )   nPerm += QUrlInfo::WriteGroup;
	if ( fileInfo.permission( QFile::ExeGroup   ) )   nPerm += QUrlInfo::ExeGroup;
	if ( fileInfo.permission( QFile::ReadOther  ) )   nPerm += QUrlInfo::ReadOther;
	if ( fileInfo.permission( QFile::WriteOther ) )   nPerm += QUrlInfo::WriteOther;
	if ( fileInfo.permission( QFile::ExeOther   ) )   nPerm += QUrlInfo::ExeOther;

	if ( specialPerm( sFileName, UID ) )   nPerm += S_ISUID;
	if ( specialPerm( sFileName, GID ) )   nPerm += S_ISGID;
	if ( specialPerm( sFileName, VTX ) )   nPerm += S_ISVTX;
	//qDebug("FileInfoExt::permission, sFileName= %s perm= %d", sFileName.toLatin1().data(), nPerm);

	return nPerm;
}


long long FileInfoExt::size( const QString & sFileName )
{
	struct stat statBuffer;

	if ( ::lstat(QFile::encodeName( sFileName ), & statBuffer) != 0 )  {
		qDebug("lstat error=%d, file=%s", errno, qPrintable(sFileName) );
		return -1;
	}

	return statBuffer.st_size;
}

