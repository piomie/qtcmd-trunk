/***************************************************************************
                          functions.h  -  description
                             -------------------
    begin                : mon sep 7 2003
    copyright            : (C) 2003 by Piotr Mierzwiński
    email                : peterm@go2.pl

    copyright            : See COPYING file that comes with this project
 ***************************************************************************/

#ifndef FUNCTIONS_H
#define FUNCTIONS_H

#include "../../enums.h"

static QString formatNumber( long long , KindOfFormating kof=NONEformat, uint precision=2 );
static void formatNumberStr( QString & , KindOfFormating , uint precision=2 );


QString formatNumber( long long num, KindOfFormating kindOfFormating, uint precision )
{
	// Qt-3.1.x not supports long long !
//	QString numStr = QString("%1").arg((double)num, 0, 'f');
//	numStr = numStr.left( sizeStr.find('.') ); // get everything to a dot
	QString numStr = QString::number( num );

	formatNumberStr( numStr, kindOfFormating, precision );

	return numStr;
}

void formatNumberStr( QString & numStr, KindOfFormating kindOfFormating, uint precision )
{
	if ( numStr[0] == '<' || kindOfFormating == NONEformat )
		return;

	// make (max) three characters group of number, eg. input = 12345,  output =12 345 )
	if ( kindOfFormating == THREEDIGITSformat )  {
		uint spacePos = numStr.length();
		while ( spacePos > 3 )  {
			spacePos -= 3;
			numStr.insert( spacePos, " " );
		}
		numStr += " B";
	}
	else  // make number by adds B, KB, MB, GB, with precision 2
	if ( kindOfFormating == BKBMBGBformat )
	{
		double num = numStr.toDouble();

		if ( num < 1024 )
			numStr += " B";
		else
		if ( num < 1048576 )  {
			num /= 1024;
			numStr = QString("%1 KB").arg( num, 0, 'f', precision );
		}
		else
		if ( num < 1073741824 )  {
			num /= 1048576;
			numStr = QString("%1 MB").arg( num, 0, 'f', precision );
		}
		else
		if ( num > 1073741824 )  {
			num /= 1073741824;
			numStr = QString("%1 GB").arg( num, 0, 'f', precision );
		}
	}
}

#endif
