/***************************************************************************
                          urlinfoext.cpp  -  description
                             -------------------
    begin                : tue oct 14 2003
    copyright            : (C) 2003 by Piotr Mierzwiński
    email                : peterm@go2.pl

    copyright            : See COPYING file that comes with this project

 $Id$
 ***************************************************************************/

#include "fileinfoext.h"
#include "urlinfoext.h"

#include <qfileinfo.h>
#include <sys/stat.h> // for S_ISUID, S_ISGID, S_ISVTX

const UrlInfoExt UrlInfoExt::null = UrlInfoExt();


UrlInfoExt::UrlInfoExt( const QString & sName )
{
	clear();
	if (! sName.isEmpty())
		setName(sName);
}
// TODO add below constructor
//UrlInfoExt::UrlInfoExt( const QString & sName, long long nSize, bool bDir, bool bFile, bool bSymLink )

UrlInfoExt::UrlInfoExt( const QString & sName, long long nSize, int nPermissions, const QString & sOwner, const QString & sGroup, const QDateTime & dtLastModified, const QDateTime & dtLastRead, bool bDir, bool bFile, bool bSymLink, bool bReadable, bool bExecutable, bool bEmpty, const QString & sLinkTarget )
 : QUrlInfo(sName, nPermissions, sOwner, sGroup, (uint)nSize, dtLastModified, dtLastRead, bDir, bFile, bSymLink, FALSE, bReadable, bExecutable),
   m_nSize(nSize), m_bEmptyFile(bEmpty), m_dtLastRead(dtLastRead), m_sLinkTargetFile(sLinkTarget)
{  }


QString UrlInfoExt::name( bool bAddSlashWhenDir ) const
{
	QString sName = QUrlInfo::name();

	if (bAddSlashWhenDir && isDir()) {
		if (sName.isEmpty() || ! sName.startsWith('/'))
			sName += "/";
	}

	return sName;
}

void UrlInfoExt::setName( const QString & sName )
{
	setDir(sName.endsWith("/"));

	QUrlInfo::setName(FileInfoExt::fileName(sName));

	if (name().isEmpty()) {
		qDebug("UrlInfoExt::setName(). WARNING: name is empty! Set name to \"/\" and dir status.");
		QUrlInfo::setName("");
		m_sLocation = "/";
		setDir(TRUE);
		return;
	}

	setLocation(sName);
}


void UrlInfoExt::setLocation( const QString & sPath, bool bUseFilePath )
{
	if (bUseFilePath)
		m_sLocation = FileInfoExt::filePath(sPath);
	else
		m_sLocation = sPath;
}


QString UrlInfoExt::permissionsStr() const
{
	int nPerm = permissions();
	QString sPermission = "---------";

	int permTab[9] = {
		QUrlInfo::ReadOwner, QUrlInfo::WriteOwner, QUrlInfo::ExeOwner,
		QUrlInfo::ReadGroup, QUrlInfo::WriteGroup, QUrlInfo::ExeGroup,
		QUrlInfo::ReadOther, QUrlInfo::WriteOther, QUrlInfo::ExeOther
	};
	char permChrTab[3] = { 'r','w','x' };

	for (int i=0; i<9; i++) {
		if ( nPerm & permTab[i] )
			sPermission[i] = permChrTab[i%3];
	}

	if ( (nPerm & S_ISUID) )  sPermission[2] = (nPerm & QFile::ExeUser)  ? 's' : 'S';
	if ( (nPerm & S_ISGID) )  sPermission[5] = (nPerm & QFile::ExeGroup) ? 's' : 'S';
	if ( (nPerm & S_ISVTX) )  sPermission[8] = (nPerm & QFile::ExeOther) ? 't' : 'T';
	//qDebug("UrlInfoExt::permissionsStr(): name= %s, perm= %d", name().toLatin1().data(), nPerm);

	return sPermission;
}


void UrlInfoExt::setPermissionsStr( const QString & sPermission )
{
	int nPerm = 0;

	if ( sPermission.at(0) == 'r' )    nPerm += QFile::ReadUser;
	if ( sPermission.at(3) == 'r' )    nPerm += QFile::ReadGroup;
	if ( sPermission.at(6) == 'r' )    nPerm += QFile::ReadOther;
	if ( sPermission.at(1) == 'w' )    nPerm += QFile::WriteUser;
	if ( sPermission.at(4) == 'w' )    nPerm += QFile::WriteGroup;
	if ( sPermission.at(7) == 'w' )    nPerm += QFile::WriteOther;
	if ( sPermission.at(2) == 'x' || sPermission.at(2) == 's' )  nPerm += QFile::ExeUser;
	if ( sPermission.at(5) == 'x' || sPermission.at(5) == 's' )  nPerm += QFile::ExeGroup;
	if ( sPermission.at(8) == 'x' || sPermission.at(8) == 't' )  nPerm += QFile::ExeOther;

	if ( sPermission.at(2).toLower() == 's' )  nPerm += S_ISUID;
	if ( sPermission.at(5).toLower() == 's' )  nPerm += S_ISGID;
	if ( sPermission.at(8).toLower() == 't' )  nPerm += S_ISVTX;

	QUrlInfo::setPermissions( nPerm );
}


void UrlInfoExt::clear()
{
	setName("/");
	setSize(-1);
	setOwner("");
	setGroup("");
	QDateTime nullDT;
	setLastModified(nullDT);
	setLastRead(nullDT); // lack in QUrlInfo
	setPermissions(-1);

	setDir(FALSE);
	setFile(FALSE);
	setSymLink(FALSE);
	setReadable(FALSE);
	setWritable(FALSE);
	m_bEmptyFile = FALSE;
}


UrlInfoExt & UrlInfoExt::operator=( const UrlInfoExt & ui )
{
	setName(ui.name());
	m_nSize = ui.size();
	setOwner(ui.owner());
	setGroup(ui.group());
	setLastModified(ui.lastModified());
	setLastRead(ui.lastRead());
	setPermissions(ui.permissions());
	setLocation(ui.location());

	setDir(ui.isDir());
	setFile(ui.isFile());
	setSymLink(ui.isSymLink());
	setReadable(ui.isReadable());
	setWritable(ui.isWritable());
	setLinkTargetFile(ui.readLink());

	m_bEmptyFile = ui.isEmpty();

	return *this;
}


