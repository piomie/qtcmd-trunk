/***************************************************************************
                          urlinfoext.h  -  description
                             -------------------
    begin                : tue oct 14 2003
    copyright            : (C) 2003 by Piotr Mierzwi�ski
    email                : peterm@go2.pl

    copyright            : See COPYING file that comes with this project

 $Id$
 ***************************************************************************/

#ifndef _URLINFOEXT_H_
#define _URLINFOEXT_H_

#include "enums.h"
#include "functions.h" // for fun.: formatNumber()

#include <QUrlInfo>
/**
 @author Piotr Mierzwi�ski
 */
/** @short Klasa rozszerzonej informacji o pliku.
 Klasa udost�pnia dodatkowe metody wzgl�dem klasy po kt�rej dziedziczy. Omini�te
 zosta�o ograniczenie tej klasy dla plik�w wi�kszych ni� 2GB. Dost�pne s�
 dodatkowe metody zwracaj�ce sformatowany (np. wg wagi) rozmiar pliku, oraz
 czas ostatniej modyfikacji. Zapisywane jest tu wskazanie linka i informacja
 o pustym katalogu.
 */
class UrlInfoExt : public QUrlInfo
{
public:
	/** Pierwszy konstruktor klasy.
	 @param sName - nazwa,
	 @param nSize - rozmiar,
	 @param nPermissions - parawa dost�pu,
	 @param sOwner - nazwa w�a�ciciela,
	 @param sGroup - nazwa groupy,
	 @param dtLastModified - data i czas ostatniej modyfikacji,
	 @param dtLastRead - data i czas ostatniego odczytu,
	 @param bDir - TRUE mowi, �e element to katalog
	 @param bFile - TRUE oznacza, �e element jest to plikiem,
	 @param bSymLink - warto�� TRUE m�wi, �e element to link symboliczny,
	 @param bReadable - TRUE oznacza, �e plik lub katalog jest odczytywalny
	 @param bExecutable - TRUE mowi, �e plik lub katalog jest uruchamialny,
	 @param bEmpty - je�li element jest katalogiem i znaje si� na partycji nie FAT,
	  wtedy TRUE oznacza �e jest on, pusty, w przeciwnym razie mo�e zawiera� pliki
	 @param sLinkTarget - je�li element jest linkiem wtedy znajduje si� tutaj
	  jego dowi�zanie.
	 Inicjowane s� tu sk�adowe klasy.
	 */
	UrlInfoExt( const QString & sName, long long nSize, int nPermissions, const QString & sOwner, const QString & sGroup, const QDateTime & dtLastModified, const QDateTime & dtLastRead, bool bDir, bool bFile, bool bSymLink, bool bReadable, bool bExecutable, bool bEmpty=FALSE, const QString & sLinkTarget=QString::null );

	/** Drugi konstruktor klasy.
	 Domy�lnie tworzony jest tu pusty obiekt klasy.
	 @param sName - je�li nie jest pusta wtedy obiekt b�dzie mia� ustawion�: nazw�, lokalizacje i status bycia katalogiem.
	 */
	UrlInfoExt( const QString & sName=QString::null );

	/** Destruktor.
	 Brak definicji (jest pusta).
	 */
	virtual ~UrlInfoExt() {}

	static const UrlInfoExt null;

	/** Ustawia nazw� pliku.
	 @param sName - nowa nazwa
	Przy czym je�li w nazwie na ko�cu znajduje si� znak '/' to dodatkowo jest ustawiany
	atrybur isDir.
	*/
	void setName( const QString & sName );

	/** Zwraca nazw� pliku.
	 @param bAddSlashWhenDir - TRUE wymusza dodanie na ko�cu nazwy znaku slash,
	 ale tylko je�li plik jest katalogiem.
	 @return nazwa pliku.
	 */
	QString name( bool bAddSlashWhenDir=TRUE ) const;

	/** Ustawia prawa dost�pu podane jako ci�g tekstowy.
	 @param sPermission - prawa dost�pu w formacie 'rwxrwxrwx',
	przy czym '-' oznacza nie ustawione prawo.
	 */
	void setPermissionsStr( const QString & sPermission );

	/** Zwraca rozmiar pliku.
	 @return rozmiar w bajtach.
	 */
	long long size() const { return m_nSize; }

	/** Ustawia rozmiar na podany.
	 @param nSize - rozmiar w bajtach.
	 */
	void setSize( long long nSize ) { m_nSize = nSize; }


	/** Zwraca prawa dost�pu w postaci ci�gu tekstowego.
	 @return prawa dost�pu.
	 */
	QString permissionsStr() const;

	/** Zwraca sformatowany rozmiar pliku, wg podanej regu�y.
	 @param kof - rodzaj formatowania, patrz: @see KindOfFormating.
	 */
	QString sizeStr( KindOfFormating eKoF=NONEformat ) const { return formatNumber(m_nSize, eKoF); }

	/** Zwraca czas ostatniej modyfikacji w postaci sformatowanego ci�gu
	 tekstowego.
	 @return czas ostatniej modyfikacji.
	 */
	QString lastModifiedStr() const { return lastModified().toString( "yyyy-MM-dd hh:mm" ); }


	/** Zwraca informacj� o tym czy katalog jest pusty.
	 @return TRUE, je�li katalog jest pusty, w przeciwnym razie FALSE.
	 Przy czym zwracana jest prawda tylko je�li, element jest katalogiem i
	 znajduje si� on na Linuksowej partycji.
	 */
	bool isEmpty() const { return m_bEmptyFile; }

// 	void setEmpty( bool empty ) { m_bEmptyFile = empty; }


	/** Zwraca informacj� o tym czy element jest uruchamialny.
	 @return TRUE, je�li element jest uruchamialny, w przeciwnym razie FALSE.
	 */
	bool isExecutable() const { return (permissions() & 0111); }

//	bool specialFile( int specialFile ) const { return (mSpecialFile & specialFile); }
//	bool specialPermission( int specialPermission ) const { return (permissions() & specialPermission); }
//	bool isSymLinkToFile() const { return (isSymLink() && isFile()); }
//	bool isSymLinkToDir() const  { return (isSymLink() && isDir());  }


	/** Metoda zeruje element.
	 */
	void clear();


	/** Zwraca czas ostatniego odczytu.
	 @param czas ostatniego odczytu.
	 */
	QDateTime lastRead() const { return m_dtLastRead; }

	/** Ustawia czas ostatniego odczytu na podany.
	 @param. - czas ostatniego odczytu
	 */
	void setLastRead( const QDateTime & dtLastRead ) { m_dtLastRead = dtLastRead; }


	/** Zwraca dowi�zanie linka (je�li element jest linkiem).dtLastRead
	 @return dowi�zanie linka.
	 */
	QString readLink() const { return m_sLinkTargetFile; }

	/** Ustawia plik docelowy dla linku.
	 @param sTargetFile - nazwa docelowego pliku ze �cie�k� absolutn� dla linku.
	 */
	void setLinkTargetFile( const QString & sTargetFile ) { m_sLinkTargetFile = sTargetFile; };


	/** Zwraca absolutn� scie�k� w systemie plik�w.
	 @return �cie�ka pliku/katalogu.
	 */
	QString location() const { return m_sLocation; }

	/** Ustawia absolutn� �cie�k� dla pliku/katalogu w systemie plik�w.
	 @param sPath - nowa �cie�ka dla pliku/katalogu
	 @param bUseFilePath - rowne TRUE powoduje uruchomienie metody: FileInfoExt::filePath(sPath), w przeciwnym razie przypisuje tylko warto��
	 */
	void setLocation( const QString & sPath, bool bUseFilePath=TRUE );

	/** Umo�liwia wykonanie operacji przypisania pomi�dzy obiektami bie��cej klasy.
	 @param ui - adres obiektu UrlInfoExt.
	 */
	UrlInfoExt & operator=( const UrlInfoExt & ui );

private:
	long long m_nSize;
	bool m_bEmptyFile;
//	int mSpecialFile;

	QDateTime m_dtLastRead;
	QString m_sLinkTargetFile;
	QString m_sLocation;

};


#endif
