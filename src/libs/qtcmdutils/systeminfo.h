/***************************************************************************
                          systeminfo.h  -  description
                             -------------------
    begin                : sun sep 12 2004
    copyright            : (C) 2004 by Piotr Mierzwiński
    email                : peterm@go2.pl

    copyright            : See COPYING file that comes with this project
 ***************************************************************************/

#ifndef _SYSTEMINFO_H_
#define _SYSTEMINFO_H_

#include <qstringlist.h>

class QString;

namespace SystemInfo {


bool getStatFS( const QString & path, long long & freeBytes, long long & allBytes );

unsigned int getCurrentUserID();

QString getCurrentUserName();

QString getUserName(unsigned int userId);

unsigned int getCurrentUserGroupID();

unsigned int getCurrentGroupForUser(unsigned int userId);

QString getCurrentGroupName();

QString getGroupName(unsigned int grpId);

QStringList getAllUsers();

QStringList getAllUserGroups(unsigned int userId);

QStringList getAllGroups();

} // namespace SystemInfo

#endif
