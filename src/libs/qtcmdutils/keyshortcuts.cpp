/***************************************************************************
                          keyshortcuts.cpp  -  description
                             -------------------
    begin                : sun aug 1 2004
    copyright            : (C) 2004 by Piotr Mierzwi�ski
    email                : peterm@go2.pl

    copyright            : See COPYING file that comes with this project
 ***************************************************************************/
#include <QtGui>
//#include <QSettings> TODO add a support for QSettings

#include "keyshortcuts.h"


KeyShortcuts::KeyShortcuts()
{
	m_KeyCodeList.clear();
}


void KeyShortcuts::updateEntryList( const QString & sConfigFileKey )
{
	if ( ! m_KeyCodeList.count() )
		return;

	// --- try to read shortcuts from config file
	//QSettings *settings = new QSettings;
	QString sNewKey, sDefaultKey;
	KeyCodeList::iterator it;
	int k;

	for (it = m_KeyCodeList.begin(); it != m_KeyCodeList.end(); ++it) {
		sDefaultKey = QKeySequence( (*it)->defaultCode() ).toString();
		sNewKey     = ""; //settings->readEntry( sConfigFileKey+(*it)->description(), sDefaultKey );
		if (sNewKey.isEmpty())
			sNewKey = sDefaultKey;

		k = QKeySequence(sNewKey); // below need to check combination too
		if ( k == Qt::SHIFT || k == Qt::ALT || k == Qt::CTRL ) // occured a wrong value
			sNewKey = sDefaultKey;

		(*it)->code = QKeySequence(sNewKey);
	}
	//delete settings;
}


int KeyShortcuts::key( int nAction )
{
	int nKeyCode = Qt::Key_unknown;

	KeyCodeList::iterator it;
	for (it = m_KeyCodeList.begin(); it != m_KeyCodeList.end(); ++it) {
		if ( (*it)->action() == nAction ) {
			nKeyCode = (*it)->code;
			break;
		}
	}
	if ( it == m_KeyCodeList.end() )
		qDebug() << "KeyShortcuts::key, Not found a passed action='" << nAction << "'";

	return nKeyCode;
}


QString KeyShortcuts::keyStr( int nIndex ) const
{
	if ( nIndex < 0 || nIndex >= m_KeyCodeList.size() )
		return QString("none");

	int nKeyCode = (*m_KeyCodeList.at( nIndex )).code;

	return (nKeyCode == Qt::Key_unknown) ? QString("none") : QString(QKeySequence( nKeyCode ));
}


QString KeyShortcuts::keyDefaultStr( uint nIndex ) const
{
	return QKeySequence((*m_KeyCodeList.at( nIndex )).defaultCode());
}


void KeyShortcuts::setKey( int nAction, int nShortcut )
{
	KeyCodeList::iterator it;
	for (it = m_KeyCodeList.begin(); it != m_KeyCodeList.end(); ++it) {
		if ( (*it)->action() == nAction ) {
			(*it)->code = nShortcut;
			break;
		}
	}
	if ( it == m_KeyCodeList.end() )
		qDebug() << "KeyShortcuts::setKey, Not found a passed action='" << nAction << "'";
}


void KeyShortcuts::setKey( const QString & sDescription, int shortcut )
{
	KeyCodeList::iterator it;
	for (it = m_KeyCodeList.begin(); it != m_KeyCodeList.end(); ++it) {
		if ( (*it)->description() == sDescription ) {
			(*it)->code = shortcut;
			break;
		}
	}
	if ( it == m_KeyCodeList.end() )
		qDebug() << "KeyShortcuts::setKey, Not found a passed description='" << sDescription << "'!";
}


int KeyShortcuts::ke2ks( QKeyEvent * pKeyEvent )
{
	int key   = pKeyEvent->key();
	//int state = pKeyEvent->state();
	QString sKey = pKeyEvent->text();
	bool bKeysException = (key == Qt::Key_Space || key == Qt::Key_Backspace || key == Qt::Key_Delete || key == Qt::Key_Enter || key == Qt::Key_Return);
	int ksk = (sKey.isEmpty() || bKeysException) ? (int)QKeySequence(key) : (int)QKeySequence(sKey);
	//int keToKs = Qt::UNICODE_ACCEL + key + ((state & 0x0FFF)*0x2000);
	int keToKs = Qt::UNICODE_ACCEL + key + pKeyEvent->modifiers();
	if ( ksk == key ) // QKeySequence returns no UNICODE_ACCEL type
		keToKs -= Qt::UNICODE_ACCEL;

	return keToKs;
}
