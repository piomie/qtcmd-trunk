/***************************************************************************
                          functions_col.h  -  description
                             -------------------
    begin                : wed mar 31 2004
    copyright            : (C) 2003 by Piotr Mierzwiński
    email                : peterm@go2.pl

    copyright            : See COPYING file that comes with this project
 ***************************************************************************/

#ifndef FUNCTIONSCOL_H
#define FUNCTIONSCOL_H

#include <qcolor.h>

static QColor color( const QString & hexColorStr, const QString & defaultHexColorStr="000000" );
static QString colorToString( const QColor & color );

QColor color( const QString & hexColorStr, const QString & defaultHexColorStr ) // TODO zrezygnowac z fun. color, bo w Qt4 QColor ma konstruktor z param QString
{
	QString colorStr = hexColorStr;
	bool okRed, okGreen, okBlue;

	if ( hexColorStr.length() < 6 || hexColorStr.length() > 6 )
		colorStr = defaultHexColorStr;

	int red   = (colorStr.left( 2 )).toInt( &okRed, 16 );
	int green = (colorStr.mid( 2,2 )).toInt( &okGreen, 16 );
	int blue  = (colorStr.right( 2 )).toInt( &okBlue, 16 );

	if ( ! okRed || ! okGreen || ! okBlue ) {
		colorStr = defaultHexColorStr;
		red   = colorStr.left( 2 ).toInt( &okRed, 16 );
		green = colorStr.mid( 2,2 ).toInt( &okGreen, 16 );
		blue  = colorStr.right( 2 ).toInt( &okBlue, 16 );
	}

	return QColor( red, green, blue );
}


QString colorToString( const QColor & color ) // TODO zrezygnowac z fun. colorToString , bo w Qt4 jest met. name() dla QColor
{
	QString redStr, greenStr, blueStr;

	redStr.sprintf( "%.2X", color.red() );
	greenStr.sprintf( "%.2X", color.green() );
	blueStr.sprintf( "%.2X", color.blue() );

	QString colorStr = redStr+greenStr+blueStr;

	return colorStr;
}

#endif
