
/***************************************************************************
                          url  -  description
                             -------------------
    begin                : pon lis 29 2004
    copyright            : (C) 2004 by Piotr Mierzwiński
    email                : peterm@go2.pl

    copyright            : See COPYING file that comes with this project

 $Id$
 ***************************************************************************/

#include "url.h"
#include "fileinfoext.h"


URL::URL( const QString & sURL )
{
	setUrl(sURL);
}


void URL::setPath( const QString & sPath )
{
	m_sPath = sPath;
	if ( sPath.at(sPath.length()-1) != '/' )
		m_sPath += "/";
	if ( sPath.at(0) != '/' )
		m_sPath.insert(0, "/");
}

void URL::setUrl( const QString & sURL )
{
	m_nPort = 0;
	m_sUser = "";
	m_sPassword = "";
	m_sURL = "";
	m_bParseError = FALSE;

	if ( sURL.at(0) == '/' ) { // Local File System or ArchiveFS
		m_sHost = "/";
		if ( sURL.at(sURL.length()-1) == '/' ) {
			m_sPath = sURL;
			m_sURL  = sURL;
			return;
		}
		// sURL this is file name
		m_sFileName = sURL;
	}
	else
	if ( sURL.startsWith("ftp://") || sURL.startsWith("sftp://") ) { // FTP
		enum { LOGIN=0, PASS_AND_HOST, PORT_AND_PATH };
		QString sUrl = sURL;
		sUrl.remove(0, sURL.indexOf("//")+2); // remove protocol name
		QStringList slUrl = sUrl.split(':');
		//qDebug("- sURL= %s, slUrl.size()=%d", sUrl.toLatin1().data(), slUrl.size());
		QString sPort;
		if (slUrl.size() == 3) { // 3 part of URL separate by colons
			//qDebug() << "-  sURL= " << sURL;
			m_sUser = slUrl.at(LOGIN);
			//qDebug() << "-  m_sUser= " << m_sUser;
			m_sHost = slUrl.at(PASS_AND_HOST);
			m_sHost = m_sHost.right(m_sHost.length()-m_sHost.lastIndexOf('@')-1);
			//qDebug() << "- m_sHost= " << m_sHost;
			m_sPassword = slUrl.at(PASS_AND_HOST);
			m_sPassword = m_sPassword.left(m_sPassword.length()-m_sHost.length()-1);
			//qDebug() << "- m_sPassword= " << m_sPassword;
			sPort = slUrl.at(PORT_AND_PATH);
			sPort = sPort.left(sPort.indexOf('/'));
			m_nPort = sPort.toInt();
			//qDebug() << "- sPort= " << sPort;
			m_sPath = slUrl.at(PORT_AND_PATH);
			m_sPath = m_sPath.right(m_sPath.length()-sPort.length());
			//qDebug() << "- m_sPath= " << m_sPath;
		}
		else {
			qDebug("URL::setUrl, Url parse error for: %s", sURL.toLatin1().data());
			m_bParseError = TRUE;
		}
	}
	else
	if (sURL.indexOf("smb://") == 0) { // Samba
		// parse the URL
		;
	}
	else
	if (sURL.indexOf("nfs://") == 0) { // NFS
		// parse the URL
		;
	}
	else
	if (sURL.indexOf("sfs://") == 0) { // Shell FS  (connect by shell - ssh connection)
		// parse the URL
		;
	}

	if (FileInfoExt::isArchive(sURL)) {
		//m_sUser = ; // wlasciciel pliku
		//m_sPath = ; // sciezka wewnatrz archiwum
		//m_sHost = ; // sciezka w FSie, na ktorym lezy archiwum + nazwa pliku archiwum
	}

	if (! m_bParseError)
		m_sURL = sURL;
}

