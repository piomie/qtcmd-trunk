/***************************************************************************
                          url  -  description
                             -------------------
    begin                : pon lis 29 2004
    copyright            : (C) 2004 by Piotr Mierzwiński
    email                : peterm@go2.pl

    copyright            : See COPYING file that comes with this project
 ***************************************************************************/

#ifndef URL_H
#define URL_H

#include <QString>

/**
	@author Piotr Mierzwiński
*/
class URL
{
public:
	URL( const QString & sURL );
	~URL() {}

	QString host() const      { return m_sHost; } // ftp://nazwa_hosta
	QString path() const      { return m_sPath; } // katalog na hoscie
	QString user() const      { return m_sUser; }
	QString password()  const { return m_sPassword; }
	QString directory() const { return m_sPath; } // katalog na hoscie
	QString fileName()  const { return m_sFileName; } // plik na hoscie (ze sciezka bez nazwy hosta)
	QString url() const       { return m_sURL;  } // pelna sciezka do katalogu lub pliku, tj.: ftp://nazwa_hosta/sciezka
	uint   port() const       { return m_nPort; } // port

	void setPath( const QString & sPath );
	void setUrl( const QString & sURL );
	void setPort( uint port ) { m_nPort = port; }
	void setFileName( uint fileName ) { m_sFileName = fileName; }
	void setUser( const QString & user ) { m_sUser = user; }
	void setPassword( const QString & password ) { m_sPassword = password; }

	bool parseError() const { return m_bParseError; }

private:
	QString m_sHost, m_sUser, m_sPassword, m_sPath, m_sURL, m_sFileName;
	bool m_bParseError;
	uint m_nPort;

};

#endif
