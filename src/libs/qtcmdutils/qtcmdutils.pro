# Plik utworzony przez menad?era projekt�w kdevelopa
# ------------------------------------------- 
# Podkatalog wzgl?dem g?�wnego katalogu projektu: ./src/libs/qtcmdutils
# Cel to biblioteka qtcmdutils

INSTALLS += target 
target.path = $$PREFIX/lib 
MOC_DIR = ../../../build/.tmp/moc 
UI_DIR = ../../../build/.tmp/ui 
OBJECTS_DIR = ../../../build/.tmp/obj 
QMAKE_CXXFLAGS_RELEASE += -O2 
QMAKE_CXXFLAGS_DEBUG += -g3 
TARGET = qtcmdutils 
DESTDIR = ../../../build/lib
INCLUDEPATH += \
				../../
CONFIG += warn_on \
          qt \
          thread \
          dll \
          precompile_header 
TEMPLATE = lib 
TRANSLATIONS += ../../../translations/pl/qtcmd-utils.ts 
HEADERS += filters.h \
           filesassociation.h \
           keyshortcuts.h \
           urlinfoext.h \
           fileinfoext.h \
           functions_col.h \
           functions.h \
           systeminfo.h \
           url.h 
SOURCES += filters.cpp \
           filesassociation.cpp \
           keyshortcuts.cpp \
           urlinfoext.cpp \
           fileinfoext.cpp \
           extentions.cpp \
           systeminfo.cpp \
           url.cpp 
QT += network
