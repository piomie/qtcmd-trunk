/***************************************************************************
                          filters.h  -  description
                             -------------------
    begin                : sun aug 15 2004
    copyright            : (C) 2004 by Piotr Mierzwiński
    email                : peterm@go2.pl

    copyright            : See COPYING file that comes with this project
 ***************************************************************************/

#ifndef _FILTERS_H_
#define _FILTERS_H_

#include <QDir> // for DirFilter
#include <qobject.h>
#include <qstringlist.h>

#include "filesassociation.h"

/**
	@author Piotr MierzwiDski
 */
class Filters : public QObject
{
public:
	Filters( FilesAssociation *pFA=NULL );
	virtual ~Filters() {}

	uint baseFiltersNum() const { return m_nBaseFiltersNum; }

	uint defaultId() const { return m_nDefaultFilterId; }
	uint count() const { return m_slFiltersList.count(); }

	QString name( uint nId ) const;
	QString patterns( uint nId ) const;
	QString patterns( const QString & sFilterName ) const;

	void add( const QString & sName, const QString & sPatterns );
	void remove( int nId );

	void updateByFilesAssociation( FilesAssociation *pFA );
	void updateFilters();


	enum DirFilter {
		AllFiles     =  0x007 | 0x200 | 0x100, //QDir::AllEntries | QDir::System | QDir::Hidden
		DirsOnly     = 0x001 | 0x100, //QDir::Dirs  | QDir::Hidden
		FilesOnly    = 0x002 | 0x100, //QDir::Files | QDir::Hidden
		HiddenNot    = 0x007 | 0x200, //QDir::AllEntries | QDir::System
		SymLinks     = 1015,
		HangSymLinks = 1016
	};

private:
	QStringList m_slFiltersList;
	FilesAssociation *m_pFilesAssociation;

	int m_nDefaultFilterId, m_nBaseFiltersNum;

};

#endif
