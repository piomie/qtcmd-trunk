/***************************************************************************
                          filesassociation.cpp  -  description
                             -------------------
    begin                : wed may 12 2004
    copyright            : (C) 2004 by Piotr Mierzwiński
    email                : peterm@go2.pl

    copyright            : See COPYING file that comes with this project
 ***************************************************************************/

#include <QFile>
// #include <QSettings> // TODO port QSettings support
#include <QTextStream>


#include "filesassociation.h"

FilesAssociation::FilesAssociation()
{
	m_bFilesAssociationLoaded = FALSE;

	// --- read all types of file
	QStringList slFileTypesList;
	QFile f( QDir::homePath()+"/.qt/qtcmd_mimerc" );
	if ( f.exists() ) {
		if ( f.open( QIODevice::ReadOnly ) )  {
			QString sLineBuffer;
			QTextStream t( &f );        // use a text stream
			while ( ! t.atEnd() )  {        // until end of file...
				sLineBuffer = t.readLine();       // line of text excluding '\n'
				if ( ! sLineBuffer.isEmpty() )
					if ( sLineBuffer[0] == '[' )
						slFileTypesList.append( sLineBuffer.mid(1, sLineBuffer.indexOf(']')-1) );
			}
			f.close();
		}
	}
	else
		return;

	// --- read all FilesAssociation for each type of file
	//     and append it to the FilesAssociationList
	QString sFileType, sKindOfFile;
	FileAssociation pFA;
	qDebug("---FilesAssociation::FilesAssociation(), QSettings not yet supported");
	/*
	bool bExternalViewer;
	QSettings settings;

	for ( int i=0; i<slFileTypesList.count(); i++ ) {
		sFileType = slFileTypesList[i];

		pFA.fileType   = sFileType;
		pFA.appsForView = settings.readEntry( "qtcmd_mime/"+sFileType+"/Application", "" );
		pFA.description = settings.readEntry( "qtcmd_mime/"+sFileType+"/Description", "" );
		pFA.templates   = settings.readEntry( "qtcmd_mime/"+sFileType+"/FileTemplates", "" );
		bExternalViewer  = settings.readBoolEntry( "qtcmd_mime/"+sFileType+"/ExternalViewer", FALSE );
		sKindOfFile     = settings.readEntry( "qtcmd_mime/"+sFileType+"/KindOfFile", "Unknown" );

		pFA.kindOfViewer = (bExternalViewer) ? EXTERNALviewer : INTERNALviewer;
		pFA.kindOfFile   = kindOfFileFromName( sKindOfFile );

		m_FilesAssociationList.append( pFA );
		if ( i == 1 ) {
			m_bFilesAssociationLoaded = TRUE;
			qDebug("FilesAssociation(), files association has been loaded");
		}
	}
	*/
}


KindOfFile FilesAssociation::kindOfFile( const QString & sFileName )
{
	if ( sFileName.isEmpty() || ! m_bFilesAssociationLoaded )
		return UNKNOWNfile;

	Iterator it = find( sFileName );
	KindOfFile kof = (it != m_FilesAssociationList.end()) ? (*it).kindOfFile : UNKNOWNfile;

	return kof;
}


QString FilesAssociation::fileTemplates( const QString & sFileType )
{
	if ( sFileType.isEmpty() || ! m_bFilesAssociationLoaded )
		return QString::null;

	QString sTemplates = "";
	bool bNotFoundFileType = TRUE;
	FilesAssociationList::iterator it;
	for (it = m_FilesAssociationList.begin(); it != m_FilesAssociationList.end(); ++it) {
		if ( (*it).fileType == sFileType ) {
			sTemplates = (*it).templates;
			bNotFoundFileType = FALSE;
		}
	}
	if ( bNotFoundFileType )
		qDebug("'%s' - unknown file type !", qPrintable(sFileType) );

	return sTemplates;
}


KindOfViewer FilesAssociation::kindOfViewer( const QString & sFileName )
{
	if ( sFileName.isEmpty() || ! m_bFilesAssociationLoaded )
		return UNKNOWNviewer;

	Iterator it = find( sFileName );
	KindOfViewer eKOV = (it != m_FilesAssociationList.end()) ? (*it).kindOfViewer : UNKNOWNviewer;

	return eKOV;
}


QString FilesAssociation::appsForView( const QString & sFileName )
{
	if ( sFileName.isEmpty() || ! m_bFilesAssociationLoaded )
		return QString::null;

	Iterator it = find( sFileName );
	QString sViewer = (it != m_FilesAssociationList.end()) ? (*it).appsForView : QString::null;

	return sViewer;
}


QString FilesAssociation::fileDescription( const QString & sFileName )
{
	if ( sFileName.isEmpty() || ! m_bFilesAssociationLoaded )
		return QString::null;

	Iterator it = find( sFileName );
	QString sDesc = (it != m_FilesAssociationList.end()) ? (*it).description : QString::null;

	return sDesc;
}


KindOfFile FilesAssociation::kindOfFileFromName( const QString & sKindOfFile )
{
	if ( sKindOfFile == "Source" )
		return SOURCEfile;
	else
	if ( sKindOfFile == "RenderableText" )
		return RENDERfile;
	else
	if ( sKindOfFile == "Image" )
		return IMAGEfile;
	else
	if ( sKindOfFile == "Sound" )
		return SOUNDfile;
	else
	if ( sKindOfFile == "Video" )
		return VIDEOfile;
// 	else
// 	if ( sKindOfFile == "Unknown" )
// 		return UNKNOWNfile;
//	else
		return UNKNOWNfile;
}


FilesAssociation::Iterator FilesAssociation::find( const QString & sFileName )
{
	if ( sFileName.isEmpty() || ! m_bFilesAssociationLoaded )
		m_FilesAssociationList.end();

	bool bFoundExt;
	QStringList slExtentionList;
	int findDot = sFileName.lastIndexOf( '.' );
	QString ext = (findDot > 0) ? sFileName.right(sFileName.length()-findDot-1).toLower() : QString("");

	Iterator it = m_FilesAssociationList.end();
	for (it = m_FilesAssociationList.begin(); it != m_FilesAssociationList.end(); ++it) {
		slExtentionList = ((*it).templates).split( "*." );
		bFoundExt = FALSE;
		for ( int i=0; i<slExtentionList.count(); i++ ) {
			if ( ext+", " == slExtentionList[i] || ext == slExtentionList[i] ) {
				bFoundExt = TRUE;
				break;
			}
		}
		if ( bFoundExt )
			break;
	}

	return it;
}

