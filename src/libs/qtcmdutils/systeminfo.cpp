/***************************************************************************
                          systeminfo.cpp  -  description
                             -------------------
    begin                : sun sep 12 2004
    copyright            : (C) 2004 by Piotr Mierzwi�ski
    email                : peterm@go2.pl

    copyright            : See COPYING file that comes with this project
 ***************************************************************************/

#include <sys/types.h>
#include <inttypes.h>
#include <sys/vfs.h> // or <sys/statfs.h>
#include <unistd.h>
#include <pwd.h>
#include <grp.h>

#include <QFile>
#include <QString>

#include "systeminfo.h"


namespace SystemInfo {


/** Funkcja pobiera informacj� o systemie plik�w dla podanego katalogu.\n
    @param path - nazwa katalogu w systemie plik�w do sprawdzenia,
    @param freeBytes - zmienna, w kt�rej nale�y umie�ci� ilo�� wolnych bajt�w,
    @param allBytes - zmienna, w kt�rej nale�y umie�ci� ilo�� wszystkich bajt�w.
    @return TRUE, je�li uda�o si� pobra� informacje, w przeciwnym razie FALSE.
*/
bool getStatFS(const QString & path, long long & freeBytes, long long & allBytes)
{
	struct statfs fsd;

	if (::statfs(QFile::encodeName(path), &fsd) != 0)
		return false;

	#define PROPAGATE_ALL_ONES(x) ((x) == -1 ? (uintmax_t) -1 : (uintmax_t) (x))
	const long blockSize = fsd.f_bsize;
	const long long freeBytesForNonSuperuser = PROPAGATE_ALL_ONES((long)fsd.f_bavail) * blockSize;
	const long long freeBytesForSuperuser    = PROPAGATE_ALL_ONES((long)fsd.f_bfree)  * blockSize;

	freeBytes = (::getuid()) ? freeBytesForNonSuperuser : freeBytesForSuperuser;
	allBytes  = PROPAGATE_ALL_ONES((long)fsd.f_blocks) * blockSize;
/*
	uint mbtotal = allBytes/(1024*1024);
	uint mbfree  = freeBytes/(1024*1024);
	qDebug("SoLF::getStatFS, totalMB=%d, totalFreeMB=%d, totalBytes=%lld, freeBytes=%lld",
		mbtotal, mbfree, allBytes, freeBytes );
*/
	return true;
}

////////////////////// REFACTORING ////////////////////////////////////

/**
 * @return real user ID of the current process
 */
unsigned int getCurrentUserID()
{
    return ::getuid();
}

/**
 * @return current user name
 */
QString getCurrentUserName()
{
    struct passwd *pw = ::getpwuid(::getuid());
    Q_CHECK_PTR(pw);
    return QString::fromLocal8Bit(pw->pw_name);
}

QString getUserName(unsigned int userId)
{
    struct passwd *pw = ::getpwuid(userId);
    Q_CHECK_PTR(pw);
    return QString::fromLocal8Bit(pw->pw_name);
}

/**
 * @return real user ID of the current process
 */
unsigned int getCurrentUserGroupID()
{
    return ::getgid();
}

unsigned int getCurrentGroupForUser(unsigned int userId)
{
	struct passwd *pw = ::getpwuid(userId);
	Q_CHECK_PTR(pw);
	return pw->pw_gid;
}

QString getCurrentGroupName()
{
	struct group *gr = ::getgrgid(::getgid());
	Q_CHECK_PTR(gr);
	return QString::fromLocal8Bit(gr->gr_name);
}

QString getGroupName(unsigned int grpId)
{
	struct group *gr = ::getgrgid(grpId);
	Q_CHECK_PTR(gr);
	return QString::fromLocal8Bit(gr->gr_name);
}

QStringList getAllUsers()
{
	QStringList list;
	::setpwent();
	struct passwd *pw = 0;
	while ((pw = ::getpwent()) != 0) {
		if (list.count() > 0)
			if (pw->pw_uid == 0)  // prevent to duplicate list
				break;
		list << QString::fromLocal8Bit(pw->pw_name);
	}
	::endpwent();
	return list;
}

QStringList getAllUserGroups(unsigned int userId)
{
	QStringList list;
	struct passwd *pw = ::getpwuid(userId); // for user name
	Q_CHECK_PTR(pw);
	::setgrent();
	struct group *gr = 0;
	while ((gr = ::getgrent()) != 0) {
		while (*gr->gr_mem) {
			if (::strcmp(pw->pw_name, *gr->gr_mem) == 0)
				list << QString::fromLocal8Bit(gr->gr_name);
		}
	}
	::endgrent();
	return list;
}

QStringList getAllGroups()
{
	QStringList list;
	::setgrent();
	struct group *gr = 0;
	while ((gr = ::getgrent()) != 0) {
		if (list.count() > 0)
			if (gr->gr_gid == 0) // prevent to duplicate list
				break;
		list << QString::fromLocal8Bit(gr->gr_name);
	}
	::endgrent();

	return list;
}

} // namespace SystemInfo
