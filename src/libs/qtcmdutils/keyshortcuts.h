/***************************************************************************
                          keyshortcuts.h  -  description
                             -------------------
    begin                : sun aug 1 2004
    copyright            : (C) 2004 by Piotr Mierzwi�ski
    email                : peterm@go2.pl

    copyright            : See COPYING file that comes with this project
 ***************************************************************************/
#ifndef _KEYSHORTCUTS_H_
#define _KEYSHORTCUTS_H_

#include <QList>
#include <QObject>
#include <QKeyEvent>

/**
	@author Piotr Mierzwi�ski <piotrmierzwinski@gmail.com>
 */
/** @short Klasa pomocnicza trzymaj�ca opis pojedynczego kodu klawiszowego.
 * Na opis skr�tu sk�adaj� si� nast�puj�ce rzeczy: identyfikator akcji, opis
 * akcji, domy�lny kod klawisza, aktualny kod klawisza. W celu utworzenia kodu
 * nale�y zainicjowa� konstruktor. Klasa udost�pnia niezb�dne metody do
 * pobierania i ustawiania warto�ci kodu, przy czym ustawi� mo�na tylko kod
 * klawiszowy.
 */
class KeyCode
{
public:
	/** Konstruktor.
	 * @param nActionId - identyfikator akcji,
	 * @param sDesc - opis akcji,
	 * @param nDefaultCode - domy�lny kod klawisza,
	 * @param nCode - w�a�ciwy kod klawisza.
	 * Inicjalizowane s� tutaj sk�adowe klasy.
	 */
	KeyCode( int nActionId, const QString & sDesc, int nDefaultCode, int nCode=Qt::Key_unknown ) :
		m_nAction(nActionId), m_sDescription(sDesc), m_nDefaultCode(nDefaultCode), code(nCode)
	{}

	/** Destruktor.
	 * Nie zawiera definicji.
	 */
	~KeyCode() {}

	/** Zwraca opis akcji.
	 * @return opis akcji.
	 */
	QString description() const { return m_sDescription; }

	/** Zwraca domy�lny kod klawisza.
	 * @return domy�lny kod klawisza.
	 */
	int defaultCode() const { return m_nDefaultCode; }

	/** Zwraca identyfikator akcji.
	 * @return identyfikator akcji.
	 */
	int action() const { return m_nAction; }

private:
	int m_nAction;
	QString m_sDescription;
	int m_nDefaultCode;
	//int m_nCode;

public:
	/** Aktualny kod klawisza.
	 */
	int code;

};

// ooooooooooo-----------         -------------ooooooooooo

/** @short Klasa obs�uguj�ca skr�ty klawiszowe.
 * Klasa przeznaczona do edycji skr�t�w klawiszowych u�ywanych w oknie podgl�du
 * oraz oknie g��wnym aplikacji. Trzymana jest tutaj lista powy�szych,
 * udost�pniane s� metody konieczne do pobierania i ustawiania skr�t�w. W
 * momencie inicjacji nast�puje pr�ba odczytania ich z pliku konfiguracyjnego,
 * je�li si� to nie uda przyjmowane s� domy�lne warto�ci.
*/
class KeyShortcuts : public QObject
{
	Q_OBJECT
public:
	/** Konstruktor.
	 * Czyszczona jest tutaj tylko lista skr�t�w.
	 */
	KeyShortcuts();

	/** Destruktor.
	 * Nie zawiera definicji.
	 */
	virtual ~KeyShortcuts() {}

	/** Powoduje uaktualnienie kod�w skr�t�w klawiszowych na podstawie klucza z
	 * pliku konfiguracyjnego.
	 * @param sConfigFileKey - klucz dla pliku konfiguracyjnego.
	 * Przy czym przed wywo�aniem tej funkcji nale�y wype�ni� list� domy�lnymi
	 * warto�ciami, patrz: @see append.
	 */
	virtual void updateEntryList( const QString & sConfigFileKey );

	/** Zwraca skr�t klawiszowy dla podanego jego identyfikatora akcji.
	 * @param nAction - identyfikator akcji dla skr�tu klawiszowego, (patrz te�:
	 * @see ActionsOfView, oraz @see ActionsOfApp),
	 * @return skr�t klawiszowy.
	 */
	virtual int key( int nAction );

	/** Metoda powoduje ustawienie podanego skr�tu klawiszowego dla podanego
	 * jego opisu.
	 * @param nAction - identyfikator akcji dla skr�tu klawiszowego, (patrz te�:
	 * @see ActionsOfView, oraz @see ActionsOfApp),
	 * @param nShortcut - nowy skr�t do ustawienia.
	 */
	virtual void setKey( int nAction, int nShortcut );

	/** Metoda powoduje ustawienie podanego skr�tu klawiszowego dla podanego
	 * jego identyfikatora akcji.
	 * @param sDescription - opis akcji dla skr�tu klawiszowego,
	 * @param nShortcut - nowy skr�t do ustawienia.
	 */
	virtual void setKey( const QString & sDescription, int nShortcut );


	/** Zwraca aktualn� ilo�� skr�t�w klawiszowych.
	 * @return liczba skr�t�w klawiszowych.
	 */
	uint count() const { return m_KeyCodeList.count(); }

	/** Metoda powoduje wyczyszczenie listy skr�t�w klawiszowych.
	 */
	void clear() { m_KeyCodeList.clear(); }

	/** Metoda umo�liwia dodanie skr�tu klawiszowego do listy.
	 * @param nAction - identyfikator akcji,
	 * @param sDescription - opis akcji,
	 * @param nDefaultKeyShortcut - domy�lny skr�t klawiszowy. Przy czym w przypadku
	 * kombinacji klawiszowych nale�y u�y� typu 'Qt::Modifier', np. Qt::CTRL.
	 */
	virtual void append( int nAction, const QString & sDescription, int nDefaultKeyShortcut=Qt::Key_unknown ) {
		m_KeyCodeList.append( new KeyCode(nAction, sDescription, nDefaultKeyShortcut) );
	}

	/** Wykonuje konwersje z typu QKeyEvent to QKeySequence i zwraca ten drugi.
	 * @param pKeyEvent - wska�nika na zdarzenie KeyEvent,
	 * @return zwraca skr�t klawiszowy typu QKeySequence.
	 */
	virtual int ke2ks( QKeyEvent *pKeyEvent );

	/** Zwraca ci�g tekstowy domy�lnego skr�tu klawiszowego dla podanego Id.
	 * @return ci�g tekstowy opisuj�cy skr�t klawiszowy.
	 */
	virtual QString keyDefaultStr( uint nIndex ) const;

	/** Zwraca ci�g tekstowy aktualnego skr�tu klawiszowego dla podanego Id.
	 * @return ci�g tekstowy opisuj�cy skr�t klawiszowy.
	 */
	virtual QString keyStr( int nIndex ) const;

	/** Zwraca opis akcji dla podanego Id.
	 * @return opis akcji.
	 */
	QString desc( uint nIndex ) const {
		return (*m_KeyCodeList.at( nIndex )).description();
	}

private:
	typedef QList <KeyCode *> KeyCodeList;
	KeyCodeList m_KeyCodeList;

};

#endif
