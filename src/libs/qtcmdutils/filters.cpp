/***************************************************************************
                          filters.cpp  -  description
                             -------------------
    begin                : sun aug 15 2004
    copyright            : (C) 2004 by Piotr Mierzwiński
    email                : peterm@go2.pl

    copyright            : See COPYING file that comes with this project
 ***************************************************************************/

#include "filters.h"

//#include <QSettings> // TODO port QSettings support


Filters::Filters( FilesAssociation *pFA )
{
	updateFilters();

	if ( pFA != NULL )
		m_pFilesAssociation = pFA;

	updateByFilesAssociation( m_pFilesAssociation );
}


QString Filters::name( uint nId ) const
{
	return (nId > count()-1) ? QString::null : m_slFiltersList[nId].section( '=', 0,0 );
}


QString Filters::patterns( uint nId ) const
{
	return (nId > count()-1) ? QString::null : m_slFiltersList[nId].section( '=', -1 );
}


QString Filters::patterns( const QString & sName ) const
{
	int nFilterId = -1;
	for (int i=0; i<m_slFiltersList.count(); i++) {
		if ( m_slFiltersList[i].section('=', 0,0) == sName ) {
			nFilterId = i;
			break;
		}
	}
	if ( nFilterId < 0 ) {
		qDebug("not found filter about name=%s", qPrintable(sName) );
		return QString::null;
	}

	return m_slFiltersList[nFilterId].section( '=', -1 );
}


void Filters::add( const QString & sName, const QString & sPatterns )
{
	int nFilterId = -1; // default not found
	for (int i=0; i<m_slFiltersList.count(); i++) {
		if ( m_slFiltersList[i].section('=', 0,0) == sName ) {
			nFilterId = i;
			break;
		}
	}
	if ( nFilterId > 0 ) {
		if ( nFilterId > m_nBaseFiltersNum-1 ) { // only for user filters
			m_slFiltersList[nFilterId] = sName+"="+sPatterns;
			qDebug("Filter about name '%s' has been replaced !", qPrintable(sName) );
		}
		else
			qDebug("Cannot replace a standard filter !");
	}
	else
		m_slFiltersList.append( sName+"="+sPatterns );
}


void Filters::remove( int nId )
{
	if ( nId > m_slFiltersList.count()-1 ) {
		qDebug("Too big number as filter index !");
		return;
	}
	if ( nId > m_nBaseFiltersNum-1 ) {
		//m_slFiltersList.remove( m_slFiltersList.at(nId) ); // Qt4 has'nt remove method :(
		QStringList tmpSL;
		for (int i=0; i<m_slFiltersList.count(); i++) {
			if (i != nId)
				tmpSL << m_slFiltersList.at(i);
		}
		m_slFiltersList = tmpSL;
	}
	else
		qDebug("Cannot remove a standard filter !");
}


void Filters::updateFilters()
{
	// --- add standard filters
	m_slFiltersList.clear();
	m_slFiltersList.append( tr( "files and directories" )+"=*" ); // first from 'enum FilesFilters'
	m_slFiltersList.append( tr( "directories only" )+"=*" );
	m_slFiltersList.append( tr( "files only" )+"=*" );
	m_slFiltersList.append( tr( "links" )+"=*" );
	m_slFiltersList.append( tr( "hanging links" )+"=*" );
	m_nBaseFiltersNum = m_slFiltersList.count();

	qDebug("---Filters::updateFilters(), QSettings not yet supported");
/*
	QSettings *pSettings = new QSettings;
	// --- info for Qt4 port
	// key /qtcmd/FilesPanel/UserFilters is saved like this: UserFilters=first_filter=*.first^eother_filter=*.other^e

	QStringList slUserFilters = pSettings->readListEntry( "/qtcmd/FilesPanel/UserFilters" );
	if ( slUserFilters.count() )
		m_slFiltersList += slUserFilters;

	m_nDefaultFilterId = pSettings->readNumEntry( "/qtcmd/FilesPanel/DefaultFilesFilter", 0 );
	if ( m_nDefaultFilterId > m_slFiltersList.count()-1 )
		m_nDefaultFilterId = 0;

	delete pSettings;
*/
}


void Filters::updateByFilesAssociation( FilesAssociation *pFA )
{
	if ( ! pFA )
		return;

	// read all the files association and adds them to the list
	//m_slFiltersList.append( desc+"="+sPatterns )
}

