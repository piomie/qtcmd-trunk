/***************************************************************************
                          fileinfoext.h  -  description
                             -------------------
    begin                : tue dec 9 2003
    copyright            : (C) 2003 by Piotr Mierzwi�ski
    email                : peterm@go2.pl

    copyright            : See COPYING file that comes with this project
 ***************************************************************************/

#ifndef _FILEINFOEXT_H_
#define _FILEINFOEXT_H_

#include "../../enums.h"

#include <unistd.h>
#include <sys/stat.h>
#include <qfileinfo.h>

/**
 * @author Piotr Mierzwi�ski
 */
/** @short Klasa rozszerzonej informacji o pliku.
 * Dost�pne jest tu wiele metod udost�pniaj�ce r�ne rozszerzone (wzgl�dem
 * klasy QFileInfo) informacje o pliku. U�ywaj�c tej klasy mo�na uzyska�
 * nast�puj�ce informacje:
 * @li czy plik ma ustawiony jeden z bit�w: UID, GID, VXD;
 * @li czy plik to kolejka FIFO, urz�dzenie blokowe, urz�dzenie znakowe, gniazdko;
 * @li rodzaj pliku (typ zgodny z KindOfFile);
 * @li nazw� pliku bez �cie�ki lub sam� �cie�k� bez nazwy;
 * @li czy plik to archiwum i typ tego archiwum (zgodny z KindOfArchive)
*/
class FileInfoExt : public QFileInfo
{
public:
	/** Konstruktor klasy.
	 * Inicjowana jest tu podanym parametrem sk�adowa, poza tym brak definicji.
	 */
	FileInfoExt( const QString & fileName ) : QFileInfo(fileName), m_sFileName(fileName) {}
	FileInfoExt() : QFileInfo() {}

	/** Destruktor klasy.
	 * Brak definicji, poniewa� jest zb�dna.
	 */
	virtual ~FileInfoExt() {}

	/** Zwraca rozmiar pliku.
	 * @return rozmiar pliku w bajtach.
	 */
	long long size() { return size( m_sFileName ); }

// 	QString owner() const { QFileInfo::owner().isEmpty() ? QString::number(ownerId()) : QFileInfo::owner(); }
// 	QString group() const { QFileInfo::group().isEmpty() ? QString::number(groupId()) : QFileInfo::group(); }

	/** Sprawdza czy plik ma ustawiony bit UID, w prawach dost�pu.
	 * @return TRUE, je�li bit jest ustawiony, w przeciwnym razie FALSE.
	 */
	bool isUID() { return specialPerm( m_sFileName, UID ); }

	/** Sprawdza czy plik ma ustawiony bit GID, w prawach dost�pu.
	 * @return TRUE, je�li bit jest ustawiony, w przeciwnym razie FALSE.
	 */
	bool isGID() { return specialPerm( m_sFileName, GID ); }

	/** Sprawdza czy plik ma ustawiony bit VXT, w prawach dost�pu.
	 * @return TRUE, je�li bit jest ustawiony, w przeciwnym razie FALSE.
	 */
	bool isVTX() { return specialPerm( m_sFileName, VTX ); }


	/** Sprawdza czy plik to kolejka FIFO.
	 * @return TRUE, je�li plik to kolejka FIFO, w przeciwnym razie FALSE.
	 */
	bool isFifo()    { return specialFile( m_sFileName, FIFO );   }

	/** Sprawdza czy plik to urz�dzenie blokowe.
	 * @return TRUE, je�li plik to urz�dzenie blokowe, w przeciwnym razie FALSE.
	 */
	bool isBlkDev()  { return specialFile( m_sFileName, BLKDEV ); }

	/** Sprawdza czy plik to urz�dzenie znakowe.
	 * @return TRUE, je�li plik to urz�dzenie znakowe, w przeciwnym razie FALSE.
	 */
	bool isChrDev()  { return specialFile( m_sFileName, CHRDEV ); }

	/** Sprawdza czy plik jest gniadkiem (SOCKET).
	 * @return TRUE, je�li plik to gniazdko, w przeciwnym razie FALSE.
	 */
	bool isSocket()  { return specialFile( m_sFileName, SOCKET ); }


	/** Funkcja zwraca rodzaj pliku zgodny z type @em KindOfFile dla podanego
	 * rozszerzenia.
	 * @param sFileName nazwa pliku.
	 */
	static KindOfFile kindOfFile( const QString & sFileName );

	/** Funkcja zwraca �cie�k� do podanej nazwy pliku.
	 * @param sFullFileName - nazwa pliku, z kt�rego nale�y pobra� �cie�k�,
	 * @return �cie�ka z podanego pliku.
	 */
	static QString filePath( const QString & sFullFileName );

	/** Zwraca nazw� pliku bez ewentualnie towarzysz�cej jej �cie�ki.
	 * @param sFullFileName - absolutna nazwa pliku, z kt�rego nale�y pobra� nazw�,
	 * @return nazwa podanego pliku bez �cie�ki.
	 */
	static QString fileName( const QString & sFullFileName );

	/** Funkcja zwraca nazw� hosta dla podanego URL-a.
	 * @param sUrl - sciezka do pliku lub katalogu
	 * @param bAddSlash - ustawione na TRUE powoduje ze ostatnim znakiem hosta b�dzie
	 * znak slash ('/').
	 */
	static QString hostName( const QString & sUrl, bool bAddSlash=TRUE );

	/** Sprawdza czy podany plik to archiwum.
	 * @param sFileNameIn - nazwa pliku do sprawdzenia,
	 * @return TRUE je�li nazwa wskazuje na archiwum, w przeciwnym razie FALSE.
	 * Funkcja przgl�da ca�� �cie�k� podanego pliku poszukiwaniu nazwy archiwum
	 */
	static bool isArchive( const QString & sFileName );

	/** Zwraca nazw� archiwum obcinaj�c ewentualn� �cie�k� za nazw�.
	 * @param sFileNameIn - nazwa pliku archiwum.
	 * @return nazwa archiwum
	 */
	static QString archiveFullName( const QString & sFileNameIn );

	/** Zwraca rodzaj archiwum zgodny z typem @em KindOfArchive .
	 * @param sFileNameIn - nazwa pliku archiwum,
	 * @return rodzaj archiwum typu @em KindOfArchive .
	 */
	static KindOfArchive kindOfArchive( const QString & sFileNameIn );

	/** Zwraca sum� praw dost�pu do podanego pliku.
	 * @param sFileName - nazwa pliku, kt�remu nale�y pobra� prawa dost�pu.
	 * @return suma praw dost�pu.
	 * Poza standardowymi prawami, sprawdzane s� tak�e atrybuty takie jak: S_ISUID,
	 * S_ISGID, S_ISVTX. Nazwa nie musi zawiera� �cie�ki absolutnej.
	 */
	static int permission( const QString & sFileName );

	/** Zwraca wag� podanego pliku.
	 * @param sFileName - nazwa pliku, kt�remu nale�y pobra� wag�.
	 * @return waga pliku.
	 */
	static long long size( const QString & sFileName );

	/** Typ dla plik�w:
	 *  @li SOCKET - gniazdko,
	 *  @li BLKDEV - urz�dzenie blokowe,
	 *  @li CHRDEV - urz�dzenia znakowego,
	 *  @li FIFO   - kolejka FIFO.
	 */
	enum SpecialFileType { NONEsft=0, SOCKET=S_IFSOCK, BLKDEV=S_IFBLK, CHRDEV=S_IFCHR, FIFO=S_IFIFO };

	/** Typ dla plik�w kt�re maj� ustawiony jeden z bit�w: UID, GID, VTX.
	 */
	enum SpecialPerm     { NONEsp=0,  UID=S_ISUID, GID=S_ISGID, VTX=S_ISVTX };

	/** Sprawdza czy podany plik zawiera podany bit praw dost�pu.
	 * @param sFileName - nazwa pliku, kt�ry nale�y sprawdzi�,
	 * @param sp - bit specjalny praw dostepu zgodny z typem @em SpecialPerm
	 */
	static bool specialPerm( const QString & sFileName, SpecialPerm sp );

	/** Sprawdza czy podany plik jest plikiem specjalnym.
	 * @param sFileName - nazwa pliku, kt�ry nale�y sprawdzi�,
	 * @param sft - bit specjalny praw dostepu zgodny z typem @em SpecialFileType
	 */
	static bool specialFile( const QString & sFileName, SpecialFileType sft );

private:
	/** Funkcja wykonuje operacje 'stat' na podanym pliku.
	 * @param sFileName - nazwa pliku, kt�ry nale�y sprawdzi�,
	 * @param nStatInfo - rodzaj bitu do sprawdzenia, dost�pne typy to:
	 * @em SpecialFileType i @em SpecialPerm .
	 * @return TRUE je�li plik posiada podany bit, w przeciwnym razie FALSE.
	 * Uwaga. Nazwa pliku musi posiada� absolutn� �cie�k�.
	 */
	static bool doStat( const QString & sFileName, int nStatInfo );


	QString m_sFileName;

};

#endif
