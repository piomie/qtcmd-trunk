/***************************************************************************
                          toolbarpage.cpp  -  description
                             -------------------
    begin                : pon mar 21 2005
    copyright            : (C) 2005 by Piotr Mierzwiński
    email                : peterm@go2.pl

    copyright            : See COPYING file that comes with this project

 $Id$
 ***************************************************************************/

#include <QImage>
#include <QListWidget>
//#include <QSettings> // TODO support QSettings port

#include "toolbarpage.h"

#include "storage.xpm"
#include "settings.xpm"
#include "favorites.xpm"
#include "toolbarapp_icons.h"
#include "toolbarsett_icons.h"
#include "view_icons.h"
#include "textview_icons.h"
#include "mainview_icons.h"


ToolBarPage::ToolBarPage( QWidget * /*pParent*/ )
{
	setupUi( this );

	connect( m_pBarsComboBox,  SIGNAL( highlighted(int) ), this, SLOT( slotChangeCurrentBar(int) ) );
	connect( m_pInsertToolBtn, SIGNAL( clicked() ), this, SLOT( slotInsert() ) );
	connect( m_pUpToolBtn,     SIGNAL( clicked() ), this, SLOT( slotUp() ) );
	connect( m_pDownToolBtn,   SIGNAL( clicked() ), this, SLOT( slotDown() ) );
	connect( m_pDeletePushBtn, SIGNAL( clicked() ), this, SLOT( slotDelete() ) );
}

ToolBarPage::~ToolBarPage()
{
}


void ToolBarPage::init( bool bAppSettings )
{
	m_pInputActionsListWidget->setFocus();

	if ( bAppSettings ) {
		m_pInputActionsListWidget->addItem( new QListWidgetItem(QIcon(treeview),    tr("Change kind of list view"), m_pInputActionsListWidget) );
		m_pInputActionsListWidget->addItem( new QListWidgetItem(QIcon(ftptool),     tr("FTP connection manager"), m_pInputActionsListWidget) );
		m_pInputActionsListWidget->addItem( new QListWidgetItem(QIcon(reread),      tr("Reread current directory"), m_pInputActionsListWidget) );
		m_pInputActionsListWidget->addItem( new QListWidgetItem(QIcon(proportions), tr("Proportions of panels"), m_pInputActionsListWidget) );
		m_pInputActionsListWidget->addItem( new QListWidgetItem(QIcon(storage),     tr("Show storage devices list"), m_pInputActionsListWidget) );
		m_pInputActionsListWidget->addItem( new QListWidgetItem(QIcon(freespace),   tr("Show free space for current disk"), m_pInputActionsListWidget) );
		m_pInputActionsListWidget->addItem( new QListWidgetItem(QIcon(findfile),    tr("Find file"), m_pInputActionsListWidget) );
		m_pInputActionsListWidget->addItem( new QListWidgetItem(QIcon(favorites),   tr("Show favorites"), m_pInputActionsListWidget) );
		m_pInputActionsListWidget->addItem( new QListWidgetItem(QIcon(quickgetout), tr("Quick get out from file system"), m_pInputActionsListWidget) );
		m_pInputActionsListWidget->addItem( new QListWidgetItem(QIcon(settings),    tr("Configure ")+" QtCommander", m_pInputActionsListWidget) );
	}
	else { // viewSettings
		m_pInputActionsListWidget->addItem( new QListWidgetItem(QIcon(filenew),  tr("Open in new window")) );
		m_pInputActionsListWidget->addItem( new QListWidgetItem(QIcon(fileopen), tr("Open")) );
		m_pInputActionsListWidget->addItem( new QListWidgetItem(QIcon(filesave), tr("Save")) );
		m_pInputActionsListWidget->addItem( new QListWidgetItem(QIcon(print),    tr("Print")) );
		m_pInputActionsListWidget->addItem( new QListWidgetItem(QIcon(filereload), tr("Reload")) );
		m_pInputActionsListWidget->addItem( new QListWidgetItem(QIcon(undo),     tr("Undo")) );
		m_pInputActionsListWidget->addItem( new QListWidgetItem(QIcon(redo),     tr("Redo")) );
		m_pInputActionsListWidget->addItem( new QListWidgetItem(QIcon(cut),      tr("Cut")) );
		m_pInputActionsListWidget->addItem( new QListWidgetItem(QIcon(copy),     tr("Copy")) );
		m_pInputActionsListWidget->addItem( new QListWidgetItem(QIcon(paste),    tr("Paste")) );
		m_pInputActionsListWidget->addItem( new QListWidgetItem(QIcon(findfirst),tr("Find")) );
		m_pInputActionsListWidget->addItem( new QListWidgetItem(QIcon(findnext), tr("Find next")) );
		m_pInputActionsListWidget->addItem( new QListWidgetItem(QIcon(replace),  tr("Replace")) );
		m_pInputActionsListWidget->addItem( new QListWidgetItem(QIcon(gotoline), tr("Go to line")) );
		m_pInputActionsListWidget->addItem( new QListWidgetItem(QIcon(viewmode), tr("Change kind of view to")+"...") );
		m_pInputActionsListWidget->addItem( new QListWidgetItem(QIcon(zoomin),   tr("Zoom in")) );
		m_pInputActionsListWidget->addItem( new QListWidgetItem(QIcon(zoomout),  tr("Zoom out")) );
		m_pInputActionsListWidget->addItem( new QListWidgetItem(QIcon(settings), tr("Configure view")) );
		m_pInputActionsListWidget->addItem( new QListWidgetItem(QIcon(sidelist), tr("Show/Hide side list")) );
	}

	m_pInputActionsListWidget->setCurrentRow(0);

	m_pDeletePushBtn->setShortcut( Qt::Key_Delete );
	m_pUpToolBtn->setIcon( QIcon(up) );
	m_pDownToolBtn->setIcon( QIcon(down) );
	m_pInsertToolBtn->setIcon( QIcon(insert) );

// 	QSettings settings;
	// --- TODO read bars with icons list from config file


	// adds bars name
	QStringList slBarsList;
	slBarsList << ( tr("global icons") );
	if ( ! bAppSettings ) {
		slBarsList << tr("text view") << tr("image view") << tr("sound view") << tr("video view");
	}
	m_pBarsComboBox->addItems( slBarsList );

	slotChangeCurrentBar( 0 );
}

void ToolBarPage::slotInsert()
{
	if ( ! m_pInputActionsListWidget->hasFocus() )
		m_pInputActionsListWidget->setFocus();

	if ( m_pOutputActionsListWidget->findItems(m_pInputActionsListWidget->currentItem()->text(), Qt::MatchExactly).count() == 0 ) {
		m_pOutputActionsListWidget->addItem( new QListWidgetItem(*m_pInputActionsListWidget->currentItem()) );
		updateControlButtons();
	}
}

void ToolBarPage::slotUp()
{
	moveCurrentItem( UP );
}


void ToolBarPage::slotDown()
{
	moveCurrentItem( DOWN );
}


void ToolBarPage::slotDelete()
{
	if ( m_pOutputActionsListWidget->count() == 0 )
		return;

	if ( ! m_pOutputActionsListWidget->hasFocus() )
		m_pOutputActionsListWidget->setFocus();

	m_pOutputActionsListWidget->takeItem( m_pOutputActionsListWidget->currentRow() );
	updateControlButtons();

	// -- set item after deleted as current item or current if no previous
	QListWidgetItem *prevItem = m_pOutputActionsListWidget->item(m_pOutputActionsListWidget->currentRow()-1);
	QListWidgetItem *nextItem = m_pOutputActionsListWidget->item(m_pOutputActionsListWidget->currentRow()+1);
	if (prevItem != NULL) {
		if (nextItem != NULL)
			m_pOutputActionsListWidget->setCurrentItem(nextItem);
	}
}

// TODO ToolBarPage - moving all selected items
// QList<QListWidgetItem *> selectedItems ()
// set m_pOutputActionsListWidget and m_pInputActionsListWidget to multiselection

void ToolBarPage::moveCurrentItem( Direct eDirect )
{
	if ( m_pOutputActionsListWidget->count() < 2 )
		return;

	if ( ! m_pOutputActionsListWidget->hasFocus() )
		m_pOutputActionsListWidget->setFocus();

	QListWidgetItem *pReplacedItem = NULL;
	QListWidgetItem *pCurrentItem  = m_pOutputActionsListWidget->currentItem();
	if (pCurrentItem == NULL)
		return;

	int nItemHeight   = m_pOutputActionsListWidget->visualItemRect(pCurrentItem).height();
	int nCurrItemYpos = m_pOutputActionsListWidget->visualItemRect(pCurrentItem).y()+6;

	if ( eDirect == UP )
		pReplacedItem = m_pOutputActionsListWidget->itemAt( QPoint(5,nCurrItemYpos-nItemHeight) );
	else
	if ( eDirect == DOWN)
		pReplacedItem = m_pOutputActionsListWidget->itemAt( QPoint(5,nCurrItemYpos+nItemHeight) );

	if ( pReplacedItem == NULL || pReplacedItem == pCurrentItem )
		return;

	m_pOutputActionsListWidget->insertItem( m_pOutputActionsListWidget->currentRow()+((eDirect == UP) ? -1 : 2),
											(pReplacedItem=new QListWidgetItem(*pCurrentItem)) );
	m_pOutputActionsListWidget->takeItem(m_pOutputActionsListWidget->row(pCurrentItem));
	m_pOutputActionsListWidget->setCurrentItem(pReplacedItem);
}


void ToolBarPage::slotChangeCurrentBar( int /*nCurrentBarId*/ )
{
	//m_pOutputActionsListWidget->clear();
	//inserts icons list about id == currentBarId (0 - global icons, 1 - TextView icons)
	updateControlButtons();
}


void ToolBarPage::updateControlButtons()
{
	m_pUpToolBtn->setEnabled( (m_pOutputActionsListWidget->count() > 1) );
	m_pDownToolBtn->setEnabled( (m_pOutputActionsListWidget->count() > 1) );
	m_pDeletePushBtn->setEnabled( (m_pOutputActionsListWidget->count() > 0) );
}
