/***************************************************************************
                          fontchooserpage  -  description
                             -------------------
    begin                : nie gru 23 2007
    copyright            : (C) 2007 by Piotr Mierzwiński, Mariusz Borowski
    email                : piom@nes.pl, mariusz@nes.pl

    copyright            : See COPYING file that comes with this project

 $Id$
 ***************************************************************************/
#include <QtGui>
//#include <QSettings> // TODO add support for QSettings

#include "fontchooserpage.h"


FontChooserPage::FontChooserPage()
{
	m_bSpinBoxChanged = FALSE;

	setupUi(this);

	connect( m_pBoldChkBox, SIGNAL(clicked()), this, SLOT(slotSetFontAttribut()) );
	connect( m_pItalicChkBox, SIGNAL(clicked()), this, SLOT(slotSetFontAttribut()) );
	connect( m_pDefaultFontBtn, SIGNAL(clicked()), this, SLOT(slotSetDefaultFont()) );
	connect( m_pOriginalFontBtn, SIGNAL(clicked()), this, SLOT(slotSetOriginalFont()) );

	connect( m_pFontSizeListWidget, SIGNAL(currentRowChanged(int)), this, SLOT(slotFontSizeHighlighted(int)) );
	connect( m_pFontSizeSpinBox, SIGNAL(valueChanged(int)), this, SLOT(slotFontSizeChanged(int)) );
	connect( m_pFontListWidget, SIGNAL(itemSelectionChanged()), this, SLOT(slotFamilySelectionChanged()) );

	init();
}


void FontChooserPage::init()
{
	/*
// --- read current font from config file
	QSettings settings;
	m_sOriginalFont = settings.readEntry( "/qtcmd/FilesPanel/Font" );
	if ( m_sOriginalFont.isEmpty() )
		m_sOriginalFont = "Helvetica,10"; // default

	m_pOriginalFontBtn->setToolTip( m_sOriginalFont );
	//QToolTip::add( m_pOriginalFontBtn, m_sOriginalFont );
	*/
	updateFontFamilies();
	slotSetOriginalFont();
}


void FontChooserPage::updateFontFamilies()
{
// --- get font sFamily list
	m_slFamilyName = m_fdFontDataBase.families();

	QString sFont;
	QStringList slNewFontList;
	QStringList::Iterator it = m_slFamilyName.begin();

	for( ; it != m_slFamilyName.end() ; it++ )
	{
		sFont = *it;
		if ( sFont.contains('-') )  {
			int i = sFont.indexOf('-');
			sFont = sFont.right( sFont.length() - i - 1 ) + " [" + sFont.left( i ) + "]";
		}
		sFont[0] = sFont[0].toUpper(); // make Capitalic
#if 0
		if ( m_fdFontDataBase.isSmoothlyScalable( *it ) )
			slNewFontList.append( sFont + "(TT)" );
		else if ( m_fdFontDataBase.isBitmapScalable( *it ) )
			slNewFontList.append( sFont + "(BT)" );
		else
#endif
		slNewFontList.append( sFont );
	}

	m_pFontListWidget->addItems( slNewFontList );
	//m_pFontListWidget->insertStringList( slNewFontList );
}


void FontChooserPage::updateFontSizes()
{
	m_pFontSizeListWidget->clear();
	QList<int> nlSize = m_fdFontDataBase.pointSizes( m_pFontListWidget->currentItem()->text(), m_sStyle);

	if ( nlSize.isEmpty() )  {
		qDebug() << "FontChooserPage::updateFontSizes(): Internal error!\n"
				 << "No pointsizes for family \"" << m_pFontListWidget->currentItem()->text()
				 << " with script " << m_sCharSet << " and style " << m_sStyle;
		//qDebug("FontChooserPage::updateFontSizes(): Internal error!");
		//qDebug("No pointsizes for family \"%s\" with script %s and style %s"
		//		, m_pFontListWidget->currentItem()->text().toLatin1().data(), m_sCharSet.toLatin1().data(), m_sStyle.toLatin1().data());
		return;
	}

	for(int i=0; i<nlSize.count(); i++)  {
		m_pFontSizeListWidget->addItem( QString("%1").arg(nlSize[i]) );
	}
	m_pFontSizeSpinBox->setRange( nlSize.at(0), nlSize.at(nlSize.size()-1) );
}



void FontChooserPage::slotFamilySelectionChanged()
{
	QString sFamily = m_slFamilyName[ m_pFontListWidget->currentRow() ];

//	slCharSetName = m_fdFontDataBase.charSets( sFamily ); // Qt-ver. < 3.x
	QStringList slCharSetName = m_fdFontDataBase.families();

	if ( slCharSetName.isEmpty() )  {
		qDebug() << "FontChooserPage::slotFamilySelectionChanged(): Internal error, no character sets for family \"" << sFamily << "\"";
		return;
	}
	if ( ! slCharSetName.isEmpty() )
		m_sCharSet = slCharSetName[0];

	QStringList slStyle = m_fdFontDataBase.styles(sFamily);
	m_sStyle = slStyle[0]; // select always style "normal"

	int nCurrentSizePoint = m_pFontSizeListWidget->currentRow();
	int nCurrentFontSize = ( m_pFontSizeListWidget->count() > 0 ) ? m_pFontSizeListWidget->item( nCurrentSizePoint )->text().toInt() : 0;
	updateFontSizes();

	// checks is current familly has on own list size previous font familly too
	QList<int> nlSize = m_fdFontDataBase.pointSizes( sFamily, m_sStyle );
	nCurrentSizePoint = 0;
	for ( int nSizePoint=0; nSizePoint < nlSize.count(); nSizePoint++ ) {
		if ( nlSize[ nSizePoint ] == nCurrentFontSize )  {
			nCurrentSizePoint = nSizePoint;
			break;
		}
	}
	slotFontSizeHighlighted( nCurrentSizePoint );
	m_pFontSizeListWidget->setCurrentRow( nCurrentSizePoint );
}


void FontChooserPage::slotFontSizeHighlighted( int nId ) // connected to QListWidget
{
	//qDebug() << "FontChooserPage::slotFontSizeHighlighted, nId= " << nId;
	if ( nId < 0 || nId > (m_pFontSizeListWidget->count()-1) )
		return;

	int nFontSize = m_pFontSizeListWidget->item( nId )->text().toInt();
	QString sCurrentFontFamily = m_pFontListWidget->currentItem()->text();
	//qDebug() << "- nFontSize= " << nFontSize << ", m_bSpinBoxChanged= " << m_bSpinBoxChanged;
	if (m_pFontSizeListWidget->currentItem() != NULL && ! m_bSpinBoxChanged) {
		m_pFontSizeSpinBox->setValue(m_pFontSizeListWidget->currentItem()->text().toInt());
	}
	m_bSpinBoxChanged = false;

	int nFontWeight = (m_pBoldChkBox->isChecked()) ? QFont::Bold : QFont::Normal;
	m_pFontTestLineEdit->setFont(  QFont(sCurrentFontFamily, nFontSize, nFontWeight, m_pItalicChkBox->isChecked())  );
}

void FontChooserPage::slotFontSizeChanged( int nId ) // connected to QSpinBox
{
	m_bSpinBoxChanged = true;

	QListWidgetItem *pListWidgetItem = m_pFontSizeListWidget->findItems( QString("%1").arg(nId), Qt::MatchExactly ).at(0);
	if ( pListWidgetItem != NULL )
		m_pFontSizeListWidget->setCurrentItem( pListWidgetItem );
}


void FontChooserPage::slotSetDefaultFont()
{
	m_pItalicChkBox->setChecked( FALSE );
	m_pBoldChkBox->setChecked( FALSE );
	setPreviewFont( "Helvetica", 10 ); // default font
}


void FontChooserPage::slotSetOriginalFont()
{
	// parse 'm_sOriginalFont'
	QString sBold     = m_sOriginalFont.section( ',', 2,2 ).simplified().toLower();
	QString sItalic   = m_sOriginalFont.section( ',', 3,3 ).simplified().toLower();
	QString sFontSize = m_sOriginalFont.section( ',', 1,1 ).simplified();
	bool bOk;
	uint nFontSize = sFontSize.toInt( &bOk );
	if ( ! bOk )
		nFontSize = 10; // default size

	m_pBoldChkBox->setChecked( (sBold == "bold") );
	m_pItalicChkBox->setChecked( (sItalic == "italic") );
	setPreviewFont( m_sOriginalFont.section( ',', 0,0 ), nFontSize );
}


void FontChooserPage::setPreviewFont( const QString & sFamily, int nSize )
{
	int nIdFontFamily = -1;
	// empty sFamily made set default font
	QList<QListWidgetItem *> fontList = m_pFontListWidget->findItems( sFamily, Qt::MatchExactly );
	if (fontList.count() > 0)
		nIdFontFamily = m_pFontListWidget->row( fontList.at(0) );

	if ( nIdFontFamily < 0 ) {
		if ( sFamily != "Helvetica" ) { // sFamily different than default font sFamily
			qDebug() << "FontChooserPage::setPreviewFont, font family= " << sFamily << " not found!";
			slotSetDefaultFont();
			return;
		}
		else // not found default font sFamily, get first avilable
			nIdFontFamily = 0;
	}
	m_pFontListWidget->setCurrentRow( nIdFontFamily );

	int nIdFontSize = -1;
	int nSizeToFind = nSize;
	while ( nIdFontSize == -1 ) {
		for (int i=0; i<m_pFontSizeListWidget->count(); i++) {
			if ( m_pFontSizeListWidget->item(i)->text().toInt() == nSizeToFind ) {
				nIdFontSize = i;
				break; // 'for' loop
			}
		}
		if ( nIdFontSize == -1 )
			qDebug() << "FontChooserPage::setPreviewFont, font size= " << nSizeToFind << " not found!";
		nSizeToFind++; // not found passed font nSize
		if (nSizeToFind > 100)
			break;
	}
	m_pFontSizeListWidget->setCurrentRow( nIdFontSize );
	slotSetFontAttribut();
}

void FontChooserPage::slotSetFontAttribut()
{
	slotFontSizeHighlighted( m_pFontSizeListWidget->currentRow() );
}


QString FontChooserPage::fontFamily()
{
	return m_pFontListWidget->currentItem()->text();
}

uint FontChooserPage::fontSize()
{
	return m_pFontSizeListWidget->currentItem()->text().toInt();
}

bool FontChooserPage::fontItalic()
{
	return m_pItalicChkBox->isChecked();
}


bool FontChooserPage::fontBold()
{
	return m_pBoldChkBox->isChecked();
}


void FontChooserPage::slotEncodingHighlighted( int /*nId*/ )
{

}

void FontChooserPage::slotSave()
{
	QString sFont = fontFamily() + "," + QString::number( fontSize() );

	if ( m_pBoldChkBox->isChecked() )
		sFont += ",Bold";
	if ( m_pItalicChkBox->isChecked() )
		sFont += ",Italic";
/*
	QSettings settings;
	settings.writeEntry( "/qtcmd/FilesPanel/Font", sFont );
*/
}

