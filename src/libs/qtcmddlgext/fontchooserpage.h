/***************************************************************************
                          fontchooserpage  -  description
                             -------------------
    begin                : nie gru 23 2007
    copyright            : (c) 2007 by Piotr Mierzwiński, Mariusz Borowski
    email                : piom@nes.pl, mariusz@nes.pl

    copyright            : See COPYING file that comes with this project

 $Id$
 ***************************************************************************/
#ifndef FONTCHOOSERPAGE_H
#define FONTCHOOSERPAGE_H

#include <QFontDatabase>

#include "ui_fontchooserpagedialog.h"

/**
	@author Piotr Mierzwiński <piom@nes.pl>
*/
class FontChooserPage : public QWidget, Ui::FontChooserPageDialog
{
	Q_OBJECT

private:
	QFontDatabase m_fdFontDataBase;

	QStringList m_slFamilyName;

	QString m_sStyle;
	QString m_sCharSet;
	QString m_sOriginalFont;

	bool m_bSpinBoxChanged;

public:
	FontChooserPage();

	QString fontFamily();
	bool fontItalic();
	bool fontBold();
	uint fontSize();

	void init();
	void slotEncodingHighlighted( int nId );
	void setPreviewFont( const QString & sFamily, int nSize );

private:
	void updateFontFamilies();
	void updateFontSizes();

private slots:
	void slotFamilySelectionChanged();

	void slotFontSizeHighlighted( int nId );
	void slotFontSizeChanged( int nId );
	void slotSetFontAttribut();
	void slotSetOriginalFont();
	void slotSetDefaultFont();

public slots:
	void slotSave();


};

#endif
