/***************************************************************************
                          toolbarpage.h  -  description
                             -------------------
    begin                : pon mar 21 2005
    copyright            : (c) 2005 by Piotr Mierzwiński
    email                : peterm@go2.pl

    copyright            : See COPYING file that comes with this project

 $Id$
 ***************************************************************************/

#ifndef TOOLBARPAGE_H
#define TOOLBARPAGE_H

#include "ui_toolbarpagedialog.h"

/**
	@author Piotr Mierzwiński
*/
class ToolBarPage : public QWidget, Ui::ToolBarPageDialog
{
	Q_OBJECT
public:
	ToolBarPage( QWidget *pParent=NULL );
	~ToolBarPage();

	void init( bool appSettings );

private:
	enum Direct { UP=0, DOWN };

	void moveCurrentItem( Direct eDirect );
	void updateControlButtons();

private slots:
	void slotUp();
	void slotDown();
	void slotDelete();
	void slotInsert();
	void slotChangeCurrentBar( int );


};

#endif
