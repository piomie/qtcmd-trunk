/***************************************************************************
                          controlpanel  -  description
                             -------------------
    begin                : pon mar 21 2005
    copyright            : (c) 2005 by Piotr Mierzwiński
    email                : peterm@go2.pl

    copyright            : See COPYING file that comes with this project

 $Id$
 ***************************************************************************/
#ifndef CONTROLPANEL_H
#define CONTROLPANEL_H

#include <QTime>

#include "ui_controlpaneldialog.h"

/**
	@author Piotr Mierzwiński
*/
class ControlPanel : public QWidget, Ui::ControlPanelDialog
{
	Q_OBJECT

public:
	ControlPanel();
	~ControlPanel();

	void setMaxTime( const QTime & tTime );

private slots:
	void slotSliderChanged( int nPosition );
	void slotPlay();
	void slotPause();
	void slotStop();
	void slotForward();
	void slotRewind();
	void slotTimeChanged( const QTime & tTime );

signals:
	void signalPlay();
	void signalPause();
	void signalStop();
	void signalForward( int nPlusPos );
	void signalRevind( int nMinusPos );
	void signalTimeChanged( int nSeconds );

};

#endif
