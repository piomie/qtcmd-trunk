/***************************************************************************
                          messagebox.cpp  -  description
                             -------------------
    begin                : pon mar 21 2005
    copyright            : (C) 2005 by Piotr Mierzwi�ski
    email                : peterm@go2.pl

    copyright            : See COPYING file that comes with this project

 $Id$
 ***************************************************************************/

#include <QIcon>
#include <QLabel>
#include <QPixmap>
#include <QMessageBox>
#include <QPushButton>
#include <QApplication>
#include <QStyle>

#include "messagebox.h"
#include "squeezelabel.h"


MessageBox::MessageBox()
{
	setupUi(this);
	m_pButtonGroup = new QButtonGroup;

	connect( m_pButtonGroup, SIGNAL(buttonClicked(int)), this, SLOT(done(int)) );
}


MessageBox::~MessageBox()
{
}
// TODO add checkBox
int MessageBox::common( QWidget * /*pParent*/, const QString & sCaption, const QString & sMsg, const QStringList & slBtnsNames, int nDefaultButton, QMessageBox::Icon icon, const QString & sChkBoxMsg )
{
	MessageBox *pDlg = new MessageBox;
	Q_CHECK_PTR( pDlg );

	pDlg->mPixmapLab->setPixmap( standardIcon(icon) );

	if ( ! sCaption.isEmpty() )
		pDlg->setWindowTitle( sCaption );

	if ( ! sMsg.isEmpty() ) {
		pDlg->mMsgLabel->setText( sMsg );
	}

	// --- set buttons
	const int nTotalButtons = 5;
	int nMaxButtons = slBtnsNames.count();

	if ( nMaxButtons > nTotalButtons ) {
		qDebug("MessageBox::common. Too big number of buttons, max= %d", nTotalButtons);
		nMaxButtons = 5;
	}
	if ( nMaxButtons < 1 )
		nMaxButtons = 1;

	QPushButton *pButtonsTab[nTotalButtons] =
		{ pDlg->mBtn1, pDlg->mBtn2, pDlg->mBtn3, pDlg->mBtn4, pDlg->mBtn5 };

	for (int i=0; i<nTotalButtons; i++ )
		pDlg->m_pButtonGroup->addButton( pButtonsTab[i], i + Yes );

	// set buttons text
	if ( nMaxButtons > 1 ) {
		if ( ! slBtnsNames[0].isEmpty() )
			for ( int i=0; i<slBtnsNames.count(); i++ )
				pButtonsTab[i]->setText( slBtnsNames[i] );
	}
	else { // nMaxButtons == 1
		if ( slBtnsNames[0].isEmpty() )
			pDlg->mBtn1->setText( tr("&OK") );
		else
			pDlg->mBtn1->setText( slBtnsNames[0] );
	}

	int nMaxDlgWidth = 0; // it's width of dialog with everyone buttons
	for ( int i=0; i<nTotalButtons; i++ )
		nMaxDlgWidth += pButtonsTab[i]->width()+10; // 10 - margin

	// set default button
	if ( nDefaultButton > 0 )
		pButtonsTab[nDefaultButton-1]->setDefault( TRUE );
	else // set nDefaultButton on first button
		pButtonsTab[0]->setDefault( TRUE );

	qDebug("MessageBox::common, sChkBoxMsg= %s", sChkBoxMsg.toLatin1().data());
	// support for check box
	if ( sChkBoxMsg.isEmpty() )
		delete pDlg->m_pCheckBox;
	else
		pDlg->m_pCheckBox->setText(sChkBoxMsg);

	// remove not used buttons
	int i = nMaxButtons;
	while ( i<nTotalButtons ) {
		delete pButtonsTab[i];
		i++;
	}
	pDlg->m_pButtonBox->adjustSize();

	if ( pDlg->width() > nMaxDlgWidth )
		pDlg->adjustSize();
	else {
		if ( nMaxButtons < 3 )
			nMaxDlgWidth += (nMaxDlgWidth/2)/2;
		else
		if ( nMaxButtons > 2 )
			nMaxDlgWidth += nMaxDlgWidth/2;

		nMaxDlgWidth /= 2;
		pDlg->resize( nMaxDlgWidth+2, pDlg->sizeHint().height() );
	}

	// --- show dialog
	int nResult = pDlg->exec();

	// check checkBox
	if ( ! sChkBoxMsg.isEmpty() ) {
		if ( pDlg->m_pCheckBox->isChecked() )
			nResult += ChkBoxTrue;
	}

	delete pDlg;
	pDlg = 0;

	return nResult;
}


int MessageBox::information( QWidget * pParent, const QString & sCaption, const QString & sMsg, const QString & sButton0Text )
{
	QStringList lst;
	lst << sButton0Text;
	return common( pParent, sCaption, sMsg, lst, 0, QMessageBox::Information );
}


int MessageBox::warning( QWidget * pParent, const QString & sCaption, const QString & sMsg, const QString & sButton0Text )
{
	QStringList lst;
	lst << sButton0Text;
	return common( pParent, sCaption, sMsg, lst, 0, QMessageBox::Warning );
}


int MessageBox::critical( QWidget * pParent, const QString & sCaption, const QString & sMsg, const QString & sChkBoxMsg, const QString & sButton0Text )
{
	QStringList lst;
	lst << sButton0Text;
	return common( pParent, sCaption, sMsg, lst, 0, QMessageBox::Critical, sChkBoxMsg );
}

int MessageBox::critical( QWidget * pParent, const QString & sMsg, bool , const QString & sChkBoxMsg, const QString & sButton0Text )
{
	QStringList lst;
	lst << sButton0Text;
	return common( pParent, tr("Error")+" - QtCommander", sMsg, lst, 0, QMessageBox::Critical, sChkBoxMsg );
}

int MessageBox::yesNo( QWidget * pParent, const QString & sCaption, const QString & sMsg, int nDefaultButton, bool bThreeButtons, const QString & sButtonText, const QString & sChkBoxMsg )
{
	QStringList slBtnsNames;
	slBtnsNames << tr("&Yes") << tr("&No");

	if ( bThreeButtons ) {
		if ( sButtonText.isEmpty() )
			slBtnsNames.append( tr("&Cancel") );
		else
			slBtnsNames.append( sButtonText );
	}

	return common( pParent, sCaption, sMsg, slBtnsNames, nDefaultButton, QMessageBox::Warning, sChkBoxMsg );
}


QPixmap MessageBox::standardIcon( QMessageBox::Icon icon )
{
	int iconSize = QApplication::style()->pixelMetric(QStyle::PM_MessageBoxIconSize);
	QIcon tmpIcon;

	switch (icon) {
		case QMessageBox::Information:
			tmpIcon = QApplication::style()->standardIcon(QStyle::SP_MessageBoxInformation);
			break;
		case QMessageBox::Warning:
			tmpIcon = QApplication::style()->standardIcon(QStyle::SP_MessageBoxWarning);
			break;
		case QMessageBox::Critical:
			tmpIcon = QApplication::style()->standardIcon(QStyle::SP_MessageBoxCritical);
			break;
		case QMessageBox::Question:
			tmpIcon = QApplication::style()->standardIcon(QStyle::SP_MessageBoxQuestion);
		default:
			break;
	}
	if (! tmpIcon.isNull())
		return tmpIcon.pixmap(iconSize, iconSize);

	return QPixmap();
}
