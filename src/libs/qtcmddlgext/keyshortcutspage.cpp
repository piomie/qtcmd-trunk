/***************************************************************************
                          keyshortcutspage.cpp  -  description
                             -------------------
    begin                : pon mar 21 2005
    copyright            : (C) 2005 by Piotr Mierzwiński
    email                : peterm@go2.pl

    copyright            : See COPYING file that comes with this project

 $Id$
 ***************************************************************************/
#include <QtGui>
//#include <QSettings> // TODO add support for QSettings

#include "messagebox.h"
#include "listwidgetview.h"

#include "keyshortcutspage.h"
#include "listviewfinditem.h"


KeyShortcutsPage::KeyShortcutsPage()
	: m_pFindItem(NULL)
{
	setupUi(this);

	m_pSearchBtn->setShortcut( QKeySequence(tr("Ctrl+S")) );

	connect( m_pListWidget, SIGNAL(cellClicked(int, int)), this, SLOT(slotCellClicked(int, int)) );

	connect( m_pSearchBtn,  SIGNAL(clicked()), this, SLOT(slotShowFindItemDialog()) );
	connect( m_pDefaultBtn, SIGNAL(clicked()), this, SLOT(slotSetDefaults()) );
	connect( m_pEditCurrShortcutBtn, SIGNAL(clicked()), this, SLOT(slotEditCurrentShortcut()) );
}

KeyShortcutsPage::~KeyShortcutsPage()
{
	if (m_pFindItem != NULL)
		delete m_pFindItem;
}


void KeyShortcutsPage::init( bool bAppSettings )
{
	qDebug("KeyShortcutsPage::init");
	m_pFindItem = new ListViewFindItem(m_pListWidget); // hide() , calls destroy() (when quit)
	/*
	QSettings settings;

	QString sScheme = settings.readEntry( "/qtcmd/FilesPanel/ColorsScheme", "DefaultColors" );
	QString sColor  = settings.readEntry( "/qtcmd/"+sScheme+"/SecondBackgroundColor", "FFEBDC" );
	*/

	// prepare view
	// -- add columns TODO make 'add column' support for plugin ListWidgetView

	m_pListWidget->addColumn( ListWidgetView::TextLabel,   tr("Action")); //, Qt::AlignmentFlag eColumnAlign=Qt::AlignLeft );
	m_pListWidget->addColumn( ListWidgetView::KeyEditBox,  tr("Shortcut") );
	m_pListWidget->addColumn( ListWidgetView::RadioButton, tr("Own") );
	m_pListWidget->addColumn( ListWidgetView::RadioButton, tr("Default") );
	m_pListWidget->addColumn( ListWidgetView::RadioButton, tr("None") );
	m_pListWidget->addColumn( ListWidgetView::TextLabel,   tr("Default shortcut") );

	// -- set look&feel
	//m_pListWidget->setSecondColorOfBg( color(sColor, "FFEBDC") ); // not yet supported
	//m_pListWidget->setEnableTwoColorsOfBg( TRUE );

	m_sConfigFileKey = (bAppSettings) ? "/qtcmd/FilesPanelShortcuts/" : "/qtcmd/FileViewShortcuts/";

/*
	connect( m_pListWidget, SIGNAL(editBoxTextApplied(int, const QString &, const QString &)),
	 this, SLOT(slotEditBoxTextApplied(int, const QString &, const QString &)) );
*/
}


void KeyShortcutsPage::setKeyShortcuts( KeyShortcuts *pKeyShortcuts )
{
	if ( pKeyShortcuts == NULL ) {
		qDebug() << "KeyShortcutsPage::setKeyShortcuts, KeyShortcuts object is empty!";
		return;
	}
	m_pKeyShortcuts = pKeyShortcuts;

	QString sKey, sDefaultKey;
	// -- init values
	for ( uint nRow=0; nRow<pKeyShortcuts->count(); nRow++ ) {
		sDefaultKey = pKeyShortcuts->keyDefaultStr(nRow);
		sKey = (pKeyShortcuts->keyStr(nRow) == "none") ? sDefaultKey : pKeyShortcuts->keyStr(nRow);

		m_pListWidget->addRow( true ); // set buttons exclusive mode
		// - set values
		m_pListWidget->setCellValue( nRow, ActionCOL, pKeyShortcuts->desc(nRow) );
		if ( sKey == "none" )
			m_pListWidget->setCellValue( nRow, NoneCOL, "TRUE" );
		else {
			m_pListWidget->setCellValue( nRow, ShortcutCOL, sKey );
			m_pListWidget->setCellValue( nRow, OwnCOL, "TRUE" );
		}
		m_pListWidget->setCellValue( nRow, DefaultShortcutCOL, sDefaultKey );
		if ( sKey == sDefaultKey )
			m_pListWidget->setCellValue( nRow, DefaultCOL, "TRUE" );
	}
	m_pListWidget->resizeColumnsToContents();
	//m_pListWidget->header()->resizeSection( 0, (width()-20)/2 );
	//m_pListWidget->setSortingEnabled( true ); // do not move to inside of ListWidgetView class, do not change order
	m_pListWidget->setCurrentItem( m_pListWidget->item(0,0) );
}


void KeyShortcutsPage::slotCellClicked( int nRow, int nColumn )
{
	if ( nColumn == OwnCOL ) {
		QTableWidgetItem *pItem = m_pListWidget->item( nRow, ShortcutCOL );
		m_pListWidget->editItem( pItem );
		m_pListWidget->openPersistentEditor( pItem );
		; // triggering the ShortcutCOL column TODO show the ListViewEditBox obj.for current pItem
	}
	else
	if ( nColumn == NoneCOL )
		m_pListWidget->setCellValue( nRow, ShortcutCOL, "" );
	else
	if ( nColumn == DefaultCOL )
		m_pListWidget->setCellValue( nRow, ShortcutCOL, m_pListWidget->item(nRow, DefaultShortcutCOL)->text() );
}


void KeyShortcutsPage::slotEditBoxTextApplied( int nColumn, const QString &, const QString & newShortcut ) // TODO implement slotEditBoxTextApplied
{
/*
	ListViewWidgetsItem *pItem = (ListViewWidgetsItem *)m_pListWidget->currentItem();
	if ( ! pItem )
		return;

	// --- check a sShortcut whether it's use yet
	int nResult = -1;
	Q3ListViewItemIterator it( m_pListWidget );
	while ( it.current() ) {
		if ( (*it)->text(nColumn) == newShortcut && (*it) != pItem ) {
			nResult = MessageBox::yesNo( this,
			 tr("Key shortcut conflict")+" - QtCommander",
			 QString( tr("The '%1' key combination has already been allocated to the sAction") ).arg(newShortcut)
			 +" :\n\n\t'"+(*it)->text(ActionCOL)+"'\n\n"+tr("Do you want to reassign it from that action to the current one ?"),
			 MessageBox::No
			);
			break;
		}
		it++;
	}

	if ( nResult == MessageBox::Yes ) {
		// init current pItem
		pItem->setText( nColumn, newShortcut );
		pItem->setBoolValue( ((newShortcut == pItem->text(DefaultShortcutCOL)) ? DefaultCOL : OwnCOL), TRUE );
		// reset founded sShortcut
		pItem = (ListViewWidgetsItem *)(*it);
		pItem->setText( ShortcutCOL, "" );
		pItem->setBoolValue( NoneCOL, TRUE );

		nResult = MessageBox::yesNo( this,
		 tr("Set a new key sShortcut")+" - QtCommander",
		 tr("The action '%1' has not been allocated to the key shortcut").arg(pItem->text(ActionCOL))
		 +".\n\n"+tr("Do you want to allocate it now ?"),
		 MessageBox::Yes
		);
		if ( nResult == MessageBox::Yes ) {
			pItem->setBoolValue( NoneCOL, TRUE );
			m_pListWidget->showWidget( nColumn, pItem ); // shows the ListViewEditBox obj.
			uint col = NoneCOL;
			if ( pItem->text(ShortcutCOL) == pItem->text(DefaultShortcutCOL) )
				col = DefaultCOL;
			else
			if ( ! pItem->text(ShortcutCOL).isEmpty() )
				col = OwnCOL;
			pItem->setBoolValue( col, TRUE );
		}
	}
	else
	if ( nResult != MessageBox::No && nResult != 0 ) { // a new sShortcut no causing a conflict
		pItem->setText( nColumn, newShortcut );
		pItem->setBoolValue( ((newShortcut == pItem->text(DefaultShortcutCOL)) ? DefaultCOL : OwnCOL), TRUE );
	}
	// set a new key sShortcut for the KeyShortcuts obj.
	m_pKeyShortcuts->setKey( pItem->text(ActionCOL), QKeySequence(pItem->text(ShortcutCOL)) );
	*/
}


void KeyShortcutsPage::slotSetDefaults()
{
	for ( int nRow=0; nRow<m_pListWidget->rowCount(); nRow++ ) {
		m_pListWidget->setCellValue( nRow, ShortcutCOL, m_pListWidget->item(nRow, DefaultShortcutCOL)->text() );
		m_pListWidget->setCellValue( nRow, DefaultCOL, "TRUE" );
	}
}


void KeyShortcutsPage::slotEditCurrentShortcut()
{
	slotCellClicked( m_pListWidget->currentRow(), ShortcutCOL );
	; // triggering the ShortcutCOL column TODO show the ListViewEditBox obj.for current pItem
}


void KeyShortcutsPage::slotSave()
{
/*
	QSettings settings;
	QString sAction, sShortcut;
	Q3ListViewItemIterator it( m_pListWidget );

	while ( it.current() ) {
		sAction = (*it)->text(ActionCOL);
		sShortcut = (*it)->text(ShortcutCOL);

		if ( sShortcut.isEmpty() )
			sShortcut = "none";

		it++;
		settings.writeEntry( m_sConfigFileKey+sAction, sShortcut );
	}
*/
}

void KeyShortcutsPage::slotShowFindItemDialog()
{
	if (m_pFindItem != NULL)
		m_pFindItem->show(mapToGlobal(QPoint(10, height()-10)), 320);
}

