/***************************************************************************
                          controlpanel  -  description
                             -------------------
    begin                : pon mar 21 2005
    copyright            : (C) 2005 by Piotr Mierzwiński
    email                : peterm@go2.pl

    copyright            : See COPYING file that comes with this project

 $Id$
 ***************************************************************************/
//#include <QSlider>
//#include <QPushButton>

#include "controlpanel.h"


ControlPanel::ControlPanel()
{
	setupUi( this );

	connect( m_pSlider,     SIGNAL(sliderMoved(int)), this, SLOT(slotSliderChanged(int)) );
	connect( m_pPlayBtn,    SIGNAL(clicked()), this, SLOT(slotPlay()) );
	connect( m_pPauseBtn,   SIGNAL(clicked()), this, SLOT(slotPause()) );
	connect( m_pStopBtn,    SIGNAL(clicked()), this, SLOT(slotStop()) );
	connect( m_pForwardBtn, SIGNAL(clicked()), this, SLOT(slotForward()) );
	connect( m_pRevindBtn,  SIGNAL(clicked()), this, SLOT(slotRewind()) );

	connect( m_pCurrentTimeEdit, SIGNAL(valueChanged(const QTime &)), this, SLOT(slotTimeChanged(const QTime &)) );
}

ControlPanel::~ControlPanel()
{
}


void ControlPanel::slotSliderChanged( int /*nPosition*/ )
{

}


void ControlPanel::slotPlay()
{

}


void ControlPanel::slotPause()
{

}


void ControlPanel::slotStop()
{

}


void ControlPanel::slotForward()
{

}


void ControlPanel::slotRewind()
{

}



void ControlPanel::slotTimeChanged( const QTime & /*tTime*/ )
{

}


void ControlPanel::setMaxTime( const QTime & tTime )
{
	m_pMaxTimeEdit->setTime( tTime );
}

