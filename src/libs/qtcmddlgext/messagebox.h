/***************************************************************************
                          messagebox.h  -  description
                             -------------------
    begin                : pon mar 21 2005
    copyright            : (c) 2005 by Piotr Mierzwi�ski
    email                : peterm@go2.pl

    copyright            : See COPYING file that comes with this project

 $Id$
 ***************************************************************************/

#ifndef MESSAGEBOX_H
#define MESSAGEBOX_H

#include <QMessageBox>
#include <QButtonGroup>

#include "ui_messageboxdialog.h"

/**
	@author Piotr Mierzwi�ski
*/
class MessageBox : public QDialog, Ui::MessageBoxDialog
{
	Q_OBJECT

public:
	MessageBox();
	~MessageBox();

	enum MsgButtons { Yes=1, No, All, None, Cancel, Continue=1, Stop };
	enum CheckBoxFlag { ChkBoxTrue=100 };

	/** Shows message window with button(s).
	 * @param pParent NOT USED NOW,
	 * @param sCaption title of window,
	 * @param sMsg window message,
	 * @param slBtnsNames buttons name list,
	 * @param nDefaultButton default button id,
	 * @param icon QMessageBox ikon,
	 * @param sChkBoxMsg message for CheckBox if empty then not show CheckBox.
	 * @return button id (type MsgButtons), but when sChkBoxMsg is not empty then button id + 100
	*/
	static int common( QWidget * pParent, const QString & sCaption, const QString & sMsg, const QStringList & slBtnsNames=QStringList(), int nDefaultButton=-1, QMessageBox::Icon icon=QMessageBox::NoIcon, const QString & sChkBoxMsg=QString::null );

	static int information( QWidget * pParent, const QString & sCaption, const QString & sMsg, const QString & sButton0Text=QString::null );
	static int warning( QWidget * pParent, const QString & sCaption, const QString & sMsg, const QString & sButton0Text=QString::null );
	static int critical( QWidget * pParent, const QString & sCaption, const QString & sMsg, const QString & sChkBoxMsg=QString::null, const QString & sButton0Text=QString::null );
	static int critical( QWidget * pParent, const QString & sMsg, bool bErrorCaption=TRUE, const QString & sChkBoxMsg=QString::null, const QString & sButton0Text=QString::null );
	static int yesNo( QWidget * pParent, const QString & sCaption, const QString & sMsg, int nDefaultButton=-1, bool bThreeButtons=FALSE, const QString & sButtonText=QString::null, const QString & sChkBoxMsg=QString::null );

private:
	static QPixmap standardIcon(QMessageBox::Icon icon);

private:
	QButtonGroup *m_pButtonGroup;

};

#endif
