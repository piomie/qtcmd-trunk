/***************************************************************************
                          keyshortcutspage.h  -  description
                             -------------------
    begin                : pon mar 21 2005
    copyright            : (c) 2005 by Piotr Mierzwiński
    email                : peterm@go2.pl

    copyright            : See COPYING file that comes with this project

 $Id$
 ***************************************************************************/

#ifndef KEYSHORTCUTSPAGE_H
#define KEYSHORTCUTSPAGE_H

#include "ui_keyshortcutspagedialog.h"


class ListViewFindItem;
/**
	@author Piotr Mierzwiński <piotrmierzwinski@gmail.com>
 */
class KeyShortcutsPage : public QWidget, Ui::KeyShortcutsPageDialog
{
	Q_OBJECT
public:
	KeyShortcutsPage();
	~KeyShortcutsPage();

	void init( bool bAppSettings );

	KeyShortcuts * keyShortcuts() { return m_pKeyShortcuts; }
	void setKeyShortcuts( KeyShortcuts *pKeyShortcuts );

private:
	enum Columns { ActionCOL=0, ShortcutCOL, OwnCOL, DefaultCOL, NoneCOL, DefaultShortcutCOL };

	ListViewFindItem *m_pListViewFindItem;
	KeyShortcuts *m_pKeyShortcuts;
	QString m_sConfigFileKey;

	ListViewFindItem *m_pFindItem;

public slots:
	void slotSave();

private slots:
	void slotCellClicked( int nRow, int nColumn );
	void slotEditBoxTextApplied( int, const QString &, const QString & );
	void slotSetDefaults();
	void slotShowFindItemDialog();
	void slotEditCurrentShortcut();


};

#endif
