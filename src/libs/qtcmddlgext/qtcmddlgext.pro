# Plik utworzony przez menad?era projekt�w kdevelopa
# ------------------------------------------- 
# Podkatalog wzgl?dem g?�wnego katalogu projektu: ./src/libs/qtcmddlgext
# Cel to biblioteka qtcmddlgext

INSTALLS += target 
target.path = $$PREFIX/lib 
TARGETDEPS += \
              ../../../build/lib/libqtcmduiext.so \
              ../../../build/lib/libqtcmdutils.so 
LIBS += \
        -lqtcmduiext \
        -lqtcmdutils 
INCLUDEPATH += \
               ../../../src \
               ../qtcmdutils \
               ../qtcmduiext \
               ../qtcmduiext/lwv \
               ../../../icons \
               .ui 
MOC_DIR = ../../../build/.tmp/moc 
UI_DIR = ../../../build/.tmp/ui 
OBJECTS_DIR = ../../../build/.tmp/obj 
QMAKE_LIBDIR = ../../../build/lib
QMAKE_CXXFLAGS_RELEASE += -O2 
QMAKE_CXXFLAGS_DEBUG += -g3 
TARGET = qtcmddlgext 
DESTDIR = ../../../build/lib 
TEMPLATE = lib 
CONFIG += \
          warn_on \
          qt \
          thread \
          dll \
          precompile_header 
FORMS += \
         messageboxdialog.ui \
         toolbarpagedialog.ui \
         fontchooserpagedialog.ui \
         controlpaneldialog.ui \
         keyshortcutspagedialog.ui
HEADERS += \
           messagebox.h \
           toolbarpage.h \
           fontchooserpage.h \
           controlpanel.h \
           keyshortcutspage.h
SOURCES += \
           messagebox.cpp \
           toolbarpage.cpp \
           fontchooserpage.cpp \
           controlpanel.cpp \
           keyshortcutspage.cpp
QT += network
CONFIG += uic4
TRANSLATIONS += ../../../translations/pl/qtcmd-dlgext.ts 
