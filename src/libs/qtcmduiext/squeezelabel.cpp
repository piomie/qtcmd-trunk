/***************************************************************************
                          squeezelabel.cpp  -  description
                             -------------------
    begin                : sun aug 10 2003
    copyright            : (C) 2003 by Piotr Mierzwiński
    email                : peterm@go2.pl

    Copyright: See COPYING file that comes with this project
***************************************************************************/

#include "squeezelabel.h"

#include <QStringList>


SqueezeLabel::SqueezeLabel( QWidget * pParent )
	: QLabel( pParent ), m_bShowTip( TRUE )
{
	setScaledContents( TRUE );
}


void SqueezeLabel::setShowToolTip( bool bShow )
{
	m_bShowTip = bShow;
}


void SqueezeLabel::setText( const QString & sText )
{
	if ( ! hasScaledContents() )
		QLabel::setText( sText );
	else {
		m_sFullText = sText;
		setSqueezingText( width() );
	}
}


void SqueezeLabel::setSqueezingText( uint nLabelWidth )
{
	if ( ! hasScaledContents() )
		return;

	QString sSqueezeText, sFullText = m_sFullText;

	if ( sFullText.isEmpty() ) {
		sFullText   = text();
		m_sFullText = text();
	}

	if ( sFullText.contains('\n') ) {
		QStringList textLines = sFullText.split("\n");
		for ( int i=0; i<textLines.count(); i++)
			sSqueezeText += squeezeLine( textLines[i], nLabelWidth ) + "\n";
	}
	else
		sSqueezeText = squeezeLine( sFullText, nLabelWidth );

	if ( sSqueezeText.endsWith('\n') )
		sSqueezeText.chop(1); // removes last character

	QLabel::setText( sSqueezeText );

	if ( sSqueezeText.contains("...") && sSqueezeText != "..." ) {
		if ( m_bShowTip )
			setToolTip( sFullText );
		else
			setToolTip("");
	}
	else
		setToolTip("");
}


QString SqueezeLabel::squeezeLine( const QString & sLine, uint nLabelWidth )
{
	if ( sLine.isEmpty() || sLine == "..." )
		return QString("");

	QString sNewLine = sLine;
	// replace each occurence of tab character to 8 space char.
	sNewLine.replace( QRegExp("\\t"), "        " );

	// TODO need to add a rich text support !

	QString leftHalfOfText, rightHalfOfText;
	uint nTextWidthInPixels = fontMetrics().width( sNewLine, sNewLine.length() );
	uint nThreeDotsWidth = fontMetrics().width( "...", QString("...").length() );

	if ( nLabelWidth < nTextWidthInPixels ) {
		// text measuring
		QString sChr;
		int i, nTxtWidth = 0;
		for (i=0; i<sNewLine.length(); i++) {
			sChr = sNewLine[i];
			nTxtWidth += fontMetrics().width(sChr, 1);
			if (nTxtWidth+nThreeDotsWidth > nLabelWidth)
				break;
		}
		leftHalfOfText  = sNewLine.left( i/2 );
		rightHalfOfText = sNewLine.right( i/2 );
		sNewLine = leftHalfOfText + "..." + rightHalfOfText;
	}

	return sNewLine;
}


void SqueezeLabel::resizeEvent( QResizeEvent * )
{
	setSqueezingText( width() );
}
