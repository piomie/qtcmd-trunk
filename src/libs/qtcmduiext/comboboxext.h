/***************************************************************************
                          comboboxext.h  -  description
                             -------------------
    begin                : Tue Sep 21 2004
    copyright            : (C) 2004 by Piotr Mierzwi�ski
    email                : peterm@go2.pl

    copyright            : See COPYING file that comes with this project

$Id$
 ***************************************************************************/

#ifndef COMBOBOXEXT_H
#define COMBOBOXEXT_H

#include <QComboBox>

/**
 * Author:
 * Piotr Mierzwi�ski
 **/
/** @short Klasa zajmuje si� obs�ug� listy �cie�ek.
 * Zawiera kilka rozszerze� wzgl�dem klasy, po kt�rej dziedziczy.
 * Umo�liwia wstawianie �cie�ek na list� bez dublowania si� ich, dba aby nie
 * przekroczony zosta� limit �cie�ek do wstawienia. Pozawala znale�� podan�
 * �cie�k� na li�cie. Zwraca poprzedni� i nast�pn� �cie�k� wzgl�dem bie��cej.
*/
class ComboBoxExt : public QComboBox
{
public:
	/** Konstruktor klasy.
	 * @param nMaxItems - maksylna ilo�� element�w na li�cie,
	 * @param bRW - warto�� TRUE w��cza tryb odczytu i zapisu bie��cego elementu,
	 * @param pParent - wska�nika na rodzica dla obiektu,
	 * @param sz_pName - nazwa dla obiektu.\n
	 Inicjowane s� tutaj sk�adowe klasy oraz bie��cy obiekt (QComboBox).
	 */
	ComboBoxExt( unsigned int nMaxItems, bool bRW, QWidget *pParent, const char *sz_pName=0 );

	/** Destruktor klasy.
	 * Nie zawiera definicji.
	 */
	virtual ~ComboBoxExt() {}

	/** Powoduje wstawienie podanego parametrze ci�gu na podan� pozycj� na
	 * li�cie.
	 * @param sItem - ci�g tekstowy do wstawienia,
	 * @param itemId - pozycja, na kt�r� nale�y wstawi� element. Je�li jej warto��
	 * b�dzie si� r�wna� -1, to element zostanie wstawiony za bie��cym, gdy�
	 * wstawianie dzia�a tylko w trybie InsertionPolicy == AfterCurrent.\n
	 * Przy czym, je�li podany element ju� si� na li�cie znajduje, wtedy jest on
	 * ustawiany jako bie��cy.
	 */
	void insertItem( const QString & sItem, int itemId=-1 );

	/** Powoduje wstawienie listy podanych ci�g�w tekstowych.
	 * @param slItems - lista ci�g�w tekstowych,
	 * @param bClearAllBefore - r�wne TRUE wymusza wyczyszczenie listy.
	 */
	void insertStringList( const QStringList & slItems, bool bClearAllBefore=FALSE );

	/** Metoda usuwa podan� �cie�k� z listy.
	 * @param sItem - nazwa �cie�ki do usuni�cia.
	 */
	void removeItem( const QString & sItem );

	/** Zwraca poprzedni� �cie�k� wzgl�dem bie��cej.
	 * @return poprzednia �cie�ka.
	 * Przy czym, je�li bie��ca jest pierwsz�, wtedy zwracana jest ostatnia.
	 */
	QString prevItem() const {
		return itemText( prevId() );
	}

	/** Zwraca nast�pn� �cie�k� wzgl�dem bie��cej.
	 * @return nast�pna �cie�ka.
	 * Przy czym, je�li bie��ca jest ostatni�, wtedy zwracana jest pierwsza.
	 */
	QString nextItem() const {
		return itemText( nextId() );
	}

	/** Metoda wyszukuje na li�cie podan� nazw�.
	 * @param sItemName - nazwa �cie�ki do odszukania.
	 * @return -1 je�li nazwa nie zosta�a odnaleziona, Id je�li j� znaleziono.
	 */
	int findItem( const QString & sItemName ) const;

	/** Zwraca Id poprzednie wzgl�dem Id bie��cego elementu.
	 * @return Id poprzedniego elementu, je�li bie��cym jest pierwszy to ostatni.
	 */
	uint prevId() const {
		return (currentIndex() > 0) ? currentIndex()-1 : count()-1;
	}

	/** Zwraca Id nast�pne wzgl�dem Id bie��cego elementu.
	 * @return Id nast�pnego elementu, je�li bie��cym jest ostatni to 0.
	 */
	uint nextId() const {
		return (count() == 0) ? 0 : currentIndex()+1;
		//return (currentItem() < count()-1) ? currentItem()+1 : 0;
	}

};

#endif
