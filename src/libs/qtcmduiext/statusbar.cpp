/***************************************************************************
                          statusbar.cpp  -  description
                             -------------------
    begin                : sun oct 27 2002
    copyright            : (C) 2002 by Piotr Mierzwiński
    email                : peterm@go2.pl

    copyright            : See COPYING file that comes with this project
 ***************************************************************************/

#include <QTimer>
#include <QFrame>

#include "statusbar.h"


StatusBar::StatusBar( QWidget *pParent )
	: SqueezeLabel( pParent )
{
	setFrameStyle( QFrame::Panel | QFrame::Sunken );
	setAlignment( Qt::AlignCenter );
	setScaledContents( FALSE ); // for list status

	m_pProgress = new QProgressBar( this );
	m_pProgress->setAlignment( Qt::AlignHCenter ); // centred precentage value
	m_pProgress->setRange( 0, 100 );
	m_pProgress->hide();

	m_nHtmlTagsWidth = fontMetrics().width( "<font color=red><b></b></font><b></b><font color=red><b></b></font>" );
	m_nNumOfSelectedItems = 0;  m_nNumOfAllItems = 0;  m_nWeightOfSelectedItems = 0;

	m_eSizeOfMessage = BigMSG;
}


StatusBar::~StatusBar()
{
	delete m_pProgress;
}


void StatusBar::clear( bool bProgress )
{
	if ( bProgress )
		m_pProgress->hide();
	else
		setText("");
}


void StatusBar::setStatusList()
{
	QString sNoSI = QString::number( m_nNumOfSelectedItems );
	QString sNoAI = QString::number( m_nNumOfAllItems );
	QString sWoSI = QString::number( m_nWeightOfSelectedItems );

	uint nInsertPos = sWoSI.length();
	// make (max) three digits group, eg. input = 12345,  output = 12 345
	if ( m_eSizeOfMessage != SmallMSG ) {
		while ( nInsertPos > 3 )  {
			nInsertPos -= 3;
			sWoSI.insert( nInsertPos, " " );
		}
	}

	if ( m_eSizeOfMessage == BigMSG )  {
		m_sListStatus = tr("Selected")+" <font color=red><b>"+sNoSI+"</b></font> "
			+ tr("files, from")+ " <b>"+sNoAI+"</b>, "+tr("about total size")+" <font color=red><b>"+sWoSI+"</b></font> "
			+tr("bytes");
	}
	else if ( m_eSizeOfMessage == TinyMSG )  {
		m_sListStatus = "<font color=red><b>"+sNoSI+"</b></font> "+tr("files, from")+" <b>"+sNoAI+"</b>,"
			+ tr("size")+" <font color=red><b>"+sWoSI+"</b></font>";
	}
	else if ( m_eSizeOfMessage == SmallMSG )  {
		m_sListStatus = "<font color=red><b>"+sNoSI+"</b></font> "+", <b>"+sNoAI+"</b>, "
			+"<font color=red><b>"+sWoSI+"</b></font>";
	}

	m_sStatus = m_sListStatus;
}


void StatusBar::squeezeListStatus()
{
	m_ntWidthOfListStatus[m_eSizeOfMessage] = fontMetrics().width( m_sListStatus ) - m_nHtmlTagsWidth;
	uint nWidgetWidth = width();

	if ( nWidgetWidth < m_ntWidthOfListStatus[m_eSizeOfMessage]+3 ) {
		if ( m_eSizeOfMessage > SmallMSG ) {
			int nSize = m_eSizeOfMessage;
			nSize--;
			m_eSizeOfMessage = (SizeOfMessage)nSize;
			//(int)m_eSizeOfMessage -= 1;
		}
		setStatusList();
	}
	else { // widget is wider than string
		int sil = ((int)m_eSizeOfMessage+1 > BigMSG) ? (int)m_eSizeOfMessage : (int)m_eSizeOfMessage+1;
		// whether widget is wider than string (level current+1 - here: Tiny+1 = Big )
		if ( nWidgetWidth > m_ntWidthOfListStatus[sil]+3 ) { // yes, cat make wide string
			if ( m_eSizeOfMessage < BigMSG ) {
				int nSize = m_eSizeOfMessage;
				nSize++;
				m_eSizeOfMessage = (SizeOfMessage)nSize;
				//(int)m_eSizeOfMessage += 1;
			}
		}
	}

	setStatusList();
}

// -------- SLOTs --------

void StatusBar::slotSetProgress( int nValue )
{
	if ( nValue == 101 ) {
		m_pProgress->hide();
		return;
	}

	if ( ! m_pProgress->isVisible() ) {
		m_pProgress->move( mapToGlobal(QPoint(0,0)) );
		m_pProgress->show();
	}

	m_pProgress->setValue( nValue );

	if ( nValue == 100 )
		m_pProgress->hide();
}


void StatusBar::slotSetMessage( const QString & sMessage, uint nMiliSec, bool bShowPrevious )
{
	m_sOldStatus = m_sStatus;
	m_bShowPrevMsg  = bShowPrevious;

	m_bShowListStatus = (sMessage.contains("</b>")); // or "</font>"
	if ( m_bShowListStatus ) {
		setStatusList();
		squeezeListStatus();
	}
	else
		m_sStatus = sMessage;

	setText( m_sStatus );

	if ( nMiliSec )
		QTimer::singleShot( nMiliSec, this, SLOT(slotTimerDone()) );
}


void StatusBar::slotUpdateOfListStatus( uint nNumOfSelectedItems, uint nNumOfAllItems, uint nWeightOfSelected )
{
	m_nNumOfSelectedItems = nNumOfSelectedItems;
	m_nNumOfAllItems      = nNumOfAllItems;
	m_nWeightOfSelectedItems = nWeightOfSelected;

	m_eSizeOfMessage = BigMSG;
	setStatusList();
	squeezeListStatus();

	setScaledContents( FALSE );
	setText( m_sStatus );
}


void StatusBar::slotTimerDone()
{
	setText( m_bShowPrevMsg ? m_sOldStatus : QString("") );
}

// -------- EVENTs --------

void StatusBar::resizeEvent( QResizeEvent * )
{
	if ( m_pProgress->isVisible() )
		m_pProgress->resize( width(), height() );

	if ( m_bShowListStatus ) {
		squeezeListStatus();
		setText( m_sStatus ); // jesli w setStatusList() na koncu znajdzie sie setText, to ten tu jest zbedny
	}
}

