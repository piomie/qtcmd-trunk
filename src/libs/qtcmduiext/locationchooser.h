
/***************************************************************************
                          locationchooser.h  -  description
                             -------------------
    begin                : Wed Mar 17 2004
    copyright            : (C) 2004 by Piotr Mierzwi�ski
    email                : peterm@go2.pl

    copyright            : See COPYING file that comes with this project

 $Id$
 ***************************************************************************/

#ifndef LOCATIONCHOOSER_H
#define LOCATIONCHOOSER_H

#include "comboboxext.h"

#include <QWidget>

#include <QMenu>
#include <QLineEdit>
#include <QGroupBox>
#include <QPushButton>
#include <QToolButton>
#include <QHBoxLayout>


/**
 * Author:
 * Piotr Mierzwi�ski
 **/
/** @short Klasa wspomagaj�ca wyb�r pliku lub katalogu.
 * Obiekt tej klasy sk�ada si� z widgetu pobieraj�cego (linijka edycyjna lub
 * lista typu 'ComboBoxExt') oraz przycisku z menu, kt�re zawiera opcje zwi�zane z
 * widgetem pobieraj�cym. Ka�dy element w menu mo�na ukry� i pokaza� jak r�wnie�
 * uczyni� nieaktywnym lub aktywnym. Po wstawieniu �cie�ki katalogu wysy�any jest
 * sygna�: @em locationChanged() . Wygl�d widgetu mo�na modyfikowa� za pomoc�
 * dost�pnych metod. Mo�liwa jest zmiana widgetu pobieraj�cego na linijk�
 * edycyjn� lub list� ComboBoxExt, przycisk z menu mo�e by� umieszczony z lewej, lub
 * z prawej strony widgetu pobieraj�cego, mo�liwe jest r�wnie� jego usuni�cie.
 * Lista �cie�ek z widetu ComboBoxExt mo�e by� automatycznie odczytywana oraz
 * zapisywana do pliku konfiguracyjnyego (poprzez metod�), je�li ustawiony
 * zostanie dla niego klucz.
 */
class LocationChooser : public QWidget
{
	Q_OBJECT

	Q_ENUMS( GetMode )
	Q_PROPERTY( GetMode getMode READ getMode WRITE setGetMode )
	Q_ENUMS( ButtonSide )
	Q_PROPERTY( ButtonSide buttonSide READ buttonSide WRITE setButtonSide )
	Q_PROPERTY( bool showSeparator READ showSeparator WRITE setShowSeparator )
	Q_ENUMS( GettingWidget )
	Q_PROPERTY( GettingWidget gettingWidget READ gettingWidget WRITE setGettingWidget )
//	Q_PROPERTY( bool showPrevNextActions READ showPrevNextActions WRITE setShowPrevNextActions )

public:
	/** Tryb pobierania scie�ki (dla pliku albo dla katalogu).
	 */
	enum GetMode { GetDirectory=0, GetFile };

	/** Identyfikatory akcji w menu przycisku.
	 */
	enum ActionMenu { CURRENT_DIR=0, ORIGINAL_DIR, HOME_DIR, ROOT_DIR, UP_DIR, PREVIOUS_DIR, NEXT_DIR, SELECT_DIR, CLEAR_CURRENT, CLEAR_ALL };

	/** Rodzaj obiektu pokazywanego obok przycisku z menu.
	 */
	enum GettingWidget { LineEdit=0, ComboBox };

	/** Rodzaj strony, na kt�rej b�dzie pokazany przycisk, lub jego brak.
	 */
	enum ButtonSide { LeftButton=0, RightButton, NoneButton };

	/** Rodzaj strony, wybieranej przy ustawianiu akcji dla multi-przycisku.
	 */
	enum MultiButtonSide { LeftMultiBtn=0, RightMultiBtn, BottomMultiBtn };


	/** Konstruktor klasy.
	 * @param pParent - wska�nik na rodzica,
	 * Inicjowane s� tutaj sk�adowe oraz tworzone obiekty sk�adaj�ce si� na bie��cy.
	 */
	LocationChooser( QWidget *pParent );

	/** Destruktor.
	 * Usuwane s� tu obiekty u�ywane w klasie.
	 */
	~LocationChooser();


	/** Metoda powoduje wczytanie listy element�w na list�.
	 *  Przy czym jest robione tylko je�li obiektem pobieraj�cym jest ComboBoxExt oraz
	 *  nazwa klucza dla pliku konfiguracyjnego nie jest pusta.
	 *  Patrz te�: @see setKeyListReading.
	 */
	void init();

	/** Zapisuje do pliku konfiguracyjnego list� oraz bie��cy jej indeks z obiektu
	 * pobieraj�cego ComboBoxExt.
	 * Przy czym korzysta z wcze�niej ustawionego klucza, je�li jest on pusty to
	 * nic nie jest robione.
	 * Patrz te�: @see setKeyListReading.
	 */
	void saveSettings();


	/** Ustawia bie��c� �cie�k� aplikacji.
	 * @param sCurrentPath - nazwa bie��cej �cie�ki.
	 */
	void setCurrentPath( const QString & sCurrentPath )   { m_sCurrentPath  = sCurrentPath;  }

	/** Ustawia �cie�k�, kt�ra by�a bie��c�.
	 * @param sOriginalPath - poprzednia bie��ca �cie�ka.
	 */
	void setOriginalPath( const QString & sOriginalPath ) { m_sOriginalPath = sOriginalPath; }


	/** Ukrywa b�d� pokazuje element o podanym indeksie w menu widoku.
	 * @param eAction - identyfikator elementu menu (patrz @see ActionMenu),
	 * @param bVisible - TRUE, oznacza �e element b�dzie widoczny, FALSE powoduje jego
	 * ukrycie.
	 */
	void setMenuItemVisible( ActionMenu eAction, bool bVisible );

	/** Uaktywnia lub dezaktywuje element o podanym indeksie w menu widoku.
	 * @param eAction - identyfikator elementu menu (patrz @see ActionMenu),
	 * @param bEnable - TRUE, oznacza �e element b�dzie aktywny, FALSE powoduje jego
	 * dezaktywacje.
	 */
	void setMenuItemEnabled( ActionMenu eAction, bool bEnable );

	/** Ustawia focus na widget-cie pobierania �cie�ki (QLineEdit).
	 */
	void setFocus() { m_pLineEdit ? m_pLineEdit->setFocus() : m_pComboBoxExt->setFocus(); }

	/** Ustawia polityk� focusa.
	*/
	void setFocusPolicy( Qt::FocusPolicy focusPolicy );

	/** Metoda ustawia nazw� klucza (dla pliku konfiguracyjnego), z kt�rego nale�y
	 * odczyta� list� URLi.
	 * @param sKeyReadingStr - pe�na nazwa klucza,
	 * @param bReadSettings - odczytaj list� �cie�ek.
	 */
	void setKeyListReading( const QString & sKeyReadingStr, bool bReadSettings=FALSE );

	/** Powoduje zaznaczenie bie��cej zawarto�ci widgetu pobieraj�cego.
	 */
	void selectAll();


	/** Powoduje wstawienia podanego elementu na podanej pozycji na list� ComboBox.
	 * @param sItemStr - ci�g tekstowy do wstawienia,
	 * @param nItemId - pozycja, na kt�rej nale�y umie�ci� ci�g.
	 * Metoda obs�uguje obiekt pobierania ComboBox, je�li jest nim LineEdit, wtedy
	 * poday ci�g jest ustawiany na nim. Je�li podany ci�g ju� istnieje na li�cie,
	 * wtedy nie jest on wstawiany.
	 */
	void insertItem( const QString & sItemStr, uint nItemId=0 ) {
		if ( m_pComboBoxExt != NULL )
			m_pComboBoxExt->insertItem( sItemStr, nItemId );
	}

	/** Wstawiana jest tu podana lista ci�g�w tekstowych na list� ComboBox.
	 * @param slStringList - lista do wstawienia.
	 */
	void insertStringList( const QStringList & slStringList, bool bClearAllBefore=FALSE ) {
		if ( m_pComboBoxExt )
			m_pComboBoxExt->insertStringList( slStringList, bClearAllBefore );
	}

	/** Powoduje usuni�cie podanego ci�gu tekstowego z listy.
	 * @param sItemStr - ci�g do usuni�cia.
	 */
	void removeItem( const QString & sItemStr ) {
		if ( m_pComboBoxExt != NULL )
			m_pComboBoxExt->removeItem( sItemStr );
	}


	/** Zwraca nast�pny wzgl�dem bie��cego element listy.
	 * @return nast�pny element listy ComboBox.
	 * Przy czym, je�li bie��cy jest ostatni, wtedy zwracany jest pierwszy.
	 */
	QString nextEntry() const {
		return (m_pComboBoxExt != NULL) ? m_pComboBoxExt->nextItem() : QString::null;
	}

	/** Zwraca poprzedni wzgl�dem bie��cego element listy.
	 * @return poprzedni element listy ComboBox.
	 * Przy czym, je�li bie��cy jest pierwszy, wtedy zwracany jest ostatni.
	 */
	QString prevEntry() const {
		return (m_pComboBoxExt != NULL) ? m_pComboBoxExt->prevItem() : QString::null;
	}

	/** Zwraca poprzedni wzgl�dem bie��cego identyfikator elementu listy 'ComboBoxExt'.
	 * @return identyfikator poprzedniego elementu, lub -1 je�li bie��cy obiekt
	 * nie posiada listy 'ComboBoxExt'.
	 */
	int prevItemId() const {
		return (m_pComboBoxExt != NULL) ? (int)m_pComboBoxExt->prevId() : -1;
	}

	/** Zwraca ilo�� wszystkich element�w w li�cie 'ComboBoxExt'.
	 */
	int count() const {
		return (m_pComboBoxExt != NULL) ? m_pComboBoxExt->count() : -1;
	}

	/** Zwraca aktualn� zawarto�� obiektu pobieraj�cego.
	 * @param nId - indeks ci�gu.
	 * @return ci�g tekstowy
	 */
	QString text( int nId=-1 ) const;

	/** Zwraca identyfikator bie��cego elementu listy.
	 */
	int currentId() const { return m_pComboBoxExt->currentIndex(); }

	/** Zwraca wska�nik do menu przycisku.
	 */
	QMenu * buttonMenu() const { return m_pBottomBtnMenu; }

	/** Zwraca wska�nik do obiektu QLineEdit dla obiektu ComboBoxExt.
	 * @return wska�nik na obiekt, 0 je�li aktualny obiekt nie posiada ComboBoxExt.
	 */
	QLineEdit * lineEdit() const {
		return (m_pComboBoxExt) ? m_pComboBoxExt->lineEdit() : NULL;
	}

	/** Zwraca informacj� o rodzaju pobierania (plik/katalog) poprzez akcje
	 'Select directory' z menu przycisku.\n
	 @return rodzaj pobierania, patrz @see GetMode.\n
	 W�a�ciwo�� domy�lnie ustawiona jest na 'GetDirectory'.
	 */
	GetMode getMode() const { return m_eGetMode; }

	/** Zwraca informacj� o tym czy pokazywany jest separator pomi�dzy widget-em
	 * pobieraj�cym a przyciskiem z menu.
	 * @return TRUE - separator jest pokazywany, FALSE - nie jest.
	 * Domy�lnie separator jest pokazywany.
	 */
	bool showSeparator() const { return m_bShowSeparator; }

	/** Zwraca informacj� o tym, z kt�rej strony jest pokazywany przycisk z menu,
	 * albo czy nie jest pokazywany.
	 * @return strona pokazywania, patrz te�: @see ButtonSide.
	 */
	ButtonSide buttonSide() const { return m_eButtonSide; }

	/** Zwraca informacj� o tym czy pokazywany jest aktualnie multi-przycisk.
	 * @return TRUE multi-przycisk jest pokazywany, FALSE - wy�wietlany jest
	 * pojedynczy przycisk z popupMenu.
	 */
	bool multiButton() const { return m_bMultiButton; }

	/** Zwraca informacj� o rodzaju widgetu pobieraj�cego.
	 * @return identyfikator widgetu, patrz te�: @see GettingWidget.
	 */
	GettingWidget gettingWidget() const { return m_eGettingWidget; }


	/** Powoduje pokazanie listy ComboBoxExt.
	 */
	void showPopupList() {
		if ( m_pComboBoxExt != NULL )
			m_pComboBoxExt->showPopup();
	}

	/** Metoda umo�liwia pokazanie menu przycisku.
	 */
	void showButtonMenu() {
		m_pBottomBtnMenu->popup( mapToGlobal(QPoint(0, y()+height())) );
	}

	/** Powoduje ustawienie jako bie��cego elementu na li�cie tego o podanym
	 * indeksie.
	 * @param nIndex - nowa bie��ca warto�� indeksu na li�cie.
	 */
	void setCurrentItem( int nIndex ) {
		if ( m_pComboBoxExt != NULL )
			m_pComboBoxExt->setCurrentIndex( nIndex );
	}

public slots:
	/** Ustawia podany tekst na linijce edycyjnej.
	 * @param sText - tekst do ustawienia.
	 */
	void setText( const QString & sText );

	/** Ustawia tryb pobierania �cie�ki przez dialog pokazywany po wybraniu opcji
	 "Select directory" z menu przycisku.
	 * @param mode - tryb pobierania (patrz: @see GetMode).
	 */
	void setGetMode( GetMode eMode ) { m_eGetMode = eMode; }

	/** Ustawia stron� pokazywania przycisku lub usuwa go.
	 * @param eBtnSide - strona dla przycisku lub wymuszenie usuni�cia go.
	 * Po wywo�aniu tej metody widget jest przebudowywany.
	 */
	void setButtonSide( ButtonSide eBtnSide );

	/** W��cza wy�wietlanie tzw. multi-przycisku.
	 * @param bMultiButton - r�wne TRUE powoduje wy�wietlenie multi-przycisku,
	 * w przeciwnym razie wy�wietlany jest pojedynczy.
	 * Multi-przycisk r�ni si� tym od pojedynczego, �e sk�ada si� z trzech przycisk�w.
	 * Dwa znajduj� si� na g�rze a jeden pod nimi.
	 */
	void setMultiButton( bool bMultiButton );

	/** Ustawia podan� akcje dla lewego przycisku z multi-przycisku.
	 * @param eAction - kod akcji.
	 */
	void setLeftButtonAction( ActionMenu eAction ) {
		setButtonAction( LeftMultiBtn, eAction );
	}

	/** Ustawia podan� akcje dla prawego przycisku z multi-przycisku.
	 * @param eAction - kod akcji.
	 */
	void setRightButtonAction( ActionMenu eAction ) {
		setButtonAction( RightMultiBtn, eAction );
	}

	/** Ustawia podan� akcje dla dolnego przycisku z multi-przycisku lub pojedynczego
	 * przycisku.
	 * @param action - kod akcji.
	 */
	void setBottomButtonAction( ActionMenu eAction ) {
		setButtonAction( BottomMultiBtn, eAction );
	}

	/** Metoda powoduje pokazanie lub ukrycie separatora pomi�dzy widget-em
	 * pobieraj�cym a przyciskiem z menu.
	 * @param bShow - r�wne TRUE, spowoduje pokazanie separatora, w przeciwnym razie
	 * zostanie on ukryty.
	 */
	void setShowSeparator( bool bShow );

	/** Umo�liwia zmian� widgetu pobieraj�cego na podany.
	 * @param eGetWidget - typ nowego widgetu, patrz te�: @see GettingWidget
	 * Po wywo�aniu tej metody widget jest przebudowywany.
	 */
	void setGettingWidget( GettingWidget eGetWidget );

signals:
	/** Sygna� informuje o zmianie �cie�ki w widgetcie pobieraj�cym.
	 * @param sNewURL - nowa �cie�ka w widoku.
	 * Sygna� jest wysy�any po wstawieniu �cie�ki do widoku.
	 */
	void locationChanged( const QString & sNewURL );

	/** Sygna� wysy�any w momencie zmiany zawarto�ci linijki edycyjnej.
	 * @param sText - nowy tekst
	 */
	void textChanged( const QString & sText );


private slots:
	/** Slot u�ywany przez menu przycisku do uruchamiania jego opcji.
	 * @param pAction - wybrana opcja.
	 */
	void slotActionTriggered( QAction * pAction );

	/** Slot powoduje ustawienie �cie�ki do bie��cego (np. panelu) katalogu w widoku
	 * URLa.
	 */
	void slotSetCurrentDir() {
		selectMenuItem( CURRENT_DIR );
	}

	/** Slot powoduje ustawienie �cie�ki do katalogu pierwotnego w widoku URLa.
	 */
	void slotSetOriginalDir() {
		selectMenuItem( ORIGINAL_DIR );
	}

	/** Slot powoduje ustawienie �cie�ki do katalogu domowego w widoku URLa.
	 */
	void slotSetHomeDir() {
		selectMenuItem( HOME_DIR );
	}

	/** Slot powoduje ustawienie �cie�ki do katalogu korzenia w widoku URLa.
	 */
	void slotSetRootDir() {
		selectMenuItem( ROOT_DIR );
	}

	/** Slot powoduje ustawienie �cie�ki do poprzedniego katalogu (z listy)
	 * w widoku URLa.
	 */
	void slotSetPreviousDir() {
		selectMenuItem( PREVIOUS_DIR );
	}

	/** Slot powoduje ustawienie �cie�ki do nast�pnego katalogu (z listy)
	 * w widoku URLa.
	 */
	void slotSetNextDir() {
		selectMenuItem( NEXT_DIR );
	}

	/** Slot powoduje ustawienie �cie�ki do katalogu o poziom wy�szego w widoku
	 * URLa.
	 */
	void slotUpDir() {
		selectMenuItem( UP_DIR );
	}

	/** Slot pokazuje dialog pobierania �cie�ki do pliku lub katalogu.
	 * To czy b�dzie wybierany plik czy katalog zale�y od ustawienia sk�adowej
	 * @em m_eGetMode . Patrz te�: @see setGetMode().
	 */
	void slotSelectDirectory();

	/** Slot powoduje wyczyszczenie bie��cego widoku URLa.
	 */
	void slotClearCurrent() {
		selectMenuItem( CLEAR_CURRENT );
	}

	/** Slot powoduje wyczyszczenie listy wszystkich URLi.
	 */
	void slotClearAll() {
		selectMenuItem( CLEAR_ALL );
	}

	/** Slot uruchamiany w momencie wci�ni�cia klawisza ENTER/RETURN.
	 */
	void slotReturnPressed();

private:
	QString m_sCurrentPath, m_sOriginalPath;

	GetMode m_eGetMode;
	ButtonSide m_eButtonSide;
	GettingWidget m_eGettingWidget;
	bool m_bShowSeparator;

	QHBoxLayout *m_pHBoxLayout;
	QGroupBox   *m_pGroupBox;

	QLineEdit   *m_pLineEdit;
	ComboBoxExt *m_pComboBoxExt;
	QToolButton *m_pBottomButton;

	QMenu       *m_pBottomBtnMenu;

	bool m_bMultiButton;
	QPushButton *m_pLeftBtn, *m_pRightBtn;
	ActionMenu m_eLeftBtnAction, m_eRightBtnAction, m_eBottomBtnAction;

	QString m_sKeyListReading;
	QStringList m_slActionsListName;
	QList <QAction *> m_ptActionsList;

	/** Usuwa wszystkie obiekty widgetu.
	*/
	void removeWidget();

	/** Metoda powoduje budowanie widgetu sk�adaj�cego si� z przycisku oraz widgetu
	 * pobieraj�cego.
	 * Wykonywane jest to na podstawie sk�adowych klasy okre�laj�cych wygl�d.
	 */
	void createWidget();

	/** Metoda powoduje utworzenie przycisku z menu.
	 */
	void createChooserButton();

	/** Metoda powoduje ustawienie podanej akcji dla wybranego przycisku
	 * z multi-przycisku.
	 */
	void setButtonAction( MultiButtonSide eMultiButtonSide, ActionMenu eAction );

	/** Funkcja u�ywana przez sloty podl�czane do lewego i/lub prawego przycisku.
	 * @param eAction - identyfikator opcji menu, patrz @see ActionMenu.
	 */
	void selectMenuItem( ActionMenu eAction );

};

#endif
