/***************************************************************************
                          comboboxext.cpp  -  description
                             -------------------
    begin                : Tue Sep 21 2004
    copyright            : (C) 2004 by Piotr Mierzwiński
    email                : peterm@go2.pl

    copyright            : See COPYING file that comes with this project

 $Id$
 ***************************************************************************/

#include "comboboxext.h"


ComboBoxExt::ComboBoxExt( unsigned int nMaxItems, bool /*bRW*/, QWidget *pParent, const char */*sz_pName*/ )
//	: QComboBox(bRW, pParent, sz_pName)
	: QComboBox(pParent)
{
	setInsertPolicy( QComboBox::NoInsert ); // turn off auto-insertion after Enter/Return pressed
	setDuplicatesEnabled( FALSE );
	setMaxCount( nMaxItems );
	setEditable( TRUE );
}


void ComboBoxExt::insertItem( const QString & sItem, int nItemId )
{
// 	if ( sItem.at(sItem.length()-1) != '/' ) // inserts path to dir only
// 		return;
	if ( sItem.isEmpty() )
		return;

	int nId = findItem( sItem ); // int next = nextId();
	if ( nId < 0 ) // item not bFound
		QComboBox::insertItem( (nItemId<0) ? nextId() : nItemId, sItem ); // force insertion policy == AfterCurrent

	setCurrentIndex( (nId < 0) ? (nItemId<0 ? nextId() : nItemId) : nId );

	if (count() > maxCount()) // if all item is too much then
		QComboBox::removeItem( maxCount() ); // cut last one
}


void ComboBoxExt::insertStringList( const QStringList & slItems, bool bClearAllBefore )
{
	if ( bClearAllBefore )
		clear();

	for ( int i=0; i<slItems.count(); i++ )
		insertItem( slItems[i], i );
}


void ComboBoxExt::removeItem( const QString & sItem )
{
	int nId = findItem( sItem );
	if ( nId > 0 )
		QComboBox::removeItem( nId );
	else
		qDebug("ComboBoxExt::removeItem(). Not found item='%s'", sItem.toLatin1().data());
}


int ComboBoxExt::findItem( const QString & sItem ) const
{
	int nId;
	bool bFound = FALSE;

	for (nId=0; nId<count(); nId++ ) {
		if ( itemText(nId) == sItem ) {
			bFound = TRUE;
			break;
		}
	}

	return (bFound) ? nId : -1;
}

