/***************************************************************************
                          menuext.cpp  -  description
                             -------------------
    begin                : sun jan 27 2008
    copyright            : (C) 2008 by Piotr Mierzwiński
    email                : peterm@go2.pl

    Copyright: See COPYING file that comes with this project
***************************************************************************/
#include "menuext.h"


QAction *MenuExt::addAction( const QString & sText, const QString & sToolTip )
{
	QAction *pAction = QMenu::addAction( sText );
	if (! sToolTip.isEmpty())
		pAction->setToolTip( sToolTip );

	return pAction;
}

QAction *MenuExt::addAction( const QIcon & icon, const QString & sText, const QString & sToolTip )
{
	QAction *pAction = QMenu::addAction( icon, sText );
	if (! sToolTip.isEmpty())
		pAction->setToolTip( sToolTip );

	return pAction;
}

