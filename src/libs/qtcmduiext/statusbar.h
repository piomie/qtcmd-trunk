/***************************************************************************
                          statusbar.h  -  description
                             -------------------
    begin                : sun oct 27 2002
    copyright            : (C) 2002 by Piotr Mierzwi�ski
    email                : peterm@go2.pl

    copyright            : See COPYING file that comes with this project
 ***************************************************************************/

#ifndef _STATUSBAR_H_
#define _STATUSBAR_H_

#include <QProgressBar>
#include <QResizeEvent>

#include "squeezelabel.h"

/**
 * @author Piotr Mierzwi�ski
 **/
/** @short Klasa odpowiedzialna za obs�ug� paska statusu.
 * Dzi�ki dziedziczeniu po klasie SqueezeLabel informacja na pasku mo�e by�
 * �ciskana. Poza tym obs�ugiwane jest tutaj dostosowywanie d�ugo�ci informacji
 * dla panela plik�w zale�ne od szeroko�ci paska. Dost�pne s� trzy tryby
 * pokazywania, tj.: ma�y (SmallMSG), �redni (TinyMSG) oraz du�y (BigMSG).
 * Prze��czanie pomi�dzy trybami jest wykonywane automatycznie przy zmianie
 * szeroko�ci paska. Dodatkowo klasa wspiera pokazywanie paska post�pu, kt�ry
 * przykrywa ca�y pasek statusu. Wspierane jest tak�e czasowe pokazywanie
 * informacji i po jego mini�ciu przywracany jest poprzedni lub nast�puje
 * czyszczenie widoku.
 */
class StatusBar : public SqueezeLabel
{
	Q_OBJECT
public:
	/** Konstruktor klasy.
	 * @param pParent - wska�nik na rodzica.
	 * Nast�puje tutaj inicjowanie obiektu klasy oraz utworzenie obiektu
	 * paska post�pu.
	 */
	StatusBar( QWidget *pParent );

	/** Destruktor.
	 * Usuwany jest tutaj obiekt paska post�pu.
	 */
	~StatusBar();

	/** Metoda czy�ci pasek statusu lub chowa pasek post�pu.
	 * @param bProgress - TRUE wymusza schowanie paska post�pu, w przeciwnym
	 * razie czyszczona jest zawrto�� paska statusu.
	 */
	void clear( bool bProgress=FALSE );

	/** W��cza/wy��cza �ciskanie zawarto�ci paska.
	 * @param bSqueezing - TRUE w��cza �ciskanie, FALSE wy��cza je.
	 */
	void setSqueezingText( bool bSqueezing ) { setScaledContents( bSqueezing ); }

private:
	QProgressBar *m_pProgress;

	uint m_nHtmlTagsWidth, m_ntWidthOfListStatus[3];
	uint m_nNumOfAllItems, m_nNumOfSelectedItems, m_nWeightOfSelectedItems;
	QString m_sStatus, m_sOldStatus, m_sListStatus;
	bool m_bShowListStatus, m_bShowPrevMsg;

	/** Rodzaj trybu pokazywania informacji dla panela plik�w.
	 */
	enum SizeOfMessage { SmallMSG, TinyMSG, BigMSG };
	SizeOfMessage m_eSizeOfMessage;

protected:
	/** Inicjuje sk�adow� zawieraj�c� informacje do pokazania na pasku o d�ugo�ci,
	 * na kt�r� wskazuje tryb pokazywania (patrz @see SizeOfMessage).
	 * Powoduje przypisnie do sk�adowej @em m_sListStatusStr wskazywanego przez
	 * @em m_eSizeOfMessage' odpowiedniej d�ugo�ci ci�gu informacyjnego, kt�ry zawiera
	 * informacje o ilo�ci wszystkich element�w na li�cie, ilo�ci zaznaczonych, oraz
	 * wadze wszystkich zaznaczonych plik�w. Pe�na informacja wygl�da nast�puj�ca:
	 * "Zaznaczono X plik�w z Y, o ca�kowitym rozmiarze Z bajt�w", skr�cona:
	 * "X plik�w z Y, rozmiar Z", a najkr�tsza tak: "X, Y, Z".
	 */
	void setStatusList();

	/** Ustawiana jest tutaj sk�adowa zawieraj�ca informacj� o trybie pokazywania
	 * informacji na pasku.
	 * Po ustawieniu sk�adowej wywo�ywana jest funkcja @em setStatusList .
	 */
	void squeezeListStatus();

	/** Obs�uga zdarzenie zmiany rozmiaru.
	 * Pasek post�pu dostaje bie��c� szeroko�� paska, wywo�ywana jest tutaj r�wnie�
	 * funkcja ustawiaj�ca tryb pokazywania informacji, po czym jest ona wy�wietlana
	 */
	void resizeEvent( QResizeEvent * );

public slots:
	/** W slocie jest uaktualniana warto�� paska statusu.
	 * @param nValue - procentowa warto�� post�pu.
	 * Je�li pasek post�pu nie jest widoczny to jest pokazywany, podanie warto�ci
	 * 101 spowoduje ukrycie go, tak samo si� dzieje po osi�gni�ciu warto�ci 100
	 * przez pasek post�pu.
	 */
	void slotSetProgress( int nValue );

	/** Ustawia podany ci�g na pasku i ewentualnie ukrywa po mini�ciu okre�lonego czasu.
	 * @param sMessage - informacja do ustawienia,
	 * @param nMiliSec - liczba milisekund, po kt�rej nast�pi ukrycie podanej informacji,
	 * @param bShowPrevious - TRUE wymusza pokazanie poprzedniej zawarto�ci paska,
	 * FALSE powoduje poprostu wyczyszczenie go, kiedy minie podany czas.
	 */
	void slotSetMessage( const QString & sMessage, uint nMiliSec=0, bool bShowPrevious=TRUE );

	/** Slot powoduje uaktualnianie statusu dla panela plik�w informacj� o ilo�ci
	 * i wadze jego plik�w.
	 * @param nNumOfSelectedItems - liczba zaznaczonych element�w,
	 * @param nNumOfAllItems - liczba wszystkich element�w,
	 * @param nWeightOfSelected - waga wszystkich zaznaczonych plik�w.
	 */
	void slotUpdateOfListStatus( uint nNumOfSelectedItems, uint nNumOfAllItems, uint nWeightOfSelected );

private slots:
	/** Slot przywraca poprzedni� informacje lub czy�ci widok.
	 * ( w zale�no�ci od warto�ci sk�adowej @em m_bShowPrevMsg ustawianej w
	 * @em slotSetMessage() )
	 */
	void slotTimerDone();

};

#endif
