/***************************************************************************
                          menuext.h  -  description
                             -------------------
    begin                : sun jan 27 2008
    copyright            : (C) 2008 by Piotr Mierzwi�ski
    email                : peterm@go2.pl

    Copyright: See COPYING file that comes with this project
***************************************************************************/
#ifndef MENUEXT_H
#define MENUEXT_H

#include <QMenu>

/**
	@author Piotr Mierzwi�ski <piotrmierzwinski@gmail.com>
 */
/**
 @short Klasa dostarcza obiektu QMenu z rozszerzeniem ToolTip na elemencie.
 Rozszerzenie polega na tym, �e dla wybranego (po QAction *) elementu menu mo�na
 ustawi� tzw. dymek (QToolTip), kt�ry pojawia� si� b�dzie, gdy kursor myszy
 znajdzie si� nad tym elementem.
*/
class MenuExt : public QMenu
{
public:
	MenuExt( QWidget *pParent=NULL ) : QMenu(pParent) {}

	QAction *addAction( const QString & sText, const QString & sToolTip=QString::null );
	QAction *addAction( const QIcon & icon, const QString & sText, const QString & sToolTip=QString::null );

};

#endif
