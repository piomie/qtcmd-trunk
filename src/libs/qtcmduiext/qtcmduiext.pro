# Plik utworzony przez mened?era projekt�w kdevelopa
# ------------------------------------------- 
# Podkatalog wzgl?dem g?�wnego katalogu projektu: ./src/libs/qtcmduiext
# Cel to biblioteka qtcmduiext

INSTALLS += target 
target.path = $$PREFIX/lib 
MOC_DIR = ../../../build/.tmp/moc
UI_DIR = ../../../build/.tmp/ui
OBJECTS_DIR = ../../../build/.tmp/obj
INCLUDEPATH += ../../../icons
QMAKE_CXXFLAGS_RELEASE += -O2
QMAKE_CXXFLAGS_DEBUG += -g3
TARGET = qtcmduiext
DESTDIR = ../../../build/lib
TEMPLATE = lib
CONFIG += \
          warn_on \
          qt \
          thread \
          dll \
          precompile_header
HEADERS += \
           squeezelabel.h \
           statusbar.h \
           locationchooser.h \
           listviewfinditem.h \
           menuext.h \
           comboboxext.h \
           lwv/comboboxdata.h \
           lwv/comboboxdelegate.h \
           lwv/comboboxeditor.h \
           lwv/listwidgetview.h \
           lwv/listwidgetbutton.h \
           lwv/shortkeyeditboxdelegate.h \
           lwv/shortkeyeditboxeditor.h \
           lwv/shortkeyeditboxdata.h
SOURCES += \
           squeezelabel.cpp \
           statusbar.cpp \
           locationchooser.cpp \
           listviewfinditem.cpp \
           menuext.cpp \
           comboboxext.cpp \
           lwv/comboboxdata.cpp \
           lwv/comboboxdelegate.cpp \
           lwv/comboboxeditor.cpp \
           lwv/listwidgetview.cpp \
           lwv/listwidgetbutton.cpp \
           lwv/shortkeyeditboxdelegate.cpp \
           lwv/shortkeyeditboxeditor.cpp \
           lwv/shortkeyeditboxdata.cpp

TRANSLATIONS += \
                ../../../translations/pl/qtcmd-uiext.ts 
