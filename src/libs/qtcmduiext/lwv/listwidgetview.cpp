/***************************************************************************
                          listwidgetview.cpp  -  description
                             -------------------
    begin                : Wed Jan 02 2008
    copyright            : (C) 2008 by Piotr Mierzwiński
    email                : peterm@go2.pl

    Copyright            : See COPYING file that comes with this project

 $Id$
 ***************************************************************************/
#include <QtGui>
#include <QHeaderView>

#include "listwidgetview.h"
#include "listwidgetbutton.h"

#include "comboboxdata.h"
#include "comboboxdelegate.h"
#include "shortkeyeditboxdata.h"
#include "shortkeyeditboxdelegate.h"


ListWidgetView::ListWidgetView( QWidget *pParent ) : QTableWidget( pParent )
	, m_pParent(pParent), m_nColumnCount(0), m_nRowCount(0)
{
	verticalHeader()->setVisible( false );

	setSelectionMode( QAbstractItemView::SingleSelection );
	setSelectionBehavior( QAbstractItemView::SelectRows );
	setAlternatingRowColors( true );

	setShowGrid( false );
}


void ListWidgetView::addColumn( Widget eWidget, const QString & sColumnName, Qt::AlignmentFlag eColumnAlign )
{
	m_nColumnCount++;
	m_eRowWidgetList << eWidget;
	m_slHeaderLabels << sColumnName;
	m_eColumnAlignList << eColumnAlign;
// do not to change order
	setColumnCount( m_nColumnCount );
	setHorizontalHeaderLabels( m_slHeaderLabels );
}

void ListWidgetView::addRow( bool bButtonsExclusive )
{
	insertRow( m_nRowCount );
// m_eColumnAlignList
	QButtonGroup *pButtonGrp = NULL;
	QTableWidgetItem *pItem = NULL;
	QStringList slEmptyList;

	for ( int nCol=0; nCol < m_nColumnCount; nCol++ ) {
		m_pTableWidgetItemList << (pItem = new QTableWidgetItem);

		/* // high for text is OK, but other widgets are not correct
		if (nCol == 0) {
			QFont cellFont( pItem->font() );
			//setRowHeight(m_nRowCount, cellFont.pixelSize()+4);
			setRowHeight(m_nRowCount, cellFont.pointSize()*2); // 9*3
		}
		*/
		setItem( m_nRowCount, nCol, pItem);
		//pItem->setTextAlignment(m_eColumnAlignList[nCol]);
		ListWidgetButton *pListWidgetButton = NULL;

		switch ( columnWidget(nCol) ) {
			case TextLabel: {
				pItem->setFlags( Qt::ItemIsEnabled | Qt::ItemIsSelectable );
				break;
			}
			case ComboBox: {
				if ( m_nRowCount == 0 ) // delegating made is one time
					setItemDelegateForColumn(nCol, new ComboBoxDelegate);
				//pItem->setData( Qt::DisplayRole, qVariantFromValue(ComboBoxData(slEmptyList)) );
				//pItem->data(Qt::DisplayRole).value<ComboBoxData>().setTextAlignment(m_eColumnAlignList[nCol]);
// 				pItem->data(Qt::DisplayRole).value<ComboBoxData>().setTextAlign(Qt::AlignHCenter);
				break;
			}
			case EditBox: {
				break;
			}
			case KeyEditBox: {
				if ( m_nRowCount == 0 ) // delegating made is one time
					setItemDelegateForColumn(nCol, new ShortKeyEditBoxDelegate);
				break;
			}
			case RadioButton: {
				if ( pButtonGrp == NULL && bButtonsExclusive ) { // make one for one row
					pButtonGrp = new QButtonGroup( this );
					pButtonGrp->setExclusive( true );
				}
				pListWidgetButton = new ListWidgetButton( pItem, ListWidgetButton::RadioButton, Qt::AlignHCenter );
				setCellWidget( m_nRowCount, nCol, pListWidgetButton );
				if ( bButtonsExclusive )
					pButtonGrp->addButton( pListWidgetButton->radioButton() );
				break;
			}
			case CheckBox: {
				pListWidgetButton = new ListWidgetButton( pItem, ListWidgetButton::CheckBox, Qt::AlignHCenter );
				//pListWidgetButton->setMinimumSize(pListWidgetButton->sizeHint().width(), pListWidgetButton->sizeHint().height()); // button put down of cell
				setCellWidget( m_nRowCount, nCol, pListWidgetButton );
				break;
			}
			case ColorChooser: {
				pListWidgetButton = new ListWidgetButton( pItem, ListWidgetButton::PushButton, Qt::AlignHCenter );
				setCellWidget( m_nRowCount, nCol, pListWidgetButton );
				break;
			}
			default:
				break;
		}
		if (columnWidget(nCol) == ColorChooser || columnWidget(nCol) == CheckBox || columnWidget(nCol) == RadioButton)
			connect( pListWidgetButton, SIGNAL(clicked()), this, SLOT(slotButtonClicked()) );
	}

	m_nRowCount++;
}



void ListWidgetView::setCellValue( int nRow, int nColumn, const QStringList &slValue, int nInitId )
{
	if ( nRow > rowCount() ) {
		qDebug() << "ListWidgetView::setCellValue: Row over the range!";
		return;
	}
	if ( nColumn > columnCount() ) {
		qDebug() << "ListWidgetView::setCellValue: Column over the range!";
		return;
	}
	Widget eWidget = columnWidget( nColumn );

	QString sValue;
	if ( eWidget != ComboBox )
		sValue = slValue[0];

	switch ( eWidget )
	{
		case EditBox:
		case TextLabel: {
			item(nRow, nColumn)->setData( Qt::DisplayRole, qVariantFromValue(sValue) );
			break;
		}
		case ComboBox: {
			if ( nInitId < 0 )
				nInitId = 0;
			else if (nInitId > slValue.count())
				nInitId = slValue.count();
			item(nRow, nColumn)->setData( Qt::DisplayRole, qVariantFromValue(ComboBoxData(slValue, nInitId, Qt::AlignHCenter)) );
			//pItem->data(Qt::DisplayRole).value<ComboBoxData>().setTextAlignment(m_eColumnAlignList[nCol]);
			//item(nRow, nColumn)->data(Qt::DisplayRole).value<ComboBoxData>().setTextAlignment(Qt::AlignHCenter); // DO NOT WORK !? :/
			break;
		}
		case KeyEditBox: {
			item(nRow, nColumn)->setData( Qt::DisplayRole, qVariantFromValue(ShortKeyEditBoxData(sValue)) );
			break;
		}
		case RadioButton: {
			ListWidgetButton *pButton = dynamic_cast<ListWidgetButton *>( cellWidget(nRow, nColumn) );
			if ( pButton != NULL )
				pButton->radioButton()->setChecked( getBoolValue(sValue) );
			break;
		}
		case CheckBox: {
			ListWidgetButton *pButton = dynamic_cast<ListWidgetButton *>( cellWidget(nRow, nColumn) );
			if ( pButton != NULL )
				pButton->checkBox()->setChecked( getBoolValue(sValue) );
			break;
		}
		case ColorChooser: {
			ListWidgetButton *pButton = dynamic_cast<ListWidgetButton *>( cellWidget(nRow, nColumn) );
			if ( pButton != NULL )
				pButton->setColor( QColor(sValue) );
			break;
		}
		default:
			break;
	}
}

QString ListWidgetView::cellValue( int nRow, int nColumn ) const
{
	if ( nRow > rowCount() ) {
		qDebug() << "ListWidgetView::cellValue: Row over the range!";
		return QString("");
	}
	if ( nColumn > columnCount() ) {
		qDebug() << "ListWidgetView::cellValue: Column over the range!";
		return QString("");
	}
	int nRowCell    = (nRow < 0)    ? currentRow()    : nRow;
	int nColumnCell = (nColumn < 0) ? currentColumn() : nColumn;

	Widget eWidget = columnWidget( nColumnCell );

	QString sValue;

	switch ( eWidget )
	{
		case EditBox:
		case TextLabel: {
			sValue = item(nRowCell, nColumnCell)->text();
			break;
		}
		case ComboBox: {
			sValue = item(nRowCell, nColumnCell)->data(Qt::DisplayRole).value<ComboBoxData>().text();
			break;
		}
		case KeyEditBox: {
			sValue = item(nRowCell, nColumnCell)->data(Qt::DisplayRole).value<ShortKeyEditBoxData>().text();
			break;
		}
		case RadioButton: {
			ListWidgetButton *pButton = dynamic_cast<ListWidgetButton *>( cellWidget(nRow, nColumn) );
			if ( pButton != NULL )
				sValue = (pButton->radioButton()->isChecked()) ? "TRUE" : "FALSE";
			break;
		}
		case CheckBox: {
			ListWidgetButton *pButton = dynamic_cast<ListWidgetButton *>( cellWidget(nRow, nColumn) );
			if ( pButton != NULL )
				sValue = (pButton->checkBox()->isChecked()) ? "TRUE" : "FALSE";
			break;
		}
		case ColorChooser: {
			ListWidgetButton *pButton = dynamic_cast<ListWidgetButton *>( cellWidget(nRow, nColumn) );
			if ( pButton != NULL )
				sValue = pButton->color().name();
			break;
		}
		default:
			break;
	}

	return sValue;
}

/*
public slots:
    void slotFindItem();

void ListWidgetView::slotFindItem()
{
	ListViewFindItem *pFind = new ListViewFindItem(this);
	pFind->show(mapToGlobal(QPoint(10, height()-10)), 300);
}
*/


bool ListWidgetView::getBoolValue( const QString &sValue )
{
	return (sValue.toUpper() == "TRUE");
}


void ListWidgetView::setCellAttributes( int nRow, int nColumn, const QColor &color, TextWeight eTextWeight )
{
	if ( nRow > rowCount() ) {
		qDebug() << "ListWidgetView::setCellAttributes: Row over the range!";
		return;
	}
	if ( nColumn > columnCount() ) {
		qDebug() << "ListWidgetView::setCellAttributes: Column over the range!";
		return;
	}
	QTableWidgetItem *pItem = item( nRow, nColumn );

	QFont cellFont( pItem->font() );
	if ( eTextWeight == ListWidgetView::Bold ) {
		cellFont.setItalic(false);
		cellFont.setBold(true);
	}
	else
	if ( eTextWeight == ListWidgetView::Italic ) {
		cellFont.setItalic(true);
		cellFont.setBold(false);
	}
	else
	if ( eTextWeight == (ListWidgetView::Bold | ListWidgetView::Italic) ) {
		cellFont.setItalic(true);
		cellFont.setBold(true);
	}
	else
	if ( eTextWeight == ListWidgetView::Normal ) {
		cellFont.setItalic(false);
		cellFont.setBold(false);
	}

	//font = pItem->font();
	if ( columnWidget(nColumn) != ComboBox ) {
		pItem->setForeground( color );
		pItem->setFont( cellFont );
	}
	else {
		item(nRow, nColumn)->data(Qt::DisplayRole).value<ComboBoxData>().setTextColor(QColor("blue")); // NOT WORK!
		//paint( QPainter * pPainter, const QRect & rect, const QPalette & /*palette*/ )
		//item(nRow, nColumn)->data(Qt::DisplayRole).value<ComboBoxData>().repaint();
	}
}

void ListWidgetView::setCellAttributes( int nRow, int nColumn, TextWeight eTextWeight )
{
	setCellAttributes( nRow, nColumn, item(nRow, nColumn)->foreground().color(), eTextWeight );
}


void ListWidgetView::setEnableCell( int nRow, int nColumn, bool bEnable, bool bEditable )
{
	if ( nRow > rowCount() ) {
		qDebug() << "ListWidgetView::setEnableCell: Row over the range!";
		return;
	}
	if ( nColumn > columnCount() ) {
		qDebug() << "ListWidgetView::setEnableCell: Column over the range!";
		return;
	}
	QTableWidgetItem *pItem = item(nRow, nColumn);
	pItem->setFlags( bEnable ? (Qt::ItemIsEnabled | Qt::ItemIsSelectable | (bEditable ? Qt::ItemIsEditable : (Qt::ItemFlags)0)) : (Qt::ItemFlags)0 );
	pItem->setTextColor( bEnable ? QColor("black") : QColor("gray") );
}

bool ListWidgetView::cellEnable( int nRow, int nColumn ) const
{
	if ( nRow > rowCount() ) {
		qDebug() << "ListWidgetView::cellEnable: Row over the range!";
		return false;
	}
	if ( nColumn > columnCount() ) {
		qDebug() << "ListWidgetView::cellEnable: Column over the range!";
		return false;
	}
	return (item(nRow, nColumn)->flags() != (Qt::ItemFlags)0);
}


void ListWidgetView::setEditableCell( int nRow, int nColumn, bool bEditable )
{
	if ( nRow > rowCount() ) {
		qDebug() << "ListWidgetView::setEditableCell: Row over the range!";
		return;
	}
	if ( nColumn > columnCount() ) {
		qDebug() << "ListWidgetView::setEditableCell: Column over the range!";
		return;
	}
	QTableWidgetItem *pItem = item(nRow, nColumn);

	if (bEditable) {
		if (! editableCell(nRow, nColumn))
			pItem->setFlags(pItem->flags() | Qt::ItemIsEditable);
	}
	else {
		if (editableCell(nRow, nColumn))
			pItem->setFlags((Qt::ItemFlags)(pItem->flags() - Qt::ItemIsEditable));
	}
}

bool ListWidgetView::editableCell( int nRow, int nColumn ) const
{
	if ( nRow > rowCount() ) {
		qDebug() << "ListWidgetView::editableCell: Row over the range!";
		return false;
	}
	if ( nColumn > columnCount() ) {
		qDebug() << "ListWidgetView::editableCell: Column over the range!";
		return false;
	}
	return (item(nRow, nColumn)->flags() & Qt::ItemIsEditable);
}


int ListWidgetView::findItem( const QString & sText, int nColumn )
{
	if ( rowCount() < 1 )
		return -1;
	if ( columnWidget(nColumn) == CheckBox || columnWidget(nColumn) == RadioButton || columnWidget(nColumn) == ColorChooser )
		return -1;

	if ( nColumn < 0 )
		nColumn = currentColumn();

	int nRow = -1;
	for ( nRow=0; nRow<rowCount(); nRow++ ) {
		if ( item(nRow, nColumn)->text() == sText )
			break;
	}

	return nRow;
}


void ListWidgetView::slotButtonClicked()
{
	ListWidgetButton *pButton = qobject_cast<ListWidgetButton *>( sender() );
	if ( pButton == NULL ) {
		return;
	}
	int nColumn = pButton->tableWidgetItem()->column();

	if ( columnWidget(nColumn) == ColorChooser && pButton->button() == ListWidgetButton::PushButton ) {
		int nRow = pButton->tableWidgetItem()->row();
		QColor color = QColorDialog::getColor( pButton->color() );
		if ( color.isValid() ) {
			setCellValue( nRow, nColumn, QStringList(color.name()) );
			emit colorChanged( item(nRow, nColumn) );
		}
	}
	else
		emit buttonClicked( pButton->tableWidgetItem() );
}

void ListWidgetView::removeRows()
{
	if (rowCount() > 0) {
		m_nRowCount = 0;
		clearContents();
		setRowCount(0);
	}
}

void ListWidgetView::commitData( QWidget * pEditor )
{
	int nColumn = currentColumn();

	if (columnWidget(nColumn) == EditBox || columnWidget(nColumn) == KeyEditBox) {
		QString sNewText = dynamic_cast<QLineEdit *>(pEditor)->text();
		if (columnWidget(nColumn) == KeyEditBox) { // commit
			if (! sNewText.endsWith('+') || sNewText == "+") // workaround for Tab key
				setCellValue( currentRow(), nColumn, sNewText );
		}
		emit editBoxTextApplied( item(currentRow(), nColumn), sNewText );
	}
	else
		QTableWidget::commitData(pEditor);
}

