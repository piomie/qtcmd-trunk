/***************************************************************************
                          listwidgetbutton  -  description
                             -------------------
    begin                : pi� lut 1 2008
    copyright            : (c) 2008 by Piotr Mierzwi�ski, Mariusz Borowski
    email                : piom@nes.pl, mariusz@nes.pl

    copyright            : See COPYING file that comes with this project

 $Id$
 ***************************************************************************/
#ifndef LISTWIDGETBUTTON_H
#define LISTWIDGETBUTTON_H

#include <QTableWidgetItem>
#include <QRadioButton>
#include <QHBoxLayout>
#include <QPushButton>
#include <QCheckBox>
#include <QWidget>

/**
	@author Piotr Mierzwi�ski <piom@nes.pl>
*/
class ListWidgetButton : public QWidget
{
	Q_OBJECT
public:
	enum Button { RadioButton=0, CheckBox, PushButton };

	ListWidgetButton( QTableWidgetItem *pItem, Button eButton, Qt::Alignment eAlign=Qt::AlignLeft, QWidget* pParent=NULL );
	~ListWidgetButton();

	Button button() { return m_eButton; }

	void setAlignment( Qt::Alignment eAlign ) { m_pLayout->setAlignment( buttonPtr(), eAlign ); }
	Qt::Alignment alignment() const { return m_eAlignment; }

	void setColor( const QColor &color );
	QColor color() const { return m_Color; }

	QRadioButton *radioButton() const { return m_pRadioButton; }
	QPushButton  *pushButton() const  { return m_pPushButton;  }
	QCheckBox    *checkBox() const    { return m_pCheckBox; }

	QTableWidgetItem *tableWidgetItem() const { return m_pTableWidgetItem; }

private:
	QTableWidgetItem *m_pTableWidgetItem;

	Button m_eButton;
	Qt::Alignment m_eAlignment;

	QRadioButton *m_pRadioButton;
	QPushButton  *m_pPushButton;
	QCheckBox    *m_pCheckBox;

	QHBoxLayout  *m_pLayout;

	QColor m_Color;

	QAbstractButton *m_pButton;
	QAbstractButton *buttonPtr();

signals:
	void clicked();

};

#endif
