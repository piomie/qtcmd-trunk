/***************************************************************************
                          shortkeyeditboxdelegate.cpp  -  description
                             -------------------
    begin                : Mon Feb 04 2008
    copyright            : (C) 2008 by Piotr Mierzwiński
    email                : peterm@go2.pl

    Copyright            : See COPYING file that comes with this project

 $Id$
 ***************************************************************************/
#include <QtGui> // for qDebug

#include "shortkeyeditboxdata.h"
#include "shortkeyeditboxeditor.h"
#include "shortkeyeditboxdelegate.h"


ShortKeyEditBoxDelegate::ShortKeyEditBoxDelegate( QObject *pParent ) : QItemDelegate(pParent)
{
}


QWidget *ShortKeyEditBoxDelegate::createEditor( QWidget *pParent, const QStyleOptionViewItem &option, const QModelIndex &index ) const
{
	//qDebug() << "ShortKeyEditBoxDelegate::createEditor, variant=" << qVariantCanConvert<ShortKeyEditBoxData>(index.data()) << ", modelIndex= " << index << ", index.data()= " << index.data();
	if ( qVariantCanConvert<ShortKeyEditBoxData>(index.data()) ) {
		//qDebug() << "ShortKeyEditBoxDelegate::createEditor - ShortKeyEditBoxData, data valid";
		ShortKeyEditBoxEditor *pEditor = new ShortKeyEditBoxEditor(pParent);
		connect( pEditor, SIGNAL(signalDone()), this, SLOT(slotCommitAndCloseEditor()) );
		return pEditor;
	}
	else {
		return QItemDelegate::createEditor(pParent, option, index);
	}
}

void ShortKeyEditBoxDelegate::setEditorData( QWidget *pEditor, const QModelIndex &index ) const
{
	if ( qVariantCanConvert<ShortKeyEditBoxData>(index.data()) ) {
		ShortKeyEditBoxData shortKeyEditBoxData = qVariantValue<ShortKeyEditBoxData>( index.data() );
		ShortKeyEditBoxEditor *pShortKeyEditBoxEditor = qobject_cast<ShortKeyEditBoxEditor *>( pEditor );
		pShortKeyEditBoxEditor->setShortKeyEditBoxData( shortKeyEditBoxData );
		pShortKeyEditBoxEditor->setText( shortKeyEditBoxData.text() );
	}
	else {
		QItemDelegate::setEditorData(pEditor, index);
	}
}

void ShortKeyEditBoxDelegate::setModelData( QWidget *pEditor, QAbstractItemModel *pModel, const QModelIndex &index ) const
{
	if ( qVariantCanConvert<ShortKeyEditBoxData>(index.data()) ) {
		ShortKeyEditBoxEditor *pShortKeyEditBoxEditor = qobject_cast<ShortKeyEditBoxEditor *>( pEditor );
		pModel->setData( index, qVariantFromValue(pShortKeyEditBoxEditor->shortKeyEditBoxData()) );
	} else {
		QItemDelegate::setModelData( pEditor, pModel, index );
	}
}

void ShortKeyEditBoxDelegate::updateEditorGeometry( QWidget *pEditor,
											 const QStyleOptionViewItem &option, const QModelIndex &/*index */ ) const
{
	pEditor->setGeometry(option.rect);
}


void ShortKeyEditBoxDelegate::paint( QPainter *pPainter, const QStyleOptionViewItem &option, const QModelIndex &index ) const
{
	if ( qVariantCanConvert<ShortKeyEditBoxData>(index.data()) )
	{
		ShortKeyEditBoxData shortKeyEditBoxData = qVariantValue<ShortKeyEditBoxData>(index.data());

		if ( option.state & QStyle::State_Selected ) {
			pPainter->fillRect( option.rect, option.palette.highlight() );
			pPainter->setPen( option.palette.highlightedText().color() );
		}
		else
			pPainter->setPen( option.palette.text().color() );

		shortKeyEditBoxData.paint( pPainter, option.rect, option.palette );
	}
	else {
		QItemDelegate::paint( pPainter, option, index );
	}
}


void ShortKeyEditBoxDelegate::slotCommitAndCloseEditor()
{
//	qDebug() << "ShortKeyEditBoxDelegate::slotCommitAndCloseEditor (commitData and closeEditor)";
	ShortKeyEditBoxEditor *pEditor = qobject_cast<ShortKeyEditBoxEditor *>(sender());
	emit commitData ( pEditor );
	emit closeEditor( pEditor );
}

