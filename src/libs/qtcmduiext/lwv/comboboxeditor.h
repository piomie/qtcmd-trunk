/***************************************************************************
 *   Copyright (C) 2008 by Piotr Mierzwiński   *
 *   piotrmierzwinski@gmail.com   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef COMBOBOXEDITOR_H
#define COMBOBOXEDITOR_H

#include <QComboBox>

#include "comboboxdata.h"

/**
	@author Piotr MierzwiDski <piotrmierzwinski@gmail.com>
*/
class ComboBoxEditor : public QComboBox
{
	Q_OBJECT
public:
	ComboBoxEditor( QWidget *pParent = 0 );

	void setComboBoxData( const ComboBoxData &comboBoxData ) {
		m_ComboBoxData = comboBoxData;
	}
	ComboBoxData comboBoxData() { return m_ComboBoxData; }

private:
	ComboBoxData m_ComboBoxData;

private slots:
	void slotCurrentItemChanged( int nComboId );

};

#endif
