/***************************************************************************
                          listwidgetbutton  -  description
                             -------------------
    begin                : pi� lut 1 2008
    copyright            : (C) 2008 by Piotr Mierzwi�ski, Mariusz Borowski
    email                : piom@nes.pl, mariusz@nes.pl

    copyright            : See COPYING file that comes with this project

 $Id$
 ***************************************************************************/
#include <QtGui>

#include "listwidgetbutton.h"

ListWidgetButton::ListWidgetButton( QTableWidgetItem *pItem, Button eButton, Qt::Alignment eAlign, QWidget *pParent )
	: QWidget(pParent)
	, m_pTableWidgetItem(pItem), m_eButton(eButton), m_eAlignment(eAlign)
	, m_pRadioButton(NULL), m_pPushButton(NULL), m_pCheckBox(NULL)
{
	m_pLayout = new QHBoxLayout(this);
	setLayout(m_pLayout);

	if (m_eButton == RadioButton) {
		m_pRadioButton = new QRadioButton;
		m_pRadioButton->setAutoExclusive( false );
	}
	else
	if (m_eButton == PushButton) {
		m_pPushButton = new QPushButton;
		m_pPushButton->setMinimumSize(m_pPushButton->sizeHint().width()-40, m_pPushButton->sizeHint().height()-8);
	}
	else
	if (m_eButton == CheckBox) {
		m_pCheckBox = new QCheckBox;
		m_pCheckBox->setMinimumSize(m_pCheckBox->sizeHint().width(), m_pCheckBox->sizeHint().height());
	}

	if ( buttonPtr() != NULL ) {
		m_pLayout->addWidget( buttonPtr(), 0, Qt::AlignHCenter );

		m_Color = buttonPtr()->palette().foreground().color();
		buttonPtr()->setFocusPolicy( Qt::NoFocus );
		connect( buttonPtr(), SIGNAL(clicked()), this, SIGNAL(clicked()) );
	}
	m_eAlignment = Qt::AlignLeft;
}


ListWidgetButton::~ListWidgetButton()
{
	delete m_pRadioButton;  m_pRadioButton=0;
	delete m_pPushButton;  m_pPushButton=0;
	delete m_pCheckBox;  m_pCheckBox=0;
	delete m_pLayout;  m_pLayout=0;
}


void ListWidgetButton::setColor( const QColor & color )
{
	m_Color = color;
	QPalette newPalette = palette();
	newPalette.setColor( QPalette::Button, color );

	m_pButton->setPalette( newPalette );
}


QAbstractButton * ListWidgetButton::buttonPtr()
{
	if ( m_eButton == RadioButton )
		m_pButton = dynamic_cast<QAbstractButton *>(m_pRadioButton);
	else
	if ( m_eButton == PushButton )
		m_pButton = dynamic_cast<QAbstractButton *>(m_pPushButton);
	else
	if ( m_eButton == CheckBox ) {
		m_pButton = dynamic_cast<QAbstractButton *>(m_pCheckBox);
	}

	return m_pButton;
}


