/***************************************************************************
                          listwidgetview.cpp  -  description
                             -------------------
    begin                : Wed Jan 02 2008
    copyright            : (C) 2008 by Piotr Mierzwiński
    email                : peterm@go2.pl

    Copyright            : See COPYING file that comes with this project

 $Id$
 ***************************************************************************/
#ifndef LISTWIDGETVIEW_H
#define LISTWIDGETVIEW_H

#include <QTableWidget>

/**
	@author Piotr Mierzwiński <piom@nes.pl>
*/
class ListWidgetView : public QTableWidget
{
	Q_OBJECT
public:
	ListWidgetView( QWidget *pParent );

	enum Widget      { CheckBox=0, RadioButton, ColorChooser, ComboBox, EditBox, KeyEditBox, TextLabel };
	enum TextWeight  { Normal=0, Bold=2, Italic=4 };


	void addColumn( Widget eWidget, const QString & sColumnName, Qt::AlignmentFlag eColumnAlign=Qt::AlignLeft );
	void addRow( bool bButtonsExclusive = false );

	void removeRows();

	void setCellValue( int nRow, int nColumn, const QString &sValue ) { setCellValue( nRow, nColumn, QStringList(sValue) ); }
	void setCellValue( int nRow, int nColumn, const QStringList &slValue, int nInitId=0 );
	QString cellValue( int nRow=-1, int nColumn=-1 ) const;

	void setEnableCell( int nRow, int nColumn, bool bEnable, bool bEditable=TRUE );
	bool cellEnable( int nRow, int nColumn ) const;

	void setEditableCell( int nRow, int nColumn, bool bEditable );
	bool editableCell( int nRow, int nColumn ) const;

	void setCellAttributes( int nRow, int nColumn, const QColor &color, TextWeight eTextWeight=ListWidgetView::Normal );
	void setCellAttributes( int nRow, int nColumn, TextWeight eTextWeight );

	int findItem( const QString & sText, int nColumn=-1 );

	bool getBoolValue( const QString &sValue );

private:
	QWidget *m_pParent;

	int m_nColumnCount;
	int m_nRowCount;

	bool m_bButtonsExclusive;

	QStringList m_slHeaderLabels;

	QList < Widget > m_eRowWidgetList;
	QList < Qt::AlignmentFlag > m_eColumnAlignList;
	QList < QTableWidgetItem * > m_pTableWidgetItemList;


	Widget columnWidget( int nColumn ) const { return m_eRowWidgetList[nColumn]; }

private slots:
	void slotButtonClicked();

protected slots:
	virtual void commitData ( QWidget * pEditor );

signals:
	void buttonClicked( QTableWidgetItem *pItem );
	void colorChanged( QTableWidgetItem *pItem );
	void editBoxTextApplied( QTableWidgetItem *pItem, const QString & sText );

};

#endif
