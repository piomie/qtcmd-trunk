/***************************************************************************
                          shortkeyeditboxdata.cpp  -  description
                             -------------------
    begin                : Mon Feb 04 2008
    copyright            : (C) 2008 by Piotr Mierzwiński
    email                : peterm@go2.pl

    Copyright            : See COPYING file that comes with this project

 $Id$
 ***************************************************************************/
//#include <QtGui> // for qDebug

#include "shortkeyeditboxdata.h"

const int PaintingScaleFactor = 20;


ShortKeyEditBoxData::ShortKeyEditBoxData( const QString & sShortKey, Qt::AlignmentFlag eAlign )
	: m_sShortKey(sShortKey), m_eAlign(eAlign)
	, m_TxtColor(QColor("black")), m_bTxtBold(false), m_bTxtItalic(false)
{
}


void ShortKeyEditBoxData::paint( QPainter * pPainter, const QRect & rect, const QPalette & palette ) const
{
	pPainter->save();
	pPainter->setRenderHint(QPainter::Antialiasing, true);

	int yOffset = (rect.height() - PaintingScaleFactor) / 2;
	pPainter->translate(rect.x(), rect.y() + yOffset);

	QString sTxt = m_sShortKey;
	//pPainter->setPen(m_TxtColor);

	int fontHeight = pPainter->fontMetrics().height();
	int yPos = (rect.height() - fontHeight)/2 + 1;
	int xPos = 0; // Qt::AlignLeft and others except below is checked
	//qDebug() << "ComboBoxData::paint, m_eAlign=" << m_eAlign;
	if ( m_eAlign == Qt::AlignHCenter || m_eAlign == (Qt::AlignHCenter|Qt::AlignVCenter) ) {
		xPos = (rect.width() - pPainter->fontMetrics().width(sTxt))/2 + 1;
	}
	else
	if ( m_eAlign == Qt::AlignRight ) {
		// TODO implement me  (m_eAlign == Qt::AlignRight)
	}
	//qDebug() << "height="<< rect.height() << ", yOffset=" << yOffset << ", fontHeight=" << fontHeight;
	sTxt = pPainter->fontMetrics().elidedText( sTxt, Qt::ElideRight, rect.width() ); // option.textElideMode
	pPainter->drawText(xPos, yPos+yOffset, sTxt );
	//qDebug() << "ShortKeyEditBoxData::paint, pen_color=" << pPainter->pen()->color().name() << ", xpos= " << xPos << ", yPos= " << yPos+yOffset << ", txt= " << sTxt;

	pPainter->restore();
}


void ShortKeyEditBoxData::setTextAlignment( Qt::AlignmentFlag eAlign )
{
	m_eAlign = eAlign;
	//qDebug() << "setTextAlignment, m_eAlign=" << m_eAlign;
}

