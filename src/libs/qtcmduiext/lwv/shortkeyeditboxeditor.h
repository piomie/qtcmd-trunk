/***************************************************************************
                          shortkeyeditboxeditor.h  -  description
                             -------------------
    begin                : Mon Feb 04 2008
    copyright            : (C) 2008 by Piotr Mierzwiński
    email                : peterm@go2.pl

    Copyright            : See COPYING file that comes with this project

 $Id$
 ***************************************************************************/
#ifndef SHORTKEYEDITBOXEDITOR_H
#define SHORTKEYEDITBOXEDITOR_H

#include <QMap>
#include <QLineEdit>

#include "shortkeyeditboxdata.h"

/**
	@author Piotr Mierzwiński <piom@nes.pl>
*/
class ShortKeyEditBoxEditor : public QLineEdit
{
	Q_OBJECT
public:
	ShortKeyEditBoxEditor( QWidget *pParent=NULL );

	void setShortKeyEditBoxData( const ShortKeyEditBoxData &shortKeyEditBoxData ) {
		m_ShortKeyEditBoxData = shortKeyEditBoxData;
	}
	ShortKeyEditBoxData shortKeyEditBoxData() { return m_ShortKeyEditBoxData; }

private:
	ShortKeyEditBoxData m_ShortKeyEditBoxData;

	typedef QMap<int, QString> KeysMap;
	KeysMap m_KeysStateMap;

	QString m_sModifiers;

protected:
	void keyPressEvent( QKeyEvent *pKeyEvent );

signals:
	void signalDone();

};

#endif
