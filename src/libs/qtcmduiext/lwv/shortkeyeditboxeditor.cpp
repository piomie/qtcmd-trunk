/***************************************************************************
                          shortkeyeditboxeditor.cpp  -  description
                             -------------------
    begin                : Mon Feb 04 2008
    copyright            : (C) 2008 by Piotr Mierzwiński
    email                : peterm@go2.pl

    Copyright            : See COPYING file that comes with this project

 $Id$
 ***************************************************************************/
#include <QtGui> // for debug

#include "shortkeyeditboxeditor.h"


ShortKeyEditBoxEditor::ShortKeyEditBoxEditor( QWidget *pParent ) : QLineEdit(pParent)
{
	this->setFrame(false);
	this->setReadOnly(true);

	m_KeysStateMap[Qt::Key_Menu]    = "Menu";
	m_KeysStateMap[Qt::Key_Meta]    = "Win";
	m_KeysStateMap[Qt::Key_Alt]     = "Alt";
	m_KeysStateMap[Qt::Key_Control] = "Ctrl";
	m_KeysStateMap[Qt::Key_Shift]   = "Shift";
	m_KeysStateMap[Qt::Key_Super_L] = "LeftWin";
	m_KeysStateMap[Qt::Key_Super_R] = "RightWin";

	m_KeysStateMap[Qt::Key_Hyper_L] = "LeftHyper";
	m_KeysStateMap[Qt::Key_Hyper_R] = "RightHyper";
	m_KeysStateMap[Qt::Key_Direction_L] = "LeftDirection";
	m_KeysStateMap[Qt::Key_Direction_R] = "RightDirection";
}


// FIXME Key_Menu not work (parent class or QTableWidget class grab it) maybe eventFilter help
// FIXME Shift modifier work for choosed key, eg. can achieving Shift+? should be Shift+/ (kde3 has this same bug)
void ShortKeyEditBoxEditor::keyPressEvent( QKeyEvent *pKeyEvent )
{
	QString sState = "";
	int key = pKeyEvent->key();
	//qDebug() << "key=" << key;

	if (key == Qt::Key_Tab || key == Qt::Key_Backtab || key == Qt::Key_Escape) {
		pKeyEvent->ignore();
		return;
	}
	Qt::KeyboardModifiers modifiers = pKeyEvent->modifiers();
	KeysMap::Iterator itState = m_KeysStateMap.find( key );
	if (itState != m_KeysStateMap.end()) {
		if ( ! itState.value().isEmpty() )
			sState = itState.value()+"+";
		if ( modifiers == (Qt::AltModifier | Qt::ControlModifier) )
			sState = "Alt+Ctrl+";
		else
		if ( modifiers == (Qt::AltModifier | Qt::ShiftModifier) )
			sState = "Alt+Shift+";
		else
		if ( modifiers == (Qt::ControlModifier | Qt::ShiftModifier) )
			sState = "Ctrl+Shift+";
		else
		if ( modifiers == (Qt::AltModifier | Qt::ControlModifier | Qt::ShiftModifier) )
			sState = "Alt+Ctrl+Shift+";
		m_sModifiers = sState;
	}
	if (key == Qt::Key_AltGr) { // rightAlt
		qDebug() << "Right ALT is not supported! This is modifier for achieving national characters.";
		pKeyEvent->ignore();
		return;
	}
	QString sKey = pKeyEvent->text();
	QString sKeySeq = qPrintable(QKeySequence( key ).toString());
	QString sKeyCombin = m_sModifiers + sKeySeq;

	QLineEdit::setText(sKeyCombin);

	if (! sKey.isEmpty())
		m_sModifiers = "";

	//qDebug() << "keyText=" << sKey << "key=" << key << "sKeySeq=" << sKeySeq << "sKeySeq.length()=" << sKeySeq.length() << "keyCombin=" << sKeyCombin << "modifiers=" << modifiers;

	if (! sKeyCombin.endsWith('+') || sKeyCombin == "+") {
		emit signalDone();
	}
}

