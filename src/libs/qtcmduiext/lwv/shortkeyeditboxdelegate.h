/***************************************************************************
                          shortkeyeditboxdelegate.h  -  description
                             -------------------
    begin                : Mon Feb 04 2008
    copyright            : (C) 2008 by Piotr Mierzwiński
    email                : peterm@go2.pl

    Copyright            : See COPYING file that comes with this project

 $Id$
 ***************************************************************************/
#ifndef SHORTKEYEDITBOXDELEGATE_H
#define SHORTKEYEDITBOXDELEGATE_H

#include <QItemDelegate>
#include <QTableWidgetItem>

/**
	@author Piotr Mierzwiński <piom@nes.pl>
*/
class ShortKeyEditBoxDelegate : public QItemDelegate
{
	Q_OBJECT
public:
	ShortKeyEditBoxDelegate( QObject *pParent = NULL );

	QWidget *createEditor( QWidget *pParent, const QStyleOptionViewItem &option, const QModelIndex &index ) const;

	void setEditorData( QWidget *pEditor, const QModelIndex &index ) const;
	void setModelData( QWidget *pEditor, QAbstractItemModel *pModel, const QModelIndex &index ) const;

	void updateEditorGeometry( QWidget *pEditor, const QStyleOptionViewItem &option, const QModelIndex &index ) const;

	void paint( QPainter *pPainter, const QStyleOptionViewItem &option, const QModelIndex &index ) const;

private slots:
	void slotCommitAndCloseEditor();

};

#endif
