/***************************************************************************
                          shortkeyeditboxdata.h  -  description
                             -------------------
    begin                : Mon Feb 04 2008
    copyright            : (C) 2008 by Piotr Mierzwiński
    email                : peterm@go2.pl

    Copyright            : See COPYING file that comes with this project

 $Id$
 ***************************************************************************/
#ifndef SHORTKEYEDITBOXDATA_H
#define SHORTKEYEDITBOXDATA_H

#include <QMetaType>
#include <QPainter>
#include <QPalette>

/**
	@author Piotr Mierzwiński <piom@nes.pl>
*/
class ShortKeyEditBoxData
{
public:
	ShortKeyEditBoxData() {}
	ShortKeyEditBoxData( const QString & sShortKey, Qt::AlignmentFlag eAlign=Qt::AlignLeft );

	void paint( QPainter *pPainter, const QRect &rect, const QPalette &palette ) const;

	QString text() const { return m_sShortKey; }
	void setText( const QString & sShortKey ) { m_sShortKey = sShortKey; }

	//void setTextAlignment( Qt::AlignmentFlag eAlign ) { m_eAlign = eAlign; }
	void setTextAlignment( Qt::AlignmentFlag eAlign );
	Qt::AlignmentFlag textAlignment() const { return m_eAlign; }

	void setTextColor( const QColor &txtColor ) { m_TxtColor = txtColor; };
	void setTextBold( bool bTxtBold ) { m_bTxtBold = bTxtBold; }
	void setTextItalic( bool bTxtItalic ) { m_bTxtItalic = bTxtItalic; }

private:
	int m_nShortKey;
	QString m_sShortKey;

	Qt::AlignmentFlag m_eAlign;

	QColor m_TxtColor;
	bool m_bTxtBold;
	bool m_bTxtItalic;

};

Q_DECLARE_METATYPE(ShortKeyEditBoxData)

#endif
