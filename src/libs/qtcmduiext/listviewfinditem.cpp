/***************************************************************************
                          listviewfinditem.cpp  -  description
                             -------------------
    begin                : Thu Nov 30 2000
    copyright            : (C) 2000 by Piotr Mierzwiński
    email                : peterm@go2.pl

    copyright            : See COPYING file that comes with this project
 ***************************************************************************/
#include "listviewfinditem.h"


ListViewFindItem::ListViewFindItem( QTableWidget *pParent, int nFindInColumn, Qt::WindowFlags fl )
	: QFrame( pParent, fl ), m_pListViewTable( pParent ), m_pListViewTree( NULL ), m_nFindInColumn( nFindInColumn )
{
	setFrameStyle( QFrame::WinPanel | QFrame::Raised );

	QFont fontWeight;
	fontWeight.setWeight( QFont::Bold );

	m_pLabel = new QLabel( tr("Search")+" :", this );
	m_pLabel->setMaximumHeight( 24 );
	m_pLabel->setFont( fontWeight );
	m_pLabel->move( 5, 8 );

	m_pLineEdit = new QLineEdit( this );
	m_pLineEdit->installEventFilter( this );

	if (m_pListViewTable != NULL && m_nFindInColumn > m_pListViewTable->columnCount())
		m_nFindInColumn = m_pListViewTable->columnCount();
}

ListViewFindItem::ListViewFindItem( QTreeView *pParent, int nFindInColumn, Qt::WindowFlags fl )
	: QFrame( pParent, fl ), m_pListViewTree( pParent ), m_nFindInColumn(nFindInColumn)
{
	setFrameStyle( QFrame::WinPanel | QFrame::Raised );

	QFont fontWeight;
	fontWeight.setWeight( QFont::Bold );

	m_pLabel = new QLabel( tr("Search")+" :", this );
	m_pLabel->setMaximumHeight( 24 );
	m_pLabel->setFont( fontWeight );
	m_pLabel->move( 5, 8 );

	m_pLineEdit = new QLineEdit( this );
	m_pLineEdit->installEventFilter( this );

	if (m_pListViewTable != NULL && m_nFindInColumn > m_pListViewTable->columnCount())
		m_nFindInColumn = m_pListViewTable->columnCount();
}


ListViewFindItem::~ListViewFindItem()
{
	m_pLineEdit->removeEventFilter(this);
	//delete m_pLineEdit;
	//delete mTimer;
}


void ListViewFindItem::show( const QPoint & pos, int nWidth )
{
	setGeometry( pos.x(), pos.y(),  ((nWidth>0) ? nWidth : 270), 32 );

	m_pLineEdit->setGeometry( m_pLabel->sizeHint().width()+25, 3, ((nWidth>0) ? nWidth-80 : 190), 26 );
	m_pLineEdit->setFocus();

	QWidget::show();
}


bool ListViewFindItem::keyPressed( QKeyEvent * pKeyEvent )
{
	if (! m_pLineEdit->hasFocus()) {
		hide();
	}

	unsigned int key = pKeyEvent->key();
	QString sText = pKeyEvent->text();

	switch( key )
	{
		case Qt::Key_Tab:
		case Qt::Key_Escape: // po ponownym wcisnieciu Esc na oknie rodzica, okno rodzica znika!
			hide();
			m_pListViewTable->setFocus();
			break;

		case Qt::Key_Up:
			moveCursor( Up );
		break;

		case Qt::Key_Down:
			moveCursor( Down );
		break;

		case Qt::Key_Home:
			moveCursor( Home );
		break;

		case Qt::Key_End:
			moveCursor( End );
		break;

		case Qt::Key_Enter:
		case Qt::Key_Return: {
			hide();
			m_pListViewTable->setFocus();
			if (m_pListViewTable != NULL)
				emit signalReturnPressed( m_pListViewTable->currentItem() );
		}
		break;

		case Qt::Key_Backspace:
		case Qt::Key_Delete:
		{
			return false;
		}
		break;

		default:
		{
			if ( sText.isEmpty() ) // Control/Shift/Alt pressed
				break;

			m_pLineEdit->insert( sText ); // insert char
			matchItemForCurrentText();
		}
		break;
	}

	if ( key >= Qt::Key_F1 && key <= Qt::Key_F12 )
		hide();

	return true;
}


void ListViewFindItem::matchItemForCurrentText()
{
	if ( m_pListViewTable == NULL )
		return;

	bool bFound = FALSE;
	int nCurrentRow = 0;

	QString sTxtToFound = m_pLineEdit->text();
	QTableWidgetItem *pCurrentItem = m_pListViewTable->item( nCurrentRow, m_nFindInColumn );

	//const int MAX_ROWS = m_pListViewTable->rowCount() - nCurrentRow;
	while ( (pCurrentItem=m_pListViewTable->item( nCurrentRow, m_nFindInColumn )) != NULL ) {
		if ( pCurrentItem->text().startsWith(sTxtToFound) )  {
			bFound = TRUE;
			break;
		}
		nCurrentRow++;
	}
	if ( ! bFound )  // fix text on m_pLineEdit
		m_pLineEdit->backspace();
	else  {  // move cursor to founded item
		m_pListViewTable->setCurrentItem( pCurrentItem );
	}
}


void ListViewFindItem::moveCursor( Direct direct )
{
	if ( m_pListViewTable == NULL )
		return;

	QString sTxtToFound = m_pLineEdit->text();
	if ( sTxtToFound.isEmpty() )
		return;

	QTableWidgetItem *pItem;
	int nNextPrev   = (direct == Up || direct == Home) ? -1 : 1;
	int nCurrentRow = m_pListViewTable->currentRow() + nNextPrev;

	while ( TRUE ) {
		pItem = m_pListViewTable->item( nCurrentRow, m_nFindInColumn );
		if ( pItem == NULL )
			break;
		if ( pItem->text().startsWith(sTxtToFound) ) {
			m_pListViewTable->setCurrentItem( pItem );
			if ( direct == Up || direct == Down )
				break;
		}
		nCurrentRow += nNextPrev;
	}
}


bool ListViewFindItem::eventFilter( QObject * pObj, QEvent * pEvent )
{
	if ( pObj == m_pLineEdit && pEvent->type() == QEvent::KeyPress ) {
		QKeyEvent *pKeyEvent = static_cast<QKeyEvent *>(pEvent);
		if ( keyPressed(pKeyEvent) )
			return true; // eat event
	}

	return QObject::eventFilter(pObj, pEvent); // standard event processing
}

