/***************************************************************************
                          listviewfinditem.h  -  description
                             -------------------
    begin                : Thu Nov 30 2000
    copyright            : (C) 2000 by Piotr Mierzwi�ski
    email                : peterm@go2.pl

    copyright            : See COPYING file that comes with this project
 ***************************************************************************/
#ifndef LISTVIEWFINDITEM_H
#define LISTVIEWFINDITEM_H

#include <QtGui>

/**
	@author Piotr Mierzwi�ski <piotrmierzwinski@gmail.com>
 */
/** @short Klasa okienka dialogowego do szybkiego wyszukiwania elementu listy.
 * Obiekt tej klasy powoduje pokazanie, na pozycji dolnego brzegu rodzica (tutaj
 * panela), okienka z linijk� edycyjn� s�u��c� do wprowadzania nazw szukanych
 * element�w. Obs�uga jego dzia�a na zasadzie przyrostowego wyszukiwania ci�gu,
 * tzn. w miar� wpisywania kursor panela jest ustawiany na szukanym elemencie
 * listy. Kiedy wpisany jest fragment tekstu, mo�na porusza� si� po podobnych
 * element�w za pomoc� klawiszy strza�ek (g�ra/d�), na pierwszy pasuj�cy (Home)
 * i ostatni (End). Klawisz Escape ukrywa okienko. Wci�ni�cie "Enter/Return"
 * spowoduje wys�anie sygna�u: "signalReturnPressed()" z parametrem b�d�cym
 * wska�nikiem do elementu, na kt�rym aktualnie stoi kursor, po czym nast�pi
 * zamkni�cie dialogu.
 * Klasa jest przystosowana do pracy z widokiem typu QTableWidget i klas
 * dziedzicz�cych po nim.
 */
class ListViewFindItem : public QFrame {
	Q_OBJECT
public:
	/** Konstruktor klasy.
	 * @param pParent - wska�nik do listy (rodzica dla bie��cego obiektu),
	 * @param nFindInColumn - numer kolumny, w kt�rej nale�y rozpocz�� wyszukiwanie,
	 * @param fl - flaga typu WindowFlags, ustawia rodzaj okna, tutaj domyslnie
	 * popup.
	 */
	ListViewFindItem( QTableWidget *pParent, int nFindInColumn = 0, Qt::WindowFlags f=Qt::Popup );

	/** Konstruktor klasy.
	 * @param pParent - wska�nik do listy (rodzica dla bie��cego obiektu),
	 * @param nFindInColumn - numer kolumny, w kt�rej nale�y rozpocz�� wyszukiwanie,
	 * @param fl - flaga typu WindowFlags, ustawia rodzaj okna, tutaj domyslnie
	 * popup.
	 */
	ListViewFindItem( QTreeView *pParent, int nFindInColumn = 0, Qt::WindowFlags f=Qt::Popup );

	/** Destruktor klasy.
	 * Usuwane s� tutaj obiekty linijki edycyjnej oraz timera.
	 */
	~ListViewFindItem();

	/** Funkcja powoduje pokazanie bie|^cego obiektu na podanej pozycji.
	 * @param pos - pozycja, na kt�rej powinno si^ pokaza^ obiekt,
	 * @param nWidth - szerokosc widgetu.
	 */
	void show( const QPoint & pos, int nWidth=-1 );

private:
	QTableWidget *m_pListViewTable;
	QTreeView *m_pListViewTree;
	QLineEdit *m_pLineEdit;
	QLabel *m_pLabel;

	int m_nFindInColumn;

	/** Kierunek ruchu kursorem.
	 */
	enum Direct { Up, Down, Home, End };


	/** Obs�uga klawiatury dla dialogu szybkiego wyszukiwania.
	 *  @param pKeyEvent - zdarzenie klawiatury, np. wci�ni�cie klawisza.
	 * Obs�ugiwane klawisze to: Escape, Up, Down, Home, End, Enter/Return,
	 * Wci�ni�cie klawiszy: Backspace, Delete zwraca warto�� FALSE.
	 * Wci�ni�cie klawisza innego ni� powy�sze spowoduje wywo�anie
	 * funkcji dopasowuj�cej ci�g do elementu na li�cie, patrz te�:
	 * @see matchItemForCurrentText().
	 * @return TRUE, jesli klawisz zosta� obs�u�ony, w przeciwnym razie FALSE
	 */
	bool keyPressed( QKeyEvent * pKeyEvent );

	/** Dopasowanie wpisanej w okienku edycyjnym nazwy z nazw� na li�cie.
	 * Wyszukuje element na liscie pocz�wszy od bie��cego elementu w podanej kolumnie
	 * listy (jej wska�nik jest inicjowany w konstruktorze), kt�rego nazwa zaczyna si�
	 * tak jak w podano w okienku edycyjnym. Je�li element zostanie znaleziony
	 * ustawiany jest na niego kursor listy (belka), w przeciwnym razie nic nie
	 * jest robione.
	 */
	void matchItemForCurrentText();

	/** Pr�buje przesun�� kursor o jeden element listy w podanym kierunku.
	 * @param direct - kierunek przesuni�cia kursora.
	 * Kursor jest przesuwany tylko wtedy, gdy nazwa elementu listy zaczyna tak
	 * samo, jak tekst w okienku edycyjnym.
	 */
	void moveCursor( Direct direct );

protected:
	bool eventFilter( QObject *pObj, QEvent *pEvent );

signals:
	/** Sygna� wysy�any gdy wci�ni�ty zostnianie klawisz Return/Enter.
	 * @param pItem - wska�nik elementu listy, na kt�rym aktualnie stoi kursor.
	 */
	void signalReturnPressed( QTableWidgetItem * pItem );

};

#endif
