/***************************************************************************
                          locationchooser.cpp  -  description
                             -------------------
    begin                : Wed Mar 17 2004
    copyright            : (C) 2004 by Piotr Mierzwiński
    email                : peterm@go2.pl

    copyright            : See COPYING file that comes with this project

 $Id$
 ***************************************************************************/

#include "locationchooser.h"

#include "home.xpm"
#include "root.xpm"
#include "cdup.xpm"
#include "next.xpm"
#include "prev.xpm"

#include <QMessageBox>
#include <QFileDialog>
#include <QSettings>
#include <QIcon>

// UWAGA dla bezpieczenstwa: wewnątrz nie uzywac odwolan bezposrednio do obiektu,
// np. m_pComboBoxExt->currentText() zamiast tego wykorzystac metode z klasy biezacej - text(id).

LocationChooser::LocationChooser( QWidget *pParent )
	: QWidget( pParent ),
	m_pHBoxLayout(NULL), m_pGroupBox(NULL), m_pLineEdit(NULL), m_pComboBoxExt(NULL),
	m_pBottomButton(NULL), m_pLeftBtn(NULL), m_pRightBtn(NULL)
{
	//qDebug("LocationChooser::LocationChooser");
	m_sCurrentPath    = "";
	m_sOriginalPath   = "";
	m_sKeyListReading = "";

	// --- init properties by default values
	m_eGetMode       = GetDirectory;
	m_eButtonSide    = RightButton;
	m_eGettingWidget = ComboBox;
	m_bMultiButton   = TRUE;
	m_bShowSeparator = TRUE;

	// info used by buttons-toolTip too
	m_slActionsListName.append( tr("&Current directory") );
	m_slActionsListName.append( tr("&Original directory") );
	m_slActionsListName.append( tr("&Home directory") );
	m_slActionsListName.append( tr("&Root directory") );
	m_slActionsListName.append( tr("&Up directory") );
	m_slActionsListName.append( tr("&Previous directory") );
	m_slActionsListName.append( tr("&Next directory") );
	m_slActionsListName.append( tr("&Select directory") );
	m_slActionsListName.append( tr("Cl&ear current") );
	m_slActionsListName.append( tr("Clear &all") );

	// generating a menu
	const char** sz_pIconList[] = { 0, 0, home, root, cdup, prev, next, 0, 0, 0 };
	m_pBottomBtnMenu = new QMenu( this );
	for ( int i=0; i<m_slActionsListName.count(); i++ ) {
		m_ptActionsList << m_pBottomBtnMenu->addAction( QIcon(sz_pIconList[i]), m_slActionsListName[i] );
		if ( i == 7 )
			m_pBottomBtnMenu->addSeparator();
	}
	connect( m_pBottomBtnMenu, SIGNAL(triggered ( QAction * )), this, SLOT(slotActionTriggered( QAction * )) );

	// info used by tool tip
	for (int i=0; i<m_slActionsListName.count(); i++)
		m_slActionsListName[i].remove( '&' );

	m_pHBoxLayout = new QHBoxLayout( this ); // default margin == 0
	m_pHBoxLayout->setContentsMargins(0, 0, 0, 0);

	createWidget();
}


LocationChooser::~LocationChooser()
{
	//saveSettings();
	removeWidget();

	if ( m_pHBoxLayout != NULL ) {
		delete m_pHBoxLayout;	m_pHBoxLayout = NULL;
	}
	if ( m_pBottomBtnMenu != NULL ) {
		delete m_pBottomBtnMenu;	m_pBottomBtnMenu = NULL;
	}
}


void LocationChooser::setKeyListReading( const QString & sKeyListReading, bool bReadSettings )
{
	m_sKeyListReading = sKeyListReading;

	if ( bReadSettings )
		init();
}


void LocationChooser::init()
{
	/*
	if ( m_pComboBoxExt && ! m_sKeyListReading.isEmpty() ) {
		QSettings *pSettings = new QSettings();
		QStringList URLsList = pSettings->readListEntry( m_sKeyListReading );
		int nCurrentURLid = pSettings->readNumEntry( m_sKeyListReading+"Id", -1 );

		if ( nCurrentURLid < 0 )
			nCurrentURLid = 0;
		if ( nCurrentURLid > (int)URLsList.count()-1 )
			nCurrentURLid = URLsList.count()-1;

		if ( URLsList.count() ) {
			insertStringList( URLsList );
			m_pComboBoxExt->setCurrentIndex( nCurrentURLid );
		}
		else
			m_pComboBoxExt->insertItem( m_sCurrentPath );

		delete pSettings;
		setMenuItemVisible( CLEAR_ALL, (m_pComboBoxExt->count() > 0) );
	}
	*/
}


void LocationChooser::saveSettings()
{
	/*
	if ( m_pComboBoxExt != NULL && ! m_sKeyListReading.isEmpty() ) {
		QStringList slList;
		int nItemsCount = m_pComboBoxExt->count();
		if ( nItemsCount > 0 ) {
			for (int i=0; i<nItemsCount; i++)
				slList.append( text(i) );
		}
// 		else // combo is empty
// 			list.append( text() ); // get currentText

		QSettings *pSettings = new QSettings;
		if ( slList.count() > 0 )
			pSettings->writeEntry( m_sKeyListReading, slList );
		else
			pSettings->removeEntry( m_sKeyListReading );

		pSettings->writeEntry( m_sKeyListReading+"Id", m_pComboBoxExt->currentIndex() );
		delete pSettings;
	}
	*/
}


void LocationChooser::setButtonSide( ButtonSide eBtnSide )
{
	m_eButtonSide = eBtnSide;
	createWidget();
}

void LocationChooser::setShowSeparator( bool bShow )
{
	m_bShowSeparator = bShow;
	if ( m_pHBoxLayout != NULL )
		m_pHBoxLayout->setSpacing( m_bShowSeparator ? 5 : 0 );
}

void LocationChooser::setGettingWidget( GettingWidget eGetWidget )
{
	m_eGettingWidget = eGetWidget;
	createWidget();
}


void LocationChooser::setMultiButton( bool bMultiButton )
{
	m_bMultiButton = bMultiButton;
	createWidget();
}


void LocationChooser::setButtonAction( MultiButtonSide eMultiButtonSide, ActionMenu eAction )
{
	if ( ! m_bMultiButton ) {
		if ( eMultiButtonSide != BottomMultiBtn )
			return;
	}

	const char *actionSlots[] = {
		SLOT(slotSetCurrentDir()),
		SLOT(slotSetOriginalDir()),
		SLOT(slotSetHomeDir()),
		SLOT(slotSetRootDir()),
		SLOT(slotUpDir()),
		SLOT(slotSetPreviousDir()),
		SLOT(slotSetNextDir()),
		SLOT(slotSelectDirectory()),
		SLOT(slotClearCurrent()),
		SLOT(slotClearAll())
	};

	if ( eMultiButtonSide == LeftMultiBtn && m_pLeftBtn ) {
		m_eLeftBtnAction = eAction;
		m_pLeftBtn->setToolTip(m_slActionsListName[eAction]);
		connect( m_pLeftBtn, SIGNAL(clicked()), this, actionSlots[eAction] );
	}
	else
	if ( eMultiButtonSide == RightMultiBtn && m_pRightBtn ) {
		m_eRightBtnAction = eAction;
		m_pRightBtn->setToolTip(m_slActionsListName[eAction]);
		connect( m_pRightBtn, SIGNAL(clicked()), this, actionSlots[eAction] );
	}
	else
	if ( m_pBottomButton != NULL ) { // BottomMultiBtn
		m_eBottomBtnAction = eAction;
		m_pBottomButton->setToolTip(m_slActionsListName[eAction]);
		connect( m_pBottomButton, SIGNAL(clicked()), this, actionSlots[eAction] );
	}
}

void LocationChooser::removeWidget()
{
	// remove input widget
	if ( m_pLineEdit != NULL ) {
		delete m_pLineEdit;	m_pLineEdit = NULL;
	}
	if ( m_pComboBoxExt != NULL ) {
		delete m_pComboBoxExt;	m_pComboBoxExt = NULL;
	}
	// remove ChooserButton object
	if ( m_pLeftBtn != NULL ) {
		delete m_pLeftBtn;  m_pLeftBtn = NULL;
	}
	if ( m_pRightBtn != NULL ) {
		delete m_pRightBtn;  m_pRightBtn = NULL;
	}
	if ( m_pBottomButton != NULL ) {
		delete m_pBottomButton;  m_pBottomButton = NULL;
	}
	if ( m_pGroupBox != NULL ) {
		delete m_pGroupBox;  m_pGroupBox = NULL;
	}
}

void LocationChooser::createWidget()
{
	// -- destroy old widget
	removeWidget();
	// -- create new widget
	m_pHBoxLayout->setSpacing( m_bShowSeparator ? 5 : 0 );
	//m_pHBoxLayout->setContentsMargins(0, 0, 0, 0);

	if ( m_eButtonSide == LeftButton ) {
		createChooserButton();
	}
	if ( m_eGettingWidget == LineEdit ) {
		m_pLineEdit = new QLineEdit( /*m_pLayoutWidget*/ this );
		m_pLineEdit->setSizePolicy( QSizePolicy( (QSizePolicy::Expanding), QSizePolicy::Preferred, QSizePolicy::LineEdit) ); // Fixed -> Preferred
		connect( m_pLineEdit, SIGNAL(textChanged(const QString &)), this, SIGNAL(textChanged(const QString &)) );
		connect( m_pLineEdit, SIGNAL(returnPressed()), this, SLOT(slotReturnPressed()) );
		setFocusProxy( m_pLineEdit );
		m_pHBoxLayout->addWidget( m_pLineEdit );
	}
	else
	if ( m_eGettingWidget == ComboBox ) {
		m_pComboBoxExt = new ComboBoxExt( 50, TRUE, /*m_pLayoutWidget*/ this ); // contains max 50 pathes
		m_pComboBoxExt->setSizePolicy( QSizePolicy( (QSizePolicy::Expanding), QSizePolicy::Preferred, QSizePolicy::ComboBox) ); // Fixed -> Preferred
		m_pComboBoxExt->setCompleter(0);
		connect( m_pComboBoxExt->lineEdit(), SIGNAL(textChanged(const QString &)), this, SIGNAL(textChanged(const QString &)) );
		connect( m_pComboBoxExt->lineEdit(), SIGNAL(returnPressed()), this, SLOT(slotReturnPressed()) );
		setFocusProxy( m_pComboBoxExt->lineEdit() );
		m_pHBoxLayout->addWidget( m_pComboBoxExt );
	}
	if ( m_eButtonSide == RightButton ) {
		createChooserButton();
	}

	if ( m_eButtonSide == NoneButton )  {
		if ( m_eGettingWidget == ComboBox )
			setSizePolicy( QSizePolicy( (QSizePolicy::Expanding), QSizePolicy::Fixed, QSizePolicy::ComboBox) ); // Fixed -> Preferred
		else
		if ( m_eGettingWidget == LineEdit )
			m_pLineEdit->setSizePolicy( QSizePolicy( (QSizePolicy::Expanding), QSizePolicy::Fixed, QSizePolicy::LineEdit) ); // Fixed -> Preferred
	}
/*
	// default settings
	setMenuItemVisible( PREVIOUS_DIR, (m_eGettingWidget == ComboBox) );
	setMenuItemVisible( NEXT_DIR, (m_eGettingWidget == ComboBox) );
	setMenuItemVisible( CLEAR_ALL, (m_eGettingWidget == ComboBox) );

	if ( m_eButtonSide != NoneButton ) {
		setLeftButtonAction( CLEAR_CURRENT );
		setRightButtonAction( HOME_DIR );
		setBottomButtonAction( SELECT_DIR );
	}
*/
	setMinimumHeight( 30 );
	setMaximumHeight( 30 );

	//setLayout(m_pHBoxLayout);
	//show();
}


void LocationChooser::createChooserButton()
{
	if ( m_bMultiButton ) {
		QGridLayout *gridLayout;
		QPalette newPalette = palette();

		m_pGroupBox = new QGroupBox(this);
		m_pGroupBox->setMinimumSize(QSize(31, 30));
		m_pGroupBox->setMaximumSize(QSize(31, 30));
		m_pGroupBox->setFlat(true);

		gridLayout = new QGridLayout(m_pGroupBox);
		gridLayout->setHorizontalSpacing(0);
		gridLayout->setVerticalSpacing(0);
		gridLayout->setContentsMargins(0, 0, 0, 0);

		newPalette.setColor(QPalette::Button, Qt::darkBlue);
		m_pLeftBtn = new QPushButton(m_pGroupBox);
		m_pLeftBtn->setMaximumSize(QSize(15, 12));
		m_pLeftBtn->setPalette(newPalette);
		m_pLeftBtn->setAutoFillBackground(true);
		m_pLeftBtn->setFocusPolicy(Qt::NoFocus);
		m_pLeftBtn->setFlat(true);

		newPalette.setColor(QPalette::Button, Qt::darkGreen);
		m_pRightBtn = new QPushButton(m_pGroupBox);
		m_pRightBtn->setMaximumSize(QSize(15, 12));
		m_pRightBtn->setPalette(newPalette);
		m_pRightBtn->setAutoFillBackground(true);
		m_pRightBtn->setFocusPolicy(Qt::NoFocus);
		m_pRightBtn->setFlat(true);

		m_pBottomButton = new QToolButton(m_pGroupBox);
		m_pBottomButton->setMaximumSize(QSize(31, 16));
		m_pBottomButton->setAutoRaise( TRUE );

		gridLayout->addWidget(m_pLeftBtn, 0, 0, 1, 1);
		gridLayout->addWidget(m_pRightBtn, 0, 1, 1, 1);
		gridLayout->addWidget(m_pBottomButton, 1, 0, 1, 2);

		m_pHBoxLayout->addWidget( m_pGroupBox );
	}
	else { // simple chooser button
		m_pBottomButton = new QToolButton( this );
		m_pBottomButton->setSizePolicy( QSizePolicy(QSizePolicy::Fixed, QSizePolicy::Preferred, QSizePolicy::ToolButton) ); // Fixed -> Preferred
		m_pBottomButton->setMaximumHeight( m_pBottomButton->height()-5 );
		m_pHBoxLayout->addWidget( m_pBottomButton );
	}

	if ( m_pBottomBtnMenu != NULL ) {
		m_pBottomButton->setPopupMode( QToolButton::MenuButtonPopup );
		m_pBottomButton->setMenu( m_pBottomBtnMenu );
	}
	QFont font;  font.setBold( TRUE );
	m_pBottomButton->setFont( font );
	m_pBottomButton->setText( "..." );
	m_pBottomButton->setFocusPolicy(Qt::NoFocus);
}


QString LocationChooser::text( int nId ) const
{
	if ( m_pLineEdit != NULL )
		return m_pLineEdit->text();

	if ( m_pComboBoxExt == NULL )
		return QString("");

	return (nId < 0) ? m_pComboBoxExt->currentText() : m_pComboBoxExt->itemText( nId );
}


void LocationChooser::setText( const QString & sText )
{
	if ( m_pLineEdit != NULL )
		m_pLineEdit->setText( sText );
	else
	if ( m_pComboBoxExt != NULL  )
		m_pComboBoxExt->setEditText( sText );
}


void LocationChooser::selectAll()
{
	if ( m_pLineEdit != NULL  )
		m_pLineEdit->selectAll();
	else
	if ( m_pComboBoxExt != NULL  )
		m_pComboBoxExt->lineEdit()->selectAll();
}


void LocationChooser::setMenuItemVisible( ActionMenu eAction, bool bVisible )
{
	QAction *pAction = m_ptActionsList.at( eAction );
	if ( pAction != NULL )
		pAction->setVisible( bVisible );
	else
		qDebug( "LocationChooser::setMenuItemVisible, eAction is out of range" );
}

void LocationChooser::setMenuItemEnabled( ActionMenu eAction, bool bEnable )
{
	QAction *pAction = m_ptActionsList.at( eAction );
	if ( pAction != NULL )
		pAction->setEnabled( bEnable );
	else
		qDebug( "LocationChooser::setMenuItemEnabled, eAction is out of range" );
}

	// ------ SLOTs -------

void LocationChooser::slotActionTriggered( QAction * pAction )
{
	if ( pAction == NULL )
		return;

	int nItemId = m_ptActionsList.indexOf( pAction );
	//qDebug( "LocationChooser::slotActionTriggered, nItemId= %d", nItemId );

	QString sTargetDir;

	if ( nItemId == CURRENT_DIR )
		sTargetDir = m_sCurrentPath;
	else
	if ( nItemId == ORIGINAL_DIR )
		sTargetDir = m_sOriginalPath;
	else
	if ( nItemId == HOME_DIR )
		sTargetDir = QDir::homePath()+"/";
	else
	if ( nItemId == ROOT_DIR )
		sTargetDir = "/";
	else
	if ( nItemId == UP_DIR ) {
		sTargetDir = text();
		if ( sTargetDir.at(sTargetDir.length()-1) == '/' ) // directory
			sTargetDir = sTargetDir.left( sTargetDir.lastIndexOf('/', sTargetDir.length()-2)+1 );
		else
			sTargetDir = sTargetDir.left( sTargetDir.lastIndexOf('/')+1 );
	}
	else
	if ( nItemId == SELECT_DIR ) {
		slotSelectDirectory();
		return;
	}
	else
	if ( nItemId == CLEAR_ALL ) {
		if ( ! m_pComboBoxExt )
			return;
		int nResult = QMessageBox::question( this,
			tr("Clear history pathes")+" - QtCommander",
			tr("History contains")+" "+QString::number(m_pComboBoxExt->count())+" "+tr("items")+".\n\n"+
			tr("Do you really want to clear all")+" ?",
			tr("&Yes"), tr("&No"), QString::null,
			1, 1 // default, cancel - 'No' button
		);
		if ( nResult == 0 ) // Yes
			m_pComboBoxExt->clear();
		return;
	}
	else
	if ( nItemId == CLEAR_CURRENT ) {
		m_pComboBoxExt->lineEdit()->clear();
		return;
	}
	else
	if ( nItemId == PREVIOUS_DIR )
		sTargetDir = prevEntry();
	else
	if ( nItemId == NEXT_DIR )
		sTargetDir = nextEntry();
	else
		return;

	if ( m_pLineEdit )
		m_pLineEdit->setText( sTargetDir );
	else
	if ( m_pComboBoxExt )
		m_pComboBoxExt->insertItem( sTargetDir, 0 );

	emit locationChanged( sTargetDir );
}

void LocationChooser::selectMenuItem( ActionMenu eAction )
{
	//qDebug("LocationChooser::selectMenuItem, actionMenu= %d", (int)eAction);
	QAction *pAction = m_ptActionsList.at( (int)eAction );
	if ( pAction != NULL )
		slotActionTriggered( pAction );
	else
		qDebug( "LocationChooser::selectMenuItem, action out of range" );
}


void LocationChooser::slotSelectDirectory()
{
	QString sTargetUrl;

	if ( m_eGetMode == GetDirectory  )
		sTargetUrl = QFileDialog::getExistingDirectory( this,
			tr("Select target directory")+" - QtCommander", m_sCurrentPath
		);
	else
		sTargetUrl = QFileDialog::getOpenFileName( this,
			tr("Select target file")+" - QtCommander", m_sCurrentPath,
			tr("All files")+"(*)"
		);

	if ( ! sTargetUrl.isEmpty() ) {
		if ( sTargetUrl.at(sTargetUrl.length()-1) != '/' )
			sTargetUrl += "/";

		if ( m_pLineEdit )
			m_pLineEdit->setText( sTargetUrl );
		else
		if ( m_pComboBoxExt )
			m_pComboBoxExt->setEditText( sTargetUrl );

		emit locationChanged( sTargetUrl );
	}
}

void LocationChooser::slotReturnPressed()
{
	QString sPath = text();
	if ( sPath.isEmpty() )
		return;

	if ( sPath.at(sPath.length()-1) != '/' )
		sPath += "/";

	emit locationChanged( sPath );
}


void LocationChooser::setFocusPolicy( Qt::FocusPolicy focusPolicy )
{
	if ( m_pLineEdit != NULL )
		m_pLineEdit->setFocusPolicy(focusPolicy);
	else
	if ( m_pComboBoxExt != NULL )
		m_pComboBoxExt->setFocusPolicy(focusPolicy);

	QWidget::setFocusPolicy(focusPolicy);
}

