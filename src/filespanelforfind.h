/***************************************************************************
                          filespanelforfind.h  -  description
                             -------------------
    begin                : �ro kwi 20 2005
    copyright            : (c) 2005 by Piotr Mierzwi�ski
    email                : peterm@go2.pl

    copyright            : See COPYING file that comes with this project

 $Id$
 ***************************************************************************/

#ifndef FILESPANELFORFIND_H
#define FILESPANELFORFIND_H

#include "filespanel.h"
//Added by qt3to4:
#include <QResizeEvent>

class QLineEdit;

/**
	@author Piotr Mierzwi�ski
*/
class FilesPanelForFind : public FilesPanel
{
Q_OBJECT
public:
	FilesPanelForFind(const QString & sInitURL, QWidget * pParent, Panel::KindOfPanel eKindOfPanel, const char * sz_pName);
	~FilesPanelForFind();

	void updateView();

private:
	QLineEdit *m_pLE_PathView;

protected:
	void resizeEvent( QResizeEvent * );

};

#endif
