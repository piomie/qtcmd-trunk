/***************************************************************************
                          listview.cpp  -  description
                             -------------------
    begin                : Mon Jul 9 2001
    copyright            : (C) 2001 by Mariusz Borowski (init)
                         : (C) 2002 by Piotr Mierzwiński (develop)
    email                : mcsoft@softhome.net, peterm@go2.pl

    copyright            : See COPYING file that comes with this project

 $Id$
 ***************************************************************************/

#include "fileinfoext.h" // for static fun.: specialFile()
#include "urlinfoext.h"
#include "functions.h" // for static functions
#include "listview.h"

#include "../icons/files_icons.h"
#include "../icons/specf_icons.h"
#include "../icons/cdup.xpm"
#include "../icons/folder.xpm"
#include "../icons/folder_lock.xpm"
#include "../icons/folder_link.xpm"
#include "../icons/broken_link.xpm"
#include "../icons/file_link.xpm"

#include <qtimer.h>
#include <qcursor.h>
#include <qpixmap.h>
#include <qregexp.h>
#include <qsettings.h>
#include <qinputdialog.h>
#include <qapplication.h>
//Added by qt3to4:
#include <QKeyEvent>
#include <QMouseEvent>
#include <QEvent>
#include <QDesktopWidget>

static QString s_sDevPath = "/dev/";

ListView::ListView( QWidget *pParent, bool bPopupMenu, bool bToolTip, bool /*bRenameBox*/, bool /*bFindDlg*/, const char *sz_pName )
	: Q3ListView( pParent, sz_pName )
	, m_pParent(pParent)
{
	qDebug("ListView::ListView");
	m_pToolTip = NULL;
	m_pPopupMenuFP = NULL;
	m_pScrollTimer = NULL;
//	m_pListViewEditBox = NULL;
//	m_pListViewFindItem = NULL;

	mNumOfSelectedItems = 0;  mWeightOfSelected = 0;
	mNumOfAllItemsPB = 0; mInsertedItemsCounter = 0;
	m_eKindOfView = UNKNOWN_PANELVIEW;
	m_bSelectFilesOnly = TRUE;
	m_bInvSelection = TRUE;
	m_sCurrentURL = "";

	setName( QString(pParent->name())+"_LV" );
	mGridColor = Qt::gray;
	mFullCursorColor = Qt::darkBlue;
	setBackgroundColor( Qt::white );
	mSecondColorOfBg = QColor( 11, 240, 11 ); // paletteBackgroundColor().dark(110);

	m_bDragging = FALSE;
	m_bShowGrid = FALSE;
	m_bFullCursor = TRUE;
	m_bDblFrameCursor = FALSE;
	m_bEnableTwoColorsOfBg = FALSE;
	m_eKindOfFormating = NONEformat;
	m_nlColumnsList.clear();

	setFullCursor();
	setShowIcons( FALSE );
	setCursorColor( Qt::darkBlue );
	setFontList( "Helvetica", 10 ); // default


// 	setMouseTracking( TRUE );
// 	viewport()->setMouseTracking( TRUE );

	setMultiSelection( TRUE );
	setShowSortIndicator( TRUE );
	setAllColumnsShowFocus( TRUE );

	connect( verticalScrollBar(),   SIGNAL(valueChanged(int)), this, SLOT(slotChangeVslider(int)) );
	connect( horizontalScrollBar(), SIGNAL(valueChanged(int)), this, SLOT(slotChangeHSlider(int)) );

	header()->setMaximumHeight( 23 );
	header()->setMovingEnabled( FALSE );
	connect( header(), SIGNAL(clicked(int)), this, SLOT(slotHeaderClicked(int)) );
	connect( header(), SIGNAL(sizeChange(int, int, int)),  this, SLOT(slotHeaderSizeChange(int, int, int)) );

	m_pSelectedItemsList = new SelectedItemsList;
	m_pSelectedItemsList->clear();
// 	m_pSelectedItemsList->setAutoDelete( TRUE );

	if ( bToolTip ) {
		m_pToolTip = new ListViewToolTip( viewport(), this );
		//FIXME : m_pToolTip->setPalette( QColor( 221, 255, 227 ) ); // green background of bToolTip
	}

// 	if ( bRenameBox ) {
// 		m_pListViewEditBox = new ListViewEditBox( viewport() );
// 		connect( m_pListViewEditBox, SIGNAL(signalRename(int, const QString &, const QString &)),
// 		 this, SIGNAL(signalRenameCurrentItem(int, const QString &, const QString &)) );
// 		setTabOrder( m_pListViewEditBox, this );
// 	}

//	if ( bFindDlg )
//		m_pListViewFindItem = new ListViewFindItem( this );

	if ( bPopupMenu ) {
		m_pPopupMenuFP = new PopupMenuFP( this );
		connect( m_pPopupMenuFP, SIGNAL(signalRename(int)),         this, SLOT(slotShowEditBox(int)) );
		connect( m_pPopupMenuFP, SIGNAL(signalSelectAll(bool)),     this, SLOT(slotSelectAllItems(bool)) );
		connect( m_pPopupMenuFP, SIGNAL(signalSelectSameExt(bool)), this, SLOT(slotSelectWithThisSameExtension(bool)) );
	}

	installEventFilter( this ); // for grab follow events: FocusIn, FocusOut, KeyPress
}


ListView::~ListView()
{
	delete m_pToolTip;           m_pToolTip = 0;
	delete m_pScrollTimer;       m_pScrollTimer = 0;
//	delete m_pListViewEditBox;   m_pListViewEditBox = 0;
//	delete m_pListViewFindItem;  m_pListViewFindItem = 0;
	delete m_pSelectedItemsList; m_pSelectedItemsList = 0;
}


void ListView::setColumn( ColumnName eColumnName, uint columnWidth )
{
	if ( eColumnName == NAMEcol ) {
		m_nlColumnsList.clear();
		m_nlColumnsList.append( NAMEcol ); // the column NAMEcol must be always visible
	}
	else
	if ( columnWidth )
		m_nlColumnsList.append( eColumnName );

	if ( columnWidth == 0 && eColumnName != NAMEcol )
		return;

	uint colWidth = columnWidth;
	const QStringList defaultColumnList = QStringList::split( ',', "0-170,1-70,2-107,3-70,4-55,5-55,6-170" );
	uint defaultColumnWidth = (defaultColumnList[eColumnName].section('-', -1 )).toInt();
	if ( colWidth < defaultColumnWidth && colWidth != 0 )
		colWidth = defaultColumnWidth;

	if ( eColumnName == NAMEcol )       addColumn( tr("Name"), (colWidth==0) ? (defaultColumnList[0].section('-', -1 )).toInt() : colWidth );
	else if ( eColumnName == SIZEcol )  addColumn( tr("Size"), colWidth );
	else if ( eColumnName == TIMEcol )  addColumn( tr("Time"), colWidth );
	else if ( eColumnName == PERMcol )  addColumn( tr("Permission"), colWidth );
	else if ( eColumnName == OWNERcol ) addColumn( tr("Owner"), colWidth );
	else if ( eColumnName == GROUPcol ) addColumn( tr("Group"), colWidth );
	else if ( eColumnName == LOCATIONcol ) addColumn( tr("Location"), colWidth );
}


void ListView::setShowColumns( bool show )
{
	if ( show )
		header()->show();
	else
		header()->hide();
}


void ListView::setFontList( const QString & fontFamily, const uint fontSize )
{
	m_sFontFamily = fontFamily;  mFontSize = fontSize;
}


void ListView::setShowIcons( bool showIcons )
{
	m_bShowIcons = showIcons;
	mIconWidth = (showIcons) ? 16 : 0;
}


void ListView::setColor( QColorGroup::ColorRole r, const QColor & c )
{
	QPalette p = palette();
	QColorGroup cg = p.active();
	cg.setColor( r, c );
	p.setActive( cg );
	cg = p.inactive();
	cg.setColor( r, c );
	p.setInactive( cg );
	setPalette( p );
}


void ListView::setHighlightBgColor( const QColor & highlBgColor )
{
	setColor( QColorGroup::Highlight, highlBgColor );
}


void ListView::setHighlightedTextColor( const QColor & highlTextColor )
{
	setColor( QColorGroup::HighlightedText, highlTextColor );
}


void ListView::setTextColor( const QColor & txtColor )
{
	setColor( QColorGroup::Text, txtColor );
}


void ListView::formatNumberStr( QString & numStr )
{
	::formatNumberStr( numStr, m_eKindOfFormating );
}


QPixmap * ListView::icon( const UrlInfoExt & urlInfo ) const
{
	QString fileName = urlInfo.name();

	if ( fileName == ".." )
		return (new QPixmap( cdup ));

	static const char **iconName;

	if ( urlInfo.isSymLink() )  {
		iconName = broken_link;
		if ( mCurrentDirIsDevDir ) {
/*			if ( urlInfo.specialFile(S_IFCHR) )  iconName = chrDevice;
			else
			if ( urlInfo.specialFile(S_IFBLK) )  iconName = blkDevice;
			else
			if ( urlInfo.specialFile(S_IFIFO) )  iconName = fifo;
			else
			if ( urlInfo.specialFile(S_IFSOCK) ) iconName = socket;*/
			if ( FileInfoExt::specialFile(s_sDevPath+fileName, FileInfoExt::CHRDEV) )  iconName = chrDevice;
			else
			if ( FileInfoExt::specialFile(s_sDevPath+fileName, FileInfoExt::BLKDEV) )  iconName = blkDevice;
			else
			if ( FileInfoExt::specialFile(s_sDevPath+fileName, FileInfoExt::FIFO) )    iconName = fifo;
			else
			if ( FileInfoExt::specialFile(s_sDevPath+fileName, FileInfoExt::SOCKET) )  iconName = socket;
		}
		if ( urlInfo.isFile() )
			iconName = file_link;
		else
		if ( urlInfo.isDir() )
			iconName = folder_link;
	}
	else
	if ( urlInfo.isDir() )  {
		iconName = folder;
		if ( ! urlInfo.isReadable() )
			iconName = folder_lock;
	}
	else  { // this is file
		int findDot = fileName.findRev( '.' );
		QString ext = (findDot > 0) ? fileName.right(fileName.length()-findDot-1).lower() : QString("");

				 if ( ext== "bmp" )  iconName= bmp;
		else if ( ext== "gif" )  iconName= gif;
		else if ( ext== "jpg" )  iconName= jpg;
		else if ( ext== "png" )  iconName= png;
		else if ( ext== "xbm" )  iconName= xbm;
		else if ( ext== "xpm" )  iconName= xpm;
//		else if ( ext== "pcx" )  iconName= pcx;
		else if ( ext== "txt" )  iconName= txt;
		else if ( ext== "so" )   iconName= lib;
		else if ( ext== "c" )    iconName= c;
		else if ( ext== "h"    || ext== "hh" )    iconName= h;
		else if ( ext== "cpp"  || ext== "cxx" || ext== "cc"  || ext== "C" )  iconName= cpp;
		else if ( ext== "html" || ext== "htm" || ext== "php" || ext== "php3" || ext== "phtml" || ext== "css" )  iconName= html;
		else if ( ext== "gz"   || ext== "bz2" || ext== "zip" || ext== "rar"  || ext== "sfx"   || ext == "z" || ext== "tar" ||
							 ext== "tgz"  || ext== "ace" || ext== "arj" || ext== "lzh"  || ext== "rpm"   || ext== "deb" )
								iconName= package;
		else {
			if ( mCurrentDirIsDevDir ) {
/*				if ( urlInfo.specialFile(S_IFCHR) )  iconName = chrDevice;
				else
				if ( urlInfo.specialFile(S_IFBLK) )  iconName = blkDevice;
				else
				if ( urlInfo.specialFile(S_IFIFO) )  iconName = fifo;
				else
				if ( urlInfo.specialFile(S_IFSOCK))  iconName = socket;*/
				if ( FileInfoExt::specialFile(s_sDevPath+fileName, FileInfoExt::CHRDEV) )  iconName = chrDevice;
				else
				if ( FileInfoExt::specialFile(s_sDevPath+fileName, FileInfoExt::BLKDEV) )  iconName = blkDevice;
				else
				if ( FileInfoExt::specialFile(s_sDevPath+fileName, FileInfoExt::FIFO) )    iconName = fifo;
				else
				if ( FileInfoExt::specialFile(s_sDevPath+fileName, FileInfoExt::SOCKET) )  iconName = socket;
			}
			else
				iconName= unknown;
		}

		if ( (urlInfo.permissions() & 0111) && iconName != lib ) // urlInfo.isExecutable()
			iconName= exe;
	}

	return (new QPixmap( iconName ));
}


void ListView::moveCursorToName( const QString & name )
{
	Q3ListViewItem *i;
	if ( name.isEmpty() ) {
		i = firstChild();
// 		return;
	}

	QString key;
	if ( currentItem() )
		key = currentItem()->key( mSortColumn, mAscending );
	i = findName( name );

	if ( i == NULL ) {
		Q3ListViewItemIterator it = this;
		// --- NEW for the TREE_PANELVIEW, usuwam ostatni elem. z katalogu
		Q3ListViewItem *parentOfCurrItem;
		if ( m_eKindOfView == TREE_PANELVIEW ) {
//			parentOfCurrItem = currentItem()->parent();
			parentOfCurrItem = currentItem();
			if ( parentOfCurrItem )
				if ( ! parentOfCurrItem->text(0).isEmpty() ) {
					it = parentOfCurrItem;
//					qDebug("__ ustaw 'it'");
//					qDebug(" it.current=%s", (it.current()->text(0)).toLatin1().data() );
					ensureItemVisible( parentOfCurrItem );
					return;
				}
//			it++; // go to first item in a current branch
		}

		Q3ListViewItem *item = 0;
//		QListViewItemIterator it( this );
		while ( (item = it.current()) != 0 )  {
			++it;
			Q3ListViewItem *nextItem = it.current();
			if ( ! nextItem )
				break;

			if ( mAscending )  {
				if ( (item->key( mSortColumn, mAscending ) < key) && (nextItem->key( mSortColumn, mAscending ) > key) )  {
					i = nextItem;
					break;
				}
			}
			else
				if ( (item->key( mSortColumn, mAscending ) > key) && (nextItem->key( mSortColumn, mAscending ) < key) )  {
					i = nextItem;
					break;
				}
		} // while

		if ( i == 0 )
			i = firstChild();
	} // if ( i == 0 )

	ensureItemVisible( i );
}


void ListView::removeItem( const QString & sName )
{
	if ( sName.isEmpty() )
		return;

	Q3ListViewItem *pItem2remove = findName( sName );
	if ( ! pItem2remove )
		return;

	Q3ListViewItem *pParent = pItem2remove->parent();

	if ( pParent )
		pParent->takeItem( pItem2remove );
	else
		Q3ListView::takeItem( pItem2remove );

	moveCursorToName( currentItemText() ); // sets cursor to next/previous item
}


void ListView::slotInsertItem( const UrlInfoExt & urlInfo )
{
	insertItem( urlInfo, m_pRootsItemOfCurrentPath, FALSE, TRUE ); // FALSE - don't remove this same, TRUE - set progress
}


void ListView::insertItem( const UrlInfoExt & urlInfo, Q3ListViewItem *pParentItemTree, bool bTryRemoveThisSameItem, bool bSetProgress )
{
	Q3ListViewItem *pParentItem = (m_eKindOfView==TREE_PANELVIEW) ? pParentItemTree : 0;

	if ( bTryRemoveThisSameItem )
		removeItem( urlInfo.name() ); // remove only if an item already exists

	if ( pParentItem )
		(void) new ListViewItem( (ListViewItem *)pParentItem, urlInfo );
	else
		(void) new ListViewItem( this, urlInfo );

	if ( bSetProgress )
		if ( mNumOfAllItemsPB ) {
			mInsertedItemsCounter++;
//			emit signalSetProgress( (100 * mInsertedItemsCounter) / mNumOfAllItemsPB );
		}
}


void ListView::applyFilter( const QString & pattern, Filters::DirFilter dirFilter, bool filterForFiltered )
{
	bool hide = FALSE;
	UrlInfoExt urlInfo;
	QStringList patternLst;
	ListViewItem *itemExt;
	Q3ListViewItemIterator it( this );
	patternLst = QStringList::split( '/', pattern );

	while ( (itemExt=(ListViewItem *)it.current()) != 0 ) {
		if ( itemExt->text(NAMEcol) == ".." ) {
			it++; continue;
		}
		if ( filterForFiltered )
			if ( ! itemExt->isVisible() ) {
				it++; continue;
			}

		urlInfo = itemExt->entryInfo();

		if ( dirFilter == Filters::AllFiles ) {
			hide = FALSE;
			if ( urlInfo.isFile() ) {
				for ( int i=0; i<patternLst.count(); i++ ) { // check all patterns
					QRegExp expr( patternLst[i], TRUE, TRUE );
					hide = ( ! expr.exactMatch(itemExt->text( NAMEcol )) );
					if ( ! hide ) // file matches
						break;
				}
			}
		}
		else
		if ( dirFilter == Filters::DirsOnly )
			hide = ( ! urlInfo.isDir() );
		else
		if ( dirFilter == Filters::FilesOnly )
			hide = urlInfo.isDir();
		else
		if ( dirFilter == Filters::SymLinks )
			hide = ( ! urlInfo.isSymLink() );
		else
		if ( dirFilter == Filters::HangSymLinks )
			hide = (urlInfo.isSymLink()) ? (! ( ! urlInfo.isDir() && ! urlInfo.isFile() )) : TRUE;

		( hide ) ? itemExt->hide() : itemExt->show();
		it++;
	}
}


void ListView::setTextInColumn( int column, const QString & str )
{
	if ( column == SIZEcol ) {
		if ( str == "<DIR>" )
			((ListViewItem *)currentItem())->setShowDirSize( FALSE );
		else
			((ListViewItem *)currentItem())->setShowDirSize( TRUE );
	}
	currentItem()->setText( column, str );
}


void ListView::setSorting( int column, bool ascending )
{
	mSortColumn = column;
	mAscending  = ascending;

	Q3ListView::setSorting( column, ascending );
}


QString ListView::pathForCurrentItem( bool bAbsolutePath, bool bFindParent )
{
	Q3ListViewItem *pCurrItem = (bFindParent) ? currentItem()->parent() : currentItem();
	bool bNotRoot = ! pCurrItem->text(0).isEmpty();
	QString sPath = (bNotRoot) ? pCurrItem->text(0) : QString("");
	bool bIsDir   = (bNotRoot) ? ((ListViewItem *)pCurrItem)->isDir() : TRUE; // root dir

	if (bIsDir)
    {
		if (bNotRoot && !sPath.endsWith('/'))
			sPath += "/";
		if (sPath.isEmpty()) // path is empty
			sPath = mHost;
    }

	if (bAbsolutePath && m_eKindOfView == TREE_PANELVIEW) {
		sPath = mHost;
		if ( bNotRoot )
			sPath += ((ListViewItem *)pCurrItem)->fullName();
	}
	else
	if (bAbsolutePath && m_eKindOfView == LIST_PANELVIEW) {
		if (m_sCurrentURL.isEmpty())
			m_sCurrentURL = ((ListViewItem *)pCurrItem)->entryInfo().location();
		sPath.insert(0, m_sCurrentURL);
	}

	if (! bIsDir && sPath.at(sPath.length()-1) == '/')
		sPath = sPath.left(sPath.length()-1);
//	qDebug("ListView::pathForCurrentItem, path=%s, pathToParent=%d", path.toLatin1().data(), findParent );

	return sPath;
}


QString ListView::absolutePathForItem( Q3ListViewItem *pItem, bool bAddNameWhenDir )
{
	if ( pItem == 0 )
		pItem = currentItem();

	QString sPath;
	Q3ListViewItem *pCurrItem = (pItem->parent()) ? pItem->parent() : pItem;

	if ( m_eKindOfView == TREE_PANELVIEW ) {
		if ( pItem->isOpen() )
			pCurrItem = pItem;
		sPath = mHost+((ListViewItem *)pCurrItem)->fullName();
		if ( bAddNameWhenDir )
			if ( ((ListViewItem *)pCurrItem)->isDir() ) {
				if ( pItem->isOpen() ) // to opened dir not to need adds a name
					sPath.remove( sPath.length()-1, 1 ); // remove last slash
				else
					sPath += pItem->text(0);
			}
	}
	else {
		sPath = m_sCurrentURL;
		if (sPath.isEmpty())
			sPath = ((ListViewItem *)pCurrItem)->entryInfo().location();

		if ( bAddNameWhenDir )
// 			if ( ((ListViewItem *)currItem)->isDir() || isSymLink )
				sPath += pCurrItem->text(0);
	}

	return sPath;
}


uint ListView::numOfAllItems( bool minusOneForList )
{
	uint number = 0;

	if ( m_eKindOfView == LIST_PANELVIEW ) {
		if ( childCount() )
			number = childCount() - (minusOneForList ? 1 : 0);
	}
	else { // TREE_PANELVIEW
		if ( currentItem() ) {
			if ( currentItem()->parent() )
				number = currentItem()->parent()->childCount();
		}
	}

	return number;
}


void ListView::initFilesList( const QString & path, uint numOfAllItems, const QString & owner, const QString & group, bool forceCleaning )
{
//	setUpdatesEnabled( TRUE ); // always paint items
	m_sOwner = owner;
	m_sGroup = (group.isEmpty()) ? owner : group;

	if ( m_eKindOfView == LIST_PANELVIEW || forceCleaning ) {
		clear();
		m_pSelectedItemsList->clear();
		mNumOfSelectedItems = 0; mWeightOfSelected = 0;
	}
	mCurrentDirIsDevDir = (path.find("/dev/") == 0);
	mNumOfAllItemsPB = numOfAllItems; // for progress bar obj.
	mInsertedItemsCounter = 0;

	if ( m_eKindOfView == LIST_PANELVIEW ) {
		UrlInfoExt ui("..", 0, 0755, "root", "root", QDateTime::currentDateTime(), QDateTime::currentDateTime(), TRUE, FALSE, FALSE, TRUE, TRUE, TRUE);
		ui.setLocation(path);
		slotInsertItem( ui );
	}
}


void ListView::closeBranch( const QString & absPathName )
{
	if ( m_eKindOfView != TREE_PANELVIEW )
		return;

	Q3ListViewItem *currItem, *newItem, *pParent;
	// ---NNN
	if ( absPathName.isEmpty() )
		currItem = currentItem();
	else
	if ( (currItem=findName(absPathName)) == NULL )
		return;

	if ( ! currItem->isOpen() )
		currItem = currItem->parent();
	// ---
	//currItem = (currentItem()->isOpen()) ? currentItem() : currentItem()->parent();

	UrlInfoExt urlInfo = ((ListViewItem *)currItem)->entryInfo();
	pParent = currItem->parent();
	qDebug("ListView::closeBranch, name=%s", urlInfo.name().toLatin1().data() );

	if ( pParent ) {
		pParent->takeItem( currItem );
		newItem = new ListViewItem( (ListViewItem *)pParent, urlInfo );
	}
	else {
		takeItem( currItem );
		newItem = new ListViewItem( this, urlInfo );
	}

	ensureItemVisible( newItem );
	//setContentsPos( 0, m_pRootsItemOfCurrentPath->itemPos() ); // closed item must be on the top of view
}


void ListView::openBranch( const UrlInfoExt & _ui, bool moveViewAtTop )
{
	if (m_eKindOfView != TREE_PANELVIEW)
		return;

	UrlInfoExt ui = _ui;
	QString absPathName = ui.location() + ui.name();

	if (absPathName.isEmpty() || childCount() == 0) { // --- create a root for the tree view
		clear();
		ui.setName(mHost);
		ui.setDir(TRUE);
		m_pRootsItemOfCurrentPath = new ListViewItem(this, ui);
	}
	else // --- try to open a new brach
	if ( (m_pRootsItemOfCurrentPath = findName(absPathName)) ) {
		if ( m_pRootsItemOfCurrentPath->isOpen() )
			return;
		qDebug("ListView::openBranch with name=%s", m_pRootsItemOfCurrentPath->text(0).toLatin1().data() );
		ensureItemVisible( m_pRootsItemOfCurrentPath ); // (m_pRootsItemOfCurrentPath->childCount()) ? m_pRootsItemOfCurrentPath->itemBelow() : m_pRootsItemOfCurrentPath
		if ( moveViewAtTop )
			setContentsPos( 0, m_pRootsItemOfCurrentPath->itemPos() ); // opened item must be on the top of view
		mCurrentDirIsDevDir = (absPathName.find("/dev/") == 0);
		m_pRootsItemOfCurrentPath->setOpen( TRUE );
		ui.setName(absPathName); // set location too
	}

	emit signalOpen(ui);
}

// TODO poprawic definicje tej metody
void ListView::open( const UrlInfoExt & ui, bool closeIfOpen )
{
	if (ui.name().isEmpty())
		return;

	//QString fName = ui.location()+ (ui.name() != "/" ? ui.name() : QString("")); // TYMCZASOWE
	QString fName = ui.location() + ui.name(TRUE);
	qDebug("ListView::open(). Try for fName= %s", fName.toLatin1().data());

	ListViewItem *item = (ListViewItem *)findName(fName);
	bool bIsArchive = FileInfoExt::isArchive(ui.name());
	//qDebug("ListView::open(), fName=%s, item=%d", fName.toLatin1().data(), (int)item);
	if (m_eKindOfView == TREE_PANELVIEW) {
		if (bIsArchive) {
			if (item != NULL)
				item->setExpandable(TRUE);
		}
	}

	if (m_eKindOfView == TREE_PANELVIEW && (ui.isDir() || bIsArchive)) {
		if (closeIfOpen) {
			if (currentItem()->isOpen()) {
				emit signalClose(); // for current item
				return;
			}
		}
		if (mHost == FileInfoExt::hostName(m_sCurrentURL) && bIsArchive == FileInfoExt::isArchive(m_sCurrentURL)) {
			int slashesDifferent = fName.count('/') - m_sCurrentURL.count('/');
			if ( slashesDifferent > 1 && fName.count(m_sCurrentURL) )
				fName = fName.left( fName.find('/', m_sCurrentURL.length()+1)+1 );
			else
			if ( m_sCurrentURL.count(fName) && m_sCurrentURL != fName ) {
				fName = m_sCurrentURL.left( m_sCurrentURL.find('/', fName.length()+1)+1 );
				close( fName );
				ensureNameVisible( m_sCurrentURL );
				m_sCurrentURL = FileInfoExt::filePath( fName );
				emit signalUpdateOfListStatus( mNumOfSelectedItems, numOfAllItems(), mWeightOfSelected );
				return;
			}
			else
			if ( m_sCurrentURL != fName && closeIfOpen )
				if ( ! fName.count(m_sCurrentURL) ) {
					QString nameToClose = m_sCurrentURL.left( m_sCurrentURL.find('/', mHost.length()+1)+1 );
					close( nameToClose );
					fName = fName.left( fName.find('/', mHost.length()+1)+1 );
				}
		}
		//openBranch( fName );
		UrlInfoExt ui;
		ui.setName(FileInfoExt::fileName(fName)); // set location too
		if (bIsArchive)
			ui.setDir(TRUE);
		openBranch(ui);
	}
	else { // flat list view
		if (ui.isDir()) // TODO spr. jak zadziala dla archiwow ?
			close();
		/*
		UrlInfoExt urli;

		if (item)
			urli = item->entryInfo();
		else
			urli.setName("/"); // auto set as dir item too

		qDebug("ListView::open(), call signalOpen, urli.name()= %s", urli.name().toLatin1().data() );
		emit signalOpen(urli);
		*/
		//qDebug("ListView::open(), call signalOpen, ui.location()= %s, ui.name()= %s", ui.location().toLatin1().data(), ui.name().toLatin1().data() );
		emit signalOpen(ui);
	}
}


void ListView::close( const QString & path )
{
	QString newPath = (path.isEmpty()) ? m_sCurrentURL : path;
	if ( newPath == mHost )
		return;
// FIXME jesli sciezka nie zostanie znaleziona to nic nie robic, aktualnie powoduje to CRASH
	if ( m_eKindOfView == TREE_PANELVIEW )
		closeBranch( newPath );
	else
		mNumOfSelectedItems = 0; mWeightOfSelected = 0;
}


void ListView::refresh()
{
	close(); // no param. == current path
	open();  // no param. == current path
}


void ListView::setKindOfView( KindOfListView kindOfView )
{
	if ( m_eKindOfView == kindOfView || kindOfView == UNKNOWN_PANELVIEW )
		return;

	qDebug("ListView::setKindOfView, kindOfView=%d", kindOfView );
	clear();
	m_eKindOfView = kindOfView;
	mNumOfSelectedItems = 0; mWeightOfSelected = 0;
}


void ListView::updateList( int updateListOp, SelectedItemsList *list, const UrlInfoExt & newUrlInfo )
{
	Q3ListViewItem *pParentItemTree = (m_eKindOfView==TREE_PANELVIEW) ? findName(m_sCurrentURL) : 0;

	if ( list == NULL ) // default is class member
		list = m_pSelectedItemsList;

	// --- operation MakeDir/Put
	if ( list->isEmpty() && newUrlInfo.isValid() ) {
		if ( updateListOp == INSERTitems )
			insertItem( newUrlInfo, pParentItemTree );

		setCurrentItem( findName(newUrlInfo.name()) );
		countAndWeightSelectedFiles( FALSE ); // FALSE - do not adds to the SelectedItemsList
	}

	if ( list == NULL )
		return;
	if ( list->isEmpty() )
		return;

// 	mInsertedItemsCounter = 0; // jesli bedzie pokazywany postep wstawiania
// 	mNumOfAllItemsPB = list.count(); // jw.
	UrlInfoExt urlInfo;
	Q3ListViewItem *item;
	SelectedItemsList::iterator it;
	// TODO drobna optymalizacja szybkosciowa.
	// Przed petla mozna zrobic: if ( updateListOp == (REMOVEitems | INSERTitems) ) wtedy
	// pobrac do zmiennych wlasciwosci obiektu 'newUrlInfo' (permissions, owner, itd.)

	for (it = list->begin(); it != list->end(); ++it) {
		item = (*it);
		urlInfo = ((ListViewItem *)item)->entryInfo();

		if ( updateListOp == INSERTitems || updateListOp == INSERTitemsAsLinks ) {
			if ( urlInfo.owner() != m_sOwner ) {
				urlInfo.setOwner( m_sOwner );
				urlInfo.setGroup( m_sGroup );
			}
			if ( updateListOp == INSERTitemsAsLinks ) {
				urlInfo.setSymLink( TRUE );
				if ( ! m_bShowFileLinkSize ) { // shows length of target link path
					int signleLinkSize = FileInfoExt::fileName(urlInfo.name()).length()+FileInfoExt::filePath(newUrlInfo.name()).length();
					urlInfo.setSize( (list->count()==1) ? signleLinkSize : urlInfo.name().length() );
				}
				if ( list->count() == 1 )
					urlInfo.setName( newUrlInfo.name() ); // sets a new name
			}
			insertItem( urlInfo, pParentItemTree ); // TRUE, TRUE ); // bedzie pokazywac postep wst.
		}
		else
		if ( updateListOp == REMOVEitems )
			removeItem( item->text(0) );
		else
		if ( updateListOp == DESELECTitems )
			Q3ListView::setSelected( item, FALSE );
		else // change attributs
		if ( updateListOp == (REMOVEitems | INSERTitems) ) {
			if ( ! newUrlInfo.name().isEmpty() )
				urlInfo.setName( newUrlInfo.name() );
			if ( newUrlInfo.permissions() > -1 )
				urlInfo.setPermissions( newUrlInfo.permissions() );
			if ( ! newUrlInfo.owner().isEmpty() )
				urlInfo.setOwner( newUrlInfo.owner() );
			if ( ! newUrlInfo.group().isEmpty() )
				urlInfo.setGroup( newUrlInfo.group() );
			if ( ! newUrlInfo.lastModified().isNull() )
				urlInfo.setLastModified( newUrlInfo.lastModified() );
			if ( ! newUrlInfo.lastRead().isNull() )
				urlInfo.setLastRead( newUrlInfo.lastModified() );

			Q3ListView::takeItem( findName(item->text(0)) );
			insertItem( urlInfo, pParentItemTree );
		}

		if ( updateListOp != INSERTitems )
			if ( mNumOfSelectedItems > 0 ) {
				mNumOfSelectedItems--;
				mWeightOfSelected -= urlInfo.size();
			}
	}

	if ( updateListOp != REMOVEitems && updateListOp != DESELECTitems )
		ensureItemVisible( findName(urlInfo.name()) ); // let's show last inserted item

	emit signalUpdateOfListStatus( mNumOfSelectedItems, numOfAllItems(), mWeightOfSelected );
}


Q3ListViewItem * ListView::findName( const QString & name )
{
	if ( name.isEmpty() )
		return 0L;

	QString newName;
	if ( m_eKindOfView == LIST_PANELVIEW ) {
		// if name has a path then get only last name from it
		if ( name.at(name.length()-1) == '/' ) {
			int numOfSlashes = name.count('/')-1;
			newName = name.section( '/', numOfSlashes, numOfSlashes );
		}
		else
			newName = name.right( name.length()-name.findRev('/')-1 );
		//qDebug("ListView::findName(), newName= %s", newName.toLatin1().data());
		return findItem( newName, NAMEcol );
	}

	// --- find item into the tree
	QString itemPath;
	Q3ListViewItem *item = 0;
	Q3ListViewItemIterator it( this );

	newName = name;
	if ( newName.find(mHost) != 0 ) // the tree need to absolute path
		newName.insert( 0, absolutePathForItem() );
	if ( newName.at(newName.length()-1) == '/' ) // remove last slash
		newName.remove( newName.length()-1, 1 );

	while ( it.current() != 0 ) {
		itemPath = mHost+((ListViewItem *)*it)->fullName( FALSE ); // FALSE - without adds a slash
		if ( itemPath == newName ) {
			item = *it;
			break;
		}
		it++;
	}

	return item;
}


void ListView::ensureItemVisible( Q3ListViewItem * item )
{
	if ( item == NULL )
		item = currentItem();
	if ( item == NULL )
		return;

	setCurrentItem( item );
	Q3ListView::ensureItemVisible( item );
}


void ListView::ensureNameVisible( const QString & name )
{
	ensureItemVisible( findName(name) );
}


void ListView::showPopupMenu( bool onCursorPos )
{
	if (m_pPopupMenuFP == NULL)
		return;

	QPoint position;

	if ( onCursorPos )
		position = QPoint( QCursor::pos().x()-2, QCursor::pos().y()+2 );
	else // for Key_Menu
		position = mapToGlobal(QPoint( header()->sectionSize( NAMEcol ), itemRect( currentItem() ).y()+currentItem()->height()+header()->height()+5 ));

	int y = position.y(), menuHeight = m_pPopupMenuFP->sizeHint().height();
	if ( y+menuHeight > QApplication::desktop()->height() ) {
		position.setY( y-menuHeight );
		if ( ! onCursorPos ) // correction for Key_Menu
			position.setY( y-currentItem()->height()-5 );
	}

	m_pPopupMenuFP->showMenu( position, pathForCurrentItem(TRUE) ); // currentItem()->text(0)
}


void ListView::showFindItemDialog()
{
//	m_pListViewFindItem->show( mapToGlobal(QPoint( header()->sectionSize(NAMEcol)-20, height()-15 )) );
}


void ListView::removeItemFromSIL( Q3ListViewItem * item )
{
	if ( item != NULL ) {
		if ( m_pSelectedItemsList->count() ) { // if a list isn't empty
			if ( m_pSelectedItemsList->find(item) != m_pSelectedItemsList->end() ) { // item found
				m_pSelectedItemsList->remove( m_pSelectedItemsList->find(item) );
			}
			//setSelected( item, FALSE ); // deselect and remove from the selecting list
		}
	}
}


void ListView::slotRemoveNameFromSIL( const QString & name )
{
	removeItemFromSIL( findName(name) );
}


void ListView::slotHeaderClicked( int section )
{
	Q3ListView::ensureItemVisible( currentItem() );

	uint column = header()->mapToIndex( section ); // jesli kiedys mozna byloby zmieniac kolejnosc kolumn

	if ( column != mSortColumn )
		mAscending = TRUE;
	else
		mAscending = ! mAscending;

	mSortColumn = column;
}


void ListView::slotHeaderSizeChange( int section, int /*oldSize*/, int newSize )
{
	header()->resizeSection( section, newSize );
	triggerUpdate();

// 	int editBoxColumn = m_pListViewEditBox->column();
// 	if ( editBoxColumn == section ) { // a current colum contains m_pListViewEditBox obj.
// 		if ( m_pListViewEditBox->isVisible() )
// 			m_pListViewEditBox->setFixedWidth( newSize - ((section == NAMEcol) ? mIconWidth : 0) - 2 ); // -2 it's correction
// 	}
// 	else
// 	if ( editBoxColumn > section ) { // a next colum contains m_pListViewEditBox obj.
// 		if ( m_pListViewEditBox->isVisible() )
// 			m_pListViewEditBox->move( m_pListViewEditBox->x()+(newSize-oldSize), itemRect(currentItem()).y() );
// 	}
}


void ListView::slotChangeVslider( int contentsY )
{
	if ( m_pScrollTimer || ! m_bNotHiddenFocus ) // m_pScrollTimer if true only when is working fun. autoScroll()
		return;

	int y = itemPos( currentItem() );
	int itemHeight = currentItem()->height();

// FIXME Przewijanie w gore. Przy duzych ilosciach plikow kursor skacze o coraz wiecej elem.

	// if cursor is behind top margin then sets it on a first visible
	if ( contentsY > y )
		setCurrentItem( itemAt( QPoint( 0, contentsY/itemHeight + 1 ) ) );

	// if cursor is behind bottom margin then sets it on a last visible
	int bm = y - visibleHeight() + itemHeight;
	if ( contentsY < bm ) {
		int bottomLine = (visibleHeight()/itemHeight - 1)*itemHeight;
		setCurrentItem( itemAt( QPoint(0, bottomLine) ) );
	}
}


void ListView::slotChangeHSlider( int )
{
	currentItem()->repaint();
}


void ListView::slotShowEditBox( int /*column*/ )
{
//	if ( currentItem()->text(NAMEcol) != ".." && columnWidth(column) )
//		m_pListViewEditBox->show( column, (m_eKindOfView == TREE_PANELVIEW) );
}


bool ListView::eventFilter( QObject * o, QEvent * e )
{
	bool popupMenuOrFindItemIsVisible = m_pPopupMenuFP && /*m_pListViewFindItem &&*/ (m_pPopupMenuFP->isVisible() /*|| m_pListViewFindItem->isVisible()*/);
	QEvent::Type eventType = e->type();

	if ( eventType == QEvent::FocusOut ) {
		if ( popupMenuOrFindItemIsVisible ) {
			setFocus();
			return TRUE;
		}
	}
	else
	if ( eventType == QEvent::FocusIn ) {
		//if ( m_pListViewEditBox->isVisible() )
		//	return TRUE;

		if ( popupMenuOrFindItemIsVisible )
			return TRUE;

		// !!! every position change for cursor made update path, but it's not necessary -> BUG
		//emit signalPathChanged( m_sCurrentURL ); // need to update path in pathView
	}
	else
	if ( eventType == QEvent::KeyPress ) {
		if ( ! numOfAllItems(FALSE) )
			return TRUE;

		QKeyEvent *keyEvent = (QKeyEvent *)e;
		int state = keyEvent->state();
		int key   = keyEvent->key();

//		if ( m_pPopupMenuFP->isVisible() ) { // commented - support for keyPressed is not necessary
//			m_pPopupMenuFP->keyPressed( keyEvent );
//			return TRUE;
//		}
//		else
//		if ( m_pListViewFindItem->isVisible() ) {
//			m_pListViewFindItem->keyPressed( keyEvent );
//			if ( key >= Qt::Key_F1 && key <= Qt::Key_F12 )
//				QApplication::sendEvent( m_pParent, keyEvent );
//			return TRUE;
//		}

		switch( key )
		{
			case Qt::Key_Up:
			case Qt::Key_Down:
				if ( state != Qt::NoButton ) {
					QApplication::sendEvent( m_pParent, keyEvent );
					return TRUE;
				}
			case Qt::Key_PageUp:
			case Qt::Key_PageDown:
			case Qt::Key_Home:
			case Qt::Key_End:
				if ( state != Qt::NoButton && state != Qt::KeypadModifier ) {
					QApplication::sendEvent( m_pParent, keyEvent ); // send event to pParent
					return TRUE;
				}
				else
				if ( m_eKindOfView == TREE_PANELVIEW ) { // path changed in the tree view
					if ( key != Qt::Key_Home )
						Q3ListView::keyPressEvent( keyEvent );
					else
						ensureItemVisible( firstChild()->itemBelow() );

					emit signalPathChanged( absolutePathForItem() );
					return TRUE;
				} // if ( m_eKindOfView == TREE_PANELVIEW )
			break;

			case Qt::Key_Left:
			case Qt::Key_Right:
				if ( state != Qt::NoButton ) {
					QApplication::sendEvent( m_pParent, keyEvent );
					return TRUE;
				}
				else
				if ( m_eKindOfView == TREE_PANELVIEW && currentItemIsDir() ) { // archive == dir, too
					if ( key == Qt::Key_Left ) {
						// close(); // closes a branch, called in FSManager::slotCloseCurrentDir()
						emit signalClose(); // for current item
						return TRUE;
					}
					else if ( key == Qt::Key_Right ) {
						open(((ListViewItem *)currentItem())->entryInfo());
						//open( currentItemText() ); // opens brach
						return TRUE;
					}
				}
			break;

			case Qt::Key_Tab:
			break;

			case Qt::Key_Enter:
			case Qt::Key_Return:
				if ( state != Qt::NoButton && state != Qt::KeypadModifier ) {
					QApplication::sendEvent( m_pParent, keyEvent ); // send event to pParent
					return TRUE;
				}
				// The obj.of QListView class is emiting the signal 'returnPressed()'
			break;

			default:
				QApplication::sendEvent( m_pParent, keyEvent ); // send event to pParent
				return TRUE;
			break;
		} // switch()
	} // if ( eventType == QEvent::KeyPress )

	return Q3ListView::eventFilter( o, e );    // standard event processing
}


void ListView::setSelected( Q3ListViewItem * item, bool select )
{
	if ( ! item )
		return;

	if ( item->text(0) == ".." || item->isSelected() == select )
		return;

	long long size = ((ListViewItem *)item)->entryInfo().size();

	// need to reset list when mNumOfSelectedItems is equal 0 and is added an item to the list (in collectAllSelectedNames())
	if ( ! mNumOfSelectedItems )
		m_pSelectedItemsList->clear();

	mNumOfSelectedItems += (select) ? 1    : - 1;
	mWeightOfSelected   += (select) ? size : - size;

	Q3ListView::setSelected( item, select );

	if ( item->isSelected() )
		m_pSelectedItemsList->append( item );
	else
		m_pSelectedItemsList->remove( item );

	emit signalUpdateOfListStatus( mNumOfSelectedItems, numOfAllItems(), mWeightOfSelected );
}


void ListView::countAndWeightSelectedFiles( bool addToTheSelectedItemsList )
{
	// need to deselect item '..', becouse it is selected in follow slots:
	//  slotSelectByUsePattern(), slotInvertSelections() and slotSelectAllItems()
	Q3ListView::setSelected( firstChild(), FALSE );

	Q3ListViewItem *item;
	Q3ListViewItemIterator it( this );
	mNumOfSelectedItems = 0, mWeightOfSelected = 0;
	m_pSelectedItemsList->clear();

	while ( (item=it.current()) != 0 ) {
		if ( item->isSelected() ) {
			mNumOfSelectedItems++;
			if ( addToTheSelectedItemsList )
				m_pSelectedItemsList->append( item );
			mWeightOfSelected += ((ListViewItem *)item)->entryInfo().size();
		}
		it++;
	}

	emit signalUpdateOfListStatus( mNumOfSelectedItems, numOfAllItems(), mWeightOfSelected );
}


void ListView::selectRange( Q3ListViewItem *from, Q3ListViewItem *to, bool clear, bool invert )
{ // function based on source of Qt-3.1 - QListView::clearRange()
	if ( ! from || ! to )
		return;

	// Swap
	if ( from->itemPos() > to->itemPos() ) {
		Q3ListViewItem *temp = from;
		from = to;
		to = temp;
	}

	// Clear, select or invert items <from, to>
	for ( Q3ListViewItem *i = from; i; i = i->itemBelow() ) {
		if ( invert )
			setSelected( i, ! i->isSelected() );
		else
			if ( clear ) {
				if ( i->isSelected() )
					setSelected( i, FALSE );
			}
			else
				if ( ! i->isSelected() )
					setSelected( i, TRUE );

		if ( i == to )
			break;
	}
}


void ListView::collectAllSelectedNames( QStringList & selectedNamesList )
{
	selectedNamesList.clear();
// przy usuwaniu kat.w Ftp, do spr.czy usunac kat czy plik, dla rozpoznania jest potrzebny '/' na koncu nazwy kat., ktory trzeba tutaj dodac
// pozniej trzeba tez przejzec czy gdzies nie jest wykonywana op.dodawania '/' do elementu tutaj tworzonej listy
// najpierw szukac wywolania 'collectAllSelectedNames()'
	if ( mNumOfSelectedItems == 0 ) {
		if ( currentItemText() != ".." ) {
			//selectedNamesList += absolutePathForItem(0, TRUE); // 0 - current item
			selectedNamesList += absolutePathForItem(0, TRUE)+(((ListViewItem *)currentItem())->isDir() ? "/" : ""); // 0 - current item
			m_pSelectedItemsList->clear();
			m_pSelectedItemsList->append( currentItem() );
		}
		return;
	}
	else  { // selected is one or more items, let's store them on the selectedNamesList
		SelectedItemsList::iterator it;
		for (it = m_pSelectedItemsList->begin(); it != m_pSelectedItemsList->end(); ++it)
			if ( (*it)->isSelected() )
				selectedNamesList += absolutePathForItem( (*it), TRUE ); // get path+name
	}
}

// ----------- SLOTs ------------

void ListView::slotSelectCurrentItem()
{
	setSelected( currentItem(), (currentItem()->isSelected() ? FALSE : TRUE) );
}


void ListView::slotSelectGroupOfItems( bool clear )
{
	QSettings settings;

	QStringList templateList = settings.readListEntry( "/qtcmd/FilesSystem/TemplateSelection" );
	if ( templateList.isEmpty() )
		settings.writeEntry( "/qtcmd/FilesSystem/TemplateSelection", "*" );


	QString dlgName = clear ? tr("UnSelect") : tr("Select");
	dlgName += " " + tr("group of files");

	bool ok = FALSE;
	QString newTemplate = QInputDialog::getItem(
	 dlgName, tr( "Please to give new template:" ),
	 templateList, 0, TRUE, &ok, this
	);

	if ( ok )  {
		bool templateExists = FALSE;
		QStringList::Iterator it;
		for ( it = templateList.begin(); it != templateList.end(); ++it )
			if ( newTemplate == *it )  {
				templateExists = TRUE;
				break;
			}

		// -- selected or a new template should be always on the top a list
		if ( ! templateExists )
			templateList.insert( templateList.begin(), newTemplate );
		else {
			QString selectedTmpl = *it;
			templateList.remove( it );
			templateList.insert( templateList.begin(), selectedTmpl );
		}
		settings.writeEntry( "/qtcmd/FilesSystem/TemplateSelection", templateList );

		if ( newTemplate.find( '/' ) > 0 )  {  // is more that one templates
			QStringList currentPatternList = QStringList::split( "/", newTemplate );
			for( int i=0; i<currentPatternList.count(); i++ )
				slotSelectByUsePattern( !clear, currentPatternList[ i ] );
		}
		else  // a new template
			slotSelectByUsePattern( !clear, newTemplate );
	}
}


void ListView::slotSelectAllItems( bool clear )
{
	slotSelectByUsePattern( !clear, "*" );
}


void ListView::slotInvertSelections()
{
	slotSelectByUsePattern( m_bInvSelection, "*" );
	m_bInvSelection = !m_bInvSelection;
}


void ListView::slotSelectByUsePattern( bool select, const QString & pattern )
{
	Q3ListViewItemIterator it( this );
	QRegExp expr( pattern, TRUE, TRUE );

	while ( it.current() != 0 )  {
		if (  expr.exactMatch( it.current()->text(0) ) ) {
			if ( m_bSelectFilesOnly ) {
				if ( ! ((ListViewItem *)*it)->isDir() )
					setSelected( it.current(), select );
			}
			else
				setSelected( it.current(), select );
		}
		it++;
	}
}


void ListView::slotSelectWithThisSameExtension( bool clear )
{
	QString currentExt, currentName = currentItem()->text(0);
	QString ext = currentName.right( currentName.length() - currentName.findRev('.') - 1 );
	Q3ListViewItemIterator it( this );

	while ( it.current() != 0 )  {
		currentName = it.current()->text(0);
		currentExt = currentName.right( currentName.length() - currentName.findRev('.') - 1 );
		if ( currentExt == ext ) {
			if ( m_bSelectFilesOnly ) {
				if ( ! ((ListViewItem *)*it)->isDir() )
					setSelected( it.current(), !clear );
			}
			else
				setSelected( it.current(), ! clear );
		}
		it++;
	}
}


void ListView::contentsMousePressEvent( QMouseEvent *e )
{
	Q3ListViewItem *previousItem = currentItem();
	QPoint vp = contentsToViewport( e->pos() );

	if ( ! itemAt( vp ) || numOfAllItems() == 0 )
		return;

	setCurrentItem( itemAt( vp ) );
	Q3ListViewItem *currItem = currentItem();

	int y = itemPos( currItem ) - contentsY();
	int yMax = visibleHeight() - currItem->height();
	if ( y<0 || y>yMax  )
		ensureItemVisible( currItem );

	setFocus();

	if ( e->button() == Qt::LeftButton )  {
		if ( m_eKindOfView == TREE_PANELVIEW )
			emit signalPathChanged( absolutePathForItem(currItem) );

		// check is the user clicked on the root decoration of an item
		int x1 = (treeStepSize() * (currItem->depth()-1))+5;
		if ( currentItem()->isExpandable() && vp.x() > x1 && vp.x() < x1+11 )  {
			emit returnPressed( currentItem() );
			return;
		}

		m_bDragging = TRUE;

		if ( e->state() & Qt::ShiftModifier )
			selectRange( previousItem, currItem );
		else
		if ( e->state() & (Qt::ShiftModifier | Qt::ControlModifier) )
			selectRange( previousItem, currItem, TRUE );
		else
		if ( e->state() & Qt::ControlModifier )
			slotSelectCurrentItem();
	}
	else
	if ( e->button() == Qt::RightButton )
		showPopupMenu();
}


void ListView::contentsMouseReleaseEvent( QMouseEvent * )
{
	if ( ! m_bDragging )
		return;

	m_bDragging = FALSE;

	if ( m_pScrollTimer )  {
		disconnect( m_pScrollTimer, SIGNAL(timeout()), this, SLOT(doAutoScroll()) );
		m_pScrollTimer->stop();
		delete m_pScrollTimer;
		m_pScrollTimer = 0;
	}
}


void ListView::contentsMouseMoveEvent( QMouseEvent *e )
{
	if ( ! e || ! m_bDragging )
		return;

	bool needAutoScroll = FALSE;
	QPoint vp = contentsToViewport( QPoint(e->x(), e->y()) );

	// check, if we need to scroll
	if ( vp.y() > visibleHeight() || vp.y() < 0 )
		needAutoScroll = TRUE;
	else
		setCurrentItem( itemAt( vp ) );
/*
	mShiftButtonPressed = FALSE;
	if ( e->state() & ShiftButton )  {
		mShiftButtonPressed = TRUE;
		if ( currentItem() != mCurrentItem )
			slotSelectCurrentItem();
	}
*/
	// if we need to scroll and no autoscroll timer is started, connect the timer

	if ( needAutoScroll && ! m_pScrollTimer ) {
		m_pScrollTimer = new QTimer( this );
		connect( m_pScrollTimer, SIGNAL(timeout()), this, SLOT(slotDoAutoScroll()) );
		m_pScrollTimer->start( 100, FALSE );
		// call it once manually
		slotDoAutoScroll();
	}
	if ( ! needAutoScroll )
		slotDoAutoScroll();

	mCurrentItem = currentItem();
}

void ListView::contentsMouseDoubleClickEvent( QMouseEvent *e )
{
	QPoint vp = contentsToViewport( e->pos() );

	if ( ! itemAt( vp ) || numOfAllItems(FALSE) == 0 )
		return;

	emit returnPressed( currentItem() );
}


void ListView::slotDoAutoScroll()
{
	QPoint pos = QCursor::pos();
	pos = viewport()->mapFromGlobal( pos );
// 	pos = mapFromGlobal( pos );
	Q3ListViewItem *current_item = currentItem();

	if ( pos.y() > visibleHeight() )  {  // if cursor out of up visible range
		if ( currentItem()->itemBelow() )
			current_item = currentItem()->itemBelow();
	}
	else
	if ( pos.y() < 0 )  {  // if cursor out of bottom visible range
		if ( currentItem()->itemAbove() )
			current_item = currentItem()->itemAbove();
	}

	ensureItemVisible( current_item );
	setCurrentItem( current_item );
/*
	if ( mShiftButtonPressed )  {
		if ( currentItem() != mCurrentItem )
			slotSelectCurrentItem();
	}
*/
}
