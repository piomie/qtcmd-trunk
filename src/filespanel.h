/***************************************************************************
                          filespanel.h  -  description
                             -------------------
    begin                : Sun Oct 27 2002
    copyright            : (C) 2002 by Piotr Mierzwiński
    email                : peterm@go2.pl

    copyright            : See COPYING file that comes with this project

 $Id$
 ***************************************************************************/

/** @file doc/api/src/filespanel.dox */

#ifndef _FILESPANEL_H_
#define _FILESPANEL_H_

#include "urlinfoext.h"
#include "fsmanager.h"
#include "panel.h" // include 'vfs.h'
//Added by qt3to4:
#include <QResizeEvent>
#include <Q3ValueList>
#include <QKeyEvent>

class QTimer;
class QToolButton;

class FileSystemSettings;
class ListViewSettings;
class OtherAppSettings;

class KeyShortcuts;
class MenuExt;
class DevicesMenu;
//class UrlInfoExt;
//class FSManager;
class StatusBar;
class ListView;
class PathView;


class FilesPanel : public Panel
{
	Q_OBJECT
public:
	FilesPanel( const QString & sInitURL, QWidget *pParent, Panel::KindOfPanel eKindOfPanel, const char *sz_pName );
	~FilesPanel();

	VFS::KindOfFilesSystem kindOfCurrentFS() const {
		return m_pFSManager ? m_pFSManager->kindOfCurrentFS() : VFS::UNKNOWNfs;
	}
	int kindOfView() const { return m_nKindOfListView; }

	bool hasFocus() const { return m_pFSManager ? m_pFSManager->hasFocus() : FALSE; }
	void setFocus()       { if ( m_pListView )  m_pListView->setFocus(); }


	QString currentURL() const { return m_pFSManager ? m_pFSManager->currentURL() : QString::null;  }

	void resizeSection( uint nSection, uint nOldSize, uint nNewSize ) {
		m_pListView->slotHeaderSizeChange( nSection, nOldSize, nNewSize );
	}

	void setPanelBesideURL( const QString & sUrl ) { m_sPanelBesideURL = sUrl; }

	void copyFiles( bool bMove );

	virtual void updateView();

	void updateList( int nUpdateListOp, Q3ValueList<Q3ListViewItem *> * pSelectedItemsList, const UrlInfoExt & uiNewUrlInfo ) {
		m_pListView->updateList( nUpdateListOp, pSelectedItemsList, uiNewUrlInfo );
	}

	void getSelectedItemsList( Q3ValueList<Q3ListViewItem *> *& selectedItemsList ) {
		selectedItemsList = m_pListView->selectedItemsList();
	}

	void setFilesAssociation( FilesAssociation *pFA ) {
		if (m_pFSManager != NULL && pFA != NULL) m_pFSManager->setFilesAssociation( (m_pFilesAssociation=pFA) );
	}
	void setKeyShortcuts( KeyShortcuts * pKeyShortcuts ) {
		if (pKeyShortcuts != NULL) m_pKeyShortcuts = pKeyShortcuts;
	}
	void setFilters( Filters * pFilters );

	void openCurrentFile( bool bEditMode ) {
		if ( m_pFSManager && m_pListView ) m_pFSManager->openFile( m_pListView->pathForCurrentItem(FALSE), bEditMode );
	}
	void createNewEmptyFile( bool bCreateFile=TRUE ) {
		if ( m_pFSManager ) m_pFSManager->createNewEmptyFile( bCreateFile );
	}
	void putFile( const QByteArray & baBuffer, const QString & sFileName ) {
		if ( m_pFSManager ) m_pFSManager->putFile( baBuffer, sFileName );
	}

	bool fileSystemCreated() const { return m_bFScreated; }

	Panel::KindOfPanel kindOfPanel() const { return m_eKindOfPanel; }
	void SetKindOfPanel( Panel::KindOfPanel eKindOfPanel ) { m_eKindOfPanel = eKindOfPanel; }

private:
	StatusBar        *m_pStatusBar;
	FilesAssociation *m_pFilesAssociation;

	FSManager        *m_pFSManager;
	DevicesMenu      *m_pDevicesMenu;
	KeyShortcuts     *m_pKeyShortcuts;

	QToolButton      *m_pFilterButton;
	MenuExt          *m_pFiltersMenu;
	Filters          *m_pFilters;

	uint m_nWidth;
	int  m_nKindOfListView;
	bool m_bShiftButtonPressed;
	Q3ListViewItem *m_pCurrentItem;

	QString m_sInitUrl;
	QString m_sPanelBesideURL;
	QString m_sFinalPathOfTree;

	int  m_nFilterId;
	bool m_bFilterForFiltered;

	bool m_bFScreated;

	Panel::KindOfPanel m_eKindOfPanel;

	void removeFS();
	void initPanel();
	void initLookOfList();
	void initFiltersButton();
	bool createFS( const QString & sURL );

protected:
	void saveSettings();
	void resizeEvent( QResizeEvent * );
	void keyPressEvent( QKeyEvent *pKeyEvent );

	ListView *m_pListView;
	PathView *m_pPathView;

public slots:
	void slotChangeKindOfView( uint nKindOfListView );
	void slotAddCurrentToFavorites();
	void slotQuickGetOutFromFS();
	void slotRereadCurrentDir();

	void slotShowFavoriteManager();
	void slotShowFiltersManager();
	void slotShowSimpleDiscStat();
	void slotShowFtpManagerDlg();
	void slotShowDevicesMenu();
	void slotShowFindFileDlg();
	void slotShowFavorites();
	void slotShowProperties();

	void slotPathChanged( const QString & sInputURL );

	void slotDelete();
	void slotMakeLink();
	void slotEditLink();
	void slotCopy( bool bShiftPressed, bool bMove );
	void slotOpenInNextPanel( bool bDirForce );
	void slotOpenArchive( const QString & sFileName );

	void slotReturnPressed( Q3ListViewItem * );

	void slotApplyListViewSettings( const ListViewSettings & lvs );
	void slotApplyOtherAppSettings( const OtherAppSettings & oas );
	void slotApplyFileSystemSettings( const FileSystemSettings & fss );

	void slotReadFileToView( const QString & sFileName, QByteArray & baBuffer, int & nBytesRead, int nFileLoadMethod ) {
		if ( m_pFSManager ) m_pFSManager->slotReadFileToView( sFileName, baBuffer, nBytesRead, nFileLoadMethod );
	} // need for load file from panel beside

private slots:
	void slotInitPanelList();
	void slotRenameCurrentItem( int nColumn, const QString & sOldText, const QString & sNewText );
	void slotResultOperation( VFS::Operation eOperation, bool bNoError );
	void slotPossibleDirWritable( VFS::DirWritable & dw );
	void slotPathChangedForTheViewPath( const QString & sPath );
	void slotChangeFilter( int nFilterId );

};

#endif
