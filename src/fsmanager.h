/***************************************************************************
                          fsmanager.h  -  description
                             -------------------
    begin                : Sat Nov 30 2002
    copyright            : (C) 2002 by Piotr Mierzwiński
    email                : peterm@go2.pl

    copyright            : See COPYING file that comes with this project

 $Id$
 ***************************************************************************/

/** @file doc/api/src/fsmanager.dox */

#ifndef _FSMANAGER_H_
#define _FSMANAGER_H_

#include "listview.h"
#include "enums.h"
#include "panel.h"
#include "vfs.h"

#include <q3urloperator.h>
#include <q3valuelist.h>
#include <q3process.h>
#include <qwidget.h>

static bool s_bCloseProgressDlgAfterFinished;

//class QLibrary;

class UrlInfoExt;
//class ListView;
class FindFileDlg;
class ProgressDlg;
class FindCriterion;
class PropertiesDlg;
class FilesAssociation;

class FSManager : public QWidget
{
	Q_OBJECT
public:
	FSManager( const QString & sInputURL, ListView * pListView );
	~FSManager();

	bool createVFS( VFS::KindOfFilesSystem eKindOfFS, const QString & sInputURL );
	void removeVFS();

	QString nameOfProcessedFile() const { return m_pVFS->nameOfProcessedFile(); }
	VFS::Error errorCode()  const { return m_pVFS->errorCode(); }

	QString host() const { return m_pVFS->host(); }
	QString path() const { return m_pVFS->path(); }
	int port() const     { return m_pVFS->port(); }

	VFS::DirWritable currentDirWritable() const {
		return (m_pVFS) ? m_pVFS->dirWritable( QString::null ) : VFS::UNKNOWNdw;
	}

	void weighFiles( const QStringList & slFilesList, bool bRealWeigh ) {
		m_pVFS->weighFiles( slFilesList, bRealWeigh );
	}

	void getWeighingStat( long long & nWeigh, uint & nFiles, uint & nDirs ) {
		m_pVFS->getWeighingStat( nWeigh, nFiles, nDirs );
	}

	void getNewUrlInfo( UrlInfoExt & uNewUrlInfo ) {
		m_pVFS->getNewUrlInfo( uNewUrlInfo );
	}

	void breakOperation() {
		m_pVFS->breakOperation();
	}

	void rereadCurrentDir();
	void createNewEmptyFile( bool bCreateFile=TRUE );
	void openFile( const QString & sFileName, bool bEditMode=FALSE );
	bool copyFilesTo( const QString & sTargetPath, ListView::SelectedItemsList *pItemsList, bool removeSrc=FALSE );
	void removeFiles();
	void makeLink( const QString & sTargetPath, bool bEditOnly );
	void changeAttribut( ListView::ColumnName eColumn, const QString & sNewAttribTxt );

	QString newAttributName() { return m_sTargetURL; }

	void showPropertiesDialog();
	void showFindFileDialog();
	void showProgressDialog( VFS::Operation eOperation, bool bMove=FALSE, const QString & sTargetPath=QString::null );
	void removeProgressDlg( bool bCheckWhetherCanToClose=FALSE );

	bool needToChangeFS( QString & sInputURL );

	QString rootName() const;

	QString currentURL() const { return m_pVFS->path(); }
	QString targetURL()  const { return m_sTargetURL; }
	QString absHostURL() const;

	QString absoluteFileName( const QString & sFileName ) const;
	QString previousDirName() const { return m_sPreviousDirName; }

	bool hasFocus() const { return m_pListView->hasFocus(); }

	static VFS::KindOfFilesSystem kindOfFilesSystem( const QString & sFullFileName );

	static bool archiveIsSupported( const QString & sFileName );

	VFS::KindOfFilesSystem kindOfCurrentFS() const { return m_eCurrentFilesSystem; }

	KindOfListView kindOfViewFilesPanel() const { return m_pListView->kindOfView(); }

	void parsePath( QString & sPath, bool bAlwaysAddSlash=TRUE );

	bool setWeighInPropetiesDlg();
	void initPropetiesDlgByUrl( const UrlInfoExt & uUrlInfo, const QString & sLoggedUserName );
	void createFileView();
	void setFilesAssociation( FilesAssociation *pFilesAssociation ) { m_pFilesAssociation = pFilesAssociation; }


	virtual void setRemoveToTrash( bool ) {};
	virtual void setWeighBeforeOperation( bool ) {};
	virtual void setTrashPath( const QString & ) {};
	virtual void setWorkDirectoryPath( const QString & ) {};
	virtual void setStandByConnection( bool ) {};
	virtual void setNumOfTimeToRetryIfServerBusy( uint ) {};


	void setAlwaysOverwrite( bool bAlwaysOverwite )          { m_bAlwaysOverwrite = bAlwaysOverwite; }
	void setAlwaysSavePermission( bool bSavePerm )           { m_bSavePermision   = bSavePerm; }
	void setAlwaysAskBeforeDeleting( bool bAskBeforeDelete ) { m_bAskBeforeDelete = bAskBeforeDelete; }
	void setAlwaysGetInToDirectory( bool bGetInToDir )       { m_bGetInToDir      = bGetInToDir; }
	void setCloseProgressDlgAfterFinished( bool bCloseAfterFinished ) { s_bCloseProgressDlgAfterFinished = bCloseAfterFinished; }

	void putFile( const QByteArray & baBuffer, const QString & sFileName ) {
		if ( m_pVFS ) m_pVFS->putFile( baBuffer, sFileName );
	}

	static QString cmdString( VFS::Operation eOperation ); // TYLKO DLA TESTOW

private:
	ListView *m_pListView;
	VFS *m_pVFS;

    FindFileDlg    *m_pFindFileDlg;
	ProgressDlg *m_pProgressDlg;
	PropertiesDlg  *m_pPropertiesDlg;
	FilesAssociation *m_pFilesAssociation;

	QString m_sTargetURL;
	QString m_sCurrentURL, m_sNewURL;
	QString m_sPreviousDirName;

	VFS::KindOfFilesSystem m_eCurrentFilesSystem;

	QStringList m_slSelectedFilesList;
	bool m_bSavePermision, m_bAlwaysOverwrite, m_bHardLink, m_bAskBeforeDelete, m_bGetInToDir;

	int m_nItemIdForOpenedFile;
	bool m_bOpenedFileEditMode;

	Q3Process m_Process;

	bool m_bFoundFilesSys;


	uint numOfAllFiles()  { return 0; } // ! tymczasowa definicja

	void decodePermissions( int & nPermissions, const QString & sOldPerm, const QString & sNewPerm );
	void decodeTime( QDateTime & dtTime, const QDateTime & dtOldTime, const QString & sNewTime );

	void runExternalViewer( const QStringList & slFilesToView );

public slots:
	void slotCloseCurrentDir();
	void slotOpen( const UrlInfoExt & ui );

	void slotInitializeShowsList();
	void slotSetCurrentURL( const QString & sNewURL );
	void slotGetUrlInfoFromList( const QString & sFileName, UrlInfoExt & uUrlInfo );

	void slotShowFileOverwriteDlg( const QString & sSourceFileName, QString & sTargetFileName, int & nResult );
	void slotShowDirNotEmptyDlg( const QString & sDirName, int & nResult );

	void slotUpdateFindStatus( uint nMatches, uint nFiles, uint nDirectories );
	void slotStartFind( const FindCriterion & fcFindCriterion );
	void slotInsertMatchedItem( const UrlInfoExt & uUrlInfo );

	void slotReadFileToView( const QString & sFileName, QByteArray & baBuffer, int & nReadBlockSize, int nFileLoadMethod ) {
		m_pVFS->slotReadFileToView( sFileName, baBuffer, nReadBlockSize, nFileLoadMethod );
	}

	void slotStartProgressDlgTimer( bool bStart );

private slots:
	void slotChangePropertiesOfFile( const UrlInfoExt & uNewUrlInfo, bool bAllSelected );
	void slotCloseAfterFinished( bool bCloseState );
	void slotCountingDirectory( int nItemNum );

	void slotOperationInBackground();

	void slotCancelOperation();

	void slotClosePropertiesDlg();
	void slotCloseFindFileDlg();

	void slotDeleteFile( const QString & sFileName );
	void slotJumpToTheFile( const QString & sFileName );
	void slotViewFile( const QString & sFileName, bool bEditMode );

	void slotShowInPanel( Q3ListView * pListView );

signals:
	void signalSetInfo( const QString & sInfo );
	void signalResultOperation( VFS::Operation eOperation, bool bNoError );

	void signalDataTransferProgress( long long nDone, long long nTotal, uint nBytesTransfered );
	void signalFileCounterProgress( int nValue, bool bFileCounting, bool bSetTotalValue );
	void signalNameOfProcessedFile( const QString & sFileName );
	void signalSetOperation( VFS::Operation op );
	void signalTotalProgress( int nDone, long long nTotalWeight ); // done in percent

	void signalOpenArchive( const QString & sFileName );

	void signalPutFile( const QByteArray & baBuffer, const QString & sFileName );

	void signalChangePanel( Panel::KindOfPanel eKindOfPanel, const QString & sInitURL, bool bCurrent );

};

#endif
