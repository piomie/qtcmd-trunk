/***************************************************************************
                          fileview.h  -  description
                             -------------------
    begin                : Sun Oct 27 2002
    copyright            : (C) 2002 by Piotr Mierzwiński
    email                : peterm@go2.pl

    copyright            : See COPYING file that comes with this project

 $Id$
***************************************************************************/

/** @file doc/api/src/fileview.dox */

#ifndef _FILEVIEW_H_
#define _FILEVIEW_H_

#include "panel.h"
#include "fileinfoext.h"
#include "view.h"

#include <QKeyEvent>
#include <QMap>

class QTimer;
class QAction;
class QMenuBar;
class QToolBar;
class QSplitter;
class QTreeWidget;
class QTreeWidgetItem;
class QToolButton;
class QStringList;
class StatusBar;
class QCloseEvent;


class FileView : public Panel
{
	Q_OBJECT
public:
	FileView( const QStringList & slFilesList, uint nCurrentItem, bool bEditMode=FALSE, QWidget *pParent=0, const char *sz_pName=0 );
	~FileView();

	void setFilesAssociation( FilesAssociation *pFA )    { m_pFilesAssociation = pFA; }
	void setKeyShortcuts( KeyShortcuts * pKeyShortcuts ) { m_pKeyShortcuts = pKeyShortcuts; }

	QString currentURL() const { return FileInfoExt::filePath(m_sCurrentFileName); }
	QString sFileName()   const { return m_sCurrentFileName; }

	int kindOfView() const { return (int)m_eKindOfView; }

	void insertFilesList( const QStringList & slFilesList );

	void initLoadFileToView( FilesAssociation * pFA );

	bool fileViewCreated() const { return TRUE; }
	Panel::KindOfPanel kindOfPanel() const { return Panel::FILEVIEW_PANEL; }

    // Panel methods
    void updateView() {}
public slots:
    void slotChangeKindOfView(uint) {}

private:
	View      *m_pView;
	QSplitter *m_pSplitter;
	QWidget   *m_pViewParent;
	StatusBar *m_pStatus;
	QTreeWidget *m_pSideListView;


	QWidget   *m_pSettingsWidget;

	FilesAssociation *m_pFilesAssociation;

	KindOfFile m_eKindOfFile;
	View::KindOfView m_eKindOfView;
	View::ModeOfView m_eModeOfView;

	QString m_sCurrentFileName, m_sInitFileName, m_sCurrentDirPath;

	bool m_bViewInWindow, m_bInitEditMode;
	QMenuBar    *m_pMenuBar;
    QMenu       *m_pFileMenu, *m_pViewMenu, *m_pEditMenu, *m_pOptionsMenu;
    QMenu       *m_pModeOfViewMenu, *m_pRecentFilesMenu;
    QToolBar    *m_pToolBar, *m_pAddonToolBar;
    QToolButton *m_pOpenBtn;

	QByteArray m_baBinBuffer;

	QTimer         *m_pTimer;
	KeyShortcuts   *m_pKeyShortcuts;

	bool m_bShowMenuBar, m_bShowToolBar, m_bShowTheFilesList;
	uint m_nInitWidth, m_nInitHeigth, m_nSideListWidth;
	bool m_bItIsTextFile;

	QStringList m_slPluginsNameList;

	int m_nBytesRead;
	int m_nFileLoadMethod;
	bool m_bDetectIsBinOrTextFile;

    QMap<QAction*, View::ModeOfView>   m_actionsModes;

    QAction     /*m_pHelpA,*/
                *m_pOpenA,
                *m_pSaveA,
                *m_pSaveAsA,
                *m_pReloadA,
                *m_pNewA,
                *m_pMenuBarA,
                *m_pToolBarA,
                *m_pSideListA,
                *m_pRawModeA,
                *m_pEditModeA,
                *m_pRenderModeA,
                *m_pHexModeA,
                *m_pConfigureA,
                *m_pSaveSettingsA,
                *m_pQuitA;

	enum ActionsOfView {
		Help_VIEW=0,
		Open_VIEW,
		Save_VIEW,
		SaveAs_VIEW,
		Reload_VIEW,
		OpenInNewWindow_VIEW,
    	ShowHideMenuBar_VIEW,
		ShowHideToolBar_VIEW,
    	ShowHideSideList_VIEW,
    	ToggleToRawMode_VIEW,
		ToggleToEditMode_VIEW,
		ToggleToRenderMode_VIEW,
		ToggleToHexMode_VIEW,
		ShowSettings_VIEW,
		Quit_VIEW
	};


	void updateMenuBar();
	void initMenuBarAndToolBar();
	void initKeyShortcuts();
	void initView();
	void removeView();

protected:
	bool createView( const QString & sFileName, View::KindOfView _eKindOfView=View::UNKNOWNview );
	void loadingLate( const QString & sFileName );

	void keyPressEvent( QKeyEvent * pKE );
	void closeEvent( QCloseEvent * pCE );

	QString kindOfViewStr( View::KindOfView ); // FUN.TYMCZASOWA - TYLKO DO TESTOW

public slots:
	void slotReadyRead();

private slots:
	void slotLoadFile();
	void slotReadFile();
    void slotChangeModeOfView(QAction *act);
    void slotItemOfRecentFilesMenuActivated(QAction *act);

	void slotNew();
	void slotOpen();
	void slotSave();
	void slotSaveAs();
	void slotReload();

	void slotShowHideMenuBar();
	void slotShowHideToolBar();
	void slotSaveSettings();
	void slotShowSettings();

	void slotLoadFilesList();
	void slotSaveFilesList();

	void slotShowTheFilesList();
    void slotOpenCurrentItem(QTreeWidgetItem *item, int column);
	void slotApplyKeyShortcuts( KeyShortcuts * pKeyShortcuts );

signals:
	void signalReadFile( const QString & sFileName, QByteArray & baBuffer, int & nBytesRead, int nFileLoadMethod );
	void signalSetCurrentKindOfView( View::KindOfView eCurrentKindOfView );
	void signalSetKeyShortcuts( KeyShortcuts * pKeyShortcuts );
	void signalUpdateItemOnLV( const QString & sFileName );
	void signalClose();

};

#endif
