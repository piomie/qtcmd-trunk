/***************************************************************************
                          popupmenufp.cpp  -  description
                             -------------------
    begin                : Tue Aug 21 2001
    copyright            : (C) 2001 by Piotr Mierzwiński
    email                : peterm@go2.pl

    copyright            : See COPYING file that comes with this project

 $Id$
 ***************************************************************************/

#include "popupmenufp.h"
#include "fileinfoext.h"


// menu for file list/tree
#define CMD_Open               101
#define CMD_OpenInPanelBeside  102
#define CMD_OpenWith           103

#define CMD_Rename             111
#define CMD_Delete             112
#define CMD_Copy               113
#define CMD_MakeLink           114
#define CMD_Move               115
#define CMD_Weigh              116
#define CMD_Properties         117

#define CMD_SelectAll          121
#define CMD_UnSelectAll        122
#define CMD_SelectSameExt      123
#define CMD_UnSelectSameExt    124
#define CMD_SelectGroup        125
#define CMD_UnSelectGroup      126

#define CMD_CreateNewArchive   131
#define CMD_AddToArchive       132
#define CMD_ExtractHere        133
#define CMD_ExtractTo          134
#define CMD_TestArchive        135

#define CMD_EditLink           141


PopupMenuFP::PopupMenuFP( QWidget * parent, KindOfPopupMenu kindOfPopupMenu )
	: QMenu(parent), m_eKindOfPopupMenu(kindOfPopupMenu)
{
	m_bMoveFile = FALSE;

	connect( this, SIGNAL(signalOpen(Q3ListViewItem *)), parent, SIGNAL(returnPressed(Q3ListViewItem *)) );
}


PopupMenuFP::~PopupMenuFP()
{
	//qDebug("PopupMenuFP::destructor" );
}


void PopupMenuFP::showMenu( const QPoint & globalPos, const QString & sItemName )
{
	m_bMoveFile = FALSE;
	m_sCurrentItemName = sItemName;

	clear();
	if ( m_eKindOfPopupMenu == FilesPanelMENU )
		initFilesPanelMenu();
	else
	if ( m_eKindOfPopupMenu == UserDefinedMENU )
		initUserMenu();

	execCmd( exec(globalPos) );
}

void PopupMenuFP::initFilesPanelMenu()
{
	QMenu *pOpenMenu    = new QMenu(tr("&Open"));
	QMenu *pSelectMenu  = new QMenu(tr("&Select"));
	QMenu *pExtractMenu = new QMenu(tr("&Extract archive"));

	addAction( tr("&Open it"), CMD_Open, pOpenMenu );
	addAction( tr("Open in panel &beside"), CMD_OpenInPanelBeside, pOpenMenu );
	addAction( tr("Open &with"), CMD_OpenWith, pOpenMenu );
	addMenu(pOpenMenu);
	addAction( tr("Re&name"), CMD_Rename );
	addAction( tr("&Delete"), CMD_Delete );
	addAction( tr("&Copy"), CMD_Copy );
	addAction( tr("&Make a link"), CMD_MakeLink );
	addSeparator();
	addAction( tr("&Select All"), CMD_SelectAll, pSelectMenu );
	addAction( tr("&UnSelect All"), CMD_UnSelectAll, pSelectMenu );
	addAction( tr("Select &with this same ext"), CMD_SelectSameExt, pSelectMenu );
	addAction( tr("UnSelect with &this same ext"), CMD_UnSelectSameExt, pSelectMenu );
	addAction( tr("Select &group"), CMD_SelectGroup, pSelectMenu );
	addAction( tr("UnSelect group"), CMD_UnSelectGroup, pSelectMenu );
	addMenu(pSelectMenu);
	addSeparator();
//	addAction( tr("Add to &archive"), CMD_AddToArchive ); // w tej opcji wybor czy nowe archiwum czy istniejace
	if (FileInfoExt::isArchive(m_sCurrentItemName)) {
		addAction( tr("&Extract to"), CMD_ExtractTo, pExtractMenu )->setEnabled(false); // not yet supported
		addAction( tr("Extract &here"), CMD_ExtractHere, pExtractMenu )->setEnabled(false); // not yet supported
		addMenu(pExtractMenu);
		addAction( tr("&Test archive"), CMD_TestArchive )->setEnabled(false); // not yet supported
		addSeparator();
	}
//	if (isLink(m_sCurrentItemName)) {
//		addAction( tr("Edit link"), CMD_EditLink );
//	}
//	addSeparator();
//	addAction( tr("&Weigh"), CMD_Weigh );
	addAction( tr("&Properties"), CMD_Properties);
}

void PopupMenuFP::initUserMenu()
{
/* TODO
 Menu uzytkownika - bedzie przygotowywane na podstawie wartosci "CMD_xxxx", ktore zostana zapisane w tablicy.
 Konfiguracje takiego menu bedzie sie odbywac poprzez drzewo. Mozliwosc dodawania i usuwania opcji w menu.
 Wyglad konfiguracji.
 Widok podzielony na dwie cz., w jednej dostepne elementy, w drugiej drzewo z gotowym menu
 Nad widokiem checkBoxy z opcjami - "FilesPanelMenu", "ViewPanelMenu"
 Ponizej checkBox z opcjami - "Uzyj standardowego menu", "Uzyj wlasnego menu"
*/
}


void PopupMenuFP::execCmd( QAction * pAction )
{
	if (pAction == NULL)
		return;

	int nActionId = pAction->statusTip().toInt();

	switch( nActionId )
	{
	// TODO: sygnaly, ktore nie maja parametru zamienic na jeden z param. KindOfAction, gdzie
	// KindOfAction byloby wartościa enum okreslajacej jedna z akcji, np. OPENkoa, DELETEkoa, itp.
		case CMD_Open:
			emit signalOpen( 0 );
			break;
		case CMD_OpenWith:
			emit signalOpenWith();
			break;
		case CMD_OpenInPanelBeside:
			emit signalOpenInNextPanel( FALSE ); // get current file for open
			break;
		case CMD_Rename:
			emit signalRename( 0 ); // column 0 (file name - ListView::NAMEcol)
			break;
		case CMD_Delete:
			emit signalDelete();
			break;
		case CMD_MakeLink:
			emit signalMakeLink();
			break;
		case CMD_Copy:
			emit signalCopy( FALSE, m_bMoveFile );
			break;
// 		case CMD_CreateNewArchive:
// 			emit signalCreateNewArchive();
// 			break;
		case CMD_SelectAll:
			emit signalSelectAll( FALSE );
			break;
		case CMD_UnSelectAll:
			emit signalSelectAll( TRUE );
			break;
		case CMD_SelectSameExt:
			emit signalSelectSameExt( FALSE );
			break;
		case CMD_UnSelectSameExt:
			emit signalSelectSameExt( TRUE );
			break;
		case CMD_SelectGroup:
			emit signalSelectGroup( TRUE );
			break;
		case CMD_UnSelectGroup:
			emit signalSelectGroup( FALSE );
			break;
		case CMD_Properties:
			emit signalShowProperties();
			break;

		default:
			break;
	}
}


QAction * PopupMenuFP::addAction( const QString & sTitle, int nId, QMenu *pMenu )
{
	QAction * pAction = NULL;

	if (pMenu != NULL)
		pAction = pMenu->QMenu::addAction( sTitle );
	else
		pAction = QMenu::addAction( sTitle );

	pAction->setStatusTip( QString("%1").arg(nId) );

	return pAction;
}

