/***************************************************************************
                          pathview.h  -  description
                             -------------------
    begin                : Wed Oct 23 2002
    copyright            : (C) 2002 by Piotr Mierzwi�ski
    email                : peterm@go2.pl

    copyright            : See COPYING file that comes with this project

 $Id$
 ***************************************************************************/


#ifndef _PATHVIEW_H_
#define _PATHVIEW_H_

#define PATHVIEW_HEIGHT		27

#include "locationchooser.h"
#include "listview.h"
#include "vfs.h" // for DirWritable

#include <qwidget.h>
//Added by qt3to4:
#include <QResizeEvent>
#include <QEvent>

/**
 * Author:
 * Piotr Mierzwi�ski
 **/
/** @short Klasa pokazuj�ca list� ze �cie�kami oraz przycisk z dodatkowymi opcjami.
 W klasie tworzony jest obiekt listy �cie�ek (@em LocationChooser) z przyciskiem
 zawieraj�cym menu. Obs�ugiwany jest tutaj prosty filtr dla linijki edycjnej
 uniemo�liwiaj�cy wpisanie b��dnego URLa. Ponadto zawarta jest w tej klasie
 prosta obs�uga kompletowania �cie�ki do katalogu, na podstawie widoku listy
 element�w bie��cego katalogu.
 Dodatkow� cech� obiektu tej klasy jest zmiana t�a widoku w zale�no�ci od
 statusu zapisywalno�ci bie��cego katalogu przez aktualnie zalogowanego
 u�ytkownika. T�o jest blado zielone je�li mo�na zapisywa�, blado czerwone -
 je�eli brak tej mo�liwo�ci oraz bia�e gdy nie jest znany ten status.
*/
class PathView : public QWidget
{
	Q_OBJECT
public:
	/** Konstruktor klasy.
	 @param pListView - wska�nik na list� plik�w,
	 @param pParent - wska�nik na obiekt rodzica,
	 @param szName - nazwa dla obiektu.
	 Inicjowane s� tutaj niezb�dne sk�adowe oraz tworzone obiekty klasy ComboPath
	 oraz QToolButton (przycisk z menu).
	 */
	PathView( Q3ListView *pListView, QWidget *pParent, const char *szName=0 );

	/** Destruktor klasy.
	 Nast�puje tutaj zapisanie do pliku konfiguracyjnego listy URLi, oraz id dla
	 bie��cego elementu listy, poza tym usuwane s� obiekty tworzone w klasie.
	 */
	~PathView();

	enum PathAutoCompletion { NONE_COMPLETION=0, APPEND_FIRST_MATCHING_NAME, SHOW_MATCHING_DIR_LIST };

	/** Zwraca bie��c� �cie�k�.
	 @return bie��cy ci�g na li�cie.
	 */
	QString currentUrl() const { return m_pComboPathView->text(); }

	/** Zwraca indeks bie��cego elementu na li�cie @em ComboPath .
	 @return indeks bie��cego elementu.
	 */
	int currentId() const { return m_pComboPathView->currentId(); }

	/** Zwraca poprzedni wzgl�dem bie��cego element listy.
	 @param bGetFromLocalFS - r�wny TRUE, wtedy szukana jest �cie�ka z lokalnego systemu
	 plik�w, w przeciwnym razie zwr�cona zostanie pierwsza napotkana.
	 @return poprzedni URL.
	 Przy czym, je�li bie��cy URL jest pierwszym, wtedy zwracany jest ostatni.
	 */
	QString prevUrl( bool bGetFromLocalFS=FALSE ) const;

	/** Zwraca nast�pny wzgl�dem bie��cego element listy.
	 @return nast�pny URL.
	 Przy czym, je�li bie��cy URL jest ostatnim, wtedy zwracany jest pierwszy.
	 */
	QString nextUrl() const {
		return m_pComboPathView->nextEntry();
	}

	/** Zwraca informacj� o obecno�ci focusa na widoku �cie�ki.
	 @return TRUE, je�li widok �cie�ki zawiera focus, FALSE widok go nie posiada.
	 */
	bool hasFocus() const {
		return m_pComboPathView->hasFocus();
	}

	/** Ustawia focus na widoku �cie�ki.
	 */
	void setFocus() {
		m_pComboPathView->setFocus();
	}

	/** Rozwija list� �cie�ek z odwiedzonymi katalogami.
	 */
	void showPopupList() {
		m_pComboPathView->showPopupList();
	}

	/** Powoduje pokazanie menu przycisku znajduj�cego si� obok widoku listy �cie�ek.
	 */
	void showButtonMenu() {
		m_pComboPathView->showButtonMenu();
	}

	/** Metoda ustawia tryb kompletowania �cie�ki.
	 @param ePAC - tryb kompletowania.
	 */
	void SetPathAutoCompletion( PathAutoCompletion ePAC ) {
		m_ePathAutoCompletion = ePAC;
	}

private:
	LocationChooser *m_pComboPathView;
	Q3ListView *m_pListView;
	QString m_sStr, mOldStr, m_sSide;
	//QStringList m_slPathesList;
	bool m_bRestoreOldPath;
	bool m_bTextInserted; ///< status wstawionego tekstu; u�ywane przez slotTextChanged
	PathAutoCompletion m_ePathAutoCompletion; ///< tryb kompletowania �cie�ki

	/** Ustawia kolor t�a widoku �cie�ki na blado zielony lub blado czerwony.
	 Przed ustawieniem koloru nast�puje sprawdzenie statusu bie��cej �cie�ki.
	*/
	void setStatusWritable();

	/** Kompletowanie bie��cej �cie�ki.
	 @param sPath - aktualna zawarto�� linijki edycyjnej.
	 Metoda pr�buje uzupe�ni� wpisan� w linii edycyjnej nazw�,
	 dopasowuj�c ci�g do nazw na przekazane do obiektu (poprzez konstruktor) li�cie rodzica.
	 Je�li dopasowanie si� powiedzie, wtedy pokazywany jest pod�wietlony
	 nowy pasuj�cy fragment nazwy, a kursor ustawiany zostaje na jego pocz�tek.
	*/
	void pathAutoCompletion( const QString & sPath );

	/** Szukanie podobnego elementu na li�cie.
	 @param sItemName - szukany fragment nazwy.
	 Metoda sprawdza czy na li�cie (jej wska�nik zosta� podany w konstruktorze)
	 wyst�puje element, w kt�rego nazwie zawiera si� podany ci�g, je�li to prawda,
	 wtedy jest zwracana pierwsza taka nazwa.
	 */
	QString listContainsItem( const QString & sItemName );

protected:
	/** Obs�uga zmiany rozmiaru dla bie��cego obiektu.
	 @param e - wska�nik na zdarzenie zmiany rozmiaru.
	 */
	void resizeEvent( QResizeEvent *e );

	/** Przechwytywanie wszystkich zdarze� dla obiektu.
	 @param o - obiekt, kt�rego dotyczny zdarzenie,
	 @param e - zdarzenie na podanym obiekcie.
	 Wewn�trz przechwytywane jest zdarzenie naci�ni�cia klawiszy Backspace oraz Delete.
	 Wykonywane jest to w celu zapewnienia poprawnego kompletowania �cie�ki.
	 */
	bool eventFilter( QObject *o, QEvent *e );

public slots:
	/** Powoduje wstawienie na list� nowego elementu (�cie�ki).
	 @param sNewPath - nazwa elementu do usuni�cia.
	 Przy czym element jest wstawiany, tylko je�li previousURL sam nie istnieje ju� na
	 li�cie.
	 */
	void slotPathChanged( const QString & sNewPath );

	/** Powoduje usuni�cie podanego elementu (�cie�ki) z listy.
	 @param sPathName - nazwa elementu do usuni�cia.
	 Przy czym jako bie��cy ustawiany jest poprzedni, a je�li nie istnieje
	 poprzedni, wtedy brany jest nast�pny. Je�li lista zawiera 1 lub 0 element�w,
	 wtedy nic nie jest robione.
	 */
	void slotRemovePath( const QString & sPathName ) {
		m_pComboPathView->removeItem( sPathName );
	}

private slots:
	/** Wywo�uje sygna� uruchamiaj�cy zmian� bie��cej �cie�ki w systemie plik�w.
	 @param sNewPath - nowa �cie�ka.
	 Uaktualniany jest tutaj r�wnie� kolor t�a widoku �cie�ki informuj�cy
	 o zapisywalno�ci, b�d� jej braku, dla nowej �cie�ki w odniesieniu do
	 aktualnie zalogowanego u�ytkownika.
	 */
	void slotChangePathInFS( const QString & sNewPath );

	/** Slot obs�uguje filtr edycyjny dla widoku �cie�ki oraz proste
	 kompletowanie dla niej.
	 @param sTxt - nowy tekst w widoku �cie�ki.
	 Slot uruchamiany jest przy ka�dej zmianie tekstu w widoku �cie�ki.
	 */
	void slotTextChanged( const QString & sTxt );


	/** Pokazuje okienko s�u��ce edycji historii �cie�ek.
	 Pozwala ono na: usuni�cie, edycj� oraz zmian� kolejno�ci na li�cie.
	 */
	void slotEditHistory();

signals:
	/** Sygna� powoduje pobranie statusu zapisywalno�ci bie��cego katalogu przez
	 aktualnie zalogowanego u�ytkownika.
	 @param dw - zmienna, kt�rej zapisana zostanie informacja, patrz te�:
	  @see DirWritable.
	 */
	void signalPossibleDirWritable( VFS::DirWritable & dw );

	/** Sygna� uruchamia zmian� bie��cego katalogu na podany.
	 @param newPath - nowa �cie�ka do odczytania.
	 */
	void signalPathChanged( const QString & newPath );

};

#endif
