/***************************************************************************
                         qtcmd.h  -  description
                            -------------------
   begin                : nie lut 20 2005
   copyright            : (C) 2005 by Piotr Mierzwinski
   email                : peterm@o2.pl

   copyright            : See COPYING file that comes with this project

$Id$
***************************************************************************/

/** @file doc/api/src/qtcmd.dox */

#ifndef QTCMD_H
#define QTCMD_H

#include <QResizeEvent>
#include <QMainWindow>
#include <QToolButton>
#include <QToolBar>
#include <QAction>
#include <QMenu>

#include "panel.h"

#include "vfs.h"

class QSplitter;
class QPushButton;


class Panel; // Panel contains follows forwarding: Filters, KeyShortcuts, FilesAssociation
//class ButtonsBar;

class QtCmd : public QMainWindow
{
    Q_OBJECT
public:
    QtCmd(const QString & sLeftURL=QString::null, const QString & sRightURL=QString::null, QWidget* pParent=0,
                const char* sz_pName=0, Qt::WFlags f=Qt::WType_TopLevel);
    ~QtCmd();

private:
	void initSettings();
	void initBars();
	void initButtonsBar();
	void initKeyShortcuts();
	void initKeyShortcutsForView();

	enum PanelSide { LEFT_PANEL=0, RIGHT_PANEL, NO_PANEL };

	Panel * panel( bool bCurrent=TRUE );// const;

    void initPanels( const QString & sLeftURL, const QString & sRightURL );
    void createPanel( PanelSide ePanelSide, Panel::KindOfPanel eKindOfPanel, const QString & sURL=QString::null );

private:
    int m_nWidth, m_nHeight;
    int m_nWidthOfLeftPanel, m_nWidthOfRightPanel;

    bool m_bFlatButtonBar;
    bool m_bShowQuitWarn;

    enum ButtonBarKeys {
        HELP_BTN=0, PANELVIEW_BTN, FILEVIEW_BTN, FILEEDIT_BTN, COPY_BTN, MOVE_BTN, MKDIR_BTN, DELETE_BTN, QUIT_BTN
    };
    QPushButton *m_btpButtonBar[10];

    enum PanelProportion { PP_5050=0, PP_3070, PP_7030, PP_USER };
	enum ToolBars { MENU_ID=0, TOOLBAR_ID, BUTTONBAR_ID };

    QSplitter *m_pSplitter;
    Panel::KindOfPanel m_eKindOfLeftPanel;
    Panel::KindOfPanel m_eKindOfRightPanel;
    Panel *m_pLeftPanel;
    Panel *m_pRightPanel;

    Filters          *m_pFilters;
    KeyShortcuts     *m_pKeyShortcuts;
    KeyShortcuts     *m_pKeyShortcutsForView;
    FilesAssociation *m_pFilesAssociation;


	QToolBar    *m_pToolBar, *m_pButtonBar;
	QToolButton *m_pChangeKindOfViewToolBtn, *m_pProportionsBtn;
	QMenu       *m_pViewMenu, *m_pKindOfVFPmenu, *m_pProportionsOfPanelsMenu, *m_pFavoritesMenu;

	QAction *m_pOpenFileA, *m_pOpenDirA, *m_pOpenArchA, *m_pQuitAppsA;
	QAction *m_pShowHideMenuBarA, *m_pShowHideToolBarA, *m_pShowHideBtnBarA;
	QAction *m_pConfigureA, *m_pConfViewA;
	QAction *m_pFtpMmngA, *m_pFreeSpaceA, *m_pStorageA, *m_pRereadA, *m_pFavoritesA, *m_pFindFilesA, *m_pQuickGetOutA;

	QActionGroup *m_pLVactGroup, *m_pProporActGroup, *m_pShowHideActGroup;

    QWidget  *m_pSettingsAppWidget, *m_pSettingsWidget;

    PanelSide m_eCurrentPanel;

	/** Identyfikatory akcji skr�t�w klawiszowych dla okna podgl�du.
	 */
	enum ActionsOfView {
		Help_VIEW=0,
		Open_VIEW,
		Save_VIEW,
		SaveAs_VIEW,
		Reload_VIEW,
		OpenInNewWindow_VIEW,
		ShowHideMenuBar_VIEW,
		ShowHideToolBar_VIEW,
		ShowHideSideList_VIEW,
		ToggleToRawMode_VIEW,
		ToggleToEditMode_VIEW,
		ToggleToRenderMode_VIEW,
		ToggleToHexMode_VIEW,
		ShowSettings_VIEW,
		Quit_VIEW
	};

private slots:
    void slotFileView();
    void slotFileEdit();
    void slotCopy();
    void slotMove();
    void slotMakeDir();
    void slotDelete();
    void slotMakeLink();

	void slotChangeProportionOfPanels( QAction *pAction );
	void slotChangeListView( QAction *pAction );

	void slotShowHideMenuBar();
	void slotShowHideToolBar();
	void slotShowHideButtonBar();

    void slotChangeView();

    void slotShowAppSettings();
    void slotShowViewSettings();

    void slotShowAboutQt();
    void slotShowAbout();
    void slotShowHelp();

    void slotOpenFile();
    void slotOpenDirectory();
    void slotOpenArchive();
    void slotQuit();

public slots:
    void slotChangePanel( Panel::KindOfPanel eKindOfPanel, const QString & sInitURL, bool bCurrent );
    void slotHeaderSizeChange( int nSection, int nOldSize, int nNewSize );
    void slotSetPanelBesideURL( const QString & sUrl );
    void slotUpdateItemOnLV( const QString & sFileName );
    void slotCloseView();
    void slotGetKindOfFSInPanelBeside( VFS::KindOfFilesSystem & eKindOfFS );
    void slotCopyFiles( bool bMove );
    void slotUpdateSecondPanel( int nUpdateListOp, Q3ValueList<Q3ListViewItem *> *pItemsList, const UrlInfoExt & uNewUrlInfo );
    void slotGetSILfromPanelBeside( Q3ValueList<Q3ListViewItem *> *& pSelectedItemsList );
    void slotApplyKeyShortcuts( KeyShortcuts *pKeyShortcuts );
    void slotApplyFilters( Filters * pFilters );
    void slotPutFile( const QByteArray & baBuffer, const QString & sFileName );

signals:
    void signalSetNewKindOfView( uint kindOfView );
    void signalQuickGetOutFromFS();
    void signalShowFtpManagerDlg();
    void signalRereadCurrentDir();
    void signalShowFavorites();
    void signalShowFreeSpace();
    void signalDevicesList();
    void signalFindFile();

    void signalApplyListViewSettings( const ListViewSettings & pLVS );
    void signalApplyOtherAppSettings( const OtherAppSettings & pOAS );
    void signalApplyFileSystemSettings( const FileSystemSettings & pFSS );
    void signalSetKeyShortcuts( KeyShortcuts * pKeyShortcuts );
    void signalSetFilesAssociation( FilesAssociation *pFA );


protected:
    void resizeEvent( QResizeEvent * pRE );
};

#endif
