/***************************************************************************
                          popupmenufp.h  -  description
                             -------------------
    begin                : Tue Aug 21 2001
    copyright            : (C) 2002, 2003 by Piotr Mierzwi�ski
    email                : peterm@go2.pl

    copyright            : See COPYING file that comes with this project

 $Id$
 ***************************************************************************/
#ifndef POPUPMENUFP_H
#define POPUPMENUFP_H

#include <QMenu>
#include <QKeyEvent>

#include <q3listview.h>

/**
 * Author:
 * Piotr Mierzwi�ski
 **/
/** @short Klasa obsluguje menu podr�czne panela plik�w.
 Inicjowane jest tutaj menu podr�czne standardowymi opcjami, po wybraniu opcji
 wysy�any jest odpowiedni sygna� dla niej.
*/
class PopupMenuFP : public QMenu  {
   Q_OBJECT
public:
	/** Rodzaj menu.
	 */
	enum KindOfPopupMenu {
		FilesPanelMENU = 0,
		TextFileMENU, ImageFileMENU, VideoFileMENU, SoundFileMENU, BinaryFileMENU, // context menu for FileView window
		UserDefinedMENU
	};

	/** Konstruktor klasy.
	 @param parent - wska�nik na rodzica,
	 @param kindOfPopupMenu - rodzaj menu, patrz: @see KindOfPopupMenu,
	 @param name - nazwa dla obiektu.
	 Inicjowane s� tu sk�adowe, inicjowane s� opcje dla menu oraz ��czone sygna�y
	 ze slotami.
	 */
	PopupMenuFP( QWidget *parent=NULL, KindOfPopupMenu kindOfPopupMenu=FilesPanelMENU );

	/** Destruktor.
	 Brak definicji.
	 */
	~PopupMenuFP();

	/** Pokazuje dialog menu na podanej pozycji.
	 @param globalPos - pozycja dla menu.
	 @param sItemName - nazwa kliknietego elementu
	 */
	void showMenu( const QPoint & globalPos, const QString & sItemName=QString::null );

private:
	KindOfPopupMenu m_eKindOfPopupMenu;
	QString m_sCurrentItemName;
	bool m_bMoveFile;

	/** Inicjowane s� tutaj opcje dla standardowego menu panela plik�w.
	 */
	void initFilesPanelMenu();

	/** Inicjowane s� tutaj opcje dla menu panela plik�w - zdefiniowane przez
	 u�ytkownika.
	 */
	void initUserMenu();

	/** Uruchamia akcje okre�lon� w menu.
	 @param pAction - wska�nik akcji dla polecenia z menu.
	 Wysy�ane s� tutaj odpowiadaj�ce opcj� sygna�y.
	 */
	void execCmd( QAction * pAction );

	/**
	 @param sTitle nazwa w menu
	 @param nId identyfikator zapisywany we wlasciwosci 'statusTip'
	 @param pMenu jesli rowne NULL to dodaj do biezacego obiektu w przeciwny razie do podanego
	*/
	QAction * addAction( const QString & sTitle, int nId, QMenu *pMenu=NULL );

signals:
	/** Powoduje uruchomienie wybranego polecenia.
	 @param cmd - kod polecenia,
	 @param param1 - parametr pierwszy,
	 @param param2 - parametr drugi.
	 */
	void signalRunCommand( int cmd, int param1, int param2 );

	/** Sygna� wysy�any po wybraniu opcji 'Open'.
	 @param item - wska�nik na element do otwarcia (0 oznacza bie��cy).
	 Uruchamia procedur� otwarcia pliku lub katalogu.
	 */
	void signalOpen( Q3ListViewItem * item );

	/** Sygna� wysy�any po wybraniu opcji 'Open with'
	 Uruchamia procedur� otwarcia pliku za pomoc� wybranego programu.
	 */
	void signalOpenWith();

	/** Sygna� wysy�any po wybraniu opcji 'OpenInPanelBeside'.
	 @param dirForce - TRUE oznacza otwarcie katalogu (je�li kursor nie stoi
	 na elemencie b�d�cym katalogiem, wtedy otwierany jest bie��cy), FALSE
	 powoduje otwarcie pliku w panelu obok.
	 Uruchamia procedur� otwarcia pliku lub katalogu w panelu obok.
	 */
	void signalOpenInNextPanel( bool dirForce );

	/** Sygna� wysy�any po wybraniu opcji 'Rename'.
	 @param column - numer kolumny w kt�rej nale�y zmieni� warto�� (tutaj
	 zawsze podawane jest 0, oznaczaj�ce kolumn� z nazw�).
	 Uruchamia procedur� zmiany nazwy pliku.
	 */
	void signalRename( int column );

	/** Sygna� wysy�any po wybraniu opcji 'Delete'.
	 Uruchamia procedur� usuni�cia bie��cego elementu.
	 */
	void signalDelete();

	/** Sygna� wysy�any po wybraniu opcji 'MakeLink'.
	 Uruchamia procedur� utworzenia linku dla bie��cego elementu.
	 */
	void signalMakeLink();

	/** Sygna� wysy�any po wybraniu opcji 'Copy'.
	 @param shiftPressed - TRUE oznacza, �e wci�ni�ty jest klawisz Shift.
	 @param move - TRUE wymusza przenoszenie, w przeciwnym razie jest to
	 kopiowanie.
	 Uruchamia procedur� kopiowania lub przenoszenia zaznaczonych lub
	 bie��cego elementu.
	 */
	void signalCopy( bool shiftPressed, bool move );

	/** Sygna� wysy�any po wybraniu opcji 'SelectAll' lub 'UnSelectAll'.
	 @param select - TRUE oznacza, �e wykonywane b�dzie zaznaczenie, w przeciwnym
	 razie elementy zostan� odznaczone.
	 Uruchamia procedur� zaznaczenia lub odznaczenia wszystkich element�w.
	 */
	void signalSelectAll( bool select );

	/** Sygna� wysy�any po wybraniu opcji 'SelectSameExt'.
	 @param select - TRUE oznacza, �e wykonywane b�dzie zaznaczenie, w przeciwnym
	 razie elementy zostan� odznaczone.
	 Uruchamia procedur� zaznaczenia lub odznaczenia element�w z tym samym
	 rozszerzeniem, jakie ma bie��cy plik.
	 */
	void signalSelectSameExt( bool select );

	/** Sygna� wysy�any po wybraniu opcji 'SelectGroup' lub 'UnSelectGroup'.
	 @param select - TRUE oznacza, �e wykonywane b�dzie zaznaczenie, w przeciwnym
	 razie elementy zostan� odznaczone.
	 Uruchamia procedur� powoduj�c� pokazanie dialogu zaznacze� grupowych
	 (podawany jest tu wzorzec, kt�ry b�dzie dopasowywany do plik�w).
	 */
	void signalSelectGroup( bool select );

	/** Sygna� wysy�any po wybraniu opcji 'Properties'.
	 Uruchamia procedur� powoduj�c� pokazanie dialogu w�a�ciwo�ci dla zaznaczonych
	 lub bie��cego pliku.
	 */
	void signalShowProperties();

//	void signalCreateNewArchive();
//	void signalAddToArchive();
//	void signalExtractArchive();
//	void signalExtractTo();
//	void signalTestArchive();

};

#endif
