/***************************************************************************
                          archivespage  -  description
                             -------------------
    begin                : nie gru 23 2007
    copyright            : (c) 2007 by Piotr Mierzwiński, Mariusz Borowski
    email                : piom@nes.pl, mariusz@nes.pl

    copyright            : See COPYING file that comes with this project

 $Id$
 ***************************************************************************/
#ifndef ARCHIVESPG_H
#define ARCHIVESPG_H

#include <QProcess>
#include <QStringList>

#include "ui_archivespage.h"

/**
	@author Piotr Mierzwiński <piom@nes.pl>
*/
class ArchivesPg : public QWidget, Ui::ArchivesPage
{
	Q_OBJECT

private:
	enum Columns  { AVAILABLEcol=0, NAMEcol, COMPRESSIONcol, FULLNAMEcol };
	enum RARsett  { CreateSolidArchive=0, RarSfxArchive, RarSaveSymbolicLinks, SaveFileOwnerAndGroup, MultimediaCompression, OverwriteExistingFiles, DictionarySize };
	enum ZIPsett  { ZipSfxArchive=0, ZipSaveSymbolicLinks, ZipDoNotSaveExtraAttrib };
	enum ARCHpath { BZIP2path, GZIPpath, RARpath, TARpath, ZIPpath };

	int m_ntRarAddsSettings[7];
	int m_ntZipAddsSettings[3];

	QStringList m_slArchiverFullNameList;

	QProcess *m_pProcess;

	void init();
	// enable/disable all column in given row except first
	void setRowEnable( int nRow, bool bEnable );

public:
	ArchivesPg();
	~ArchivesPg();

	QString workDirPath();
	QString compressionLevel( const QString & sArchiverName );
	QString archiverFullName( const QString & sArchiverName );

private slots:
	void slotEditBoxTextApplied( QTableWidgetItem *pItem, const QString & sNewText );
	void slotDetectSupportedArchives();
	void slotShowAdditionalSettings();
	void slotChangePathForArchiver();
	void slotItemSelectionChanged();
	void slotReadDataFromStdOut();
	void slotSetDefaults();

	void slotButtonClicked( QTableWidgetItem *pItem );

public slots:
	void slotSave();

};

#endif
