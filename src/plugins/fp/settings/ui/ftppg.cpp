/***************************************************************************
                          ftppg  -  description
                             -------------------
    begin                : nie gru 23 2007
    copyright            : (C) 2007 by Piotr Mierzwiński, Mariusz Borowski
    email                : piom@nes.pl, mariusz@nes.pl

    copyright            : See COPYING file that comes with this project

 $Id$
 ***************************************************************************/
//#include <QSettings> // TODO add support for QSettings

#include "ftppg.h"


FtpPg::FtpPg()
{
	setupUi(this);

	connect( m_pDefaultBtn, SIGNAL(clicked()), this, SLOT(slotSetDefaults()) );

	init();
}


void FtpPg::init()
{
	/*
	QSettings settings;

	bool bStandByConnectChkBtn = settings.readBoolEntry( "/qtcmd/FTP/StandByConnect", TRUE );
	int  nNumOfTimeToRetryIfBusy = settings.readBoolEntry( "/qtcmd/FTP/NumOfTimeToRetryIfBusy", 10 );

	m_pStandByConnectChkBox->setChecked( bStandByConnectChkBtn );
	m_pNumOfTimeToRetryIfFTPbusySpinBox->setValue( nNumOfTimeToRetryIfBusy );
	*/
}

int FtpPg::numOfTimeToRetryIfBusy()
{
	return m_pNumOfTimeToRetryIfFTPbusySpinBox->value();
}

void FtpPg::slotSetDefaults()
{
	m_pStandByConnectChkBox->setChecked( TRUE );
	m_pNumOfTimeToRetryIfFTPbusySpinBox->setValue( 10 );
}

void FtpPg::slotSave()
{
	/*
	QSettings settings;

	settings.writeEntry( "/qtcmd/FTP/StandByConnect", m_pStandByConnectChkBox->isChecked() );
	settings.readBoolEntry( "/qtcmd/FTP/NumOfTimeToRetryIfBusy", m_pNumOfTimeToRetryIfFTPbusySpinBox->value() );
	*/
}

