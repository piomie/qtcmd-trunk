/***************************************************************************
                          otherfspg  -  description
                             -------------------
    begin                : pi� gru 21 2007
    copyright            : (c) 2007 by Piotr Mierzwi�ski, Mariusz Borowski
    email                : piom@nes.pl, mariusz@nes.pl

    copyright            : See COPYING file that comes with this project

 $Id$
 ***************************************************************************/
#ifndef OTHERFSPG_H
#define OTHERFSPG_H

#include "ui_otherfspage.h"

/**
	@author Piotr Mierzwi�ski <piom@nes.pl>
*/
class OtherFSPg : public QWidget, public Ui::OtherFSPage
{
	Q_OBJECT

public:
	OtherFSPg();

	void init();

public slots:
	void slotSave();

private slots:
	void slotSetDefaults();

};

#endif
