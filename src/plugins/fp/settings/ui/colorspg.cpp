/***************************************************************************
                          colorspg  -  description
                             -------------------
    begin                : nie gru 23 2007
    copyright            : (C) 2007 by Piotr Mierzwiński, Mariusz Borowski
    email                : piom@nes.pl, mariusz@nes.pl

    copyright            : See COPYING file that comes with this project

 $Id$
 ***************************************************************************/
#include <QtGui>
#include <QSettings> // TODO add support for QSettings

//#include "functions_col.h" // for color() and colorToString() // TODO zrezygnowac, bo w Qt jest met.name() dla QColor
#include "buttonColor.xpm"

#include "colorspg.h"


ColorsPg::ColorsPg()
{
	setupUi(this);

	QList <QToolButton *> buttonList;
	buttonList  << m_pItemsColBtn << m_pItemsColUnderCursorBtn << m_pSelectItemsBtn << m_pSelectItemsColUnderCursorBtn
				<< m_pBgColOfSelectedItemsBtn << m_pBgColorOfSelectedItemsUnderCursorColBtn
				<< m_pFirstBgColBtn << m_pSecondBgColBtn
				<< m_pFullCursorColBtn << m_pInsideFrameCursColBtn << m_pOutFrameCursColBtn;

	m_pColorsBtnGroup = new QButtonGroup;
	for (int i=0; i<buttonList.count(); i++)
		m_pColorsBtnGroup->addButton( buttonList.at(i), i );
	connect( m_pColorsBtnGroup, SIGNAL(buttonClicked(int)), this, SLOT(slotSetColor(int)) );

	connect( m_pSchemeComboBox, SIGNAL(highlighted(int)), this, SLOT(slotSetNewScheme(int)) );
	connect( m_pNewSchemeBtn, SIGNAL(clicked()), this, SLOT(slotNew()) );
	connect( m_pDeleteSchemeBtn, SIGNAL(clicked()), this, SLOT(slotDelete()) );
	connect( m_pSaveSchemeBtn, SIGNAL(clicked()), this, SLOT(slotSave()) );
	connect( m_pGridColBtn, SIGNAL(clicked()), this, SLOT(slotSetGridColor()) );
	connect( m_pSecBgColChkBox, SIGNAL(toggled(bool)), this, SLOT(slotShowSecondBgColor(bool)) );
	connect( m_pShowGridChkBox, SIGNAL(toggled(bool)), this, SLOT(slotShowGrid(bool)) );
	connect( m_pFullCursorRadioBtn, SIGNAL(toggled(bool)), this, SLOT(slotShowFullCursor(bool)) );
	connect( m_pSchemeComboBox, SIGNAL(activated(int)), this, SLOT(slotSetNewScheme(int)) );

	init();
}

ColorsPg::~ColorsPg()
{
	delete m_pColorsBtnGroup;
}


void ColorsPg::init()
{
	QSettings settings;

	QStringList slSchemesNameList;// = settings.readListEntry( "/qtcmd/FilesPanel/ColorsSchemeList" ); // temporary solution

	if ( slSchemesNameList.count() == 0 )
		slSchemesNameList.append( tr("DefaultColors") );
	if ( slSchemesNameList.indexOf( tr("DefaultColors") ) == -1 )
		slSchemesNameList.insert( 0, tr("DefaultColors") );

	QString sScheme;// = settings.readEntry( "/qtcmd/FilesPanel/ColorsScheme", tr("DefaultColors") ); // temporary solution
	m_cslSchemesList.clear();

	ColorsScheme colorsScheme;
	QString sColor;

	for ( int nId=0; nId<slSchemesNameList.count(); nId++ ) {
		sScheme = slSchemesNameList[nId];

		colorsScheme.bShowSecondBackgroundColor = TRUE;// = settings.readBoolEntry( "/qtcmd/"+sScheme+"/ShowSecondBgColor", TRUE );  // temporary solution
		colorsScheme.bTwoColorCursor = TRUE;// = settings.readBoolEntry( "/qtcmd/"+sScheme+"/TwoColorsCursor", TRUE );   // TODO add QSettings support
		colorsScheme.bShowGrid = FALSE;// = settings.readBoolEntry( "/qtcmd/"+sScheme+"/ShowGrid", FALSE );    // temporary solution

		sColor = settings.value( "/qtcmd/"+sScheme+"/ItemColor", "#000000" ).toString();
		colorsScheme.itemColor = QColor(sColor);

		sColor = settings.value( "/qtcmd/"+sScheme+"/ItemColorUnderCursor", "#0000EA" ).toString();
		colorsScheme.itemColorUnderCursor = QColor(sColor);

		sColor = settings.value( "/qtcmd/"+sScheme+"/SelectedItemColor", "#FF0000" ).toString();
		colorsScheme.selectedItemColor = QColor(sColor);

		sColor = settings.value( "/qtcmd/"+sScheme+"/SelectedItemColorUnderCursor", "#57BA00" ).toString();
		colorsScheme.selectedItemColorUnderCursor = QColor(sColor);

		sColor = settings.value( "/qtcmd/"+sScheme+"/BgColorOfSelectedItems", "#FFFFFF" ).toString();
		colorsScheme.bgColorOfSelectedItems = QColor(sColor);

		sColor = settings.value( "/qtcmd/"+sScheme+"/BgColorOfSelectedItemsUnderCursor", "#FFFFFF" ).toString();
		colorsScheme.bgColorOfSelectedItemsUnderCursor = QColor(sColor);

		sColor = settings.value( "/qtcmd/"+sScheme+"/FirstBackgroundColor", "#FFFFFF" ).toString();
		colorsScheme.firstBackgroundColor = QColor(sColor);

		sColor = settings.value( "/qtcmd/"+sScheme+"/SecondBackgroundColor", "#FFEBDC" ).toString();
		colorsScheme.secondBackgroundColor = QColor(sColor);

		sColor = settings.value( "/qtcmd/"+sScheme+"/FullCursorColor", "#00008B" ).toString();
		colorsScheme.fullCursorColor = QColor(sColor);

		sColor = settings.value( "/qtcmd/"+sScheme+"/InsideFrameCursorColor", "#000080" ).toString();
		colorsScheme.insideFrameCursorColor = QColor(sColor);

		sColor = settings.value( "/qtcmd/"+sScheme+"/OutsideFrameCursorColor", "#FF0000" ).toString();
		colorsScheme.outsideFrameCursorColor = QColor(sColor);

		sColor = settings.value( "/qtcmd/"+sScheme+"/GridColor", "#A9A9A9" ).toString();
		colorsScheme.gridColor = QColor(sColor);

		m_cslSchemesList.append( colorsScheme );
	}

	m_pSchemeComboBox->addItems( slSchemesNameList );
	m_pSchemeComboBox->setCurrentIndex(m_pSchemeComboBox->findText(sScheme));
	slotSetNewScheme( m_pSchemeComboBox->currentIndex() );
}

void ColorsPg::drawPreview()
{
	bool bFullCursor  = m_pFullCursorRadioBtn->isChecked();
	bool bSecondBgCol = m_pSecBgColChkBox->isChecked();
	bool bShowGrid    = m_pShowGridChkBox->isChecked();

	int nXpos = 5, nYpos = 0;
	const int nFontHeight = 12;
	const int nTopMargin  = 10;
	const int nCursorHeight = nFontHeight + 3;
	const int nPreviewWidth = m_pPreviewLabel->width()-4, nPreviewHeight = m_pPreviewLabel->height()-2;
	const int nMaxLineNum   = (nPreviewHeight-nTopMargin) / nFontHeight; // == 7

	enum Colors {
		ItemColor=0, ItemColorUnderCursor,
		SelectedItemColor, SelectedItemColorUnderCursor,
		BgColorOfSelectedItems, BgColorOfSelectedItemsUnderCursor,
		FirstBackgroundColor, SecondBackgroundColor,
		FullCursorColor, InsideFrameCursorColor, OutsideFrameCursorColor,
		GridColor
	};


	QPixmap screen(nPreviewWidth, nPreviewHeight);
	screen.fill( getColor(FirstBackgroundColor) );

	QPainter painter( &screen );

	painter.fillRect( 0,0, nPreviewWidth, nPreviewHeight, painter.brush() );

	if ( bSecondBgCol ) { // draws two-colors background
		QColor color;
		for ( int i=0; i<nMaxLineNum; i++) {
			color = i%2 ? getColor(SecondBackgroundColor) : getColor(FirstBackgroundColor);
			painter.fillRect( 0, nTopMargin+(i*nCursorHeight), nPreviewWidth, nCursorHeight, color );
		}
	}
	if ( bShowGrid ) { // draws a grid
		painter.setPen( getColor(GridColor) );
		for ( int i=0; i<nMaxLineNum; i++)
			painter.drawLine( 0, nTopMargin+(i*nCursorHeight), nPreviewWidth, nTopMargin+(i*nCursorHeight) );
	}
	// draw the first line
	nYpos += nFontHeight + nTopMargin;
	painter.setPen( getColor(ItemColor) );
	painter.drawText( nXpos, nYpos, tr("Text") );

	// draws the second line
	if ( bFullCursor )
		painter.fillRect( 0, nYpos+2, nPreviewWidth, nCursorHeight+2, getColor(FullCursorColor) );
	else
	{	// draws two-colors frame
		painter.setPen( getColor(OutsideFrameCursorColor) );
		painter.drawRect( 0, nYpos+2, nPreviewWidth-3, nCursorHeight+2 );
		painter.setPen( getColor(InsideFrameCursorColor) );
		painter.drawRect( 1, nYpos+1+2, nPreviewWidth-5, nCursorHeight );
	}
	nYpos += nCursorHeight;
	painter.setPen( getColor(ItemColorUnderCursor) );
	painter.drawText( nXpos, nYpos, tr("TextUnderCursor") );

	// draws the third line
	nYpos+=1;
	painter.fillRect( 0, nYpos+4, nPreviewWidth, nCursorHeight, getColor(BgColorOfSelectedItems) );
	nYpos += nCursorHeight;
	painter.setPen( getColor(SelectedItemColor) );
	painter.drawText( nXpos, nYpos, tr("Selection") );

	// draws the fourth line
	painter.fillRect( 0, nYpos+2, nPreviewWidth, nCursorHeight+2, getColor(BgColorOfSelectedItemsUnderCursor) );
	if ( bFullCursor )
		painter.fillRect( 0, nYpos+2, nPreviewWidth, nCursorHeight+2, getColor(FullCursorColor) );
	else
	{	// draws two-colors frame
		painter.setPen( getColor(OutsideFrameCursorColor) );
		painter.drawRect( 0, nYpos+2, nPreviewWidth-3, nCursorHeight+2 );
		painter.setPen( getColor(InsideFrameCursorColor) );
		painter.drawRect( 1, nYpos+1+2, nPreviewWidth-4-1, nCursorHeight );
	}
	nYpos += nCursorHeight;
	painter.setPen( getColor(SelectedItemColorUnderCursor) );
	painter.drawText( nXpos, nYpos, tr("SelUnderCursor") );

	m_pPreviewLabel->setPixmap( screen );
}


QColor ColorsPg::getColor( int nColorId )
{
	QToolButton *pToolButton = (nColorId < 11) ? (QToolButton *)m_pColorsBtnGroup->button( nColorId ) : m_pGridColBtn;

	QPixmap btnPixmap = pToolButton->icon().pixmap(40); // 40x40 (The pixmap might be smaller than requested, but never larger)

	if ( btnPixmap.isNull() )
		return QColor(0, 0, 0);

	return btnPixmap.toImage().pixel(1, 1);
}

void ColorsPg::slotShowSecondBgColor( bool bShow )
{
	int nCurrentItem = m_pSchemeComboBox->currentIndex();

	if ( nCurrentItem > 0 )
		m_cslSchemesList[nCurrentItem].bShowSecondBackgroundColor = bShow;

	m_pSecondBgColBtn->setEnabled( bShow );
	drawPreview();
}

void ColorsPg::slotShowGrid( bool bShow )
{
	int nCurrentItem = m_pSchemeComboBox->currentIndex();

	if ( nCurrentItem > 0 )
		m_cslSchemesList[nCurrentItem].bShowGrid = bShow;

	m_pGridColBtn->setEnabled( bShow );
	drawPreview();
}

void ColorsPg::slotShowFullCursor( bool bShow )
{
	int nCurrentItem = m_pSchemeComboBox->currentIndex();

	if ( nCurrentItem > 0 )
		m_cslSchemesList[nCurrentItem].bTwoColorCursor = ! bShow;

	m_pFullCursorColBtn->setEnabled( bShow );
	m_pOutFrameCursColBtn->setEnabled( ! bShow );
	m_pInsideFrameCursColBtn->setEnabled( ! bShow );

	drawPreview();
}


void ColorsPg::slotNew()
{
	bool bOk = FALSE;
	QString sNewSchemeName = QInputDialog::getText( this,
		tr( "New scheme name" )+" - QtCommander",
		tr( "Please to give a name:" ),
		QLineEdit::Normal, tr("newScheme"), &bOk
	);
	if ( bOk == FALSE )
		return;

	sNewSchemeName.replace( ' ', '_' ); // ' ' zmienic na wyr.reg. - ciag spacji lub jedna


	ColorsScheme colorsScheme;
	// init by default scheme
	colorsScheme.bShowSecondBackgroundColor = m_cslSchemesList[0].bShowSecondBackgroundColor;
	colorsScheme.bTwoColorCursor = m_cslSchemesList[0].bTwoColorCursor;
	colorsScheme.bShowGrid = m_cslSchemesList[0].bShowGrid;
	colorsScheme.itemColor = m_cslSchemesList[0].itemColor;
	colorsScheme.itemColorUnderCursor = m_cslSchemesList[0].itemColorUnderCursor;
	colorsScheme.selectedItemColor = m_cslSchemesList[0].selectedItemColor;
	colorsScheme.selectedItemColorUnderCursor = m_cslSchemesList[0].selectedItemColorUnderCursor;
	colorsScheme.bgColorOfSelectedItems = m_cslSchemesList[0].bgColorOfSelectedItems;
	colorsScheme.bgColorOfSelectedItemsUnderCursor = m_cslSchemesList[0].bgColorOfSelectedItemsUnderCursor;
	colorsScheme.firstBackgroundColor = m_cslSchemesList[0].firstBackgroundColor;
	colorsScheme.secondBackgroundColor = m_cslSchemesList[0].secondBackgroundColor;
	colorsScheme.fullCursorColor = m_cslSchemesList[0].fullCursorColor;
	colorsScheme.insideFrameCursorColor = m_cslSchemesList[0].insideFrameCursorColor;
	colorsScheme.outsideFrameCursorColor = m_cslSchemesList[0].outsideFrameCursorColor;
	colorsScheme.gridColor = m_cslSchemesList[0].gridColor;

	m_cslSchemesList.append( colorsScheme );

	m_pSchemeComboBox->addItem( sNewSchemeName );
	m_pSchemeComboBox->setItemText( m_pSchemeComboBox->currentIndex(), sNewSchemeName );
	slotSetNewScheme( m_pSchemeComboBox->currentIndex() );
}


void ColorsPg::slotDelete()
{
	m_pSchemeComboBox->removeItem( m_pSchemeComboBox->currentIndex() );
	m_cslSchemesList.removeAt( m_pSchemeComboBox->currentIndex() );
}


void ColorsPg::slotSave()
{
	/*
	bool bTwoColorCursor, bShowSecondBackgroundColor, bShowGrid;
	QStringList slSchemesNameList;
	QSettings settings;
	QString sScheme;

	slSchemesNameList.append( "DefaultColors" );

	for ( uint nId=1; nId<m_cslSchemesList.count(); nId++ ) { // start from 1, becouse need to skip default sScheme
		bShowSecondBackgroundColor = m_cslSchemesList[nId].bShowSecondBackgroundColor;
		bTwoColorCursor = m_cslSchemesList[nId].bTwoColorCursor;
		bShowGrid = m_cslSchemesList[nId].bShowGrid;
		sScheme = m_pSchemeComboBox->text(nId);
		slSchemesNameList.append( sScheme );

		settings.writeEntry( "/qtcmd/"+sScheme+"/ShowSecondBgColor", bShowSecondBackgroundColor );
		settings.writeEntry( "/qtcmd/"+sScheme+"/TwoColorsCursor", bTwoColorCursor );
		settings.writeEntry( "/qtcmd/"+sScheme+"/ShowGrid", bShowGrid );

		settings.writeEntry( "/qtcmd/"+sScheme+"/ItemColor", colorToString(m_cslSchemesList[nId].itemColor) ); // w Qt4 jest met. name() dla QColor
		settings.writeEntry( "/qtcmd/"+sScheme+"/ItemColorUnderCursor", colorToString(m_cslSchemesList[nId].itemColorUnderCursor) );
		settings.writeEntry( "/qtcmd/"+sScheme+"/SelectedItemColor", colorToString(m_cslSchemesList[nId].selectedItemColor) );
		settings.writeEntry( "/qtcmd/"+sScheme+"/SelectedItemColorUnderCursor", colorToString(m_cslSchemesList[nId].selectedItemColorUnderCursor) );
		settings.writeEntry( "/qtcmd/"+sScheme+"/BgColorOfSelectedItems", colorToString(m_cslSchemesList[nId].bgColorOfSelectedItems) );
		settings.writeEntry( "/qtcmd/"+sScheme+"/BgColorOfSelectedItemsUnderCursor", colorToString(m_cslSchemesList[nId].bgColorOfSelectedItemsUnderCursor) );
		settings.writeEntry( "/qtcmd/"+sScheme+"/FirstBackgroundColor", colorToString(m_cslSchemesList[nId].firstBackgroundColor) );
		if ( bShowSecondBackgroundColor )
			settings.writeEntry( "/qtcmd/"+sScheme+"/SecondBackgroundColor", colorToString(m_cslSchemesList[nId].secondBackgroundColor) );
		if ( bTwoColorCursor ) {
			settings.writeEntry( "/qtcmd/"+sScheme+"/InsideFrameCursorColor", colorToString(m_cslSchemesList[nId].insideFrameCursorColor) );
			settings.writeEntry( "/qtcmd/"+sScheme+"/OutsideFrameCursorColor", colorToString(m_cslSchemesList[nId].outsideFrameCursorColor) );
		}
		else
			settings.writeEntry( "/qtcmd/"+sScheme+"/FullCursorColor", colorToString(m_cslSchemesList[nId].fullCursorColor) );
		if ( bShowGrid )
			settings.writeEntry( "/qtcmd/"+sScheme+"/GridColor", colorToString(m_cslSchemesList[nId].gridColor) );
	}

	settings.writeEntry( "/qtcmd/FilesPanel/ColorsSchemeList", slSchemesNameList );
	settings.writeEntry( "/qtcmd/FilesPanel/ColorsScheme", m_pSchemeComboBox->currentText() );
	*/
}


void ColorsPg::slotSetNewScheme( int nId )
{
	QPixmap btnPixmap( (const char**)buttonColor );
	m_pSchemeComboBox->setCurrentIndex( nId );

	// --- sets colors on the buttons
	btnPixmap.fill( m_cslSchemesList[nId].itemColor );
	m_pItemsColBtn->setIcon( btnPixmap );

	btnPixmap.fill( m_cslSchemesList[nId].itemColorUnderCursor );
	m_pItemsColUnderCursorBtn->setIcon( btnPixmap );

	btnPixmap.fill( m_cslSchemesList[nId].selectedItemColor );
	m_pSelectItemsBtn->setIcon( btnPixmap );

	btnPixmap.fill( m_cslSchemesList[nId].selectedItemColorUnderCursor );
	m_pSelectItemsColUnderCursorBtn->setIcon( btnPixmap );

	btnPixmap.fill( m_cslSchemesList[nId].bgColorOfSelectedItems );
	m_pBgColOfSelectedItemsBtn->setIcon( btnPixmap );

	btnPixmap.fill( m_cslSchemesList[nId].bgColorOfSelectedItemsUnderCursor );
	m_pBgColorOfSelectedItemsUnderCursorColBtn->setIcon( btnPixmap );

	btnPixmap.fill( m_cslSchemesList[nId].firstBackgroundColor );
	m_pFirstBgColBtn->setIcon( btnPixmap );

	btnPixmap.fill( m_cslSchemesList[nId].secondBackgroundColor );
	m_pSecondBgColBtn->setIcon( btnPixmap );

	btnPixmap.fill( m_cslSchemesList[nId].fullCursorColor );
	m_pFullCursorColBtn->setIcon( btnPixmap );

	btnPixmap.fill( m_cslSchemesList[nId].insideFrameCursorColor );
	m_pInsideFrameCursColBtn->setIcon( btnPixmap );

	btnPixmap.fill( m_cslSchemesList[nId].outsideFrameCursorColor );
	m_pOutFrameCursColBtn->setIcon( btnPixmap );

	btnPixmap.fill( m_cslSchemesList[nId].gridColor );
	m_pGridColBtn->setIcon( btnPixmap );

	// --- sets checkBoxes and radioButtons
	m_pSecBgColChkBox->setChecked( m_cslSchemesList[nId].bShowSecondBackgroundColor );
	m_pShowGridChkBox->setChecked( m_cslSchemesList[nId].bShowGrid );
	bool bTwoColorCursor = m_cslSchemesList[nId].bTwoColorCursor;
	if ( bTwoColorCursor )
		m_pTwoColorsCursorRadioBtn->setChecked( bTwoColorCursor );
	else
		m_pFullCursorRadioBtn->setChecked( ! bTwoColorCursor );

	m_pSecondBgColBtn->setEnabled( m_cslSchemesList[nId].bShowSecondBackgroundColor );
	m_pInsideFrameCursColBtn->setEnabled( m_cslSchemesList[nId].bTwoColorCursor );
	m_pOutFrameCursColBtn->setEnabled( m_cslSchemesList[nId].bTwoColorCursor );
	m_pFullCursorColBtn->setEnabled( ! m_cslSchemesList[nId].bTwoColorCursor );
	m_pGridColBtn->setEnabled( m_cslSchemesList[nId].bShowGrid );

	// disable delete and save buttons only for default scheme
	m_pDeleteSchemeBtn->setEnabled( nId );

	drawPreview();
}


void ColorsPg::slotSetColor( int nColorId )
{
	// --- gets color from the button
	QToolButton *pToolButton = (nColorId < 11) ? (QToolButton *)m_pColorsBtnGroup->button( nColorId ) : m_pGridColBtn;
	QPixmap btnPixmap = pToolButton->icon().pixmap(40); // 40x40 (The pixmap might be smaller than requested, but never larger)
	QColor initColor = btnPixmap.toImage().pixel(1,1);
	QColor newColor  = QColorDialog::getColor( initColor );

	if ( ! newColor.isValid() )
		return;
	// --- sets color on the button
	btnPixmap.fill( newColor );
	pToolButton->setIcon( btnPixmap );

	enum Colors {
		ItemColor=0, ItemColorUnderCursor,
		SelectedItemColor, SelectedItemColorUnderCursor,
		BgColorOfSelectedItems, BgColorOfSelectedItemsUnderCursor,
		FirstBackgroundColor, SecondBackgroundColor,
		FullCursorColor, InsideFrameCursorColor, OutsideFrameCursorColor,
		GridColor
	};
	int nSchemeId = m_pSchemeComboBox->currentIndex();
	if (nSchemeId < 0)
		nSchemeId = 0;

	if (nSchemeId > m_cslSchemesList.count()-1) {
		qDebug("ColorsPg::slotSetColor. Index out of range!");
		return;
	}

	if ( nSchemeId ) {
		// --- sets colors in scheme
		if ( nColorId == ItemColor )
			m_cslSchemesList[nSchemeId].itemColor = newColor;
		else
		if ( nColorId == ItemColorUnderCursor )
			m_cslSchemesList[nSchemeId].itemColorUnderCursor = newColor;
		else
		if ( nColorId == SelectedItemColor )
			m_cslSchemesList[nSchemeId].selectedItemColor = newColor;
		else
		if ( nColorId == SelectedItemColorUnderCursor )
			m_cslSchemesList[nSchemeId].selectedItemColorUnderCursor = newColor;
		else
		if ( nColorId == BgColorOfSelectedItems )
			m_cslSchemesList[nSchemeId].bgColorOfSelectedItems = newColor;
		else
		if ( nColorId == BgColorOfSelectedItemsUnderCursor )
			m_cslSchemesList[nSchemeId].bgColorOfSelectedItemsUnderCursor = newColor;
		else
		if ( nColorId == FirstBackgroundColor )
			m_cslSchemesList[nSchemeId].firstBackgroundColor = newColor;
		else
		if ( nColorId == SecondBackgroundColor )
			m_cslSchemesList[nSchemeId].secondBackgroundColor = newColor;
		else
		if ( nColorId == FullCursorColor )
			m_cslSchemesList[nSchemeId].fullCursorColor = newColor;
		else
		if ( nColorId == InsideFrameCursorColor )
			m_cslSchemesList[nSchemeId].insideFrameCursorColor = newColor;
		else
		if ( nColorId == OutsideFrameCursorColor )
			m_cslSchemesList[nSchemeId].outsideFrameCursorColor = newColor;
		else
		if ( nColorId == GridColor )
			m_cslSchemesList[nSchemeId].gridColor = newColor;
	}
	drawPreview();
}


void ColorsPg::slotSetGridColor()
{
	slotSetColor( 11 );
}

