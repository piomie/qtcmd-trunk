/***************************************************************************
                          ftppg  -  description
                             -------------------
    begin                : nie gru 23 2007
    copyright            : (c) 2007 by Piotr Mierzwiński, Mariusz Borowski
    email                : piom@nes.pl, mariusz@nes.pl

    copyright            : See COPYING file that comes with this project

 $Id$
 ***************************************************************************/
#ifndef FTPPG_H
#define FTPPG_H

#include "ui_ftppage.h"

/**
	@author Piotr Mierzwiński <piom@nes.pl>
*/
class FtpPg : public QWidget, public Ui::FtpPage
{
	Q_OBJECT

public:
	FtpPg();

	void init();
	int numOfTimeToRetryIfBusy();

private slots:
	void slotSetDefaults();

public slots:
	void slotSave();


};

#endif
