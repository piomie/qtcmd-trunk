/***************************************************************************
                          otherfspg  -  description
                             -------------------
    begin                : pi� gru 21 2007
    copyright            : (C) 2007 by Piotr Mierzwi�ski, Mariusz Borowski
    email                : piom@nes.pl, mariusz@nes.pl

    copyright            : See COPYING file that comes with this project

 $Id$
 ***************************************************************************/
//#include <QSettings> // TODO add support for QSettings

#include "otherfspg.h"

OtherFSPg::OtherFSPg()
{
	setupUi(this);

	connect( m_pDefaultBtn, SIGNAL(clicked()), this, SLOT(slotSetDefaults()) );

	init();
}


void OtherFSPg::init()
{
/*
	QSettings settings;

	bool alwaysOverwrite = settings.readBoolEntry( "/qtcmd/FilesSystem/AlwaysOverwrite", FALSE );
	bool savePermission = settings.readBoolEntry( "/qtcmd/FilesSystem/AlwaysSavePermission", TRUE );
	bool askBeforeDelete = settings.readBoolEntry( "/qtcmd/FilesSystem/AskBeforeDelete", TRUE );
	bool getInToDir = settings.readBoolEntry( "/qtcmd/FilesSystem/AlwaysGetInToDir", FALSE );
	bool closeProgressDlgAfterFinished = settings.readBoolEntry( "/qtcmd/FilesSystem/CloseProgressDlgAfterFinished", FALSE );

	m_pAlwaysOverwriteChkBox->setChecked( alwaysOverwrite );
	m_pAskBeforeDeleteChkBox->setChecked( askBeforeDelete );
	m_pAlwaysGetInToDirChkBox->setChecked( getInToDir );
	m_pAlwaysSavePermissionChkBox->setChecked( savePermission );
	m_pCloseProgressDlgAfterFinished->setChecked( closeProgressDlgAfterFinished );
*/
}


void OtherFSPg::slotSetDefaults()
{
	m_pAlwaysOverwriteChkBox->setChecked( FALSE );
	m_pAlwaysSavePermissionChkBox->setChecked( TRUE );
	m_pAskBeforeDeleteChkBox->setChecked( TRUE );
	m_pAlwaysGetInToDirChkBox->setChecked( FALSE );
	m_pCloseProgressDlgAfterFinished->setChecked( FALSE );
}


void OtherFSPg::slotSave()
{
	/*
	QSettings settings;

	settings.writeEntry( "/qtcmd/FilesSystem/AlwaysOverwrite", m_pAlwaysOverwriteChkBox->isChecked() );
	settings.writeEntry( "/qtcmd/FilesSystem/AlwaysSavePermission", m_pAlwaysSavePermissionChkBox->isChecked() );
	settings.writeEntry( "/qtcmd/FilesSystem/AskBeforeDelete", m_pAskBeforeDeleteChkBox->isChecked() );
	settings.writeEntry( "/qtcmd/FilesSystem/AlwaysGetInToDir", m_pAlwaysGetInToDirChkBox->isChecked() );
	settings.writeEntry( "/qtcmd/FilesSystem/CloseProgressDlgAfterFinished", m_pCloseProgressDlgAfterFinished->isChecked() );
	*/
}

