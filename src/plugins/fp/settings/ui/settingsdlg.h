/***************************************************************************
                          settingsdlg  -  description
                             -------------------
    begin                : pi� gru 21 2007
    copyright            : (c) 2007 by Piotr Mierzwi�ski, Mariusz Borowski
    email                : piom@nes.pl, mariusz@nes.pl

    copyright            : See COPYING file that comes with this project

 $Id$
 ***************************************************************************/
#ifndef SETTINGSDLG_H
#define SETTINGSDLG_H

#include "colorspg.h"
#include "columnspg.h"
#include "fontchooserpage.h"
#include "otherlistsettingspg.h"

#include "lfspg.h"
#include "ftppg.h"
#include "archivespg.h"
#include "otherfspg.h"

#include "keyshortcutspage.h"
#include "toolbarpage.h"

#include "keyshortcuts.h"
#include "filesassociation.h"
#include "listviewsettings.h"
#include "otherappsettings.h"
#include "filesystemsettings.h"

#include "ui_settingsdialog.h"

/**
	@author Piotr Mierzwi�ski, <piom@nes.pl>
*/
class SettingsDlg : public QDialog, public Ui::SettingsDialog
{
	Q_OBJECT

private:
	FontChooserPage *m_pFontAppPage;
	ColorsPg  *m_pColorsPage;
	ColumnsPg *m_pColumnsPage;
	OtherListSettingsPg *m_pOtherListSettingsPage;

	LfsPg *m_pLfsPage;
	FtpPg *m_pFtpPage;
	ArchivesPg *m_pArchivesPage;
	OtherFSPg  *m_pOtherFSPage;
	QWidget *m_pEmptyPage;

	KeyShortcutsPage *m_pKeyshortcutsPage;
	ToolBarPage      *m_pToolBarPage;

public:
	SettingsDlg();

	void init();
	void raiseWidget( const QString & sSenderName );

private slots:
	void slotOkButtonPressed();
	void slotApplyButtonPressed();
	void slotSaveButtonPressed();
	void slotItemSelectionChanged();
	void slotCurrentItemChanged( int nItemId );
	void slotSetKeyShortcuts( KeyShortcuts *pKeyShortcuts );
	void slotSetFilesAssociation( FilesAssociation * pFA );

signals:
	void signalApply( const ListViewSettings & );
	void signalApply( const FileSystemSettings & );
	void signalApply( const OtherAppSettings & );
	void signalApplyKeyShortcuts( KeyShortcuts * );
	void signalSaveSettings();

};

#endif
