
/***************************************************************************
                          archivespage  -  description
                             -------------------
    begin                : nie gru 23 2007
    copyright            : (C) 2007 by Piotr Mierzwiński, Mariusz Borowski
    email                : piom@nes.pl, mariusz@nes.pl

    copyright            : See COPYING file that comes with this project

 $Id$
 ***************************************************************************/
#include <QtGui>
#include <QFileDialog>
//#include <QSettings> // TODO add support for QSettings

#include "messagebox.h"
#include "functions_col.h" // for color() (convert hex string to QColor)
#include "comboboxdata.h"

#include "archivespg.h"


ArchivesPg::ArchivesPg()
	: m_pProcess(NULL)
{
	setupUi(this);

	connect( m_pDefaultBtn,            SIGNAL(clicked()), this, SLOT(slotSetDefaults()) );
	connect( m_pDetectArchivesBtn,     SIGNAL(clicked()), this, SLOT(slotDetectSupportedArchives()) );
	connect( m_pAdditionalSettingsBtn, SIGNAL(clicked()), this, SLOT(slotShowAdditionalSettings()) );
	connect( m_pSelectFullNameToolBtn, SIGNAL(clicked()), this, SLOT(slotChangePathForArchiver()) );
	connect( m_pArchiversListView,     SIGNAL(itemSelectionChanged()), this, SLOT(slotItemSelectionChanged()) ); // enable additional settings for arch.

	m_pProcess = new QProcess;
	connect( m_pProcess, SIGNAL(readyReadStandardOutput()), this, SLOT(slotReadDataFromStdOut()) );

	init();
}

ArchivesPg::~ArchivesPg()
{
}

void ArchivesPg::init()
{
	// prepare view
	// -- add columns TODO make 'add column' support for plugin ListWidgetView
	m_pArchiversListView->addColumn( ListWidgetView::CheckBox,  tr("available") );
	m_pArchiversListView->addColumn( ListWidgetView::TextLabel, tr("name")/*, Qt::AlignmentFlag eColumnAlign=Qt::AlignLeft*/ );
	m_pArchiversListView->addColumn( ListWidgetView::ComboBox,  tr("compression level") );
	m_pArchiversListView->addColumn( ListWidgetView::EditBox,   tr("full name") );
	/*
	QSettings settings;

	QString sWorkDir = settings.readEntry( "/qtcmd/Archives/WorkingDirectory", QDir::homePath()+"/tmp/qtcmd" );
	m_pLocationChooser->setText( sWorkDir );

	m_slArchiverFullNameList.append( settings.readEntry("/qtcmd/Archives/Bzip2FullName", "") );
	m_slArchiverFullNameList.append( settings.readEntry("/qtcmd/Archives/GzipFullName", "") );
	m_slArchiverFullNameList.append( settings.readEntry("/qtcmd/Archives/RarFullName", "") );

	m_slArchiverFullNameList.append( settings.readEntry("/qtcmd/Archives/TarFullName", "") );
	m_slArchiverFullNameList.append( settings.readEntry("/qtcmd/Archives/ZipFullName", "") );

	QString sScheme = settings.readEntry( "/qtcmd/FilesPanel/ColorsScheme", tr("DefaultColors") );
	QString sColor  = settings.readEntry( "/qtcmd/"+sScheme+"/SecondBackgroundColor", "FFEBDC" );
	*/
	// -- set look&feel
	//m_pArchiversListView->setSecondColorOfBg( color(sColor, "FFEBDC") );
	//m_pArchiversListView->setEnableTwoColorsOfBg( TRUE );
	//m_pArchiversListView->setCursorForColumn( NAMEcol ); // TODO add support for ListWidgetView to setCursorForColumn
	//m_pArchiversListView->setCursorForColumn( AVAILABLEcol, false ); // pItem->setFlags( Qt::ItemIsEnabled ); na wszystkich wierszach tej kolumny
	//m_pArchiversListView->setCursorForTxtColumnOnly(true); // ustaw kursor tylko na TextLabel, EditBox, KeyEditBox i ComboBox

	//ListViewWidgetsItem *pItem;

	QStringList slCompressionLevels;
	QStringList slArchivers;
	QString sArchName, sArchFullName;
	bool bArchFullNameList = (m_slArchiverFullNameList.count() > 0);
	int nIdOffset;
	slArchivers << "bzip2" << "gzip" << "rar" << "tar" << "tar.bz2" << "tar.gz" << "zip" << "arj";


	for ( int i=0; i<slArchivers.count(); i++ ) {
		sArchName = slArchivers[i];
		slCompressionLevels.clear();
		slCompressionLevels.append( tr("store") );

		nIdOffset = 0;
		if ( sArchName == "tar" && bArchFullNameList )
			sArchFullName = m_slArchiverFullNameList[TARpath];
		else
		if ( sArchName == "tar.bz2" || sArchName == "tar.gz" || sArchName == "bzip2" || sArchName == "gzip" ) {
			slCompressionLevels.clear(); // non store
			slCompressionLevels << "1 - "+tr("fast") << "2" << "3" << "4" << "5" << "6" << "7" << "8" << "9 - "+tr("best");
			if (bArchFullNameList) {
				if ( sArchName == "bzip2" )
					sArchFullName = m_slArchiverFullNameList[BZIP2path];
				else
				if ( sArchName == "gzip" )
					sArchFullName = m_slArchiverFullNameList[GZIPpath];
				else
					sArchFullName = m_slArchiverFullNameList[TARpath];
			}
		}
		else
		if ( sArchName == "rar" ) {
			slCompressionLevels.append( tr("the fastest") );
			slCompressionLevels.append( tr("fast") );
			slCompressionLevels.append( tr("default") );
			slCompressionLevels.append( tr("good") );
			slCompressionLevels.append( tr("the best") );
			if (bArchFullNameList)
				sArchFullName = m_slArchiverFullNameList[RARpath];
		}
		else
		if ( sArchName == "zip" ) {
			slCompressionLevels << "1 - "+tr("fast") << "2" << "3" << "4" << "5" << "6 - "+tr("default") << "7" << "8" << "9 - "+tr("best");
			if (bArchFullNameList)
				sArchFullName = m_slArchiverFullNameList[ZIPpath];
			nIdOffset = 1;
		}

		m_pArchiversListView->addRow();
		m_pArchiversListView->setCellValue( i, NAMEcol, slArchivers[i] );
		m_pArchiversListView->setCellValue( i, COMPRESSIONcol, slCompressionLevels, (slCompressionLevels.count()/2+nIdOffset) );
		m_pArchiversListView->setCellAttributes( i, NAMEcol, Qt::black, ListWidgetView::Bold );
		if ( sArchFullName.isEmpty() )
			setRowEnable(i, FALSE);
		else
			m_pArchiversListView->setCellAttributes( i, FULLNAMEcol, sArchFullName );
		/*
		pItem = new ListViewWidgetsItem( m_pArchiversListView, CheckBox, TextLabel, ComboBox, EditBox );
		pItem->setTextValue( NAMEcol, slArchivers[i] );
		pItem->insertStringList( COMPRESSIONcol, slCompressionLevels );
		c = (sArchName == "zip") ? 1 : 0;
		pItem->setText( COMPRESSIONcol, slCompressionLevels[slCompressionLevels.nCount()/2+c] ); // default is first
		pItem->setTextAttributs( NAMEcol, Qt::black, TRUE );
		if ( sArchFullName.isEmpty() ) {
			pItem->setEnableCell( NAMEcol, FALSE ); // disable all archivers
			pItem->setEnableCell( COMPRESSIONcol, FALSE ); // disable all archivers
		}
		else
			pItem->setText( FULLNAMEcol, sArchFullName );
		*/
	}
/*
	// --- init RAR addons settings
	m_ntRarAddsSettings[CreateSolidArchive] = settings.readBoolEntry( "/qtcmd/Archives/RarSolidArchive", TRUE );
	m_ntRarAddsSettings[RarSfxArchive] = settings.readBoolEntry( "/qtcmd/Archives/RarSfxArchive", FALSE );
	m_ntRarAddsSettings[RarSaveSymbolicLinks] = settings.readBoolEntry( "/qtcmd/Archives/RarSaveLinks", TRUE );
	m_ntRarAddsSettings[SaveFileOwnerAndGroup] = settings.readBoolEntry( "/qtcmd/Archives/RarSaveOwnerAndGroup", TRUE );
	m_ntRarAddsSettings[MultimediaCompression] = settings.readBoolEntry( "/qtcmd/Archives/RarMultimediaCompression", FALSE );
	m_ntRarAddsSettings[OverwriteExistingFiles] = settings.readBoolEntry( "/qtcmd/Archives/RarOvrExistingFiles", FALSE );
	m_ntRarAddsSettings[DictionarySize] = settings.readNumEntry( "/qtcmd/Archives/RarDictionarySize", 512 );
	// --- init ZIP addons settings
	m_ntZipAddsSettings[ZipSfxArchive] = settings.readBoolEntry( "/qtcmd/Archives/ZipSfxArchive", FALSE );
	m_ntZipAddsSettings[ZipSaveSymbolicLinks] = settings.readBoolEntry( "/qtcmd/Archives/ZipSaveSymbolicLinks", TRUE );
	m_ntZipAddsSettings[ZipDoNotSaveExtraAttrib] = settings.readBoolEntry( "/qtcmd/Archives/ZipDoNotSaveExtraAttrib", FALSE );
	*/

	connect( m_pArchiversListView, SIGNAL(editBoxTextApplied(QTableWidgetItem *, const QString &)),
			 this, SLOT(slotEditBoxTextApplied(QTableWidgetItem *, const QString &)) );
	connect( m_pArchiversListView, SIGNAL(buttonClicked(QTableWidgetItem *)),
			 this, SLOT(slotButtonClicked(QTableWidgetItem *)) );

	m_pArchiversListView->selectRow(0);
	m_pArchiversListView->resizeColumnsToContents();
}


void ArchivesPg::setRowEnable( int nRow, bool bEnable )
{
	m_pArchiversListView->setEnableCell( nRow, NAMEcol,        bEnable, FALSE ); // switch to not editable
	m_pArchiversListView->setEnableCell( nRow, COMPRESSIONcol, bEnable );
	m_pArchiversListView->setEnableCell( nRow, FULLNAMEcol,    bEnable );
}


void ArchivesPg::slotDetectSupportedArchives()
{
	QString sArchiverName;
	QString sCmd = "whereis -b"; // print path list, like: "rar: /usr/bin/rar"

	for (int i=0; i<m_pArchiversListView->rowCount(); i++ ) {
		sArchiverName = m_pArchiversListView->item( i, NAMEcol )->text();
		if ( sArchiverName.indexOf("tar.") != -1 ) {
			continue;
		}
		sCmd += " "+sArchiverName;
	}
	qDebug() << "sCmd=" << sCmd;
	m_pProcess->start(sCmd);
}


void ArchivesPg::slotSetDefaults()
{
	m_pLocationChooser->setText( QDir::homePath()+"/tmp/qtcmd" );

	int nIdOffset;
	QStringList slCompressionLvl;

	QTableWidgetItem *pItem;
	for (int i=0; i<m_pArchiversListView->rowCount(); i++ ) {
		pItem = m_pArchiversListView->item( i, NAMEcol );
		nIdOffset = (pItem->text() == "zip") ? 1 : 0;
		pItem = m_pArchiversListView->item( i, COMPRESSIONcol );
		slCompressionLvl = pItem->data(Qt::DisplayRole).value<ComboBoxData>().items();
		m_pArchiversListView->setCellValue( i, COMPRESSIONcol, slCompressionLvl, (slCompressionLvl.count()/2+nIdOffset) );
	}

	// --- set default RAR addons settings
	m_ntRarAddsSettings[CreateSolidArchive]     = TRUE;
	m_ntRarAddsSettings[RarSfxArchive]          = FALSE;
	m_ntRarAddsSettings[RarSaveSymbolicLinks]   = TRUE;
	m_ntRarAddsSettings[SaveFileOwnerAndGroup]  = TRUE;
	m_ntRarAddsSettings[MultimediaCompression]  = FALSE;
	m_ntRarAddsSettings[OverwriteExistingFiles] = FALSE;
	m_ntRarAddsSettings[DictionarySize] = 512;

	// --- set default ZIP addons settings
	m_ntZipAddsSettings[ZipSfxArchive]           = FALSE;
	m_ntZipAddsSettings[ZipSaveSymbolicLinks]    = TRUE;
	m_ntZipAddsSettings[ZipDoNotSaveExtraAttrib] = FALSE;
}


void ArchivesPg::slotSave()
{
	/*
	QSettings settings;

	settings.writeEntry( "/qtcmd/Archives/WorkingDirectory", m_pLocationChooser->text() );

	// --- write full name for all archivers
	settings.writeEntry( "/qtcmd/Archives/Bzip2FullName", m_slArchiverFullNameList[BZIP2path] );
	settings.writeEntry( "/qtcmd/Archives/GzipFullName", m_slArchiverFullNameList[GZIPpath] );
	settings.writeEntry( "/qtcmd/Archives/RarFullName", m_slArchiverFullNameList[RARpath] );
	settings.writeEntry( "/qtcmd/Archives/TarFullName", m_slArchiverFullNameList[TARpath] );
	settings.writeEntry( "/qtcmd/Archives/ZipFullName", m_slArchiverFullNameList[ZIPpath] );

	// --- write RAR addons settings
	settings.writeEntry( "/qtcmd/Archives/RarSolidArchive", m_ntRarAddsSettings[CreateSolidArchive] );
	settings.writeEntry( "/qtcmd/Archives/RarSfxArchive", m_ntRarAddsSettings[RarSfxArchive] );
	settings.writeEntry( "/qtcmd/Archives/RarSaveLinks", m_ntRarAddsSettings[RarSaveSymbolicLinks] );
	settings.writeEntry( "/qtcmd/Archives/RarSaveOwnerAndGroup", m_ntRarAddsSettings[SaveFileOwnerAndGroup] );
	settings.writeEntry( "/qtcmd/Archives/RarMultimediaCompression", m_ntRarAddsSettings[MultimediaCompression] );
	settings.writeEntry( "/qtcmd/Archives/RarOvrExistingFiles", m_ntRarAddsSettings[OverwriteExistingFiles] );
	settings.writeEntry( "/qtcmd/Archives/RarDictionarySize", m_ntRarAddsSettings[DictionarySize] );
	// --- write ZIP addons settings
	settings.writeEntry( "/qtcmd/Archives/ZipSfxArchive", m_ntZipAddsSettings[ZipSfxArchive] );
	settings.writeEntry( "/qtcmd/Archives/ZipSaveSymbolicLinks", m_ntZipAddsSettings[ZipSaveSymbolicLinks] );
	settings.writeEntry( "/qtcmd/Archives/ZipDoNotSaveExtraAttrib", m_ntZipAddsSettings[ZipDoNotSaveExtraAttrib] );
	*/
}


void ArchivesPg::slotShowAdditionalSettings()
{
	if ( ! m_pArchiversListView->cellEnable(m_pArchiversListView->currentRow(), NAMEcol) )
		return;

	QTableWidgetItem *pItem = m_pArchiversListView->item( m_pArchiversListView->currentRow(), NAMEcol );
	QString sCurrentArch = pItem->text();

	if ( sCurrentArch != "rar" && sCurrentArch != "zip" )
		return;

	QStringList slAddonsSettings;
	if ( sCurrentArch == "rar" ) { // do not change order of this list
		slAddonsSettings.append( tr("Create solid archive") );
		slAddonsSettings.append( tr("Create SFX archive") );
		slAddonsSettings.append( tr("Save symbolic links") );
		slAddonsSettings.append( tr("Save file owner and group") );
		slAddonsSettings.append( tr("Multimedia compression") );
		slAddonsSettings.append( tr("Overwrite existing files") );
	}
	else
	if ( sCurrentArch == "zip" ) { // do not change order of this list
		slAddonsSettings.append( tr("Create SFX archive") );
		slAddonsSettings.append( tr("Save symbolic links") );
		slAddonsSettings.append( tr("Do not save file owner and group and file times") );
	}

	// --- create a dialog and the objects to inserting
	QDialog *pDlg = new QDialog;
	QPushButton *pOkBtn      = new QPushButton( tr("&OK"), pDlg );
	QPushButton *pCancelBtn  = new QPushButton( tr("&Cancel"), pDlg );
	QGridLayout *pGridLayout = new QGridLayout( pDlg );
	//pGridLayout->addWidget(

	//void addWidget ( QWidget * widget, int row, int column, Qt::Alignment alignment = 0 )
	//QGridLayout *pGridLayout = new QGridLayout( pDlg, 1, 1, 11, 6 );
	QSpacerItem *pSpacer1 = new QSpacerItem( 16, 21, QSizePolicy::Minimum,   QSizePolicy::Expanding );
	QSpacerItem *pSpacer2 = new QSpacerItem( 31, 16, QSizePolicy::Expanding, QSizePolicy::Minimum );

	QLabel *pLabel = new QLabel( pDlg );
	QComboBox *pDictionarySizeCombo = new QComboBox( pDlg );
	if ( sCurrentArch == "rar" ) {
		pDictionarySizeCombo->addItems( QString("64,128,256,512,1024,2048,4096").split(',') );
		pLabel->setText( tr("Dictionary size")+" :" );
	}
	else
		pDictionarySizeCombo->hide();

	// signals and slots connections
	connect( pOkBtn, SIGNAL(clicked()), pDlg, SLOT(accept()) );
	connect( pCancelBtn, SIGNAL(clicked()), pDlg, SLOT(reject()) );

	// --- insert the objects into the dialog
	int i;
	QCheckBox *chkBox;
	QList <QCheckBox *> checkBoxList;

	for ( i=0; i<slAddonsSettings.count(); i++ ) {
		chkBox = new QCheckBox(slAddonsSettings[i], pDlg);
		pGridLayout->addWidget( chkBox, i, 0 );
		//pGridLayout->addMultiCellWidget( chkBox, i, i, 0, 3 );
		checkBoxList << chkBox;
		if ( sCurrentArch == "rar" )
			chkBox->setChecked( m_ntRarAddsSettings[i] );
		else
		if ( sCurrentArch == "zip" )
			chkBox->setChecked( m_ntZipAddsSettings[i] );
	}

	if ( sCurrentArch == "rar" ) {
		pGridLayout->addWidget(pLabel, i, 0);
		pGridLayout->addWidget( pDictionarySizeCombo, i, 1 );
		pDictionarySizeCombo->setItemText( pDictionarySizeCombo->currentIndex(), QString::number(m_ntRarAddsSettings[DictionarySize]) );
		//pGridLayout->addMultiCellWidget( pLabel, i, i, 0, 1 );
		//pGridLayout->addMultiCellWidget( pDictionarySizeCombo, i, i, 2, 3 );
		i++;
	}
	pGridLayout->addItem( pSpacer1, i, 1 );
	i++;
	pGridLayout->addItem( pSpacer2, i, 0 );
	pGridLayout->addWidget( pOkBtn, i, 1 );
	pGridLayout->addWidget( pCancelBtn, i, 3 );

	pDlg->adjustSize();
	pDlg->setWindowTitle( sCurrentArch+" "+tr("settings")+" - QtCommander" );

	// --- show dialog
	int nResult = pDlg->exec();

	// --- get settings from dialog
	if ( nResult > 0 ) { // 1 == OK
		uint nCount = 0;
		for ( int i=0; i<checkBoxList.count(); i++ ) {
			if ( sCurrentArch == "rar" )
				m_ntRarAddsSettings[nCount] = checkBoxList.at(i)->isChecked();
			else
			if ( sCurrentArch == "zip" )
				m_ntZipAddsSettings[nCount] = checkBoxList.at(i)->isChecked();
			nCount++;
		}
		if ( sCurrentArch == "rar" )
			m_ntRarAddsSettings[DictionarySize] = pDictionarySizeCombo->currentText().toInt(); // KB (*1024);
	}

	delete pDlg;
}


void ArchivesPg::slotChangePathForArchiver()
{
	if ( ! m_pArchiversListView->currentItem() )
		return;

	int nCurrentRow = m_pArchiversListView->currentRow();
	if (! m_pArchiversListView->cellEnable(nCurrentRow, NAMEcol))
		return;

	QString sFileName = QFileDialog::getOpenFileName( this,
			tr("Select archiver file")+" "+m_pArchiversListView->item(nCurrentRow, NAMEcol)->text()+" - QtCommander"
			, "/", tr("All files")+"(*)" );
	if (sFileName.isEmpty())
		return;
	if ( QFileInfo(sFileName).isExecutable() ) {
		m_pArchiversListView->setCellValue( nCurrentRow, FULLNAMEcol, sFileName );
		m_pArchiversListView->setCellValue( nCurrentRow, AVAILABLEcol, "TRUE" );
		setRowEnable(nCurrentRow, TRUE);

		m_pArchiversListView->resizeColumnsToContents(); // fit column to new 'full name' size
		m_pArchiversListView->setFocus(); // need to refresh view
		m_pArchiversListView->selectRow(nCurrentRow);
	}
	else
		MessageBox::critical(this, tr("Wrong archiver name.")+"\n\n"+tr("File is'nt executable!"));
}


void ArchivesPg::slotReadDataFromStdOut()
{
	QString sBuffer = m_pProcess->readAllStandardOutput();
	if ( sBuffer.isEmpty() )
		return;
	QString sArchiverName, sFullName, s;
	QStringList slOutputLines = sBuffer.split('\n');
	QStringList slLine;
	int nRow;

	for ( int i=0; i<slOutputLines.count()-1; i++ ) { // -1 for skip empty line
		slLine = slOutputLines[i].split(':');
		sArchiverName = slLine[0];
		sFullName = slLine[1].simplified();
		slLine = sFullName.split(' ');

		for ( int j=0; j<slLine.count(); j++ ) {
			if ( QFileInfo(slLine[j]).isDir() )
				continue;
			if ( QFileInfo(slLine[j]).isExecutable() ) {
				sFullName = slLine[j];
				break;
			}
		}
		nRow = m_pArchiversListView->findItem( sArchiverName, NAMEcol );
		//qDebug() << "sArchiverName" << sArchiverName << ", sFullName= "<< sFullName << ", nRow=" << nRow;
		if ( nRow >= 0 ) {
			m_pArchiversListView->setCellValue( nRow, FULLNAMEcol, sFullName );
			if ( ! sFullName.isEmpty() ) {
				m_pArchiversListView->setCellValue(nRow, AVAILABLEcol, "TRUE" );
				setRowEnable(nRow, TRUE);
			}
			if ( sArchiverName == "tar" ) { // update tar.bz2 and tar.gz
				nRow = m_pArchiversListView->findItem( "tar.bz2", NAMEcol );
				if ( nRow >= 0 ) {
					m_pArchiversListView->setCellValue( nRow, FULLNAMEcol, sFullName );
					if ( ! sFullName.isEmpty() ) {
						m_pArchiversListView->setCellValue(nRow, AVAILABLEcol, "TRUE" );
						setRowEnable(nRow, TRUE);
					}
				}
				nRow = m_pArchiversListView->findItem( "tar.gz", NAMEcol );
				if ( nRow >= 0 ) {
					m_pArchiversListView->setCellValue( nRow, FULLNAMEcol, sFullName );
					if ( ! sFullName.isEmpty() ) {
						m_pArchiversListView->setCellValue(nRow, AVAILABLEcol, "TRUE" );
						setRowEnable(nRow, TRUE);
					}
				}
			}
		}
	}
	QTableWidgetItem *pItem = m_pArchiversListView->item( m_pArchiversListView->currentRow(), NAMEcol );
	if ( pItem != NULL )
		m_pAdditionalSettingsBtn->setEnabled( (pItem->text() == "rar" || pItem->text() == "zip") &&
				m_pArchiversListView->cellEnable(m_pArchiversListView->currentRow(), NAMEcol) );

	m_pArchiversListView->resizeColumnsToContents();
	m_pArchiversListView->setFocus(); // need to refresh view
}


// enable button for additional settings
void ArchivesPg::slotItemSelectionChanged()
{
	int nCurrentRow = m_pArchiversListView->currentRow();

	QString sArchName = m_pArchiversListView->cellValue(nCurrentRow, NAMEcol);
	m_pArchiverNameLab->setText( sArchName );
	m_pAdditionalSettingsBtn->setEnabled( (sArchName == "rar" || sArchName == "zip")
			&& m_pArchiversListView->cellEnable(nCurrentRow, NAMEcol) );
}


void ArchivesPg::slotEditBoxTextApplied( QTableWidgetItem *pItem, const QString & sNewText )
{
	int nRow    = pItem->row();
	int nColumn = pItem->column();
	qDebug() << "ArchivesPg::slotEditBoxTextApplied, (row=" << nRow <<", col=" << nColumn << "), newText=" << sNewText;

	QString sFileName = sNewText;
	if (sFileName.isEmpty()) {
		sFileName = QFileDialog::getOpenFileName( this,
			tr("Select archiver file")+" "+m_pArchiversListView->item(nRow, NAMEcol)->text()+" - QtCommander"
			, "/", tr("All files")+"(*)"
		);
	}
	bool bFail = FALSE;
	if (sFileName.isEmpty()) {
		return;
	}
	QFileInfo file(sFileName);
	if (! file.exists() && ! bFail) {
		bFail = TRUE;
		MessageBox::critical(this, tr("Wrong archiver name.")+"\n\n"+tr("File not exists!"));
	}
	else {
		if (file.isDir()) {
			bFail = TRUE;
			MessageBox::critical(this, tr("Wrong archiver name.")+"\n\n"+tr("This is directory!"));
		}
		else
		if (! file.isExecutable()) {
			bFail = TRUE;
			MessageBox::critical(this, tr("Wrong archiver name.")+"\n\n"+tr("File is'nt executable!"));
		}
	}
	if (bFail) {
		m_pArchiversListView->setCellValue(nRow, AVAILABLEcol, "FALSE");
		setRowEnable(nRow, FALSE);
	}
	else {
		m_pArchiversListView->setCellValue(nRow, FULLNAMEcol, sFileName);
		m_pArchiversListView->resizeColumnsToContents(); // fit column to new 'full name' size
		m_pArchiversListView->setFocus(); // need to refresh view
		m_pArchiversListView->selectRow(nRow);
	}
	slotItemSelectionChanged(); // need to be after row enable/disable
}



void ArchivesPg::slotButtonClicked( QTableWidgetItem *pItem )
{
	int nRow    = pItem->row();
	int nColumn = pItem->column();

	if ( nColumn != AVAILABLEcol )
		return;

	bool bEnableArch = (m_pArchiversListView->cellValue(nRow, AVAILABLEcol) == "TRUE");
	if ( bEnableArch ) {
		QString sFileName = m_pArchiversListView->cellValue(nRow, FULLNAMEcol);
		if ( sFileName.isEmpty() ) {
			sFileName = QFileDialog::getOpenFileName( this,
				tr("Select archiver file")+" "+m_pArchiversListView->item(nRow, NAMEcol)->text()+" - QtCommander"
				, "/", tr("All files")+"(*)"
			);
		}
		if ( sFileName.isEmpty() ) {
			m_pArchiversListView->setCellValue(nRow, AVAILABLEcol, "FALSE");
			return;
		}
		else {
			m_pArchiversListView->setCellValue(nRow, FULLNAMEcol, sFileName);
			m_pArchiversListView->resizeColumnsToContents(); // fit column to new 'full name' size
			m_pArchiversListView->setFocus(); // need to refresh view
			m_pArchiversListView->selectRow(nRow);
		}
		if (QFileInfo(sFileName).isDir()) {
			m_pArchiversListView->setCellValue(nRow, AVAILABLEcol, "FALSE");
			slotItemSelectionChanged(); // need to be after row enable/disable
			MessageBox::critical(this, tr("Wrong archiver name.")+"\n\n"+tr("This is directory!"));
			return;
		}
		else
		if (! QFileInfo(sFileName).isExecutable()) {
			m_pArchiversListView->setCellValue(nRow, AVAILABLEcol, "FALSE");
			slotItemSelectionChanged(); // need to be after row enable/disable
			MessageBox::critical(this, tr("Wrong archiver name.")+"\n\n"+tr("File is'nt executable!"));
			return;
		}
	}
	setRowEnable(nRow, bEnableArch);
	slotItemSelectionChanged(); // need to be after row enable/disable
}


QString ArchivesPg::workDirPath()
{
	return m_pLocationChooser->text();
}


QString ArchivesPg::compressionLevel( const QString & sArchiverName )
{
	int nRow = m_pArchiversListView->findItem( sArchiverName, NAMEcol );

	if ( nRow >= 0 && m_pArchiversListView->cellEnable(nRow, COMPRESSIONcol) )
		return m_pArchiversListView->item(nRow, COMPRESSIONcol)->text();

	return QString::null;
}


QString ArchivesPg::archiverFullName( const QString & sArchiverName )
{
	int nRow = m_pArchiversListView->findItem( sArchiverName, NAMEcol );

	if ( nRow >= 0 && m_pArchiversListView->cellEnable(nRow, NAMEcol) )
		return m_pArchiversListView->item(nRow, FULLNAMEcol)->text();

	return QString::null;
}

