/***************************************************************************
                          otherlistsettingspg  -  description
                             -------------------
    begin                : pi� gru 21 2007
    copyright            : (c) 2007 by Piotr Mierzwi�ski, Mariusz Borowski
    email                : piom@nes.pl, mariusz@nes.pl

    copyright            : See COPYING file that comes with this project

 $Id$
 ***************************************************************************/
#ifndef OTHERLISTSETTINGSPG_H
#define OTHERLISTSETTINGSPG_H

#include <QButtonGroup>

#include "filters.h"
#include "filesassociation.h"

#include "ui_otherlistsettingspage.h"

/**
	@author Piotr Mierzwi�ski <piom@nes.pl>
*/
class OtherListSettingsPg : public QWidget, public Ui::OtherListSettingsPage
{
	Q_OBJECT

private:
	Filters *m_pFilters;

	QButtonGroup *m_pFileSizeFormatBtnGrpFL;
	QButtonGroup *m_pFileSizeFormatBtnGrpSB;

public:
	OtherListSettingsPg();
	~OtherListSettingsPg();

	int defaultFilter();
	int fileSizeFormatFL();
	int fileSizeFormatSB();

	void init();

	/** Uaktualniana jest tu lista filtr�w.
	 * @param pFA - wska�nik na skojarzenia plik�w.
	 * Metoda wywo�ywana podczas uruchamiania dialogu ustawie�.
	 */
	void updateFilters( FilesAssociation * pFA );

private slots:
	/** Ustawiane s� tutaj domy�lne warto�ci dla dialogu.
	 */
	void slotSetDefaults();
	void slotFilterHighlighted( int nFilterId );

public slots:
	void slotSave();


};

#endif
