/***************************************************************************
                          columnspg  -  description
                             -------------------
    begin                : nie gru 23 2007
    copyright            : (C) 2007 by Piotr Mierzwiński, Mariusz Borowski
    email                : piom@nes.pl, mariusz@nes.pl

    copyright            : See COPYING file that comes with this project

 $Id$
 ***************************************************************************/
#include <QtGui>
//#include <QSettings> // TODO add support for QSettings

#include "columnspg.h"


ColumnsPg::ColumnsPg()
{
	setupUi(this);

	enum ColumnNames {  // TODO use ColumnNames (id) from some namespace
		NAMEcol=0, SIZEcol, TIMEcol, PERMcol, OWNERcol, GROUPcol
		//NAMEcol=0, EXTNAMEcol, TIMEcol, PERMcol, OWNERcol, GROUPcol
	};
	m_pColumnsButtonGroup = new QButtonGroup;
	m_pColumnsButtonGroup->setExclusive(FALSE);
	m_pColumnsButtonGroup->addButton(m_pNameChkBox,  NAMEcol);
	//m_pColumnsButtonGroup->addButton(m_pNameExtChkBox, EXTNAMEcol); // not yet supported TODO add support for extention column
	m_pColumnsButtonGroup->addButton(m_pSizeChkBox,  SIZEcol);
	m_pColumnsButtonGroup->addButton(m_pTimeChkBox,  TIMEcol);
	m_pColumnsButtonGroup->addButton(m_pPermChkBox,  PERMcol);
	m_pColumnsButtonGroup->addButton(m_pOwnerChkBox, OWNERcol);
	m_pColumnsButtonGroup->addButton(m_pGroupChkBox, GROUPcol);

	connect( m_pDefaultBtn, SIGNAL(clicked()), this, SLOT(slotDefaultSettings()) );

	init();
}

ColumnsPg::~ColumnsPg()
{
	delete m_pColumnsButtonGroup;
}


// m_pColorsBtnGroup
void ColumnsPg::init()
{
	QSettings settings;
	//const QStringList slDefaultColumns = QString("0-170,1-0,2-70,3-107,4-70,5-55,6-55")::split( ',' ); // with ext.column TODO support for extention column
	const QStringList slDefaultColumns = QString("0-170,1-70,2-107,3-70,4-55,5-55").split( ',' );

	// --- read info from config file
	bool bColumnsSynchronization = TRUE; // = settings.readBoolEntry( "/qtcmd/FilesPanel/SynchronizeColumns", TRUE ); // temporary solution
	QStringList slColumns; // = settings.readListEntry( "/qtcmd/FilesPanel/Columns" );
	if ( slColumns.isEmpty() )
		slColumns = slDefaultColumns;

	bool bShowExtention = TRUE;
	int nColumnWidth;
	QCheckBox *pColumnVisible;
	for (int i=0; i<slDefaultColumns.count(); i++) { // slDefaultColumns.count() - num of all columns
		//pColumnVisible = (QCheckBox *)m_pShowColumnsGrpBox->find(i);
		pColumnVisible = (QCheckBox *)m_pColumnsButtonGroup->button(i);
		if ( slColumns[i].section('-', 0, 0 ) == QString("%1").arg(i) ) { // current column is visible
			nColumnWidth = (slColumns[i].section('-', -1 )).toInt();
			if ( nColumnWidth > 0 && nColumnWidth < 10 ) {
				nColumnWidth = (slDefaultColumns[i].section('-', -1 )).toInt();
			}
		}
		else
			nColumnWidth = 0;

		if (i == 1 && nColumnWidth < 1)
			bShowExtention = FALSE;

		m_ntColumnsWidth[i] = nColumnWidth;
		pColumnVisible->setChecked( (nColumnWidth > 0) );
	}

	m_pSynchronizeColumnChkBox->setChecked( bColumnsSynchronization );
	m_pExtentionGrpBox->setEnabled( bShowExtention );
}

int ColumnsPg::columnWidth( int nId )
{
	//qDebug() << "ColumnsPg::columnWidth, id= " << nId;
	if ( nId < 0 || nId > 7 )
		return 0;

	//qDebug() << m_pColumnsButtonGroup->button(nId)
	if (m_pColumnsButtonGroup->button(nId) == NULL)
		return 0;
	qDebug() << "ColumnsPg::columnWidth, get width, m_ntColumnsWidth[" << nId << "]= " << m_ntColumnsWidth[nId];
	int nWidth = m_pColumnsButtonGroup->button(nId)->isChecked() ? m_ntColumnsWidth[nId] : 0;
/*
	qDebug() << "m_pShowColumnsGrpBox->find(nId)= " << m_pShowColumnsGrpBox->indexOf(nId);
	if (m_pShowColumnsGrpBox->indexOf(nId) == NULL)
	//if (m_pShowColumnsGrpBox->indexfind(nId) == NULL)
		return 0;
	int nWidth = (((QCheckBox *)m_pShowColumnsGrpBox->indexOf(nId))->isChecked()) ? m_ntColumnsWidth[nId] : 0;
	qDebug() << "ColumnsPg::columnWidth, end";
*/
	return nWidth;
}

void ColumnsPg::slotDefaultSettings()
{
// "0-170,1-0,2-70,3-107,4-70,5-55,6-55"
	m_pNameChkBox->setChecked( TRUE );
	m_pNameExtChkBox->setChecked( FALSE );
	m_pSizeChkBox->setChecked( TRUE );
	m_pTimeChkBox->setChecked( TRUE );
	m_pPermChkBox->setChecked( TRUE );
	m_pOwnerChkBox->setChecked( TRUE );
	m_pGroupChkBox->setChecked( TRUE );

	m_pSynchronizeColumnChkBox->setChecked( TRUE );
//	m_pSortColBtnGroup->button( 0 )->setChecked( TRUE );
}

void ColumnsPg::slotSave()
{
	/*
	//const QStringList slDefaultColumns = QString("0-170,1-0,2-70,3-107,4-70,5-55,6-55")::split( ',' ); // with ext.column TODO support for extention column
	const QStringList slDefaultColumns = QString("0-170,1-70,2-107,3-70,4-55,5-55").split( ',' );

	QSettings settings;
	settings.writeEntry( "/qtcmd/FilesPanel/SynchronizeColumns", m_pSynchronizeColumnChkBox->isChecked() );

	int nColumnWidth;
	QCheckBox *pColumnVisible;
	QStringList slColumns;
	for (int i=0; i<slDefaultColumns.count(); i++) { // slDefaultColumns.count() - num of all columns
		pColumnVisible = (QCheckBox *)m_pColumnsButtonGroup->button(i);
		nColumnWidth = (pColumnVisible->isChecked() ? m_ntColumnsWidth[i] : 0);
		slColumns.append( QString("%1-%2").arg(i).arg(nColumnWidth) );
	}
	settings.writeEntry( "/qtcmd/FilesPanel/Columns", slColumns );
	*/
}

