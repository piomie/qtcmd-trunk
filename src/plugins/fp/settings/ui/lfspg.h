/***************************************************************************
                          lfspg  -  description
                             -------------------
    begin                : sob gru 22 2007
    copyright            : (c) 2007 by Piotr Mierzwiński, Mariusz Borowski
    email                : piom@nes.pl, mariusz@nes.pl

    copyright            : See COPYING file that comes with this project

 $Id$
 ***************************************************************************/
#ifndef LFSPG_H
#define LFSPG_H

#include "ui_lfspage.h"

/**
	@author Piotr Mierzwiński <piom@nes.pl>
*/
class LfsPg : public QWidget, public Ui::LfsPage
{
	Q_OBJECT

public:
	LfsPg();

	void init();
	QString trashPath();

private slots:
	void slotSetDefaults();

public slots:
	void slotSave();


};

#endif
