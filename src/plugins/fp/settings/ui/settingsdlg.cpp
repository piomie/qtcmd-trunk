/***************************************************************************
                          settingsdlg  -  description
                             -------------------
    begin                : pi� gru 21 2007
    copyright            : (C) 2007 by Piotr Mierzwi�ski, Mariusz Borowski
    email                : piom@nes.pl, mariusz@nes.pl

    copyright            : See COPYING file that comes with this project

 $Id$
 ***************************************************************************/
#include <QList>

#include "filesystemsettings.h"
#include "listviewsettings.h"
#include "otherappsettings.h"

#include "settingsdlg.h"


SettingsDlg::SettingsDlg()
{
    setupUi(this);

	connect( m_pListListWidget,        SIGNAL(itemSelectionChanged()), this, SLOT(slotItemSelectionChanged()) );
	connect( m_pFileSystemsListWidget, SIGNAL(itemSelectionChanged()), this, SLOT(slotItemSelectionChanged()) );
	connect( m_pOtherPageListWidget,   SIGNAL(itemSelectionChanged()), this, SLOT(slotItemSelectionChanged()) );

	connect(m_pOkBtn,    SIGNAL(clicked()), this, SLOT(slotOkButtonPressed()));
	connect(m_pApplyBtn, SIGNAL(clicked()), this, SLOT(slotApplyButtonPressed()));
	connect(m_pSaveBtn,  SIGNAL(clicked()), this, SLOT(slotSaveButtonPressed()));
	connect(m_pToolBox,  SIGNAL(currentChanged(int)), this, SLOT(slotCurrentItemChanged(int)));

	init();

	m_pToolBox->setMinimumWidth(width()/4);
}


void SettingsDlg::init()
{
	setWindowTitle( "QtCommander - "+tr("settings") );

	// --- List pages
	m_pFontAppPage = new FontChooserPage;
	m_pColorsPage  = new ColorsPg;
	m_pColumnsPage = new ColumnsPg;
	m_pOtherListSettingsPage = new OtherListSettingsPg;

	m_pStackedWidget->addWidget( m_pColorsPage );
	m_pStackedWidget->addWidget( m_pColumnsPage );
	m_pStackedWidget->addWidget( m_pFontAppPage );
	m_pStackedWidget->addWidget( m_pOtherListSettingsPage );

	// polaczyc liste ze stacked
//	slot void setCurrentIndex ( int index )

	// --- FileSystem pages
	m_pLfsPage = new LfsPg;
	m_pFtpPage = new FtpPg;
	m_pArchivesPage = new ArchivesPg;
	m_pOtherFSPage  = new OtherFSPg;

	m_pStackedWidget->addWidget( m_pArchivesPage );
	m_pStackedWidget->addWidget( m_pFtpPage );
	m_pStackedWidget->addWidget( m_pLfsPage );
	m_pStackedWidget->addWidget( m_pOtherFSPage );

	// --- Other pages
	m_pKeyshortcutsPage = new KeyShortcutsPage;
	m_pToolBarPage = new ToolBarPage;

	m_pStackedWidget->addWidget( m_pKeyshortcutsPage );
	m_pStackedWidget->addWidget( m_pToolBarPage );

	m_pKeyshortcutsPage->init( TRUE ); // TRUE -> appSettings
	m_pToolBarPage->init( TRUE ); // TRUE -> appSettings

	// --- an empty page for no items list
	m_pEmptyPage = new QWidget;
	m_pStackedWidget->addWidget( m_pEmptyPage );

	// --- init view onto dialog
	resize( QSize(635, 515).expandedTo(minimumSizeHint()) );

	static QList <int>sSW; // width the children of spliter obj.
	sSW << (width()*34)/100 << 0;
	sSW[1] = width()-sSW[0];
	m_pSplitter->setSizes( sSW );
}


void SettingsDlg::raiseWidget( const QString & sSenderName )
{
	int nListWidgetRow = 0;
	QString sTitle;

	if ( sSenderName == "m_pListListWidget" || sSenderName == "ListPage" ) {
		nListWidgetRow = m_pListListWidget->currentRow();
		if (nListWidgetRow >= 0)
			sTitle = m_pListListWidget->item(nListWidgetRow)->text();
	}
	else
	if ( sSenderName == "m_pFileSystemsListWidget" || sSenderName == "FilesSystemsPage" ) {
		nListWidgetRow = m_pFileSystemsListWidget->currentRow();
		if (nListWidgetRow >= 0)
			sTitle = m_pFileSystemsListWidget->item(nListWidgetRow)->text();
		nListWidgetRow += 4; // + pageOffset
	}
	else
	if ( sSenderName == "m_pOtherPageListWidget" || sSenderName == "OtherPage" ) {
		nListWidgetRow = m_pOtherPageListWidget->currentRow();
		if (nListWidgetRow >= 0)
			sTitle = m_pOtherPageListWidget->item(nListWidgetRow)->text();
		nListWidgetRow += 8; // + pageOffset
	}
	//qDebug() << "SettingsViewDlg::raiseWidget, nListWidgetRow=" << nListWidgetRow << ", sender=" << sSenderName << "sTitle" << sTitle;

	if (nListWidgetRow < 0)
		m_pStackedWidget->setCurrentWidget( m_pEmptyPage );
	else
		m_pStackedWidget->setCurrentIndex( nListWidgetRow );

	m_pTitleLab->setText( sTitle );
}


void SettingsDlg::slotSetKeyShortcuts( KeyShortcuts *pKeyShortcuts )
{
	if ( m_pKeyshortcutsPage )
		m_pKeyshortcutsPage->setKeyShortcuts( pKeyShortcuts );
}


void SettingsDlg::slotSetFilesAssociation( FilesAssociation * pFA )
{
	if ( m_pOtherListSettingsPage )
		m_pOtherListSettingsPage->updateFilters( pFA );
}


void SettingsDlg::slotOkButtonPressed()
{
	slotApplyButtonPressed();
	slotSaveButtonPressed();
	close();
}


void SettingsDlg::slotApplyButtonPressed()
{
	enum Colors {
		ItemColor=0, ItemColorUnderCursor,
		SelectedItemColor, SelectedItemColorUnderCursor,
		BgColorOfSelectedItems, BgColorOfSelectedItemsUnderCursor,
		FirstBackgroundColor, SecondBackgroundColor,
		FullCursorColor, InsideFrameCursorColor, OutsideFrameCursorColor,
		GridColor
	};
	enum ColumnNames { // TODO ColumnNames (id) move to some namespace
		NAMEcol=0, SIZEcol, TIMEcol, PERMcol, OWNERcol, GROUPcol
		//NAMEcol=0, EXTNAMEcol, TIMEcol, PERMcol, OWNERcol, GROUPcol
	};
	// --- inits the list settings
	ListViewSettings listViewSettings;
	// - font
	listViewSettings.fontFamily = m_pFontAppPage->fontFamily();
	listViewSettings.fontSize   = m_pFontAppPage->fontSize();
	listViewSettings.fontBold   = m_pFontAppPage->fontBold();
	listViewSettings.fontItalic = m_pFontAppPage->fontItalic();
	// - colors
	listViewSettings.showSecondBackgroundColor    = m_pColorsPage->m_pSecBgColChkBox->isChecked();
	listViewSettings.showGrid                     = m_pColorsPage->m_pShowGridChkBox->isChecked();
	listViewSettings.twoColorsCursor              = m_pColorsPage->m_pTwoColorsCursorRadioBtn->isChecked();
	listViewSettings.itemColor                    = m_pColorsPage->getColor(ItemColor);
	listViewSettings.itemColorUnderCursor         = m_pColorsPage->getColor(ItemColorUnderCursor);
	listViewSettings.selectedItemColor            = m_pColorsPage->getColor(SelectedItemColor);
	listViewSettings.selectedItemColorUnderCursor = m_pColorsPage->getColor(SelectedItemColorUnderCursor);
	listViewSettings.bgColorOfSelectedItems       = m_pColorsPage->getColor(BgColorOfSelectedItems);
	listViewSettings.bgColorOfSelectedItemsUnderCursor = m_pColorsPage->getColor(BgColorOfSelectedItemsUnderCursor);
	listViewSettings.firstBackgroundColor         = m_pColorsPage->getColor(FirstBackgroundColor);
	listViewSettings.secondBackgroundColor        = m_pColorsPage->getColor(SecondBackgroundColor);
	listViewSettings.fullCursorColor              = m_pColorsPage->getColor(FullCursorColor);
	listViewSettings.insideFrameCursorColor       = m_pColorsPage->getColor(InsideFrameCursorColor);
	listViewSettings.outsideFrameCursorColor      = m_pColorsPage->getColor(OutsideFrameCursorColor);
	listViewSettings.gridColor                    = m_pColorsPage->getColor(GridColor);
	// - columns
	listViewSettings.columnsWidthTab[NAMEcol]  = m_pColumnsPage->columnWidth( NAMEcol );
	//listViewSettings.columnsWidthTab[EXTNAMEcol]  = m_pColumnsPage->columnWidth( EXTNAMEcol ); // not yet supported TODO support for extention column
	listViewSettings.columnsWidthTab[SIZEcol]  = m_pColumnsPage->columnWidth( SIZEcol );
	listViewSettings.columnsWidthTab[TIMEcol]  = m_pColumnsPage->columnWidth( TIMEcol );
	listViewSettings.columnsWidthTab[PERMcol]  = m_pColumnsPage->columnWidth( PERMcol );
	listViewSettings.columnsWidthTab[OWNERcol] = m_pColumnsPage->columnWidth( OWNERcol );
	listViewSettings.columnsWidthTab[GROUPcol] = m_pColumnsPage->columnWidth( GROUPcol );
	listViewSettings.columnsSynchronize        = m_pColumnsPage->m_pSynchronizeColumnChkBox->isChecked();
	// - other
	listViewSettings.showIcons           = m_pOtherListSettingsPage->m_pShowIconsChkBox->isChecked();
	listViewSettings.showHiddenFiles     = m_pOtherListSettingsPage->m_pShowHiddenChkBox->isChecked();
	listViewSettings.cursorAlwaysVisible = m_pOtherListSettingsPage->m_pSelectFilesOnlyChkBox->isChecked();
	listViewSettings.selectFilesOnly     = m_pOtherListSettingsPage->m_pCursorAlwaysVisibleChkBox->isChecked();
	listViewSettings.filterForFiltered   = m_pOtherListSettingsPage->m_pFilterForFilteredChkBox->isChecked();

	listViewSettings.fileSizeFormatFL    = m_pOtherListSettingsPage->fileSizeFormatFL();
	listViewSettings.fileSizeFormatSB    = m_pOtherListSettingsPage->fileSizeFormatSB();
	listViewSettings.defaultFilter       = m_pOtherListSettingsPage->defaultFilter();

	emit signalApply( listViewSettings );

	// --- inits the file system settings
	FileSystemSettings fileSystemSettings;
	// archives page
	fileSystemSettings.workDirectoryPath = m_pArchivesPage->workDirPath();
	// lfs page
	fileSystemSettings.removeToTrash        = m_pLfsPage->m_pRemoveToTrash->isChecked();
	fileSystemSettings.weighBeforeOperation = m_pLfsPage->m_pWeighBeforeOperationChkBox->isChecked();
	fileSystemSettings.trashPath            = m_pLfsPage->trashPath();
	// ftp page
	fileSystemSettings.standByConnection         = m_pFtpPage->m_pStandByConnectChkBox->isChecked();
	fileSystemSettings.numOfTimeToRetryIfFtpBusy = m_pFtpPage->numOfTimeToRetryIfBusy();
	// other page
	fileSystemSettings.alwaysOverwrite               = m_pOtherFSPage->m_pAlwaysOverwriteChkBox->isChecked();
	fileSystemSettings.alwaysSavePermission          = m_pOtherFSPage->m_pAlwaysSavePermissionChkBox->isChecked();
	fileSystemSettings.alwaysAskBeforeDeleting       = m_pOtherFSPage->m_pAskBeforeDeleteChkBox->isChecked();
	fileSystemSettings.alwaysGetInToDirectory        = m_pOtherFSPage->m_pAlwaysGetInToDirChkBox->isChecked();
	fileSystemSettings.closeProgressDlgAfterFinished = m_pOtherFSPage->m_pCloseProgressDlgAfterFinished->isChecked();

	emit signalApply( fileSystemSettings );

	// --- inits the other apps. settings
// 	OtherAppSettings otherAppSettings;

	emit signalApplyKeyShortcuts( m_pKeyshortcutsPage->keyShortcuts() );
}


void SettingsDlg::slotSaveButtonPressed()
{
	// --- List pages
	m_pColorsPage->slotSave();
	m_pFontAppPage->slotSave();
	m_pColumnsPage->slotSave();
	m_pKeyshortcutsPage->slotSave();
	m_pOtherListSettingsPage->slotSave();

	// --- FileSystem pages
	m_pLfsPage->slotSave();
	m_pFtpPage->slotSave();
	m_pArchivesPage->slotSave();
}

void SettingsDlg::slotItemSelectionChanged()
{
	raiseWidget(sender()->objectName());
}

void SettingsDlg::slotCurrentItemChanged( int /*nItemId*/ )
{
	raiseWidget(m_pToolBox->currentWidget()->objectName());
}

