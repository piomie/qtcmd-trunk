/***************************************************************************
                          otherlistsettingspg  -  description
                             -------------------
    begin                : pi� gru 21 2007
    copyright            : (C) 2007 by Piotr Mierzwi�ski, Mariusz Borowski
    email                : piom@nes.pl, mariusz@nes.pl

    copyright            : See COPYING file that comes with this project

 $Id$
 ***************************************************************************/
#include <QSettings> // TODO add support for QSettings

#include "enums.h"

#include "otherlistsettingspg.h"

OtherListSettingsPg::OtherListSettingsPg()
{
	setupUi(this);

	m_pFileSizeFormatBtnGrpFL = new QButtonGroup;
	m_pFileSizeFormatBtnGrpFL->addButton( m_pNoSpacesFormatRadioBtnFL, 0);
	m_pFileSizeFormatBtnGrpFL->addButton( m_pSpacesFormatRadioBtnFL, 1);
	m_pFileSizeFormatBtnGrpFL->addButton( m_pWeighFormatRadioBtnFL, 2);

	m_pFileSizeFormatBtnGrpSB = new QButtonGroup;
	m_pFileSizeFormatBtnGrpSB->addButton( m_pNoSpacesFormatRadioBtnSB, 0);
	m_pFileSizeFormatBtnGrpSB->addButton( m_pSpacesFormatRadioBtnSB, 1);
	m_pFileSizeFormatBtnGrpSB->addButton( m_pWeighFormatRadioBtnSB, 2);

	connect(m_pDefaultBtn, SIGNAL(clicked()), this, SLOT(slotSetDefaults()));
	connect(m_pFiltersComboBox, SIGNAL(highlighted(int)), this, SLOT(slotFilterHighlighted(int)));

	init();
}

OtherListSettingsPg::~OtherListSettingsPg()
{
	delete m_pFileSizeFormatBtnGrpFL;
	delete m_pFileSizeFormatBtnGrpSB;
}


void OtherListSettingsPg::init()
{
	QSettings settings;

// 	bool bShowIcons = settings.readBoolEntry( "/qtcmd/FilesPanel/ShowIcons", TRUE );
// 	bool bShowHidden = settings.readBoolEntry( "/qtcmd/FilesPanel/ShowHidden", TRUE );
// 	bool bSelectFilesOnly = settings.readBoolEntry( "/qtcmd/FilesPanel/SelectFilesOnly", FALSE );
// 	bool bNotHiddenCursor = settings.readBoolEntry( "/qtcmd/FilesPanel/NotHiddenCursor", TRUE );
// 	bool bFilterForFiltered = settings.readBoolEntry( "/qtcmd/FilesPanel/FilterForFiltered", FALSE );
	bool bShowIcons = TRUE; // temporary solution (set default value)
	bool bShowHidden = TRUE;
	bool bSelectFilesOnly = FALSE;
	bool bNotHiddenCursor = TRUE;
	bool bFilterForFiltered = FALSE;

	m_pShowIconsChkBox->setChecked( bShowIcons );
	m_pShowHiddenChkBox->setChecked( bShowHidden );
	m_pSelectFilesOnlyChkBox->setChecked( bSelectFilesOnly );
	m_pCursorAlwaysVisibleChkBox->setChecked( bNotHiddenCursor );
	m_pFilterForFilteredChkBox->setChecked( bFilterForFiltered );

	//int nFileSizeFormatFL = settings.readNumEntry( "/qtcmd/FilesPanel/FileSizeFormatFileList", BKBMBGBformat );
	int nFileSizeFormatFL = BKBMBGBformat;
	if ( nFileSizeFormatFL < NONEformat || nFileSizeFormatFL > BKBMBGBformat )
		nFileSizeFormatFL = BKBMBGBformat;
	m_pFileSizeFormatBtnGrpFL->button(nFileSizeFormatFL)->setChecked(TRUE);
	//(dynamic_cast <QRadioButton *> (m_pFileSizeFormatBtnGrpFL->button(nFileSizeFormatFL)))->setChecked(TRUE);

	//int nFileSizeFormatSB = settings.readNumEntry( "/qtcmd/FilesPanel/FileSizeFormatStatusBar", BKBMBGBformat );
	int nFileSizeFormatSB = BKBMBGBformat;
	if ( nFileSizeFormatSB < NONEformat || nFileSizeFormatSB > BKBMBGBformat )
		nFileSizeFormatSB = BKBMBGBformat;
	m_pFileSizeFormatBtnGrpSB->button(nFileSizeFormatFL)->setChecked(TRUE);
	//(dynamic_cast <QRadioButton *> (m_pFileSizeFormatBtnGrpSB->button(nFileSizeFormatSB)))->setChecked(TRUE);

	m_pFileSizeFormatBtnGrpSB->button(2)->setChecked(TRUE); // TEST

	m_pFilters = NULL;
}


int OtherListSettingsPg::defaultFilter()
{
	return m_pFiltersComboBox->currentIndex();
}

int OtherListSettingsPg::fileSizeFormatFL()
{
	return m_pFileSizeFormatBtnGrpFL->checkedId();
}

int OtherListSettingsPg::fileSizeFormatSB()
{
	return m_pFileSizeFormatBtnGrpSB->checkedId();
}

void OtherListSettingsPg::updateFilters( FilesAssociation * pFA )
{
	if ( m_pFilters )
		delete m_pFilters;

	// --- init the filters
	m_pFilters = new Filters( pFA );
	for ( uint i=0; i<m_pFilters->count(); i++ )
		m_pFiltersComboBox->addItem( m_pFilters->name(i) );

	slotFilterHighlighted( m_pFilters->defaultId() );
}

void OtherListSettingsPg::slotSetDefaults()
{
	m_pShowIconsChkBox->setChecked( TRUE );
	m_pShowHiddenChkBox->setChecked( TRUE );
	m_pSelectFilesOnlyChkBox->setChecked( FALSE );
	m_pCursorAlwaysVisibleChkBox->setChecked( TRUE );
	m_pFiltersComboBox->setCurrentIndex( 0 ); // files and directories
	m_pFilterForFilteredChkBox->setChecked( FALSE );

	m_pWeighFormatRadioBtnSB->setChecked( TRUE );
	m_pWeighFormatRadioBtnFL->setChecked( TRUE );
}


void OtherListSettingsPg::slotFilterHighlighted( int nFilterId )
{
	m_pExtentionsLineEdit->setText( m_pFilters->patterns(nFilterId) );
	m_pExtentionsLineEdit->setCursorPosition( 0 );
}


void OtherListSettingsPg::slotSave()
{
	/*
	QSettings settings;

	settings.writeEntry( "/qtcmd/FilesPanel/ShowIcons", m_pShowIconsChkBox->isChecked() );
	settings.writeEntry( "/qtcmd/FilesPanel/ShowHidden", m_pShowHiddenChkBox->isChecked() );
	settings.writeEntry( "/qtcmd/FilesPanel/SelectFilesOnly", m_pSelectFilesOnlyChkBox->isChecked() );
	settings.writeEntry( "/qtcmd/FilesPanel/NotHiddenCursor", m_pCursorAlwaysVisibleChkBox->isChecked() );
	settings.writeEntry( "/qtcmd/FilesPanel/DefaultFilesFilter", m_pFiltersComboBox->currentItem() );
	settings.writeEntry( "/qtcmd/FilesPanel/FileSizeFormatFileList", m_pFileSizeFormatBtnGrpFL->checkedId() );
	settings.writeEntry( "/qtcmd/FilesPanel/FileSizeFormatStatusBar", m_pFileSizeFormatBtnGrpSB->checkedId() );
	settings.writeEntry( "/qtcmd/FilesPanel/FilterForFiltered", m_pFilterForFilteredChkBox->isChecked() );
	*/
}

