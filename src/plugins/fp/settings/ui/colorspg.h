/***************************************************************************
                          colorspg  -  description
                             -------------------
    begin                : nie gru 23 2007
    copyright            : (c) 2007 by Piotr Mierzwiński, Mariusz Borowski
    email                : piom@nes.pl, mariusz@nes.pl

    copyright            : See COPYING file that comes with this project

 $Id$
 ***************************************************************************/
#ifndef COLORSPG_H
#define COLORSPG_H

#include <QList>
#include <QButtonGroup>

#include "ui_colorspage.h"

/**
	@author Piotr Mierzwiński <piom@nes.pl>
*/
class ColorsPg : public QWidget, public Ui::ColorsPage
{
	Q_OBJECT

private:
	struct ColorsScheme {
			bool bShowSecondBackgroundColor;
			bool bShowGrid;
			bool bTwoColorCursor;
			QColor itemColor;
			QColor itemColorUnderCursor;
			QColor selectedItemColor;
			QColor selectedItemColorUnderCursor;
			QColor bgColorOfSelectedItems;
			QColor bgColorOfSelectedItemsUnderCursor;
			QColor firstBackgroundColor;
			QColor secondBackgroundColor;
			QColor fullCursorColor;
			QColor insideFrameCursorColor;
			QColor outsideFrameCursorColor;
			QColor gridColor;
	};
	QList <ColorsScheme> m_cslSchemesList;

	QButtonGroup *m_pColorsBtnGroup;

public:
	ColorsPg();
	~ColorsPg();

	QColor getColor( int nColorId );

	void init();

private:
	void drawPreview();

private slots:
	void slotShowSecondBgColor( bool bShow );
	void slotShowFullCursor( bool bShow );
	void slotSetColor( int nColorId );
	void slotSetNewScheme( int nId );
	void slotShowGrid( bool bShow );
	void slotSetGridColor();
	void slotDelete();
	void slotNew();

public slots:
	void slotSave();


};

#endif
