/***************************************************************************
                          lfspg  -  description
                             -------------------
    begin                : sob gru 22 2007
    copyright            : (C) 2007 by Piotr Mierzwiński, Mariusz Borowski
    email                : piom@nes.pl, mariusz@nes.pl

    copyright            : See COPYING file that comes with this project

 $Id$
 ***************************************************************************/
#include <QDir>
//#include <QSettings> // TODO add support for QSettings

#include "locationchooser.h"

#include "lfspg.h"


LfsPg::LfsPg()
{
	setupUi(this);

	connect(m_pDefaultBtn, SIGNAL(clicked()), this, SLOT(slotSetDefaults()));

	init();
}


void LfsPg::init()
{
	/*
	QSettings settings;

	bool bWeighBeforeOperation = settings.readBoolEntry( "/qtcmd/LFS/AlwaysWeighBeforeOperation", TRUE );
	bool bRemoveToTrash = settings.readBoolEntry( "/qtcmd/LFS/AlwaysRemoveToTrash", FALSE );
	QString sTrashDir = settings.readEntry( "/qtcmd/LFS/TrashDirectory", QDir::homePath()+"/tmp/qtcmd/trash" );

	m_pWeighBeforeOperationChkBox->setChecked( bWeighBeforeOperation );
	m_pRemoveToTrash->setChecked( bRemoveToTrash );
	m_pLocationChooser->setText( sTrashDir );
	*/
}

QString LfsPg::trashPath()
{
	return m_pLocationChooser->text();
}

void LfsPg::slotSetDefaults()
{
	m_pWeighBeforeOperationChkBox->setChecked( TRUE );
	m_pRemoveToTrash->setChecked( FALSE );
	m_pLocationChooser->setText( QDir::homePath()+"/tmp/qtcmd/trash" );
}


void LfsPg::slotSave()
{
	/*
	QSettings settings;

	settings.writeEntry( "/qtcmd/LFS/AlwaysRemoveToTrash", m_pRemoveToTrash->isChecked() );
	settings.writeEntry( "/qtcmd/LFS/AlwaysWeighBeforeOperation", m_pWeighBeforeOperationChkBox->isChecked() );
	settings.writeEntry( "/qtcmd/LFS/TrashDirectory", m_pLocationChooser->text() );
	*/
}

