/***************************************************************************
                          columnspg  -  description
                             -------------------
    begin                : nie gru 23 2007
    copyright            : (c) 2007 by Piotr Mierzwiński, Mariusz Borowski
    email                : piom@nes.pl, mariusz@nes.pl

    copyright            : See COPYING file that comes with this project

 $Id$
 ***************************************************************************/
#ifndef COLUMNSPG_H
#define COLUMNSPG_H

#include "ui_columnspage.h"

/**
	@author Piotr Mierzwiński <piom@nes.pl>
*/
class ColumnsPg : public QWidget, public Ui::ColumnsPage
{
	Q_OBJECT

private:
	//bool m_bSizeSynchronization;
	int m_ntColumnsWidth[6]; // max columns == 6, TODO need global constant to get max columns

	QButtonGroup *m_pColumnsButtonGroup;

public:
	ColumnsPg();
	~ColumnsPg();

	int columnWidth( int nId );

	void init();

private slots:
	void slotDefaultSettings();


public slots:
	void slotSave();

};

#endif
