/***************************************************************************
                          settingsdialogplugin.cpp  -  description
                             -------------------
    begin                : wed nov 5 2003
    copyright            : (C) 2003 by Piotr Mierzwi�ski
    email                : peterm@go2.pl

    copyright            : See COPYING file that comes with this project
 ***************************************************************************/

//#include "settingsdialog.h"
#include "settingsdlg.h"

// export symbol for resolve it by 'QLibrary::resolve( const char * )'
extern "C" {
	QWidget * create_plugin( QWidget * )
	{
		return new SettingsDlg();
	}
}
