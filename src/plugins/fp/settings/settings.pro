# Plik utworzony przez menad?era projekt�w kdevelopa
# ------------------------------------------- 
# Podkatalog wzgl?dem g?�wnego katalogu projektu: ./src/plugins/fp/settings
# Cel to biblioteka settingsapp

DEPENDPATH = ui 
INSTALLS += target 
target.path = $$PREFIX/lib/qtcmd/plugins 
TARGETDEPS += ../../../../build/lib/libqtcmduiext.so \
              ../../../../build/lib/libqtcmdutils.so \
              ../../../../build/lib/libqtcmddlgext.so 
LIBS += -lqtcmduiext \
        -lqtcmdutils \
        -lqtcmddlgext 
INCLUDEPATH += ../../../../src \
               ../../../../src/libs/qtcmduiext/lwv \
               ../../../../src/libs/qtcmduiext \
               ../../../../src/libs/qtcmddlgext \
               ../../../../src/libs/qtcmdutils \
               ../../../../build/.tmp/ui \
               ../../../../icons \
               ../../ \
               ./ui
MOC_DIR = ../../../../build/.tmp/moc 
UI_DIR = ../../../../build/.tmp/ui 
OBJECTS_DIR = ../../../../build/.tmp/obj 
QMAKE_LIBDIR = ../../../../build/lib
TARGET = settingsapp 
DESTDIR = ../../../../build/lib/qtcmd/plugins 
CONFIG += warn_on \
          qt \
          thread \
          plugin 
TEMPLATE = lib 
FORMS += ui/colorspage.ui \
         ui/columnspage.ui \
         ui/ftppage.ui \
         ui/lfspage.ui \
         ui/archivespage.ui \
         ui/otherfspage.ui \
         ui/otherlistsettingspage.ui \
         ui/settingsdialog.ui 
TRANSLATIONS += ../../../../translations/pl/qtcmd-setts.ts
HEADERS += ui/colorspg.h \
           ui/columnspg.h \
           ui/archivespg.h \
           ui/ftppg.h \
           ui/lfspg.h \
           ui/otherfspg.h \
           ui/otherlistsettingspg.h \
		   ui/settingsdlg.h \
           listviewsettings.h \
           filesystemsettings.h \
           otherappsettings.h
SOURCES += settingsdialogplugin.cpp \
           ui/colorspg.cpp \
           ui/columnspg.cpp \
           ui/archivespg.cpp \
           ui/ftppg.cpp \
           ui/lfspg.cpp \
           ui/otherfspg.cpp \
		   ui/otherlistsettingspg.cpp \
		   ui/settingsdlg.cpp
QT += network
CONFIG += uic4
