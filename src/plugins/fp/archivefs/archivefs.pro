# Plik utworzony przez menad?era projekt�w kdevelopa
# ------------------------------------------- 
# Podkatalog wzgl?dem g?�wnego katalogu projektu: ./src/plugins/fp/archivefs
# Cel to biblioteka archivefs

INSTALLS += target 
target.path = $$PREFIX/lib/qtcmd/plugins 
TARGETDEPS += ../../../../build/lib/libqtcmduiext.so \
              ../../../../build/lib/libqtcmdutils.so \
              ../../../../build/lib/libqtcmddlgext.so 
LIBS += -lqtcmduiext \
        -lqtcmdutils \
        -lqtcmddlgext 
INCLUDEPATH += ../../../../src \
               ../../../../src/libs/qtcmddlgext \
               ../../../../src/libs/qtcmdutils \
	       ../../../../src/libs/qtcmduiext \
	       ../../../../build/.tmp/ui \
	       ../../
MOC_DIR = ../../../../build/.tmp/moc 
OBJECTS_DIR = ../../../../build/.tmp/obj 
QMAKE_LIBDIR = ../../../../build/lib
TARGET = archivefs 
DESTDIR = ../../../../build/lib/qtcmd/plugins 
CONFIG += warn_on \
          qt \
          thread \
          plugin
TEMPLATE = lib 
HEADERS += archivefs.h \
           tar.h \
           virtualarch.h \
	   ../../vfs.h
SOURCES += archivefs.cpp \
           archivefsplugin.cpp \
           tar.cpp \
           virtualarch.cpp 
#The following line was inserted by qt3to4
QT += network
