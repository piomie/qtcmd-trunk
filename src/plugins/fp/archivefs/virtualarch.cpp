/***************************************************************************
                          virtualarch.cpp  -  description
                             -------------------
    begin                : tue dec 23 2003
    copyright            : (C) 2003 by Piotr Mierzwi�ski
    email                : peterm@go2.pl

    copyright            : See COPYING file that comes with this project
 ***************************************************************************/
#include <QtGui>

#include "virtualarch.h"


VirtualArch::VirtualArch( QWidget *parent )
	: QProcess(parent)
{
	connect( this, SIGNAL(processExited()),   this, SLOT(slotProcessExited()) );
	connect( this, SIGNAL(readyReadStdout()), this, SLOT(slotReadFromStdout()) );
	connect( this, SIGNAL(readyReadStderr()), this, SLOT(slotReadFromStderr()) );
}


void VirtualArch::listFiles( QStringList & rawFilesList, uint levelInArchive )
{
	qDebug() << "VirtualArch::listFiles(), levelInArchive=" << levelInArchive;
	emit commandStarted( VFS::List );
	emit stateChanged( state() );

	QDateTime dt;
	UrlInfoExt uie;
	long long size;
	uint slashesNum, filesNum = rawFilesList.count();
	bool isDir, isSymLink;
	QString name, perm;//, line;
	QStringList lineList;

	QString currentPath = mCurrentPath;
	currentPath.replace( ' ', '|' );
	if ( currentPath.length() > 1 )
		currentPath.remove(0,1);
	enum { PERM=0, SIZE, DATE, TIME, OWNER, GROUP, NAME };
		//line = rawFilesList[i];
	// Optymalizacja szybk. moznaby wyeliminowac zliczanie slash przez wczesniejsze dopisanie w nowej kolumnie
	// podobnie moznaby zrobic ze statusem pliku (f/d/l)

	for (uint i=0; i<filesNum; i++) {
		isDir = (rawFilesList[i].at(0) == 'd');
		slashesNum = rawFilesList[i].count('/', Qt::CaseInsensitive /*false*/);
		if ( isDir ) {
			if ( levelInArchive != slashesNum )
				continue;
		}
		else // files & links
			if ( levelInArchive-1 != slashesNum )
				continue;
		if ( rawFilesList[i].indexOf(currentPath) == -1 && slashesNum )
			continue;

		lineList = rawFilesList[i].split(' ');

		name = lineList[NAME];
		if ( name.indexOf('|') != -1 )
			name.replace('|', ' ');

		perm = lineList[PERM];
		perm.remove( 0, 1 );// remove the first bit (-/d/l) - (file/dir/link)
		uie.setPermissionsStr( perm );
		//bool readable = uie.isReadable();
		//bool executable = uie.isExecutable();
		//int permissions = uie.permissions();
		size = lineList[SIZE].toLongLong();
		dt = QDateTime::fromString(lineList[DATE]+" "+lineList[TIME], Qt::ISODate);
		isSymLink = (isDir) ? FALSE : (lineList[PERM].at(0) == 'l');

		emit listInfo(
		 UrlInfoExt( FileInfoExt::fileName(name),
		  isDir ? 0 : size, uie.permissions(), lineList[OWNER], lineList[GROUP], dt, dt,
		  isDir, ! isDir, isSymLink,
		  TRUE, isDir ? TRUE : (perm.indexOf('x')!=-1), FALSE // FALSE - is not empty
		  //uie.isReadable(), uie.isExecutable(), FALSE // FALSE - is not empty
		 )
		);
	}

	emit commandFinished( VFS::List, FALSE );
	emit done( FALSE );
}


VFS::Operation VirtualArch::currentOperation() const
{
	ArchOperation archOperation = archiveOperation();

	if ( archOperation == OPEN )
		return VFS::Connect;
	else
	if ( archOperation == LIST )
		return VFS::List;
	else
	if ( archOperation == CD )
		return VFS::Cd;
	else
	if ( archOperation == GET || archOperation == PUT )
		return VFS::Copy;
	else
	if ( archOperation == REMOVE )
		return VFS::Remove;

	return VFS::NoneOp;
}

/*
	mUrlInfo.setName();
	mUrlInfo.setSize();
	mUrlInfo.setPermissionStr();
	mUrlInfo.setLastModified();
	mUrlInfo.setLastRead();
	mUrlInfo.setOwner();
	mUrlInfo.setGroup();
	mUrlInfo.setDir();
	mUrlInfo.setFile();
	mUrlInfo.setSymLink();
*/
