/***************************************************************************
                          archivefs.cpp  -  description
                             -------------------
    begin                : tue dec 23 2003
    copyright            : (C) 2003 by Piotr Mierzwiński
    email                : peterm@go2.pl

    copyright            : See COPYING file that comes with this project

 $Id$
 ***************************************************************************/
#include <QtGui>

#include <qdir.h>
#include <qsettings.h>
#include <qapplication.h>

//#include "rar.h"
#include "tar.h"
//#include "zip.h"
#include "archivefs.h"
#include "messagebox.h"
#include "fileinfoext.h"


ArchiveFS::ArchiveFS( QWidget *pParent, const char *sz_pName )
	: VFS( pParent, sz_pName )
	, m_pVirtualArch(NULL)
{
	qDebug() << "ArchiveFS::ArchiveFS !!! VirtualArch IS NOT CREATED";
	/*
	KindOfArchive arch = FileInfoExt::kindOfArchive( inputURL );
	qDebug("ArchiveFS::ArchiveFS, arch=%d", arch );

// 	if ( arch == RARarch )
// 		m_pVirtualArch = new Rar( this );
// 	else
	if ( arch == TARarch || arch == TGZIParch || arch == TBZIP2arch || arch == GZIPcompr || arch == BZIP2compr )
		m_pVirtualArch = new Tar( this, arch );
// 	else
// 	if ( kindOfArch == ZIParch )
// 		m_pVirtualArch = new Zip( this );
	else
		qDebug("__ archive '%s' not bSupported !", QFileInfo(inputURL).extension().toLatin1().data() );

// 	if (m_pVirtualArch == NULL)
// 		return;

	m_pVirtualArch->setArchiveName( inputURL );
	qDebug("__setArchiveName, archiveName=%s", m_pVirtualArch->archiveName().toLatin1().data() );

	QSettings *pSettings = new QSettings;
	QString sWorkDirectory = pSettings->readEntry( "/qtcmd/Archives/WorkingDirectory", QDir::homeDirPath()+"/tmp/qtcmd" );
	if ( sWorkDirectory.at(sWorkDirectory.length()-1) != '/' )
		sWorkDirectory += "/";
	delete pSettings;

	m_pVirtualArch->setWorkDirectory( sWorkDirectory );

	connect( m_pVirtualArch, SIGNAL(done(bool)),                 SLOT(slotDone(bool)) );
	connect( m_pVirtualArch, SIGNAL(stateChanged(int)),          SLOT(slotStateChanged(int)) );
	connect( m_pVirtualArch, SIGNAL(commandStarted(int)),        SLOT(slotCommandStarted(int)) );
	connect( m_pVirtualArch, SIGNAL(commandFinished(int,bool)),  SLOT(slotCommandFinished(int,bool)) );
//	connect( m_pVirtualArch, SIGNAL(listInfo(const UrlInfoExt &)), SIGNAL(signalListInfo(const UrlInfoExt &)) );
	connect( m_pVirtualArch, SIGNAL(listInfo(const UrlInfoExt &)), SLOT(slotListInfo(const UrlInfoExt &)) );
	*/
}


ArchiveFS::~ArchiveFS()
{
	delete m_pVirtualArch;
}


VFS::DirWritable ArchiveFS::dirWritable( const QString & /*sPath*/ ) const
{
	// spr. czy kat. w ktorym lezy archimum jest zapisywalny
	// czy plik archiwum ma ust. "w" dla wlasciciela

	//m_pVirtualArch->archiveName()
	// ENABLEdw, DISABLEdw, UNKNOWNdw
	return UNKNOWNdw;
}


void ArchiveFS::readDir( const URL & url )
{
	m_pVirtualArch->list( url.path() );
	if ( m_pVirtualArch->errorCode() == NoError )
		mSkipNextErrorOccures = FALSE;
}

		// ------- SLOTS ---------
/*
void ArchiveFS::slotPause( bool stop )
{
}


void ArchiveFS::slotReadFileToView( const QString & sFileName, QByteArray & buffer )
{
}
*/


void ArchiveFS::slotStateChanged( int nState )
{
	switch ( (VirtualArch::ArchState)nState ) {
		case VirtualArch::Connecting:
			emit signalSetInfo( tr("Reading an archive") );
			break;
		case VirtualArch::Listing:
			emit signalSetInfo( tr("Listing an archive") );
			break;
/*	case QFtp::Unconnected:
			emit signalSetInfo( tr("Unconnected") );
			break;
		case QFtp::HostLookup:
			emit signalSetInfo( tr("Host lookup") );
			break;
		case QFtp::Connected:
			emit signalSetInfo( tr("Connected") );
			break;
		case QFtp::LoggedIn:
			emit signalSetInfo( tr("Logged in as")+" "+mUserName );
			break;
		case QFtp::Closing:
			emit signalSetInfo( tr("Closing") );
			break;*/
			default:
			break;
	}
}


void ArchiveFS::slotCommandStarted( int nCommand )
{
// 	qDebug("ArchiveFS::slotCommandStarted(), operation=%s, nCommand=%s", cmdString(mSoLF->currentOperation()).toLatin1().data(), cmdString((Operation)nCommand).toLatin1().data() );
	qDebug() << "ArchiveFS::slotCommandStarted, nCommand=" << cmdString((Operation)nCommand);

	if ( nCommand == List ) { // if ( currentOp == List )  {
		//qDebug("__ start listing dir=%s", (host()+m_pVirtualArch->nameOfProcessedFile()).toLatin1().data() );
		// absHostURL() - zwraca tylko sciezke, po wejsciu do archiwum
		qDebug() << "__ start listing nameOfProcessedFile=" << nameOfProcessedFile();
//		qDebug("__ start listing absHostURL=%s, nameOfProcessedFile=%s", absHostURL().toLatin1().data(), nameOfProcessedFile().toLatin1().data() );
		QApplication::setOverrideCursor( QCursor(Qt::WaitCursor) );
		emit signalPrepareForListing(); // min. ustawia zmienna korzenia dla drzewa
	}
}


void ArchiveFS::slotCommandFinished( int nCommand, bool bError )
{
	qDebug() << "ArchiveFS::slotCommandFinished, nCommand=" << cmdString((VFS::Operation)nCommand) << ", errorStat=" << bError;

	if ( bError ) {
		if ( nCommand != VFS::Connect )
			slotDone( bError );
		return;
	}
	// po wejsciu do archiwum (po Connect) nameOfProcessedFile jest rowny arc://nazwa_arch.roz/
	//qDebug("__ absHostURL()=%s, nameOfProcessedFile()=%s", absHostURL().toLatin1().data(), m_pVirtualArch->nameOfProcessedFile().toLatin1().data() );
	qDebug() << "__ nameOfProcessedFile()=" << m_pVirtualArch->nameOfProcessedFile();

	if ( nCommand == Connect )
		mCurrentURL = m_pVirtualArch->nameOfProcessedFile()+"/";
		//setCurrentURL( m_pVirtualArch->nameOfProcessedFile()+"/" );
/*	else
	if ( nCommand == Cd || nCommand == List )
		mCurrentURL = m_pVirtualArch->nameOfProcessedFile()+"/";
		setCurrentURL( absHostURL() ); // mSkipNextErrorOccures = FALSE;*/
// 	else
// 	if ( nCommand ==  )
}


void ArchiveFS::slotDone( bool bError )
{
	QApplication::restoreOverrideCursor();
	Operation currentOp = m_pVirtualArch->currentOperation();
	qDebug() << "ArchiveFS::slotDone(), errorStat=" << bError << ", operation=" << cmdString(currentOp);
	// po wejsciu do archiwum (po Connect) nameOfProcessedFile jest rowny arc://nazwa_arch.roz/
	//qDebug("ArchiveFS::slotDone(), errorStat=%d, errorNo=%d, operation=%s", bError, mSoLF->errorNumber(), cmdString(currentOp).toLatin1().data() );
	if ( bError && currentOp == Connect ) { // cannot open an archive
		MessageBox::critical( this,
		 tr("Can not open an archive")+"\n\n"+m_pVirtualArch->nameOfProcessedFile()
		);
		return; // no return made CRASH, becouse FS will be removed and later is call to not exists method from Tar class
	}
	// FIXME timer na progresie ust. wyzej
	//startProgressDlgTimer( FALSE ); // stop it
/*
	if ( ! bError )
		;
	else
		;
*/
// 		m_pVirtualArch->setState( VirtualArch::Opened ); // nState powinien byc tutaj tylko lapany przez sygnal
	emit signalResultOperation( currentOp, !bError );
}


// -------- TYMCZASOWA FUNKCJA DLA TESTOW File Systemu
QString ArchiveFS::cmdString( VFS::Operation op )
{
	QString s = "Unknown";

			if ( op == HostLookup ) s = "HostLookup";
	else if ( op == NoneOp )   s = "NoneOp";
	else if ( op == Connect )  s = "Connect";
	else if ( op == List )     s = "List";
	else if ( op == Open )     s = "Open";
	else if ( op == Cd )       s = "Cd";
	else if ( op == Rename )   s = "Rename";
	else if ( op == Remove )   s = "Remove";
	else if ( op == MakeDir )  s = "MakeDir";
	else if ( op == MakeLink ) s = "MakeLink";
	else if ( op == Get )      s = "Get";
	else if ( op == Put )      s = "Put";
	else if ( op == Touch )    s = "Touch";
	else if ( op == Copy )     s = "Copy";
	else if ( op == Move )     s = "Move";
	else if ( op == Weigh )    s = "Weigh";
	else if ( op == Find )     s = "Find";
	else if ( op == SetAttributs ) s = "SetAttributs";

	return s;
}
