/***************************************************************************
                          tar.cpp  -  description
                             -------------------
    begin                : tue dec 23 2003
    copyright            : (C) 2003 by Piotr Mierzwi�ski
    email                : peterm@go2.pl

    copyright            : See COPYING file that comes with this project
 ***************************************************************************/
#include <QtGui>

#include "tar.h"
#include "messagebox.h"

#include <qtimer.h>


Tar::Tar( QWidget *parent, KindOfArchive kindOfArchive, int compressionLevel, const char */*name*/ )
	: VirtualArch(parent),
	  mKindOfArchive(kindOfArchive), mCompressionLevel(compressionLevel), mState(NotOpened)
{
	mErrorCode = VFS::NoError;
}


void Tar::runProcess( ArchOperation archOperation, const QStringList & filesList )
{
	mListFilesToProcessed = filesList;
	mArchOperation = archOperation;
	mBuffer = "";

	mArgumentsList.clear();
	mArgumentsList.append( "tar" );

	if ( mArchOperation == CREATE ) {
		if ( targetPath().isEmpty() ) {
			qDebug("Tar::runProcess, 'targetPath' is empty !");
			return;
		}
		if ( archiveName().isEmpty() ) {
			qDebug("Tar::runProcess, 'archiveName' is empty !");
			return;
		}
		QString ext = "tar";
		if ( mKindOfArchive == TBZIP2arch )
			ext += ".bz2";
		else
		if ( mKindOfArchive == TGZIParch )
			ext += ".gz";
		QString targetFileName = targetPath()+archiveName()+ext;

// tak robi mc: 'tar cf - nazwa | bzip2 -f > nazwa.tar.bz2'
		mArgumentsList << "c" << "-C" << targetPath() << "-vf" << targetFileName;
		mArgumentsList += filesList;
	}
	else
	if ( mArchOperation == OPEN ) {
		mArgumentsList << "tvf" << filesList[0];
	}
	else
	if ( mArchOperation == PUT ) // append file(s)
		mArgumentsList << " -f" << "--target-directory=" << targetPath();
	else
	if ( mArchOperation == GET || mArchOperation == VIEW ) { // GET it is extract file(s), VIEW - view of file
		mArgumentsList << " -C" << targetPath() << "-x";
		mArgumentsList += filesList;
		mArgumentsList << " -p" << "--same-owner" << "--force-local" << "-vf" << archiveName();
	}
	else
	if ( mArchOperation == EXTRACTall )
		mArgumentsList << "xvf" << filesList[0];
	else
	if ( mArchOperation == REMOVE ) {
		mArgumentsList << " --delete -vf";
		mArgumentsList += filesList;
	}
	else
	if ( mArchOperation == TEST ) {
		if ( mKindOfArchive == BZIP2compr || mKindOfArchive == GZIPcompr )
			mArgumentsList << "-tf";
		else // tar/tar.bz2/tar.gz
			mArgumentsList << "tvf";

		mArgumentsList << filesList[0];
	}

	if ( mKindOfArchive == TBZIP2arch )
		mArgumentsList[1].insert( 0, 'j' ); // dla wer.bzip2 nowszej niz ...., dla starszej wlasciwa jest opcja 'y'
	else
	if ( mKindOfArchive == TGZIParch )
		mArgumentsList[1].insert( 0, 'z' );


	if ( mArchOperation == LIST ) // archive open
		QTimer::singleShot( 0, this, SLOT(slotOpen()) );
	else
		QTimer::singleShot( 0, this, SLOT(slotRunProcess()) );
}


void Tar::cd( const QString & dirName )
{
	QString currentDir = (dirName.at(dirName.length()-1) != '/') ? "/" : dirName;
	mDirHierachyLevel = currentDir.count('/');
}


void Tar::list( const QString & dirName )
{
	uint pathBegin = dirName.indexOf(archiveName())+archiveName().length(); // zawsze ta sama wart.
	QString currentDir = dirName.right(dirName.length()-pathBegin);
	if ( currentDir.isEmpty() ) // first level in an archive
		currentDir = "/";

	mDirHierachyLevel = currentDir.count('/');
	setCurrentPath( currentDir );
	qDebug() << "Tar::list, on level=" << mDirHierachyLevel << ", dirName=" << dirName << ", currentDir=" << currentDir << ", mState=" << mState;
	ArchOperation archOperation = LIST;
	mNameOfProcessedFile = (NotOpened) ? dirName.left(pathBegin) : dirName; // archiveName without path inside

	if ( currentDir == "/" && mState == NotOpened ) {
		// user want to list an archive - need to open it
		archOperation = OPEN;
		emit commandStarted( VFS::Connect );
	}
	else {
		if ( mState == Opened ) {
			mState = Listing;
			mArchOperation = LIST;
			listFiles( mFilesListBuffer, mDirHierachyLevel );
			mState = Opened;
		}
		else // wykonywac to takze dla operacji reread current dir
		if ( mState == NotOpened ) {
			archOperation = OPEN;
			emit commandStarted( VFS::Connect );
		}
	}

	if ( archOperation != LIST )
	{
        QStringList lst;
        lst << mNameOfProcessedFile;
		runProcess( archOperation, lst );
	}
}


void Tar::append( const QStringList & filesList )
{
	runProcess( PUT, filesList );
}


void Tar::remove( const QStringList & filesList )
{
	runProcess( REMOVE, filesList );
}


void Tar::extract( const QStringList & filesList, bool all )
{
	ArchOperation archOperation = (all) ? EXTRACTall : GET;
	runProcess( archOperation, filesList );
}


void Tar::getWeighingStat( long long & weigh, uint & files, uint & dirs )
{
	weigh = 0; files = 0; dirs = 0;
	qDebug("Tar::getWeighingStat, not yet supported !");
}

		// ------- SLOTS ---------

void Tar::slotReadFromStdout()
{
//	mBuffer += QString::fromLocal8Bit(readStdout());
//	mBuffer += readAllStandardOutput();
	mBuffer = readAllStandardOutput();
}


void Tar::slotReadFromStderr()
{
//	mErrBuffer += readStderr();
	mErrBuffer = readAllStandardError();
}


void Tar::slotBreakProcess()
{
}


void Tar::slotProcessExited()
{
	qDebug("Tar::slotProcessExited(), exitStatus()=%d", exitStatus() );

	if ( mArchOperation == OPEN ) {
		mState = (exitStatus() == 0) ? Opened : NotOpened;

		emit commandFinished( VFS::Connect, (mState==NotOpened) );
		emit done( (mState==NotOpened) );

		if ( mState == Opened ) {
			slotOpen(); // prepare buffer for listing the files on specific level
			mArchOperation = LIST;
			listFiles( mFilesListBuffer, mDirHierachyLevel ); // parent class method
		}
	}

	// FIXME tutaj trzeba zaimplementowac obsluge bledow (exitStatus() != 0)
	// exitStatus == 2, dla OPEN, komunikat err: "stara opcja 'f' wymaga argumentu", wywolanie: 'tar tvf'

	if ( exitStatus() ) // if an error occures
		qDebug() << "__ stdErr:\n" << mErrBuffer;
// 	else
// 		qDebug() << "__ stdOut:\n" << mBuffer;

	mErrBuffer = "";
	mArchOperation = NONE;
}


void Tar::slotRunProcess()
{
	mErrorCode = VFS::NoError;

	QString sProcess;
	for(int i=0; i<mArgumentsList.count(); i++)
		sProcess += mArgumentsList[i]+" ";
	qDebug() << "Tar::slotRunProcess, CMD=" << sProcess;

	start(sProcess);
/*	if ( ! start(sProcess) ) {
		MessageBox::critical( 0, tr("Could not start")+" :\n"+sProcess );
		mErrorCode = VFS::CannotRunProc;
		emit commandFinished( VFS::Connect, TRUE ); // TRUE - error
	}
*/
}


void Tar::slotOpen()
{
	if ( mBuffer.isEmpty() )
		return;

	emit stateChanged( mState );
	//mBuffer = QString::fromLocal8Bit(mBuffer); // ponizej jest to robione w dla kazdej nazwy pliku

	// --- prepare buffer to pull out function
	// Output string list (mFilesListBuffer) will have the follow memebers:
	//  Size Date Time Permissions Owner Group Name
	// Note: Spaces in the 'Name' will replaces to the '|' characters
	mFilesListBuffer.clear();
	uint j, k, lines = mBuffer.count('\n');
	QString line, fileName;
	QStringList lineList, ownerAndGroup;
	QStringList bufStrList = mBuffer.split('\n');
	qDebug("Tar::slotOpen(), filesNum=%d, preparing filesStringList that is need to work listing the files", bufStrList.count() );
	enum { PERM=0, OWNER, SIZE, DATE, TIME }; // , NAME
// 	for (uint i=0; i<bufStrList.count(); i++) // show buffer
// 		qDebug("bufStrList[%d]=%s", i, bufStrList[i].toLatin1().data() );

	for (uint i=0; i<lines; i++) { // i<bufStrList.count();
		line = bufStrList[i];
		j = 0; k = 12; // k==12 - made skip permissions
		while (line[k++] != ' ') ; // skip owner/group
		while (line[k++] == ' ') ; // skip spaces (before size)
		while ((j++) < 3)  while (line[k++] != ' ') ; // skip: size, date, time
		fileName = line.right(line.length() - k);
			// if a name have trailing spaces then need to save it
		if ( fileName.indexOf(' ') != -1 )
			fileName.replace( ' ', '|' );

		lineList = line.split(' ') ;
		ownerAndGroup = lineList[OWNER].split('/'); // get owner and group
		if ( lineList[PERM].at(0) == 'l') // is SymLink
			fileName = fileName.left(fileName.indexOf("->")-1);

		fileName = QString::fromLocal8Bit( fileName.toLatin1().data() );

		line =  lineList[PERM]+" "+lineList[SIZE]+" "+lineList[DATE]+" "+lineList[TIME]+" ";
		line += ownerAndGroup[0]+" "+ownerAndGroup[1]+" "+fileName;

		mFilesListBuffer.append( line ); // += line;
	}
}
