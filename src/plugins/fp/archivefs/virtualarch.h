/***************************************************************************
                          virtualarch.h  -  description
                             -------------------
    begin                : tue dec 23 2003
    copyright            : (C) 2003 by Piotr Mierzwi�ski
    email                : peterm@go2.pl

    copyright            : See COPYING file that comes with this project
 ***************************************************************************/

#ifndef _VIRTUALARCH_H_
#define _VIRTUALARCH_H_

#include "vfs.h"
#include "enums.h"
#include "fileinfoext.h"

#include <QWidget>
#include <QProcess>

/**
 * Author:
 * Piotr Mierzwi�ski
 **/
/** @short Klasa najwy�szego poziomu z obs�uguj�cych archiwum.
 Nast�puje tu ko�cowe przetwarzanie polece� wywo�ywanych na rzecz archiw�w,
 np. listowanie. Wysy�ane s� tutaj te� sygna�y przeznaczone dla obiektu klasy
 systemu plik�w (FSManager).
*/
class VirtualArch : public QProcess
{
	Q_OBJECT
public:
	/** Konstruktor klasy.\n
	 @param parent - wska�nik na rodzica,
	 ��czone s� tu sygna�y ze slotami.
	 */
	VirtualArch( QWidget *parent );

	/** Destruktor.\n
	 Brak definicji.
	 */
	virtual ~VirtualArch() {}

	/** Operacje na archiwum.
	 */
	enum ArchOperation { OPEN, CD, LIST, CREATE, GET, PUT, REMOVE, VIEW, TEST, EXTRACTall, NONE };

	/** Stan operacji na archiwum.
	 */
	enum ArchState { Opened, NotOpened, BreakedProcessing, Connecting, Analysis, Listing, Putting, Getting, Removing };

	/** Metoda uruchamia funkcj� zmiany katalogu w archiwum.
	 */
	virtual void cd( const QString & ) {}

	// ponizszych 5 wirt.met. bedzie przeniesionych do biezacej klasy
	/** Metoda uruchamia listowanie podanego archiwum.
	 */
	virtual void list( const QString & ) {}

	/** Lista plik�w z archiwum dla podgl�du pliku przy u�yciu klawisza F3.
	 */
	virtual void quickList( const QString & ) {}

	/** Metoda uruchamia do��czanie podanych plik�w do archiwum.
	 */
	virtual void append( const QStringList & ) {}

	/** Metoda uruchamia wypakowywanie podanych plik�w z archiwum.
	 */
	virtual void extract( const QStringList & , bool ) {}

	/** Metoda uruchamia usuni�cie podanych plik�w z archiwum.
	 */
	virtual void remove( const QStringList & ) {}

	/** Uruchamia pobieranie wynik�w operacji wa�enia, tj. waga ca�o�ci, ilo��
	 plik�w i katalog�w
	 */
	virtual void getWeighingStat( long long & , uint & , uint &  ) {}

	/** Zwraca kod b��du dla ostatniej operacji na archiuwm.
	 */
	virtual VFS::Error errorCode() const { return VFS::NoError; }

	/** Zwraca nazw� aktualnie przetwarzanego pliku z absolutn� �cie�k�.
	 */
	virtual QString nameOfProcessedFile() const { return QString::null; }

	/** Zwraca bie��cy stan operacji na archiwum.
	 */
	virtual int state() const { return NotOpened; }


	/** Ustawiana jest tutaj nazwa bie��cego archiwum.\n
	 @param fileName - nazwa pliku z archiwum.
	 */
	void setArchiveName( const QString & fileName ) { mArchiveName = FileInfoExt::archiveFullName(fileName); }

	/** Ustawia docelow� �cie�k� dla operacji na archiwum, np. dla wypakowywania.\n
	 @param targetPath - docelowa �cie�ka.
	 */
	void setTargetPath( const QString & targetPath ) { mTargetPath  = targetPath; }

	/** Ustawia bie��c� �cie�k� w archiwum.\n
	 @param - bie��ca �cie�ka w archiwum.
	 */
	void setCurrentPath( const QString & currentPath ) { mCurrentPath = currentPath; }

	/** Zwraca nazw� archiwum.\n
	 @return nazwa archiwum.
	 */
	QString archiveName() const { return mArchiveName; }

	/** Zwraca docelow� �cie�ka dla operacji na archiwum.\n
	 @return docelowa �cie�ka.
	 */
	QString targetPath() const  { return mTargetPath;  }

	/** Zwraca bie��c� �cie�k� w archiwum.\n
	 @return bie��ca �cie�ka.
	 */
	QString currentPath() const { return mCurrentPath; }

	/** Metoda zwraca warto�� okre�laj�c� bie��c� operacj�.\n
	 @return bie��ca operacja.\n
	 Rodzaj operacji pobierany jest metod� @em archiveOperation() i konwertowany
	 na warto�� typu Operation.
	 */
	VFS::Operation currentOperation() const;

	/** Metoda powoduje wys�anie poprzez sygna� @em listInfo() obiekt�w typu
	 UrlInfoExt, kt�re s� plikami z podanego poziomu w archiwum.\n
	 @param rawFilesList - lista plik�w przygotowana w slocie @em slotOpen() klasy
	 bie��cego obiektu archiwum (np. 'Tar'),
	 @param levelInArchive - poziom, w archiwum z kt�rego nale�y wylistowa� pliki
	 (liczony od 1).
	 \n Uwaga !\n Slot @em slotOpen() musi przygotowa� elementy listy w taki
	 spos�b, aby ka�dy mia� nast�puj�ce pola (rozdzielone spacjami):
	 Prawa_dost�pu Rozmiar Data Czas W�a�ciciel Grupa Nazwa. Przy czym spacje w polu
	 'Nazwa' musz� by� zamienione na znaki '|".
	 */
	void listFiles( QStringList & rawFilesList, uint levelInArchive );

	/** Zwraca nazw� katalogu roboczego.\n
	 @return nazwa katalogu roboczego.
	 */
	QString workDirectory() const { return mWorkDirectory; }

	/** Ustawian �cie�k� dla katalogu roboczego.\n
	 @param workDirectory - nowa �cie�k� dla katalogu roboczego.
	 */
	void setWorkDirectory( const QString & workDirectory ) { mWorkDirectory = workDirectory; }

private:
	QString mArchiveName, mTargetPath, mCurrentPath, mWorkDirectory;

protected:
	/** Zwraca identyfikator bie��cej operacji na archiwum.
	 */
	virtual ArchOperation archiveOperation() const { return NONE; }

	/** Zwraca nazw� w�a�ciciela bie��cego archiwum.\n
	 @return nazwa w�a�ciciela.
	 */
	QString loggedOwner() { return QFileInfo(mArchiveName).owner(); }

	/** Nazwa grupy, do kt�rej nale�y bie��ce archiwum.\n
	 @return nazwa grupy.
	 */
	QString loggedGroup() { return QFileInfo(mArchiveName).group(); }

private slots:
	/** Slot wywo�ywany w chwili zako�czenia dzia�ania uruchomionego procesu.
	 */
	virtual void slotProcessExited()  {}

	/** Slot wywo�ywany w momencie przerywania dzialania procesu.
	 */
	virtual void slotBreakProcess()   {}

	/** Slot wywo�ywany w chwili przesy�ania danych przez proces na standardowe
	 wyj�cie.
	 */
	virtual void slotReadFromStdout() {}

	/** Slot wywo�ywany, gdy proces przesy�a dane na standardowe wyj�cie b��du.
	 */
	virtual void slotReadFromStderr() {}

signals:
	/** Sygna� wysy�any po zako�czeniu operacji na archiwum.\n
	 @param error - r�wne TRUE, je�li wyst�pi� b��d, w przeciwnym razie FALSE.
	 */
	void done( bool error );

	/** Sygna� wysy�any przed rozpocz�ciem bie��cego polecenia.\n
	 @param command - polecenie do wykonania ( typu @see Operation ).
	 */
	void commandStarted( int command );

	/** Sygna� wysy�any po zako�czeniu bie��cego polecenia.\n
	 @param command - polecenie do wykonania ( typu @see Operation ),
	 @param error - TRUE wyst�pi� b��d podczas wykonywania polecenia, FALSE
	 polecenie zako�czono si� pomy�lnie.
	 */
	void commandFinished( int command, bool error );

	/** Sygna� wysy�any przed zmian� na kolejne polecenie.\n
	 @param state - aktualne polecenie ( typu @see Operation )
	 */
	void stateChanged( int state );

	/** Sygna� wysy�any w momencie pobrania kolejnego elementu podczas listowania
	 katalogu, wysy�a informacje o bie��cym pliku lub katalogu.\n
	 @param urlInfo - informacja o pliku lub katalogu
	 */
	void listInfo( const UrlInfoExt & urlInfo );

};

#endif
