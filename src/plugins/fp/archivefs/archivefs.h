/***************************************************************************
                          archivefs.h  -  description
                             -------------------
    begin                : tue dec 23 2003
    copyright            : (C) 2003 by Piotr Mierzwiński
    email                : peterm@go2.pl

    copyright            : See COPYING file that comes with this project

 $Id$
 ***************************************************************************/

#ifndef _ARCHIVEFS_H_
#define _ARCHIVEFS_H_

#include "virtualarch.h"
#include "enums.h"
#include "vfs.h"


class ArchiveFS : public VFS
{
	Q_OBJECT
public:
	ArchiveFS( QWidget *pParent, const char *sz_pName=0 );
	~ArchiveFS();

	// ------ INFO ABOUT OPERATIONS -----

	Error errorCode() const { return (m_pVirtualArch) ? m_pVirtualArch->errorCode() : Unknown; }

//	QString host() const { return QString("arc://"+m_pVirtualArch->archiveName()+"/"); }
	//QString host() const { return currentURL().left(currentURL().find(m_pVirtualArch->archiveName()))+m_pVirtualArch->archiveName()+"/"; }
	QString host() const { return (m_pVirtualArch) ? m_pVirtualArch->archiveName()+"/" : QString(""); }
	QString path() const { return (m_pVirtualArch) ? m_pVirtualArch->currentPath() : QString(""); }

	QString nameOfProcessedFile() const {
		return (m_pVirtualArch) ? m_pVirtualArch->nameOfProcessedFile() : QString("");
	}

	bool isReadable( const QString & sFileName, bool bSilent=FALSE, bool bSendSignal=FALSE ) {
		return TRUE;
	}

	DirWritable dirWritable( const QString & sPath=QString::null ) const;

	void getWeighingStat( long long & nWeigh, uint & nFiles, uint & nDirs ) {
		m_pVirtualArch->getWeighingStat( nWeigh, nFiles, nDirs );
	}

	void getNewUrlInfo( UrlInfoExt & uiNewUrlInfo ) {
		uiNewUrlInfo = mNewUrlInfo;
	}

	QString workDirectory() const { return m_pVirtualArch->workDirectory(); }

	// ------ FILE OPERATIONS -----

	void readDir( const URL & url );


	void setWorkDirectoryPath( const QString & sWorkDirectory ) {
		m_pVirtualArch->setWorkDirectory( sWorkDirectory );
	}

private:
	VirtualArch *m_pVirtualArch;
	UrlInfoExt mNewUrlInfo;

	bool mSkipNextErrorOccures;
	QString mCurrentURL;

// public slots:
// 	void slotPause( bool stop );
// 	void slotReadFileToView( const QString & sFileName, QByteArray & buffer );

	QString cmdString( VFS::Operation op );

private slots:
	void slotDone( bool bError );
	void slotStateChanged( int nState );
	void slotCommandStarted( int nCommand );
	void slotCommandFinished( int nCommand, bool bError );
//	void slotUpdateItemOnLV( const QString & );

};

#endif
