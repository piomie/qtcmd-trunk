 /***************************************************************************
                          ftpfs.h  -  description
                             -------------------
    begin                : sat nov 30 2002
    copyright            : (C) 2002 by Piotr Mierzwi�ski
    email                : peterm@go2.pl

    copyright            : See COPYING file that comes with this project
 ***************************************************************************/

#ifndef _FTPFS_H_
#define _FTPFS_H_

#include "vfs.h"
#include "qftpext.h"

/**
 * Author:
 * Piotr Mierzwi�ski
 **/
/** @short Klasa klienta FTP.
 Obs�ugiwane s� tu wszystkie podstawowe operacje na plikach, takie jak:
 kopiowanie/przenoszenie usuwanie, tworzenie nowych pustych, tworzenie nowych
 katalog�w, zmiana atrybut�w. Podczas listowania rozr�niane s� linki do plik�w
 i do katalog�w. Nie s� wspierane rekurencyjne operacje na plikach.
 */
class FtpFS : public VFS
{
	Q_OBJECT
public:
	/** Konstruktor klasy.\n
	 @param parent - wska�nik na rodzica,
	 @param name - nazwa dla obiektu.\n
	 Inicjowane s� tu sk�adowe klasy, tworzony obiekt klasy QFtpExt oraz ��czone
	 sygna�y ze slotami.
	 */
	FtpFS( QWidget *parent, const char *name=0 );

	/** Destruktor.\n
	 Usuwany jest tu obiekt uzywany w klasie (klasy QFtpExt).
	 */
	~FtpFS();

	// ------ INFO ABOUT OPERATIONS -----

	/** Zwraca nazw� g��wnego katalogu.\n
	 @return nazwa g��wnego katalogu - 'ftp://nazwa_hosta/'.
	 */
	QString host() const { return QString("ftp://"+mFtpExt->host()+"/"); }
//	QString host() const { return QString("ftp://"+mFtpExt->hostName()+"/"); }

	/** Zwraca bie��c� �cie�k� absolutn�.\n
	 @return bie��ca �cie�ka absolutna (bez nazwy g��wnego katalogu).
	 */
	QString path() const { return mFtpExt->directory(); }
//	QString path() const { return mFtpExt->remoteDir(); }

	/** Zwraca numer portu na kt�rym obs�ugiwany jest klient FTP.\n
	 @return numer portu.
	 */
	uint portNum() const { return mFtpExt->port(); }

	/** Zwraca nazw� (ze �cie�k� absolut�) aktualnie przetwarzanego pliku.
	 @return nazwa pliku ze �cie�� absolutn�.
	 */
	QString nameOfProcessedFile() const { return mFtpExt->nameOfProcessedFile(); }

	/** Zwraca kod b��du dla ostatniej operacji plikowej.
	 @return kod b��du, patrz: @see Error.
	 */
	Error errorCode() const { return mFtpExt->errorCode(); }

	/** Metoda sprawdza czy podany plik istnieje i czy mo�na go odczytywa�.\n
	 @param fileName - nazwa pliku do sprawdzenia.
	 @param silent - TRUE spowoduje, �e nie zostanie pokazana informacja
	  o b��dzie,
	 @param sendSignal - r�wne TRUE wymusza wys�anie sygna�u 'signalReadyRead',
	  gdy nie pojawi si� b��d.\n
	 @return TRUE je�li sprawdzanie zako�czy�o si� sukcesem, w przeciwnym
	  wypadku FALSE.
	 Uwaga! Metoda ma pust� definicj�.
	 */
	bool isReadable( const QString & fileName, bool silent=FALSE, bool sendSignal=FALSE );

	/** Uruchamia funkcj� pobieraj�c� wyniki operacji wa�enia, tj. waga ca�o�ci,
	 ilo�� plik�w i katalog�w.\n
	 @param weight - zmienna, w kt�rej zapisana b�dzie waga,
	 @param files - zmienna do zapisania ilo�ci plik�w,
	 @param dirs - zmienna do zapisania ilo�ci katalog�w.\n
	 */
	void getWeighingStat( long long & weigh, uint & files, uint & dirs ) {
		mFtpExt->getWeighingStat( weigh, files, dirs );
	}

	/** Pobiera informacj� o pliku wcze�niej zainicjowan� w sk�adowej
	 @em mNewUrlInfo .\n
	 @param newUrlInfo - zmienna, w kt�rej nale�y umie�ci� informacj� o pliku.\n
	 Metoda jest wykorzystywana przez @em FilesPanel::slotResultOperation() ,
	 w celu zapewnienia odpowiedniej obs�ugi widoku listy.
	 */
	void getNewUrlInfo( UrlInfoExt & newUrlInfo ) {
		newUrlInfo = mNewUrlInfo;
	}

	// ------ FILE OPERATIONS -----

	/** Je�li u�ytkownik jest zalogowany, wtedy podejmowana jest pr�ba zmiany
	 katalogu, w przeciwnym razie pokazywane jest okienko z pytaniem o ponowne
	 po��czenie.\n
	 @param inputURL - nowy URL do odczytania.\n
	*/
	void readDir( const URL & inputURL );

	/** Utworzenie nowego katalogu.\n
	 @param dirName - nazwa nowego katalogu.
	 */
	void mkDir( const QString & dirName );

	/** Metoda uruchamia procedur� utworzenia nowego pustego pliku.\n
	 @param fileName - nazwa pliku do utworzenia.\n
	*/
	void mkFile( const QString & fileName );

	/** Metoda powoduje podj�cie pr�by usuni�cia podanych plik�w.\n
	 @param filesList - lista plik�w do usuni�cia.
	 */
	void removeFiles( const QStringList & filesList );

	/** Metoda uruchamia wa�enie podanych na li�cie plik�w.\n
	 @param filesList - lista plik�w do zwa�enia
	 @param realWeight - r�wne TRUE spowoduje, �e funkcja wa��ca uwzgl�dni
	  rzeczywist� wag� pliku w systemie plik�w.\n
	 */
	void weighFiles( const QStringList & filesList, bool realWeigh );

	/** Metoda uruchamia "surowe polecenie" ustawiania atrybut�w dla ka�dego pliku
	 z listy.\n
	 @param filesList - lista plik�w, kt�rym nale�y ustawi� atrybuty,
	 @param newUrlInfo - informacja o atrybutach dla plik�w,
	 @param recursive - r�wne TRUE, spowoduje �e atrybuty b�d� zmieniane dla ca�ej
	  zawarto�ci katalog�w z listy,
	 @param changesFor - rodzaj zmiany atrybut�w, dozwolone s�:
	  @li 0 - zmiana dla wszystkich,
	  @li 1 - zmiana tylko dla plik�w,
	  @li 2 - zmiana tylko dla katalog�w.\n
	 */
	void setAttrib( const QStringList & filesList, const UrlInfoExt & newUrlInfo, bool recursive, int changesFor );

	/** Metoda powoduje uruchomienie wyszukiwanie pliku, wg podanych kryteri�w.\n
	 @param findCriterion - adres obiektu zawieraj�cego kryteria wyszukiwania.\n
	 @param stop - r�wne TRUE powoduje wywo�anie wyszukiwania, FALSE powoduje
	  zatrzymanie operacji.\n
	 Metoda jest wywo�ywana po uruchomieniu wyszukiwania na dialogu szukania pliku.\n
	 */
	void find( const FindCriterion & findCriterion, bool stop );

	/** Zapisuje zawarto�� podanego bufora do podanego pliku.\n
	 @param buffer - adres bufora,
	 @param fileName - nazwa pliku.
	 */
	void putFile( const QByteArray & data, const QString & file ) {
		mFtpExt->putFile( data, file );
	}

	/** Funkcja uruchamia procedur� przerwania bie��cej operacji.\n
	 UWAGA! Narazie wywo�ywany jest tu @see slotDone oraz ukrywany jest dialog
	 post�pu wykonywania operacji.
	 */
	void breakOperation() {
		mFtpExt->breakCurrentOperation();
	}

	/** Metoda zwraca informacj� o tym czy operacja si� ju� zako�czy�a.\n
	 @return TRUE, je�li operacja si� zako�czy�a w przeciwnym razie FALSE.
	 */
	bool operationHasFinished() const { return mOperationHasFinished; }

private:
	QFtpExt *mFtpExt;

	QString mCurrentPath;
	UrlInfoExt mNewUrlInfo;
	bool mOperationHasFinished;
	QFtpExt::Command mLastFinishedCmd;


	// Funkcja TYMCZASOWA, zwraca nazwe podanego kodu polecenia
	QString cmdName( int cmd );

	// Funkcja TYMCZASOWA, zwraca nazwe podanego kodu stanu polaczenia
	QString stateName( int state );

//public slots:

private slots:
	/** Slot wywo�ywany w momencie rozpocz�cia wykonywania polecenia.\n
	 @param cmd - kod polecenia (typu QFtp::Command).\n
	 Wykonywane s� tu czynno�ci przygotowuj�ce, niezb�dne dla danego polecenia.
	 */
	void slotCommandStarted( int command );

	/** Slot wywo�ywany w chwili zako�czenia wykonywania podanego polecenia.\n
	 @param command - kod polecenia ( typu @em Operation ),
	 @param finishedWithError - r�wne TRUE oznacza, �e pojawi� si� b��d
	 po wykonanym poleceniu @em command .\n
	 Je�li pr�ba wykonania polecenia zako�czy�a si� b��dem pokazywany jest
	 komunikat z informacj� o b��dzie i nazw� pliku lub katalogu, kt�rego b��d
	 dotyczy�.
	 */
	void slotCommandFinished( int command, bool finishedWithError );

	/** Pokazywane sa tutaj bledy dla ostatnio wykonywanej czynnosci.
	 @param finishedWithError - TRUE, oznacza �e operacja zako�czy�a si� b��dem
	  (do odczytania przez @em errorCode() ), FALSE mowi �e wszystko posz�o OK.
	*/
	void slotDone( bool finishedWithError );

	/** Slot powoduje wy�wietlenie informacje o stanie bie��cej operacji na pasku
	 statusu.\n
	 @param state - status operacji (typu QFtp::State).\n
	 Wysy�any jest tutaj tylko sygna� @em signalSetInfo() .
	 */
	void slotStateChanged( int state );

};

#endif
