/****************************************************************************
** $Id: qt/qftp.h   3.3.3   edited May 27 2003 $
**
** Definition of QFtpExt class.
**
** Created : 970521
**
** Copyright (C) 1997-2000 Trolltech AS.  All rights reserved.
**
** This file is part of the network module of the Qt GUI Toolkit.
**
** This file may be distributed under the terms of the Q Public License
** as defined by Trolltech AS of Norway and appearing in the file
** LICENSE.QPL included in the packaging of this file.
**
** This file may be distributed and/or modified under the terms of the
** GNU General Public License version 2 as published by the Free Software
** Foundation and appearing in the file LICENSE.GPL included in the
** packaging of this file.
**
** Licensees holding valid Qt Enterprise Edition licenses may use this
** file in accordance with the Qt Commercial License Agreement provided
** with the Software.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.trolltech.com/pricing.html or email sales@trolltech.com for
**   information about Qt Commercial License Agreements.
** See http://www.trolltech.com/qpl/ for QPL licensing information.
** See http://www.trolltech.com/gpl/ for GPL licensing information.
**
** Contact info@trolltech.com if any conditions of this licensing are
** not clear to you.
**
**********************************************************************/

#ifndef QFTP_H
#define QFTP_H

#include "vfs.h" // for Error type and URL
#include "urlinfoext.h"

#ifndef QT_H
#include <qstring.h> // char*->QString conversion
#include <q3socket.h> // NNN
//#include <qurlinfo.h>
#include <q3networkprotocol.h>
#endif // QT_H

#if !defined( QT_MODULE_NETWORK ) || defined( QT_LICENSE_PROFESSIONAL ) || defined( QT_INTERNAL_NETWORK )
#define QM_EXPORT_FTP
#else
#define QM_EXPORT_FTP Q_EXPORT
#endif

#ifndef QT_NO_NETWORKPROTOCOL_FTP


class Q3Socket;
class QFtpCommandExt;

class QFtpExt : public Q3NetworkProtocol
{
    Q_OBJECT

public:
    QFtpExt(); // ### Qt 4.0: get rid of this overload
    QFtpExt( QObject *parent, const char *name=0 );
    virtual ~QFtpExt();

    int supportedOperations() const;

    // non-QNetworkProtocol functions:
    enum State {
	Unconnected,
	HostLookup,
	Connecting,
	Connected,
	LoggedIn,
	Closing
    };
    enum Error {
	NoError,
	UnknownError,
	HostNotFound,
	ConnectionRefused,
	NotConnected
    };
    enum Command {
	None,
	ConnectToHost,
	Login,
	Close,
	List,
	Cd,
	Get,
	Put,
	Remove,
	Mkdir,
	Rmdir,
	Rename,
	RawCommand
    };

	int connectToHost( const QString &host, Q_UINT16 port=21 );
	int login( const QString &user=QString::null, const QString &password=QString::null );
	int close();
	int list( const QString &dir=QString::null );
	int cd( const QString &dir );
	int get( const QString &file, QIODevice *dev=0 );
	int put( const QByteArray &data, const QString &file );
	int put( QIODevice *dev, const QString &file );
	int remove( const QString &file );
	int mkdir( const QString &dir );
	int rmdir( const QString &dir );
	int rename( const QString &oldname, const QString &newname );

	int rawCommand( const QString &command );

	Q_ULONG bytesAvailable() const;
	Q_LONG readBlock( char *data, Q_ULONG maxlen );
	QByteArray readAll();

	int currentId() const;
	QIODevice* currentDevice() const;
	Command currentCommand() const;
	bool hasPendingCommands() const;
	void clearPendingCommands();

	State state() const;

	Error error() const;
	QString errorString() const;

		// NNN -----
	void initSession( const URL & url ) { /* TODO implement me */ }
	QString host() const { return ""; /*d->mHostName;*/ }
	QString directory() const { return ""; }
	uint    port() const { return 0; }
	QString userName() const { return ""; }
	QString nameOfProcessedFile() const { return ""; }
	VFS::Error  errorCode() const;

	void    putFile( const QByteArray & data, const QString & file ) { /* TODO implement me */ }
	void    breakCurrentOperation()  { /* TODO implement me */ }
	void    getWeighingStat( long long & weigh, uint & files, uint & dirs ) { /* TODO implement me */ }
	void    touch( const QString & fileName ) { /* TODO implement me */ }
	void    weigh( const QStringList & filesList ) { /* TODO implement me */ }
	// NNN____

public slots:
    void abort();

signals:
    void stateChanged( int );
    void listInfo( const UrlInfoExt & );
    void readyRead();
    void dataTransferProgress( int, int );
    void rawCommandReply( int, const QString& );

    void commandStarted( int );
    void commandFinished( int, bool );
    void done( bool );

protected:
    void parseDir( const QString &buffer, QUrlInfo &info ); // ### Qt 4.0: delete this? (not public API)
    void operationListChildren( Q3NetworkOperation *op );
    void operationMkDir( Q3NetworkOperation *op );
    void operationRemove( Q3NetworkOperation *op );
    void operationRename( Q3NetworkOperation *op );
    void operationGet( Q3NetworkOperation *op );
    void operationPut( Q3NetworkOperation *op );

    // ### Qt 4.0: delete these
    // unused variables:
    Q3Socket *commandSocket, *dataSocket;
    bool connectionReady, passiveMode;
    int getTotalSize, getDoneSize;
    bool startGetOnFail;
    int putToWrite, putWritten;
    bool errorInListChildren;

private:
    void init();
    int addCommand( QFtpCommandExt * );

    bool checkConnection( Q3NetworkOperation *op );

private slots:
    void startNextCommand();
    void piFinished( const QString& );
    void piError( int, const QString& );
    void piConnectState( int );
    void piFtpReply( int, const QString& );

private slots:
    void npListInfo( const UrlInfoExt & );
    void npDone( bool );
    void npStateChanged( int );
    void npDataTransferProgress( int, int );
    void npReadyRead();

protected slots:
    // ### Qt 4.0: delete these
    void hostFound();
    void connected();
    void closed();
    void dataHostFound();
    void dataConnected();
    void dataClosed();
    void dataReadyRead();
    void dataBytesWritten( int nbytes );
    void error( int );
};

class QFtp_PI;

class QFtp_DTP : public QObject
{
    Q_OBJECT

public:
    enum ConnectState {
	CsHostFound,
	CsConnected,
	CsClosed,
	CsHostNotFound,
	CsConnectionRefused
    };

	QFtp_DTP( QFtp_PI *p, QObject *parent=0, const char *name=0 );
	virtual ~QFtp_DTP() {}

    void setData( QByteArray * );
    void setDevice( QIODevice * );
    void writeData();

    void setBytesTotal( int bytes )
    {
	bytesTotal = bytes;
	bytesDone = 0;
	emit dataTransferProgress( bytesDone, bytesTotal );
    }

    bool hasError() const;
    QString errorMessage() const;
    void clearError();

    void connectToHost( const QString & host, Q_UINT16 port )
    { socket.connectToHost( host, port ); }

    Q3Socket::State socketState() const
    { return socket.state(); }

    Q_ULONG bytesAvailable() const
    { return socket.bytesAvailable(); }

    Q_LONG readBlock( char *data, Q_ULONG maxlen )
    {
	Q_LONG read = socket.readBlock( data, maxlen );
	bytesDone += read;
	return read;
    }

    QByteArray readAll()
    {
	QByteArray tmp = socket.readAll();
	bytesDone += tmp.size();
	return tmp;
    }

    void abortConnection();

    static bool parseDir( const QString &buffer, const QString &userName, QUrlInfo *info );

signals:
    void listInfo( const UrlInfoExt & );
    void readyRead();
    void dataTransferProgress( int, int );

    void connectState( int );

private slots:
    void socketConnected();
    void socketReadyRead();
    void socketError( int );
    void socketConnectionClosed();
    void socketBytesWritten( int );

private:
    void clearData()
    {
	is_ba = FALSE;
	data.dev = 0;
    }

    Q3Socket socket;
    QFtp_PI *pi;
    QString err;
    int bytesDone;
    int bytesTotal;
    bool callWriteData;

    // If is_ba is TRUE, ba is used; ba is never 0.
    // Otherwise dev is used; dev can be 0 or not.
    union {
	QByteArray *ba;
	QIODevice *dev;
    } data;
    bool is_ba;
};

class QFtp_PI : public QObject
{
    Q_OBJECT

public:
    QFtp_PI( QObject *parent = 0 );
		virtual ~QFtp_PI() {}

    void connectToHost( const QString &host, Q_UINT16 port );

    bool sendCommands( const QStringList &cmds );
    bool sendCommand( const QString &cmd )
    { return sendCommands( QStringList( cmd ) ); }

    void clearPendingCommands();
    void abort();

    QString currentCommand() const
    { return currentCmd; }

    bool rawCommand;

    QFtp_DTP dtp; // the PI has a DTP which is not the design of RFC 959, but it
		 // makes the design simpler this way
signals:
    void connectState( int );
    void finished( const QString& );
    void error( int, const QString& );
    void rawFtpReply( int, const QString& );

private slots:
    void hostFound();
    void connected();
    void connectionClosed();
    void delayedCloseFinished();
    void readyRead();
    void error( int );

    void dtpConnectState( int );

private:
    // the states are modelled after the generalized state diagram of RFC 959,
    // page 58
    enum State {
	Begin,
	Idle,
	Waiting,
	Success,
	Failure
    };

    enum AbortState {
	None,
	AbortStarted,
	WaitForAbortToFinish
    };

    bool processReply();
    bool startNextCmd();

    Q3Socket commandSocket;
    QString replyText;
    char replyCode[3];
    State state;
    AbortState abortState;
    QStringList pendingCommands;
    QString currentCmd;

    bool waitForDtpToConnect;
    bool waitForDtpToClose;
};



#endif // QT_NO_NETWORKPROTOCOL_FTP

#endif // QFTP_H
