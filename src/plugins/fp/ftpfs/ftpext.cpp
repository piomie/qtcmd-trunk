/***************************************************************************
                          ftpext.cpp  -  description
                             -------------------
    begin                : Thu Sep 23 2004
    copyright            : (C) 2004 by Piotr Mierzwiński
    email                : peterm@go2.pl

    copyright            : See COPYING file that comes with this project
 ***************************************************************************/

#include "ftpext.h"

#include <qtimer.h>

#include <netdb.h> // for gethostbyname()
extern int h_errno;

// TODO listowanie: ominac linki, wylistowac pliki i kat., asynchronicznie spr.linki i je wstawiac


FtpExt::FtpExt( QObject *parent, const char *name )
	: QFtpExt(parent, name)
{
	mReadFile     = FALSE;
	mGettingURL   = FALSE;
	mLinkChecking = FALSE;

	connect( this, SIGNAL(rawCommandReply(int, const QString &)),
	 this, SLOT(slotRawCommandReply(int, const QString &)) );
}


void FtpExt::list( const QString & inputURL )
{
	qDebug("FtpExt::list");
	if ( ! inputURL.isEmpty() ) {
		if ( inputURL.at(0) != '/' ) // full URL
			initSessionParameters( inputURL );
		else
			mRemoteDir = inputURL;
	}
	// --- connecting to host and listing remote directory
	if ( state() == QFtpExt::Unconnected ) {
		qDebug("__ try CONNECT, host=%s, remoteDir=%s, port=%d, user=%s, pass=%s",
			mHostName.latin1(), mRemoteDir.latin1(), mPortNum, mUserName.latin1(), mPassword.latin1());
		if ( mHostName.isEmpty() ) {
			qDebug("__ Host name is empty !");
			return;
		}
		struct hostent *hostEntries = ::gethostbyname( mHostName );
		if ( hostEntries ) {
			QFtpExt::connectToHost( hostEntries->h_name, mPortNum );
			//QFtpExt::rawCommand( QString("USER ")+mUserName+"\r\n" + QString("PASS ")+mPassword+"\r\n" ); // login
			QFtpExt::login( mUserName, mPassword ); // jesli logowanie wywolane zostanie ze slotDone to polecenie to nie zostanie rozpoczete, wywolany bedzie tylko slot zakanczajacy to polecenie !
// 			QFtpExt::cd( mRemoteDir ); // wywoluje Close, dlaczego ???
// 			QFtpExt::rawCommand( "PWD" );
// 			QFtpExt::list();
				// run listing
// 			QFtpExt::rawCommand( "TYPE A" );
// 			QFtpExt::rawCommand( "PASV" );
// 			QFtpExt::rawCommand( "STAT "+mRemoteDir );
// 			emit commandStarted( QFtpExt::List );
		}
		else // an error occured
			emit commandFinished( QFtpExt::ConnectToHost, TRUE );
	}
	else { // change dir and if no error then init listing
		qDebug("__ cd %s", mRemoteDir.latin1() );
// 		emit commandStarted( QFtpExt::Cd );
// 		QFtpExt::rawCommand( "CWD "+mRemoteDir );
		QFtpExt::cd( mRemoteDir );
		//QFtpExt::rawCommand( "PWD" );
// 		QFtpExt::list();
				// run listing
		QFtpExt::rawCommand( "TYPE A" );
		QFtpExt::rawCommand( "PASV" );
		QFtpExt::rawCommand( "STAT "+mRemoteDir );
// 		QFtpExt::rawCommand( "PWD" );
		emit commandStarted( QFtpExt::List );
	}
}

void FtpExt::loginUser()
{
	//QFtpExt::login( mUserName, mPassword );
	//QFtpExt::rawCommand( QString("USER ")+mUserName+"\r\n" + QString("PASS ")+mPassword+"\r\n" ); // login
}

void FtpExt::makeDir( const QString & dirName )
{
	mkdir( dirName );
	qDebug("not yet implement !");
}


void FtpExt::touch( const QString & /*fileName*/ )
{
	// utw.pusty plik w kat.domowym i przeniesc go na serwer
	qDebug("not yet implement !");
}


void FtpExt::remove( const QString & fileName )
{
	QFtpExt::remove( fileName );
	qDebug("not yet implement !"); // dodaje kolejki polecenie usuwania
}


void FtpExt::weigh( const QString & /*fileName*/ )
{
	qDebug("not yet implement !"); // dodaje kolejki polecenie wazenia
}


void putFile( const QByteArray & /*data*/, const QString & /*file*/ )
{
	qDebug("not yet implement !");
}


void FtpExt::breakCurrentOperation()
{
	qDebug("not yet implement !");
}


void FtpExt::initSessionParameters( const QString & inputURL )
{
	qDebug("FtpExt::initSessionParameters, inputURL=%s", inputURL.latin1() );
	if ( inputURL.find("ftp://") != 0 )
		return;
	// default settings
	mUserName = mOldUserName;
	mPassword = mOldPassword;
	mPortNum  = mOldPortNum;

	mRemoteDir = inputURL;
	mRemoteDir = mRemoteDir.remove( "ftp://" );

	int monkeyPos = mRemoteDir.find('@'), colonPos;
	if ( monkeyPos != -1 ) {
		mUserName = mRemoteDir.left( monkeyPos ); // cut user name (and maybe password)
		colonPos  = mUserName.find(':');
		if ( colonPos != -1 ) {
			mPassword = mUserName.right( mUserName.length()-colonPos-1 );
			mUserName = mUserName.left( colonPos );
		}
		mRemoteDir = mRemoteDir.right( mRemoteDir.length()-monkeyPos-1 ); // rest (without '@') save into member mRemoteDir
	}
	mHostName = mRemoteDir.left( mRemoteDir.find('/') );

	int portPos = mRemoteDir.find(':');
	if ( portPos != -1 ) {
		mPortNum = QString(mHostName.right(mHostName.length()-portPos-1)).toInt();
		mHostName = mHostName.left( portPos ); // rest (without 'portNum:') save into member mHostName
	}
	mRemoteDir = mRemoteDir.right( mRemoteDir.length()-mRemoteDir.find('/') );
	//qDebug(" >> mHostName=%s, mRemoteDir=%s, mUserName=%s, port=%d", mHostName.latin1(), mRemoteDir.latin1(), mUserName.latin1(), mPortNum );
}


int FtpExt::nameIndex( const QString & name )
{
	uint i = 1, sepCount = 0;

	while ( sepCount < 8 && name[i] != ' ' && i<name.length() ) {
		i++;
		if ( name[i]==' ' ) {
			sepCount++;
			while (name[i]==' ') i++;
		}
	}

	return i;
}


void FtpExt::checkAllLinks()
{
	mGettingURL = TRUE;
	mLinkCheckingCounter = 0;
	QStringList itemList;

	for ( uint i=0; i<mTargetFileForLinkLst.count(); i++ ) {
		itemList = QStringList::split( '\n', mTargetFileForLinkLst[i] );
		//mNameOfProcessedFile = itemList[1]; // NN TEST
		//QTimer::singleShot( 0, this, SLOT(slotRunLinkChecking()) ); // NN TEST
//		QFtpExt::rawCommand( "SIZE "+(mNameOfProcessedFile=itemList[1]) ); // need to check all target files
		//emit rawCommand( "SIZE "+(mNameOfProcessedFile=itemList[1]) ); // need to check all target files
		emit rawCommand( "STAT "+(mNameOfProcessedFile=itemList[1]) ); // need to check all target files
	}
}

void FtpExt::slotRunLinkChecking() // TEST
{
//		QFtpExt::rawCommand( "SIZE "+(mNameOfProcessedFile=itemList[1]) ); // need to check all target files
//	emit rawCommand( "SIZE "+mNameOfProcessedFile ); // need to check all target files
}

void FtpExt::slotRawCommandReply( int replyCode, const QString & detail )
{
	qDebug("FtpExt::slotRawCommandReply, replyCode=%d", replyCode );
	if ( replyCode != 211 )
		qDebug("__ detail=%s", detail.latin1() );
	bool runListingFilesAgain = FALSE;

	if ( replyCode == 257 ) // PWD
		mRemoteDir = detail.section( '"', 1, 1 );
	else
	if ( replyCode == 250 ) // CWD
		emit commandFinished( QFtpExt::Cd, ! (detail.find("successful")>0) );
	else
	if ( replyCode == 213 ) { // SIZE
		if ( mLinkChecking ) { // DO_SPRAWDZENIA
			mTargetFileForLinkLst[mLinkCheckingCounter] = mTargetFileForLinkLst[mLinkCheckingCounter]+"\nfile+";
			mLinkCheckingCounter++;
			if ( mLinkCheckingCounter == mTargetFileForLinkLst.count() ) {
				mGettingURL = FALSE;
				mLinkChecking = FALSE;
				runListingFilesAgain = TRUE;
			}
		}
	}
	else
	if ( replyCode == 550 ) { // error
		if ( mLinkChecking ) { // DO_SPRAWDZENIA
			QString linkItem = mTargetFileForLinkLst[mLinkCheckingCounter];
			QString fName = linkItem.section('\n',1,1);
			if ( detail.find(linkItem.section('\n',1,1)+": not a regular file") != -1 ) // link to dir
				linkItem += "\ndir";
			else // pub: not a regular file
			if ( detail.find( linkItem.section('\n',1,1)+": No such file") != -1 ) // link to ? == hang link
				linkItem += "\nfile-";
			mTargetFileForLinkLst[mLinkCheckingCounter] = linkItem;
			mLinkCheckingCounter++;
			if ( mLinkCheckingCounter == mTargetFileForLinkLst.count() ) {
				mGettingURL = FALSE;
				mLinkChecking = FALSE;
				runListingFilesAgain = TRUE;
			}
		}
	}
	else
	// STAT for listing and check created new dir
	if ( replyCode == 211 ) { // parse output for STAT command (for new dir), it's need to make new UrlInfoExt obj.
		QStringList filesList = QStringList::split( "\r\n", detail );
		if ( ! mGettingURL && ! detail.isEmpty() ) { // if listing
			mMainFilesList = filesList;
			mMainFilesList[mMainFilesList.count()-1] = "."; // overwrite line "End Of Status"
		}
		const int count = (mGettingURL) ? filesList.size() : mMainFilesList.size();
		if ( count < 3 ) { // file not exists
			mErrorCode = VFS::NotExists;
			if ( mLinkChecking ) {
				// not existing target file/dir is always a file
				mTargetFileForLinkLst[mLinkCheckingCounter] = mTargetFileForLinkLst[mLinkCheckingCounter] + "\nfile";
				if ( mLinkCheckingCounter == mTargetFileForLinkLst.count() ) {
					mGettingURL = FALSE;
					mLinkChecking = FALSE;
					runListingFilesAgain = TRUE;
				}
				else
					mLinkCheckingCounter++;
			}
			emit commandFinished( QFtpExt::List, TRUE );
			return;
		}
		QString name, targetLink;
		QStringList itemList;
		// --- collect all links
		if ( mTargetFileForLinkLst.count() == 0 ) {
			for ( uint i=1; i<mMainFilesList.count(); i++ ) {
				if ( mMainFilesList[i].at(1) == 'l' ) {
					name = mMainFilesList[i].mid( 56, mMainFilesList[i].length()-56 ); // 56 it's begin position of name (server: proftp)
					itemList = QStringList::split( " -> ", name );
					mTargetFileForLinkLst.append( itemList[0]+"\n"+itemList[1] ); // name + target file
				}
			}
			// and init checking all links
			if ( mTargetFileForLinkLst.count() > 0 ) {
				mLinkChecking = TRUE;
//				checkAllLinks(); // that contains in mTargetFileForLinkLst
				mGettingURL = TRUE;
				mLinkCheckingCounter = 0;
				QStringList itemList;

				for ( uint i=0; i<mTargetFileForLinkLst.count(); i++ ) {
					itemList = QStringList::split( '\n', mTargetFileForLinkLst[i] );
					//mNameOfProcessedFile = itemList[1]; // NN TEST

					// np.dla SIZE, Qt::Ftp, po poleceniu zakonczonym bledem, wyw. jest slotDone
//					emit rawCommand( "SIZE "+(mNameOfProcessedFile=itemList[1]) ); // need to check all target files
					emit rawCommand( "STAT "+(mNameOfProcessedFile=itemList[1]) ); // need to check all target files
				}
				return;
			}
		}

		UrlInfoExt uie;
		bool isDir, isReadable, isExecutable;
		long long size;
		QTime time;
		QDate date;
		QDateTime dt;
		uint nameId;
		const int futureTolerance = 600; // QFtpExt part
		const int counter = (mGettingURL) ? 3 : mMainFilesList.count();

		for ( int i=1; i<counter; i++ ) {
			// get name and target file for link
			name = (mGettingURL) ? filesList[i] : mMainFilesList[i];
			nameId = nameIndex( name );
			name = name.mid( nameId, name.length()-nameId );
			itemList = QStringList::split( " -> ", name ); // jesli separatora nie bedzie w ciagu to w wyniku znajdzie sie ciag
			targetLink = itemList[1];
			name = itemList[0];

			if ( name == "." || name.isEmpty() || (! mGettingURL && name == "..") )
				continue;

			itemList = QStringList::split( " ", (mGettingURL) ? filesList[i] : mMainFilesList[i] );

			uie.setPermissionsStr( itemList[0].right(9) ); // conversion to int (result in the 'uie' obj.)

			isDir = (itemList[0].at(0) == 'd');
			// --- check whether file is readable and executable
			if ( itemList[2] == mUserName ) {
				isReadable = (itemList[0].at(1) == 'r'); // user can to read
				isExecutable = (itemList[0].at(3) == 'x'); // user can to execute
			}
			else {
				isReadable = (itemList[0].at(7) == 'r'); // other can to read
				isExecutable = (itemList[0].at(9) == 'x'); // other can to execute
			}
			// --- get size
			size = itemList[4].toLongLong();

		// --- this part is come from the QFtpExt class (Qt-3.2.3) (little modified by author)
			// --- set date and time
			QString dateStr = "Sun "+itemList[ 5 ]+" "+itemList[ 6 ]+" ";

			if ( itemList[ 7 ].contains( ":" ) ) {
				time = QTime( itemList[ 7 ].left( 2 ).toInt(), itemList[ 7 ].right( 2 ).toInt() );
				dateStr += QString::number( QDate::currentDate().year() );
			} else {
				dateStr += itemList[ 7 ];
			}

			date = QDate::fromString( dateStr );
			uie.setLastModified( QDateTime( date, time ) );

			if ( itemList[ 7 ].contains( ":" ) ) {
				if( uie.lastModified().secsTo( QDateTime::currentDateTime() ) < -futureTolerance ) {
					QDateTime dt = uie.lastModified();
					QDate d = dt.date();
					d.setYMD(d.year()-1, d.month(), d.day());
					dt.setDate(d);
					uie.setLastModified(dt);
				}
			}
		// ---
			if ( mGettingURL ) // get file name from first line
				name = filesList[0].mid( 10, filesList[0].length()-(1+10) ); // 14 - begin pos.of name in first line
			else // is listing, check current link
			if ( mTargetFileForLinkLst.count() > 0 && itemList[0].at(0) == 'l' ) {
				QStringList itemLst;
				for ( uint i=0; i<mTargetFileForLinkLst.count(); i++ ) {
					itemLst = QStringList::split( '\n', mTargetFileForLinkLst[i] );
					if ( name == itemLst[0] ) {
						isDir = (itemLst[2] == "dir" );
						break;
					}
				}
			}
			mNewUrlInfo = UrlInfoExt(
			 name, size, uie.permissions(),
			 itemList[2], itemList[3], uie.lastModified(), uie.lastModified(),
			 isDir, !isDir, (itemList[0].at(0) == 'l'),
			 isReadable, isExecutable, (size == 48), targetLink
			);

			if ( mGettingURL ) {
				if ( mReadFile )
					mFileIsReadable = isReadable;
				else
				if ( mLinkChecking ) {
					QString linkItem = mTargetFileForLinkLst[mLinkCheckingCounter];
					linkItem += (isDir) ? "\ndir" : "\nfile";
					mTargetFileForLinkLst[mLinkCheckingCounter] = linkItem;
					mLinkCheckingCounter++;
					if ( mLinkCheckingCounter == mTargetFileForLinkLst.count() ) {
						mGettingURL = FALSE;
						mLinkChecking = FALSE;
						runListingFilesAgain = TRUE;
					}
				}
				break;
			}
			else
				emit listInfo( mNewUrlInfo );
		} // for
	} // replyCode == 211
	// replyCode == 213 - SIZE command
	if ( runListingFilesAgain ) {
		slotRawCommandReply( 211, QString::null );
		return;
	}
	if ( ! mGettingURL && ! mLinkChecking && replyCode != 200 )
		emit commandFinished( QFtpExt::List, FALSE );
}

// polecenia FTP:
//status: STAT\r\n
//list: TYPE A\r\n, PASV\r\n, LIST\r\n  "LIST " + dir + "\r\n"
//cd :  CWD "+dir+"\r\n
//login: "USER anonymous\r\n" + PASS password + "\r\n"
//get:  "SIZE " + file + "\r\n, "TYPE I\r\n, PASV\r\n, "RETR " + file + "\r\n"
//put:  "TYPE I\r\n", "PASV\r\n", "ALLO "+rozmiar_bloku lub "STOR " + file + "\r\n"
//remove: "DELE "+file+"\r\n"
//mkdir:  "MKD "+dir+"\r\n"
//quit:  QUIT
//chmod: SITE CHMOD 0700 nazwa_pliku
