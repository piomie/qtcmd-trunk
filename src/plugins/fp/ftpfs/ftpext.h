/***************************************************************************
                          ftpext.h  -  description
                             -------------------
    begin                : Thu Sep 23 2004
    copyright            : (C) 2004 by Piotr Mierzwi�ski
    email                : peterm@go2.pl

    copyright            : See COPYING file that comes with this project
 ***************************************************************************/

#ifndef _FTPEXT_H_
#define _FTPEXT_H_

#include <qptrstack.h>
#include <qftp.h>

#include "vfs.h" // for Error type

/**
 * Author:
 * Piotr Mierzwi�ski
 **/
/** @short Klasa obs�uguj�ca protok� FTP.
 */
class FtpExt : public QFtp
{
	Q_OBJECT
public:
	FtpExt( QObject *parent, const char *name=0 );
	virtual ~FtpExt() {}


	/** Zwraca nazw� bie��cego hosta bez przyrostka 'ftp://'.\n
	 @return nazwa hosta.
	 */
	QString hostName() const { return mHostName; }

	/** Zwraca nazw� bie��cego katalogu bez nazwy hosta i przyrostka 'ftp://'.\n
	 @return zdalny katalog.
	 */
	QString remoteDir() const { return mRemoteDir; }

	/** Zwraca nazw� zalogowanego na serwer u�ytkownika.\n
	 @return nazwa u�ytkownika.
	 */
	QString userName() const { return mUserName; }

	/** Zwraca numer portu.\n
	 @return numer portu.
	 */
	int portNum() const { return mPortNum; }

	/** Funkcja uruchamia logowanie na ustawionego u�ytkownika.
	 */
	void loginUser();

	/** Pobierac� wyniki operacji wa�enia, tj. waga ca�o�ci, ilo�� plik�w
	 i katalog�w.\n
	 @param weight - zmienna, w kt�rej zapisana b�dzie waga,
	 @param files - zmienna do zapisania ilo�ci plik�w,
	 @param dirs - zmienna do zapisania ilo�ci katalog�w.\n
	 */
	void getWeighingStat( long long & /*weigh*/, uint & /*files*/, uint & /*dirs*/ ) { }

	/** Zwraca nazw� (wraz ze �cie�k� absolutn�) aktualnie przetwarzanego pliku.\n
	 */
	QString nameOfProcessedFile() const { return mNameOfProcessedFile; }

	/** Zwraca kod b��du, kt�ry si� ostatnio pojawi�.\n
	 @param errorCode - kod b��du.
	 */
	VFS::Error errorCode() const { return mErrorCode; }


	/** Metoda uruchamia procedure listowania z podanego URLa.\n
	 @param inputURL - URL do odczytania.
	 */
	void list( const QString & inputURL=QString::null );

	/**
	 */
	void makeDir( const QString & dirName );

	/**
	 */
	void touch( const QString & fileName );

	/**
	 */
	void remove( const QString & fileName );

	/**
	 */
	void weigh( const QString & fileName );

	/** Zapisuje zawarto�� podanego bufora do podanego pliku.\n
	 @param buffer - adres bufora,
	 @param fileName - nazwa pliku.
	 */
	void putFile( const QByteArray & data, const QString & file );

	/**
	 */
	void breakCurrentOperation();

private:
	QString mHostName, mRemoteDir, mUserName, mPassword;
	QString mOldHost, mOldRemoteDir, mOldUserName, mOldPassword;
	int mPortNum, mOldPortNum;

	QString mNameOfProcessedFile;
	VFS::Error mErrorCode;

	bool mGettingURL;
	bool mLinkChecking;
	bool mReadFile, mFileIsReadable;
	QStringList mMainFilesList, mTargetFileForLinkLst;
	uint mLinkCheckingCounter;
	UrlInfoExt mNewUrlInfo;

	/** Inicjowane s� tutaj parametry sesji (host, zdalny katalog, nazwa
	 u�ytkownika i has�o).\n
	 @param inputURL - URL wykorzystywany do inicjowania.
	 */
	void initSessionParameters( const QString & inputURL );


	/** Metoda zwraca pocz�tkow� pozycj� nazwy w listowanym elemencie.\n
	 @param name - nazwa do sprawdzenia.\n
	 @return indeks nazwy.
	 */
	int nameIndex( const QString & name );

	/** Metoda uruchamia "surowe polecenie" sprawdzenia, ka�dego pliku docelowego
	 dla link�w z listy zebranej podczas listowania plik�w.
	 ( @em mTargetFileForLinkLst ).\n
	 Wynik dzia�ania polecenia nale�y sprawdza� w @see slotRawCommandReply.
	*/
	void checkAllLinks();

private slots:
	/** W slocie parsowane s� polecenia uruchaniane "r�cznie" na ho�cie FTP.\n
	 @param code - kod polecenia
	 @param text - wynik wykonania polecenia
	 */
	void slotRawCommandReply( int code, const QString & text );

	void slotRunLinkChecking();

signals:
	void listInfo( const UrlInfoExt & urlInfoExt );

};

#endif
