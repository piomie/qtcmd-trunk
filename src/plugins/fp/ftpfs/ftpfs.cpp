/***************************************************************************
                          ftpfs.cpp  -  description
                             -------------------
    begin                : sat nov 30 2002
    copyright            : (C) 2002 by Piotr Mierzwiński
    email                : peterm@go2.pl

    copyright            : See COPYING file that comes with this project
 ***************************************************************************/

#include "ftpfs.h"

#include <qapplication.h>


FtpFS::FtpFS( QWidget *parent, const char *name )
	: VFS(parent, name)
{
	qDebug("FtpFS::FtpFS");
	mOperationHasFinished = TRUE;
	mLastFinishedCmd = QFtpExt::None;

	mFtpExt = new QFtpExt( this, name );

	connect( mFtpExt, SIGNAL(done(bool)),                 SLOT(slotDone(bool)) );
	connect( mFtpExt, SIGNAL(stateChanged(int)),          SLOT(slotStateChanged(int)) );
	connect( mFtpExt, SIGNAL(commandStarted(int)),        SLOT(slotCommandStarted(int)) );
	connect( mFtpExt, SIGNAL(commandFinished(int,bool)),  SLOT(slotCommandFinished(int,bool)) );

	connect( mFtpExt, SIGNAL(listInfo(const UrlInfoExt &)), this, SIGNAL(signalListInfo(const UrlInfoExt &)) );
}


FtpFS::~FtpFS()
{
	delete mFtpExt;  mFtpExt = 0;
}

bool FtpFS::isReadable( const QString & /*fileName*/, bool /*silent*/, bool /*sendSignal*/ )
{
	qDebug("not yet implement !");
	return FALSE;
}

	// ------ FILE OPERATIONS -----

void FtpFS::readDir( const URL & inputURL )
{
	qDebug("FtpFS::readDir, inputURL='%s'", inputURL.url().toLatin1().data() );
	if (mFtpExt->state() == QFtpExt::Unconnected) {
//		mFtpExt->initSession(inputURL);
		mFtpExt->connectToHost( inputURL.host(), inputURL.port() );
		mFtpExt->login( inputURL.user(), inputURL.password() );
	}
	mFtpExt->list( inputURL.path() );
	// spr. kolejke
}


void FtpFS::mkDir( const QString & dirName )
{
	mFtpExt->mkdir( dirName );
}


void FtpFS::mkFile( const QString & fileName )
{
	mFtpExt->touch( fileName );
}


void FtpFS::removeFiles( const QStringList & filesList )
{
	for (int i=0; i<filesList.count(); i++)
		mFtpExt->remove( filesList[i] );
}


void FtpFS::weighFiles( const QStringList & filesList, bool )
{
//	for (uint i=0; i<filesList.count(); i++)
//      mFtpExt->weigh( filesList[i] );
    mFtpExt->weigh( filesList );
}


void FtpFS::setAttrib( const QStringList & /*filesList*/, const UrlInfoExt & /*newUrlInfo*/, bool /*recursive*/, int /*changesFor*/ )
{
	qDebug("not yet implement !");
}


void FtpFS::find( const FindCriterion & /*findCriterion*/, bool /*stop*/ )
{
	qDebug("not yet implement !");
}

// ---- SLOTs -----

void FtpFS::slotCommandStarted( int command )
{
	qDebug("FtpFS::slotCommandStarted, command='%s', currentCommand='%s', state='%s'", cmdName(command).toLatin1().data(), cmdName(mFtpExt->currentCommand()).toLatin1().data(), stateName(mFtpExt->state()).toLatin1().data() );

	if ( command == QFtpExt::List ) {
//		QApplication::setOverrideCursor( waitCursor );
		emit signalPrepareForListing();
		emit signalSetInfo( tr("Getting the files list...") );
	}
}


void FtpFS::slotCommandFinished( int command, bool finishedWithError )
{
	qDebug("FtpFS::slotCommandFinished, command='%s', currentCommand='%s', state='%s'", cmdName(command).toLatin1().data(), cmdName(mFtpExt->currentCommand()).toLatin1().data(), stateName(mFtpExt->state()).toLatin1().data() );
	if ( finishedWithError ) // TEST
		qDebug("__ error ocurres, errorCode=%d, errorInfo=%s", mFtpExt->error(), mFtpExt->errorString().toLatin1().data() );
	// ------
	mLastFinishedCmd = (QFtpExt::Command)command; // used by 'slotDone'

/*	if ( ! finishedWithError ) {
		if ( command == FtpExt::Cd ) {
			QApplication::setOverrideCursor( waitCursor );
			emit signalPrepareForListing();
		}
	}*/
}


void FtpFS::slotDone( bool finishedWithError )
{
	qDebug("FtpFS::slotDone, currentCommand='%s', state='%s', LastFinishedCmd=%s", cmdName(mFtpExt->currentCommand()).toLatin1().data(), stateName(mFtpExt->state()).toLatin1().data(), cmdName(mLastFinishedCmd).toLatin1().data() );
	if ( finishedWithError ) // TEST
		qDebug("__ error ocurres, code=%d, info=%s", mFtpExt->error(), mFtpExt->errorString().toLatin1().data() );
	// ------
//	QApplication::restoreOverrideCursor();
	if ( ! finishedWithError ) {
/*		if ( mLastFinishedCmd == FtpExt::ConnectToHost ) // connected, try to login
			mFtpExt->loginUser();
		// po wyw.stad logowania, polecenie nie zostanie rozpoczete ! Wywolany bedzie tylko slot zakanczajacy je (poleceniem bedzie 'Close') ! pozniejsza proba zmiany kat.pokazuje blad ze nie polaczony
		else*/
		if ( mLastFinishedCmd == QFtpExt::Login ) // logged, try to list
			mFtpExt->list(); // zachowanie tak jak wyzej, z tym ze nast.polecenia sa wykonywane
	}

	if ( mLastFinishedCmd == QFtpExt::Mkdir ) // dziala tylko po polaczeniu sie, ale wyswietla "0,z 0"
		emit signalResultOperation( VFS::List, !finishedWithError );

	mLastFinishedCmd = (QFtpExt::Command)QFtpExt::None; // reset memeber
}


void FtpFS::slotStateChanged( int state )
{
	qDebug("FtpFS::slotStateChanged, state='%s'", stateName(state).toLatin1().data() );
	switch ( (QFtpExt::State)state ) {
		case QFtpExt::Unconnected:
			emit signalSetInfo( tr("Unconnected") );
			break;
		case QFtpExt::HostLookup:
			emit signalSetInfo( tr("Host lookup") );
			break;
		case QFtpExt::Connecting:
			emit signalSetInfo( tr("Connecting") );
			break;
		case QFtpExt::Connected:
			emit signalSetInfo( tr("Connected") );
			break;
		case QFtpExt::LoggedIn:
			emit signalSetInfo( tr("Logged in as")+" "+mFtpExt->userName() );
			break;
		case QFtpExt::Closing:
			emit signalSetInfo( tr("Closing") );
			break;
	}
}

// -------- TYMCZASOWA FUNKCJA DLA TESTOW File Systemu
QString FtpFS::cmdName( int cmd )
{
	QString s = "Unknown";

			 if ( cmd == QFtpExt::None )   s = "None";
	else if ( cmd == QFtpExt::ConnectToHost )  s = "Connect";
	else if ( cmd == QFtpExt::Login )  s = "Login";
	else if ( cmd == QFtpExt::Close )  s = "Close";
	else if ( cmd == QFtpExt::List )   s = "List";
	else if ( cmd == QFtpExt::Cd )     s = "Cd";
	else if ( cmd == QFtpExt::Rename ) s = "Rename";
	else if ( cmd == QFtpExt::Remove ) s = "Remove";
	else if ( cmd == QFtpExt::Mkdir )  s = "MkDir";
	else if ( cmd == QFtpExt::Rmdir )  s = "RmDir";
	else if ( cmd == QFtpExt::Get )    s = "Get";
	else if ( cmd == QFtpExt::Put )    s = "Put";
	else if ( cmd == QFtpExt::RawCommand ) s = "RawCommand";

	return s;
}

// -------- TYMCZASOWA FUNKCJA DLA TESTOW File Systemu
QString FtpFS::stateName( int state )
{
	if ( state == QFtpExt::Unconnected ) return QString("Unconnected");
	else
	if ( state == QFtpExt::HostLookup )  return QString("HostLookup");
	else
	if ( state == QFtpExt::Connecting )  return QString("Connecting");
	else
	if ( state == QFtpExt::Connected )   return QString("Connected");
	else
	if ( state == QFtpExt::LoggedIn )    return QString("LoggedIn");
	else
	if ( state == QFtpExt::Closing )     return QString("Closing");

	return QString("Unknown");
}
