# Plik utworzony przez menad?era projekt�w kdevelopa
# ------------------------------------------- 
# Podkatalog wzgl?dem g?�wnego katalogu projektu: ./src/plugins/fp/ftpfs
# Cel to biblioteka ftpfs

INSTALLS += target 
target.path = $$PREFIX/lib/qtcmd/plugins 
TARGETDEPS += ../../../../build/lib/libqtcmduiext.so \
              ../../../../build/lib/libqtcmdutils.so \
              ../../../../build/lib/libqtcmddlgext.so 
LIBS += -lqtcmduiext \
        -lqtcmdutils \
        -lqtcmddlgext 
INCLUDEPATH += ../../../../src \
               ../../../../src/libs/qtcmdutils \
               ../../../../src/libs/.ui \
	       ../../
MOC_DIR = ../../../../build/.tmp/moc 
OBJECTS_DIR = ../../../../build/.tmp/obj 
QMAKE_LIBDIR = ../../../../build/lib
QMAKE_CXXFLAGS_RELEASE += -O2 
QMAKE_CXXFLAGS_DEBUG += -g2 
TARGET = ftpfs 
DESTDIR = ../../../../build/lib/qtcmd/plugins 
CONFIG += warn_on \
          qt \
          thread \
          plugin 
TEMPLATE = lib 
HEADERS += ftpfs.h \
           qftpext.h \
	   ../../vfs.h
SOURCES = qftpext.cpp \
	  ftpfs.cpp \
          ftpfsplugin.cpp
           
#The following line was inserted by qt3to4
QT += network  qt3support 
