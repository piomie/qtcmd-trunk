/***************************************************************************
                          ftpfsplugin.cpp  -  description
                             -------------------
    begin                : mon sep 13 2004
    copyright            : (C) 2004 by Piotr Mierzwiński
    email                : peterm@go2.pl

    copyright            : See COPYING file that comes with this project
 ***************************************************************************/

#include "ftpfs.h"

// export symbol 'create_fs' for resolves it by 'QLibrary::resolve( const char * )'

extern "C" {
	QWidget* create_plugin(QWidget * parent)
	{
		return new FtpFS( parent );
	}
}
