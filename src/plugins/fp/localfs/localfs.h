/***************************************************************************
                          localfs.h  -  description
                             -------------------
    begin                : mon oct 28 2002
    copyright            : (C) 2004 by Piotr Mierzwiński
    email                : peterm@go2.pl

    copyright            : See COPYING file that comes with this project

 $Id$
 ***************************************************************************/

/** @file doc/api/src/localfs.dox */

#ifndef _LOCALFS_H_
#define _LOCALFS_H_

#include "vfs.h"
#include "lfs.h"

#include "systeminfo.h"
//class SystemInfo;

class LocalFS : public VFS
{
	Q_OBJECT
public:
	LocalFS( QWidget *pParent, const char *sz_pName=0 );
	~LocalFS();

	// ------ INFO ABOUT OPERATIONS -----

	QString host() const { return QString("/"); }
	QString path() const { return m_sCurrentPath; }
	QString nameOfProcessedFile() const { return m_pLFS->nameOfProcessedFile(); }
	Error errorCode() const { return m_pLFS->errorCode(); }
	bool isReadable( const QString & sFileName, bool silent=FALSE, bool sendSignal=FALSE );
	DirWritable dirWritable( const QString & sPath=QString::null ) const {
		return QFileInfo( sPath.isEmpty() ? m_sCurrentPath : sPath ).isWritable() ? ENABLEdw : DISABLEdw;
	}
	void getWeighingStat( long long & nWeigh, uint & nFiles, uint & nDirs ) {
		m_pLFS->getWeighingStat( nWeigh, nFiles, nDirs );
	}
	void getNewUrlInfo( UrlInfoExt & uiNewUrlInfo ) {
		uiNewUrlInfo = m_uiNewUrlInfo;
	}

	// ------ FILE OPERATIONS -----

	void readDir( const URL & url );
	void mkDir( const QString & sDirName );
	void mkFile( const QString & sFileName );
	void copyFiles( QString & sTargetPath, const QStringList & slFilesList, bool bSavePerm, bool bAlwaysOvr, bool bFollowByLink, bool bMove );
	void removeFiles( const QStringList & slFilesList );
	void weighFiles( const QStringList & slFilesList, bool bRealWeight );
	void mkLinks( const QStringList & slSourceFilesList, const QStringList & slTargetFilesList, bool bHard, bool bAlwaysOvr );
	void editLink( const QString & sFileName, const QString & sNewTargetPath );
	void setAttrib( const QStringList & slFilesList, const UrlInfoExt & uiNewUrlInfo, bool bRecursive, int nChangesFor );
	void find( const FindCriterion & findCriterion, bool bStop );
	void putFile( const QByteArray & baData, const QString & sFileName ) {
		if ( m_pLFS ) m_pLFS->putFile( baData, sFileName );
	}

	void get( const QString & sFileName, QIODevice * pDev = 0 );
	void put( QIODevice * pDev, const QString & sFileName );

	void breakOperation() {
		m_pLFS->breakCurrentOperation();
	}
	bool operationHasFinished() const { return m_bOperationHasFinished; }

private:
	LFS *m_pLFS;

	QString m_sCurrentPath;

	bool m_bOperationHasFinished;
	bool m_bSkipNextErrorOccures;
//	uint m_nOpFileCounter, m_nTotalFiles;
	UrlInfoExt m_uiNewUrlInfo;

	QFile m_File;
	bool m_bFileHasBeenOpened;

	bool m_bRemoveToTrash;
	bool m_bWeighBeforeOperation;
	QString m_sTrashDirectory;


	QString cmdString( Operation eOperation ); // TYLKO DLA TESTOW
	void initNewUrlInfo( const QString & sFileName=QString::null );

public slots:
	void slotPause( bool bStop );
	void slotGetUrlInfo( const QString & sFullFileName, UrlInfoExt & uiUrlInfoExt );
	void slotReadFileToView( const QString & sFileName, QByteArray & baBuffer, int & nBytesRead, int nFileLoadMethod );

private slots:
	void slotCommandStarted( int nCmd );
	void slotCommandFinished( int nCmd, bool bError );
	void slotDone( bool bError );
	void slotStateChanged( int nState );

};

#endif
