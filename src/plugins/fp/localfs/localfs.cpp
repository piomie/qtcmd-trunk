/***************************************************************************
                          localfs.cpp  -  description
                             -------------------
    begin                : Mon Oct 28 2002
    copyright            : (C) 2004 by Piotr Mierzwiński
    email                : peterm@go2.pl

    copyright            : See COPYING file that comes with this project

 $Id$
 ***************************************************************************/

#include "lfs.h"
#include "localfs.h"
#include "systeminfo.h"
#include "messagebox.h"
#include "fileinfoext.h"

#include <qsettings.h>
#include <qapplication.h>


LocalFS::LocalFS( QWidget *pParent, const char *sz_pName )
	: VFS(pParent, sz_pName), m_pLFS(NULL), m_bFileHasBeenOpened(FALSE)
{
	qDebug("LocalFS::LocalFS()" );
	m_pLFS = new LFS( this );

	QSettings *pSettings = new QSettings;
	m_sTrashDirectory       = pSettings->readEntry( "/qtcmd/LFS/TrashDirectory", QString::null );
	m_bRemoveToTrash        = pSettings->readBoolEntry( "/qtcmd/LFS/AlwaysRemoveToTrash", FALSE );
	m_bWeighBeforeOperation = pSettings->readBoolEntry( "/qtcmd/LFS/AlwaysWeighBeforeOperation", TRUE );
	delete pSettings;

	m_bOperationHasFinished = TRUE;

	connect( m_pLFS, SIGNAL(done(bool)),                 SLOT(slotDone(bool)) );
	connect( m_pLFS, SIGNAL(stateChanged(int)),          SLOT(slotStateChanged(int)) );
	connect( m_pLFS, SIGNAL(commandStarted(int)),        SLOT(slotCommandStarted(int)) );
	connect( m_pLFS, SIGNAL(commandFinished(int,bool)),  SLOT(slotCommandFinished(int,bool)) );

	connect( m_pLFS, SIGNAL(listInfo(const UrlInfoExt &)),
		this, SIGNAL(signalListInfo(const UrlInfoExt &)) );
	connect( m_pLFS, SIGNAL(signalInsertMatchedItem(const UrlInfoExt &)),
		this, SIGNAL(signalInsertMatchedItem(const UrlInfoExt &)) );
	connect( m_pLFS, SIGNAL(signalUpdateFindStatus(uint, uint, uint)) ,
		this, SIGNAL(signalUpdateFindStatus(uint, uint, uint)) );

	connect( m_pLFS, SIGNAL(signalNameOfProcessedFiles(const QString &, const QString &)),
		this, SIGNAL(signalNameOfProcessedFiles(const QString &, const QString &)) );
	connect( m_pLFS, SIGNAL(signalFileCounterProgress(int, bool, bool)),
		this, SIGNAL(signalFileCounterProgress(int, bool, bool)) );
	connect( m_pLFS, SIGNAL(signalTotalProgress(int, long long)),
		this, SIGNAL(signalTotalProgress(int, long long)) );
	connect( m_pLFS, SIGNAL(signalDataTransferProgress(long long, long long, uint)),
		this, SIGNAL(signalDataTransferProgress(long long, long long, uint)) );

	connect( m_pLFS, SIGNAL(signalFileOverwrite(const QString &, QString &, int &)),
	 this, SIGNAL(signalShowFileOverwriteDlg(const QString &, QString &, int &)) );
	connect( m_pLFS, SIGNAL(signalDirectoryNotEmpty(const QString &, int &)),
	 this, SIGNAL(signalShowDirNotEmptyDlg(const QString &, int &)) );

	connect( m_pLFS, SIGNAL(signalPutFile(const QByteArray &, const QString &)),
	 this, SIGNAL(signalPutFile(const QByteArray &, const QString &)) );
}


LocalFS::~LocalFS()
{
	delete m_pLFS;  m_pLFS = 0;
}

	// ------ INFO ABOUT OPERATIONS -----

bool LocalFS::isReadable( const QString & sFileName, bool bSilent, bool bSendSignal )
{
	bool bNoError = m_pLFS->open( sFileName ); // checks whether a file exists and is readable
	if ( (! bNoError && ! bSilent) || (bNoError && ! bSilent) ) // an bError occures or no bError
		slotCommandFinished( Open, !bNoError );

	if ( bNoError && bSendSignal )
		emit signalReadyRead();

	return bNoError;
}

	// ------ FILE OPERATIONS -----

void LocalFS::readDir( const URL & url )
{
	m_pLFS->list( url.path() );
	if ( m_pLFS->errorCode() == NoError )
		m_bSkipNextErrorOccures = FALSE;
}


void LocalFS::mkDir( const QString & sDirName )
{
	m_bSkipNextErrorOccures = FALSE;
	m_pLFS->makeDir( sDirName );
}


void LocalFS::mkFile( const QString & sAbsFileName )
{
	m_bSkipNextErrorOccures = FALSE;
	m_pLFS->touch( sAbsFileName );
}


void LocalFS::copyFiles( QString & sTargetPath, const QStringList & slFilesList, bool bSavePerm, bool bAlwaysOvr, bool bFollowByLink, bool bMove )
{
	qDebug("LocalFS::copyFiles; targetPath=%s, fNum=%d, move=%d", sTargetPath.toLatin1().data(), slFilesList.size(), bMove );
/*
//	m_nOpFileCounter = 0;
//	m_nTotalFiles = slFilesList.count();
	qDebug("LocalFS::copyFiles, bMove=%d, sTargetPath=%s, toCopied=%d", bMove, sTargetPath.toLatin1().data(), m_nTotalFiles );
	m_bSkipNextErrorOccures = FALSE;

	// --- check whether it's copying to new file (eg.to this same dir.)
	bool bCopyingToNewFile = FALSE;
	QFileInfo targetFile( sTargetPath );
	if ( ! targetFile.exists() )
		bCopyingToNewFile = TRUE;
	else
	if ( targetFile.isDir() ) {
		if ( sTargetPath != "/" ) // host()
			if ( sTargetPath.at(sTargetPath.length()-1) != '/' )
				sTargetPath += "/";
	}
	else
		bCopyingToNewFile = TRUE;
	// optymalizacja powyzszego
// 	if ( ! targetFile.isDir() )
// 		bCopyingToNewFile = TRUE;
// 	else { // is Dir
// 		if ( sTargetPath != "/" ) // host()
// 			if ( sTargetPath.at(sTargetPath.length()-1) != '/' )
// 				sTargetPath += "/";
// 	}

	if ( bMove ) // (if tree view then is possible that everyone file have a different sPath)
		for ( uint i=0; i<slFilesList.count(); i++ )
			if ( sTargetPath == FileInfoExt::filePath(slFilesList[i]) ) {
				bCopyingToNewFile = TRUE;
				break;
			}

	QString sTargetFileName;
	if ( bCopyingToNewFile ) {
		if ( slFilesList.count() > 1 ) {
			QString actionStr = (bMove) ? tr("Cannot rename more than one file") : tr("Cannot to copy more than one file to this same directory");
			MessageBox::critical( this,
			 QString::number(slFilesList.count())+" "+tr("files selected")+"\n\n"+
			 actionStr+" !"
			);
			return;
		}
		// copying/renaming 1 file
		sTargetFileName = FileInfoExt::fileName( sTargetPath );
		if ( sTargetPath.at(sTargetPath.length()-1) == '/' )
			sTargetPath = FileInfoExt::filePath( sTargetPath );
		else
			sTargetPath = FileInfoExt::filePath( slFilesList[0] );

		initNewUrlInfo( slFilesList[0] ); // inits member: 'm_uiNewUrlInfo'
		m_uiNewUrlInfo.setName(sTargetFileName);
	}

	// --- if a directory that have'nt write permission (check for one file)
	if ( bMove )
		if ( m_nTotalFiles == 1 )
			if ( ! QFileInfo(FileInfoExt::filePath(absoluteFileName(slFilesList[0]))).isWritable() ) {
				MessageBox::critical( this,
				 "\""+slFilesList[0]+"\""+"\n\n"+ tr("Cannot remove this file")+
				 " !\n"+tr("Permission denied")+"."
				);
			}
	// --- if copying/moving to this same directory
	if ( sTargetPath == FileInfoExt::filePath(slFilesList[0]) )
		if ( m_nTotalFiles == 1 )
			if ( bMove ) {
				changeAttribut( ListView::NAMEcol, sTargetPath+sTargetFileName );
				return;
			}

	m_pLFS->setSavePermission( bSavePerm );
	m_pLFS->setAlwaysOverwrite( bAlwaysOvr );
	m_pLFS->setFollowByLink( bFollowByLink );
	m_pLFS->setWeightBefore( TRUE, TRUE ); // for directory bRecursive weighing

	bool existsFilesToProcessed = FALSE;
	for( uint i=0; i<m_nTotalFiles; i++) {
		sTargetFileName = sTargetPath+((bCopyingToNewFile) ? sTargetFileName : FileInfoExt::sFileName(slFilesList[i]));
		if ( slFilesList[i] != sTargetPath && slFilesList[i]+"/" != sTargetPath ) { // check file and dir
			m_pLFS->copy( slFilesList[i], sTargetFileName, bMove );
			existsFilesToProcessed = TRUE;
		}
		else {
		// early, into VirtualFS::copyFilesTo() fun. this file or directory is deselected and removed from the SelectedList
			QString opStr = (bMove) ? tr("bMove") : tr("copy");
			MessageBox::critical( this,
			 tr("Source file :")+"\n\t\""+slFilesList[i]+"\"\n"+
			 tr("Target file :")+"\n\t\""+sTargetPath+"\"\n\n"+
			 tr("Cannot")+" "+opStr+" "+tr("this same to this same")+" !\n\n"+
			 tr("This file will be passing over.")
			);
		}
	}
//	if ( existsFilesToProcessed )
//		showProgressDialog( Copy, bMove, sTargetPath );
*/
}


void LocalFS::removeFiles( const QStringList & slFilesList )
{
	m_bSkipNextErrorOccures = FALSE;
	//m_nTotalFiles = slFilesList.count();
	//m_nOpFileCounter = 0;

	m_pLFS->setRemoveToTheTrash( m_bRemoveToTrash );
	m_pLFS->setTrashDir( m_sTrashDirectory );
	m_pLFS->setWeightBefore( m_bWeighBeforeOperation, TRUE ); // for directory bRecursive weighing

	for (int i=0; i<slFilesList.count(); i++)
		m_pLFS->remove( slFilesList[i] );
}


void LocalFS::weighFiles( const QStringList & slFilesList, bool bRealWeight )
{
	m_bSkipNextErrorOccures = FALSE;
	m_pLFS->setRealWeightInFS( bRealWeight );

	for (int i=0; i<slFilesList.count(); i++)
		m_pLFS->weigh( slFilesList[i] );
}


void LocalFS::mkLinks( const QStringList & slSourceFilesList, const QStringList & slTargetFilesList, bool bHard, bool bAlwaysOvr )
{
	m_bSkipNextErrorOccures = FALSE;
// 	m_nTotalFiles = slSourceFilesList.count();
// 	m_nOpFileCounter = 0;

	m_pLFS->setHardLink( bHard );
	m_pLFS->setWeightBefore( TRUE, TRUE ); // for directory bRecursive weighing
	m_pLFS->setAlwaysOverwrite( bAlwaysOvr );

	for (int i=0; i<slSourceFilesList.count(); i++)
		m_pLFS->makeLink( slSourceFilesList[i], slTargetFilesList[i] );
}


void LocalFS::editLink( const QString & sFileName, const QString & sNewTargetPath )
{
	m_pLFS->remove( sFileName );
	m_pLFS->makeLink( sFileName, sNewTargetPath, TRUE );
}


void LocalFS::setAttrib( const QStringList & slFilesList, const UrlInfoExt & uiNewUrlInfo, bool bRecursive, int nChangesFor )
{
//	m_nOpFileCounter = 0;
//	m_nTotalFiles = slFilesList.count();
	m_bSkipNextErrorOccures = FALSE;
	bool bIsRename = FALSE;

	m_uiNewUrlInfo = uiNewUrlInfo; // if an bError ocurres then member is cleared (in slotDone())
	m_uiNewUrlInfo.setName(uiNewUrlInfo.name()); // TODO moznaby to przeniesc do getNewUrlInfo()

	if ( ! uiNewUrlInfo.name().isEmpty() ) {
		// not asynchronously eOperation (made instant effect on the list)
		slotCommandFinished( Rename, m_pLFS->rename(slFilesList[0], uiNewUrlInfo.name()) );
		bIsRename = TRUE;
	}

	if ( uiNewUrlInfo.permissions() < 0 &&
	 uiNewUrlInfo.owner().isEmpty() && uiNewUrlInfo.group().isEmpty() &&
	 uiNewUrlInfo.lastRead().isNull() && uiNewUrlInfo.lastModified().isNull()
	)
		return;

	m_pLFS->setPermissionForAll( uiNewUrlInfo.permissions() );
	if ( ! m_pLFS->setOwnerAndGroupForAll(uiNewUrlInfo.owner(), uiNewUrlInfo.group()) ) {
		slotCommandFinished( SetAttributs, TRUE ); // bError occures (not found Id), show announcement
		return;
	}
	// FIXME trzeba tu jeszcze spr. czy nowy id (lub nazwa grupy) jest na liscie grup do ktorych nalezy zalogowany uzytkownik
	// jesli nie, wtedy tez pokazywac blad

	m_pLFS->setAccessAndModificationTimeForAll( uiNewUrlInfo.lastRead(), uiNewUrlInfo.lastModified() );
	m_pLFS->setRecursiveAttributs( bRecursive );
	m_pLFS->setWeightBefore( TRUE, bRecursive ); // for directory set 'bRecursive' weighing
	m_pLFS->setChangesForAll( nChangesFor );

	for ( int i=0; i<slFilesList.count(); i++ )
		if ( bIsRename )
			m_pLFS->setAttributs( uiNewUrlInfo.name() );
		else
			m_pLFS->setAttributs( slFilesList[i] );
}


void LocalFS::find( const FindCriterion & findCriterion, bool bStop )
{
	if ( bStop )
		m_pLFS->breakCurrentOperation();
	else {
		m_bSkipNextErrorOccures = FALSE;
		QString location = findCriterion.location;
		// TODO dzieki wazeniu przed szukaniem, moznaby pokazywac postep szukania, ale to wydluzy czas oczekiwania na rozp.szukania
		m_pLFS->setWeightBefore( FALSE /*TRUE*/ ); // for directory bRecursive weighing
		m_pLFS->findFile( findCriterion );
	}
}


void LocalFS::get( const QString & /*sFileName*/, QIODevice * /*pDev*/ )
{
}


void LocalFS::put( QIODevice * /*pDev*/, const QString & /*sFileName*/ )
{
}


void LocalFS::initNewUrlInfo( const QString & sFileName )
{
	QString sNewFileName = (sFileName.isEmpty()) ? m_pLFS->nameOfProcessedFile() : sFileName;

	QFileInfo f( sNewFileName );
	//FileInfoExt f( sNewFileName );
	//bool isEmptyDir = f.isEmpty(); // TODO

	bool isEmptyDir = FALSE;
	if ( f.isDir() && ! f.isSymLink() )
		if ( f.size() == 48 ) // it's true only for non Win/DOS FileSystems.
			isEmptyDir = TRUE;

	UrlInfoExt URLinfo( FileInfoExt::fileName( sNewFileName ),
	 f.isDir() ? 0 : (f.isSymLink() ? f.readLink().length() : f.size()),
	 FileInfoExt::permission(sNewFileName),
	 f.owner(),        f.group(),
	 f.lastModified(), f.lastRead(),
	 f.isDir(),        f.isFile(),  f.isSymLink(),
	 f.isReadable(),   f.isExecutable(), isEmptyDir // f.isEmpty()
	);

	m_uiNewUrlInfo = URLinfo;
	//m_uiNewUrlInfo = f.uiUrlInfoExt(); // TODO wtedy nie byloby potrzebne powyzsze tw.obiektu i spr.czy pusty
}

	// ------------ SLOTs --------------

void LocalFS::slotCommandStarted( int nCmd )
{
	qDebug("LocalFS::slotCommandStarted(); operation=%s, command=%s", cmdString(m_pLFS->currentOperation()).toLatin1().data(), cmdString((Operation)nCmd).toLatin1().data() );

	if (nCmd == List) {
		qDebug("__ start listing dir, sPath=%s", m_pLFS->nameOfProcessedFile().toLatin1().data() );
		if (m_pLFS->currentOperation() != NoneOp)
			QApplication::setOverrideCursor( Qt::waitCursor );
		emit signalPrepareForListing();
	}
	else {
		if ( nCmd != Weigh ) {
			//if ( ! m_nOpFileCounter ) // FIXME ? po co jest 'm_nOpFileCounter'
				m_bOperationHasFinished = FALSE;
			//m_nOpFileCounter++;
		}
	}
}


void LocalFS::slotCommandFinished( int nCmd, bool bError )
{
	qDebug("LocalFS::slotCommandFinished(), command=%s, operation= %s, error=%d", cmdString((VFS::Operation)nCmd).toLatin1().data(), cmdString(operation()).toLatin1().data(), bError );
	bool bSingleFileOperation = (nCmd == Cd || nCmd == Open || nCmd == Rename || nCmd == MakeDir || nCmd == Put || nCmd == Touch);
//	bool bSingleFileOperation = (nCmd == Open || nCmd == Rename || nCmd == MakeDir || nCmd == Put || nCmd == Touch);

	if ( ! bError ) { // no bError
		if ( nCmd == Cd ) {
			m_sCurrentPath = m_pLFS->nameOfProcessedFile();
			m_bSkipNextErrorOccures = FALSE;
		}
		Error bError = m_pLFS->errorCode(); // NNN
		if ( bError == BreakOperation ) // for removed skipped file from the selected list
			if ( (nCmd == Copy && bError != CannotSetAttributs) || nCmd == Move || nCmd == Remove || nCmd == MakeLink )
				emit signalRemoveFileNameFromSIL( m_pLFS->nameOfProcessedFile() );
	}
	else { // an error occured
		Error bError = m_pLFS->errorCode();
		if ( bError == BreakOperation ) // NNN
			if ( (nCmd == Copy && bError != CannotSetAttributs) || nCmd == Move )
				emit signalRemoveFileNameFromSIL( m_pLFS->nameOfProcessedFile() ); // for removed skipped file from the selected list

		emit signalStartProgressDlgTimer( FALSE ); // bStop timer
	// --- show an bError announcement
		if ( bError != CannotSetAttributs )
			m_uiNewUrlInfo.clear();

		if ( ! m_bSkipNextErrorOccures ) {
			int nErrorNum = m_pLFS->errorNumber();
			QString sFileName = m_pLFS->nameOfProcessedFile(); // errorFileName
			bool bIsFile = (sFileName.at(sFileName.length()-1) != '/');
			QString sKindOfFile = (bIsFile) ? tr("file") : tr("directory"); // all symlinks are treats as nFiles
			QString sMessage = "\""+sFileName+"\""+"\n\n\t";

			switch ( nErrorNum ) {
				case NotExists:
					sMessage += tr("This")+" "+sKindOfFile+" "+tr("not exists")+" !";
					break;
				case NoFreeSpace:
					sMessage += tr("Not enough free space in target directory")+" !";
					break;
				case ReadError:
					sMessage += tr("Cannot read this")+" "+sKindOfFile+" !\n"+tr("Read bError")+" = "+QString::number(nErrorNum);
					break;
				case CannotRead:
					sMessage += tr("Cannot read this");
					break;
				case CannotWrite:
					sMessage += (nCmd == Remove) ? tr("Cannot to remove this file") : tr("Cannot write to this");
					break;
				case CannotRemove:
					sMessage += tr("Cannot remove this");
					break;
				case CannotSetAttributs:
					sMessage += tr("Cannot set attributs for this");
					break;
				case CannotCreateLink:
					sMessage += tr("Cannot create this link");
					break;
				case AlreadyExists:
					sMessage += tr("This")+" "+sKindOfFile+" "+tr("already exists")+" !";
					break;
				default:
					sMessage += tr("Error number: ")+QString::number(nErrorNum);
					break;
			}
			if ( bError == CannotRead || bError == CannotWrite || bError == CannotRemove )
				sMessage += " "+sKindOfFile+" !\n\t"+tr("Permission denied")+".";
			else
			if ( bError == CannotSetAttributs || bError == CannotCreateLink )
				sMessage += " "+sKindOfFile+" !\n\t"+tr("Operation not permitted")+".";

			if ( nCmd == Rename && bError == AlreadyExists ) {
				int nResult = MessageBox::yesNo( this,
					tr("Overwrite file")+" - QtCommander",
					sMessage+"\n\t"+tr("Overwrite it by")+"\n\n\""+sFileName+"\" ?",
					MessageBox::Yes
				);
				if ( nResult == MessageBox::Yes ) {
// 					m_pLFS->remove( sFileName );
// 					m_pLFS->rename( currentFileName(), sFileName ); // zrobic jako asynchr
					// --- rozw.tymczasowe
					if ( QFile::remove(sFileName) ) {
/* // odkomentowac pozniej
						emit signalResultOperation( Remove, !bError ); // removes an item from list
						bool stat = m_pLFS->rename( currentFileName(), sFileName );
						emit signalResultOperation( Rename, !stat );
*/
					}
					else
						MessageBox::critical( this,
							FileInfoExt::filePath(sFileName)+"\n\n\t"+tr("Cannot write to this directory !")
						);
					return;
				}
			}
			else
			if ( bError != BreakOperation ) {
				if ( bIsFile && bSingleFileOperation )
					MessageBox::critical( this, sMessage );
				else {
					if (MessageBox::critical( this, sMessage, TRUE, tr("Don't show next error") ) != 1) // == 101
						m_bSkipNextErrorOccures = TRUE;
					/*int nResult = MessageBox::yesNo( this,
						tr("Error")+" - QtCommander",
						sMessage+"\n\n\t"+tr("Do you want to see next error announcement")+" ?",
						MessageBox::No
					);
					if ( nResult == MessageBox::No )
						m_bSkipNextErrorOccures = TRUE;
					*/
				}
			}
		} // if not skip all errors
		if ( nCmd == Find )
			find( FindCriterion(), TRUE ); // break a finding
	} // bError occured

	if ( bSingleFileOperation )
		slotDone( bError );
	else
		emit signalStartProgressDlgTimer( TRUE );
}


void LocalFS::slotDone( bool bError )
{
	QApplication::restoreOverrideCursor();
	Operation eCommand = m_pLFS->currentCommand();
	Operation eOperation = m_pLFS->currentOperation();
	qDebug("LocalFS::slotDone(), errorStat=%d, errno=%d, cmd=%s, operation=%s", bError, m_pLFS->errorNumber(), cmdString(eCommand).toLatin1().data(), cmdString(eOperation).toLatin1().data() );

	emit signalStartProgressDlgTimer( FALSE ); // bStop timer
	m_bOperationHasFinished = TRUE;

	if (! bError || (eOperation == Copy && m_pLFS->errorCode() == CannotSetAttributs)) {
		switch ( eOperation ) {
			case Touch:
				//openFile( m_pLFS->nameOfProcessedFile(), TRUE ); // open empty new file into view
			case Put: // Put is used during EditLink eOperation, too
			case MakeDir:
				initNewUrlInfo();
				break;

			default: break;
		}
	}
	if (eCommand == Cd && ! bError)
		return;

	emit signalResultOperation( eOperation, ! bError );
}


void LocalFS::slotStateChanged( int /*nState*/ )
{
//	emit signalSetOperation( (Operation)state ); // set info about an eOperation on the progress dialog
}


void LocalFS::slotReadFileToView( const QString & sFileName, QByteArray & baBuffer, int & nBytesRead, int nFileLoadMethod )
{
	if ( ! nBytesRead )
		return;

	if ( ! m_bFileHasBeenOpened ) {
		if ( m_File.name() != sFileName )
			m_File.setName( sFileName );

		if ( ! m_File.open(QIODevice::ReadOnly) )
			return;
		else
			m_bFileHasBeenOpened = TRUE;
	}

	enum UpdateMode { ALL=0, APPEND, ON_DEMAND, NONE };

	if ( nFileLoadMethod == ALL || nBytesRead < 0 ) { // read all bytes of file
		baBuffer    = m_File.readAll();
		nBytesRead = baBuffer.size()+1;
		baBuffer.resize( nBytesRead );
		m_bFileHasBeenOpened = FALSE;
		m_File.close();
	}
	else
	if ( nFileLoadMethod == APPEND ) { // read file by parts
		baBuffer.resize( nBytesRead+1 ); // +1 for null character (need to text nFiles)
		int bRead = m_File.readBlock( baBuffer.data(), nBytesRead );

		if ( bRead == 0 ) {
			m_File.close();
			m_bFileHasBeenOpened = FALSE;
		}
		else
		if ( bRead < 0 ) { // an bError occures
			m_File.close();
			slotDone( TRUE );
			m_bFileHasBeenOpened = FALSE;
		}

		nBytesRead = bRead;
	}
}


void LocalFS::slotGetUrlInfo( const QString & sFullFileName, UrlInfoExt & uiUrlInfoExt )
{
	uiUrlInfoExt.setName( sFullFileName );
	uiUrlInfoExt.setSize( QFileInfo(sFullFileName).size() );
	uiUrlInfoExt.setLastModified( QFileInfo(sFullFileName).lastModified() );
}


void LocalFS::slotPause( bool bStop )
{
	m_pLFS->pauseCurrentOperation( bStop ); // or start
}


// -------- TYMCZASOWA FUNKCJA DLA TESTOW File Systemu
QString LocalFS::cmdString( Operation eOp )
{
	QString s = "Unknown";

		 if ( eOp == HostLookup ) s = "HostLookup";
	else if ( eOp == NoneOp )   s = "NoneOp";
	else if ( eOp == Connect )  s = "Connect";
	else if ( eOp == List )     s = "List";
	else if ( eOp == Open )     s = "Open";
	else if ( eOp == Cd )       s = "Cd";
	else if ( eOp == Rename )   s = "Rename";
	else if ( eOp == Remove )   s = "Remove";
	else if ( eOp == MakeDir )  s = "MakeDir";
	else if ( eOp == MakeLink ) s = "MakeLink";
	else if ( eOp == Get )      s = "Get";
	else if ( eOp == Put )      s = "Put";
	else if ( eOp == Touch )    s = "Touch";
	else if ( eOp == Copy )     s = "Copy";
	else if ( eOp == Move )     s = "Move";
	else if ( eOp == Weigh )    s = "Weigh";
	else if ( eOp == Find )     s = "Find";
	else if ( eOp == SetAttributs ) s = "SetAttributs";

	return s;
}
