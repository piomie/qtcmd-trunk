/***************************************************************************
                          lfs.h  -  description
                             -------------------
    begin                : sat dec 28 2002
    copyright            : (C) 2002, 2003 by Piotr Mierzwi�ski
    email                : peterm@go2.pl

    Copyright: See COPYING file that comes with this project

 $Id$
***************************************************************************/

/** @file doc/api/src/lfs.dox */

#ifndef _LFS_H_
#define _LFS_H_

#include <q3ptrstack.h>
#include <qwidget.h>
#include <qregexp.h>
#include <qtimer.h>
//Added by qt3to4:
#include <Q3PtrList>
#include <QListIterator>

#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>
#include <errno.h>

#include "enums.h"
#include "vfs.h"

class QFileInfo;
class QDir;

class UrlInfoExt;
class FindCriterion;


class LFSCommand
{
public:
	LFSCommand( VFS::Operation cmd, const QString & fName )
	 : command(cmd), sFileName(fName) {}

	VFS::Operation command;
	QString sFileName;
};


class LFS : public QWidget
{
	Q_OBJECT
public:
	LFS( QWidget *parent=0, const QString & path="/", const char *name=0 );
	~LFS();

	void breakCurrentOperation();
	void pauseCurrentOperation( bool pause=TRUE );

	void setRemoveToTheTrash( bool removeToTheTrash );
	void setTrashDir( const QString & trashPath );
	void setSavePermission( bool savePerm )   { m_bSavePermission = savePerm; }
	void setFollowByLink( bool followByLink ) { m_bFollowByLink = followByLink; }
	void setAlwaysOverwrite( bool alwaysOvr ) { m_bAlwaysOverwrite = alwaysOvr; }
	void setRealWeightInFS( bool realWeight ) { m_bRealWeightInFS = realWeight; }
	void setHardLink( bool hard ) { m_bHardLink = hard; }
	void setWeightBefore( bool weightBefore, bool recursive=TRUE ) {
		m_bWeightBefore = weightBefore;  m_bRecursiveAttributs = recursive;
	}

	void getWeighingStat( long long & weight, uint & files, uint & dirs );

	void cd( const QString & pathName );
	bool open( const QString & sFileName );
	void touch( const QString & sFileName );
	void remove( const QString & sFileName );
	void makeDir( const QString & dirName );
	void list( const QString & dir=QString::null, bool asynchr=FALSE );
	void copy( const QString & sourceFileName, const QString & targetFileName, bool move=FALSE );
	bool rename( const QString & oldName, const QString & newName );
	void weigh( const QString & sFileName );
	void makeLink( const QString & sourceFileName, const QString & targetFileName, bool editLink=FALSE );
	void setAttributs( const QString & );
	void findFile( const FindCriterion & );

	VFS::Operation currentOperation() const { return m_eCurrentOperation; }
	VFS::Operation currentCommand() const   { return m_eCurrentCommand; }

	QString currentPath() const  { return m_sCurrentPath;  }

	VFS::Error errorCode() const { return m_eErrorCode;    }
	int errorNumber() const      { return errno; }

	void setGetFileLinkSize( bool getLinkSize ) { m_bGetFileLinkSize = getLinkSize; }

	uint numOfAllFiles() const { return mDir.count(); }
	QString nameOfProcessedFile() const { return m_sNameOfProcessedFile; }

	void setRecursiveAttributs( bool recursive ) { m_bRecursiveAttributs = recursive; }
	bool setOwnerAndGroupForAll( const QString & owner, const QString & group );
	void setAccessAndModificationTimeForAll( const QDateTime & accessTime, const QDateTime & modificationTime );
	void setPermissionForAll( int permisson ) { m_nPermissionForAll = permisson; }
	void setChangesForAll( int changesFor ) { m_eChangesFor = (ChangesFor)changesFor; }

	bool dirIsEmpty( const QString & dirName );
	void putFile( const QByteArray & data, const QString & file );

private:
	QString m_sCurrentPath, m_sNameOfProcessedFile;

	//QFileInfoListIterator *mFIListIterator;
    QListIterator<QFileInfo> *mFIListIterator;
	QFileInfo mFI;
	QDir mDir;

	Q3PtrList<LFSCommand> mLFSCommandList;
	VFS::Operation m_eCurrentOperation, m_eCurrentCommand;
	QTimer mOperationTimer;

	uint m_nListingFilesCounter;
	uint m_nOpFileCounter, m_nErrorNumber;
	bool m_bPauseOperation, m_bInitOperation, m_bSkipCurrentFile;

	char *m_pFileBuffer;
	int m_nFileBufferSize;
	bool m_bFollowByLink, m_bSavePermission, m_bAlwaysOverwrite, m_bHardLink, m_bNoneOverwriting, m_bLinkEdit;
	long long m_nSourceFileSize, m_nWritedBytes;
	QString m_sSourceFileName, m_sTargetFileName;
	QFile mFileIn, mFileOut;

	bool m_bRealWeightInFS, m_bWeightBefore;
	long long m_nTotalWeight;
	uint m_nDirCounter,  m_nFileCounter, m_nTotalFiles;
	uint m_nDirCounterR, m_nFileCounterR;

	bool m_bRemoveToTrash, m_bRecursivelyRemoveAllDirs, m_bNoneRecursivelyRemoveDir;
	QString m_sTrashPath;

	bool m_bTreeView, m_bGetFileLinkSize, m_bAsynchronouslyListing;
	VFS::Error m_eErrorCode;
	QFileInfo mFInfo;

	enum RecursiveOperation {
		NONE,
		FIND,
		COPY,
		WEIGH,
		REMOVE,
		SET_ATTRIBUTES,
	};
	RecursiveOperation m_eRecursiveOp;
	DIR *m_pDIR_DirPtr;
	off_t m_nCurrLocInDIR;
	struct stat mStatBuffer;
	struct dirent *m_pDirEntries;
	Q3PtrStack <int> mDirPtrStack;
	Q3PtrStack <int> mDirInodeStack;
	Q3PtrStack <int> m_nDirInode, m_nThrowedOffInode;
	Q3PtrStack <off_t>mCurrLocInDIRstack;
	bool m_bWasDir, m_bNeedToSeek, m_bOkStat;
	int  m_nWasDots;
	QString m_sAbsSrcFileName, m_sAbsTargetFName;
	QString m_sAbsSrcDirName, m_sAbsTargetDirName;
	QString m_sCurrentFileName;

	QString m_sOwnerForAll, m_sGroupForAll;
	int m_nAccessTimeForAll, m_nModificationTimeForAll;
	bool m_bRecursiveAttributs;
	int m_nPermissionForAll, m_nCurrentUserID, m_nOwnerIdForAll, m_nGroupIdForAll;
	enum ChangesFor {
		ALLcf = 0, FILESONLYcf, DIRECTORIESONLYcf
	};
	ChangesFor m_eChangesFor;

	enum KindOfUpdating { NONEupd=0, DATEupd, SIZEupd };
	KindOfUpdating m_eUpdatingFilesWith;

	FindCriterion mFindCriterion;
	int m_nMatchedFiles;
	bool m_bFindAll;
	QRegExp mFindRegExp; //,mFindTextRegExp;

	bool isError( const QString & sFileName, VFS::KindOfError kindOfError );
	bool illegalFileName( const QString & sFileName );
	QString absoluteFileName( const QString & sFileName ) const;

	void clearFilesCounter() {
		m_nDirCounter=0; m_nFileCounter=0; m_nTotalWeight=0; m_nOpFileCounter=0; m_nMatchedFiles = 0;
	}

	void initListing();
	void initCopying();
	bool initCopiedFiles( long long fileSize=-1 );
	bool initGetFile( long long fileSize=-1 );
	bool initPutFile();
	bool initMakeLink();

	void addCommand( VFS::Operation command, const QString & sFileName=QString::null );

	void currentCommandFinished();
	void jobFinished();

	void getListedItem();

	void putFileFinished();
	void getFileFinished();
	void copyingFile();

	bool removeNextFile();
	void removeDir( const QString & dirName );
	void removeFile( const QString & sFileName );

	bool recursiveOperation( RecursiveOperation recursiveOp, const QString & sourceDirName, const QString & targetDirName=QString::null );

	bool sizeNextFile();
	long long fileSize( const QString & sFileName );

	bool makeNextLink( const QString & sourceFileName, const QString & targetFileName, bool checkErrors=FALSE );

	bool setAttributsNextFile();
	bool setFilePermission( const QString & sFileName, int permission );
	bool setFileTime( const QString & sFileName, int secondsToAccess, int secondsToModification );
	bool setFileOwnerAndGroup( const QString & sFileName, const QString & owner, const QString & group );
	bool setFileOwnerAndGroup( const QString & sFileName, int ownerId, int groupId );

	void insertMatchedItem();
	bool currentFileMatches();

public slots:
	void abort();

private slots:
	void slotFileProcessing();
	void slotStartNextJob();
	void slotDirProcessing();
	void slotStartProcessDir();
	void slotListing();

signals:
	void done( bool error );

	void commandStarted( int command );
	void commandFinished( int command, bool error );

	void stateChanged( int state );
	void listInfo( const UrlInfoExt & urlInfo );

	void signalDirectoryNotEmpty( const QString & dirName, int & result );
	void signalFileOverwrite( const QString & sourceFileName, QString & targetFileName, int & result );
	void signalDataTransferProgress( long long done, long long total, uint bytesTransfered );
	void signalFileCounterProgress( int value, bool fileCounting, bool valueOfAll=FALSE );
	void signalNameOfProcessedFiles( const QString & sourceName, const QString & targetName=QString::null );
	void signalTotalProgress( int done, long long totalWeight=0 ); // done is in percent

	void signalInsertMatchedItem( const UrlInfoExt & urlInfo );
	void signalUpdateFindStatus( uint matchedFiles, uint files, uint dirs );

	void signalPutFile( const QByteArray & buffer, const QString & sFileName );

};

#endif

//      void get( const QString & , QIODevice * dev = 0 );
//      void put ( const QByteArray & data, const QString & file );
//      void put ( QIODevice * dev, const QString & file );
//      Q_LONG readBlock( char * data, Q_ULONG maxlen );
//      QByteArray readAll();
//      Q_ULONG bytesAvailable() const;
