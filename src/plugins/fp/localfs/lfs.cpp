/***************************************************************************
                          lfs.cpp  -  description
                             -------------------
    begin                : Sat Dec 28 2002
    copyright            : (C) 2002, 2003 by Piotr Mierzwi�ski
    email                : peterm@go2.pl

    copyright            : See COPYING file that comes with this project

 $Id$
***************************************************************************/

#include <qfileinfo.h>
#include <qdir.h>

#include "lfs.h"
#include "functions.h" // for static functions
#include "systeminfo.h" // for getting user and group
#include "messagebox.h"
#include "urlinfoext.h"
#include "fileinfoext.h"
#include "findcriterion.h"

#include <fcntl.h>
#include <utime.h>

static bool sNotReseted = TRUE;

LFS::LFS( QWidget *parent, const QString & path, const char *name )
	: QWidget( parent,name ), m_sCurrentPath( path )
{
	mDir.setPath( path );
	mDir.setFilter( QDir::All | QDir::System | QDir::Hidden );

	mDirPtrStack.setAutoDelete( TRUE );
	mDirInodeStack.setAutoDelete( TRUE );
	mLFSCommandList.setAutoDelete( TRUE );
	mCurrLocInDIRstack.setAutoDelete( TRUE );

	m_bInitOperation   = TRUE;
	m_bSavePermission  = TRUE;
	m_bAlwaysOverwrite = FALSE;
	m_bSkipCurrentFile = FALSE;
	m_bNoneOverwriting = FALSE;
	m_bFollowByLink  = FALSE;
	m_bRealWeightInFS = FALSE; // not round up file size to file system block (cluster size), during weighing dir
	m_bWeightBefore   = FALSE; // do'nt weigh before operation
	m_bLinkEdit = m_bHardLink = FALSE; // make symlinks
	mFIListIterator = NULL;
	m_pFileBuffer = NULL;
	clearFilesCounter();
	m_nFileBufferSize = 1024*16; //(1024*1024)*1; // 1 MB buffer
	m_eCurrentCommand = VFS::NoneOp;
	m_eRecursiveOp    = NONE;
	m_eErrorCode      = VFS::NoError;
	m_eChangesFor = ALLcf;
	m_nCurrentUserID  = ::getuid();
	m_bRecursiveAttributs = FALSE;
	m_nAccessTimeForAll = m_nModificationTimeForAll = -1;
	m_nPermissionForAll = m_nOwnerIdForAll = m_nGroupIdForAll = -1;

	m_bRemoveToTrash = FALSE;  // real remove
	m_sTrashPath = "";

	connect( &mOperationTimer, SIGNAL(timeout()), this, SLOT(slotFileProcessing()) );
}

// UWAGA. Najlepiej gdy listowane sa wszystkie pliki, a pozniej sa ewentualnie filtrowane w klasie widoku listy

LFS::~LFS()
{
	if ( m_pFileBuffer ) {
		delete m_pFileBuffer;
		m_pFileBuffer = 0;
	}
    if (mFIListIterator)
	   delete mFIListIterator;
}


void LFS::setRemoveToTheTrash( bool removeToTheTrash )
{
	m_bRemoveToTrash = removeToTheTrash;

	if ( removeToTheTrash )
		setTrashDir( m_sTrashPath );
}


void LFS::setTrashDir( const QString & trashPath )
{
	QString path = trashPath.isEmpty() ? QDir::homeDirPath()+"/tmp/qtcmd/trash" : trashPath;
	if ( ! QFileInfo(path).exists() ) {
		makeDir( path );
		if ( m_eErrorCode != VFS::NoError )
			return;
	}
	if ( QFileInfo(path).isWritable() )
		m_sTrashPath = path + ( (path.at( path.length()-1 ) != '/') ? "/" : QString("") );
	else
		m_eErrorCode = VFS::CannotWrite;
}


void LFS::addCommand( VFS::Operation command, const QString & sFileName )
{
	bool appendCommand = TRUE;
	if ( m_eCurrentOperation != VFS::Weigh )
		if ( command == VFS::Weigh && mLFSCommandList.count() > 0 ) {
			// need to shedule commands - weigh command must be on the top
			for( int i=mLFSCommandList.count()-1; i>-1; i--)
				if ( mLFSCommandList.at(i)->command == VFS::Weigh ) {
					mLFSCommandList.insert( i+1, new LFSCommand(command, sFileName) );
					appendCommand = FALSE;
					break;
				}
		}

	if ( appendCommand )
		mLFSCommandList.append( new LFSCommand(command, sFileName) );

	if ( mLFSCommandList.count() == 1 )
		QTimer::singleShot( 0, this, SLOT(slotStartNextJob()) );
}


void LFS::currentCommandFinished()
{
//	mOperationTimer.stop(); // listowanie asynchroniczne moze tego potrzebowac (aktualnie nie uzywane)
	bool earlyError = (m_eErrorCode != VFS::NoError);
	if ( m_eCurrentCommand == VFS::Copy || m_eCurrentCommand == VFS::Move ) {
		mOperationTimer.stop();
		if ( ! earlyError || m_eErrorCode == VFS::BreakOperation || m_eErrorCode == VFS::CannotWrite || m_eErrorCode == VFS::CannotRemove ) { // CannotRemove - target file (if exists)
			getFileFinished();
			if ( m_eErrorCode != VFS::CannotWrite && m_eErrorCode != VFS::CannotRemove && ! m_bSkipCurrentFile && ! m_bNoneOverwriting )
				putFileFinished();
		}
		if ( m_eRecursiveOp == NONE )
			mLFSCommandList.removeFirst(); // remove 'Get' action, below 'Put' is removed
	}
	if ( m_eCurrentCommand != VFS::Weigh && m_eCurrentCommand != VFS::NoneOp ) // for Copy/Move/Remove/MakeLink
		m_bInitOperation = FALSE;
	if ( m_eErrorCode != VFS::NoError && m_nFileCounter ) { // update counters when an error occured
		emit signalFileCounterProgress( m_nTotalFiles, TRUE, TRUE ); // update total value
		emit signalFileCounterProgress( (m_eCurrentCommand==VFS::Remove) ? m_nFileCounterR : m_nFileCounter, TRUE ); // update file counter
	}

	if ( ! m_bSkipCurrentFile && ! m_bNoneOverwriting )
		if ( (m_eErrorCode == VFS::NoError && ! earlyError) || m_eErrorCode == VFS::CannotSetAttributs ) {
			if ( m_eCurrentCommand == VFS::Weigh ) {
				m_nTotalFiles++;
				emit signalFileCounterProgress( ++m_nFileCounter, TRUE, TRUE ); // update total value
			}
			else {
				if ( m_eCurrentCommand == m_eCurrentOperation || m_eCurrentOperation == VFS::Move ) {
					if ( !(m_eRecursiveOp == REMOVE && m_eCurrentCommand == VFS::Remove) || m_eCurrentOperation == VFS::Remove )
						m_nOpFileCounter++;
					if ( m_nTotalFiles )
						emit signalTotalProgress( (100 * m_nOpFileCounter) / m_nTotalFiles, m_nTotalWeight );
				}
			}
			if ( m_eCurrentCommand == VFS::Remove || m_eCurrentCommand == VFS::SetAttributs )
				emit signalFileCounterProgress( (m_eCurrentCommand==VFS::Remove) ? ++m_nFileCounterR : ++m_nFileCounter, TRUE ); // update file couter
		}

	if ( m_eRecursiveOp == NONE ) {
		if ( mLFSCommandList.isEmpty() )
			return;
		else {
			mLFSCommandList.removeFirst(); // remove an single action or 'Put'
			if ( m_eCurrentCommand == VFS::Move && (m_eErrorCode == VFS::CannotWrite  || m_eErrorCode == VFS::CannotRemove || m_eErrorCode == VFS::BreakOperation || m_bSkipCurrentFile || m_bNoneOverwriting) )
				mLFSCommandList.removeFirst(); // remove 'Remove' action
		}
		if ( ! m_bSkipCurrentFile ) {
			if ( m_eCurrentCommand == VFS::Remove ) {
				if ( ! mLFSCommandList.isEmpty() ) {
					if ( mLFSCommandList.at(0)->command != VFS::MakeLink )
						if ( earlyError )
							emit commandFinished( m_eCurrentCommand, earlyError ); // shows error announcement
				}
				else {
					if ( earlyError )
						emit commandFinished( m_eCurrentCommand, earlyError ); // // shows error announcement
				}
			}
			else
			if ( m_eCurrentCommand != VFS::Weigh ) {
				if ( m_eCurrentCommand == VFS::Copy || m_eCurrentCommand == VFS::Move )
					m_sNameOfProcessedFile = m_sTargetFileName; // need to correct inserting on a list and show error info
				if ( m_bLinkEdit )
					m_eCurrentCommand = VFS::Put;
				if ( m_eErrorCode != VFS::NoError )
					emit commandFinished( m_eCurrentCommand, (m_eErrorCode != VFS::NoError) ); // shows error announcement
				if ( m_bLinkEdit ) {
					m_eCurrentCommand = VFS::MakeLink;
					m_bLinkEdit = FALSE;
				}
			}
		}
	}
	else {
		m_sAbsSrcFileName = QFile::decodeName( ::get_current_dir_name() ); // = m_sAbsSrcDirName, zawsze
		if ( m_eErrorCode != VFS::NoError && m_eCurrentCommand != VFS::Weigh && m_eCurrentCommand != VFS::Find ) {
			m_sNameOfProcessedFile = m_sAbsTargetFName;
			emit commandFinished( m_eCurrentCommand, (m_eErrorCode != VFS::NoError) ); // shows error announcement
		}
		else
		if ( m_eErrorCode != VFS::NoError && m_eCurrentCommand == VFS::Weigh ) { // NNN
			if (m_eErrorCode != VFS::CannotRead) // for CannotRead is setting
				m_sNameOfProcessedFile = m_sAbsSrcFileName;
			emit commandFinished( m_eCurrentCommand, (m_eErrorCode != VFS::NoError) ); // shows error announcement
		}
		if ( m_eCurrentCommand == VFS::Copy || m_eCurrentCommand == VFS::Move )
			m_sAbsTargetFName = m_sAbsTargetDirName;
	}

	if ( mLFSCommandList.isEmpty() || (m_eCurrentCommand == VFS::Cd && earlyError) || m_eErrorCode == VFS::BreakOperation )
		jobFinished();
	else
	if (m_eRecursiveOp != NONE) {
		if (m_eErrorCode == VFS::NoError) // NNN (2005-07-21)
			QTimer::singleShot( 0, this, SLOT(slotDirProcessing()) );
	}
	else
		QTimer::singleShot( 0, this, SLOT(slotStartNextJob()) );
}


void LFS::jobFinished()
{
	mOperationTimer.stop();
	mLFSCommandList.clear(); // if an error ocurred then list is not empty

	while ( ! mCurrLocInDIRstack.isEmpty() ) // clear made CRASH
		mCurrLocInDIRstack.pop();
	while ( ! mDirInodeStack.isEmpty() ) // clear made CRASH
		mDirInodeStack.pop();
	while ( ! mDirPtrStack.isEmpty() )
		::closedir( (DIR *)mDirPtrStack.pop() );

	if ( m_eCurrentOperation == VFS::Copy || m_eCurrentOperation == VFS::Move )
		if ( m_pFileBuffer ) {
			delete m_pFileBuffer;  m_pFileBuffer = 0;
		}

	VFS::Operation op = m_eCurrentOperation;

	if ( op == VFS::Find && m_nMatchedFiles == 0 )
		emit signalUpdateFindStatus( m_nMatchedFiles, m_nFileCounter, m_nDirCounter );
	else
	if ( op == VFS::Weigh )
		m_sNameOfProcessedFile = m_sAbsSrcFileName; // NNN
	else
	if ( op == VFS::Copy || op == VFS::Move )
		m_sNameOfProcessedFile = m_sTargetFileName; // need to correct inserting on the list

	if ( op != VFS::NoneOp )
		if ( (m_eErrorCode != VFS::NoError && (op == VFS::Copy || op == VFS::Move || op == VFS::Remove || op == VFS::MakeLink || op == VFS::SetAttributs) )
		|| m_eErrorCode == VFS::NoError || m_eCurrentCommand == VFS::Cd || op == VFS::Weigh )
			emit done( (m_eErrorCode != VFS::NoError) );

	m_eRecursiveOp = NONE;
	m_eErrorCode = VFS::NoError;
	m_eCurrentOperation = m_eCurrentCommand = VFS::NoneOp;
	m_bSkipCurrentFile = FALSE;
	m_bNoneOverwriting = FALSE;
	m_bInitOperation = TRUE;
	sNotReseted = TRUE;
	m_nTotalFiles = 0;
	clearFilesCounter();
}

	// ------- FILE OPERATIONS --------

bool LFS::open( const QString & sFileName )
{
	m_eCurrentOperation = m_eCurrentCommand = VFS::Open;
	if ( isError(sFileName, VFS::Exists) ) {
		m_sNameOfProcessedFile = sFileName;
		return FALSE;
	}
	if ( QFileInfo(sFileName).isDir() ) {
		m_eCurrentOperation = m_eCurrentCommand = VFS::Cd;
		cd( sFileName );
		return (m_eErrorCode==VFS::NoError);
	}
	bool notReadable = isError( sFileName, VFS::Readable );
	m_sNameOfProcessedFile = sFileName;

	return ! notReadable;
}


void LFS::cd( const QString & pathName )
{
    qDebug("LFS::cd(): to %s", pathName.toLatin1().data());
	m_eCurrentCommand = VFS::Cd;
	m_sNameOfProcessedFile = pathName;

	if ( illegalFileName(pathName) )
		return;

	QString path = pathName;
	if ( path == "~" )
		path = QDir::homeDirPath()+"/";
	else
	if ( path == ".." )
		path = FileInfoExt::filePath( m_sCurrentPath );
/*	else
	if ( path == "." )
		path = m_sCurrentPath;*/
	else
	if ( path.at(0) != '/' )
		path.insert( 0, m_sCurrentPath );

	if ( path.at(path.length()-1) != '/' )
		path += "/";

	if ( ! isError(path, VFS::Readable) ) // and early is existable
		mDir.setPath( (m_sCurrentPath = path) );
}


void LFS::initListing()
{
	if ( mFIListIterator )
		delete mFIListIterator;

	QFileInfoList fileInfoList = mDir.entryInfoList();

	mFIListIterator = new QListIterator<QFileInfo>( fileInfoList );

	if ( m_bAsynchronouslyListing ) // asynchronously - one after another (very slow)
		return;

	//slotListing(); // asynchronously by portion
	// --- synchronously files listing (is much faster than asynchronously)
    while( mFIListIterator->hasNext() )
    {
        //mFI = mFIListIterator->next(); - moved into getListedItem
        getListedItem();
    }
}


void LFS::slotListing()
{
	m_nListingFilesCounter   = 0;
	const uint maxFilesNum = 100;

    while( mFIListIterator->hasNext() && m_nListingFilesCounter < maxFilesNum )
    {
		m_nListingFilesCounter++;
		getListedItem();
	}
	//if ( mFI != 0 )
    if (mFIListIterator->hasNext())
		QTimer::singleShot( 0, this, SLOT(slotListing()) );
	else
		jobFinished();
}


void LFS::getListedItem()
{
	if ( ! mFIListIterator->hasNext() )
		return;

    mFI = mFIListIterator->next();
	if (mFI.fileName() != "." && mFI.fileName() != "..") {
		bool isEmptyDir = FALSE;
		if (mFI.isDir() && ! mFI.isSymLink()) {
			if (mFI.size() == 48) // it's true only for non Windows/DOS FileSystems.
				isEmptyDir = TRUE;
		}

		UrlInfoExt ui(
			mFI.fileName(),
			mFI.isDir() ? 0 : (mFI.isSymLink() ? mFI.readLink().length() : mFI.size()),
			FileInfoExt::permission( (m_sCurrentPath+mFI.fileName()) ),
			mFI.owner().isEmpty() ? QString::number(mFI.ownerId()) : mFI.owner(), // mFI.owner() // mFI it's FileInfoExt
			mFI.group().isEmpty() ? QString::number(mFI.groupId()) : mFI.group(), // mFI.group() // mFI it's FileInfoExt
			mFI.lastModified(), mFI.lastRead(),
			mFI.isDir(),        mFI.isFile(),  mFI.isSymLink(),
			mFI.isReadable(),   mFI.isExecutable(), isEmptyDir, mFI.readLink()
		);
		ui.setLocation(m_sCurrentPath);
		emit listInfo(ui);
	}

	if ( ! m_bAsynchronouslyListing )
		return;

	if ( mFIListIterator->hasNext() )
		mOperationTimer.start( 0, TRUE );
	else
		currentCommandFinished();
}


void LFS::list( const QString & path, bool asynchr )
{
	m_bAsynchronouslyListing = asynchr;
	m_eCurrentOperation = VFS::List;

	if ( ! asynchr ) {
		cd( path );
		emit commandFinished( VFS::Cd, (m_eErrorCode!=VFS::NoError) );
		if ( m_eErrorCode == VFS::NoError ) {
			m_eCurrentCommand = VFS::List;
			emit commandStarted( VFS::List );
			initListing(); // synchronously listing
		}
		jobFinished();
		return;
	}
	// ---
	if ( ! illegalFileName(path) )
		addCommand( VFS::Cd, path );

	addCommand( VFS::List, path );
}


void LFS::touch( const QString & sFileName )
{
	m_eCurrentOperation = VFS::Touch;

	if ( isError(sFileName, VFS::Exists) ) { // if a file not exists then try to create it
		if ( ! isError(FileInfoExt::filePath(sFileName), VFS::Writable) ) { // then create empty file
			QFile emptyFile( sFileName );
			emptyFile.open( QIODevice::ReadWrite | QIODevice::Append );
			emptyFile.close();
			m_sNameOfProcessedFile = sFileName;
		}
	}
	else { // file exists
		m_sNameOfProcessedFile = sFileName;
		m_eErrorCode = VFS::NoError;
	}
	emit commandFinished( VFS::Touch, (m_eErrorCode!=VFS::NoError) );
}


void LFS::makeDir( const QString & dirName )
{
	m_eCurrentOperation = VFS::MakeDir;

	if ( isError(dirName, VFS::Exists) ) {
		QString targetPath;
		m_eErrorCode = VFS::NoError;
		QStringList dirsList = QStringList::split( '/', dirName );
		for ( int i=0; i<dirsList.count(); i++ ) {
			targetPath += "/"+dirsList[i];
			if ( ! QFileInfo(targetPath).exists() )
				if ( ! mDir.mkdir(targetPath, TRUE) ) {
					m_sNameOfProcessedFile = FileInfoExt::filePath( targetPath );
					m_eErrorCode = VFS::CannotWrite;
					break;
				}
		}
		if ( targetPath.contains(m_sCurrentPath) && m_eErrorCode == VFS::NoError ) {
			int id = targetPath.find('/', m_sCurrentPath.length()+1 );
			targetPath.remove( id, targetPath.length()-id );
			m_sNameOfProcessedFile = targetPath;
		}
	}
	else {
		m_eErrorCode = VFS::NoError; // disable shows dialog -> "directory already exists"
		m_sNameOfProcessedFile = dirName;
	}
	emit commandFinished( VFS::MakeDir, (m_eErrorCode!=VFS::NoError) );
}


bool LFS::rename( const QString & oldName, const QString & newName )
{
	m_eCurrentOperation = VFS::Rename;

	if ( ! isError(oldName, VFS::Exists) )
		if ( isError(newName, VFS::Exists) ) {
			m_eErrorCode = VFS::NoError;
			if ( ! isError(m_sCurrentPath, VFS::Writable) ) {
				m_sNameOfProcessedFile = absoluteFileName(oldName); // for FilesPanel::slotResultOperation
				mFInfo.setFile( m_sNameOfProcessedFile );

				if ( mFInfo.isDir() && ! mFInfo.isSymLink() )
					m_sNameOfProcessedFile += "/";
				if ( ! mDir.rename(oldName, newName) ) {
					m_sNameOfProcessedFile = FileInfoExt::filePath(oldName);
					m_eErrorCode = VFS::CannotWrite; // to dir with file 'oldName'
				}
			}
		}

	return (m_eErrorCode!=VFS::NoError);
}


void LFS::copy( const QString & sourceFileName, const QString & targetFileName, bool move )
{
	if ( sNotReseted ) { // call only one time
		m_eCurrentOperation = move ? VFS::Move : VFS::Copy;
		if ( move ) {
			m_bRecursivelyRemoveAllDirs = TRUE;
			m_nDirCounterR = 0;  m_nFileCounterR = 0;
		}
		sNotReseted = FALSE;
	}
	if ( m_bWeightBefore ) {
		addCommand( VFS::Weigh, sourceFileName );
		m_nTotalFiles = 0;
	}

	addCommand( VFS::Get, sourceFileName );
	addCommand( VFS::Put, targetFileName );

	if ( move )
		addCommand( VFS::Remove, sourceFileName );
}


void LFS::remove( const QString & sFileName )
{
	if ( m_bRemoveToTrash )
		copy( sFileName, m_sTrashPath+FileInfoExt::fileName(sFileName), TRUE ); // move to the trash
	else { // real remove
		if ( sNotReseted ) { // call only one time
			m_nFileCounterR = 0; m_nDirCounterR = 0;
			m_eCurrentOperation = VFS::Remove;
			sNotReseted = FALSE;
		}
		if ( m_bWeightBefore ) {
			addCommand( VFS::Weigh, sFileName );
			m_nTotalFiles = 0;
		}
		addCommand( VFS::Remove, sFileName );
	}
}


void LFS::weigh( const QString & sFileName )
{
	if ( sNotReseted ) { // call only one time
		m_nTotalFiles = 0;
		sNotReseted = FALSE;
		clearFilesCounter();
		m_eCurrentOperation = VFS::Weigh;
		m_bRecursiveAttributs = TRUE;
		m_bNoneOverwriting = FALSE;
		m_bSkipCurrentFile = FALSE;
	}
	addCommand( VFS::Weigh, sFileName );
}


void LFS::makeLink( const QString & sourceFileName, const QString & targetFileName, bool editLink )
{
	if ( sNotReseted ) { // call only one time
		m_eCurrentOperation = VFS::MakeLink;
		m_bRecursiveAttributs = FALSE;
		m_bLinkEdit = editLink;
		sNotReseted = FALSE;
	}
	if ( m_bWeightBefore && ! editLink ) {
		addCommand( VFS::Weigh, sourceFileName );
		m_nTotalFiles = 0;
	}

	addCommand( VFS::MakeLink );
	addCommand( VFS::Get, sourceFileName );
	addCommand( VFS::Put, targetFileName );
}


void LFS::setAttributs( const QString & sFileName )
{
	if ( sNotReseted ) { // call only one time
		m_eCurrentOperation = VFS::SetAttributs;
		sNotReseted = FALSE;
	}
	if ( m_bWeightBefore ) {
		addCommand( VFS::Weigh, sFileName );
		m_nTotalFiles = 0;
	}

	addCommand( VFS::SetAttributs, sFileName );
}


void LFS::findFile( const FindCriterion & findCriterion )
{
	if ( sNotReseted ) { // reset only one time
		m_eCurrentOperation = VFS::Find;
		mFindCriterion = findCriterion;
		sNotReseted = FALSE;
	}
	if ( m_bWeightBefore ) {
		addCommand( VFS::Weigh, mFindCriterion.name );
		m_nTotalFiles = 0;
	}

	addCommand( VFS::Find, mFindCriterion.name );
}


bool LFS::removeNextFile()
{
	if ( ! isError(m_sSourceFileName, VFS::Exists) ) {
		emit signalNameOfProcessedFiles( m_sSourceFileName );
		mFInfo.setFile( m_sSourceFileName );
		m_eErrorCode = VFS::NoError;
		if ( mFInfo.isFile() || mFInfo.isSymLink() )
			removeFile( m_sSourceFileName );
		else {
			removeDir( m_sSourceFileName );
			return FALSE;
		}
	}
	return TRUE;
}


void LFS::removeFile( const QString & sFileName )
{
	m_eErrorCode = QFile::remove(sFileName) ? VFS::NoError : VFS::CannotRemove;
}


void LFS::removeDir( const QString & dirName )
{
	m_eErrorCode = VFS::NoError;
	if ( isError(FileInfoExt::filePath(dirName), VFS::Writable) )
        if (SystemInfo::getCurrentUserID() != 0/*root*/ && mFInfo.owner() != SystemInfo::getCurrentUserName())
			m_eErrorCode = VFS::CannotRemove;

	if ( dirIsEmpty(dirName) ) {
		if ( mDir.rmdir(dirName) ) {
			emit signalFileCounterProgress( ++m_nDirCounterR, FALSE ); // update dir counter
			m_nFileCounterR--; // becouse this counter is increment into below call function
			currentCommandFinished();
			return;
		}
		else
			m_eErrorCode = VFS::CannotRemove;
	}

	if ( m_eErrorCode != VFS::NoError ) {
		currentCommandFinished();
		return;
	}
	int result=-1;
	if ( m_eCurrentOperation == VFS::Remove ) {
		if ( ! m_bRecursivelyRemoveAllDirs && ! m_bNoneRecursivelyRemoveDir ) {
			emit signalDirectoryNotEmpty( dirName, result ); // shows dialog
			m_bSkipCurrentFile = (result == MessageBox::No);
			if ( result == MessageBox::All )
				m_bRecursivelyRemoveAllDirs = TRUE;
			else
			if ( result == MessageBox::None )
				m_bNoneRecursivelyRemoveDir = TRUE;
			else
			if ( result == MessageBox::Cancel || result == 0 ) {
				m_eErrorCode = VFS::BreakOperation;
				return;
			}
		}
		if ( m_bSkipCurrentFile || m_bNoneRecursivelyRemoveDir ) {
			// remove an item (current dir - 'm_sNameOfProcessedFile') from the SelectedItemsList
			m_eErrorCode = VFS::BreakOperation;
			emit commandFinished( m_eCurrentCommand, FALSE );
			m_eErrorCode = VFS::NoError;
		}
		if ( ! m_bNoneRecursivelyRemoveDir && result != MessageBox::No )
			recursiveOperation( REMOVE, dirName );
		else
			currentCommandFinished();
	}
	else
		recursiveOperation( REMOVE, dirName );
}


void LFS::putFile( const QByteArray & /*data*/, const QString & /*file*/ )
{
	qDebug("LFS::putFile, not implemented yet");
}


bool LFS::initGetFile( long long fileSize )
{
	bool isSymLink = mFInfo.isSymLink();
	if ( isError(m_sSourceFileName, VFS::Readable) && ! isSymLink )
		return FALSE;

	m_eErrorCode = VFS::NoError;

	if ( fileSize < 0 ) {
		if ( ! isSymLink )
			m_nSourceFileSize = QFile( m_sSourceFileName ).size();
		else
			m_nSourceFileSize = mFInfo.readLink().length();
	}
	else
		m_nSourceFileSize = fileSize;

	if ( ! isSymLink ) {
		mFileIn.setName( m_sSourceFileName );
		if ( ! mFileIn.open(QIODevice::ReadOnly) ) {
			m_sNameOfProcessedFile = m_sSourceFileName;
			m_eErrorCode = VFS::CannotRead;
		}
	}

	return (m_eErrorCode==VFS::NoError);
}


bool LFS::initPutFile()
{
	// --- checking amount of free bytes in target directory
	long long freeBytes, totalBytes;
	SystemInfo::getStatFS( FileInfoExt::filePath(m_sTargetFileName), freeBytes, totalBytes );
	if ( freeBytes < m_nSourceFileSize ) { // m_nSourceFileSize is init.into 'initPutFile()'
		m_sNameOfProcessedFile = FileInfoExt::filePath(m_sTargetFileName);
		m_eErrorCode = VFS::NoFreeSpace;
		return FALSE;
	}
	m_nWritedBytes = 0;
	// --- checking whether overwrite a file is possible
	enum FileOverwriteBtns { Yes=1, No, DiffSize, Rename, All, Update, None, Cancel };
	int  result = -1;
	bool isSymLink = mFInfo.isSymLink();
	bool nonMakeTargetFile = FALSE, renameTargetFile = FALSE;
	if ( ! m_bAlwaysOverwrite ) {
		if ( m_eUpdatingFilesWith == NONEupd ) {
			if ( ! isError(m_sTargetFileName, VFS::Exists) ) {
				if ( ! m_bNoneOverwriting ) { // shows query's dialog
					emit signalDataTransferProgress( 0, m_nSourceFileSize, 0 );
					emit signalFileOverwrite( m_sSourceFileName, m_sTargetFileName, result );
					if ( result == 0 ) // a dialog has been closed
						result = Cancel;
				}
				else { // none overwriting
					if ( m_eErrorCode == VFS::AlreadyExists )
						nonMakeTargetFile = TRUE;
				}
			}
		}
		m_eErrorCode = VFS::NoError;
		if ( result != -1 && ! renameTargetFile ) {
			if ( result == No )
				nonMakeTargetFile = TRUE;
			else
			if ( result == DiffSize )
				m_eUpdatingFilesWith = SIZEupd;
			else
			if ( result == Rename )
				renameTargetFile = TRUE;
			else
			if ( result == All )
				m_bAlwaysOverwrite = TRUE;
			else
			if ( result == Update )
				m_eUpdatingFilesWith = DATEupd;
			else
			if ( result == None ) {
				m_bNoneOverwriting = TRUE; // disable to show OverwritingDialog
				//return FALSE; // need to disable 'slotFileProcessing()'
			}
			else
			if ( result == Cancel ) {
				m_eErrorCode = VFS::BreakOperation;
				jobFinished();
				return FALSE;
			}
		} // result != -1
	}

	if ( m_eUpdatingFilesWith == DATEupd ) {
		QDateTime UTCTime( QDate(1970,1,1), QTime(0,0) );
		uint sourceFileTime = UTCTime.secsTo( QFileInfo(m_sSourceFileName).lastModified() );
		uint targetFileTime = UTCTime.secsTo( QFileInfo(m_sTargetFileName).lastModified() );
		nonMakeTargetFile = (sourceFileTime <= targetFileTime);
	}
	if ( m_eUpdatingFilesWith == SIZEupd )
		nonMakeTargetFile = (m_nSourceFileSize == QFileInfo(m_sTargetFileName).size());

	if ( (m_eUpdatingFilesWith != NONEupd && nonMakeTargetFile) || m_bNoneOverwriting ) {
		// remove current file from the selected list it's need to disable inserting after copying it
		m_eErrorCode = VFS::BreakOperation;
		m_sNameOfProcessedFile = m_sSourceFileName;
		emit commandFinished( m_eCurrentCommand, FALSE ); // remove an item from the SelectedItemsList
		m_eErrorCode = VFS::NoError;
		if ( m_bNoneOverwriting )
			return FALSE; // need to disable 'slotFileProcessing()'
	}

	if ( nonMakeTargetFile ) {
		m_bSkipCurrentFile = TRUE;
		if ( m_eRecursiveOp != NONE ) { // bo w slocie slotDirProcessing nie jest wyw. ponizsza fun. (jest specjalnie omijana)
			currentCommandFinished();
			m_bSkipCurrentFile = FALSE;
		}
		else { // need to remove current file from the SelectedItemsList
			m_eErrorCode = VFS::BreakOperation;
			emit commandFinished( m_eCurrentCommand, FALSE );
			m_eErrorCode = VFS::NoError;
		}
		return FALSE; // need to disable processing current file
	}
	else
	if ( (! renameTargetFile && result != -1) || m_bAlwaysOverwrite ) { // overwriting - need to remove existing target file
		removeFile( m_sTargetFileName );
		if ( m_eErrorCode != VFS::NoError ) {
			if ( m_eRecursiveOp != NONE ) // bo w slocie slotDirProcessing nie jest wyw. ponizsza fun. (jest specjalnie omijana)
				currentCommandFinished();
			return FALSE;
		}
	}

	if ( isSymLink || m_eCurrentCommand == VFS::MakeLink ) {
		mFInfo.setFile( m_sSourceFileName );
		initMakeLink();
	}
	else {
		if ( renameTargetFile ) {
			result = Rename;
			while ( ! isError(m_sTargetFileName, VFS::Exists) ) {
				m_sNameOfProcessedFile = m_sTargetFileName;
				emit commandFinished( m_eCurrentCommand, TRUE ); // shows error announcement
				emit signalFileOverwrite( m_sSourceFileName, m_sTargetFileName, result ); // shows the RenameDlg
			}
			m_eErrorCode = VFS::NoError;
		}
		// --- make a new target file
		mFileOut.setName( m_sTargetFileName );
		if ( ! mFileOut.open(QIODevice::WriteOnly | QIODevice::Append) ) {
			m_eErrorCode = VFS::CannotWrite;
			return FALSE;
		}
		if ( m_nSourceFileSize == 0 ) // an empty file already has been created (by 'open()')
			emit signalDataTransferProgress( 1,1,0 );
		if ( renameTargetFile ) { // need to insert item before it has been copying
			; // ERROR, cmd.Put inserts, but in a current panel !
		}
	}

	return (m_eErrorCode==VFS::NoError);
}


bool LFS::initCopiedFiles( long long fileSize )
{
	if ( m_eRecursiveOp == NONE )
		emit signalNameOfProcessedFiles( m_sSourceFileName, m_sTargetFileName );

	mFInfo.setFile( m_sSourceFileName );
	if ( m_eCurrentCommand != VFS::MakeLink ) {
		if ( isError(m_sSourceFileName, VFS::Exists) )
			return FALSE;
		else
			m_eErrorCode = VFS::NoError;
	}

	bool stat = TRUE;
	if ( mFInfo.isFile() || mFInfo.isSymLink() || m_eCurrentCommand == VFS::MakeLink ) {
		if ( (stat=initGetFile(fileSize)) )
			if ( (stat=initPutFile()) )
				slotFileProcessing();
	}
	else
		stat = recursiveOperation( COPY, m_sSourceFileName, m_sTargetFileName );

	return stat;
}


void LFS::initCopying()
{
	m_bSkipCurrentFile = FALSE;
	m_bNoneOverwriting = FALSE;
	m_eUpdatingFilesWith = NONEupd;
	emit signalTotalProgress( 0, m_nTotalWeight );
	clearFilesCounter();

	if ( m_eCurrentOperation == VFS::MakeLink )
		return;

	if ( m_pFileBuffer )
		delete m_pFileBuffer;
	m_pFileBuffer = new char[ m_nFileBufferSize ];
}


void LFS::copyingFile()
{
	if ( m_eErrorCode == VFS::BreakOperation || m_nSourceFileSize == 0 || mFInfo.isSymLink() ) {
		currentCommandFinished();
		return;
	}
	int realReadBytes = mFileIn.readBlock( m_pFileBuffer, m_nFileBufferSize );

	if ( realReadBytes < 0 ) { // an error occured
		m_sNameOfProcessedFile = m_sSourceFileName;
		m_eErrorCode = VFS::ReadError;
	}
	else {
		uint realWriteBytes = mFileOut.writeBlock( m_pFileBuffer, realReadBytes );
		m_nWritedBytes += realWriteBytes;
		emit signalDataTransferProgress( m_nWritedBytes, m_nSourceFileSize, realWriteBytes );
	}

	if ( realReadBytes < m_nFileBufferSize )
		currentCommandFinished();
	else
		mOperationTimer.start( 0, TRUE );
}


void LFS::getFileFinished()
{
	if ( m_eErrorCode == VFS::BreakOperation ) {
		int result = MessageBox::yesNo( this,
		 tr("Breaked copying")+" - QtCommander",
		 m_sTargetFileName+"\n\n\t"+formatNumber(m_nWritedBytes,BKBMBGBformat)+" "+
		 tr("bytes have been copied.")+"\n\n\t"+tr("Remove this file")+" ?",
		 MessageBox::Yes
		);
		if ( result == MessageBox::Yes )
			removeFile( m_sTargetFileName ); // not asynchronously remove
	}
	mFileIn.close();
}


void LFS::putFileFinished()
{
	if ( m_bSkipCurrentFile )
		return;

	mFileOut.close();

	if ( m_eErrorCode == VFS::NoError ) {
		if ( ! mFInfo.isSymLink() ) {
			if ( m_bSavePermission ) {
				::lstat( QFile::encodeName(m_sSourceFileName), & mStatBuffer );
				if ( setFileTime(m_sTargetFileName, mStatBuffer.st_atime, mStatBuffer.st_mtime) )
					setFilePermission( m_sTargetFileName, FileInfoExt::permission(m_sSourceFileName) );
			}
		}
		emit signalFileCounterProgress( ++m_nFileCounter, TRUE );
	}
}


bool LFS::initMakeLink()
{
// 	if ( m_eCurrentCommand != MakeLink ) // mFInfo.isSymLink()
// 		mHardLind = ( isHardLink(m_sSourceFileName) ) ? TRUE : FALSE;

	if ( mFInfo.isSymLink() )
		m_sSourceFileName = mFInfo.readLink();

	bool stat = makeNextLink( m_sSourceFileName, m_sTargetFileName );

	if ( m_eCurrentCommand == VFS::MakeLink )
		mLFSCommandList.removeFirst(); // remove Get

	emit signalDataTransferProgress( 1,1,m_sSourceFileName.length() );

	return stat;
}


bool LFS::makeNextLink( const QString & sourceFileName, const QString & targetFileName, bool checkErrors )
{
	if ( checkErrors ) {
		if ( isError(sourceFileName, VFS::Readable) )
			return FALSE;
		if ( isError(FileInfoExt::filePath( targetFileName ), VFS::Writable) )
			return FALSE;
	}

	if ( m_bAlwaysOverwrite ) { // overwrite the target file
		m_sNameOfProcessedFile = targetFileName;
		removeFile( targetFileName );
		if ( m_eErrorCode != VFS::NoError )
			return FALSE;
	}
	else {
		if ( ! isError(targetFileName, VFS::Exists) )
			return FALSE;
		else
			m_eErrorCode = VFS::NoError;
	}

	if ( ! m_bHardLink ) {
		if ( ::symlink(QFile::encodeName(sourceFileName), QFile::encodeName(targetFileName)) != 0 ) {
			qDebug("symlink error=%d", errno );
			m_eErrorCode = VFS::CannotCreateLink;
			return FALSE;
		}
	}
	else
	if ( ::link(QFile::encodeName(sourceFileName), QFile::encodeName(targetFileName)) != 0 ) {
		qDebug("link error=%d", errno );
		m_eErrorCode = VFS::CannotCreateLink;
		return FALSE;
	}

	return TRUE;
}


bool LFS::recursiveOperation( RecursiveOperation recursiveOp, const QString & sourceDirName, const QString & targetDirName )
{
	m_bWasDir = TRUE;
	m_nDirInode.clear();
	//m_nDirInode = NULL;
	m_eErrorCode = VFS::NoError;
	m_eRecursiveOp = recursiveOp;
	m_sAbsSrcFileName = sourceDirName;
	if ( sourceDirName.at(sourceDirName.length()-1) == '/' && sourceDirName != "/" )
		m_sAbsSrcFileName.remove( sourceDirName.length()-1, 1 );
	m_sAbsSrcDirName  = m_sAbsSrcFileName;
	DIR *mainDirPtr = NULL;

	if ( recursiveOp == COPY ) {
		m_sAbsTargetFName   = targetDirName;
		m_sAbsTargetDirName = m_sAbsTargetFName;
		m_sAbsSrcDirName    = m_sAbsSrcFileName;

		if ( ! QFileInfo(m_sAbsTargetDirName).exists() )
			if ( ! mDir.mkdir(m_sAbsTargetDirName) ) {
			//if ( ::mkdir( QFile::encodeName(m_sAbsTargetDirName), 0755 ) == -1 ) {// make main copied directory
				// The 'Get' and 'Put' actions are removed into a currentCommandFinished() fun.
				m_sTargetFileName = FileInfoExt::filePath(targetDirName); // becouse into currentCommandFinished will get is this value
				m_eErrorCode = VFS::CannotWrite;
				m_eRecursiveOp = NONE;
				return FALSE;
			}
		m_nOpFileCounter++;
		mLFSCommandList.removeFirst(); // remove Get
		m_sNameOfProcessedFile = m_sTargetFileName; // need to correct inserting an item
		emit signalFileCounterProgress( ++m_nDirCounter, FALSE );
	}
	else
	if ( recursiveOp == FIND ) {
		m_bFindAll = (mFindCriterion.kindOfFiles == FindCriterion::ALL);
		mFindRegExp = QRegExp( mFindCriterion.name, mFindCriterion.caseSensitiveName, TRUE );
	}

	if ( (mainDirPtr=::opendir(QFile::encodeName(m_sAbsSrcDirName))) == 0 ) { // only for readable checks
		qDebug("opendir error=%d, directory='%s'", errno, sourceDirName.toLatin1().data() );
		m_eErrorCode = QFileInfo(m_sAbsSrcDirName).exists() ? VFS::CannotRead : VFS::NotExists;
		m_eRecursiveOp = NONE;
		return FALSE;
	}
	else
		::closedir( mainDirPtr ); // in slotStartProcessDir() dir is opened again

	slotStartProcessDir();

	return TRUE;
}


bool LFS::setFileTime( const QString & sFileName, int secondsToAccess, int secondsToModification )
{
	if ( secondsToAccess < 0 && secondsToModification < 0 )
		return TRUE;

	utimbuf uTimeBuf;
	uTimeBuf.actime  = secondsToAccess;
	uTimeBuf.modtime = secondsToModification;

	if ( m_bFollowByLink ) {
		mFInfo.setFile( sFileName );
		QString fName;
		if ( mFInfo.isSymLink() )
			fName = mFInfo.readLink();
		else
			fName = sFileName;

		if ( ::utime(QFile::encodeName(fName), &uTimeBuf) != 0 ) {
			m_eErrorCode = VFS::CannotSetAttributs;
			qDebug("utime error=%d, file=%s", errno, fName.toLatin1().data() );
			return FALSE;
		}
	}
	else
	if ( ::utime(QFile::encodeName(sFileName), &uTimeBuf) != 0 ) {
		m_eErrorCode = VFS::CannotSetAttributs;
		qDebug("utime error=%d, file=%s", errno, sFileName.toLatin1().data() );
		return FALSE;
	}

	return TRUE;
}


bool LFS::setFilePermission( const QString & sFileName, int permission )
{
	if ( permission < 0 )
		return TRUE;

	if ( m_bFollowByLink ) {
		mFInfo.setFile( sFileName );
		QString fName;
		if ( mFInfo.isSymLink() )
			fName = mFInfo.readLink();
		else
			fName = sFileName;

		if ( ::chmod(QFile::encodeName(fName), permission) != 0 ) {
			m_eErrorCode = VFS::CannotSetAttributs;
			qDebug("chmod error=%d, file=%s", errno, fName.toLatin1().data() );
			return FALSE;
		}
	}
	else
	if ( ::chmod(QFile::encodeName(sFileName), permission) != 0 ) {
		m_eErrorCode = VFS::CannotSetAttributs;
		qDebug("chmod error=%d, file=%s", errno, sFileName.toLatin1().data() );
		return FALSE;
	}

	return TRUE;
}


bool LFS::sizeNextFile()
{
	emit signalNameOfProcessedFiles( m_sSourceFileName );

	if ( ! isError(m_sSourceFileName, VFS::Exists) )
		m_eErrorCode = VFS::NoError;
	else
		return TRUE;

	mFInfo.setFile( m_sSourceFileName );
	if ( mFInfo.isFile() || mFInfo.isSymLink() || ! m_bRecursiveAttributs )
		m_nTotalWeight += fileSize( m_sSourceFileName );
	else { // dir
		recursiveOperation( WEIGH, m_sSourceFileName );
		return FALSE;
	}
	return TRUE;
}


long long LFS::fileSize( const QString & sFileName )
{
	long long fSize;
	mFInfo.setFile( sFileName );

	if ( mFInfo.isSymLink() ) {
		if ( m_bFollowByLink ) // get file size
			fSize = QFile( mFInfo.readLink() ).size();
		else // get link size (length of target path)
			fSize = ( mFInfo.readLink() ).length();
	}
	else
		fSize = mFInfo.size();

	return fSize;
}


bool LFS::setAttributsNextFile()
{
	if ( m_bRecursiveAttributs ) {
		recursiveOperation( SET_ATTRIBUTES, m_sSourceFileName );
		return FALSE;
	}
	else {
		if ( ! m_sSourceFileName.isEmpty() ) {
			if ( m_nPermissionForAll > -1 )
				setFilePermission(m_sSourceFileName, m_nPermissionForAll);
			if ( m_nAccessTimeForAll > -1 || m_nModificationTimeForAll > -1 )
				setFileTime(m_sSourceFileName, m_nAccessTimeForAll, m_nModificationTimeForAll);
			if ( m_nOwnerIdForAll > -1 || m_nGroupIdForAll > -1 )
				setFileOwnerAndGroup( m_sSourceFileName, m_nOwnerIdForAll, m_nGroupIdForAll );
		}
	}
	return TRUE;
}


void LFS::insertMatchedItem()
{
	QDateTime timeAccess, timeModified;
	timeAccess.setTime_t(mStatBuffer.st_atime);
	timeModified.setTime_t(mStatBuffer.st_mtime);

	UrlInfoExt urlInfo(
	 m_sAbsSrcFileName, mStatBuffer.st_size, FileInfoExt::permission(m_sAbsSrcFileName),
     SystemInfo::getUserName(mStatBuffer.st_uid), SystemInfo::getGroupName(mStatBuffer.st_gid),
	 timeModified, timeAccess,
	 S_ISDIR(mStatBuffer.st_mode), S_ISREG(mStatBuffer.st_mode), S_ISLNK(mStatBuffer.st_mode),
	 FALSE, FALSE // there isReadable and isExecutable are dummy
	);

	emit signalUpdateFindStatus( ++m_nMatchedFiles, m_nFileCounter, m_nDirCounter );
	emit signalInsertMatchedItem( urlInfo );
}


bool LFS::currentFileMatches()
{
	bool match = mFindRegExp.exactMatch( m_sCurrentFileName );

	if ( mFindCriterion.checkFileOwner )
		match = (mStatBuffer.st_uid == (uint)mFindCriterion.ownerId);
	if ( mFindCriterion.checkFileGroup )
		match = (mStatBuffer.st_gid == (uint)mFindCriterion.groupId);

	if ( mFindCriterion.checkTime ) {
		QDateTime fileTime = QFileInfo(m_sAbsSrcFileName).lastModified();
		match = ( fileTime >= mFindCriterion.firstTime && fileTime <= mFindCriterion.secondTime);
	}

	if ( mFindCriterion.checkFileSize != FindCriterion::NONE ) {
		int size = QFileInfo( m_sAbsSrcFileName ).size();
		if ( mFindCriterion.checkFileSize == FindCriterion::EQUAL )
			match = (size == mStatBuffer.st_size);
		if ( mFindCriterion.checkFileSize == FindCriterion::ATLEAST )
			match = (size >= mStatBuffer.st_size);
		if ( mFindCriterion.checkFileSize == FindCriterion::MAXIMUM )
			match = (size <= mStatBuffer.st_size);
	}

	if ( mFindCriterion.stopIfXMatched )
		if ( m_nMatchedFiles == mFindCriterion.stopAfterXMaches ) {
			match = FALSE;
			jobFinished();
		}

	return match;
}


bool LFS::setFileOwnerAndGroup( const QString & sFileName, const QString & owner, const QString & group )
{
	int ownerId = ( owner.isEmpty() ) ? -1 : m_nOwnerIdForAll;
	int groupId = ( group.isEmpty() ) ? -1 : m_nGroupIdForAll;

	return setFileOwnerAndGroup( sFileName, ownerId, groupId );
}


bool LFS::setFileOwnerAndGroup( const QString & sFileName, int ownerId, int groupId )
{
	if ( ownerId < 0 && groupId < 0 )
		return TRUE;

	if ( m_nCurrentUserID != 0 ) // if not superuser then check whether ownerId of sFileName is changed
		if ( ownerId > 0 && QFileInfo(sFileName).ownerId() != (uint)ownerId ) {
			m_eErrorCode = VFS::CannotSetAttributs; // becouse only superuser can change owner of sFileName
			return FALSE;
		}

	if ( m_bFollowByLink ) {
		mFInfo.setFile( sFileName );
		QString fName;
		if ( mFInfo.isSymLink() )
			fName = mFInfo.readLink();
		else
			fName = sFileName;

		if ( ::chown(QFile::encodeName(fName), ownerId, groupId) != 0 ) {
			qDebug("chown error=%d, file=%s, userID=%d", errno, sFileName.toLatin1().data(), m_nCurrentUserID );
			return FALSE;
		}
	}
	else
		if ( ::chown(QFile::encodeName(sFileName), ownerId, groupId) != 0 ) {
			qDebug("chown error=%d, file=%s, userID=%d", errno, sFileName.toLatin1().data(), m_nCurrentUserID );
			return FALSE;
		}

	return TRUE;
}


bool LFS::dirIsEmpty( const QString & dirName )
{
	bool emptyDir = FALSE;
	m_eErrorCode = VFS::NoError;

	if ( (m_pDIR_DirPtr=::opendir(QFile::encodeName(dirName+"/"))) != NULL ) {
		emptyDir = TRUE;
		while ( (m_pDirEntries=::readdir(m_pDIR_DirPtr)) != NULL ) {
			m_sCurrentFileName = m_pDirEntries->d_name;
			if ( m_sCurrentFileName != "." && m_sCurrentFileName != ".." ) {
				emptyDir = FALSE;
				break;
			}
		}
	}
	else
		m_eErrorCode = VFS::CannotRead;

	return emptyDir;
}


QString LFS::absoluteFileName( const QString & sFileName ) const
{
	if ( sFileName.at(0) == '/' )
		return sFileName;

	QString absFileName = sFileName;
	absFileName.insert( 0, m_sCurrentPath );

	return absFileName;
}


bool LFS::isError( const QString & sFileName, VFS::KindOfError kindOfError )
{
	m_eErrorCode = VFS::NoError;

	if ( illegalFileName(sFileName) ) {
		m_eErrorCode = VFS::IllegalName;
		return TRUE;
	}

	mFInfo.setFile( absoluteFileName(sFileName) );
	bool fileExists = mFInfo.exists();

	if ( kindOfError == VFS::Exists ) {
		if ( mFInfo.isSymLink() )
			return FALSE;

		if ( fileExists )
			m_eErrorCode = VFS::AlreadyExists;
		else
			m_eErrorCode = VFS::NotExists;

		return !fileExists;
	}
	else {
		if ( ! fileExists ) {
			if ( ! mFInfo.isSymLink() ) {
				m_eErrorCode = VFS::NotExists;
				m_sNameOfProcessedFile = absoluteFileName( sFileName );
				return TRUE;
			}
		}
	}

	bool noError = FALSE;
	if ( kindOfError == VFS::Readable ) {
		if ( ! (noError=mFInfo.isReadable()) )
			m_eErrorCode = VFS::CannotRead;
		if ( mFInfo.isDir() )
			if ( ! (noError=mFInfo.isExecutable()) )
				m_eErrorCode = VFS::CannotRead;
	}
	else
	if ( kindOfError == VFS::Writable ) {
		if ( ! (noError=mFInfo.isWritable()) )
			m_eErrorCode = VFS::CannotWrite;
	}

	if ( ! noError ) // an error occured
		m_sNameOfProcessedFile = absoluteFileName( sFileName );

	return !noError;
}


bool LFS::illegalFileName( const QString & sFileName )
{
	m_eErrorCode = VFS::NoError;

	if ( sFileName.isEmpty() )
		m_eErrorCode = VFS::IllegalName;

// implementation not finished

	return (m_eErrorCode!=VFS::NoError);
}


void LFS::getWeighingStat( long long & weight, uint & files, uint & dirs )
{
	weight = m_nTotalWeight;
	files  = m_nFileCounter;
	dirs   = m_nDirCounter;
}


void LFS::pauseCurrentOperation( bool pause )
{
	m_bPauseOperation = pause;

	if ( pause )
		mOperationTimer.stop();
	else {
		mOperationTimer.start( 0, TRUE ); // singleShot
		// jesli akurat skopiowal sie plik i nastapilo zatrzymanie, wznowienie, to operacja nie bedzie kontynuowana
		// trzebaby wprowadzic skladowa globalna 'bool' ustawiana gdy plik jest skopiowany na TRUE
		if ( (m_eCurrentCommand != VFS::Copy || m_eCurrentCommand != VFS::Move) && m_eRecursiveOp != NONE )
			QTimer::singleShot( 0, this, SLOT(slotDirProcessing()) ); // resume an operation
	}
}


void LFS::breakCurrentOperation()
{
	if ( m_eCurrentCommand != VFS::NoneOp && m_eRecursiveOp != NONE ) {
		m_eErrorCode = VFS::BreakOperation;
		m_bPauseOperation = FALSE;
		currentCommandFinished();
	}
}


void LFS::setAccessAndModificationTimeForAll( const QDateTime & accessTime, const QDateTime & modificationTime )
{
	QDateTime UTCTime( QDate(1970,1,1), QTime(0,0) );

	m_nAccessTimeForAll = (accessTime.isNull()) ? -1 : UTCTime.secsTo( accessTime );
	m_nModificationTimeForAll = (modificationTime.isNull()) ? -1 : UTCTime.secsTo( modificationTime );
}


bool LFS::setOwnerAndGroupForAll( const QString & owner, const QString & group )
{
	QStringList ownerList;
    ownerList << owner;
	QStringList groupList;
    groupList << group;
	m_sOwnerForAll = owner; m_sGroupForAll = group;

    int newOwnerId = SystemInfo::getCurrentUserID(); // get 'owner' Id
	int newGroupId = SystemInfo::getCurrentUserGroupID();  // get 'group' Id
	if ( (newOwnerId < 0 && ! owner.isEmpty()) || (newGroupId < 0 && ! group.isEmpty()) ) // not found Id for passed name
		m_eErrorCode = VFS::CannotSetAttributs;

	m_nOwnerIdForAll = (owner.isEmpty()) ? -1 : newOwnerId;
	m_nGroupIdForAll = (group.isEmpty()) ? -1 : newGroupId;

	return (m_eErrorCode==VFS::NoError);
}

		// ---------- SLOTs -----------

void LFS::slotStartNextJob()
{
	if ( mLFSCommandList.isEmpty() )
		return;

	VFS::Operation prevCmd = m_eCurrentCommand;
	LFSCommand *firstCmd = mLFSCommandList.getFirst();
	m_eCurrentCommand = firstCmd->command;
	m_sSourceFileName = m_sNameOfProcessedFile = firstCmd->sFileName;
	m_bSkipCurrentFile = FALSE;
	m_bPauseOperation  = FALSE;

	if ( mLFSCommandList.count() > 1 ) {
		LFSCommand *secondCmd = mLFSCommandList.at( 1 );
		if ( m_eCurrentCommand == VFS::Get && secondCmd->command == VFS::Put ) {
			m_eCurrentOperation = m_eCurrentCommand = VFS::Copy;
			m_sTargetFileName   = secondCmd->sFileName;
			if ( mLFSCommandList.count() > 2 ) // NNN
				if ( mLFSCommandList.at(2)->command == VFS::Remove )
					m_eCurrentOperation = m_eCurrentCommand = VFS::Move;
		}
		else
		if ( m_eCurrentCommand == VFS::MakeLink ) { // special case: empty MakeLink+Get+Put
			mLFSCommandList.removeFirst(); // remove MakeLink
			m_sSourceFileName = mLFSCommandList.at( 0 )->sFileName;
			m_sTargetFileName = m_sNameOfProcessedFile = mLFSCommandList.at( 1 )->sFileName;
		}
	}
	if ( prevCmd != m_eCurrentCommand )
		emit stateChanged( (int)m_eCurrentCommand ); // sets info on the progress dlg.
	if ( m_eCurrentCommand != VFS::Weigh && prevCmd != m_eCurrentCommand ) {
		emit signalFileCounterProgress( (m_eCurrentCommand == VFS::Remove) ? m_nFileCounterR : 0, TRUE, FALSE ); // update files counter
		emit signalFileCounterProgress( (m_eCurrentCommand == VFS::Remove) ? m_nDirCounterR : 0, FALSE, FALSE ); // update dirs counter
	}
	emit commandStarted( (int)m_eCurrentCommand );

	switch( m_eCurrentCommand ) {
		case VFS::Cd: {
			cd( m_sSourceFileName ); // ! param.nie potrzebny, bo jest globalny
			break;
		}
		case VFS::List: {
			if ( m_bInitOperation )
				initListing(); // there files will be listing when m_bAsynchronouslyListing == FALSE
			if ( m_bAsynchronouslyListing )
				getListedItem(); // start asynchronously listing
			else
				currentCommandFinished(); // finished synchronously listing
			return;
			break;
		}
		case VFS::Weigh: {
			if ( ! sizeNextFile() ) // get size of directory
				return; // a co jesli zaznaczono kilka kat. !
			if ( m_eErrorCode == VFS::BreakOperation ) {
				jobFinished();
				return;
			}
			break;
		}
		case VFS::Copy:
		case VFS::Move: {
			if ( m_bInitOperation )
				initCopying();
			m_eErrorCode = VFS::NoError;
			initCopiedFiles(); // and run copying file
			if ( m_eErrorCode == VFS::BreakOperation ) {
				jobFinished();
				return;
			}
			break;
		}
		case VFS::Remove: {
			if ( m_bInitOperation ) {
				clearFilesCounter();
				m_bRecursivelyRemoveAllDirs = FALSE;
				m_bNoneRecursivelyRemoveDir = FALSE;
			}
			if ( m_eErrorCode == VFS::BreakOperation ) { // user select 'Cancel'
				jobFinished();
				return;
			}
			if ( ! removeNextFile() ) // remove directory
				if ( m_eErrorCode != VFS::BreakOperation || m_eErrorCode == VFS::NoError )
					return;
			break;
		}
		case VFS::MakeLink: {
			if ( m_bInitOperation )
				initCopying();
			initCopiedFiles(); // make a link
			break;
		}
		case VFS::SetAttributs: {
			if ( m_bInitOperation )
				clearFilesCounter();
			if ( ! setAttributsNextFile() ) // set attr.for directory
				return;
			break;
		}
		case VFS::Find: {
			if ( m_bInitOperation )
				clearFilesCounter();
			if ( ! recursiveOperation( FIND, mFindCriterion.location ) ) {
				m_sNameOfProcessedFile = mFindCriterion.location;
				currentCommandFinished();
			}
			break;
		}
		default: break;
	}

	if ( m_eCurrentOperation != VFS::NoneOp && m_eCurrentCommand != VFS::Find && m_eRecursiveOp == NONE ) {
		if ( m_eCurrentCommand == VFS::Copy || m_eCurrentCommand == VFS::Move ) {
			if ( m_eErrorCode != VFS::NoError || m_nSourceFileSize == 0 || m_bSkipCurrentFile || m_bNoneOverwriting )
				currentCommandFinished();
		}
		else
			currentCommandFinished();
	}
}


void LFS::slotFileProcessing()
{
	switch( m_eCurrentCommand ) {
		case VFS::Copy:
		case VFS::Move:
			copyingFile();
			break;
		case VFS::List:
			getListedItem();
			break;

		default: break;
	}
}


void LFS::slotStartProcessDir()
{
	if ( m_bPauseOperation )
		return;

	m_bOkStat = ( ::lstat(QFile::encodeName(m_sAbsSrcFileName), & mStatBuffer) == 0);
	m_nDirInode.push((const int *)mStatBuffer.st_ino);
	m_bNeedToSeek = FALSE;

	if (! m_bWasDir) {
		if (mDirInodeStack.top() == NULL) {
			// --- recursive operation is finished
			// post operation actions (for main directory)
			if ( m_eRecursiveOp == WEIGH ) {
				m_nTotalFiles++; // add current
				m_nTotalWeight += (mStatBuffer.st_blksize/8)*mStatBuffer.st_blocks; // ((4096/8)*allocatedBlocks) add weigh of current
				// tylko dla mniejszych kat. rozm. 512 to prawda (mniejszych czyli ? :/)
				emit signalFileCounterProgress( ++m_nDirCounter, FALSE, TRUE );
			}
			else if ( m_eRecursiveOp == REMOVE ) {
				m_bOkStat = ( ::rmdir(QFile::encodeName(m_sAbsSrcFileName)) == 0 ); // attempt to remove empty(?) dir
				if ( ! m_bOkStat )
					m_eErrorCode = VFS::CannotRemove;
			} // if ( m_eErrorCode != NoError )
			else if ( m_eRecursiveOp == SET_ATTRIBUTES ) {
				if ( m_nPermissionForAll > -1 )
					m_bOkStat = setFilePermission( m_sAbsSrcFileName, m_nPermissionForAll );
				if ( m_bOkStat )
					if ( m_nAccessTimeForAll > -1 || m_nModificationTimeForAll > -1 )
						m_bOkStat = setFileTime( m_sAbsSrcFileName, m_nAccessTimeForAll, m_nModificationTimeForAll );
				if ( m_nOwnerIdForAll > -1 || m_nGroupIdForAll > -1 )
					m_bOkStat = setFileOwnerAndGroup( m_sAbsSrcFileName, m_nOwnerIdForAll, m_nGroupIdForAll );
				if ( ! m_bOkStat )
					m_eErrorCode = VFS::CannotSetAttributs;
			}
			else if ( m_eRecursiveOp == COPY ) {
				if ( m_bSavePermission ) {
					if ( (m_bOkStat=setFileTime(m_sAbsTargetDirName, mStatBuffer.st_atime, mStatBuffer.st_mtime)) )
						m_bOkStat = setFilePermission( m_sAbsTargetDirName, FileInfoExt::permission(m_sAbsSrcFileName) );
				}
			}
			// update an operation progress
			if ( m_eRecursiveOp != COPY && m_eRecursiveOp != FIND ) {
				if ( m_bOkStat )
					if ( m_eRecursiveOp != WEIGH ) {
						if ( !(m_eRecursiveOp == REMOVE && m_eCurrentCommand == VFS::Remove) || m_eCurrentOperation == VFS::Remove )  m_nOpFileCounter++;
						emit signalFileCounterProgress( (m_eCurrentCommand == VFS::Remove) ? ++m_nDirCounterR : ++m_nDirCounter, FALSE );
						if ( m_nTotalFiles )
							emit signalTotalProgress( (100 * m_nOpFileCounter) / m_nTotalFiles, m_nTotalWeight );
					}
			}
			// if one of above operation has finished by an error then show announcement
			if ( ! m_bOkStat ) {
				if ( m_eRecursiveOp == REMOVE || m_eRecursiveOp == SET_ATTRIBUTES )
					m_sNameOfProcessedFile = m_sAbsSrcFileName+"/";
				else
					m_sNameOfProcessedFile = m_sAbsTargetDirName+"/";
				emit commandFinished( m_eCurrentCommand, (m_eErrorCode!=VFS::NoError) );
			}
			// cleanig list and stacks
			if (! mLFSCommandList.isEmpty())
				mLFSCommandList.removeFirst(); // remove Put or an single action
			while (! mCurrLocInDIRstack.isEmpty())
				mCurrLocInDIRstack.pop();
			while (! mDirInodeStack.isEmpty())
				mDirInodeStack.pop();
			m_eRecursiveOp = NONE;
			while ( ! mDirPtrStack.isEmpty() )
				::closedir( (DIR *)mDirPtrStack.pop() );

			if ( mLFSCommandList.isEmpty() )
				jobFinished();
			else
				QTimer::singleShot( 0, this, SLOT(slotStartNextJob()) );

			return;
		}

		m_nThrowedOffInode.push(mDirInodeStack.pop());
		if ( m_nDirInode.current() == m_nThrowedOffInode.current() ) {
			if ( m_eRecursiveOp == REMOVE ) {
				m_bOkStat = ( ::rmdir(QFile::encodeName(m_sAbsSrcFileName)) == 0 ); // attempt to remove empty(?) dir
				if ( m_bOkStat ) {
					m_nOpFileCounter++;
					emit signalFileCounterProgress( (m_eCurrentCommand == VFS::Remove) ? ++m_nDirCounterR : ++m_nDirCounter, FALSE );
					emit signalTotalProgress( (100 * m_nOpFileCounter) / m_nTotalFiles, m_nTotalWeight );
				}
				else {
					m_sNameOfProcessedFile = m_sAbsSrcFileName;
					m_eErrorCode = VFS::CannotRemove;
				}
			}
			else
			if ( m_eRecursiveOp == COPY ) { // sets attributs for subdirectory
				if ( m_bSavePermission ) {
					if ( setFileTime(m_sAbsTargetDirName, mStatBuffer.st_atime, mStatBuffer.st_mtime) )
						setFilePermission( m_sAbsTargetDirName, FileInfoExt::permission(m_sAbsSrcFileName) );
					if ( m_eErrorCode != VFS::NoError ) {
						m_sNameOfProcessedFile = m_sAbsTargetDirName+"/";
						emit commandFinished( m_eCurrentCommand, (m_eErrorCode!=VFS::NoError) );
					}
				}
			}
			m_sAbsSrcFileName = m_sAbsSrcFileName.left( m_sAbsSrcFileName.findRev('/') ); // wejdz wyzej
			m_sAbsTargetFName = m_sAbsTargetFName.left( m_sAbsTargetFName.findRev('/') );
			if ( m_sAbsSrcFileName.isEmpty() )
				m_sAbsSrcFileName = "/";
			if ( m_sAbsTargetFName.isEmpty() )
				m_sAbsTargetFName = "/";
			m_sAbsTargetDirName = m_sAbsTargetFName;
			m_nCurrLocInDIR = (off_t)mCurrLocInDIRstack.pop();
			m_bNeedToSeek = TRUE;
		}
	}
	m_bWasDir = FALSE;
	m_eErrorCode = VFS::NoError;
	DIR *pTmpDirPtr = NULL; //, *pOldDirPtr = m_pDIR_DirPtr; // pOldDirPtr unused
	//if ( (m_pDIR_DirPtr=::opendir(QFile::encodeName(m_sAbsSrcFileName))) != NULL )
	if ( (pTmpDirPtr=::opendir(QFile::encodeName(m_sAbsSrcFileName))) != NULL ) {
		m_pDIR_DirPtr = pTmpDirPtr;
		mDirPtrStack.push( (int *)m_pDIR_DirPtr );
	}
	else {
		qDebug("opendir error=%d, dir='%s'", errno, m_sAbsSrcFileName.toLatin1().data() );
		m_sNameOfProcessedFile = m_sAbsSrcFileName;
		m_eErrorCode = VFS::CannotRead;
		currentCommandFinished(); // need to show error announcement
		mCurrLocInDIRstack.pop();
		mDirInodeStack.pop();
		mDirPtrStack.pop();
		if ( (pTmpDirPtr=::opendir(QFile::encodeName(m_sAbsSrcFileName))) != NULL ) {
			m_pDIR_DirPtr = pTmpDirPtr;
			mDirPtrStack.push( (int *)m_pDIR_DirPtr );
		}
		m_bNeedToSeek = TRUE;
	}
	if (m_eErrorCode == VFS::NoError)
	{
		if ( (m_bOkStat=(::chdir(QFile::encodeName(m_sAbsSrcFileName)) != 0 )) ) {
			qDebug("chdir error=%d, dir='%s'", errno, m_sAbsSrcFileName.toLatin1().data() );
			m_eErrorCode = VFS::CannotRead;
			if (mDirInodeStack.top() == NULL) { // cannot chdir to main op.dir.
				jobFinished();
				return;
			}
			else {
				m_sAbsSrcFileName = FileInfoExt::filePath(m_sAbsSrcDirName);
				mCurrLocInDIRstack.pop();
				mDirInodeStack.pop();
				mDirPtrStack.pop();
				m_bNeedToSeek = TRUE;
			}
		}
	}
	if ( m_bNeedToSeek )
		::seekdir(m_pDIR_DirPtr, m_nCurrLocInDIR);
	else
		m_nWasDots = -1;

	slotDirProcessing();
}


void LFS::slotDirProcessing()
{
	if (m_pDIR_DirPtr != NULL) {
		if ( (m_pDirEntries=::readdir(m_pDIR_DirPtr)) == NULL ) { // all files processed
			QTimer::singleShot( 0, this, SLOT(slotStartProcessDir()) );
// 			while ( ! mDirInodeStack.isEmpty() ) // clear made CRASH
// 				mDirInodeStack.pop();
			return;
		}
		m_sCurrentFileName = QFile::decodeName( m_pDirEntries->d_name );
	}
	if ( m_nWasDots < 1 )
		if ( m_sCurrentFileName == "." || m_sCurrentFileName == ".." ) {
			m_nWasDots++;
			slotDirProcessing();
			return;
		}

	if (m_bPauseOperation)
		return;
	m_bOkStat = (::lstat(QFile::encodeName(m_sCurrentFileName), & mStatBuffer) == 0);

	if (m_pDIR_DirPtr == NULL)
		m_bWasDir = S_ISDIR(mStatBuffer.st_mode);
	else {
		m_bWasDir = FALSE;
		m_sAbsSrcFileName += "/"+m_sCurrentFileName; // element do otwarcia
		m_eErrorCode = VFS::NoError;
	}
//	m_eErrorCode = VFS::NoError;
//	m_sAbsSrcFileName += "/"+m_sCurrentFileName;
	if ( m_eRecursiveOp == COPY ) {
		m_sAbsTargetFName += "/"+m_sCurrentFileName;
		m_sSourceFileName = m_sAbsSrcFileName;
		m_sTargetFileName = m_sAbsTargetFName;
	}
	if ( m_eRecursiveOp != FIND )
		emit signalNameOfProcessedFiles( m_sAbsSrcFileName, m_sAbsTargetFName );

	if ( S_ISLNK(mStatBuffer.st_mode) && m_bOkStat ) {
		if ( m_eRecursiveOp == COPY ) {
			m_bOkStat = initCopiedFiles(); // make a link
		}
		else
		if ( m_eRecursiveOp == WEIGH ) {
		//m_nTotalWeight += fileSize( m_sAbsSrcFileName ); // ta fun.narazie nie uwzgl. m_bRealWeightInFS
			if ( m_bFollowByLink ) {
				m_bOkStat = ::stat( QFile::encodeName(m_sAbsSrcFileName), & mStatBuffer );
				if ( ! m_bRealWeightInFS )
					m_nTotalWeight += mStatBuffer.st_size;
			}
			else {
				if ( ! m_bRealWeightInFS )
					m_nTotalWeight += mStatBuffer.st_size; // sys.cmd.'du' weigh it as 0
			}
		}
		else
		if ( m_eRecursiveOp == REMOVE ) {
			m_bOkStat = ( ::unlink( QFile::encodeName(m_sAbsSrcFileName) ) == 0 );
			if ( ! m_bOkStat ) {
				qDebug("Cannot remove link: '%s', errno=%d", m_sAbsSrcFileName.toLatin1().data(), errno );
				m_eErrorCode = VFS::CannotRemove;
			}
		}
		else
		if ( m_eRecursiveOp == SET_ATTRIBUTES ) {
			if ( m_eChangesFor != DIRECTORIESONLYcf ) {
				if ( m_nPermissionForAll > -1 )
					m_bOkStat = setFilePermission( m_sAbsSrcFileName, m_nPermissionForAll );
				if ( m_nAccessTimeForAll > -1 || m_nModificationTimeForAll > -1 )
					m_bOkStat = setFileTime( m_sAbsSrcFileName, m_nAccessTimeForAll, m_nModificationTimeForAll );
				if ( m_nOwnerIdForAll > -1 || m_nGroupIdForAll > -1 )
					m_bOkStat = setFileOwnerAndGroup( m_sAbsSrcFileName, m_nOwnerIdForAll, m_nGroupIdForAll );
			}
		}
		else
		if ( m_eRecursiveOp == FIND ) {
			m_nFileCounter++;
			if ( ! m_bFindAll )
				if ( mFindCriterion.kindOfFiles == FindCriterion::LINKS )
					if ( currentFileMatches() )
						insertMatchedItem(); // and update status of found
		}
	}
	else
	if ( S_ISDIR(mStatBuffer.st_mode) && m_bOkStat ) {
		m_bWasDir = TRUE;
		if ( m_eRecursiveOp == COPY ) {
			m_bOkStat = ( ::mkdir(QFile::encodeName(m_sAbsTargetFName), 0755 ) == 0 );
			if ( ! m_bOkStat ) {
				if ( errno != EEXIST ) { // File NOT exists
					qDebug("Cannot create directory: '%s', errno=%d", m_sAbsTargetFName.toLatin1().data(), errno );
					m_eErrorCode = VFS::CannotWrite;
				}
				else m_bOkStat = TRUE;
			}
		}
		else
		if ( m_eRecursiveOp == WEIGH ) {
			m_bOkStat = TRUE;
			if ( m_bRealWeightInFS )
				m_nTotalWeight += (mStatBuffer.st_blksize/8)*mStatBuffer.st_blocks;  //4096/8==512, sys.cmd.'du' weigh it as 512
//			else m_nTotalWeight += mStatBuffer.st_size; // rozm.taki jak pokazuje mc
		}
		else
		if ( m_eRecursiveOp == REMOVE ) // if ( mStatBuffer.st_size == 48 ) // if empty dir (true for non Win/DOS.partition)
			m_bOkStat = ( ::rmdir(QFile::encodeName(m_sAbsSrcFileName)) == 0 ); // attempt to remove of unknown sizes dir
		else
		if ( m_eRecursiveOp == SET_ATTRIBUTES ) {
			if ( m_eChangesFor != FILESONLYcf ) {
				if ( m_nPermissionForAll > -1 )
					m_bOkStat = setFilePermission( m_sAbsSrcFileName, m_nPermissionForAll );
				if ( m_nAccessTimeForAll > -1 || m_nModificationTimeForAll > -1 )
					m_bOkStat = setFileTime( m_sAbsSrcFileName, m_nAccessTimeForAll, m_nModificationTimeForAll );
				if ( m_nOwnerIdForAll > -1 || m_nGroupIdForAll > -1 )
					m_bOkStat = setFileOwnerAndGroup( m_sAbsSrcFileName, m_nOwnerIdForAll, m_nGroupIdForAll );
			}
		}
		else
		if ( m_eRecursiveOp == FIND ) {
			m_nDirCounter++;
			if ( ! m_bFindAll ) {
				if ( mFindCriterion.kindOfFiles == FindCriterion::DIRECTORIES ) {
					if ( currentFileMatches() )
						insertMatchedItem(); // and update found's status
				}
			}
		}

		if ( m_bOkStat ) {
			if ( m_eRecursiveOp == WEIGH ) { // przeniesc do war. IS_DIR
				m_nTotalFiles++;
				emit signalFileCounterProgress( ++m_nDirCounter, FALSE, TRUE );
			}
			else
			if ( m_eRecursiveOp != FIND ) {
				if ( !(m_eRecursiveOp == REMOVE && m_eCurrentCommand == VFS::Remove) || m_eCurrentOperation == VFS::Remove ) m_nOpFileCounter++; // NNN (dodano 'if')
				emit signalFileCounterProgress( (m_eCurrentCommand == VFS::Remove) ? ++m_nDirCounterR : ++m_nDirCounter, FALSE );
				if ( m_eRecursiveOp == REMOVE ) {
					if ( m_eCurrentCommand != VFS::Remove )
						emit signalTotalProgress( (100 * m_nOpFileCounter) / m_nTotalFiles, m_nTotalWeight ); // NNN (dodano 'if')
					m_sAbsSrcFileName = QFile::decodeName( ::get_current_dir_name() ); // = m_sAbsSrcDirName, zawsze
					QTimer::singleShot( 0, this, SLOT(slotStartProcessDir()) );
					return;
				}
			}
		}
	}
	else
	if ( m_bOkStat ) { // it's file
		if ( m_eRecursiveOp == COPY ) {
			if ( ! (m_bOkStat=initCopiedFiles( mStatBuffer.st_size )) ) {
				if ( m_nSourceFileSize && ! errno ) // becouse when empty file, initCopiedFiles return FALSE
					if ( m_eErrorCode != VFS::BreakOperation && m_eErrorCode != VFS::NoError )
						qDebug("error copying; file: %s, errno=%d", m_sSourceFileName.toLatin1().data(), errno );
			}
		}
		else
		if ( m_eRecursiveOp == WEIGH ) {
			if ( m_bRealWeightInFS ) {
				if ( mStatBuffer.st_size ) // weigh only not empty file
					m_nTotalWeight += ((mStatBuffer.st_size/mStatBuffer.st_blksize)+1)*mStatBuffer.st_blksize;
			}
			else
				m_nTotalWeight += mStatBuffer.st_size;
		}
		else
		if ( m_eRecursiveOp == REMOVE ) {
			m_bOkStat = ( ::unlink( QFile::encodeName(m_sAbsSrcFileName) ) == 0 ); // removeFile( m_sAbsSrcFileName );
			if ( ! m_bOkStat ) {
				qDebug("Cannot remove file: '%s', errno=%d", m_sAbsSrcFileName.toLatin1().data(), errno );
				m_eErrorCode = VFS::CannotRemove;
			}
		}
		else
		if ( m_eRecursiveOp == SET_ATTRIBUTES ) {
			if ( m_eChangesFor != DIRECTORIESONLYcf ) {
				if ( m_nPermissionForAll > -1 )
					m_bOkStat = setFilePermission( m_sAbsSrcFileName, m_nPermissionForAll );
				if ( m_nAccessTimeForAll > -1 || m_nModificationTimeForAll > -1 )
					m_bOkStat = setFileTime( m_sAbsSrcFileName, m_nAccessTimeForAll, m_nModificationTimeForAll );
				if ( m_nOwnerIdForAll > -1 || m_nGroupIdForAll > -1 )
					m_bOkStat = setFileOwnerAndGroup( m_sAbsSrcFileName, m_nOwnerIdForAll, m_nGroupIdForAll );
			}
		}
		else
		if ( m_eRecursiveOp == FIND ) {
			m_nFileCounter++;
			if ( ! m_bFindAll )
				if ( mFindCriterion.kindOfFiles == FindCriterion::FILES )
					if ( currentFileMatches() )
						insertMatchedItem(); // and update found's status
		}
	}

	if ( m_eRecursiveOp == FIND && m_bFindAll ) {
		if ( currentFileMatches() )
			insertMatchedItem(); // and update found's status
	}
	if ( m_eRecursiveOp == FIND ) {
		if ( m_bWasDir )
			m_bWasDir = mFindCriterion.findRecursive;
		else {
			m_sAbsSrcFileName = QFile::decodeName( ::get_current_dir_name() );
			QTimer::singleShot( 0, this, SLOT(slotDirProcessing()) );
			return;
		}
	}
	if ( m_bWasDir ) { // pojawil sie kat.
		if (m_pDIR_DirPtr != NULL) { // NNN
			m_sAbsSrcDirName    = m_sAbsSrcFileName;
			m_sAbsTargetDirName = m_sAbsTargetFName;
			m_nDirInode.push((const int *)mStatBuffer.st_ino);
			//m_nDirInode = mStatBuffer.st_ino;
			mDirInodeStack.push( (int *)m_nDirInode );
			m_nCurrLocInDIR = ::telldir(m_pDIR_DirPtr);
			mCurrLocInDIRstack.push( (off_t *)m_nCurrLocInDIR );
		}
		while ( ! mDirPtrStack.isEmpty() ) // close all opened dirs
			::closedir( (DIR *)mDirPtrStack.pop() );
		QTimer::singleShot( 0, this, SLOT(slotStartProcessDir()) );
		if (m_pDIR_DirPtr == NULL)
			m_bWasDir = FALSE;
		return;
	}

	if ( m_eErrorCode != VFS::NoError ) { // occures an error
		if ( m_eErrorCode == VFS::CannotWrite )
			m_sNameOfProcessedFile = FileInfoExt::filePath(m_sAbsTargetFName);
		else
			m_sNameOfProcessedFile = m_sAbsSrcFileName;
		// tutaj przydaloby sie wstrzymac operacje i pokazac dialog z info o bledzie
	}
	if ( m_eRecursiveOp != COPY )
		currentCommandFinished();
}


void LFS::abort()
{
	breakCurrentOperation();
}
