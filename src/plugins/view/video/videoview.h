/***************************************************************************
                          videoview.h  -  description
                             -------------------
    begin                : mon nov 3 2003
    copyright            : (C) 2003 by Piotr Mierzwiński
    email                : peterm@go2.pl

    copyright            : See COPYING file that comes with this project
 ***************************************************************************/

#ifndef _VIDEOVIEW_H_
#define _VIDEOVIEW_H_

#include "view.h"


class VideoView : public View {
// 	Q_OBJECT
public:
	VideoView( QWidget *parent );
	~VideoView() { delete mWidget; }

	View::ModeOfView modeOfView() { return View::RENDERmode; }

private:
	QWidget *mWidget;

protected:
	void setGeometry( int x, int y, int w, int h )  { mWidget->setGeometry( x,y, w,h ); }

};

#endif
