# Plik utworzony przez menad?era projekt�w kdevelopa
# ------------------------------------------- 
# Podkatalog wzgl?dem g?�wnego katalogu projektu: ./src/plugins/view/video
# Cel to biblioteka videoview

INSTALLS += target 
target.path = $$PREFIX/lib/qtcmd/plugins 
INCLUDEPATH += ../../../../build/.tmp/ui ../../
MOC_DIR = ../../../../build/.tmp/moc 
OBJECTS_DIR = ../../../../build/.tmp/obj 
TARGET = videoview 
DESTDIR = ../../../../build/lib/qtcmd/plugins 
CONFIG += warn_on \
          qt \
          thread \
          plugin 
TEMPLATE = lib 
HEADERS += videoview.h \
	    ../../view.h
SOURCES += videoview.cpp \
           videoviewplugin.cpp 
#The following line was inserted by qt3to4
QT += network  qt3support 
