# Plik utworzony przez menad?era projekt�w kdevelopa
# ------------------------------------------- 
# Podkatalog wzgl?dem g?�wnego katalogu projektu: ./src/plugins/view/settings
# Cel to biblioteka settingsview

DEPENDPATH = ui 
INSTALLS += target 
target.path = $$PREFIX/lib/qtcmd/plugins 
TARGETDEPS += ../../../../build/lib/libqtcmduiext.so \
              ../../../../build/lib/libqtcmdutils.so \
              ../../../../build/lib/libqtcmddlgext.so 
LIBS += -lqtcmduiext \
        -lqtcmdutils \
        -lqtcmddlgext 
INCLUDEPATH += ../../../../src \
               ../../../../src/libs/qtcmddlgext \
               ../../../../src/libs/qtcmdutils \
               ../../../../src/libs/qtcmduiext \
               ../../../../src/libs/qtcmduiext/lwv \
               ../../../../build/.tmp/ui \
               ../../ \
               ./ui/
MOC_DIR = ../../../../build/.tmp/moc 
UI_DIR = ../../../../build/.tmp/ui 
OBJECTS_DIR = ../../../../build/.tmp/obj 
QMAKE_LIBDIR = ../../../../build/lib
TARGET = settingsview 
DESTDIR = ../../../../build/lib/qtcmd/plugins 
CONFIG += warn_on \
          qt \
          thread \
          plugin 
TEMPLATE = lib 
FORMS += ui/othertextviewpage.ui \
         ui/syntaxhighlightingpage.ui \
         ui/filesassociationpage.ui \
         ui/settingsviewdialog.ui 
TRANSLATIONS += ../../../../translations/pl/qtcmd-view-setts.ts
HEADERS += textviewsettings.h \
           ui/filesassociationpg.h \
           ui/othertextviewpg.h \
           ui/syntaxhighlightingpg.h \
           ui/settingsviewdlg.h \
           ui/mimedataitem.h \
           ui/mimedatamodel.h
SOURCES += settingsviewdialogplugin.cpp \
           ui/filesassociationpg.cpp \
           ui/othertextviewpg.cpp \
           ui/syntaxhighlightingpg.cpp \
           ui/settingsviewdlg.cpp \
           ui/mimedataitem.cpp \
           ui/mimedatamodel.cpp

#The following line was inserted by qt3to4
QT += network
CONFIG += uic4
