/***************************************************************************
                          othertextviewpg  -  description
                             -------------------
    begin                : sob gru 29 2007
    copyright            : (c) 2007 by Piotr Mierzwiński, Mariusz Borowski
    email                : piom@nes.pl, mariusz@nes.pl

    copyright            : See COPYING file that comes with this project

 $Id$
 ***************************************************************************/
#ifndef OTHERTEXTVIEWPG_H
#define OTHERTEXTVIEWPG_H

#include "ui_othertextviewpage.h"

/**
	@author Piotr Mierzwiński <piom@nes.pl>
*/
class OtherTextViewPg : public QWidget, public Ui::OtherTextViewPage
{
	Q_OBJECT

public:
	OtherTextViewPg( QWidget *pParent=NULL );

	void init();
	int  tabWidth();
	QString externalEditorPath();

};

#endif
