/***************************************************************************
 *   Copyright (C) 2008 by Mariusz Borowski <mariusz@nes.pl>               *
 *                   (This code is based on Qt4 Simple Tree Model Example) *
 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef TREEITEM_H
#define TREEITEM_H

#include <QList>
#include <QVariant>

class MimeDataItem
{
public:
	MimeDataItem(const QVariant &data, MimeDataItem *parent = 0);
	MimeDataItem(const QList<QVariant> &data, MimeDataItem *parent = 0);
	~MimeDataItem();

	void appendChild(MimeDataItem *child);

	MimeDataItem* child(int row);
	int childCount() const;
	int columnCount() const;
	QVariant data(int column) const;
	int row() const;
	MimeDataItem* parent();

private:
	QList<MimeDataItem*> childItems;
	QList<QVariant> itemData;
	MimeDataItem *parentItem;

};

#endif
