/***************************************************************************
                          othertextviewpg  -  description
                             -------------------
    begin                : sob gru 29 2007
    copyright            : (C) 2007 by Piotr Mierzwiński, Mariusz Borowski
    email                : piom@nes.pl, mariusz@nes.pl

    copyright            : See COPYING file that comes with this project

 $Id$
 ***************************************************************************/
//#include <QSettings> // TODO support for QSettings

#include "othertextviewpg.h"


OtherTextViewPg::OtherTextViewPg( QWidget * /*pParent*/ )
{
	setupUi(this);

	connect( m_pExternalEditorChkBox, SIGNAL(toggled(bool)), m_pLocationChooser, SLOT(setEnabled(bool)) );

	init();
}


void OtherTextViewPg::init()
{
	/*
	QSettings settings;

	int  nTabWidth = settings.readNumEntry( "/qtcmd/TextFileView/TabWidth", 2 );
	int  nUseExtEditor = settings.readNumEntry( "/qtcmd/TextFileView/UseExternalEditor", -1 );
	bool bUseExternalEditor = (nUseExtEditor < 0) ? FALSE : (bool)nUseExtEditor;
	m_pExternalEditorChkBox->setChecked( bUseExternalEditor );
	QString sExternalEditorPath = settings.readEntry( "/qtcmd/TextFileView/ExternalEditor" );

	m_pTabSizeSpinBox->setValue( nTabWidth );
	m_pLocationChooser->setText( sExternalEditorPath );
	m_pLocationChooser->setEnabled( m_pExternalEditorChkBox->isChecked() );
	*/
}


int OtherTextViewPg::tabWidth()
{
	return m_pTabSizeSpinBox->value();
}

QString OtherTextViewPg::externalEditorPath()
{
	return (m_pExternalEditorChkBox->isChecked()) ? m_pLocationChooser->text() : QString("");
}

