/***************************************************************************
                          syntaxhighlightingpg  -  description
                             -------------------
    begin                : sob gru 29 2007
    copyright            : (c) 2007 by Piotr Mierzwiński, Mariusz Borowski
    email                : piom@nes.pl, mariusz@nes.pl

    copyright            : See COPYING file that comes with this project

 $Id$
 ***************************************************************************/
#ifndef SYNTAXHIGHLIGHTINGPG_H
#define SYNTAXHIGHLIGHTINGPG_H

#include <QWidget>

#include "textviewsettings.h"

#include "ui_syntaxhighlightingpage.h"

/**
	@author Piotr Mierzwiński <piom@nes.pl>
*/
class SyntaxHighlightingPg : public QWidget, public Ui::SyntaxHighlightingPage
{
	Q_OBJECT

private:
	enum Columns { ContextCOL=0, BoldCOL, ItalicCOL, ColorCOL, UseDefaultCOL };
	enum Highlighting { CCpp=0, Html };
	QStringList m_CCppMap, m_HtmlMap;
	bool m_bInsertDefault;

public:
	SyntaxHighlightingPg( QWidget *pParent=NULL );
	~SyntaxHighlightingPg();

	void init();

	bool highlightingChanged();
	int  currentHighlighting();

private slots:
	void slotSelectHighlighting( int nCurrentHighlighting );
	void slotSetDefaultHighlighting();
	void slotButtonClicked( QTableWidgetItem * pItem );
	void slotColorChanged( QTableWidgetItem * pItem );

public slots:
	void shAttribut( int id, SHattribut & shAttiribut );


};

#endif
