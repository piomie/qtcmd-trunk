/***************************************************************************
                          syntaxhighlightingpg  -  description
                             -------------------
    begin                : sob gru 29 2007
    copyright            : (C) 2007 by Piotr Mierzwiński, Mariusz Borowski
    email                : piom@nes.pl, mariusz@nes.pl

    copyright            : See COPYING file that comes with this project

 $Id$
 ***************************************************************************/
//#include <QSettings> // TODO add support for QSettings
#include <QtGui>
#include <QTableWidgetItem>

//#include "functions_col.h"

#include "syntaxhighlightingpg.h"


SyntaxHighlightingPg::SyntaxHighlightingPg( QWidget * /*pParent*/ )
{
	setupUi(this);

	connect( m_pDefaultHighlightingBtn, SIGNAL(clicked()), this, SLOT(slotSetDefaultHighlighting()) );
	connect( m_pHighlightingComboBox, SIGNAL(activated(int)), this, SLOT(slotSelectHighlighting(int)) );

	init();
}

SyntaxHighlightingPg::~SyntaxHighlightingPg()
{
}


void SyntaxHighlightingPg::init()
{
	// prepare view
	// -- add columns TODO make 'add column' support for plugin ListWidgetView
	m_pListWidgetView->addColumn( ListWidgetView::TextLabel, tr("Context")/*, Qt::AlignmentFlag eColumnAlign=Qt::AlignLeft*/ );
	m_pListWidgetView->addColumn( ListWidgetView::CheckBox,  tr("Bold") );
	m_pListWidgetView->addColumn( ListWidgetView::CheckBox,  tr("Italic") );
	m_pListWidgetView->addColumn( ListWidgetView::ColorChooser, tr("Color") );
	m_pListWidgetView->addColumn( ListWidgetView::CheckBox,  tr("Use default") );

	//QSettings settings;

	QStringList slHighlightingList;// = settings.readListEntry( "/qtcmd/SyntaxHighlighting/HighlightingList" );
	slHighlightingList << tr("Source/C,C++") << tr("Markup/HTML");

	if ( slHighlightingList.count() > 0 )
		m_pHighlightingComboBox->addItems( slHighlightingList );

	m_CCppMap.clear();
	m_HtmlMap.clear();

	// --- init default highlightings
	// C/C++
	m_CCppMap.append( "Base N Integer;(default)FF00FF,,;" );
	m_CCppMap.append( "Character;(default)008080,,;" );
	m_CCppMap.append( "Comment;(default)808080,,Italic;" );
	m_CCppMap.append( "Data type;(default)800000,,;" );
	m_CCppMap.append( "Decimal or Value;(default)0000FF,,;" );
	m_CCppMap.append( "Extentions;(default)0095FF,Bold,;" );
	m_CCppMap.append( "Floating point;(default)800080,,;" );
	m_CCppMap.append( "Key word;(default)000000,Bold,;" );
	m_CCppMap.append( "Normal;(default)000000,,;" );
	m_CCppMap.append( "Preprocesor;(default)008000,,;" );
	m_CCppMap.append( "String;(default)FF0000,,;" );

	// HTML (an empty items for void SettingsViewDialog::slotApplyButtonPressed())
	m_HtmlMap.append( ";,,;" );
	m_HtmlMap.append( ";,,;" );
	m_HtmlMap.append( "Comment;(default)808080,,Italic;" );
	m_HtmlMap.append( "Data type;(default)800000,,;" );
	m_HtmlMap.append( "Decimal or Value;(default)0000FF,,;" );
	m_HtmlMap.append( ";,,;" );
	m_HtmlMap.append( ";,,;" );
	m_HtmlMap.append( "Key word;(default)000000,Bold,;" );
	m_HtmlMap.append( "Normal;(default)000000,,;" );
	m_HtmlMap.append( ";,,;" );
	m_HtmlMap.append( "String;(default)FF0000,,;" );

	/*
	// --- read from config file
	QString sNewSH;
	for ( uint i=0; i<m_CCppMap.count(); ++i ) {
		sNewSH = settings.readEntry( "/qtcmd/SyntaxHighlighting/CC++/"+m_CCppMap[i].section(';', 0,0) );
		if ( ! sNewSH.isEmpty() )
			m_CCppMap += sNewSH;
	}
	for ( uint i=0; i<m_HtmlMap.count(); ++i ) {
		sNewSH = settings.readEntry( "/qtcmd/SyntaxHighlighting/HTML/"+m_HtmlMap[i].section(';', 0,0) );
		if ( ! sNewSH.isEmpty() )
			m_HtmlMap += sNewSH;
	}
	*/
	// --- set default highlighting
	m_bInsertDefault = FALSE;
	slotSelectHighlighting( CCpp );

	m_pListWidgetView->selectRow(0);
	m_pListWidgetView->resizeColumnsToContents();

	connect( m_pListWidgetView, SIGNAL(buttonClicked(QTableWidgetItem *)),
			 this, SLOT(slotButtonClicked(QTableWidgetItem *)) );

	connect( m_pListWidgetView, SIGNAL(colorChanged(QTableWidgetItem *)),
			 this, SLOT(slotColorChanged(QTableWidgetItem *)) );
}

void SyntaxHighlightingPg::slotSelectHighlighting( int nCurrentHighlighting )
{
	QStringList slShMap;
	QString sData, sDefineSH, sDefaultSH;
	ListWidgetView::TextWeight eBold, eItalic;

	if ( nCurrentHighlighting == CCpp )
		slShMap = m_CCppMap;
	else
	if ( nCurrentHighlighting == Html )
		slShMap = m_HtmlMap;

	m_pListWidgetView->removeRows();

	int nRow = 0;
	for ( int i=0; i<slShMap.count(); ++i ) {
		if ( slShMap[i].section(';', 0,0).isEmpty() )
			continue;

		m_pListWidgetView->addRow();

		sDefaultSH = slShMap[i].section( ';', 1,1 );
		sDefineSH  = slShMap[i].section( ';', 2,2 );
		m_pListWidgetView->setCellValue( nRow, ContextCOL, slShMap[i].section(';', 0,0) );

		if ( m_bInsertDefault )
			sData = sDefaultSH;
		else
			sData = (sDefineSH.isEmpty()) ? sDefaultSH : sDefineSH;

		if ( sData.section( ',', 1,1 ).toLower() == "bold" )
			m_pListWidgetView->setCellValue( nRow, BoldCOL, "TRUE" );
		if ( sData.section( ',', 2,2 ).toLower() == "italic" )
			m_pListWidgetView->setCellValue( nRow, ItalicCOL, "TRUE" );

		sData = sData.section( ',', 0,0 );
		if ( sData.indexOf("(default)") == 0 ) {
			m_pListWidgetView->setCellValue( nRow, UseDefaultCOL, "TRUE" );
			sData.remove( 0, 9 ); // remove string '(default)'
		}

		eBold   = (m_pListWidgetView->cellValue(nRow, BoldCOL)   == "TRUE") ? ListWidgetView::Bold   : ListWidgetView::Normal;
		eItalic = (m_pListWidgetView->cellValue(nRow, ItalicCOL) == "TRUE") ? ListWidgetView::Italic : ListWidgetView::Normal;
		if (! sData.isEmpty())
			m_pListWidgetView->setCellValue( nRow, ColorCOL, "#"+sData );
		m_pListWidgetView->setCellAttributes( nRow, ContextCOL, QColor(m_pListWidgetView->cellValue(nRow, ColorCOL)), (ListWidgetView::TextWeight)(eBold | eItalic) );
		nRow++;
	}

	m_bInsertDefault = FALSE;
}


void SyntaxHighlightingPg::slotSetDefaultHighlighting()
{
	m_bInsertDefault = TRUE;
	slotSelectHighlighting( m_pHighlightingComboBox->currentIndex() );

	// --- update main 'slShMap'
	QStringList slShMap;
	int nCurrentHighlighting = m_pHighlightingComboBox->currentIndex();

	if ( nCurrentHighlighting == CCpp )
		slShMap = m_CCppMap;
	else
	if ( nCurrentHighlighting == Html )
		slShMap = m_HtmlMap;

	QString sData;
	for ( int i=0; i<slShMap.count(); i++ ) {
		sData = slShMap[i];
		if ( ! sData.section( ';', 2,2 ).isEmpty() ) // if sDefineSH exists then
			slShMap[i] = sData.left( sData.lastIndexOf(';')+1 ); // remove it
	}
	// --- update main '*Map'
	if ( nCurrentHighlighting == CCpp )
		m_CCppMap = slShMap;
	else
	if ( nCurrentHighlighting == Html )
		m_HtmlMap = slShMap;
}

void SyntaxHighlightingPg::slotButtonClicked( QTableWidgetItem *pItem )
{
	QString sData, sValue;
	QStringList slShMap;
	int nCurrentHighlighting = m_pHighlightingComboBox->currentIndex();

	if ( nCurrentHighlighting == CCpp )
		slShMap = m_CCppMap;
	else
	if ( nCurrentHighlighting == Html )
		slShMap = m_HtmlMap;

	int nRow    = pItem->row();
	int nColumn = pItem->column();
	// --- search id of clicked pItem
	int id;
	bool bFound = FALSE;
	for ( id=0; id<slShMap.count(); id++ ) {
		if ( slShMap[id].section( ';', 0,0 ) == m_pListWidgetView->item(nRow, ContextCOL)->text() ) {
			bFound = TRUE;
			break;
		}
	}
	if ( ! bFound )
		return;

	// --- set default value
	if ( nColumn == UseDefaultCOL ) {
		if ( m_pListWidgetView->cellValue(nRow, UseDefaultCOL) == "TRUE" ) {
			qDebug() << "default is true";
			sData = slShMap[id].section(';', 1,1);

			sValue = sData.section( ',', 1,1 ).toLower();
			m_pListWidgetView->setCellValue( nRow, BoldCOL,   (sValue == "bold"  ) ? "TRUE" : "FALSE" );
			sValue = sData.section( ',', 2,2 ).toLower();
			m_pListWidgetView->setCellValue( nRow, ItalicCOL, (sValue == "italic") ? "TRUE" : "FALSE" );

			sData.remove( 0, 9 ); // remove string '(default)'

			m_pListWidgetView->setCellValue( nRow, ColorCOL, "#"+sData.section( ',', 0,0) );
			sData = slShMap[id].left( slShMap[id].lastIndexOf( ';' )+1 ); // init by default value
		}
	}
	else {
		m_pListWidgetView->setCellValue( nRow, UseDefaultCOL, "FALSE" );

		int nDefineSHid = slShMap[id].lastIndexOf( ';' )+1;
		sData = slShMap[id].left( nDefineSHid );
		sData.remove( nDefineSHid, slShMap[id].length()-nDefineSHid );
		sData += m_pListWidgetView->cellValue( nRow, ColorCOL )+",";
		if ( m_pListWidgetView->cellValue( nRow, BoldCOL ) == "TRUE" )
			sData += "Bold";
		sData += ",";
		if ( m_pListWidgetView->cellValue( nRow, ItalicCOL ) == "TRUE" )
			sData += "Italic";
	}
// --- update main '*Map'
	if ( nCurrentHighlighting == CCpp )
		m_CCppMap[id] = sData;
	else
	if ( nCurrentHighlighting == Html )
		m_HtmlMap[id] = sData;

// the *Map formats:
//   ContextName;(default)color,bold,itali;
//   ContextName;(default)color,bold,itali;color,bold,italic

	int nTxtWeight = ListWidgetView::Normal;
	if ( m_pListWidgetView->cellValue( nRow, BoldCOL ) == "TRUE" )
		nTxtWeight |= (ListWidgetView::TextWeight)ListWidgetView::Bold;
	if ( m_pListWidgetView->cellValue( nRow, ItalicCOL ) == "TRUE" )
		nTxtWeight |= (ListWidgetView::TextWeight)ListWidgetView::Italic;

	m_pListWidgetView->setCellAttributes( nRow, ContextCOL, QColor(m_pListWidgetView->cellValue(nRow, ColorCOL)), (ListWidgetView::TextWeight)nTxtWeight );

	m_bInsertDefault = FALSE;
}

void SyntaxHighlightingPg::slotColorChanged( QTableWidgetItem *pItem )
{
	slotButtonClicked( pItem );
}

void SyntaxHighlightingPg::shAttribut( int id, SHattribut & shAttiribut )
{
	if ( id > 10 )
		return;

	QStringList slShMap;
	int nCurrentHighlighting = m_pHighlightingComboBox->currentIndex();

	if ( nCurrentHighlighting == CCpp )
		slShMap = m_CCppMap;
	else
	if ( nCurrentHighlighting == Html )
		slShMap = m_HtmlMap;

	QString sh = slShMap[id];
	QString sData;
	if ( sh.at(sh.length()-1) == ';' ) // is default only
		sData = sh.section(';', 1,1);
	else { // defined
		sData = sh;
		sData = sData.remove(0, sData.lastIndexOf( ';' )+1 ); // remove default
	}

	shAttiribut.bold   = (sData.section( ',', 1,1 ).toLower() == "bold");
	shAttiribut.italic = (sData.section( ',', 2,2 ).toLower() == "italic");

	sData = sData.section( ',', 0,0 );
	if ( sData.indexOf("(default)") == 0 )
		sData.remove( 0, 9 ); // remove string '(default)'

//	shAttiribut.color = color( sData );
	shAttiribut.color = QColor( sData );
}

bool SyntaxHighlightingPg::highlightingChanged()
{
	for (int i=0; i<m_pListWidgetView->rowCount(); i++) {
		if (m_pListWidgetView->cellValue(i, UseDefaultCOL) == "TRUE" )
			return true;
	}

	return FALSE;
}

int SyntaxHighlightingPg::currentHighlighting()
{
	return m_pHighlightingComboBox->currentIndex();
}

