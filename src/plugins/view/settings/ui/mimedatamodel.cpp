/***************************************************************************
 *   Copyright (C) 2008 by Mariusz Borowski <mariusz@nes.pl>               *
 *                   (This code is based on Qt4 Simple Tree Model Example) *
 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include <QtGui>

#include "mimedataitem.h"
#include "mimedatamodel.h"

namespace {
    const int NUMBER_OF_COLUMNS = 3;
}

MimeDataModel::MimeDataModel( const MimeDataInfo::MimeData *pMimeData, QObject *pParent )
    : QAbstractItemModel(pParent)
{
	QList <QVariant> headerData;
	headerData << "Type" << "Extentions" << "Description";
	m_pRootItem = new MimeDataItem(headerData);
	setupModelData(pMimeData, m_pRootItem);
}

MimeDataModel::~MimeDataModel()
{
	delete m_pRootItem;
}

int MimeDataModel::columnCount( const QModelIndex & /*parent*/ ) const
{
	return NUMBER_OF_COLUMNS;
}

QVariant MimeDataModel::data( const QModelIndex &index, int role ) const
{
	if (! index.isValid())
		return QVariant();

	if (role != Qt::DisplayRole)
		return QVariant();

	MimeDataItem *item = static_cast<MimeDataItem*>(index.internalPointer());

	return item->data(index.column());
}

Qt::ItemFlags MimeDataModel::flags( const QModelIndex &index ) const
{
	if (! index.isValid())
		return 0;

	return Qt::ItemIsEnabled | Qt::ItemIsSelectable;
}

QVariant MimeDataModel::headerData( int section, Qt::Orientation orientation, int role ) const
{
	if (orientation == Qt::Horizontal && role == Qt::DisplayRole)
		return m_pRootItem->data(section);

	return QVariant();
}

QModelIndex MimeDataModel::index( int row, int column, const QModelIndex &parent ) const
{
	if (!hasIndex(row, column, parent))
		return QModelIndex();

	MimeDataItem *parentItem;

	if (! parent.isValid())
		parentItem = m_pRootItem;
	else
		parentItem = static_cast<MimeDataItem*>(parent.internalPointer());

	MimeDataItem *childItem = parentItem->child(row);
	if (childItem)
		return createIndex(row, column, childItem);

	return QModelIndex();
}

QModelIndex MimeDataModel::parent( const QModelIndex &index ) const
{
	if (! index.isValid())
		return QModelIndex();

	MimeDataItem *childItem = static_cast<MimeDataItem*>(index.internalPointer());
	MimeDataItem *parentItem = childItem->parent();

	if (parentItem == m_pRootItem)
		return QModelIndex();

	return createIndex(parentItem->row(), 0, parentItem);
}

int MimeDataModel::rowCount( const QModelIndex &parent ) const
{
	MimeDataItem *parentItem;
	if (parent.column() > 0)
		return 0;

	if (! parent.isValid())
		parentItem = m_pRootItem;
	else
		parentItem = static_cast<MimeDataItem*>(parent.internalPointer());

	return parentItem->childCount();
}

void MimeDataModel::setupModelData( const MimeDataInfo::MimeData *pMimeData, MimeDataItem *pRoot )
{
	Q_CHECK_PTR(pMimeData);
	Q_CHECK_PTR(pRoot);
	m_pRootItem = pRoot;

	typedef QMap<QString, MimeDataItem*> CategoriesItems;
	CategoriesItems categories, subcategories;

	int row = 0;
	while (pMimeData[row].typeDesc != 0)
	{
		CategoriesItems::iterator it = categories.find(pMimeData[row].category);
		MimeDataItem *categoryItem = 0;
		if (it == categories.end()) {
			categoryItem = new MimeDataItem(qVariantFromValue(pMimeData[row].category), m_pRootItem);
			categories.insert(pMimeData[row].category, categoryItem);
			m_pRootItem->appendChild(categoryItem);
		}
		else
			categoryItem = it.value();

		MimeDataItem *itemParent = 0;
		MimeDataItem *subCategoryItem = 0;
		if (! pMimeData[row].subCategory.isEmpty())
		{
			CategoriesItems::iterator it2 = subcategories.find(pMimeData[row].subCategory);
			if (it2 == subcategories.end()) {
				subCategoryItem = new MimeDataItem(qVariantFromValue(pMimeData[row].subCategory), categoryItem);
				subcategories.insert(pMimeData[row].subCategory, subCategoryItem);
				categoryItem->appendChild(subCategoryItem);
			}
			else {
				subCategoryItem = it2.value();
			}
			itemParent = subCategoryItem;
		}
		else {
			itemParent = categoryItem;
		}

		QList<QVariant> columnData;
		columnData << pMimeData[row].typeDesc << pMimeData[row].extentions << pMimeData[row].description;
		itemParent->appendChild( new MimeDataItem(columnData, itemParent) );
		++row;
	}
}

