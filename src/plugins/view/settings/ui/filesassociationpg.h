/***************************************************************************
                          filesassociationpg  -  description
                             -------------------
    begin                : sob gru 29 2007
    copyright            : (c) 2007 by Piotr Mierzwiński, Mariusz Borowski
    email                : piom@nes.pl, mariusz@nes.pl

    copyright            : See COPYING file that comes with this project

 $Id$
 ***************************************************************************/
#ifndef FILESASSOCIATIONPG_H
#define FILESASSOCIATIONPG_H

#include "ui_filesassociationpage.h"

#include "mimedataitem.h"

/**
	@author Piotr Mierzwiński <piom@nes.pl>
*/
class FilesAssociationPg : public QWidget, public Ui::FilesAssociationPage
{
	Q_OBJECT

private:
	enum KindOfFileInfo { FILETEMPL=0, DESCRIPTION, APPStoVIEW, EXTVIEWER, KINDofFILE };
	typedef QMap <QString, QString> ViewerInfoMap;
	ViewerInfoMap m_ViewerInfoMap;
	ViewerInfoMap m_DefaultDescriptionMap;

public:
	FilesAssociationPg( QWidget *pParent=NULL );

	void init();

private slots:
	void slotGetApplication();
	void slotEnableViewerInfo( int bEnabled );

public slots:
	void slotSave();

protected slots:
	void slotSelectionChanged ( const QItemSelection & selected, const QItemSelection & deselected );

};

#endif
