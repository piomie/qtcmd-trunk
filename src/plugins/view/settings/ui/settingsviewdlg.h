/***************************************************************************
                          settingsviewdlg  -  description
                             -------------------
    begin                : sob gru 29 2007
    copyright            : (c) 2007 by Piotr Mierzwiński, Mariusz Borowski
    email                : piom@nes.pl, mariusz@nes.pl

    copyright            : See COPYING file that comes with this project

 $Id$
 ***************************************************************************/
#ifndef SETTINGSVIEWDLG_H
#define SETTINGSVIEWDLG_H

#include <QDialog>

#include "toolbarpage.h"
#include "fontchooserpage.h"
#include "keyshortcutspage.h"
#include "othertextviewpg.h"
#include "filesassociationpg.h"
#include "syntaxhighlightingpg.h"

#include "view.h"
#include "keyshortcuts.h"
#include "textviewsettings.h"

#include "ui_settingsviewdialog.h"

/**
	@author Piotr Mierzwiński <piom@nes.pl>
*/
class SettingsViewDlg : public QDialog, public Ui::SettingsViewDialog
{
	Q_OBJECT

private:
	OtherTextViewPg *m_pOtherTextViewPage;
	FontChooserPage *m_pFontViewPage;
	SyntaxHighlightingPg *m_pSyntaxhighlightingPage;
	FilesAssociationPg *m_pFilesAssociationPage;
	KeyShortcutsPage *m_pKeyshortcutsPage;
	ToolBarPage *m_pToolBarPage;

	QWidget *m_pTextViewPage;
	QWidget *m_pImageViewPage;
	QWidget *m_pVideoViewPage;
	QWidget *m_pSoundViewPage;
	QWidget *m_pBinaryViewPage;
	QWidget *m_pOtherViewPage;
	QWidget *m_pEmptyPage;

	View::KindOfView m_eCurrentKindOfView;

	void raiseWidget( const QString & sSenderName );

public:
	SettingsViewDlg( QWidget *pParent=NULL );

	void init();

private slots:
	void slotOkButtonPressed();
	void slotApplyButtonPressed();
	void slotSaveButtonPressed();
	void slotSetCurrentKindOfView( View::KindOfView eCurrentKindOfView );
	void slotCurrentItemChanged( int nItemId );
	void slotSetKeyShortcuts( KeyShortcuts *pKeyShortcuts );

	void slotItemSelectionChanged();

//public slots:
//	void shAttribut( int id, SHattribut & shAttiribut );

signals:
	void signalSaveSettings();
	void signalApply( const TextViewSettings & );
	void signalApplyKeyShortcuts( KeyShortcuts * );

};

#endif
