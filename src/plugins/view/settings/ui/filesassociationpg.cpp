/***************************************************************************
                          filesassociationpg  -  description
                             -------------------
    begin                : sob gru 29 2007
    copyright            : (C) 2007 by Piotr Mierzwiński, Mariusz Borowski
    email                : piom@nes.pl, mariusz@nes.pl

    copyright            : See COPYING file that comes with this project

 $Id$
 ***************************************************************************/
//#include <QSettings> // TODO add support for QSettings
#include <QtGui>

#include <QFileInfo>
#include <QFileDialog>
#include <QInputDialog>

#include "messagebox.h"

#include "filesassociationpg.h"
#include "mimedatamodel.h"
#include "mimetypedata.h"


FilesAssociationPg::FilesAssociationPg( QWidget * /*pParent*/ )
{
	setupUi(this);

	init();
}


void FilesAssociationPg::init()
{
	const MimeDataInfo::MimeData *pMimeData = MimeDataInfo::mimeData;
	MimeDataModel *pMimeDataModel = new MimeDataModel(pMimeData);

	m_pKindOfFilesTreeView->setModel(pMimeDataModel);
	QHeaderView *pHeader = m_pKindOfFilesTreeView->header();
	pHeader->hideSection(1);
	pHeader->hideSection(2);
	pHeader->resizeSection(0, m_pKindOfFilesTreeView->width());
	//pItem->setFlags( Qt::ItemIsEnabled | Qt::ItemIsSelectable );
	//m_pKindOfFilesTreeView->setEditTriggers(QAbstractItemView::SelectedClicked);

//	slotSelectionChanged( pRoot );
//	mAddNewKindpushBtn->hide(); // NNN (all support not yet implemented)

	m_pKindOfFilesTreeView->setSelectionModel(new QItemSelectionModel(pMimeDataModel, this));
	connect( m_pKindOfFilesTreeView->selectionModel(), SIGNAL(selectionChanged(const QItemSelection &, const QItemSelection &)), this, SLOT(slotSelectionChanged(const QItemSelection &, const QItemSelection &)) );

	if (m_pKindOfFilesTreeView->selectionModel()->selectedIndexes().count() == 0) {
		m_pViewerInfoGroupBox->setEnabled( FALSE );
		m_pIntOrExtViewerGroupBox->setEnabled( FALSE );
		m_pInternalViewerRadioBtn->setChecked( FALSE );
		m_pExternalViewerRadioBtn->setChecked( FALSE );
	}
}


void FilesAssociationPg::slotGetApplication()
{
	QString sAppName = QFileDialog::getOpenFileName(this,
			tr("Select viewer")+" - QtCommander",
				"/", tr("All files")+"(*)"
		);
	if ( sAppName.isEmpty() )
		return;

	if ( ! QFileInfo(sAppName).isExecutable() )
		MessageBox::critical( this, tr("This file is'nt executable !") );
	else
		m_pApplicationLineEdit->setText( sAppName );
}


void FilesAssociationPg::slotEnableViewerInfo( int bEnabled )
{
	m_pDescriptionLineEdit->setReadOnly( ! bEnabled );
	m_pFileTemplatesLineEdit->setReadOnly( ! bEnabled );
	m_pApplicationLineEdit->setEnabled( bEnabled );
	m_pSelectAppToolBtn->setEnabled( bEnabled );

//	changeMapValue( EXTVIEWER, (bEnabled ? "ext" : "int") );
}

void FilesAssociationPg::slotSave()
{
	/*
	QSettings settings;
	QStringList slInfoList;

	for ( ViewerInfoMap::Iterator it = m_ViewerInfoMap.begin(); it != m_ViewerInfoMap.end(); ++it ) {
		slInfoList = it.data().split( '\n' );

		settings.writeEntry( "/qtcmd_mime/"+it.key()+"/Application",    slInfoList[APPStoVIEW].isEmpty() ? "qtcmd" : slInfoList[APPStoVIEW] );
		settings.writeEntry( "/qtcmd_mime/"+it.key()+"/Description",    slInfoList[DESCRIPTION] );
		settings.writeEntry( "/qtcmd_mime/"+it.key()+"/FileTemplates",  slInfoList[FILETEMPL] );
		settings.writeEntry( "/qtcmd_mime/"+it.key()+"/ExternalViewer", (slInfoList[EXTVIEWER] == "ext") );
		settings.writeEntry( "/qtcmd_mime/"+it.key()+"/KindOfFile",     slInfoList[KINDofFILE] );
	}
	*/
}

void FilesAssociationPg::slotSelectionChanged ( const QItemSelection & selected, const QItemSelection & /*deselected*/ )
{
	//bool bEnabled = (pItem->depth() > 1 && pItem->childCount() == 0);
	bool bEnabled = ! selected.indexes().first().model()->hasChildren(selected.indexes().first());
	m_pViewerInfoGroupBox->setEnabled( bEnabled );
	m_pIntOrExtViewerGroupBox->setEnabled( bEnabled );

	if ( ! bEnabled ) {
		m_pInternalViewerRadioBtn->setChecked( FALSE );
		m_pExternalViewerRadioBtn->setChecked( FALSE );
		m_pFileTemplatesLineEdit->clear();
		m_pDescriptionLineEdit->clear();
		m_pApplicationLineEdit->clear();
		return;
	}

	QString sExt = "*."+selected.indexes().first().sibling(selected.indexes().first().row(), 1).data(0).toString();
	sExt = sExt.replace(" ", " *.");
	m_pFileTemplatesLineEdit->setText(sExt);
	m_pDescriptionLineEdit->setText( selected.indexes().first().sibling(selected.indexes().first().row(), 2).data(0).toString() );
	//m_pApplicationLineEdit->setText( slInfoList[APPStoVIEW] );
	//qDebug() << "hasChildren=" << selected.indexes().first().model()->hasChildren(selected.indexes().first());

	// --- get info about pItem and inits all edits
	//QStringList slInfoList = m_ViewerInfoMap[pItem->text(0)].split( '\n' );

	//m_pInternalViewerRadioBtn->setChecked( (slInfoList[EXTVIEWER] == "int") );
	//m_pExternalViewerRadioBtn->setChecked( (slInfoList[EXTVIEWER] == "ext") );

	m_pFileTemplatesLineEdit->setCursorPosition( 0 );
	m_pDescriptionLineEdit->setCursorPosition( 0 );
	m_pApplicationLineEdit->setCursorPosition( 0 );

	//slotEnableViewerInfo( (slInfoList[EXTVIEWER] == "ext") );
}

