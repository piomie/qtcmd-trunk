/***************************************************************************
                          settingsviewdlg  -  description
                             -------------------
    begin                : sob gru 29 2007
    copyright            : (C) 2007 by Piotr Mierzwiński, Mariusz Borowski
    email                : piom@nes.pl, mariusz@nes.pl

    copyright            : See COPYING file that comes with this project

 $Id$
 ***************************************************************************/
#include <QList>

#include "../text/syntaxhighlighter.h"

#include "settingsviewdlg.h"


SettingsViewDlg::SettingsViewDlg( QWidget * /*pParent*/ )
{
	setupUi(this);

	connect( m_pTextViewListWidget,   SIGNAL(itemSelectionChanged()), this, SLOT(slotItemSelectionChanged()) );
// 	connect( m_pImageViewListWidget,  SIGNAL(itemSelectionChanged()), this, SLOT(slotItemSelectionChanged()) );
// 	connect( m_pVideoViewListWidget,  SIGNAL(itemSelectionChanged()), this, SLOT(slotItemSelectionChanged()) );
// 	connect( m_pSoundViewListWidget,  SIGNAL(itemSelectionChanged()), this, SLOT(slotItemSelectionChanged()) );
// 	connect( m_pBinaryViewListWidget, SIGNAL(itemSelectionChanged()), this, SLOT(slotItemSelectionChanged()) );
	connect( m_pOtherListWidget,      SIGNAL(itemSelectionChanged()), this, SLOT(slotItemSelectionChanged()) );

	connect( m_pToolBox, SIGNAL(currentChanged(int)), this, SLOT(slotCurrentItemChanged(int)) );

	connect( m_pCancelBtn, SIGNAL(clicked()), this, SLOT(close()) );
	connect( m_pOkBtn,     SIGNAL(clicked()), this, SLOT(slotOkButtonPressed()) );
	connect( m_pApplyBtn,  SIGNAL(clicked()), this, SLOT(slotApplyButtonPressed()) );
	connect( m_pSaveBtn,   SIGNAL(clicked()), this, SLOT(slotSaveButtonPressed()) );

	init();

	m_pToolBox->setMinimumWidth(width()/4);
}


void SettingsViewDlg::init()
{
	setWindowTitle( tr("View settings")+" - QtCommander" );

	// --- TextViewListBox pages
	m_pFontViewPage           = new FontChooserPage;
	m_pOtherTextViewPage      = new OtherTextViewPg;
	m_pSyntaxhighlightingPage = new SyntaxHighlightingPg;

	m_pStackedWidget->addWidget( m_pFontViewPage );
	m_pStackedWidget->addWidget( m_pSyntaxhighlightingPage );
	m_pStackedWidget->addWidget( m_pOtherTextViewPage );

	// --- ImageViewListBox pages
	// --- VideoViewListBox pages
	// --- SoundViewListBox pages
	// --- BinaryViewListBox pages

	// --- OtherView pages
	m_pKeyshortcutsPage     = new KeyShortcutsPage;
	m_pFilesAssociationPage = new FilesAssociationPg;
	m_pToolBarPage          = new ToolBarPage;

	m_pStackedWidget->addWidget( m_pFilesAssociationPage );
	m_pStackedWidget->addWidget( m_pKeyshortcutsPage );
	m_pStackedWidget->addWidget( m_pToolBarPage );

	// --- an empty page for no items list
	m_pEmptyPage = new QWidget;
	m_pStackedWidget->addWidget( m_pEmptyPage );

	m_pKeyshortcutsPage->init( FALSE ); // FALSE -> viewSettings
	m_pToolBarPage->init( FALSE ); // FALSE -> viewSettings

	resize( QSize(620, 454).expandedTo(minimumSizeHint()) );

	static QList <int> sSW; // width the children of spliter obj.
	sSW << (width()*27)/100 << 0;
	sSW[1] = width()-sSW[0];
	m_pSplitter->setSizes( sSW );

}

void SettingsViewDlg::raiseWidget( const QString & sSenderName )
{
	int nListWidgetRow = 0;
	QString sTitle;

	if ( sSenderName == "m_pTextViewListWidget" || sSenderName == "TextViewPage" ) {
		nListWidgetRow = m_pTextViewListWidget->currentRow();
		if (nListWidgetRow >= 0)
			sTitle = m_pTextViewListWidget->item(nListWidgetRow)->text();
	}
	else
	if ( sSenderName == "m_pImageViewListWidget" || sSenderName == "ImageViewPage" ) {
		nListWidgetRow = m_pImageViewListWidget->currentRow();
		if (nListWidgetRow >= 0)
			sTitle = m_pImageViewListWidget->item(nListWidgetRow)->text();
		nListWidgetRow += 0; // + pageOffset
	}
	else
	if ( sSenderName == "m_pVideoViewListWidget" || sSenderName == "VideoViewPage" ) {
		nListWidgetRow = m_pVideoViewListWidget->currentRow();
		if (nListWidgetRow >= 0)
			sTitle = m_pVideoViewListWidget->item(nListWidgetRow)->text();
		nListWidgetRow += 0; // + pageOffset
	}
	else
	if ( sSenderName == "m_pSoundViewListWidget" || sSenderName == "SoundViewPage" ) {
		nListWidgetRow = m_pSoundViewListWidget->currentRow();
		if (nListWidgetRow >= 0)
			sTitle = m_pSoundViewListWidget->item(nListWidgetRow)->text();
		nListWidgetRow += 0; // + pageOffset
	}
	else
	if ( sSenderName == "m_pBinaryViewListWidget" || sSenderName == "BinaryViewPage" ) {
		nListWidgetRow = m_pBinaryViewListWidget->currentRow();
		if (nListWidgetRow >= 0)
			sTitle = m_pBinaryViewListWidget->item(nListWidgetRow)->text();
		nListWidgetRow += 0; // + pageOffset
	}
	else
	if ( sSenderName == "m_pOtherListWidget" || sSenderName == "OtherViewPage" ) {
		nListWidgetRow = m_pOtherListWidget->currentRow();
		if (nListWidgetRow >= 0)
			sTitle = m_pOtherListWidget->item(nListWidgetRow)->text();
		nListWidgetRow += 3; // + pageOffset
	}
	//qDebug() << "SettingsViewDlg::raiseWidget, nListWidgetRow=" << nListWidgetRow << ", sender=" << sSenderName << "sTitle" << sTitle;

	if (nListWidgetRow < 0)
		m_pStackedWidget->setCurrentWidget(m_pEmptyPage);
	else
		m_pStackedWidget->setCurrentIndex( nListWidgetRow );

	m_pTitleLab->setText( sTitle );
}

// ---------- S L O T S -----------

void SettingsViewDlg::slotSetKeyShortcuts( KeyShortcuts *pKeyShortcuts )
{
	if ( m_pKeyshortcutsPage )
		m_pKeyshortcutsPage->setKeyShortcuts( pKeyShortcuts );
}


void SettingsViewDlg::slotOkButtonPressed()
{
	slotApplyButtonPressed();
	slotSaveButtonPressed();
	close();
}

void SettingsViewDlg::slotApplyButtonPressed()
{
	if ( m_eCurrentKindOfView == View::TEXTview ) {
		TextViewSettings textViewSettings;
		textViewSettings.fontFamily = m_pFontViewPage->fontFamily();
		textViewSettings.fontSize   = m_pFontViewPage->fontSize();
		textViewSettings.fontBold   = m_pFontViewPage->fontBold();
		textViewSettings.fontItalic = m_pFontViewPage->fontItalic();
		textViewSettings.tabWidth   = m_pOtherTextViewPage->tabWidth();
		textViewSettings.externalEditor = m_pOtherTextViewPage->externalEditorPath();

		if ( m_pSyntaxhighlightingPage->highlightingChanged() ) {
			SHattribut newSHattribut;
			for ( int i=0; i<11; i++ ) { // TODO name this constant value
				m_pSyntaxhighlightingPage->shAttribut( i, newSHattribut );
				textViewSettings.shAttrib[i].color  = newSHattribut.color;
				textViewSettings.shAttrib[i].bold   = newSHattribut.bold;
				textViewSettings.shAttrib[i].italic = newSHattribut.italic;
			}
		}
		SyntaxHighlighter::SyntaxHighlighting sh;
		int currentSH = m_pSyntaxhighlightingPage->currentHighlighting();
		if ( currentSH == 0 )
			sh = SyntaxHighlighter::CCpp;
		else
		if ( currentSH == 1 )
			sh = SyntaxHighlighter::HTML;
		else
			sh = SyntaxHighlighter::NONE;
		textViewSettings.currentSyntaxHighlighting = sh;

		emit signalApply( textViewSettings );
	}
/*
	else
	if ( m_eCurrentKindOfView == View::IMAGEview ) {
	}
	else
	if ( m_eCurrentKindOfView == View::SOUNDview ) {
	}
	else
	if ( m_eCurrentKindOfView == View::VIDEOview ) {
	}
	else
	if ( m_eCurrentKindOfView == View::BINARYview ) {
	}
*/
	// --- other page
	emit signalApplyKeyShortcuts( m_pKeyshortcutsPage->keyShortcuts() );
}


void SettingsViewDlg::slotSaveButtonPressed()
{
	m_pKeyshortcutsPage->slotSave();
	m_pFilesAssociationPage->slotSave();
	emit signalSaveSettings();
}

void SettingsViewDlg::slotSetCurrentKindOfView( View::KindOfView eCurrentKindOfView )
{
	m_eCurrentKindOfView = eCurrentKindOfView;
}


void SettingsViewDlg::slotItemSelectionChanged()
{
	raiseWidget(sender()->objectName());
}

void SettingsViewDlg::slotCurrentItemChanged( int )
{
	raiseWidget(m_pToolBox->currentWidget()->objectName());
}

