/***************************************************************************
 *   Copyright (C) 2008 by Mariusz Borowski <mariusz@nes.pl>               *
 *                   (This code is based on Qt4 Simple Tree Model Example) *
 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include <QStringList>

#include "mimedataitem.h"


MimeDataItem::MimeDataItem(const QList<QVariant> &data, MimeDataItem *parent)
{
	parentItem = parent;
	itemData = data;
}

MimeDataItem::MimeDataItem(const QVariant &data, MimeDataItem *parent)
{
	parentItem = parent;
	itemData << data;
}

MimeDataItem::~MimeDataItem()
{
	qDeleteAll(childItems);
}

void MimeDataItem::appendChild(MimeDataItem *item)
{
	childItems.append(item);
}

MimeDataItem* MimeDataItem::child(int row)
{
	return childItems.value(row);
}

int MimeDataItem::childCount() const
{
	return childItems.count();
}

int MimeDataItem::columnCount() const
{
	return itemData.count();
}

QVariant MimeDataItem::data(int column) const
{
	return itemData.value(column);
}

MimeDataItem* MimeDataItem::parent()
{
	return parentItem;
}

int MimeDataItem::row() const
{
	if (parentItem)
		return parentItem->childItems.indexOf(const_cast<MimeDataItem*>(this));

	return 0;
}

