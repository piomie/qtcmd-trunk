# Plik utworzony przez menad?era projekt�w kdevelopa
# ------------------------------------------- 
# Podkatalog wzgl?dem g?�wnego katalogu projektu: ./src/plugins/view/sound
# Cel to biblioteka soundview

INSTALLS += target 
target.path = $$PREFIX/lib/qtcmd/plugins 
INCLUDEPATH += ../../../../build/.tmp/ui ../../
MOC_DIR = ../../../../build/.tmp/moc 
OBJECTS_DIR = ../../../../build/.tmp/obj 
TARGET = soundview 
DESTDIR = ../../../../build/lib/qtcmd/plugins 
CONFIG += warn_on \
          qt \
          thread \
          plugin 
TEMPLATE = lib 
HEADERS += soundview.h
SOURCES += soundview.cpp \
           soundviewplugin.cpp 
#The following line was inserted by qt3to4
QT += network  qt3support 
