/***************************************************************************
                          imageviewplugin.cpp  -  description
                             -------------------
    begin                : tue nov 4 2003
    copyright            : (C) 2003 by Piotr Mierzwiński
    email                : peterm@go2.pl

    copyright            : See COPYING file that comes with this project
 ***************************************************************************/

#include "imageview.h"

// export symbol 'create_view' for resolves it by 'QLibrary::resolve( const char * )'
extern "C" {
	//View * create_view( QWidget * parent, const char * name )
	QWidget* create_plugin( QWidget * parent )
	{
		return new ImageView( parent );
	}
}

