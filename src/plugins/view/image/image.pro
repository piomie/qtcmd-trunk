# Plik utworzony przez menad?era projekt�w kdevelopa
# ------------------------------------------- 
# Podkatalog wzgl?dem g?�wnego katalogu projektu: ./src/plugins/view/image
# Cel to biblioteka imageview

INSTALLS += target 
target.path = $$PREFIX/lib/qtcmd/plugins 
LIBS += -lqtcmddlgext 
INCLUDEPATH += ../../../../src \
               ../../../../build/.tmp/ui \
               ../../../libs/qtcmduiext/ \
               ../../../libs/qtcmddlgext \
	       ../../
MOC_DIR = ../../../../build/.tmp/moc 
OBJECTS_DIR = ../../../../build/.tmp/obj 
QMAKE_LIBDIR = ../../../../build/lib
TARGET = imageview 
DESTDIR = ../../../../build/lib/qtcmd/plugins 
CONFIG += warn_on \
          qt \
          thread \
          plugin 
TEMPLATE = lib 
HEADERS += imageview.h \
	    ../../view.h
SOURCES += imageview.cpp \
           imageviewplugin.cpp 
#The following line was inserted by qt3to4
QT += network  qt3support 
