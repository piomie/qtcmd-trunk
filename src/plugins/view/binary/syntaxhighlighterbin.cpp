/***************************************************************************
                          syntaxhighlighterbin.cpp  -  description
                             -------------------
    begin                : thu apr 17 2003
    copyright            : (C) 2002 by Piotr Mierzwi�ski
    email                : peterm@go2.pl

    Copyright: See COPYING file that comes with this project
$Id$
 ***************************************************************************/

#include "syntaxhighlighterbin.h"

/**
 Konstruktor klasy, zajmuj�cej si� "pod�wietlaniem" tekstu.
*/
SyntaxHighlighterBin::SyntaxHighlighterBin( QTextEdit * textEdit )
	: QSyntaxHighlighter(textEdit), mHexHighlighting(TRUE)
{
	mFont = QFont( textEdit->family(), textEdit->pointSize() );
}

/**
 Funkcja wywo�ywana automatycznie przy ka�dej zmianie zawarto�ci obiektu QTextEdit.
 W parametrze 'text' znajduje si� aktualnie zmodyfikowana linia (ci�g zako�czony znakiem '\n')
*/
//int SyntaxHighlighterBin::highlightParagraph( const QString & text, int endStateOfLastPara )
void SyntaxHighlighterBin::highlightBlock(const QString & text)
{
	int endStateOfLastPara = previousBlockState();

	if ( mHexHighlighting )
		setSyntaxHighlightingForHEX( text, endStateOfLastPara );
	else
		setFormat( 0, text.length(), QColor(0,0,0) ); // RAW mode
}

/**
 Inicjuje podanymi warto�ciami (color, bold, italic) na pozycji kontekstu kolorowania,
 tablic�, z kt�rej s� pobierane informacje o sposobie kolorowania w funkcji ustawiaj�cej
 atrybuty dla kontekstu.
*/
void SyntaxHighlighterBin::initAttributs( ContextOfSH contextOfSH, QColor color, bool bold, bool italic )
{
	AttrTab[(int)contextOfSH].color  = color;
	AttrTab[(int)contextOfSH].bold   = bold;
	AttrTab[(int)contextOfSH].italic = italic;
}

/**
 Funkcja ustawia atrybuty (color, bold, italic) dla podanego kontekstu 'contextOfSH'
*/
void SyntaxHighlighterBin::setAttributsFor( ContextOfSH contextOfSH )
{
	mColor = AttrTab[contextOfSH].color;
	mFont.setBold( AttrTab[contextOfSH].bold );
	mFont.setItalic( AttrTab[contextOfSH].italic );
}

/**
 Metoda pod�wietlanie sk�adni dla linii podgl�du HEX.
*/
int SyntaxHighlighterBin::setSyntaxHighlightingForHEX( const QString & text, int )
{
	setFormat( 0, text.length(), QColor(0,0,0) );
	setFormat( 0, 8, QColor(139,0,0) ); // offset
	int len = text.length()-(text.length() - text.find('|')) - 8;// - 2;
	setFormat( 9, len, QColor(0,0,139) ); // hex numbers

	return 0;
}

