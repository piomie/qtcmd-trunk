# Plik utworzony przez menad?era projekt�w kdevelopa
# ------------------------------------------- 
# Podkatalog wzgl?dem g?�wnego katalogu projektu: ./src/plugins/view/binary
# Cel to biblioteka binaryview

INSTALLS += target 
target.path = $$PREFIX/lib/qtcmd/plugins 
INCLUDEPATH += ../../../../build/.tmp/ui ../../ ../
MOC_DIR = ../../../../build/.tmp/moc 
OBJECTS_DIR = ../../../../build/.tmp/obj 
TARGET = binaryview 
DESTDIR = ../../../../build/lib/qtcmd/plugins 
CONFIG += warn_on \
          qt \
          thread \
          plugin 
TEMPLATE = lib 
HEADERS += binaryview.h \
           syntaxhighlighterbin.h \
	   ../../view.h
SOURCES += binaryview.cpp \
           binaryviewplugin.cpp \
           syntaxhighlighterbin.cpp 
#The following line was inserted by qt3to4
QT += network  qt3support 
