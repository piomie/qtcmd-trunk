/***************************************************************************
                          binaryview  -  description
                             -------------------
    begin                : mon sep 1 2003
    copyright            : (C) 2003 by Piotr Mierzwiński
    email                : peterm@go2.pl

    copyright            : See COPYING file that comes with this project
$Id$
 ***************************************************************************/
#include <QPrintDialog>
#include <QVBoxLayout>
#include <QTextEdit>
#include <QPrinter>
#include <QPainter>

#include "binaryview.h"
//#include "syntaxhighlighterbin.h"


BinaryView::BinaryView( QWidget * parent )
	: View(parent), mModeOfView(HEXmode)
{
	//qDebug("BinaryView::BinaryView");
	mAppendOffset = 0;
	mIsModified = FALSE;
	mFontFamily = "Fixed"; // Courier New, Courier [Adobe]
	mFontSize = 12;
	mFileSize = 0;

	QVBoxLayout *layout = new QVBoxLayout(this);
	layout->setContentsMargins(1, 1, 1, 1);
	mTextEdit = new QTextEdit( this );
	layout->addWidget(mTextEdit);

//	mTextEdit->viewport()->installEventFilter( this ); // for grab some mouse events from the QTextEdit obj.
//	mTextEdit->installEventFilter( this ); // for grab some KeyEvent from the View obj.
	mTextEdit->setModified( FALSE );
	mTextEdit->setReadOnly( TRUE );
	mTextEdit->setFocus();
	mTextEdit->setFamily( mFontFamily ); // Courier [Adobe]
	mTextEdit->setPointSize( mFontSize );
	mTextEdit->setBold( FALSE );
	mTextEdit->setItalic( FALSE );
	mTextEdit->setFont( mTextEdit->currentFont() );
	mTextEdit->setPaletteBackgroundColor( Qt::white );
	mTextEdit->showNormal();

	mSyntaxHighlighter = new SyntaxHighlighterBin( mTextEdit );
}

BinaryView::~BinaryView()
{
	// lib Qt cleaning all Qt object
	//delete mSyntaxHighlighter;
	//delete mTextEdit;
}

void BinaryView::updateContents( UpdateMode updateMode, ModeOfView modeOfView, const QString & , QByteArray & binBuffer )
{
	qDebug("BinaryView::updateContents(), modeOfView=%d, updateMode=%d", modeOfView, updateMode );
	mModeOfView = modeOfView;
	QString txtBuffer = "";
	if ( updateMode == NONE ) { // file has been loaded, need to move contents to top of view
		//mTextEdit->setContentsPos( 0,0 );
		mTextEdit->moveCursor(QTextCursor::Start);
		mAppendOffset = 0;
		return;
	}

	if ( updateMode == ALL )
		mSyntaxHighlighter->setHexHighlighting( (modeOfView != RAWmode) );

	if ( modeOfView == HEXmode ) {
		convertToHex( binBuffer, txtBuffer, updateMode );
		mAppendOffset = binBuffer.size();
	}
	else { // rewrite binBuffer to txtBuffer and replace not visible characters by '.'
		uint i = 0, b;
		uint bufSize = binBuffer.size()-1;
		while( i < bufSize ) {
			b = binBuffer[i];
			txtBuffer[i] = ((b < 32 || ( b < 161 && b > 126 )) ? '.' : (char)b );
			i++;
		}
	}
	bool viewIsEmpty = mTextEdit->document()->isEmpty();
	mTextEdit->setUndoRedoEnabled( FALSE );
	if ( viewIsEmpty && updateMode == APPEND ) {
		mSyntaxHighlighter->setHexHighlighting( (modeOfView != RAWmode) );
		mFileSize = 0;
	}
	if ( viewIsEmpty ||  updateMode != APPEND ) {
		mTextEdit->clear();
		mTextEdit->setTextFormat( Qt::PlainText );
	}
	if ( updateMode == APPEND ) {
		mTextEdit->append( QString::fromLocal8Bit(txtBuffer) );
		mFileSize += binBuffer.size()-1;
	}
	else {
		mTextEdit->setText( QString::fromLocal8Bit(txtBuffer) );
		mFileSize = binBuffer.size()-1;
	}
	mTextEdit->setUndoRedoEnabled( TRUE );
	updateStatus();
}

void BinaryView::updateStatus()
{
//	emit signalUpdateStatus( QString::number(mFileSize)+" "+tr("bytes") );

	//emit signalUpdateStatus( QString::number((mModeOfView == RAWmode) ? mTextEdit->length()-1 : mFileSize)+" "+tr("bytes") );
	emit signalUpdateStatus( QString::number((mModeOfView == RAWmode) ? mTextEdit->document()->size().width()-1 : mFileSize)+" "+tr("bytes") );
}

bool BinaryView::saveToFile( const QString & )
{
	return FALSE;
}

void BinaryView::convertToHex( const QByteArray & binBuffer, QString & txtBuffer, UpdateMode updateMode )
{
	qDebug("BinaryView::convertToHex, binBuffer.size()=%d", binBuffer.size() );

	txtBuffer = "";
	QChar chr;
	QString row, hexNumber;

	uint charWidth = QFontMetrics(QFont(mFontFamily, mFontSize)).width("a");
	uint maxChars = (((mTextEdit->width() / charWidth)-10)/4) - 1;
	uint bufSize = binBuffer.size(), i, max, offsetDiff;
	uint offset = 0;

	if ( maxChars == 0 )
		return;

	while( bufSize ) {
		max = (bufSize<maxChars) ? bufSize : maxChars;

		// ---- writes an offset
		hexNumber = QString::number( (updateMode == ALL) ? offset : mAppendOffset, 16 ).upper();
		offsetDiff = 8 - hexNumber.length();
		if ( hexNumber.length() < 8 )
			while(offsetDiff--)  hexNumber.insert(0, '0');
		row.append( hexNumber+" " );

		// ---- writes string of hex's numbers
		for( i=0; i<max; i++) {
			hexNumber = QString::number( (Q_UINT8)(binBuffer[offset+i]), 16).upper();
			if ( hexNumber.length() < 2 )  hexNumber.insert(0, '0');
			row.append( hexNumber+" " );
		}
		// fills empty space on the end of row by string of space's characters
		if ( bufSize < maxChars )
			for(uint a=0; a<(maxChars-max); a++)
				row.append("   ");
		// adds separator
		row.append("|");

		// ---- writes ascii string
		for( i=0; i<max; i++) {
			chr = binBuffer[offset+i];
            int c = chr.toAscii();
			if ( c < 32 || (c > 126 && c < 161) )  chr = '.';
			row.append( chr );
		}
		// for breaks line into textView
		row.append("\n");

		txtBuffer.append( row );
		bufSize-= i; offset+= i;
		if ( updateMode == APPEND )
			mAppendOffset+= i;
		row = "";
	}
}

void BinaryView::getData( QByteArray & binBuffer )
{
	if ( mModeOfView != HEXmode ) {
		binBuffer = mTextEdit->text().local8Bit();
		return;
	}
	// ponizsze jest b.wolne (juz dla 5kb znakow) !
	uint viewOffset = 8, bytesInLine, separatorPos = 0, bufferOffset = 0;
	QString hexLine, asciiLine;
	while ( viewOffset < mFileSize ) {
		separatorPos = mTextEdit->text().find( '|', viewOffset );
		hexLine = mTextEdit->text().mid( viewOffset, separatorPos-(viewOffset+1) );
		hexLine = hexLine.stripWhiteSpace();
		bytesInLine = hexLine.count( ' ' )+1;
		asciiLine = mTextEdit->text().mid( separatorPos+1, bytesInLine );
		viewOffset = separatorPos+1+bytesInLine+1+9;

		asciiLine = asciiLine.local8Bit();
		binBuffer.resize( bufferOffset+bytesInLine );
		for ( uint i=0; i<bytesInLine; i++ ) {
        //FIXME: eliminate .toLatin1()
			binBuffer[bufferOffset+i] = asciiLine[i].toLatin1();
        }
		bufferOffset += bytesInLine;
	}
}

void BinaryView::slotPrint()
{
	QPrinter printer(QPrinter::HighResolution);
	printer.setFullPage(true);

	QPrintDialog *pDlg = new QPrintDialog(&printer, this);

	if (mTextEdit->textCursor().hasSelection())
		pDlg->addEnabledOption(QAbstractPrintDialog::PrintSelection);

	pDlg->setWindowTitle(tr("Print Document"));
	if (pDlg->exec() == QDialog::Accepted) {
		emit signalUpdateStatus( tr("Printing...") );
		mTextEdit->print(&printer);
		emit signalUpdateStatus( tr("Printing completed"), 2000 );
	} else {
		emit signalUpdateStatus( tr("Printing aborted"), 2000 );
	}
	delete pDlg;
/*
	 // this code is come from qmakeapp.cpp (template of qt-application from KDevelop) whith small modification
	const int Margin = 10;
	int pageNo = 1;

	if ( mPrinter->setup(this) ) { // printer dialog
		emit signalUpdateStatus( tr("Printing...") );
		QPainter p;
		if( !p.begin( mPrinter ) ) // paint on printer
			return;

		p.setFont( QFont(mFontFamily, mFontSize) );
		int yPos	= 0; // y-position for each line
		QFontMetrics fm = p.fontMetrics();
		Q3PaintDeviceMetrics metrics( mPrinter ); // need width/height
		// of printer surface

		for( int i = 0 ; i < mTextEdit->lines() ; i++ ) {
			if ( Margin + yPos > metrics.height() - Margin ) {
				QString msg = "Printing (page ";
				msg += QString::number( ++pageNo ) + ")...";
				emit signalUpdateStatus( msg );
				mPrinter->newPage();  // no more room on this page
				yPos = 0;  // back to top of page
			}
			p.drawText( Margin, Margin + yPos,
			  metrics.width(), fm.lineSpacing(),
			  Qt::TextExpandTabs | Qt::TextDontClip,
			  mTextEdit->text( i )
			);
			yPos = yPos + fm.lineSpacing();
		}
		p.end(); // send job to printer
		emit signalUpdateStatus( tr("Printing completed") ); //, 2000 );
	}
	else
		emit signalUpdateStatus( tr("Printing aborted") ); //, 2000 );
*/
}

/*
void BinaryView::mouseMoveEvent( QMouseEvent *e )
{
	if ( mModeOfView == HEXmode ) {
		uint charWidth = QFontMetrics(mTextEdit->currentFont()).width("a");
		int row = e->pos().y() / (QFontMetrics(mTextEdit->currentFont()).height()+2);
		uint col = e->pos().x() / charWidth;
		uint maxChars = (((width() / charWidth)-10)/4) - 1;

		if ( row > lines() ) row = mTextEdit->lines();
		if ( row == 1 ) row=1;  if ( col == 0 ) col=1;
		if ( col > 8 && col < (maxChars*3)+7 ) {
			setSelection( row, col, row, col+2, 1 );
// 			qDebug("c_2=%d c_3=%d, c_3=%d", col%2,col%3,col%4 );
// 			setSelection( row, (col+(maxChars*3))+1, row, col+(maxChars*3)+1, 2 );
// 			setSelectionAttributes( 2, QColor(black), TRUE ); // dwa czarne zaznaczenia
		}
// 		if ( col > (maxChars*3)+9 ) { // znaki ascii
// 		}
	}
}

bool BinaryView::eventFilter( QObject *o, QEvent *e )
{
	if ( o == mTextEdit ) {
		if ( e->type() == QEvent::KeyPress )
			keyPressEvent( (QKeyEvent *)e );
	}
	else
	if ( o == mTextEdit->viewport() ) {
		if ( e->type() == QEvent::MouseMove )
			mouseMoveEvent( (QMouseEvent *)e );
	}

	return QWidget::eventFilter( o, e );    // standard event processing
}
*/
