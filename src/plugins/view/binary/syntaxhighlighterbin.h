/***************************************************************************
                          syntaxhighlighterbin.h  -  description
                             -------------------
    begin                : thu apr 17 2003
    copyright            : (C) 2003 by Piotr Mierzwiński
    email                : peterm@go2.pl

    Copyright: See COPYING file that comes with this project
$Id$
***************************************************************************/
#ifndef _SYNTAXHIGHLIGHTERBIN_H_
#define _SYNTAXHIGHLIGHTERBIN_H_

#include <QSyntaxHighlighter>
#include <QTextEdit>


class SyntaxHighlighterBin : public QSyntaxHighlighter
{
public:
	enum ContextOfSH {
		BASE_N_INTEGER,
		CHARACTER,
		COMMENT,
		DATA,
		DECIMALorVALUE,
		FLOATING_POINT,
		KEYWORD,
		NORMAL,
		OTHERS,
		STRING,
		EXTENSION
	};

	SyntaxHighlighterBin( QTextEdit * textEdit );

	void initAttributs( ContextOfSH, QColor, bool, bool );

	void setHexHighlighting( bool hexHighl=TRUE ) { mHexHighlighting = hexHighl; }

private:
	QColor mColor;
	QFont mFont;
	bool mHexHighlighting;

	struct Attribut {
		QColor color;
		bool bold;
		bool italic;
	};
	Attribut AttrTab[11];

protected:
	void highlightBlock(const QString &text);

	int setSyntaxHighlightingForHEX( const QString &, int );

	void setAttributsFor( ContextOfSH );

};

#endif
