#ifndef FINDTEXTDIALOG_H
#define FINDTEXTDIALOG_H

#include <qvariant.h>


#include <Qt3Support/Q3ButtonGroup>
#include <Qt3Support/Q3MimeSourceFactory>
#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QCheckBox>
#include <QtGui/QComboBox>
#include <QtGui/QDialog>
#include <QtGui/QGridLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QPushButton>
#include <QtGui/QRadioButton>
#include <QtGui/QSpacerItem>

QT_BEGIN_NAMESPACE

class Ui_FindTextDialog
{
public:
    QGridLayout *gridLayout;
    QLabel *mNewTextLabel;
    QLabel *mTextLabel;
    QComboBox *mFindTextComboBox;
    QComboBox *mNewTextComboBox;
    Q3ButtonGroup *mOptionsBtnGroup;
    QGridLayout *gridLayout1;
    QRadioButton *mBackwardRB;
    QCheckBox *mCaseSensitiveChkBox;
    QRadioButton *mForwardRB;
    QCheckBox *mWholeWordsOnlyChkBox;
    QCheckBox *mFromCursorChkBox;
    QCheckBox *mPromptChkBox;
    QPushButton *mFRButton;
    QPushButton *mCancelButton;
    QSpacerItem *spacer;

    void setupUi(QDialog *FindTextDialog)
    {
        if (FindTextDialog->objectName().isEmpty())
            FindTextDialog->setObjectName(QString::fromUtf8("FindTextDialog"));
        FindTextDialog->resize(295, 233);
        FindTextDialog->setMinimumSize(QSize(287, 226));
        gridLayout = new QGridLayout(FindTextDialog);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        mNewTextLabel = new QLabel(FindTextDialog);
        mNewTextLabel->setObjectName(QString::fromUtf8("mNewTextLabel"));
        mNewTextLabel->setWordWrap(false);

        gridLayout->addWidget(mNewTextLabel, 1, 0, 1, 1);

        mTextLabel = new QLabel(FindTextDialog);
        mTextLabel->setObjectName(QString::fromUtf8("mTextLabel"));
        mTextLabel->setWordWrap(false);

        gridLayout->addWidget(mTextLabel, 0, 0, 1, 1);

        mFindTextComboBox = new QComboBox(FindTextDialog);
        mFindTextComboBox->setObjectName(QString::fromUtf8("mFindTextComboBox"));
        mFindTextComboBox->setEditable(true);

        gridLayout->addWidget(mFindTextComboBox, 0, 1, 1, 3);

        mNewTextComboBox = new QComboBox(FindTextDialog);
        mNewTextComboBox->setObjectName(QString::fromUtf8("mNewTextComboBox"));
        mNewTextComboBox->setEditable(true);

        gridLayout->addWidget(mNewTextComboBox, 1, 1, 1, 3);

        mOptionsBtnGroup = new Q3ButtonGroup(FindTextDialog);
        mOptionsBtnGroup->setObjectName(QString::fromUtf8("mOptionsBtnGroup"));
        mOptionsBtnGroup->setColumnLayout(0, Qt::Vertical);
        mOptionsBtnGroup->layout()->setSpacing(6);
        mOptionsBtnGroup->layout()->setContentsMargins(11, 11, 11, 11);
        gridLayout1 = new QGridLayout();
        QBoxLayout *boxlayout = qobject_cast<QBoxLayout *>(mOptionsBtnGroup->layout());
        if (boxlayout)
            boxlayout->addLayout(gridLayout1);
        gridLayout1->setAlignment(Qt::AlignTop);
        gridLayout1->setObjectName(QString::fromUtf8("gridLayout1"));
        mBackwardRB = new QRadioButton(mOptionsBtnGroup);
        mBackwardRB->setObjectName(QString::fromUtf8("mBackwardRB"));

        gridLayout1->addWidget(mBackwardRB, 1, 2, 1, 1);

        mCaseSensitiveChkBox = new QCheckBox(mOptionsBtnGroup);
        mCaseSensitiveChkBox->setObjectName(QString::fromUtf8("mCaseSensitiveChkBox"));

        gridLayout1->addWidget(mCaseSensitiveChkBox, 0, 0, 1, 1);

        mForwardRB = new QRadioButton(mOptionsBtnGroup);
        mForwardRB->setObjectName(QString::fromUtf8("mForwardRB"));

        gridLayout1->addWidget(mForwardRB, 0, 2, 1, 1);

        mWholeWordsOnlyChkBox = new QCheckBox(mOptionsBtnGroup);
        mWholeWordsOnlyChkBox->setObjectName(QString::fromUtf8("mWholeWordsOnlyChkBox"));

        gridLayout1->addWidget(mWholeWordsOnlyChkBox, 1, 0, 1, 2);

        mFromCursorChkBox = new QCheckBox(mOptionsBtnGroup);
        mFromCursorChkBox->setObjectName(QString::fromUtf8("mFromCursorChkBox"));

        gridLayout1->addWidget(mFromCursorChkBox, 2, 0, 1, 1);

        mPromptChkBox = new QCheckBox(mOptionsBtnGroup);
        mPromptChkBox->setObjectName(QString::fromUtf8("mPromptChkBox"));

        gridLayout1->addWidget(mPromptChkBox, 2, 1, 1, 2);


        gridLayout->addWidget(mOptionsBtnGroup, 2, 0, 1, 4);

        mFRButton = new QPushButton(FindTextDialog);
        mFRButton->setObjectName(QString::fromUtf8("mFRButton"));

        gridLayout->addWidget(mFRButton, 3, 2, 1, 1);

        mCancelButton = new QPushButton(FindTextDialog);
        mCancelButton->setObjectName(QString::fromUtf8("mCancelButton"));

        gridLayout->addWidget(mCancelButton, 3, 3, 1, 1);

        spacer = new QSpacerItem(90, 16, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout->addItem(spacer, 3, 0, 1, 2);

#ifndef QT_NO_SHORTCUT
        mNewTextLabel->setBuddy(mNewTextComboBox);
        mTextLabel->setBuddy(mFindTextComboBox);
#endif // QT_NO_SHORTCUT

        retranslateUi(FindTextDialog);
        QObject::connect(mCancelButton, SIGNAL(clicked()), FindTextDialog, SLOT(reject()));
        QObject::connect(mFRButton, SIGNAL(clicked()), FindTextDialog, SLOT(accept()));

        QMetaObject::connectSlotsByName(FindTextDialog);
    } // setupUi

    void retranslateUi(QDialog *FindTextDialog)
    {
        FindTextDialog->setWindowTitle(QApplication::translate("FindTextDialog", "FindTextDialog", 0, QApplication::UnicodeUTF8));
        mNewTextLabel->setText(QApplication::translate("FindTextDialog", "&New text:", 0, QApplication::UnicodeUTF8));
        mTextLabel->setText(QApplication::translate("FindTextDialog", "&Text:", 0, QApplication::UnicodeUTF8));
        mOptionsBtnGroup->setTitle(QApplication::translate("FindTextDialog", "Options", 0, QApplication::UnicodeUTF8));
        mBackwardRB->setText(QApplication::translate("FindTextDialog", "&Backward", 0, QApplication::UnicodeUTF8));
        mCaseSensitiveChkBox->setText(QApplication::translate("FindTextDialog", "C&ase sensitive", 0, QApplication::UnicodeUTF8));
        mForwardRB->setText(QApplication::translate("FindTextDialog", "F&orward", 0, QApplication::UnicodeUTF8));
        mWholeWordsOnlyChkBox->setText(QApplication::translate("FindTextDialog", "&Whole words only", 0, QApplication::UnicodeUTF8));
        mFromCursorChkBox->setText(QApplication::translate("FindTextDialog", "From c&ursor", 0, QApplication::UnicodeUTF8));
        mPromptChkBox->setText(QApplication::translate("FindTextDialog", "&Prompt on replace", 0, QApplication::UnicodeUTF8));
        mFRButton->setText(QApplication::translate("FindTextDialog", "&Find", 0, QApplication::UnicodeUTF8));
        mCancelButton->setText(QApplication::translate("FindTextDialog", "&Cancel", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class FindTextDialog: public Ui_FindTextDialog {};
} // namespace Ui

QT_END_NAMESPACE

class FindTextDialog : public QDialog, public Ui::FindTextDialog
{
    Q_OBJECT

public:
    FindTextDialog(QWidget* parent = 0, const char* name = 0, bool modal = false, Qt::WindowFlags fl = 0);
    ~FindTextDialog();

    static bool showDialog( QWidget * parent, const QStringList & initFindStrLst, QString & findedStr, const QStringList & initNewStrLst, QString & newStr, bool & cs, bool & who, bool & fromCursor, bool & findForward, bool & por, bool replace=FALSE );

protected slots:
    virtual void languageChange();

};

#endif // FINDTEXTDIALOG_H
