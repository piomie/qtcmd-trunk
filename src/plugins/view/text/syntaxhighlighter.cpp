/***************************************************************************
                          syntaxhighlighter.cpp  -  description
                             -------------------
    begin                : thu apr 17 2003
    copyright            : (C) 2002 by Piotr Mierzwi�ski
    email                : peterm@go2.pl

    Copyright: See COPYING file that comes with this project
$Id$
 ***************************************************************************/
#include <QSettings>
#include <QRegExp>

#include "functions_col.h"
#include "syntaxhighlighter.h"
#include "syntaxhighlighter_tables.cpp"

// TODO wprowadzic obsluge kontekstu kolorowania w oparciu o wyrazenia regularne

//#include <QtGui>

SyntaxHighlighter::SyntaxHighlighter( QTextEdit * textEdit )
	: QSyntaxHighlighter(textEdit)
{
	mFont = textEdit->currentFont();

	// --- inits rulles (list format: Context;default highlighting)
	mCcppRullesList.append( "Base N Integer;FF00FF,," );
	mCcppRullesList.append( "Character;008080,," );
	mCcppRullesList.append( "Comment;808080,,Italic" );
	mCcppRullesList.append( "Data type;800000,," );
	mCcppRullesList.append( "Decimal or Value;0000FF,," );
	mCcppRullesList.append( "Extentions;0095FF,Bold," );
	mCcppRullesList.append( "Floating point;800080,," );
	mCcppRullesList.append( "Key word;000000,Bold," );
	mCcppRullesList.append( "Normal;000000,," );
	mCcppRullesList.append( "Preprocesor;008000,," );
	mCcppRullesList.append( "String;FF0000,," );

	mHtmlRullesList.append( "Comment;808080,,Italic" );
	mHtmlRullesList.append( "Data type;800000,," );
	mHtmlRullesList.append( "Decimal or Value;0000FF,," );
	mHtmlRullesList.append( "Key word;000000,Bold," );
	mHtmlRullesList.append( "Normal;000000,," );
	mHtmlRullesList.append( "String;FF0000,," );

	// --- init context map
	mContextMap.insert( "Base N Integer", BASE_N_INTEGER );
	mContextMap.insert( "Character", CHARACTER );
	mContextMap.insert( "Comment", COMMENT );
	mContextMap.insert( "Data type", DATA );
	mContextMap.insert( "Decimal or Value", DECIMALorVALUE );
	mContextMap.insert( "Extentions", EXTENSION );
	mContextMap.insert( "Floating point", FLOATING_POINT );
	mContextMap.insert( "Key word", KEYWORD );
	mContextMap.insert( "Normal", NORMAL );
	mContextMap.insert( "Preprocesor", OTHERS );
	mContextMap.insert( "String", STRING );
}


void SyntaxHighlighter::setSyntaxHighlighting( SyntaxHighlighting sh, const QString & ext )
{
	if ( sh != AUTO )
		mSyntaxHighlighting = sh;
	else {
		if ( ext == "cpp" || ext == "cc" || ext == "C" || ext == "h" )
			mSyntaxHighlighting = CCpp;
		else
		if ( ext == "htm" || ext == "html" || ext == "shtml" || ext == "xml" )
			mSyntaxHighlighting = HTML;
		else
		if ( ext.find("php") == 0 )
			mSyntaxHighlighting = PHP;
		else
		if ( ext == "sh" )
			mSyntaxHighlighting = BASH;
		else
		if ( ext == "pl" )
			mSyntaxHighlighting = PERL;
		else
		if ( ext == "py" )
			mSyntaxHighlighting = PYTHON;
		else
			mSyntaxHighlighting = NONE;
	}

	if ( mSyntaxHighlighting == NONE ) // turn off highlighting
		return;

	QSettings *settings = new QSettings;
	QString context, newSH, shName;
	QStringList rullesList;
	bool bold, italic;

	if ( mSyntaxHighlighting == CCpp ) {
		rullesList = mCcppRullesList;
		shName = "C++/";
	}
	else
	if ( mSyntaxHighlighting == HTML ) {
		rullesList = mHtmlRullesList;
		shName = "Html/";
	}
// 	else
// 	if ( mSyntaxHighlighting == BASH ) {
// 		rullesList = mBashRullesList;
// 		shName = "Bash/";
// 	}
// 	else
// 	if ( mSyntaxHighlighting == PERL ) {
// 		rullesList = mPerlRullesList;
// 		shName = "Perl/";
// 	}

	for ( int i=0; i<rullesList.count(); i++ ) {
		context = rullesList[i].section( ';', 0,0 );
		newSH = settings->readEntry( "/qtcmd/SyntaxHighlighting/"+shName+context, rullesList[i].section(';', 1,1) );

		bold   = newSH.section( ',', 1,1 ).lower() == "bold";
		italic = newSH.section( ',', 2,2 ).lower() == "italic";
		newSH  = newSH.section( ',', 0,0 ); // color

        int ctx = mContextMap[context];
		initAttributsTable( ctx, color(newSH), bold, italic );
	}

	delete settings;
}


//int SyntaxHighlighter::highlightParagraph( const QString & text, int endStateOfLastPara )
void SyntaxHighlighter::highlightBlock(const QString &text)
{
	int state = 0;
	int endStateOfLastPara = previousBlockState();

	if ( mSyntaxHighlighting == CCpp )
		state = setSyntaxHighlightingForCCpp( text, endStateOfLastPara );
	else
	if ( mSyntaxHighlighting == HTML )
		state = setSyntaxHighlightingForHtml( text, endStateOfLastPara );
	else
	if ( mSyntaxHighlighting == HEX )
		state = setSyntaxHighlightingForHEX( text, endStateOfLastPara );
	else
	if ( mSyntaxHighlighting == NONE )
		QSyntaxHighlighter::setFormat( 0, text.length(), QColor(0,0,0) );

	if (state != 0)
		setCurrentBlockState(state);
}


void SyntaxHighlighter::initAttributsTable( int context, const QColor & color, bool bold, bool italic )
{
	AttrTab[context].color  = color;
	AttrTab[context].bold   = bold;
	AttrTab[context].italic = italic;
}


void SyntaxHighlighter::setAttributsFor( ContextOfSH context )
{
	mColor = AttrTab[context].color;
	mFont.setBold( AttrTab[context].bold );
	mFont.setItalic( AttrTab[context].italic );
}



namespace {

    bool isPartOfNumber(const QChar &c)
    {
        QString tab(".-abcdefABCDEFlLuU");
        return c.isDigit() || tab.indexOf(c) != -1;
    }

    /* Returns the character at index i, or 0 if i is beyond the length of the string.
    */
    QChar qt3_at(const QString &text, int pos) {
        if (pos >= text.length())
            return 0;
        return text.at(pos);
    }

}//namespace

int SyntaxHighlighter::setSyntaxHighlightingForCCpp( const QString & text, int endStateOfLastPara )
{
    QChar chr;
    QString word;
	int id, textLength=text.length();
    int pos = 0, beginPos=0, endStat = 0;
    //uint wordLen, keywordLen;

    setAttributsFor( NORMAL );
    setFormat( 0, textLength, mFont, mColor );

    if ( endStateOfLastPara == 1 ) { // komentarz wieloliniowy
        setAttributsFor( COMMENT );
        int p = text.indexOf("*/");
        if (p == -1) {
            setFormat( 0, textLength, mFont, mColor );
            return 1; // inside comment
        } else {
            pos = p + 2;
            setFormat( 0, pos, mFont, mColor );
        }
    }

    while ( pos < textLength ) {

        while( pos < textLength && (text.at(pos) == ' ' || text.at(pos) == '\t') )
            ++pos; // omit some white space character

        if ( pos >= textLength )
            break;

        chr = text.at(pos);
        // --- check for comment
        if ( chr == '/' ) {
            if (pos + 1 < textLength) {
                QChar chr2 = text.at(pos + 1);
                if (chr2 == '/')
                {
                    setAttributsFor( COMMENT );
                    setFormat( pos, textLength - pos, mFont, mColor );
                    break;
                }
                else if (chr2 == '*')
                {
                    setAttributsFor( COMMENT );
                    int p = text.indexOf("*/", pos + 2);
                    if (p == -1) {
                        setFormat( pos, textLength - pos, mFont, mColor );
                        return 1; // inside comment
                    } else {
                        setFormat( pos, p + 2, mFont, mColor );
                        pos = p + 2;
                    }
                }
            }
        }

        // --- check for number
        if (chr.isDigit() || chr == '.')
        {
            bool dot = false;
            if (chr == '.')
                dot = true;

            bool isNumber = true;
            if (pos >= 1 && ! charOkForDigit(text.at(pos-1)))
                isNumber = false;

            int endOfNumber = pos + 1;
            bool isHex = false;
            if (chr == '0' && endOfNumber + 1 < textLength
                && (text.at(endOfNumber) == 'x' || text.at(endOfNumber) == 'X'))
            {
                if ( ! chr.isLetterOrNumber() || ! isPartOfNumber(chr))
                    isNumber = false;
                else
                    isHex = true;
                endOfNumber += 2;
            }

            QChar prevChr;
            while (endOfNumber < textLength)
            {
                chr = text.at(endOfNumber);
                if ( ! isPartOfNumber(chr) )
                    break;

                if (chr == '.')
                {
                    if ( dot ) {
                        isNumber = false;
                        break;
                    } else dot = true;
                }

                if (chr == '-' && prevChr != 'e' && prevChr != 'E') {
                    isNumber = false;
                    break;
                }

                prevChr = chr;
                ++endOfNumber;
            }

            if (endOfNumber < textLength && ! charOkForDigit(chr))
                isNumber = false;

            // TODO: valodation for integer (octal & hex), float

            if ( isNumber ) {
                if (text.at(pos) == '0')
                    setAttributsFor( BASE_N_INTEGER );
                else if (dot)
                    setAttributsFor( FLOATING_POINT );
                else
                    setAttributsFor( DECIMALorVALUE );

                setFormat( pos, endOfNumber, mFont, mColor );
            }

            pos = endOfNumber;
            continue;
        }

        // --- preprocesor fe. #include "file"
        if ( chr == '#' && pos == 0 ) {
            beginPos = pos;
            setAttributsFor( OTHERS );
            if ( (id=text.find("//")) != -1 || (id=text.find("/*")) != -1 ) {
                pos = id-1;
                setFormat( beginPos, pos, mFont, mColor );
            } else {
                setFormat( beginPos, textLength-pos, mFont, mColor );
                pos = textLength;
                continue;
            }
        }

        // --- czysty ciag tekstowy i mieszany ze znakami specjalnymi ("tekst", "abc\ndef")
        if ( chr == '"' ) {
            setAttributsFor( STRING );
            beginPos = pos;
            pos++;
            while ( pos < textLength ) {
                if ( text.at(pos) == '\\' ) {
                    setFormat( beginPos, pos-beginPos, mFont, mColor ); // end of string
                    beginPos = pos;
                    setAttributsFor( CHARACTER );
                    while( text.at(pos) == '\\' ) {
                        if ( specChar(text.at(pos+1)) )
                            pos+=2;
                        else
                            break;
                    }

                    setFormat( beginPos, pos-beginPos, mFont, mColor );
                    if ( pos != textLength ) {
                        setAttributsFor( STRING );
                        beginPos = pos;
                        if ( text.at(pos) != '\\' ) // if '\' then increase pos
                            continue; // pos not need increased
                     } else break; // end of line
                }
                else {
                    if ( text.at(pos) == '"' || pos == textLength ) {
                        if ( pos != textLength ) // this same what text.at(pos) == '"' ) but faster
                            pos++;
                        setFormat( beginPos, pos-beginPos, mFont, mColor );
                        if ( pos < textLength && text.at(pos) == '"' )  pos--; // for prevent omited next char.
                        break;
                    }
                }
                pos++;
            }
        }

        // --- znak, typu: 'n' i znaki specjalne
        if ( chr == '\'' ) {
            if ( qt3_at(text, pos+1) == '\\' ) {
                if ( ! specChar(qt3_at(text, pos+2)) ) {
                    pos += 4;
                    continue;
                }
            }

            if ( pos+1 != textLength ) {
                id = ( qt3_at(text,pos+1) == '\\' && specChar(qt3_at(text,pos+2)) ) ? 1 : 0;
                pos += id;
                if ( qt3_at(text,pos+2) == '\'' ) {
                    setAttributsFor( CHARACTER );
                    beginPos = pos-id;
                    pos += 2;
                    setFormat( beginPos, (pos-beginPos)+1, mFont, mColor );
                }
            }
        }

        ++pos;
    }//while

    // --- cpp keywords
    setAttributsFor( KEYWORD );
    int inx = 0;
    while (cpp_keywords[inx])
    {
        QRegExp expr("\\b"+QString(cpp_keywords[inx])+"\\b");
        int index = text.indexOf(expr);
        while (index >= 0) {
            int length = expr.matchedLength();
            setFormat(index, length, mFont, mColor);
            index = text.indexOf(expr, index + length);
        }
        ++inx;
    }

    // --- cpp types
    setAttributsFor( DATA );
    inx = 0;
    while (cpp_types[inx])
    {
        QRegExp expr("\\b"+QString(cpp_types[inx])+"\\b");
        int index = text.indexOf(expr);
        while (index >= 0) {
            int length = expr.matchedLength();
            setFormat(index, length, mFont, mColor);
            index = text.indexOf(expr, index + length);
        }
        ++inx;
    }
    // --- qt keywords
    setAttributsFor( EXTENSION );
    inx = 0;
    while (qt_extension[inx])
    {
        QRegExp expr("\\b"+QString(qt_extension[inx])+"\\b");
        int index = text.indexOf(expr);
        while (index >= 0) {
            int length = expr.matchedLength();
            setFormat(index, length, mFont, mColor);
            index = text.indexOf(expr, index + length);
        }
        ++inx;
    }

    return endStat;
}


int SyntaxHighlighter::setSyntaxHighlightingForHtml( const QString & text, int endStateOfLastPara )
{
	int beginPos = 0, len=text.length(), cutLen, offset, offsetTag;
	QString txt, txtTag;
	bool isTagO;
// 	qDebug("endStateOfLastPara=%d, text=%s", endStateOfLastPara, text.toLatin1().data() );
// === OPTYMALIZACJA:
// 1. wyrazenia reg. mozna wrzucic do QStringList, i jechac petla po nich

	setAttributsFor( NORMAL );
	setFormat( 0, len, mFont, mColor );

	// --- multiline comment
	if ( endStateOfLastPara == 1 ) {
		if ( (beginPos=text.find("-->")) != -1 ) {
			txtTag = text;
			len = beginPos+3;
			cutLen = txtTag.length()-len;
			offsetTag = text.length()-cutLen;
			setFormat( 0, len, mFont, mColor );
			txtTag = txtTag.right( cutLen );
			if ( txtTag.length() < 2 )
				return 0;
		}
		else {
			setFormat( 0, text.length(), mFont, mColor );
			return 1;
		}
	}
	else {
		offsetTag = 0;
		txtTag = text;
	}
	// --- opened tag and tag inside
	setAttributsFor( KEYWORD );
	QRegExp tagO("<[^!%?][^>]*>"); // regexp from BlueFish
	isTagO = FALSE;
	while( (beginPos=txtTag.find(tagO)) != -1 ) {
		len = tagO.cap().length();
		cutLen = txtTag.length()-(beginPos+len);
		setFormat( beginPos+offsetTag, len, mFont, mColor );
		offsetTag = text.length()-cutLen;
		txtTag = txtTag.right( cutLen );
		isTagO = TRUE;
	}

	if ( isTagO ) {
		// - subtags
		offset = 0; txt = text;
		setAttributsFor( DATA );
		QRegExp data(" [^<\"]*=");
		while( (beginPos=txt.find( data )) != -1 ) {
			len = data.cap().length();
			cutLen = txt.length()-(beginPos+len);
			setFormat( beginPos+offset, len, mFont, mColor );
			offset = text.length()-cutLen;
			txt = txt.right( cutLen );
		}
		// - values of subtags into ""
		offset = 0; txt = text;
		setAttributsFor( STRING );
		QRegExp value("\"[^\"]*\""); // regexp from BlueFish
		while( (beginPos=txt.find(value)) != -1 ) {
			len = value.cap().length();
			cutLen = txt.length()-(beginPos+len);
			setFormat( beginPos+offset, len, mFont, mColor );
			offset = text.length()-cutLen;
			txt = txt.right( cutLen );
		}
		// - hex numbers
		offset = 0; txt = text;
		setAttributsFor( DECIMALorVALUE );
		//QRegExp hexval("\"?#[0-9A-Za-z]{6}\"?");
		QRegExp hexval("[\"=]#[0-9A-Za-z]{6}\""); // "#mirror" is colored into expr. href="#mirror", need to check is before is 'href'
		while( (beginPos=txt.find(hexval)) != -1 ) {
			len = hexval.cap().length();
			cutLen = txt.length()-(beginPos+len);
			setFormat( beginPos+offset, len, mFont, mColor );
			offset = text.length()-cutLen;
			txt = txt.right( cutLen );
		}
		// - decimal numbers in ""
		offset = 0; txt = text;
//		setAttributsFor( DECIMALorVALUE );
		QRegExp decval("\"[- ]?[0-9]+[% pxPXptPT]*\"");
		while( (beginPos=txt.find(decval)) != -1 ) {
			len = decval.cap().length();
			cutLen = txt.length()-(beginPos+len);
			setFormat( beginPos+offset, len, mFont, mColor );
			offset = text.length()-cutLen;
			txt = txt.right( cutLen );
		}
		// - decimal numbers prefixed by '='
		offset = 0; txt = text;
//		setAttributsFor( DECIMALorVALUE );
		QRegExp equdec("=[0-9]+%?");
		while( (beginPos=txt.find(equdec)) != -1 ) {
			len = equdec.cap().length()-1; // -/+1 it's correction for skiping '="
			cutLen = txt.length()-(beginPos+len);
			setFormat( beginPos+offset+1, len, mFont, mColor );
			offset = text.length()-cutLen;
			txt = txt.right( cutLen );
		}
		// - value prefixed by '=', eg.expr.: align=center
		// ....
	}
	// --- closed tag
	offset = 0;
	txt = text;
	setAttributsFor( KEYWORD );
	QRegExp tagC("(<\?)([^?]|\?[^>])*(\?>)"); // regexp from BlueFish
	while( (beginPos=txt.find(tagC)) != -1 ) {
		len = tagC.cap().length();
		cutLen = txt.length()-(beginPos+len);
		setFormat( beginPos+offset, len, mFont, mColor );
		offset = text.length()-cutLen;
		txt = txt.right( cutLen );
	}
	// --- comment (one line)
	offset = 0;
	txt = text;
	setAttributsFor( COMMENT );
	QRegExp coment("(<!--)([^>]|([^-]>))*-->"); // regexp from BlueFish
	while( (beginPos=txt.find(coment)) != -1 ) {
		len = coment.cap().length();
		cutLen = txt.length()-(beginPos+len);
		setFormat( beginPos+offset, len, mFont, mColor );
		offset = text.length()-cutLen;
		txt = txt.right( cutLen );
	}
	// - check is it begin of multiline comment
	if ( beginPos == -1 ) {
		QRegExp coment("(<!--)([^>]|([^-]>))*"); // regexp from BlueFish
		if ( (beginPos=txt.find(coment)) != -1 ) {
			setAttributsFor( COMMENT );
			len = coment.cap().length();
			setFormat( beginPos, len, mFont, mColor );
			return 1;
		}
	}

	return 0;
}


int SyntaxHighlighter::setSyntaxHighlightingForHEX( const QString & text, int )
{
	QSyntaxHighlighter::setFormat( 0, text.length(), QColor(0,0,0) );
	QSyntaxHighlighter::setFormat( 0, 8, QColor(139,0,0) ); // offset
	int len = text.length()-(text.length() - text.find('|')) - 8;// - 2;
	QSyntaxHighlighter::setFormat( 9, len, QColor(0,0,139) ); // hex numbers

	return 0;
}


bool SyntaxHighlighter::charOkForDigit( const QChar & chr )
{
	return ( !(chr > '?' && chr < '[') && !(chr > '^' && chr < '{') && (chr != '$' || chr == '\n' || chr == '\t') );
}


bool SyntaxHighlighter::charOkBehindKeyword( const QChar & chr )
{
	// after c/cpp keywords can find folowing chars:
	const char charsTab[13] = { '(',')', '{','}', '[',']', ' ', '\t', '\n', ':', ',', ';', 0 };
	bool ok = FALSE;
	uint idx = 0;

	while( charsTab[idx] != chr && idx < 13 ) idx++;
	if( idx < 12 )
		ok = TRUE;

	return ok;
}


bool SyntaxHighlighter::specChar( const QChar & chr )
{
	const char charsTab[12] = { 'e','r','t','a','f','v','b','n','\'','\\','\"', 0 };
	bool ok = FALSE;
	uint idx = 0;

	if ( chr.isDigit() )
		return TRUE;

	while( charsTab[idx] != chr && idx < 12 ) idx++;
	if( idx < 11 )
		ok = TRUE;

	return ok;
}

void SyntaxHighlighter::setFormat(int start, int count, const QFont & font, const QColor & color)
{
	QTextCharFormat format;
	format.setFont(font);
	format.setForeground(color);
	QSyntaxHighlighter::setFormat(start, count, format);
}

void SyntaxHighlighter::setFontSize( qreal nSize )
{
	mFont.setPointSizeF(nSize);
	rehighlight();
}
