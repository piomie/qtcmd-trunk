# Plik utworzony przez menad?era projekt�w kdevelopa
# ------------------------------------------- 
# Podkatalog wzgl?dem g?�wnego katalogu projektu: ./src/plugins/view/text
# Cel to biblioteka textview

INSTALLS += target 
target.path = $$PREFIX/lib/qtcmd/plugins 
LIBS += -lqtcmduiext \
        -lqtcmdutils \
        -lqtcmddlgext 
INCLUDEPATH += ../../../../src \
               ../../../../src/libs/qtcmddlgext \
               ../../../../src/libs/qtcmdutils \
               ../../../../src/libs/qtcmduiext \
               ../../../../build/.tmp/ui \
	       ../../
MOC_DIR = ../../../../build/.tmp/moc 
OBJECTS_DIR = ../../../../build/.tmp/obj 
QMAKE_LIBDIR = ../../../../build/lib
TARGET = textview 
DESTDIR = ../../../../build/lib/qtcmd/plugins 
CONFIG += warn_on \
          qt \
          thread \
          plugin
TEMPLATE = lib
#The following line was changed from FORMS to FORMS3 by qt3to4
FORMS3 += findtextdialog.ui
TRANSLATIONS += ../../../../translations/pl/qtcmd-view-text.ts
HEADERS += syntaxhighlighter.h \
           textview.h \
           findtextdialog.ui.h \
	   ../../view.h
SOURCES += syntaxhighlighter.cpp \
           syntaxhighlighter_tables.cpp \
           textview.cpp \
           textviewplugin.cpp 
#The following line was inserted by qt3to4
QT += network  qt3support 
#The following line was inserted by qt3to4
CONFIG += uic3
