/***************************************************************************
                          textview.cpp  -  description
                             -------------------
    begin                : tue apr 15 2003
    copyright            : (C) 2003 by Piotr Mierzwi�ski
    email                : peterm@go2.pl

    Copyright: See COPYING file that comes with this project
$Id$
***************************************************************************/

#include "textview.h"
#include "messagebox.h"
#include "findtextdialog.h"
#include "../../../../icons/view_icons.h"
#include "../../../../icons/textview_icons.h"

#include <QDesktopWidget>
#include <QPrintDialog>
#include <QInputDialog>
#include <QTextStream>
#include <QListWidget>
#include <QScrollBar>
#include <QClipboard>
#include <QSettings>
#include <QPrinter>
#include <QMenu>

//#include <QKeyEvent>

//#include <QtGui>

TextView::TextView( QWidget *parent )
	: View(parent), mParent(parent), mAutoSH(TRUE)
{
	mEditToolBarActions = NULL;
	mViewToolBarActions = NULL;
	mViewMenuActions    = NULL;
	mEditMenuActions    = NULL;
	mCompletionListBox  = NULL;
	mKeyShortcuts = NULL;

	mFindedText = "";
	mInsStatStr = "INS"; // default for the QTextEdit obj.
	mKindOfEOL = UNKNOWNeol;
	mStartFindFromCursorPos = TRUE;
	mPromptOnReplace = FALSE;
	mWholeWordsOnly = FALSE;
	mCaseSensitive = TRUE;
	mFindForward = TRUE;
	mFromBegin = TRUE;
	mFindOnce = FALSE;
	mOvrMode = FALSE;

	mTextEdit = new QTextEdit( this );
	mTextEdit->viewport()->installEventFilter( this ); // for grab some mouse events from the QTextEdit obj.
	mTextEdit->installEventFilter( this ); // for grab some KeyEvent from the View obj.
	mTextEdit->setReadOnly( TRUE );
	mTextEdit->setUndoRedoEnabled(true);
	mTextEdit->setFocus();
	mTextEdit->showNormal();

	QVBoxLayout *layout = new QVBoxLayout(this);
	layout->setContentsMargins(1,1,1,1);
	layout->addWidget(mTextEdit);

	connect( mTextEdit, SIGNAL(cursorPositionChanged()), this, SLOT(slotCursorPositionChanged()) );

	initView(); // read info from the config file
	mSyntaxHighlighter = new SyntaxHighlighter( mTextEdit );

	initMenuAndToolBarActions(); // read info (about toolBars) from the config file
	setEncoding(); // init encoding (default is auto)
}


void TextView::initView()
{
	QSettings *settings = new QSettings;

	QString font = settings->readEntry( "/qtcmd/TextFileView/Font" );
	if ( font.isEmpty() )
		font = "Helvetica,10"; // default
	// - parse 'font'
	mFontFamily = font.left( font.find(',') );
	QString fontSizeStr = font.section( ',', 1,1 ).stripWhiteSpace();
	QString boldStr     = font.section( ',', 2,2 ).stripWhiteSpace().lower();
	QString italicStr   = font.section( ',', 3,3 ).stripWhiteSpace().lower();

	bool ok;
	mFontSize = fontSizeStr.toInt( &ok );
	if ( ! ok )
		mFontSize = 10; // default size

	int tabWidth = settings->readNumEntry( "/qtcmd/TextFileView/TabWidth", -1 );
	if ( tabWidth <= 0 )
		tabWidth = 2; // default width
	if ( tabWidth > 8 )
		tabWidth = 8;
	mTabWidth = tabWidth;

	delete settings;

	setTabWidth( mTabWidth );
	mFontBold = (boldStr == "bold");
	mFontItalic = (italicStr == "italic");
	setFont( mFontFamily, mFontSize, mFontBold, mFontItalic );
}


void TextView::setKeyShortcuts( KeyShortcuts * keyShortcuts )
{
	if ( ! keyShortcuts )
		return;

	mKeyShortcuts = keyShortcuts;

	mKeyShortcuts->append( Find_TEXTVIEW, tr("Find"), Qt::CTRL+Qt::Key_F );
	mKeyShortcuts->append( FindNext_TEXTVIEW, tr("Find next"), Qt::Key_F3 );
	mKeyShortcuts->append( Replace_TEXTVIEW, tr("Replace"), Qt::CTRL+Qt::Key_R );
	mKeyShortcuts->append( GoToLine_TEXTVIEW, tr("Go to line"), Qt::CTRL+Qt::Key_G );
	mKeyShortcuts->append( Undo_TEXTVIEW, tr("Undo"), Qt::CTRL+Qt::Key_Z );
	mKeyShortcuts->append( Redo_TEXTVIEW, tr("Redo"), Qt::CTRL+Qt::Key_Y );
	mKeyShortcuts->append( Cut_TEXTVIEW, tr("Cut"), Qt::CTRL+Qt::Key_X );
	mKeyShortcuts->append( Copy_TEXTVIEW, tr("Copy"), Qt::CTRL+Qt::Key_C );
	mKeyShortcuts->append( Paste_TEXTVIEW, tr("Paste"), Qt::CTRL+Qt::Key_V );
	mKeyShortcuts->append( SelectAll_TEXTVIEW, tr("Select all"), Qt::CTRL+Qt::Key_A );
	mKeyShortcuts->append( Print_TEXTVIEW, tr("Print"), Qt::CTRL+Qt::Key_P );
	mKeyShortcuts->append( ZoomIn_TEXTVIEW,  tr("Zoom in"), Qt::CTRL+Qt::Key_Plus );
	mKeyShortcuts->append( ZoomOut_TEXTVIEW, tr("Zoom out"), Qt::CTRL+Qt::Key_Minus );
	mKeyShortcuts->append( TextCompletion_TEXTVIEW, tr("Text completion"), Qt::CTRL+Qt::Key_Space );

	mKeyShortcuts->updateEntryList( "/qtcmd/FileViewShortcuts/" ); // update from config file
	applyKeyShortcuts( mKeyShortcuts ); // for actions in current window
}


void TextView::initMenuAndToolBarActions()
{
	mViewMenuActions = new QActionGroup(this);
	mEditMenuActions = new QActionGroup(this);

	// --- actions for EDIT menu
	mUndoA     = new QAction(QPixmap(undo), tr("&Undo"), mTextEdit);
	mUndoA->setShortcut(Qt::CTRL + Qt::Key_Z);
	mRedoA     = new QAction(QPixmap(redo), tr("&Redo"), mTextEdit);
	mRedoA->setShortcut(Qt::CTRL + Qt::Key_Y);
	mCutA      = new QAction(QPixmap(cut), tr("C&ut"), mTextEdit);
	mCutA->setShortcut(Qt::CTRL + Qt::Key_X);
	mCopyA     = new QAction(QPixmap(copy), tr("&Copy"), mTextEdit);
	mCopyA->setShortcut(Qt::CTRL + Qt::Key_C);
	mPasteA    = new QAction(QPixmap(paste), tr("&Pase"), mTextEdit);
	mPasteA->setShortcut(Qt::CTRL + Qt::Key_V);
	mFindA     = new QAction(QPixmap(findfirst), tr("&Find"), mTextEdit);
	mFindA->setShortcut(Qt::CTRL + Qt::Key_F);
	mFindNextA = new QAction(QPixmap(findnext), tr("Find &next"), mTextEdit);
	mFindNextA->setShortcut(Qt::Key_F3);
	mReplaceA  = new QAction(QPixmap(replace), tr("&Replace"), mTextEdit);
	mReplaceA->setShortcut(Qt::CTRL + Qt::Key_R);
	mGotoLineA = new QAction(QPixmap(gotoline), tr("&Go to line"), mTextEdit);
	mGotoLineA->setShortcut(Qt::CTRL + Qt::Key_G);

	connect( mUndoA,  SIGNAL(triggered()),  mTextEdit, SLOT(undo()) );
	connect( mRedoA,  SIGNAL(triggered()),  mTextEdit, SLOT(redo()) );
	connect( mCutA,   SIGNAL(triggered()),  mTextEdit, SLOT(cut()) );
	connect( mCopyA,  SIGNAL(triggered()),  mTextEdit, SLOT(copy()) );
	connect( mPasteA, SIGNAL(triggered()),  mTextEdit, SLOT(paste()) );
	connect( mFindA,     SIGNAL(triggered()),  this, SLOT(slotFind()) );
	connect( mFindNextA, SIGNAL(triggered()),  this, SLOT(slotFindNext()) );
	connect( mReplaceA,  SIGNAL(triggered()),  this, SLOT(slotReplace()) );
	connect( mGotoLineA, SIGNAL(triggered()),  this, SLOT(slotGoToLine()) );

	mEditActions = new QActionGroup(this);
	mEditActions->add( mUndoA );
	mEditActions->add( mRedoA );
	mEditActions->add( mCutA );
	mEditActions->add( mCopyA );
	mEditActions->add( mPasteA );
	mEditActions->add( mReplaceA );
	mEditActions->add( mGotoLineA );

	mEditMenuActions->add( mFindA );
	mEditMenuActions->add( mFindNextA );
	foreach (QAction *action, mEditActions->actions())
		mEditMenuActions->addAction(action);

	// --- actions for VIEW menu

	// separator
	QAction *sep1 = new QAction(this);
	sep1->setSeparator(true);
	mViewMenuActions->addAction( sep1 );

	// End of line MENU & group
	mUnixEolA   = new QAction( tr("&Unix"), this );
	mUnixEolA->setCheckable(true);
	mWinDosEolA = new QAction( tr("&Windows and Dos"), this );
	mWinDosEolA->setCheckable(true);

	mEolActions = new QActionGroup(this);
	mEolActions->setExclusive(true);
	mEolActions->addAction(mUnixEolA);
	mEolActions->addAction(mWinDosEolA);

	mEolActions = new QActionGroup(this);
	mEolActions->setExclusive(true);
	QAction *eolActionsParentA = new QAction(tr("End of line"), this);
	QMenu *eolMenu = new QMenu(this);
	eolMenu->addAction(mUnixEolA);
	eolMenu->addAction(mWinDosEolA);
	eolActionsParentA->setMenu(eolMenu);

	mViewMenuActions->addAction(eolActionsParentA);

	// encoding MENU & group

	mAutoEncA       = new QAction( tr("Automatic"), this );
	mCeIso2EncA     = new QAction( tr("Central European")+" (ISO-8859-2)", this );
	mCeCpEncA       = new QAction( tr("Central European")+" (CP-1250)", this );
	QAction *encModeParentA = new QAction(tr("Set encoding"), this);
	QAction *manualSetParentA = new QAction(tr("Manually"), this);

	mAutoEncA->setCheckable(true);
	mCeIso2EncA->setCheckable(true);
	mCeCpEncA->setCheckable(true);

	QMenu *encModeMenu = new QMenu(this);
	encModeMenu->addAction(mAutoEncA);
	encModeMenu->addAction(manualSetParentA);

	mManualSetMenu = new QMenu(this);
	encModeParentA->setMenu(encModeMenu);
	// code page MENU & group
	mManualSetMenu->addAction(mCeIso2EncA);
	mManualSetMenu->addAction(mCeCpEncA);
	manualSetParentA->setMenu(mManualSetMenu);

	//FIXME: use config file
	mAutoEncA->setChecked(true);
	mCeIso2EncA->setChecked(true);
	slotAutoEncoding();

	mManuallyCodingActions = new QActionGroup(this);
	mManuallyCodingActions->setExclusive(true);
	mManuallyCodingActions->addAction(mCeIso2EncA);
	mManuallyCodingActions->addAction(mCeCpEncA);

	mViewMenuActions->addAction(encModeParentA);

	// - highlighting actions, MENU & group
	mNoneA = new QAction( tr("None"), this );
	mCCppA = new QAction( tr("C/C++"), this );
	mHtmlA = new QAction( tr("HTML/XML"), this );
	//mBashA = new QAction( tr("Bash"), this );
	//mPerlA = new QAction( tr("Perl"), this );

	mNoneA->setCheckable(true);
	mCCppA->setCheckable(true);
	mHtmlA->setCheckable(true);

	mSelectHighlightingActions = new QActionGroup(this);
	mSelectHighlightingActions->setExclusive(true);

	m_highlightingParentA = new QAction(tr("Highlighting &mode"), this);
	QAction *highlightingSourceParentA = new QAction(tr("Source"), this);
	QAction *highlightingMarkupParentA = new QAction(tr("Markup"), this);

	mSelectHighlightingActions = new QActionGroup(this);
	mSelectHighlightingActions->setExclusive(true);
	mSelectHighlightingActions->addAction(mNoneA);
	mSelectHighlightingActions->addAction(mCCppA);
	mSelectHighlightingActions->addAction(mHtmlA);

	QMenu *highlightingMenu = new QMenu(this);
	m_highlightingParentA->setMenu(highlightingMenu);
	highlightingMenu->addAction(mNoneA);
	highlightingMenu->addAction(highlightingSourceParentA);
	highlightingMenu->addAction(highlightingMarkupParentA);

	// highl SOURCE sub-MENU
	QMenu *sourceMenu = new QMenu(this);
	sourceMenu->addAction(mCCppA);
	highlightingSourceParentA->setMenu(sourceMenu);

	// highl Markup sub-MENU
	QMenu *markupMenu = new QMenu(this);
	markupMenu->addAction(mHtmlA);
	highlightingMarkupParentA->setMenu(markupMenu);


	mViewMenuActions->addAction(m_highlightingParentA);
	// separator
	QAction *sep2 = new QAction(this);
	sep2->setSeparator(true);
	mViewMenuActions->addAction( sep2 );

	// Print & zoom actions
	mPrintA = new QAction( QPixmap(print), tr("&Print"), mTextEdit );
	mPrintA->setShortcut(Qt::CTRL + Qt::Key_P);

	mZoomInA  = new QAction( QPixmap(zoomin), tr("Zoom &in"), mTextEdit );
	mZoomInA->setShortcut(Qt::CTRL + Qt::Key_Plus);
	mZoomOutA = new QAction( QPixmap(zoomout), tr("Zoom &out"), mTextEdit );
	mZoomOutA->setShortcut(Qt::CTRL + Qt::Key_Minus);

	QActionGroup *zoomActions = new QActionGroup(this);
	zoomActions->addAction(mZoomInA);
	zoomActions->addAction(mZoomOutA);

	mViewMenuActions->addAction( mPrintA );
	mViewMenuActions->addAction( mZoomInA );
	mViewMenuActions->addAction( mZoomOutA );


	connect( mPrintA,   SIGNAL(triggered()),  this, SLOT(slotPrint()) );
	connect( zoomActions,  SIGNAL(triggered(QAction*)),  this, SLOT(slotZoom(QAction*)) );

	connect( eolMenu,  SIGNAL(triggered(QAction*)),  this, SLOT(slotChangeEOL(QAction*)) );
	connect( mAutoEncA, SIGNAL(triggered()),  this, SLOT(slotAutoEncoding()) );
	connect( mManuallyCodingActions, SIGNAL(triggered(QAction *)),  this, SLOT(slotChangeEncoding(QAction *)) );
	connect( mSelectHighlightingActions,    SIGNAL(triggered(QAction *)),  this, SLOT(slotChangeHighlighting(QAction *)) );

	// --- update toolBars
	// read info from config file
	QAction *sep = new QAction(this);
	sep->setSeparator(true);

	mEditToolBarActions = new QActionGroup( this );
	mEditToolBarActions->addAction( sep );
	mEditToolBarActions->addAction( mFindA );
	mEditToolBarActions->addAction( mFindNextA );

	foreach (QAction *action, mEditActions->actions())
		mEditToolBarActions->addAction(action);

	sep = new QAction(this);
	sep->setSeparator(true);

	mViewToolBarActions = new QActionGroup( this );
	mViewToolBarActions->addAction( sep );
	mViewToolBarActions->addAction( mPrintA );
	mViewToolBarActions->addAction( mZoomInA );
	mViewToolBarActions->addAction( mZoomOutA );
}

/* --- z kate ---
  // encoding menu, start with auto checked !
  m_setEncoding = new KSelectAction(i18n("Set &Encoding"), 0, ac, "set_encoding");
  list = KGlobal::charsets()->descriptiveEncodingNames();
  list.prepend( i18n( "Auto" ) );
  m_setEncoding->setItems(list);
  m_setEncoding->setCurrentItem (0);
  connect(m_setEncoding, SIGNAL(activated(const QString&)), this, SLOT(slotSetEncoding(const QString&)));
*/

void TextView::setKindOfEoL( KindOfEOL kindOfEOL )
{
	if ( kindOfEOL == mKindOfEOL || kindOfEOL == UNKNOWNeol || kindOfEOL < UNKNOWNeol || kindOfEOL > WIN_DOSeol )
		return;

	bool unixEol = (kindOfEOL == UNIXeol);
	mWinDosEolA->setChecked( ! unixEol );
	mUnixEolA->setChecked( unixEol );
	mKindOfEOL = kindOfEOL;

    QTextDocument *doc = mTextEdit->document();
	if ( ! doc->isEmpty() ) { // replace current EOL
        mMainBuffer = mTextEdit->text();

		if ( kindOfEOL == UNIXeol )
			mMainBuffer.replace( "\x0d\x0a", "\x0a" );
		else // WIN_DOSeol
			mMainBuffer.replace( "\x0a", "\x0d\x0a" );

		mTextEdit->setText( mMainBuffer );
		updateStatus();
	}
}


void TextView::setFont( const QString & family, uint size, bool bold, bool italic )
{
	mTextEdit->setFont( QFont(family, size, (bold ? QFont::Bold : QFont::Normal), italic) );
}


void TextView::setTabWidth( uint amountOfChars )
{
	mTextEdit->setTabStopWidth( QFontMetrics(mTextEdit->currentFont()).width("a")*amountOfChars );
}


void TextView::updateContents( UpdateMode updateMode, ModeOfView modeOfView, const QString & fileName, QByteArray & binBuffer )
{
	qDebug("TextView::updateContents(), modeOfView=%d", modeOfView );
	QString ext;
	bool textViewIsEmpty = mTextEdit->document()->isEmpty();

	if ( updateMode == ALL || (updateMode == APPEND && textViewIsEmpty && mMainBuffer.length() <= mTxtBuffer.length()) ) {
		//setModeOfView( modeOfView ); // shows/hides actions
		mEditActions->setVisible( ((mModeOfView=modeOfView) == View::EDITmode) );
		ext = QFileInfo( fileName ).extension( FALSE ).lower();

    	if ( modeOfView != RENDERmode ) {
			mSyntaxHighlighter->setSyntaxHighlighting( (mAutoSH) ? SyntaxHighlighter::AUTO : mForceKindOfSH, ext );
            m_highlightingParentA->setVisible(true);
			updateHighlightingAction();
		}
		else // a mode 'RENDERmode' not need to the highlighting menu
            m_highlightingParentA->setVisible(false);

		// --- set the EOL info
		KindOfEOL kindOfEOL = UNIXeol;
		int p = binBuffer.find( QChar(0x0D) );
		if ( p != -1 )
			if ( binBuffer.find(QChar( 0x0A ), p) != -1 )
				kindOfEOL = WIN_DOSeol;
		setKindOfEoL( kindOfEOL );
	}

	if ( ! textViewIsEmpty && updateMode != APPEND )
		if ( mMainBuffer.length() <= mTxtBuffer.length() )
			mTxtBuffer = mTextEdit->text();
	if ( mTxtBuffer.isEmpty() && updateMode == ALL ) // only for first call this function
		mTxtBuffer = QString::fromLocal8Bit( binBuffer.data() );
	else
	if ( updateMode == APPEND ) {
		mTxtBuffer = QString::fromLocal8Bit( binBuffer.data() );
		if ( ! mPartTagBuf.isEmpty() ) {
			mTxtBuffer.insert(0, mPartTagBuf); mPartTagBuf = ""; // szybciej by dzialalo dolaczanie, czyli ponizsze polenie wczesniej, a tu dolaczanie do mMainBuffer
		}
	}

	if ( updateMode == ALL || (updateMode == APPEND && textViewIsEmpty) ) {
		mTextFormat = Qt::PlainText;
		if ( modeOfView == RENDERmode ) {
			if ( ext == "htm" || ext == "html" || ext == "shtml" || ext == "php" || ext == "php3" )
				mTextFormat = Qt::RichText;
		}
		else
		if ( modeOfView == RAWmode || modeOfView == EDITmode ) {
			//FIXME mTextEdit->setPaper( QBrush(Qt::white) ); // background always must have white color
			setTabWidth( mTabWidth );
		}
	}

	if ( updateMode == ALL || (updateMode == APPEND && textViewIsEmpty) ) {
		mTextEdit->setReadOnly( (modeOfView != EDITmode) );
		mTextEdit->setUndoRedoEnabled( FALSE );
		if ( mMainBuffer.length() <= mTxtBuffer.length() )
			mTextEdit->clear();
		if ( mTextFormat == Qt::RichText && updateMode == APPEND ) {
			mPartTagBuf = mTxtBuffer.right( mTxtBuffer.length()-mTxtBuffer.findRev('>')-1 );
			mTxtBuffer.remove( mTxtBuffer.length()-mPartTagBuf.length(), mPartTagBuf.length() );
		}
		if ( mTextEdit->document()->isEmpty() )
			mMainBuffer = mTxtBuffer;
		if ( mTextFormat == Qt::RichText ) {
			mTextEdit->setTextFormat( mTextFormat );
			if ( mMainBuffer.length() <= mTxtBuffer.length() )
				mTextEdit->setText( mTxtBuffer );
			else
				mTextEdit->setText( mMainBuffer ); // need to update all html, code becouse QTextEdit fixed incomplete code made it goes bad when it is append new code
			//mTextEdit->setText( mTxtBuffer );
		}
		else {
            qDebug("DEBUG: mMainBuffer.length() = %d  mTxtBuffer.length() = %d", mMainBuffer.length(), mTxtBuffer.length());
            Q_CHECK_PTR(mTextEdit);
			mTextEdit->setTextFormat( mTextFormat );

			if ( mMainBuffer.length() <= mTxtBuffer.length() )
				mTextEdit->setText( mTxtBuffer );
			else {
				mTextEdit->clear();
				mTextEdit->setText( mMainBuffer );
			}

		}
		mTextEdit->setUndoRedoEnabled( TRUE );
	}
	else
	if ( updateMode == APPEND ) {
		mTextEdit->setUndoRedoEnabled( FALSE );
		if ( mTextFormat == Qt::RichText ) {
			mPartTagBuf = mTxtBuffer.right( mTxtBuffer.length()-mTxtBuffer.findRev('>')-1 );
			mTxtBuffer.remove( mTxtBuffer.length()-mPartTagBuf.length(), mPartTagBuf.length() );
		}
		mMainBuffer += mTxtBuffer;
		mTextEdit->append( mTxtBuffer );
		mTextEdit->setModified( FALSE );
		mTextEdit->setUndoRedoEnabled( TRUE );
	}

	updateStatus();
}


void TextView::getData( QByteArray & binBuffer )
{
	binBuffer = mTextEdit->text().local8Bit();
}


void TextView::slotZoom(QAction * action)
{
	bool in = (action == mZoomInA);

	if (in)
		mTextEdit->zoomIn( 1 );
	else
		mTextEdit->zoomOut( 1 );

	qreal newFontSize = mTextEdit->currentFont().pointSizeF() + ((in) ? 1 : -1);
	//mTextEdit->zoomTo( newFontSize ); // not supported in Qt4 !
	mTextEdit->setFontPointSize( newFontSize );
	setTabWidth( mTabWidth );

	uint currentSize = mTextEdit->currentFont().pointSizeF();

	if (in) {
		if ( currentSize < 24 )
			mZoomOutA->setEnabled( TRUE );
		else
			mZoomInA->setEnabled( FALSE );
	}
	else {
		if ( currentSize > 8 )
			mZoomInA->setEnabled( TRUE );
		else
			mZoomOutA->setEnabled( FALSE );
	}
	mSyntaxHighlighter->setFontSize( newFontSize );

	QString message;
	message.sprintf( tr("Current font size")+" : %d", currentSize );
	emit signalUpdateStatus( message, 3000, FALSE ); // (dla RAW) niestety po tym nie jest zwracana poprzednia wart. (ta sprzed zmiany rozmiaru)

	/*
	const int range = 1;
	if (action == mZoomInA)
		mTextEdit->zoomIn( range );
	else
		mTextEdit->zoomOut( range );
	*/
/*  FIXME: does not work
    //uint currentSize = (uint) mTextEdit->fontPointSize();
	uint currentSize = (uint) mTextEdit->currentFont().pointSizeF();
    QString message;
    message.sprintf( tr("Current font size")+" : %d", currentSize );
    emit signalUpdateStatus( message, 3000, FALSE ); // (dla RAW) niestety po tym nie jest zwracana poprzednia wart. (ta sprzed zmiany rozmiaru)
*/
}


// --- FIXME uniezaleznic zapisywanie do pliku od systemu plikow
bool TextView::saveToFile( const QString & fileName )
{
	QFile file( fileName );
	if ( ! file.open(QIODevice::WriteOnly | QIODevice::Truncate) ) {
		MessageBox::critical( this,
		 "\""+fileName+"\""+"\n\n"+ tr("Cannot write to this file")
		);
		return FALSE;
	}
	mTextEdit->setModified( FALSE );

	QTextStream stream( &file );
	stream << mTextEdit->text();
	file.close();

	return TRUE;
}

void TextView::updateStatus()
{
	QString message;
	int weight = mTextEdit->toPlainText().length();

	if ( mModeOfView == RAWmode )
		message.sprintf( "%d "+tr("bytes"), weight );

	if ( mModeOfView == EDITmode )
	{
		QTextCursor cur = mTextEdit->textCursor();
		message.sprintf( tr("row")+": %d  "+tr("column")+": %d    "+tr("byte")+": %d/%d  %s",
				cur.blockNumber() + 1, cur.columnNumber() + 1, cur.position(), weight, mInsStatStr.toLatin1().data());
	}

	emit signalUpdateStatus( message );
}


void TextView::updateFindedTextList()
{
	bool notFound = TRUE;
	for( int i=0; i<mFindedTextList.count(); i++ )
		if ( mFindedTextList[i] == mFindedText ) {
			notFound = FALSE;
			break;
		}
	if ( notFound )
		mFindedTextList.append( mFindedText );
}


void TextView::getCurrentSelection( QString & selectionsStr )
{
	if ( ! mTextEdit->hasSelectedText() )
		return;

	int id = 0;
	selectionsStr = mTextEdit->selectedText();
	if ( mModeOfView == RENDERmode )
		selectionsStr.remove( 0, 20 ); // remove string <!--StartFragment-->

	if ( selectionsStr.length() > 16 )
		selectionsStr = selectionsStr.left( 16 );
	if ( (id=selectionsStr.find('\n')) != -1 )
		selectionsStr = selectionsStr.left( id-1 );
}


void TextView::setEncoding( const QString & encoding )
{
	QString enc = encoding.section( '(', 1,1 );
	if ( enc.isEmpty() )
		enc = "Automatic";
	else
		enc.remove( enc.length()-1, 1 ); // remove ')'

	qDebug("TextView::setEncoding, to '%s' (not yet implemented)", enc.toLatin1().data() );
}


void TextView::updateHighlightingAction()
{
	SyntaxHighlighter::SyntaxHighlighting sh = mSyntaxHighlighter->currentHighlighting();

//mNoneA, *mCCppA, *mHtmlA, *mBashA, *mPerlA;
	if ( sh == SyntaxHighlighter::NONE ) {
		mNoneA->setChecked(true);
	}
	else if ( sh == SyntaxHighlighter::CCpp ) {
		mCCppA->setChecked(true);
	}
	else if ( sh == SyntaxHighlighter::HTML ) {
		mHtmlA->setChecked(true);
	}
	/*else if ( sh == SyntaxHighlighter::BASH ) {
		mBashA->setChecked(true);
	}
	else if ( sh == SyntaxHighlighter::PERL ) {
		mPerlA->setChecked(true);
	}*/
}


void TextView::findNext()
{
	if ( mFindedText.isEmpty() ) {
		slotFind();
		return;
	}

	QTextDocument::FindFlags flags = 0;
	if (mCaseSensitive)
		flags |= QTextDocument::FindCaseSensitively;
	if (mWholeWordsOnly)
		flags |= QTextDocument::FindWholeWords;
	if ( ! mFindForward )
		flags |= QTextDocument::FindBackward;

	mFromBegin = ! mTextEdit->find(mFindedText, flags);

	mFindOnce = FALSE;
	if ( ! mFromBegin ) {
		mFindOnce = TRUE;
		return;
	}

	if ( ! mStartFindFromCursorPos || mBtnContinueHasPressed ) {
		MessageBox::information( this, tr("Find text")+" - QtCommander", mFindedText+"\n\n"+tr("Text not found") );
		return;
	}
	else {
		QString s0 = (mFindForward) ? tr("end")   : tr("start");
		QString s1 = (mFindForward) ? tr("begin") : tr("end");

		QStringList btnsNames;
		btnsNames << tr("Continue") << tr("Stop");
		int result = MessageBox::common( this,
		tr("Find text")+" - QtCommander",
		tr("Search reached")+" "+s0+" "+tr("of the document")+"\n\n"+tr("Continue from")+" "+s1+" ?",
		btnsNames, MessageBox::Yes, QMessageBox::Question
		);
		if ( result == MessageBox::Continue ) {
			mBtnContinueHasPressed = TRUE;
			mTextEdit->moveCursor( mFindForward ? QTextCursor::Start : QTextCursor::End );
			findNext();
		}
	}
}


void TextView::createListBox()
{
	mCompletionListBox = new QListWidget(mTextEdit);
	mCompletionListBox->setWindowFlags(mCompletionListBox->windowFlags() | Qt::Popup);
	mCompletionListBox->installEventFilter(this);
	connect(mCompletionListBox, SIGNAL(itemClicked(QListWidgetItem*)), this, SLOT(slotItemChosen(QListWidgetItem*)));
}

void TextView::adjustListBoxSize( int maxHeight, int maxWidth )
{
	if ( ! mCompletionListBox->count() )
		return;

	QFontMetrics fm(mCompletionListBox->font());
	int totalHeight = fm.height() * (mCompletionListBox->count() + 1);
	int height = qMin(totalHeight, maxHeight);
	if (mCompletionListBox->height() > height)
		mCompletionListBox->setFixedHeight(height);
	if (mCompletionListBox->width() > maxWidth)
		mCompletionListBox->setFixedWidth(maxWidth);
}


void TextView::applyKeyShortcuts( KeyShortcuts * keyShortcuts )
{
	if ( ! keyShortcuts )
		return;

	mKeyShortcuts = keyShortcuts;

	mFindA->setAccel( keyShortcuts->key(Find_TEXTVIEW) );
	mFindNextA->setAccel( keyShortcuts->key(FindNext_TEXTVIEW) );
	mReplaceA->setAccel( keyShortcuts->key(Replace_TEXTVIEW) );
	mGotoLineA->setAccel( keyShortcuts->key(GoToLine_TEXTVIEW) );
	mUndoA->setAccel( keyShortcuts->key(Undo_TEXTVIEW) );
	mRedoA->setAccel( keyShortcuts->key(Redo_TEXTVIEW) );
	mCutA->setAccel( keyShortcuts->key(Cut_TEXTVIEW) );
	mCopyA->setAccel( keyShortcuts->key(Copy_TEXTVIEW) );
	mPasteA->setAccel( keyShortcuts->key(Paste_TEXTVIEW) );
	mPrintA->setAccel( keyShortcuts->key(Print_TEXTVIEW) );
	mZoomInA->setAccel( keyShortcuts->key(ZoomIn_TEXTVIEW) );
	mZoomOutA->setAccel( keyShortcuts->key(ZoomOut_TEXTVIEW) );
}

		// ----------- SLOTs -----------

void TextView::slotChangeEOL( QAction * action )
{
	setKindOfEoL( (action == mUnixEolA) ? UNIXeol : WIN_DOSeol );
}


void TextView::slotAutoEncoding()
{
	bool checked = mAutoEncA->isChecked();
	mManualSetMenu->setEnabled( ! checked );
	if ( checked )
		setEncoding("Automatic");
	else {
		QAction *action = mManuallyCodingActions->checkedAction();
		Q_CHECK_PTR(action);
		setEncoding( action->text() );
	}
}

void TextView::slotChangeEncoding( QAction * action )
{
/*    if (action == mCeIso2EncA) {
    }
    else if (action == mCeCpEncA) {
    }*/
	setEncoding( action->text() );
}


void TextView::slotChangeHighlighting( QAction * action )
{
	if ( action == mNoneA )
		mForceKindOfSH = SyntaxHighlighter::NONE;
	else
	if ( action == mCCppA )
		mForceKindOfSH = SyntaxHighlighter::CCpp;
	else
	if ( action == mHtmlA )
		mForceKindOfSH = SyntaxHighlighter::HTML;
	else
	if ( action == mBashA )
		mForceKindOfSH = SyntaxHighlighter::BASH;
	else
	if ( action == mPerlA )
		mForceKindOfSH = SyntaxHighlighter::PERL;

	mSyntaxHighlighter->setSyntaxHighlighting( mForceKindOfSH );
	mSyntaxHighlighter->rehighlight();
}


void TextView::slotApplyTextViewSettings( const TextViewSettings & tvs )
{
	mFontFamily = tvs.fontFamily;
	mFontSize   = tvs.fontSize;
	mFontBold   = tvs.fontBold;
	mFontItalic = tvs.fontItalic;
	mTabWidth   = tvs.tabWidth;

	setTabWidth( mTabWidth );
	setFont( mFontFamily, mFontSize, mFontBold, mFontItalic );

	// do'nt change highlighting for different than current kind of highlighting
	if ( tvs.currentSyntaxHighlighting != mSyntaxHighlighter->currentHighlighting() )
		return;

	for ( int i=0; i<11; i++ )
		mSyntaxHighlighter->initAttributsTable( i, tvs.shAttrib[i].color, tvs.shAttrib[i].bold, tvs.shAttrib[i].italic );

	mSyntaxHighlighter->rehighlight();
}


void TextView::slotSaveSettings()
{
	QSettings *settings = new QSettings;
	QString font = mTextEdit->family()+","+QString::number(mTextEdit->pointSize());

	settings->writeEntry( "/qtcmd/TextFileView/Font", font );
	settings->writeEntry( "/qtcmd/TextFileView/TabWidth", mTabWidth );

	delete settings;
}


void TextView::slotGoToLine()
{
	bool ok;
	QString newLineNumStr = QInputDialog::getItem(
		tr("Go to line")+" - QtCommander", tr("Enter new line number:"),
		mLineNumbersList, 0, TRUE, &ok, this );

	if ( ! ok )
		return;

	uint newLine = newLineNumStr.toUInt( &ok );
	const uint maxLines = mTextEdit->document()->blockCount();
	if ( ok ) {
		bool notFound = TRUE;
		if ( newLine == 0 )
			newLine = 1;
		else
		if ( newLine > maxLines )
			newLine = maxLines;
		newLineNumStr = QString::number(newLine); // need to add (below) correct str.

		for( int i = 0; i < mLineNumbersList.count(); ++i ) {
			if ( mLineNumbersList[i] == newLineNumStr ) {
				notFound = FALSE;
				break;
			}
		}
		if ( notFound )
			mLineNumbersList.append( newLineNumStr );

		QTextCursor cur = mTextEdit->textCursor();
		int lines = newLine - cur.blockNumber() - 1;
		if ( lines > 0 )
			cur.movePosition(QTextCursor::Down, QTextCursor::MoveAnchor, lines);
		else
			cur.movePosition(QTextCursor::Up, QTextCursor::MoveAnchor, -lines);

		mTextEdit->setTextCursor(cur);
	}
}


void TextView::slotFind()
{
	getCurrentSelection( mFindedText );

	bool doFind = FindTextDialog::showDialog( this,
		mFindedTextList, mFindedText, mReplacedTextList, mReplacedText,
		mCaseSensitive, mWholeWordsOnly, mStartFindFromCursorPos, mFindForward, mPromptOnReplace );

	if ( ! doFind )
		return;

	updateFindedTextList();

	if ( ! mStartFindFromCursorPos )
		mTextEdit->moveCursor(QTextCursor::Start);

	mBtnContinueHasPressed = false;
	findNext();

	updateFindedTextList();
}


void TextView::slotFindNext()
{
	mBtnContinueHasPressed = FALSE;
	findNext();
}


void TextView::slotReplace()
{
	// if something has selected then var.'mFindedText' will contains this string
	getCurrentSelection( mFindedText );

	// --- show replacement dialog
	bool doFind = FindTextDialog::showDialog( this,
		mFindedTextList, mFindedText, mReplacedTextList, mReplacedText,
		mCaseSensitive, mWholeWordsOnly, mStartFindFromCursorPos, mFindForward, mPromptOnReplace, TRUE );

	if ( ! doFind )
		return;

	updateFindedTextList();
	// --- update replaced text list
	bool notFound = TRUE;
	for( int i=0; i<mReplacedTextList.count(); i++ )
		if ( mReplacedTextList[i] == mReplacedText ) {
			notFound = FALSE;
			break;
		}
	if ( notFound )
		mReplacedTextList.append( mReplacedText );

	// --- do replcement
	mBtnContinueHasPressed = FALSE;
	enum { BREAK=0, YES, NO, ALL, CLOSE };
	uint replacedCount = 0, result = YES;
	while ( 1 ) {
		findNext();
		if ( mFindOnce ) {
			if ( mPromptOnReplace && result != ALL ) {
				QStringList btnsNames;
				btnsNames << tr("&Yes") << tr("&No") << tr("&All") << tr("&Cancel");
				result = MessageBox::common( this, tr("Replace text")+" - QtCommander",
					tr("Replace this occurence ?"), btnsNames );
			}
			else
				result = ALL;

			if ( result == CLOSE || result == BREAK ) // BREAK - key Esc pressed
				break;
			else
			if ( result == YES || result == ALL ) {
// 				mTextEdit->insert( mReplacedText, (uint)QTextEdit::RemoveSelected );
				replacedCount++;
			}
		}
		else {
			MessageBox::information( this, tr("Replace text")+" - QtCommander",
				QString("%1 ").arg(replacedCount)+tr("replaced made") );
			break;
		}
	}
}


void TextView::slotPrint()
{
	QPrinter printer(QPrinter::HighResolution);
	printer.setFullPage(true);
	QPrintDialog *dlg = new QPrintDialog(&printer, this);
	if (mTextEdit->textCursor().hasSelection())
		dlg->addEnabledOption(QAbstractPrintDialog::PrintSelection);
	dlg->setWindowTitle(tr("Print Document"));
	if (dlg->exec() == QDialog::Accepted) {
		emit signalUpdateStatus( tr("Printing...") );
		mTextEdit->print(&printer);
		emit signalUpdateStatus( tr("Printing completed"), 2000 );
	} else {
		emit signalUpdateStatus( tr("Printing aborted"), 2000 );
	}
	delete dlg;
}


void TextView::slotCopyLinkToTheClipboard()
{ // All code of this function is come from Qt-3.1.1 source ( QLineEdit::copy() )
	disconnect( QApplication::clipboard(), SIGNAL(selectionChanged()), this, 0);
	QApplication::clipboard()->setText( mCurrentLink, QClipboard::Clipboard );
	connect( QApplication::clipboard(), SIGNAL(selectionChanged()), this, SLOT(clipboardChanged()) );
}


void TextView::slotCursorPositionChanged()
{
	if ( mModeOfView == EDITmode )
		updateStatus();
}


void TextView::slotCompleteText()
{
	QTextCursor cur = mTextEdit->textCursor();
	Q_ASSERT(cur.block().isValid());

	int cursorPos = cur.position();
	cur.movePosition(QTextCursor::StartOfWord);
	int wordStart = cur.position();

	QString content = mTextEdit->text();
	mWordPrefix = content.mid( wordStart, cursorPos - wordStart );
	if ( mWordPrefix.isEmpty() )
		return;

	QStringList list = QStringList::split( QRegExp("\\W+"), content );
	QMap<QString, QString> map;
	QStringList::Iterator it = list.begin();
	while ( it != list.end() ) {
		if ( (*it).startsWith(mWordPrefix) && (*it).length() > mWordPrefix.length() )
			map[(*it).lower()] = *it;
		++it;
	}

	if ( map.count() == 1 ) {
		mTextEdit->insert( (*map.begin()).mid(mWordPrefix.length()) );
	}
	else
	if ( map.count() > 1 ) {
		if ( !mCompletionListBox )
			createListBox();
		mCompletionListBox->clear();
		mCompletionListBox->insertItems(0, map.values());
		QPoint point = mapToGlobal(mTextEdit->cursorRect().topLeft());
		adjustListBoxSize( qApp->desktop()->height() - point.y(), width() / 2 );
		mCompletionListBox->setCurrentRow(0);
		mCompletionListBox->move( point );
		mCompletionListBox->show();
		mCompletionListBox->setFocus();
	}
}

void TextView::slotItemChosen(QListWidgetItem *item)
{
	if ( item )
		mTextEdit->insert( item->text().mid(mWordPrefix.length()) );

	mCompletionListBox->close();
	mParent->setActiveWindow();
	mTextEdit->setFocus();
}


		// ----------- EVENTs -----------

bool TextView::keyPressed( QKeyEvent *e )
{
	if ( ! mKeyShortcuts )
		return FALSE;

	int key   = e->key();
	int state = e->state();
	int ks    = mKeyShortcuts->ke2ks( e );

	if ( key == Qt::Key_Insert ) {
		if ( state == Qt::ControlModifier ) {
			mTextEdit->copy();
			return FALSE; // let's eat event
		}
		else
		if ( state == Qt::AltModifier )
			return FALSE; // let's eat event
		else
		if ( state == Qt::ShiftModifier )
			return TRUE;

		mTextEdit->setOverwriteMode( mOvrMode );
		mOvrMode = ! mOvrMode;
		mInsStatStr = (mOvrMode) ? "OVR" : "INS";
		updateStatus();
	}
	else
	if ( ks == mKeyShortcuts->key(SelectAll_TEXTVIEW) ) {
		mTextEdit->selectAll();
		return FALSE; // let's eat event
	}
	else
	if ( ks == mKeyShortcuts->key(TextCompletion_TEXTVIEW) ) {
		slotCompleteText();
		return FALSE; // let's eat event
	}
// for test only
    else if (key == Qt::Key_F12) {
        slotCompleteText();
        return false;
    }
// for test only

	return TRUE;
}


void TextView::mousePressEvent( QMouseEvent *e )
{
    if ( e->button() == Qt::RightButton )  {
        //int selectAllKCode  = ( mKeyShortcuts ) ? mKeyShortcuts->key(SelectAll_TEXTVIEW)  : Qt::CTRL+Qt::Key_A;
        QString findKCodeStr    = ( mKeyShortcuts ) ? mKeyShortcuts->keyStr(Find_TEXTVIEW)      : "CTRL+F";
        QString replaceKCodeStr = ( mKeyShortcuts ) ? mKeyShortcuts->keyStr(Replace_TEXTVIEW)   : "CTRL+R";

        QMenu *ctxMenu = mTextEdit->createStandardContextMenu();
        Q_CHECK_PTR(ctxMenu);

        bool updateList = false;
        QString selections;

        if ( ! mCurrentLink.isEmpty() ) {
            ctxMenu->addSeparator();
            ctxMenu->addAction(tr("Copy &link location"), this, SLOT(slotCopyLinkToTheClipboard()));
            updateList = true;
        }

        if (mTextEdit->hasSelectedText()) {
            getCurrentSelection( selections );
            ctxMenu->addSeparator();
            ctxMenu->addAction(tr("&Find")+": "+selections+"\t"+findKCodeStr, this, SLOT(slotFind()));

            if ( mModeOfView == EDITmode )
                ctxMenu->addAction(tr("&Replace")+": "+selections+"\t"+replaceKCodeStr, this, SLOT(slotReplace()));

            updateList = true;
        }

        ctxMenu->exec(mTextEdit->mapToGlobal(QPoint(e->x(), e->y()+2)));

        if (updateList) {
            mFindedText = selections;
            updateFindedTextList();
        }

        delete ctxMenu;
    }
}


void TextView::mouseMoveEvent( QMouseEvent *e )
{
	if ( mModeOfView == RENDERmode ) {
		int xPos = e->pos().x();
		int yPos = e->pos().y();
		int width  = mTextEdit->width() - (mTextEdit->verticalScrollBar()->isVisible() ? mTextEdit->verticalScrollBar()->width()+3 : 0);
		int height = mTextEdit->height() - (mTextEdit->horizontalScrollBar()->isVisible() ? mTextEdit->horizontalScrollBar()->height()+3 : 2);

		if ( yPos < 0 || xPos < 0 || xPos > width || yPos > height ) {
			QApplication::restoreOverrideCursor();
			return;
		}

		QApplication::restoreOverrideCursor();
		if ( ! mCurrentLink.isEmpty() )
			QApplication::setOverrideCursor( Qt::PointingHandCursor );

		emit signalUpdateStatus( mCurrentLink );
	}
}


void TextView::showEvent(QShowEvent * /*event*/)
{
	mTextEdit->setFocus();
}

bool TextView::eventFilter( QObject *o, QEvent *e )
{
	if ( o == mTextEdit ) {
		if ( e->type() == QEvent::KeyPress ) {
			if ( ! keyPressed( (QKeyEvent *)e ) )
				return TRUE; // eat current event
		}
		else
		if ( e->type() == QEvent::FocusOut )
			QApplication::restoreOverrideCursor();
	}
	else
	if ( o == mTextEdit->viewport() ) {
		if ( e->type() == QEvent::MouseButtonPress )
			mousePressEvent( (QMouseEvent *)e );
		else
		if ( e->type() == QEvent::MouseMove )
			mouseMoveEvent( (QMouseEvent *)e );
	}
	else
	if (o == mCompletionListBox && e->type() == QEvent::KeyPress) {
		QKeyEvent *keyEvent = static_cast<QKeyEvent*>(e);
		int key = keyEvent->key();
		switch (key) {
		case Qt::Key_Enter:
		case Qt::Key_Return:
			slotItemChosen(mCompletionListBox->currentItem());
			return true;
		case Qt::Key_Escape:
			slotItemChosen();
			return true;
		}
	}

	return QWidget::eventFilter( o, e );    // standard event processing
}
