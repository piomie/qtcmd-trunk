/***************************************************************************
                          textviewplugin.cpp  -  description
                             -------------------
    begin                : tue nov 4 2003
    copyright            : (C) 2003 by Piotr Mierzwiński
    email                : peterm@go2.pl

    copyright            : See COPYING file that comes with this project
 ***************************************************************************/

#include "textview.h"

extern "C" {
	QWidget* create_plugin( QWidget * parent )
	{
		return new TextView( parent );
	}
}
