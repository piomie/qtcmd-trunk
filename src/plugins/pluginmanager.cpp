/***************************************************************************
                          pluginmanager  -  description
                             -------------------
    begin                : So gru 8 2007
    copyright            : (C) 2007 by Mariusz Borowski
    email                : mariusz@nes.pl

    copyright            : See COPYING file that comes with this project

 $Id$
 ***************************************************************************/

#include "view.h"
#include "vfs.h"
#include "pluginmanager.h"

#include <QFile>
#include <QWidget>

PluginManager::PluginManager()
{
}

PluginManager::~PluginManager()
{
}

PluginManager* PluginManager::instance()
{
    static PluginManager instance;
    return &instance;
}

QString PluginManager::errorMessage() const
{
    return m_errorMsg;
}

void PluginManager::addPath(const QString & path)
{
    Q_ASSERT( ! path.isEmpty() );
    int len = path.length();
    if (len > 1 && path.endsWith('/'))
        m_searchPaths.append(path.left(len - 1));
    else
        m_searchPaths.append(path);
}

View* PluginManager::createViewPlugin(const QString& name, QWidget *parent)
{
    View* view = qobject_cast<View*>(createWidgetPlugin(name, parent));
    return view;
}

VFS* PluginManager::createVfsPlugin(const QString& name, QWidget *parent)
{
    VFS* vfs = qobject_cast<VFS*>(createWidgetPlugin(name, parent));
    return vfs;
}

QWidget* PluginManager::createWidgetPlugin(const QString& name, QWidget *parent)
{
    Q_ASSERT( ! name.isEmpty() );
    Q_ASSERT(m_searchPaths.count());

    QString filePath;
    for (QStringList::const_iterator it = m_searchPaths.begin(); it != m_searchPaths.end(); ++it)
    {
        QString path = *it + "/lib" + name + ".so"; //TODO: remove .so
        if (QFile::exists(path)) {
            filePath = path;
            break;
        }
    }

    if (filePath.isEmpty()) {
        m_errorMsg = QObject::tr("Could not find a plugin file (lib%1)! Search path: %2").arg(name).arg(m_searchPaths.join(":"));
        return 0;
    }

    typedef QWidget* (*t_func)(QWidget*);
    QLibrary lib(filePath);
    t_func createWidget = (t_func) lib.resolve("create_plugin");
    if ( ! createWidget ) {
        m_errorMsg = QObject::tr("Could not resolve a 'create_plugin' symbol (%1)!").arg(lib.errorString());
        return 0;
    }
    QWidget* widget = createWidget(parent);
    if ( ! widget )
        m_errorMsg = QObject::tr("Could not create the widget plugin (plugin name: %1)").arg(name);

    return widget;
}
