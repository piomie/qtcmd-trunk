/***************************************************************************
                          pluginmanager  -  description
                             -------------------
    begin                : So gru 8 2007
    copyright            : (c) 2007 by Mariusz Borowski
    email                : b0mar@nes.pl

    copyright            : See COPYING file that comes with this project

 $Id$
 ***************************************************************************/

#ifndef PLUGINMANAGER_H
#define PLUGINMANAGER_H

#include <QStringList>
#include <QLibrary>

class View;
class VFS;
class QWidget;


/**
    How to use:
    PluginManager *pm = PluginManager::instance();
    pm->addPath("/path/to/plugins");
    pm->addPath("/another/path/to/plugins");

    View *view = pm->createViewPlugin(parentWidget);
    if ( ! view )
        qDebug(qPrintable(pm->errorMessage()))

	@author Mariusz Borowski <b0mar@nes.pl>
*/
class PluginManager
{
public:
    ~PluginManager();

    static PluginManager* instance();

    QString errorMessage() const;

    void addPath(const QString& path);

    View* createViewPlugin(const QString& name, QWidget* parent);
    VFS* createVfsPlugin(const QString& name, QWidget* parent);
    QWidget* createWidgetPlugin(const QString& name, QWidget* parent);

private:
    PluginManager();

private:
    QString         m_errorMsg;
    QStringList     m_searchPaths;
};

#endif
