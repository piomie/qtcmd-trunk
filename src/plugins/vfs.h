/***************************************************************************
                          vfs.h  -  description
                             -------------------
    begin                : sun sep 12 2004
    copyright            : (C) 2004 by Piotr Mierzwi�ski
    email                : peterm@go2.pl

    copyright            : See COPYING file that comes with this project
 ***************************************************************************/

#ifndef _VFS_H_
#define _VFS_H_

#include <qiodevice.h>
#include <qwidget.h>

#include "url.h"
#include "urlinfoext.h"
#include "findcriterion.h"


class VFS : public QWidget
{
	Q_OBJECT
public:
	enum Operation   { NoneOp = 0, HostLookup, Connect, List, Open, Cd, Rename, Remove, Touch, MakeDir, MakeLink, Get, Put, Copy, Move, Weigh, SetAttributs, Find };
	enum Error       { NoError= 0, Unknown, NotExists, AlreadyExists, ReadError, CannotRead, CannotWrite, CannotRemove, CannotSetAttributs, CannotCreateLink, IllegalName, NoFreeSpace, BreakOperation, OpNotPermitted, CannotRunProc };
	enum KindOfError { Exists = 0, Readable, Writable };
	enum DirWritable { UNKNOWNdw = 0, ENABLEdw, DISABLEdw };
	enum KindOfFilesSystem { ARCHIVEfs = 0, FTPfs, LOCALfs, ISO9660, NETWORKfs, SAMBAfs, UNKNOWNfs };

	VFS( QWidget * , const char * ) {}
	virtual ~VFS() {}

	// ------ INFO ABOUT OPERATIONS -----

	virtual int port() const { return -1; }
	virtual Error errorCode() const { return NoError; }
	virtual QString host() const { return QString::null; }
	virtual QString path() const { return QString::null; }
	virtual QString nameOfProcessedFile() const { return QString::null; }
	virtual bool isReadable( const QString & , bool , bool ) { return FALSE; }
	virtual Operation operation() const { return NoneOp; }
	virtual DirWritable dirWritable( const QString & ) const { return UNKNOWNdw; };
	virtual void getWeighingStat( long long & , uint & , uint & ) {}
	virtual void getNewUrlInfo( UrlInfoExt & ) {}
	virtual bool operationHasFinished() const { return FALSE; }

	// ------ FILE OPERATIONS -----

	virtual void cd( const QString & ) {}
	virtual void readDir( const URL & ) {}
	virtual void mkDir( const QString & )  {}
	virtual void mkFile( const QString & ) {}
	virtual void removeFiles( const QStringList & ) {}
	virtual void weighFiles( const QStringList & , bool ) {}
	virtual void mkLinks( const QStringList & , const QStringList & , bool, bool ) {}
	virtual void editLink( const QString & , const QString & ) {}
	virtual void setAttrib( const QStringList & , const UrlInfoExt & , bool , int ) {}
	virtual void find( const FindCriterion & , bool ) {}
	virtual void get( const QString & , QIODevice * ) {}
	virtual void put( QIODevice * , const QString & ) {}
	virtual void putFile( const QByteArray & , const QString & ) {}
	virtual void copyFiles( QString & , const QStringList & , bool , bool , bool , bool ) {}


	virtual void breakOperation() {}

public slots:
	virtual void slotPause( bool ) {}
	virtual void slotGetUrlInfo( const QString & , UrlInfoExt & ) {}
	virtual void slotReadFileToView( const QString & , QByteArray & , int & , int ) {}

signals:
	void signalResultOperation( VFS::Operation operation, bool noError );
	void signalShowFileOverwriteDlg( const QString &, QString &, int & );
	void signalShowDirNotEmptyDlg( const QString &, int & );

	void signalReadyRead();
	void signalPrepareForListing();
	void signalListInfo( const UrlInfoExt & );
	void signalInsertMatchedItem( const UrlInfoExt & );
	void signalUpdateFindStatus( uint, uint, uint );
	void signalRemoveFileNameFromSIL( const QString & );

	// ------ UPDATE PROGRESS OF OPERATION -----
	void signalFileCounterProgress( int, bool, bool );
	void signalNameOfProcessedFiles( const QString &, const QString & );
	void signalTotalProgress( int, long long );
	void signalDataTransferProgress( long long, long long, uint );
	void signalStartProgressDlgTimer( bool );
	void signalSetInfo( const QString & );

	void signalPutFile( const QByteArray & , const QString & );

};

#endif
