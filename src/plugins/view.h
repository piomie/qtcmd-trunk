/***************************************************************************
                          view.h  -  description
                             -------------------
    begin                : sun apr 27 2003
    copyright            : (C) 2003 by Piotr Mierzwiński
    email                : peterm@go2.pl

    Copyright: See COPYING file that comes with this project
 ***************************************************************************/

#ifndef _VIEW_H_
#define _VIEW_H_

#include "../../../enums.h"

#include "../../../libs/qtcmdutils/keyshortcuts.h"
#include "view/settings/textviewsettings.h"
// #include "../settings/imageviewsettings.h"
// #include "../settings/soundviewsettings.h"
// #include "../settings/videoviewsettings.h"
// #include "../settings/binaryviewsettings.h"

#include <QWidget>
#include <QActionGroup>

class QMenu;
class QToolBar;


class View : public QWidget
{
	Q_OBJECT
public:
	enum KindOfView { TEXTview=0, IMAGEview, SOUNDview, VIDEOview, BINARYview, UNKNOWNview };
	enum ModeOfView { RAWmode=0, RENDERmode, HEXmode, EDITmode, UNKNOWNmode };
	enum UpdateMode { ALL=0, APPEND, ON_DEMAND, NONE };

	View( QWidget* ) {}
	virtual ~View() {}

	virtual void updateStatus() {}
	virtual void setKeyShortcuts( KeyShortcuts * ) {}
	virtual void updateContents( UpdateMode , ModeOfView, const QString &, QByteArray & ) {}
	virtual void getData( QByteArray & ) {}
	virtual void clear() {}

	virtual ModeOfView modeOfView() { return UNKNOWNmode; }

	virtual void setGeometry( int , int , int , int ) {}

	virtual bool saveToFile( const QString & ) { return FALSE; }
	virtual bool isModified() const { return FALSE; }

	virtual QActionGroup *editMenuBarActions() const { return 0; }
	virtual QActionGroup *viewMenuBarActions() const { return 0; }
	virtual QActionGroup *editToolBarActions() const { return 0; }
	virtual QActionGroup *viewToolBarActions() const { return 0; }

public slots:
	virtual void slotSaveSettings()  {}

	// specific for TextView
	virtual void slotApplyTextViewSettings( const TextViewSettings & ) {}

	// specific for ImageView
// 	virtual void slotApplyImageViewSettings( const ImageViewSettings & ) {}

	// specific for SoundView
// 	virtual void slotApplySoundViewSettings( const SoundViewSettings & ) {}

	// specific for VideoView
// 	virtual void slotApplyVideoViewSettings( const VideoViewSettings & ) {}

	// specific for BinaryView
// 	virtual void slotApplyBinaryViewSettings( const BinaryViewSettings & ) {}

signals:
	void signalUpdateStatus( const QString & , uint time=0, bool showPrevious=TRUE );
	void signalClose();

};

#endif
