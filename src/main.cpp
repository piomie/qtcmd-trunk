/***************************************************************************
                         qtcmd.cpp  -  description
                            -------------------
   begin                : Wed Oct 23 2002
   copyright            : (C) 2002, 2003 by Piotr Mierzwiński
   email                : peterm@go2.pl

   copyright            : See COPYING file that comes with this project

 $Id$
***************************************************************************/

#include <qapplication.h>
#include <qtranslator.h>
#include <qfile.h>
#include <qobject.h>

#include <unistd.h>
#include <getopt.h>
#include <iostream>

#include "qtcmd.h"
#include "config.h"
#include "pluginmanager.h"


int main(int argc, char **argv)
{
	QApplication a(argc, argv);
	QString translationFilePath = Config::instance()->translationsDirPath();
	qDebug("translationFilePath= %s", translationFilePath.toLatin1().data());
	// init and sets translations for application
	QTranslator torApp;
	if (torApp.load( QString("qtcmd"), translationFilePath))
		a.installTranslator(&torApp);
	// init and sets translations for application settings (plugin)
	QTranslator torAppSetts;
	if (torAppSetts.load(QString("qtcmd-setts"), translationFilePath))
		a.installTranslator(&torAppSetts);
	// init and sets translations for view settings (plugin)
	QTranslator torViewSetts;
	if (torViewSetts.load(QString("qtcmd-view-setts"), translationFilePath))
		a.installTranslator(&torViewSetts);
	// init and sets translations for text view (plugin)
	QTranslator torTextView;
	if (torTextView.load(QString("qtcmd-view-text"), translationFilePath))
		a.installTranslator(&torTextView);
	// init and sets translations for library "uiext"
	QTranslator torLibUiExt;
	if (torLibUiExt.load(QString("qtcmd-uiext"), translationFilePath))
		a.installTranslator(&torLibUiExt);
	// init and sets translations for library "utils"
	QTranslator torLibUtils;
	if ( torLibUtils.load(QString("qtcmd-utils"), translationFilePath))
		a.installTranslator(&torLibUtils);
	// init and sets translations for library "dlgext"
	QTranslator torLibDlgExt;
	if (torLibDlgExt.load(QString("qtcmd-dlgext"), translationFilePath))
		a.installTranslator(&torLibDlgExt);

    PluginManager* pm = PluginManager::instance();
    pm->addPath(Config::instance()->applicationRootDirPath() + "/lib/qtcmd/plugins");

	//parse cmd line args
	option options[] = {
		{"leftURL",  1, 0, 'l'},
		{"rightURL", 1, 0, 'r'},
		{0, 0, 0, 0}
	};

	QString sLeftURL, sRightURL;
	int optinx = 0;
	opterr = 0;
	char opt;
	while ((opt = getopt_long(a.argc(), a.argv(), "l:r:", options, &optinx)) != -1) {
		switch (opt) {
		case 'l': sLeftURL  = QString(optarg); break;
		case 'r': sRightURL = QString(optarg); break;
		default:
			std::cerr << (QObject::tr("Usage:  qtcmd [options]") + QString("\n")).toStdString();
			std::cerr << (QObject::tr("Options:") + QString("\n")).toStdString();
			std::cerr << (QString("\t-l, --leftURL=\t - ") + QObject::tr("URL to init left panel") + QString("\n")).toStdString();
			std::cerr << (QString("\t-r, --rightURL=\t - ") + QObject::tr("URL to init right panel") + QString("\n")).toStdString();
			return 1;
		}
	}

	QtCmd *pQtCmd = new QtCmd(sLeftURL, sRightURL, NULL, "QtCmd", Qt::WType_TopLevel);
	pQtCmd->setCaption( "QtCommander" );
	pQtCmd->show();

	a.connect(&a, SIGNAL(lastWindowClosed()), &a, SLOT(quit()));
	int ret = a.exec();

	delete pQtCmd;

	return ret;
}
