/***************************************************************************
                          fsmanager.cpp  -  description
                             -------------------
    begin                : Sat Nov 30 2002
    copyright            : (C) 2002, 2003,2004 by Piotr Mierzwiński
    email                : peterm@go2.pl

    copyright            : See COPYING file that comes with this project

 $Id$
 ***************************************************************************/

#include "inputdlg.h"
#include "fileview.h"
#include "fsmanager.h"
#include "messagebox.h"
#include "systeminfo.h" // for getting user and sGroup
#include "urlinfoext.h"
#include "progressdlg.h"
#include "findfiledlg.h"
#include "fileinfoext.h"
#include "propertiesdlg.h"
#include "findcriterion.h"
#include "fileoverwritedlg.h"
#include "filesassociation.h"

#include <qregexp.h>
#include <qlibrary.h>
#include <qsettings.h>
#include <qinputdialog.h>
#include <qapplication.h>
//Added by qt3to4:
#include <Q3ValueList>

#include "pluginmanager.h"


FSManager::FSManager( const QString & sInputURL, ListView *pListView )
	: m_pListView(pListView), m_pVFS(NULL),
	  m_pFindFileDlg(NULL), m_pProgressDlg(NULL), m_pPropertiesDlg(NULL), m_pFilesAssociation(NULL)
{
	// --- init FS info
	m_eCurrentFilesSystem = kindOfFilesSystem( sInputURL );
	m_sCurrentURL = sInputURL;
	parsePath( m_sCurrentURL );
	//setName( QString("FSManager_")+pListView->name());
	m_bFoundFilesSys = (sInputURL.find("ffs://") == 0);

	QSettings *pSettings = new QSettings;
	s_bCloseProgressDlgAfterFinished = pSettings->readBoolEntry( "/qtcmd/FilesSystem/CloseProgressDlgAfterFinished", FALSE );
	m_bAlwaysOverwrite = pSettings->readBoolEntry( "/qtcmd/FilesSystem/AlwaysOverwrite", FALSE );
	m_bSavePermision = pSettings->readBoolEntry( "/qtcmd/FilesSystem/AlwaysSavePermission", TRUE );
	m_bAskBeforeDelete = pSettings->readBoolEntry( "/qtcmd/FilesSystem/AskBeforeDelete", TRUE );
	m_bGetInToDir = pSettings->readBoolEntry( "/qtcmd/FilesSystem/AlwaysGetInToDir", FALSE );
	m_bHardLink = FALSE;
	delete pSettings;

	qDebug("FSManager::FSManager(), init m_eCurrentFilesSystem=%d, sInputURL=%s", m_eCurrentFilesSystem, sInputURL.toLatin1().data() );
}


FSManager::~FSManager()
{
	delete m_pProgressDlg;
	delete m_pFindFileDlg;
	delete m_pPropertiesDlg;

	removeProgressDlg();
	removeVFS();
}


bool FSManager::createVFS( VFS::KindOfFilesSystem eKindOfFS, const QString & sInputURL )
{
	qDebug("FSManager::createVFS, eKindOfFS=%d, sInputURL=%s", eKindOfFS, sInputURL.toLatin1().data() );
	if (eKindOfFS == VFS::UNKNOWNfs)
		return false;

	// --- inits
	QStringList slFSnameList;
	slFSnameList << "archivefs" << "ftpfs" << "localfs" << "iso9660fs" << "networkfs" << "sambafs" << "";
    // --- load plugin
    PluginManager *plugMgr = PluginManager::instance();
    VFS *pVFS = plugMgr->createVfsPlugin(slFSnameList[eKindOfFS], this);
    if ( ! pVFS ) {
        MessageBox::critical( this, tr("An error ocurred when loading a %1 plugin: %2").arg(slFSnameList[eKindOfFS]).arg(plugMgr->errorMessage()) );
        return false;
    }
	// OK, now we can delete old VFS and unload plugin
	removeVFS();
    m_pVFS = pVFS;

//	connect( m_pVFS, SIGNAL(signalResultOperation(VFS::Operation, bool)),
//	 parentWidget(), SLOT(slotResultOperation(VFS::Operation, bool)) );
	connect( m_pVFS, SIGNAL(signalResultOperation(VFS::Operation, bool)),
		this, SIGNAL(signalResultOperation(VFS::Operation, bool)) );

	connect( m_pVFS, SIGNAL(signalPrepareForListing()),
		this, SLOT(slotInitializeShowsList()) );
	connect( m_pVFS, SIGNAL(signalListInfo(const UrlInfoExt &)),
		m_pListView, SLOT(slotInsertItem(const UrlInfoExt &)) );
	connect( m_pVFS, SIGNAL(signalRemoveFileNameFromSIL(const QString &)),
		m_pListView, SLOT(slotRemoveNameFromSIL(const QString &)) );
	connect( m_pVFS, SIGNAL(signalSetInfo(const QString &)),
		this, SIGNAL(signalSetInfo(const QString &)) );

	connect( m_pVFS, SIGNAL(signalInsertMatchedItem(const UrlInfoExt &)),
		this, SLOT(slotInsertMatchedItem(const UrlInfoExt &)) );
	connect( m_pVFS, SIGNAL(signalUpdateFindStatus(uint, uint, uint)) ,
		this, SLOT(slotUpdateFindStatus(uint, uint, uint)) );

	connect( m_pVFS, SIGNAL(signalStartProgressDlgTimer(bool)),
		this, SLOT(slotStartProgressDlgTimer(bool)) );
	connect( m_pVFS, SIGNAL(signalShowFileOverwriteDlg(const QString &, QString &, int &)),
		this, SLOT(slotShowFileOverwriteDlg(const QString &, QString &, int &)) );
	connect( m_pVFS, SIGNAL(signalShowDirNotEmptyDlg(const QString &, int &)),
		this, SLOT(slotShowDirNotEmptyDlg(const QString &, int &)) );

	connect( m_pVFS, SIGNAL(signalPutFile(const QByteArray &, const QString &)),
		this, SIGNAL(signalPutFile(const QByteArray &, const QString &)) );

	return true;
}


void FSManager::removeVFS()
{
	if ( m_pVFS ) {
		delete m_pVFS; m_pVFS = NULL;
	}
}


void FSManager::rereadCurrentDir()
{
	m_sPreviousDirName = m_pListView->pathForCurrentItem();
	if ( m_sPreviousDirName.find("..") != -1 )
		m_sPreviousDirName = "";
	m_pListView->refresh(); // current list or current branch
	m_pListView->moveCursorToName( m_sPreviousDirName ); // need to for tree view
}


void FSManager::slotCloseCurrentDir()
{
	if ((m_sCurrentURL == "/" && m_pListView->isListView()) || ! m_pVFS)
		return;

	if ( m_pListView->isTreeView() && m_sCurrentURL != host() ) {
		ListViewItem *parent = (ListViewItem *)m_pListView->currentItem()->parent();
		if ( parent->text(0).isEmpty() )
			m_sCurrentURL = m_pListView->pathForCurrentItem();
		else
			m_sCurrentURL = host()+parent->fullName();
	}
	else
		m_sCurrentURL = m_pVFS->path();
// 	qDebug("FSManager::slotCloseCurrentDir(), m_sCurrentURL=%s", m_sCurrentURL.toLatin1().data() );

	if ( m_sCurrentURL != host() ) {
		if ( m_pListView->isTreeView() ) // need sets variable m_sPreviousDirName, it value gets moveCursorToName()
			m_sPreviousDirName = m_sCurrentURL;
		else { // 'm_sPreviousDirName' is contains current dir name (without absolute sPath)
			int numOfSlashes = m_sCurrentURL.count('/')-1;
			m_sPreviousDirName = m_sCurrentURL.section( '/', numOfSlashes, numOfSlashes );
			if ( FileInfoExt::isArchive(m_sPreviousDirName) ) {
				m_sPreviousDirName = "";
				emit signalResultOperation( VFS::Connect, FALSE ); // force create list with previous local fs directory
				return;
			}
		}
	}
	else { // if current FS is network or archive FS, and user want go out from it
		if ( m_eCurrentFilesSystem != VFS::LOCALfs ) {
			m_sPreviousDirName = "";
			emit signalResultOperation( VFS::Connect, FALSE ); // force create list with previous local fs directory
		}
		return;
	}

	// LISTpview
	if ( m_pListView->isListView() ) {
		UrlInfoExt ui;
		ui.setName("../"); // set location too
		ui.setLocation(m_sCurrentURL, FALSE); // FALSE made NOT using FileInfoExt::filePath
		slotOpen(ui);
		return;
	}

	// TREEpview
	m_pListView->close(); // closes brach for current pItem
	m_sNewURL = m_pListView->pathForCurrentItem(); // get absolute sPath
	m_sNewURL = FileInfoExt::filePath( m_sNewURL ); // gets previous sPath

	m_sCurrentURL = m_sNewURL;
	emit signalResultOperation( VFS::List, TRUE );
}


void FSManager::slotOpen( const UrlInfoExt & ui )
{
	QString sNewFileName = ui.location();
	if (ui.name() != host())
		sNewFileName += ui.name();

	if (ui.name().isEmpty())
		return;

	bool bIsArchive = FileInfoExt::isArchive(sNewFileName);

	//QString sNewFileName = (bIsArchive) ? sFileName : absoluteFileName( sFileName );
	qDebug() << "FSManager::slotOpen(). ui.isDir()= " << ui.isDir() << ", ui.name()= " << ui.name() << ", ui.location()= " << ui.location();
	qDebug() << "\t__ sNewFileName= " << sNewFileName;// << "\n\t__ m_sCurrentURL= " << m_sCurrentURL << "\n\t__ currentURL()= " << currentURL();

	if (m_bFoundFilesSys) { // the files found by FilesFindDlg
		m_sNewURL = sNewFileName;
		emit signalChangePanel(Panel::FILES_PANEL, sNewFileName, TRUE); // TRUE - current panel
		return;
	}

	if (ui.isDir()) { // in localFS
		if ( sNewFileName.endsWith("/../") ) {
			if ( sNewFileName == host()+"../" ) { // need to go out from FS
				qDebug() << "FSManager::slotOpen(). Try get out from FS.";
				if ( kindOfFilesSystem(sNewFileName) != VFS::LOCALfs )
					emit signalResultOperation( VFS::Connect, FALSE ); // force create list with previous local fs directory
				return;
			}
			qDebug() << "\t__goUp sNewFileName= " << sNewFileName;
			parsePath( sNewFileName );
			int numOfSlashes = m_sCurrentURL.count('/')-1;
			m_sPreviousDirName = m_sCurrentURL.section( '/', numOfSlashes, numOfSlashes );
		}
	}
	if (bIsArchive) {
		if (kindOfCurrentFS() != VFS::ARCHIVEfs) {
			emit signalOpenArchive( sNewFileName );
			return;
		}
	}
	else
	if (ui.isFile()) {
		openFile(sNewFileName);
		return;
	}

	if (m_pVFS != NULL) {
		//qDebug("__ call readDir, ui.isDir()=%d, sNewFileName= %s", ui.isDir(), sNewFileName.toLatin1().data() );
		m_pVFS->readDir( URL((m_sNewURL = sNewFileName)) ); // TODO spr. czy mozna dawac w param do readDir() UrlInfoExt
	}
	else
		qDebug("VFS object not found!");
}


void FSManager::openFile( const QString & , bool bEditMode )
{
	if ( m_pListView->numOfSelectedItems() == 0 )
		m_pListView->clearSelectedItemsList();

	m_pListView->collectAllSelectedNames( m_slSelectedFilesList );
	//qDebug() << "FSManager::openFile, m_slSelectedFilesList.count= " << m_slSelectedFilesList.count();

	Q3ListViewItem *pItem;
	uint nCurrentItemId = 0;
	QStringList slFilesList; // exists and readable files
	QString sFName;
	for( int i=0; i<m_slSelectedFilesList.count(); i++ ) {
		sFName = m_slSelectedFilesList[i];
		pItem  = m_pListView->findName( sFName );
		if ( pItem )
			if ( ! ((ListViewItem *)pItem)->isDir() )
//				if ( m_pVFS->isReadable(sFileName, TRUE, FALSE) )
				if ( m_pVFS->isReadable(sFName, TRUE, FALSE) )
					slFilesList.append( m_slSelectedFilesList[i] );
	}
	m_slSelectedFilesList = slFilesList;

	if ( m_pFilesAssociation ) {
		if ( m_pFilesAssociation->loaded() ) {
			m_slSelectedFilesList.clear();
			QStringList slFilesListForExtView;
			// collect the nFiles names for external viewer
			for ( int i=0; i<slFilesList.count(); i++ ) {
				sFName = slFilesList[ i ];
				if (m_pFilesAssociation->kindOfViewer(sFName) == EXTERNALviewer)
					slFilesListForExtView.append( sFName );
				else
					m_slSelectedFilesList.append( sFName );
			}
			if ( slFilesListForExtView.count() > 0 )
				runExternalViewer( slFilesListForExtView );
		}
	}

	if ( m_slSelectedFilesList.count() > 0 ) {
		m_bOpenedFileEditMode  = bEditMode;
		m_nItemIdForOpenedFile = nCurrentItemId;
		m_pVFS->isReadable( sFName, FALSE, TRUE );
	}
}


void FSManager::createFileView()
{
	FileView *pFV = new FileView( m_slSelectedFilesList, m_nItemIdForOpenedFile, m_bOpenedFileEditMode );
	pFV->setFilesAssociation( m_pFilesAssociation );

// 	connect( pFV, SIGNAL(signalUpdateItemOnLV( const QString & )),
// 		this, SLOT(slotUpdateItemOnLV( const QString & )) );
	connect( pFV, SIGNAL(signalReadFile(const QString &, QByteArray &, int &, int)),
		this, SLOT(slotReadFileToView(const QString &, QByteArray &, int &, int)) );

	disconnect( SIGNAL(signalReadyRead()) ); // old connection
	if ( m_pVFS )
		connect( m_pVFS, SIGNAL(signalReadyRead()), pFV, SLOT(slotReadyRead()) );
}


void FSManager::createNewEmptyFile( bool bCreateFile )
{
	QString kindOfFileStr = (bCreateFile) ? tr("file") : tr("directory");

	bool bOkPressed;
	bool bP1, bP2; // dummy
	QString sFileName = InputDlg::getText(
		tr("Please to give a")+" "+kindOfFileStr+" "+tr("name:"),
		tr("new")+" "+kindOfFileStr, (bCreateFile) ? VFS::Touch : VFS::MakeDir,
		QString("/qtcmd/FilesSystem/Make")+QString((bCreateFile) ? "FileNames" : "DirNames"),
		bOkPressed, bP1, bP2
	);
	if ( ! bOkPressed || sFileName.isEmpty() )
		return;

	// --- simple parser
	if ( sFileName.find("./") == 0 )
		sFileName.remove( 0, 2 );
	if ( sFileName.find("~/") == 0 )
		sFileName.replace( "~/", QDir::homeDirPath()+"/" );

	if (bCreateFile) {
		if ( sFileName.at(sFileName.length()-1) == '/' )
			sFileName = sFileName.length()-1;
	}
	else { // dir
		if ( sFileName.at(sFileName.length()-1) != '/' )
			sFileName += "/";
	}
	// ----

	QString sFullFileName = absoluteFileName(sFileName);

	if (m_eCurrentFilesSystem != VFS::LOCALfs) {
		MessageBox::information( this,
			tr("Making empty")+" "+kindOfFileStr+" - QtCommander",
			tr("Operation not yet supported in current file system.")
		);
		return;
	}

	// FIXME ponizsza obsluga jest poprawna tylko dla lokalnego systemu plikow
	// w przypadku zdalnego nalezy wykorzystac obsluge opoznionej reakcji
	bCreateFile ? m_pVFS->mkFile(sFullFileName) : m_pVFS->mkDir(sFullFileName);

	if ( errorCode() == VFS::AlreadyExists && bCreateFile ) {
		int nResult = MessageBox::yesNo( this,
			tr("Error")+" - QtCommander",
			sFullFileName+"\n\n"+tr("This")+" "+kindOfFileStr+" "+
			tr("already exists")+" !\n"+tr("Open it")+" ?",
			MessageBox::Yes // default is the 'Yes' button
		);
		if ( nResult == MessageBox::Yes )
			openFile( sFullFileName, TRUE ); // call virtual fun.
	}
}


void FSManager::removeFiles()
{
	m_pListView->collectAllSelectedNames( m_slSelectedFilesList );
	uint nNumToRemove = m_slSelectedFilesList.count(); // number of removed
	if ( nNumToRemove == 0 ) // maybe cursor match to item ".."
		return;

	int nResult;
	if ( m_bAskBeforeDelete ) {
		QString sInfo1, sInfo2;
		if ( nNumToRemove > 1 ) {
			sInfo1 = QString("%1 "+tr("nFiles selected")).arg(nNumToRemove);
			sInfo2 = tr("this nFiles");
		}
		else {
			sInfo1 = "\""+m_slSelectedFilesList[0]+"\"";
			sInfo2 = (sInfo1.at(sInfo1.length()-2) == '/') ? tr("directory") : tr("file");
		}
		nResult = MessageBox::yesNo( this,
			tr("Remove nFiles")+" - QtCommander",
			sInfo1+"\n\n"+tr("Do you really want to delete")+" "+sInfo2+" ?",
			MessageBox::Yes // default is the 'Yes' button
		);
	}
	else
		nResult = MessageBox::Yes;

	if ( nResult == MessageBox::Yes ) {
		showProgressDialog( VFS::Remove );
		m_pVFS->removeFiles( m_slSelectedFilesList );
	}
}


void FSManager::makeLink( const QString & sTargetPath, bool bEditOnly )
{
	UrlInfoExt uEntryInfo = ((ListViewItem *)m_pListView->currentItem())->entryInfo();
	QString sSourceFileName = m_pListView->currentItemText();
	if ( sSourceFileName == ".." )
		return;

	uint nFilesToCopy = 1;
	QString sTargetFileName;
	QStringList slTargetFilesList;
	sSourceFileName.insert( 0, m_sCurrentURL );
// 	QString msg1 = (bEditOnly) ? tr("Edit") : tr("Make");

	if ( bEditOnly ) {
		if ( ! uEntryInfo.isSymLink() )
			return;
		sTargetFileName = uEntryInfo.readLink();
		m_slSelectedFilesList.append( FileInfoExt::fileName(sSourceFileName) );
	}
	else {
		m_pListView->collectAllSelectedNames( m_slSelectedFilesList );
		nFilesToCopy = m_slSelectedFilesList.count();
		for (uint i=0; i<nFilesToCopy; i++)
			slTargetFilesList.append( sTargetPath+FileInfoExt::fileName(m_slSelectedFilesList[i]) );
	}

	bool bOK = FALSE;
	QString sTargetURL;
	if ( nFilesToCopy < 2 || bEditOnly ) {
		if ( ! bEditOnly )
			sTargetFileName = slTargetFilesList[0];

		sTargetURL = InputDlg::getText(
			"\""+m_slSelectedFilesList[0]+"\"", sTargetFileName, VFS::MakeLink,
			"/qtcmd/FilesSystem/MakeLinkURLs",
			bOK, m_bHardLink, m_bAlwaysOverwrite, bEditOnly, FALSE
		);
		parsePath( sTargetURL, FALSE ); // FALSE - none adds last slash (remove it if exists)

		if ( sTargetURL.isEmpty() )
			sTargetURL = "/" + m_pListView->currentItemText();

		slTargetFilesList[0] = sTargetURL;
	}
	else {
		int nResult = MessageBox::yesNo( this,
			tr("Make links")+" - QtCommander",
			QString("%1 "+tr("nFiles selected")).arg(nFilesToCopy)+
			"\n\n"+tr("Do you really want to make links for selected file(s)")+" ?",
			MessageBox::Yes // default is the 'Yes' button
		);
		if ( nResult == MessageBox::Yes )
			bOK = TRUE;
	}

	if ( bOK ) {
		if ( bEditOnly ) // target file is source file
			m_pVFS->editLink( (m_sTargetURL=sTargetURL), sSourceFileName );
		else {
			m_sTargetURL = FileInfoExt::filePath(slTargetFilesList[0]);
			m_pVFS->mkLinks( m_slSelectedFilesList, slTargetFilesList, m_bHardLink, m_bAlwaysOverwrite );
		}
	}
}


bool FSManager::copyFilesTo( const QString & sTargetPath, ListView::SelectedItemsList *pItemsList, bool bRemoveSrc )
{
	uint nFilesToCopy = pItemsList->count(); // number of files to copy

	if ( nFilesToCopy == 0 || sTargetPath.isEmpty()) // no selected items or empty target path
		return FALSE;

	bool bOK;
	QString sTargetURL, sCopyInfo;
	VFS::Operation eOperation = (bRemoveSrc) ? VFS::Move : VFS::Copy;
	QString sFirstFile = ((ListViewItem *)pItemsList->first())->entryInfo().location()+pItemsList->first()->text(0);
	QString sFileName = (nFilesToCopy == 1) ? "\""+sFirstFile+"\"" : QString::number(nFilesToCopy)+" "+tr("files selected");

	while (TRUE) {
		sTargetURL = InputDlg::getText(
			sFileName, sTargetPath, eOperation,
			"/qtcmd/FilesSystem/CopyMoveURLs",
			bOK, m_bSavePermision, m_bAlwaysOverwrite
		);
		if (! bOK || sTargetURL.isEmpty()) // user canceled an copying/moving operation
			return FALSE;

		parsePath(sTargetURL);

		if (nFilesToCopy > 1)
			break;
		else { // one file selected, check whether have it this same name
			if (sFirstFile+"/" != sTargetURL)
				break;
		}
		sCopyInfo = (bRemoveSrc) ? tr("move") : tr("copy");
		MessageBox::critical( this,
			tr("Source file:")+" \""+FileInfoExt::fileName(sFirstFile)+"\"\n"+
			tr("Target file:")+" \""+FileInfoExt::fileName(sTargetURL)+"\"\n\n"+
			tr("Cannot")+" "+sCopyInfo+" "+tr("to this same file!")
		);
	}
	parsePath(sTargetURL, FALSE); // need to remove the last slash

	// --- collecting selected files
	UrlInfoExt ui;
	//QListViewItem *item;
	QStringList slFilesList;
	ListView::SelectedItemsList::iterator it;

	for (it = pItemsList->begin(); it != pItemsList->end(); ++it)
	{
		ui = ((ListViewItem *)(*it))->entryInfo();
		sFirstFile = ui.location()+ui.name();
		(*it)->setSelected((sFirstFile != sTargetURL)); // check if copying from this same to this same, if result is true then deselect it
		if (sFirstFile != sTargetURL)
			slFilesList += ui.location()+(*it)->text(0);
	}
	/*
		for ( uint i=0; i<slFilesList.count(); i++ )
		if ( slFilesList[i] == sTargetURL ) {
			m_pListView->setSelected( m_pListView->findName(sTargetURL), FALSE );
			break;
		}
	 */

// sTargetURL : nazwa bez '/' na koncu, bez zadnego -> to moze byc kopiowanie do nowego pliku, albo do istniejacego w biezacym podkatalogu
// sTargetURL : nazwa bez '/' na koncu, bez pierwszego, ale ma wewnatrz -> kopiowanie do podkat.istniejacego wewnatrz tylko jesli pierwsza nazwa istnieje w biezacym
		//QString sFName = slFilesList[0];
	// --- check is copying to new file
/*	bool copyingToNewFile = FALSE;
	if ( m_pListView->isListView() ) {
		if ( sTargetURL.at(sTargetURL.length()-1) != '/' ) // docelowy to moze byc plik, ale to tez moze byc kat.- spr.to

			if ( slFilesList.count() > 1 ) {
				// nie mozna tylu kopiowac do jednego
			}
	}
*/
	m_sTargetURL = sTargetURL+"/"; // need to synchronize panels works in the FilesPanel class
	qDebug() << "FSManager::copyFilesTo(), info= " << sCopyInfo <<  " to sTargetURL= " << sTargetURL;
	showProgressDialog(VFS::Copy, bRemoveSrc, sTargetURL);
	m_pVFS->copyFiles(sTargetURL, slFilesList, m_bSavePermision, m_bAlwaysOverwrite, FALSE, bRemoveSrc);

	return TRUE;
}


void FSManager::parsePath( QString & sPath, bool bAlwaysAddSlash )
{
	if ( sPath.find( '~' ) != -1 )
		sPath.replace( QRegExp("~+"), QDir::homeDirPath() );
	else
	if ( sPath == "./" )
		sPath.replace( "./", QDir::currentDirPath()+"/" );

	if ( bAlwaysAddSlash ) {
		if ( sPath.at(sPath.length()-1) != '/' )
			sPath += "/";
	}
	else // remove last slash
		if ( sPath != "/" && sPath.at(sPath.length()-1) == '/' )
			sPath.remove( sPath.length()-1, 1 );

	sPath = absoluteFileName( sPath );

	// --- mini sPath-filter
	// an expression "\/[^\/]*\/\.\.\/" - have replaced string "/any_string/../" to "/"
	// here, all occures this string have been replaced
	while ( sPath.count(QRegExp("\\/[^\\/]*\\/\\.\\.\\/")) )
		sPath.replace(QRegExp("\\/[^\\/]*\\/\\.\\.\\/"), "/" );

	// for LocalFS nResult maybe "//", for other - eg. "ftp://" (equale 'rootName')
	if ( sPath.find("../") != -1 )
		sPath.replace( "../", "/" );

	if ( sPath != "/" )
		if ( sPath == rootName() ) {
			sPath = "";
			return;
		}

	QString sHost = sPath.left(sPath.find('/')+1);
	sPath.remove( 0, sHost.length() );
	// an expression "\\/\\.\\/+" - have been replaced all strings /./ to /
	while ( sPath.count( "/./") )
		sPath.replace( "/./", "/" );
	// an expression "\\/\\/+" - have been replaced all strings // to /
	while ( sPath.count( "//") )
		sPath.replace( "//", "/" );

	sPath.insert( 0, sHost );
}


bool FSManager::needToChangeFS( QString & sInputURL )
{
	bool bNeedToChange = FALSE;
	QString sTargetURL = sInputURL;

	if ( sTargetURL == "../" ) {
		slotCloseCurrentDir();
		return FALSE;
	}
	else
	if ( sTargetURL == "//" ) {
		UrlInfoExt ui;
		ui.setName(host());
		m_pListView->open(ui);
		//m_pListView->open( host() );
		return FALSE;
	}

	parsePath( sTargetURL );
	if ( sTargetURL.isEmpty() ) // go to previous path from LocalFS
		return TRUE;

	if ( m_eCurrentFilesSystem != VFS::LOCALfs )
		if ( sTargetURL == rootName() )
			sInputURL = "";


	bool sTargetURLisAbsolute =
		((sTargetURL.find("/") == 0) || (sTargetURL.find("ftp://") == 0) || (sTargetURL.find("sftp://") == 0) || (sTargetURL.find("smb://") == 0));
	if ( ! sTargetURLisAbsolute )
		sTargetURL.insert( 0, m_sCurrentURL ); // make sTargetURL as absolute

	qDebug("FSManager::needToChangeFS, (filtered) sTargetURL='%s', currentFS=%d, targetFS=%d", sTargetURL.toLatin1().data(), m_eCurrentFilesSystem, kindOfFilesSystem(sTargetURL));

	if ( m_eCurrentFilesSystem == kindOfFilesSystem(sTargetURL) ) {
		//UrlInfoExt ui(sTargetURL); // set location too
		//m_pListView->open(ui, TRUE);
		m_pListView->open( UrlInfoExt(sTargetURL), TRUE ); // UrlInfoExt set location too
	}
	else {
		if ( ! sInputURL.isEmpty() )
			sInputURL = sTargetURL;
		bNeedToChange = TRUE;
	}

	return bNeedToChange;
}


void FSManager::slotInitializeShowsList()
{ // --- get info from FS and initialize the List
    m_pListView->initFilesList(nameOfProcessedFile(), numOfAllFiles(), SystemInfo::getCurrentUserName(), SystemInfo::getCurrentGroupName());
	//numOfAllFiles() return 0
	//m_pListView->initFilesList( nameOfProcessedFile(), numOfAllFiles(), loggedOwner(), loggedGroup() );
}


VFS::KindOfFilesSystem FSManager::kindOfFilesSystem( const QString & sPath )
{
	VFS::KindOfFilesSystem eKindOfFS = VFS::UNKNOWNfs;
	bool bIsArchive = FileInfoExt::isArchive(sPath);

	if ( (sPath.at(0) == '/' || sPath == "~" || sPath == "./") && ! bIsArchive )
		eKindOfFS = VFS::LOCALfs;
	else
	if ( sPath.find("ftp://") == 0 )
		eKindOfFS = VFS::FTPfs;
	else
	if ( sPath.find("sftp://") == 0 )
		eKindOfFS = VFS::FTPfs;
	else
	if ( sPath.find("smb://") == 0 )
		eKindOfFS = VFS::SAMBAfs;
	else
	if ( sPath.find("arc://") == 0 || bIsArchive )
		eKindOfFS = VFS::ARCHIVEfs;
	else
	if (sPath.find("ffs://") == 0)
		return kindOfFilesSystem(sPath.right(sPath.length()-6));

	return eKindOfFS;
}


QString FSManager::rootName() const
{
	if ( m_eCurrentFilesSystem == VFS::LOCALfs )    return QString("/");
	else
	if ( m_eCurrentFilesSystem == VFS::FTPfs )      return QString("ftp://");
	else
	if ( m_eCurrentFilesSystem == VFS::SAMBAfs )    return QString("smb://");
	else
	if ( m_eCurrentFilesSystem == VFS::ARCHIVEfs )  return QString("arc://");

	return QString("?");
}


void FSManager::showProgressDialog( VFS::Operation eOperation, bool bMove, const QString & sTargetPath )
{
	m_pProgressDlg = new ProgressDlg();
	m_pProgressDlg->init( eOperation, s_bCloseProgressDlgAfterFinished, bMove, sTargetPath );
	connect( m_pProgressDlg, SIGNAL(signalCancel()),
		this, SLOT(slotCancelOperation()) );
	connect( m_pProgressDlg, SIGNAL(signalCloseAfterFinished( bool )),
		this, SLOT(slotCloseAfterFinished( bool )) );
	connect( m_pProgressDlg, SIGNAL(signalBackground()),
		this, SLOT(slotOperationInBackground()) );

	if ( m_pVFS ) {
		connect( m_pVFS, SIGNAL(signalNameOfProcessedFiles(const QString &, const QString &)),
			m_pProgressDlg, SLOT(slotSetFilesName(const QString &, const QString &)) );
// 		connect( m_pVFS, SIGNAL(signalNameOfProcessedFile(const QString &)),
// 			m_pProgressDlg, SLOT(slotSetSourceInfo(const QString &)) );
		connect( m_pVFS, SIGNAL(signalFileCounterProgress(int, bool, bool)),
			m_pProgressDlg, SLOT(slotSetProcessedNumber(int, bool, bool)) );
		connect( m_pVFS, SIGNAL(signalTotalProgress(int, long long)),
			m_pProgressDlg, SLOT(slotSetTotalProgress(int, long long)) );
		connect( m_pVFS, SIGNAL(signalDataTransferProgress( long long, long long, uint )),
			m_pProgressDlg, SLOT(slotDataTransferProgress( long long, long long, uint )) );
// 		connect( m_pVFS, SIGNAL(signalSetOperation(Operation)),
// 			m_pProgressDlg, SLOT(slotSetOperation(Operation)) );
	}

	m_pProgressDlg->show();
}


void FSManager::removeProgressDlg( bool bCheckWhetherCanToClose )
{
	if ( bCheckWhetherCanToClose )
		if ( ! s_bCloseProgressDlgAfterFinished )
			return;

	if ( m_pProgressDlg ) {
		delete m_pProgressDlg;  m_pProgressDlg = NULL;
		m_pListView->raise();
		m_pListView->setFocus();
	}
}


void FSManager::slotStartProgressDlgTimer( bool bStart )
{
	if ( m_pProgressDlg == NULL )
		return;

	(bStart) ? m_pProgressDlg->timerStart() : m_pProgressDlg->timerStop();
}


void FSManager::showPropertiesDialog()
{
	if ( m_pPropertiesDlg ) {
		delete m_pPropertiesDlg;  m_pPropertiesDlg = NULL;
	}
	m_pListView->collectAllSelectedNames( m_slSelectedFilesList );

	if ( m_slSelectedFilesList.count() == 0 )
		return;

	m_pPropertiesDlg = new PropertiesDlg;
	connect( m_pPropertiesDlg, SIGNAL(signalGetUrlForFile(const QString &, UrlInfoExt &)),
		this, SLOT(slotGetUrlInfoFromList(const QString &, UrlInfoExt &)) );
	connect( m_pPropertiesDlg, SIGNAL(signalOkPressed(const UrlInfoExt &, bool)),
		this, SLOT(slotChangePropertiesOfFile(const UrlInfoExt &, bool)) );
	connect( m_pPropertiesDlg, SIGNAL(signalRefresh( int )),
		this, SLOT(slotCountingDirectory( int )) );
	connect( m_pPropertiesDlg, SIGNAL(signalClose()),
		this, SLOT(slotClosePropertiesDlg()) );

    m_pPropertiesDlg->init(m_slSelectedFilesList, m_pFilesAssociation, SystemInfo::getCurrentUserName());
	m_pPropertiesDlg->show();
}


void FSManager::showFindFileDialog()
{
	if ( m_pFindFileDlg )
		delete m_pFindFileDlg;

	m_pFindFileDlg = new FindFileDlg;
	connect( m_pFindFileDlg, SIGNAL(signalFindStart(const FindCriterion &)),
		this, SLOT(slotStartFind(const FindCriterion &)) );
	connect( m_pFindFileDlg, SIGNAL(signalClose()), this, SLOT(slotCloseFindFileDlg()) );
	connect( m_pFindFileDlg, SIGNAL(signalPause(bool)), m_pVFS, SLOT(slotPause(bool)) );

	connect( m_pFindFileDlg, SIGNAL(signalJumpToTheFile(const QString &)),
		this, SLOT(slotJumpToTheFile(const QString &)) );
	connect( m_pFindFileDlg, SIGNAL(signalViewFile(const QString &, bool)),
		this, SLOT(slotViewFile(const QString &, bool)) );
	connect( m_pFindFileDlg, SIGNAL(signalDeleteFile(const QString &)),
		this, SLOT(slotDeleteFile(const QString &)) );
	connect( m_pFindFileDlg, SIGNAL(signalShowInPanel(Q3ListView *)),
		this, SLOT(slotShowInPanel(Q3ListView *)) );

	m_pFindFileDlg->init( m_sCurrentURL, m_pFilesAssociation );
	m_pFindFileDlg->show();
}


void FSManager::slotCancelOperation()
{
	if ( ! m_pVFS->operationHasFinished() ) {
// 		qDebug("FSManager::slotCancelOperation");
		m_pVFS->slotPause( TRUE );

		QString sOperation;
		VFS::Operation eOperation = m_pVFS->operation();
		if ( eOperation == VFS::Copy )
			sOperation = tr("copying"); // it's true for 'moving' too
		else
		if ( eOperation == VFS::Remove )
			sOperation = tr("removing");
		else
		if ( eOperation == VFS::SetAttributs )
			sOperation = tr("setting attributs");

		int nResult = MessageBox::yesNo( this,
			tr("Break")+" "+sOperation+" - QtCommander",
			tr("Do you really want to break an eOperation")+" ?",
			MessageBox::Yes // default is the 'Yes' button
		);
		if ( nResult == MessageBox::Yes )
			m_pVFS->breakOperation();
		else { // "No" or Esc
			m_pProgressDlg->timerStart(); // continue a timer
			m_pVFS->slotPause( FALSE ); // continue an eOperation
		}
	}
	else // an eOperation has been finished
		removeProgressDlg();
}


void FSManager::slotCloseAfterFinished( bool bCloseState )
{
	s_bCloseProgressDlgAfterFinished = bCloseState;
}


void FSManager::slotOperationInBackground()
{
	// trzeba odczepic okno od glownej app. (niech sie stanie samo glowna app.),
	// (oczywiscie kopiowanie musi byc robione w watku), potem zminimalizowac
}


QString FSManager::absoluteFileName( const QString & sFileName ) const
{
	QString sFullFileName = sFileName;

	bool absolutePath = (
		(sFileName.at(0) == '/') || (sFileName.find("ftp://") == 0) || (sFileName.find("sftp://") == 0) ||
		(sFileName.find("smb://") == 0) || (sFileName.find("arc://") == 0) || (sFileName.find("ffs://") == 0)
	);

	if (! absolutePath) { // is it relative path ?
		if ( FileInfoExt::isArchive(absHostURL()) ) {
			if ( m_sCurrentURL.at(absHostURL().length()-1) != '/' )
				sFullFileName.insert( 0, '/' );
		}
		sFullFileName.insert(0, absHostURL());
	}

	return sFullFileName;
}


QString FSManager::absHostURL() const
{
	if (m_pVFS == NULL)
		return QString::null;

	if ( m_eCurrentFilesSystem == VFS::LOCALfs && ! FileInfoExt::isArchive(m_sNewURL) )
		return path();

	return host().left(host().length()-1)+path();
}


bool FSManager::setWeighInPropetiesDlg()
{
	if ( ! m_pPropertiesDlg )
		return FALSE;

	long long nTotalWeigh;
	uint nTotalFiles, nTotalDirs;
	getWeighingStat( nTotalWeigh, nTotalFiles, nTotalDirs );

	m_pPropertiesDlg->setSize( nTotalWeigh, nTotalDirs, nTotalFiles, TRUE ); // bigInfo=TRUE as default value

	return TRUE;
}


void FSManager::initPropetiesDlgByUrl( const UrlInfoExt & uUrlInfo, const QString & sLoggedUserName )
{
	if ( ! m_pPropertiesDlg )
		return;

	m_pPropertiesDlg->init( uUrlInfo, m_pFilesAssociation, sLoggedUserName );

// 	long long nTotalWeigh;
// 	uint nTotalFiles, nTotalDirs;
// 	getWeighingStat( nTotalWeigh, nTotalFiles, nTotalDirs );

	m_pPropertiesDlg->setSize( uUrlInfo.size(), uUrlInfo.isDir() ? 1 : 0, uUrlInfo.isFile() ? 1 : 0, FALSE );
}


void FSManager::changeAttribut( ListView::ColumnName eColumn, const QString & sNewAttrib )
{
	m_pListView->clearSelectedItemsList();
	Q3ListViewItem *currentItem = m_pListView->currentItem();
	m_pListView->selectedItemsList()->append( currentItem );
	m_slSelectedFilesList.clear(); // strings list
	m_slSelectedFilesList.append( ((ListViewItem *)currentItem)->fullName() );
	m_sTargetURL = sNewAttrib;

	int nPermissions = -1;
	QDateTime lastModifiedTime;
	QString sNewName, sOwner, sGroup;
	UrlInfoExt uEntryInfo = ((ListViewItem *)(m_pListView->findName( m_slSelectedFilesList[0] )))->entryInfo();

	if ( eColumn == ListView::NAMEcol )
		sNewName = absoluteFileName( sNewAttrib );
	else
	if ( eColumn == ListView::TIMEcol )
		decodeTime( lastModifiedTime, uEntryInfo.lastModified(), sNewAttrib );
	else
	if ( eColumn == ListView::PERMcol )
		decodePermissions( nPermissions, uEntryInfo.permissionsStr(), sNewAttrib );
	else
	if ( eColumn == ListView::OWNERcol )
		sOwner = sNewAttrib;
	else
	if ( eColumn == ListView::GROUPcol )
		sGroup = sNewAttrib;

	uEntryInfo.setOwner( sOwner );
	uEntryInfo.setGroup( sGroup );
	uEntryInfo.setName( sNewName );
	uEntryInfo.setPermissions( nPermissions );
	uEntryInfo.setLastModified( lastModifiedTime );
	uEntryInfo.setLastRead( lastModifiedTime );

	m_pVFS->setAttrib( m_slSelectedFilesList, uEntryInfo, FALSE, 0 ); // FALSE - not recursive, 0 - changesForAll
	//m_pListView->countAndWeightSelectedFiles();
}


void FSManager::decodeTime( QDateTime & dtDateTime, const QDateTime & dtOldTime, const QString & sNewTime )
{
	QString sNewDT = sNewTime;
	// simplify some trailing characters to such one character
	while ( sNewDT.count(QRegExp("--+")) )
		sNewDT.replace( QRegExp("--+"), "-" );
	while ( sNewDT.count(QRegExp("\\.+")) )
		sNewDT.replace( QRegExp("\\.\\.+"), "-" );
	while ( sNewDT.count(QRegExp(" +")) )
		sNewDT.replace( QRegExp("  +"), "-" );
	while ( sNewDT.count(QRegExp("::+")) )
		sNewDT.replace( QRegExp("::+"), ":" );
	while ( sNewDT.at(sNewDT.length()-1) == '-' || sNewDT.at(sNewDT.length()-1) == ':' )
		sNewDT.remove( sNewDT.length()-1, 1 );
	while ( sNewDT.at(0) == '-' || sNewDT.at(0) == ':' )
		sNewDT.remove( 0, 1 );

	uint nTimeStrLen = sNewDT.length();
	if ( nTimeStrLen > 19 )
		sNewDT = sNewDT.left( 19 );


	QDate date = dtOldTime.date();
	QTime time = dtOldTime.time();
	int year  = date.year();
	int month = date.month();
	int day   = date.day();
	int hour   = time.hour();
	int minute = time.minute();
	int second = time.second();
	int y,m,d, hh,mm,ss;
	uint offset;
	y = m = d = hh = mm = ss = -1;

	bool bChangeDate = ( sNewDT.find('-') != -1 || sNewDT.find('.') != -1 || nTimeStrLen == 4 );
	bool bChangeTime = ( sNewDT.find(':') != -1 );

	if ( bChangeDate ) {
		if ( nTimeStrLen == 4 ) // is year only
			y = 1;
		else
		if ( nTimeStrLen == 4+3 ) // is year+month
			y = m = 1;
		else
		if ( nTimeStrLen == 4+3+3 ) // is year+month+day
			y = m = d = 1;
	}
	if ( bChangeTime ) {
		offset = (bChangeDate) ? 4+3+3 : 0;
		if ( nTimeStrLen == offset+3 ) // is [year+month+day]+hour
			hh = 1;
		else
		if ( nTimeStrLen == offset+3+3 ) // is [year+month+day]+hour+minute
			hh = mm = 1;
		else
		if ( nTimeStrLen == offset+3+3+3 ) // is [year+month+day]+hour+minute+second
			hh = mm = ss = 1;
	}


	bool bIsNumber = FALSE;
	if ( y > -1 ) {
		y = sNewDT.section( '-', 0,0 ).toUInt( &bIsNumber );
		if ( bIsNumber )
			year = y;
	}
	if ( m > -1 ) {
		m = sNewDT.section( '-', 1,1 ).toUInt( &bIsNumber );
		if ( bIsNumber )
			if ( month<12+1 )
				month = m;
	}
	if ( d > -1 ) {
		d = sNewDT.section( '-', 2,2 ).toUInt( &bIsNumber );
		if ( bIsNumber )
			if ( day<31+1 )
				day = d;
	}

	if ( bChangeTime ) {
		offset = (bChangeDate) ? 3 : 0;
		if ( hh > -1 ) {
			hh = sNewDT.section( '-', offset+0,offset+0 ).toUInt( &bIsNumber );
			if ( bIsNumber )
				hour = hh;
		}
		if ( mm > -1 ) {
			mm = sNewDT.section( '-', offset+1,offset+1 ).toUInt( &bIsNumber );
			if ( bIsNumber )
				if ( month<12+1 )
					minute = mm;
		}
		if ( ss > -1 ) {
			ss = sNewDT.section( '-', offset+2,offset+2 ).toUInt( &bIsNumber );
			if ( bIsNumber )
				if ( day<31+1 )
					second = ss;
		}
	}

	dtDateTime.setDate( QDate(year, month, day) );
	dtDateTime.setTime( QTime(hour, minute, second) );
}


void FSManager::decodePermissions( int & nPermissions, const QString & sOldPerm, const QString & sNewPerm )
{
	bool isOctalNumber;
	int  nPermStrLen = sNewPerm.length()-1;
	if ( nPermStrLen > 9 )
		nPermStrLen = 9;

	// decode from octal
	int perm = sNewPerm.toUInt( &isOctalNumber, 8 );
	if ( isOctalNumber ) {
		if ( perm < 36864 ) // 107777 it's max value (in decimal == 36863)
			nPermissions = perm;

		return;
	}

	// decode from string
	QChar c, cChr;
	QString sPerm = sOldPerm;
	for (int i=0; i<nPermStrLen; i++) {
		c = sPerm[i].lower();
		if ( c == 'r' || c == 'w' || c == 'x' )
			cChr = c;
		else
		if ( c == 's' || c == 't' )
			cChr = sPerm[i];
		else
			cChr = '-';

		sPerm[i] = cChr;
	}

	UrlInfoExt uUrlInfo;
	uUrlInfo.setPermissionsStr( sPerm );
	nPermissions = uUrlInfo.permissions();
}


void FSManager::runExternalViewer( const QStringList & slFilesToView )
{
	if ( ! m_pFilesAssociation )
		return;
	if ( ! m_pFilesAssociation->loaded() )
		return;

	QString sCmd;
	QStringList slArgList;
	for ( int i=0; i<slFilesToView.count(); i++ ) {
		sCmd = m_pFilesAssociation->appsForView(slFilesToView[i])+"\n"+slFilesToView[i];
		slArgList = QStringList::split( '\n', sCmd );
		if ( QFileInfo(slArgList[0]).exists() ) {
			m_Process.setArguments( slArgList );
			m_Process.start();
		}
	}
}


bool FSManager::archiveIsSupported( const QString & sFileName )
{
	QString sFName = FileInfoExt::archiveFullName( sFileName );
	if ( sFName.isEmpty() )
		return FALSE;

	const uint nMaxArch = 3;
	bool bSupported = FALSE;
	KindOfArchive eCurrentArchive = FileInfoExt::kindOfArchive(sFName);
	KindOfArchive etSupportedArch[nMaxArch] = { TARarch, TBZIP2arch, TGZIParch };

	for (uint i=0; i<nMaxArch; i++)
		if ( etSupportedArch[i] == eCurrentArchive ) {
			bSupported = TRUE;
			break;
		}

	return bSupported;
}

	// ------- SLOTs --------

void FSManager::slotSetCurrentURL( const QString & sNewURL )
{
	if (! sNewURL.isEmpty())
		m_sCurrentURL = sNewURL;
}


void FSManager::slotShowFileOverwriteDlg( const QString & sSourceFileName, QString & sTargetFileName, int & nResult )
{
	UrlInfoExt uSourceURL, uTargetURL;
	m_pVFS->slotGetUrlInfo( sSourceFileName, uSourceURL );
	m_pVFS->slotGetUrlInfo( sTargetFileName, uTargetURL );
	slotStartProgressDlgTimer( FALSE ); // stop
	if ( nResult != FileOverwriteDlg::Rename ) // force to show only rename dlg.
		nResult = FileOverwriteDlg::show( uTargetURL, uSourceURL );

	if ( nResult == FileOverwriteDlg::Rename ) {
		bool bOkPressed;
		bool bP1, bP2; // dummy
		QString sNewFileName = InputDlg::getText(
			"\""+sTargetFileName+"\"", FileInfoExt::fileName(sTargetFileName), VFS::Rename,
			"/qtcmd/FilesSystem/RenameURLs",
			bOkPressed, bP1, bP2
		);
		if ( bOkPressed ) {
			if ( sNewFileName.at(0) != '/' )
				sNewFileName.insert( 0, FileInfoExt::filePath(sTargetFileName) );
			sTargetFileName = sNewFileName;
			if ( m_sCurrentURL == FileInfoExt::filePath(sTargetFileName) ) {
				// need to replace name into selected list sSourceFileName to sTargetFileName
				Q3ValueList<Q3ListViewItem *>::iterator it;
				it = m_pListView->selectedItemsList()->find( m_pListView->findName(sSourceFileName) );
				(*it)->setText( 0, FileInfoExt::fileName(sTargetFileName) );
				// nazwy tych elementow trzeba zapisac na QStringList, a po wyw. signalResultOperation - ustawic nazwy
			}
		}
		else
			nResult = FileOverwriteDlg::No; // force skip file
	}

	if ( nResult != FileOverwriteDlg::Cancel )
		slotStartProgressDlgTimer( TRUE );
}


void FSManager::slotShowDirNotEmptyDlg( const QString & sDirName, int & nResult )
{
	slotStartProgressDlgTimer( FALSE ); // stop

	QStringList slBtnNames;
	slBtnNames << tr("&Yes") << tr("&No") << tr("&All") << tr("N&one") << tr("&Cancel");
	nResult = MessageBox::common( this,
		tr("Delete directory")+" - QtCommander",
		sDirName+"\n\n\t"+tr("Directory not empty")+".\n\t"+tr("Delete it recursively ?"),
		slBtnNames, MessageBox::Yes, QMessageBox::Warning
	);

	if ( nResult != MessageBox::Cancel ) // continue eOperation
		slotStartProgressDlgTimer( TRUE );
}


void FSManager::slotChangePropertiesOfFile( const UrlInfoExt & uNewUrlInfo, bool bAllSelected )
{
	if ( ! bAllSelected ) {
		QString sCurrentFileName = m_slSelectedFilesList[ m_pPropertiesDlg->currentItem() ];
		if ( sCurrentFileName.isEmpty() ) {
			m_pPropertiesDlg->close();
			return;
		}
		m_slSelectedFilesList.clear();
		m_slSelectedFilesList.append( sCurrentFileName );

		if ( ! uNewUrlInfo.name().isEmpty() )
			m_sTargetURL = absoluteFileName(uNewUrlInfo.name()); // for newAttributName()
	}

	if ( m_slSelectedFilesList.count() == 1 ) {
		UrlInfoExt uEntryInfo = ((ListViewItem *)(m_pListView->findName( m_slSelectedFilesList[0] )))->entryInfo();
		if ( uEntryInfo.isDir() && m_pPropertiesDlg->isRecursive() && ! uEntryInfo.isSymLink() )
			showProgressDialog( VFS::SetAttributs );
	}

	m_pVFS->setAttrib( m_slSelectedFilesList, uNewUrlInfo,
	 m_pPropertiesDlg->isRecursive(), m_pPropertiesDlg->changesFor()
	);

	m_pPropertiesDlg->close();
}


void FSManager::slotCountingDirectory( int nItemNum )
{
	if ( nItemNum < 0 ) {
		breakOperation();
		return;
	}

	if ( nItemNum < 1 ) // nItemNum == 0
		weighFiles( m_slSelectedFilesList, FALSE );
	else {
		if ( nItemNum-1 > m_slSelectedFilesList.count() ) // crash prevent
			qDebug( "FSManager::slotCountingDirectory(): index= %d is out of range for m_slSelectedFilesList!", nItemNum );
		else
			weighFiles( QStringList() << m_slSelectedFilesList[nItemNum-1], FALSE );
	}
}


void FSManager::slotClosePropertiesDlg()
{
	delete m_pPropertiesDlg; m_pPropertiesDlg = 0;
	m_pVFS->breakOperation();
}


void FSManager::slotCloseFindFileDlg()
{
	delete m_pFindFileDlg; m_pFindFileDlg = 0;
	m_pVFS->breakOperation();
}


void FSManager::slotStartFind( const FindCriterion & fcFindCriterion )
{
	if ( fcFindCriterion.isEmpty() )
		m_pFindFileDlg->findFinished();
	m_pVFS->find( fcFindCriterion, fcFindCriterion.isEmpty() );
}


void FSManager::slotInsertMatchedItem( const UrlInfoExt & uUrlInfo )
{
	if ( m_pFindFileDlg )
		m_pFindFileDlg->insertMatchedItem( uUrlInfo );
}


void FSManager::slotUpdateFindStatus( uint nMatches, uint nFiles, uint nDirectories )
{
	if ( m_pFindFileDlg )
		m_pFindFileDlg->updateFindStatus( nMatches, nFiles, nDirectories );
}


//void FSManager::slotJumpToTheFile( const UrlInfoExt & ui ) // TODO parametr przekazywany to UrlInfoExt
void FSManager::slotJumpToTheFile( const QString & sFileName )
{
	UrlInfoExt ui;
	ui.setName(sFileName); // set location too
	m_pListView->open(ui);
	m_pListView->moveCursorToName( sFileName );
}


void FSManager::slotViewFile( const QString & sFileName, bool bEditMode )
{
	openFile( sFileName, bEditMode );
}


void FSManager::slotDeleteFile( const QString & sFileName )
{
	m_pVFS->removeFiles( QStringList() << sFileName );
}


void FSManager::slotGetUrlInfoFromList( const QString & sFileName, UrlInfoExt & uUrlInfo )
{
	ListViewItem *pItem = (ListViewItem *)m_pListView->findName( sFileName );
	if ( pItem )
		uUrlInfo = pItem->entryInfo();
}


void FSManager::slotShowInPanel( Q3ListView * pListView )
{
	m_pListView->clearSelectedItemsList();

	Q3ListViewItemIterator it( pListView );

	for ( ; it.current(); ++it )
		m_pListView->selectedItemsList()->append((*it));

	emit signalChangePanel(Panel::FILES_PANEL_FORFIND, absHostURL(), FALSE); // FALSE - next panel
}

// -------- TYMCZASOWA FUNKCJA DLA TESTOW File Systemu
QString FSManager::cmdString( VFS::Operation op )
{
	QString s = "Unknown";

		 if ( op == VFS::HostLookup ) s = "HostLobOKup";
	else if ( op == VFS::NoneOp )   s = "NoneOp";
	else if ( op == VFS::Connect )  s = "Connect";
	else if ( op == VFS::List )     s = "List";
	else if ( op == VFS::Open )     s = "Open";
	else if ( op == VFS::Cd )       s = "Cd";
	else if ( op == VFS::Rename )   s = "Rename";
	else if ( op == VFS::Remove )   s = "Remove";
	else if ( op == VFS::MakeDir )  s = "MakeDir";
	else if ( op == VFS::MakeLink ) s = "MakeLink";
	else if ( op == VFS::Get )      s = "Get";
	else if ( op == VFS::Put )      s = "Put";
	else if ( op == VFS::Touch )    s = "Touch";
	else if ( op == VFS::Copy )     s = "Copy";
	else if ( op == VFS::Move )     s = "Move";
	else if ( op == VFS::Weigh )    s = "Weigh";
	else if ( op == VFS::Find )     s = "Find";
	else if ( op == VFS::SetAttributs ) s = "SetAttributs";

	return s;
}
